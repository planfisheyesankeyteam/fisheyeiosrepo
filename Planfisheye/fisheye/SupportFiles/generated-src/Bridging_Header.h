/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */
 


#import "AWSApiGatewayBridge.h"
#import "ProgressView.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#import <LoginWithAmazon/LoginWithAmazon.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import "GAI.h"
#import "GAITracker.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import <CommonCrypto/CommonHMAC.h>
