/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */

import AWSCore
import AWSAPIGateway

public class FEUserAPIsClient: AWSAPIGatewayClient {

	static let AWSInfoClientKey = "FEUserAPIsClient"
    let appSharedPrefernce = AppSharedPreference()

	private static let _serviceClients = AWSSynchronizedMutableDictionary()
	private static let _defaultClient: FEUserAPIsClient = {
		var serviceConfiguration: AWSServiceConfiguration?
        let serviceInfo = AWSInfo.default().defaultServiceInfo(AWSInfoClientKey)
        if let serviceInfo = serviceInfo {
            serviceConfiguration = AWSServiceConfiguration(region: serviceInfo.region, credentialsProvider: serviceInfo.cognitoCredentialsProvider)
        } else if (AWSServiceManager.default().defaultServiceConfiguration != nil) {
            serviceConfiguration = AWSServiceManager.default().defaultServiceConfiguration
        } else {
            serviceConfiguration = AWSServiceConfiguration(region: .Unknown, credentialsProvider: nil)
        }

        return FEUserAPIsClient(configuration: serviceConfiguration!)
	}()

	/**
	 Returns the singleton service client. If the singleton object does not exist, the SDK instantiates the default service client with `defaultServiceConfiguration` from `AWSServiceManager.defaultServiceManager()`. The reference to this object is maintained by the SDK, and you do not need to retain it manually.
	
	 If you want to enable AWS Signature, set the default service configuration in `func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?)`
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	        let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	        let configuration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialProvider)
	        AWSServiceManager.default().defaultServiceConfiguration = configuration
	 
	        return true
	     }
	
	 Then call the following to get the default service client:
	
	     let serviceClient = FEUserAPIsClient.default()

     Alternatively, this configuration could also be set in the `info.plist` file of your app under `AWS` dictionary with a configuration dictionary by name `FEUserAPIsClient`.
	
	 @return The default service client.
	 */ 

	public class func `default`() -> FEUserAPIsClient {
		return _defaultClient
	}

	/**
	 Creates a service client with the given service configuration and registers it for the key.
	
	 If you want to enable AWS Signature, set the default service configuration in `func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?)`
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	         let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	         let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
	         FEUserAPIsClient.registerClient(withConfiguration: configuration, forKey: "USWest2FEUserAPIsClient")
	
	         return true
	     }
	
	 Then call the following to get the service client:
	
	
	     let serviceClient = FEUserAPIsClient.client(forKey: "USWest2FEUserAPIsClient")
	
	 @warning After calling this method, do not modify the configuration object. It may cause unspecified behaviors.
	
	 @param configuration A service configuration object.
	 @param key           A string to identify the service client.
	 */

	public class func registerClient(withConfiguration configuration: AWSServiceConfiguration, forKey key: String) {
		_serviceClients.setObject(FEUserAPIsClient(configuration: configuration), forKey: key  as NSString)
	}

	/**
	 Retrieves the service client associated with the key. You need to call `registerClient(withConfiguration:configuration, forKey:)` before invoking this method or alternatively, set the configuration in your application's `info.plist` file. If `registerClientWithConfiguration(configuration, forKey:)` has not been called in advance or if a configuration is not present in the `info.plist` file of the app, this method returns `nil`.
	
	 For example, set the default service configuration in `func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) `
	
	     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
	         let credentialProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: "YourIdentityPoolId")
	         let configuration = AWSServiceConfiguration(region: .USWest2, credentialsProvider: credentialProvider)
	         FEUserAPIsClient.registerClient(withConfiguration: configuration, forKey: "USWest2FEUserAPIsClient")
	
	         return true
	     }
	
	 Then call the following to get the service client:
	 
	 	let serviceClient = FEUserAPIsClient.client(forKey: "USWest2FEUserAPIsClient")
	 
	 @param key A string to identify the service client.
	 @return An instance of the service client.
	 */
	public class func client(forKey key: String) -> FEUserAPIsClient {
		objc_sync_enter(self)
		if let client: FEUserAPIsClient = _serviceClients.object(forKey: key) as? FEUserAPIsClient {
			objc_sync_exit(self)
		    return client
		}

		let serviceInfo = AWSInfo.default().defaultServiceInfo(AWSInfoClientKey)
		if let serviceInfo = serviceInfo {
			let serviceConfiguration = AWSServiceConfiguration(region: serviceInfo.region, credentialsProvider: serviceInfo.cognitoCredentialsProvider)
			FEUserAPIsClient.registerClient(withConfiguration: serviceConfiguration!, forKey: key)
		}
		objc_sync_exit(self)
		return _serviceClients.object(forKey: key) as! FEUserAPIsClient
	}

	/**
	 Removes the service client associated with the key and release it.
	 
	 @warning Before calling this method, make sure no method is running on this client.
	 
	 @param key A string to identify the service client.
	 */
	public class func removeClient(forKey key: String) {
		_serviceClients.remove(key)
	}

	init(configuration: AWSServiceConfiguration) {
	    super.init()

	    self.configuration = configuration.copy() as! AWSServiceConfiguration
	    var URLString: String = AppConfig.userBaseURLForConfiguration
	    if URLString.hasSuffix("/") {
	        URLString = URLString.substring(to: URLString.index(before: URLString.endIndex))
	    }
	    self.configuration.endpoint = AWSEndpoint(region: configuration.regionType, service: .APIGateway, url: URL(string: URLString))
	    let signer: AWSSignatureV4Signer = AWSSignatureV4Signer(credentialsProvider: configuration.credentialsProvider, endpoint: self.configuration.endpoint)
	    if let endpoint = self.configuration.endpoint {
	    	self.configuration.baseURL = endpoint.url
	    }
	    self.configuration.requestInterceptors = [AWSNetworkingRequestInterceptor(), signer]
	}

    /*
     
     
     @param body 
     @param idtoken 
     
     return type: FERegisterNotificationResponse
     */
//    public func notificationsPost(body: FENotification, idtoken: String?) -> AWSTask<FERegisterNotificationResponse> {
//	    let headerParameters = [
//                   "Content-Type": "application/json",
//                   "Accept": "application/json",
//                   "idtoken": idtoken!
//	            ]
//	    
//	    let queryParameters:[String:Any] = [:]
//	    
//	    let pathParameters:[String:Any] = [:]
//	    
//	    return self.invokeHTTPRequest("POST", urlString: "/notifications", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: body, responseClass: FERegisterNotificationResponse.self) as! AWSTask<FERegisterNotificationResponse>
//	}

    /*
     
     
     @param phonenumber 
     @param countryCode 
     @param idtoken 
     
     return type: FEResendOTPResponse
     */
    public func phonenumberResendotpGet(phonenumber: String?, countryCode: String?, idtoken: String?) -> AWSTask<FEResendOTPResponse> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   "idtoken": idtoken!
	            ]

	    var queryParameters: [String: Any] = [:]
	    queryParameters["phonenumber"] = phonenumber
	    queryParameters["countryCode"] = countryCode

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("GET", urlString: "/phonenumber/resendotp", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: FEResendOTPResponse.self) as! AWSTask<FEResendOTPResponse>
	}

    /*
     
     
     @param phonenumber 
     @param countryCode 
     @param idtoken 
     
     return type: FESendOTPResponse
     */
    public func phonenumberSendotpGet(phonenumber: String?, countryCode: String?, idtoken: String?) -> AWSTask<FESendOTPResponse> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   "idtoken": idtoken!
	            ]

	    var queryParameters: [String: Any] = [:]
	    queryParameters["phonenumber"] = phonenumber
	    queryParameters["countryCode"] = countryCode

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("GET", urlString: "/phonenumber/sendotp", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: FESendOTPResponse.self) as! AWSTask<FESendOTPResponse>
	}

    /*
     
     
     @param idtoken 
     
     return type: FEPhoneNumberResponse
     */
    public func phonenumberVerifyGet(idtoken: String?) -> AWSTask<FEPhoneNumberResponse> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   "idtoken": idtoken!
	            ]

	    let queryParameters: [String: Any] = [:]

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("GET", urlString: "/phonenumber/verify", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: FEPhoneNumberResponse.self) as! AWSTask<FEPhoneNumberResponse>
	}

    /*
     
     
     @param phonenumber 
     @param otp 
     @param idtoken 
     
     return type: FEVerifyOTPResponse
     */
    public func phonenumberVerifyotpGet(phonenumber: String?, otp: String?, idtoken: String?) -> AWSTask<FEVerifyOTPResponse> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   "idtoken": idtoken!
	            ]

	    var queryParameters: [String: Any] = [:]
	    queryParameters["phonenumber"] = phonenumber
	    queryParameters["otp"] = otp

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("GET", urlString: "/phonenumber/verifyotp", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: FEVerifyOTPResponse.self) as! AWSTask<FEVerifyOTPResponse>
	}

    /*
     
     
     
     return type: Empty
     */
    public func testingGet() -> AWSTask<Empty> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json"

	            ]

	    let queryParameters: [String: Any] = [:]

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("GET", urlString: "/testing", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: Empty.self) as! AWSTask<Empty>
	}

    /*
     
     
     @param idtoken 
     
     return type: FEProfileResponse
     */
    public func usersUserGet(idtoken: String?) -> AWSTask<FEProfileResponse> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   "idtoken": idtoken!
	            ]

	    let queryParameters: [String: Any] = [:]

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("GET", urlString: "/users/user", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: nil, responseClass: FEProfileResponse.self) as! AWSTask<FEProfileResponse>
	}

    /*
     
     
     @param body
     @param idtoken
     
     return type: FERegisterNotificationResponse
     */
    public func notificationsPost(body: FENotification, idtoken: String?) -> AWSTask<FERegisterNotificationResponse> {
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "idtoken": idtoken!
        ]

        let queryParameters: [String: Any] = [:]

        let pathParameters: [String: Any] = [:]

        return self.invokeHTTPRequest("POST", urlString: "/notifications", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: body, responseClass: FERegisterNotificationResponse.self) as! AWSTask<FERegisterNotificationResponse>
    }

    /*
     
     
     @param body 
     @param idtoken 
     
     return type: Empty
     */
    public func usersUserAddressPut(body: FEAddress, idtoken: String?) -> AWSTask<FEAddressesResponse> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   "idtoken": idtoken!
	            ]

	    let queryParameters: [String: Any] = [:]

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("PUT", urlString: "/users/user/address", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: body, responseClass: FEAddressesResponse.self) as! AWSTask<FEAddressesResponse>
	}

    /*
     
     
     @param body 
     @param idtoken 
     
     return type: FEAddressesResponse
     */
    public func usersUserAddressPost(body: FEAddressesRequest, idtoken: String?) -> AWSTask<FEAddressesResponse> {
	    let headerParameters = [
                   "Content-Type": "application/json",
                   "Accept": "application/json",
                   "idtoken": idtoken!
	            ]

	    let queryParameters: [String: Any] = [:]

	    let pathParameters: [String: Any] = [:]

	    return self.invokeHTTPRequest("POST", urlString: "/users/user/address", pathParameters: pathParameters, queryParameters: queryParameters, headerParameters: headerParameters, body: body, responseClass: FEAddressesResponse.self) as! AWSTask<FEAddressesResponse>
	}

}
