//
//  File.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

public class newFEAddress {

    /** Address Id generate from server side when used try to add new address. */
    var addressId: String?
    /** Full mailing address, formatted for display or use on a mailing label. */
    var formatted: String?
    /** Full street address component, which MAY include house number, street name, Post Office Box, and multi-line extended street address information */
    var streetAddress: String?
    /** City or locality component. */
    var locality: String?
    /** State, province, prefecture, or region component. */
    var region: String?
    /** Zip code or postal code component. */
    var postalCode: String?
    /** Country name component. */
    var country: String?
    /** Type may be Home/Office/Business etc */
    var type: String?

    public static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        var params: [AnyHashable: Any] = [:]
        params["addressId"] = "addressId"
        params["formatted"] = "formatted"
        params["streetAddress"] = "street_address"
        params["locality"] = "locality"
        params["region"] = "region"
        params["postalCode"] = "postal_code"
        params["country"] = "country"
        params["type"] = "type"

        return params
    }
}
