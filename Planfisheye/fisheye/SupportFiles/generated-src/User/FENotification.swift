/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */

import Foundation
import AWSCore

public class FENotification: AWSModel {

    /** Based on the Logged in used, It will consider. Optional field. */
    var userId: String?
    /** Device Unique Id is nothing but UUID or IMEI number */
    var deviceUniqueId: String?
    /**  Notification Id is nothing but FCM Token ID. */
    var notificationId: String?
    /** IP Address */
    var ipAddress: String?
    /** TIme stamp should be in UTC format, which is genearated at server side. No need to send it from client Side. */
    var timestamp: String?

   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
		var params: [AnyHashable: Any] = [:]
		params["userId"] = "userId"
		params["deviceUniqueId"] = "deviceUniqueId"
		params["notificationId"] = "notificationId"
		params["ipAddress"] = "ipAddress"
		params["timestamp"] = "timestamp"

        return params
	}
}
