/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */

import Foundation
import AWSCore

public class FEAddress: AWSModel {

    /** Address Id generate from server side when used try to add new address. */
    var addressId: String?
    /** Full mailing address, formatted for display or use on a mailing label. */
    var formatted: String?
    /** Full street address component, which MAY include house number, street name, Post Office Box, and multi-line extended street address information */
    var streetAddress: String?
    /** City or locality component. */
    var locality: String?
    /** State, province, prefecture, or region component. */
    var region: String?
    /** Zip code or postal code component. */
    var postalCode: String?
    /** Country name component. */
    var country: String?
    /** Type may be Home/Office/Business etc */
    var type: String?

   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
		var params: [AnyHashable: Any] = [:]
		params["addressId"] = "addressId"
		params["formatted"] = "formatted"
		params["streetAddress"] = "street_address"
		params["locality"] = "locality"
		params["region"] = "region"
		params["postalCode"] = "postal_code"
		params["country"] = "country"
		params["type"] = "type"

        return params
	}
}
