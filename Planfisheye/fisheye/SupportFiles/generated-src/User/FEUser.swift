/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */

import Foundation
import AWSCore

public class FEUser: AWSModel {

    var name: String?
    var email: String?
    var phonenumber: String?
    var countryCode: String?
    var picture: String?
    var emailVerified: NSNumber?
    var phonenumberVerified: NSNumber?
    var addresses: [FEAddress]?
    var id: String?

   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        var params: [AnyHashable: Any] = [:]
        params["name"] = "name"
        params["email"] = "email"
        params["phonenumber"] = "phonenumber"
        params["countryCode"] = "countryCode"
        params["picture"] = "picture"
        params["emailVerified"] = "email_verified"
        params["phonenumberVerified"] = "phonenumber_verified"
        params["addresses"] = "addresses"
        params["id"] = "id"

        return params
	}
	class func addressesJSONTransformer() -> ValueTransformer {
		return  ValueTransformer.awsmtl_JSONArrayTransformer(withModelClass: FEAddress.self)
	}
}
