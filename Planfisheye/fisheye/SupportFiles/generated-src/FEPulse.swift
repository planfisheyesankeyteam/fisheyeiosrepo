/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 
 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at
 
 http://aws.amazon.com/apache2.0
 
 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */

import Foundation
import AWSCore

public class FEPulse: AWSModel {

    /** Contact Id */
    var contactId: String?
    /** IP Address */
    var latitude: String?
    /** IP Address */
    var longitude: String?
    /** Message */
    var message: String?
    /** true or false */
    var isencrypted: NSNumber?
    /** true or false */
    var createdTimeStamp: String?
    /** Value */
    var alertMessage: String?
    /**  pulse Id */
    var pulseId: String?
    /** 0 - requested,  1 - got response for your request,  2 - share location */
    var pulsetype: NSNumber?
    /** True - pulsed from your account, false - You got notification from another account  */
    var ispulsed: NSNumber?
    /** True - Your request for location share accepted and share back the location, false - not yet accepeted and not shared the location */
    var isaccepeted: NSNumber?

   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        var params: [AnyHashable: Any] = [:]
        params["contactId"] = "contactId"
        params["latitude"] = "latitude"
        params["longitude"] = "longitude"
        params["message"] = "message"
        params["isencrypted"] = "isencrypted"
        params["createdTimeStamp"] = "createdTimeStamp"
        params["alertMessage"] = "alertMessage"
        params["pulseId"] = "pulseId"
        params["pulsetype"] = "pulsetype"
        params["ispulsed"] = "ispulsed"
        params["isaccepeted"] = "isaccepeted"

        return params
    }
}
