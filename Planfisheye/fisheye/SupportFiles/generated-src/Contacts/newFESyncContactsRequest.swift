//
//  newFESyncContactsRequest.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

public class newFESyncContactsRequest {

    var contacts: [newFEContact]?

    public static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        var params: [AnyHashable: Any] = [:]
        params["contacts"] = "contacts"

        return params
    }
    class func contactsJSONTransformer() -> ValueTransformer {
        return  ValueTransformer.awsmtl_JSONArrayTransformer(withModelClass: newFEContact.self)

    }

}
