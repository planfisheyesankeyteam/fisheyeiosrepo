//
//  newContactsObject.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

public class newFEContact {

    var contactId: String?
    var name: String?
    var phoneNumber: String?
    var email: String?
    var company: String?
    var isfisheye: NSNumber?
    var secondaryPhoneNumbers: [String]?
    var secondaryEmails: [String]?
    var address: newFEAddress?

    public func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        var params: [AnyHashable: Any] = [:]
        params["contactId"] = "contactId"
        params["name"] = "name"
        params["phoneNumber"] = "phoneNumber"
        params["email"] = "email"
        params["company"] = "company"
        params["isfisheye"] = "isfisheye"
        params["secondaryPhoneNumbers"] = "secondaryPhoneNumbers"
        params["secondaryEmails"] = "secondaryEmails"
        params["address"] = "address"

        return params
    }
    class func addressJSONTransformer() -> ValueTransformer {
        return ValueTransformer.awsmtl_JSONDictionaryTransformer(withModelClass: newFEAddress.self)
    }
}
