//
//  newFEDisplayContactsResponse.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

public class newFEDisplayContactsResponse {

    var statusCode: String?
    var statusMessage: String?
    var contacts: [newFEContact]?

    public static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        var params: [AnyHashable: Any] = [:]
        params["statusCode"] = "statusCode"
        params["statusMessage"] = "statusMessage"
        params["contacts"] = "contacts"

        return params
    }
    class func contactsJSONTransformer() -> ValueTransformer {
        return  ValueTransformer.awsmtl_JSONArrayTransformer(withModelClass: newFEContact.self)
    }
}
