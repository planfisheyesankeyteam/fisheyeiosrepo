/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 
 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at
 
 http://aws.amazon.com/apache2.0
 
 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */

import Foundation
import AWSCore

public class FEContact: AWSModel {

    var contactId: String?
    var name: String?
    var phoneNumber: String?
    var email: String?
    var company: String?
    var isfisheye: NSNumber?
    var secondaryPhoneNumbers: [String]?
    var secondaryEmails: [String]?
    var address: FEAddress?

   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable: Any]! {
        var params: [AnyHashable: Any] = [:]
        params["contactId"] = "contactId"
        params["name"] = "name"
        params["phoneNumber"] = "phoneNumber"
        params["email"] = "email"
        params["company"] = "company"
        params["isfisheye"] = "isfisheye"
        params["secondaryPhoneNumbers"] = "secondaryPhoneNumbers"
        params["secondaryEmails"] = "secondaryEmails"
        params["address"] = "address"

        return params
    }
    class func addressJSONTransformer() -> ValueTransformer {
        return ValueTransformer.awsmtl_JSONDictionaryTransformer(withModelClass: FEAddress.self)
    }
}
