//
//  CodeDataManager.swift
//  fisheye
//
//  Created by Keerthi on 17/10/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class CodeDataManager: NSObject {

     static let shared = CodeDataManager()
    // MARK: Get Context

    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }

}
