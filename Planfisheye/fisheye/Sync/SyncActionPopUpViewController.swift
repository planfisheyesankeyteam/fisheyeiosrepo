//
//  SyncActionPopUpViewController.swift
//  fisheye
//
//  Created by SankeyProUser on 24/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class SyncActionPopUpViewController: UIViewController {

    let appSharedPrefernce = AppSharedPreference()
    var headingPopUp: String = ""
    var subHeadingPopUp: String = ""
    var nextAction: String = ""
    var prevAction = ""
    var method: String = ""
    var module: String = ""

    var noOfAddressToDelete: Int = 0

    @IBOutlet weak var warningImage: UIImageView!
    @IBOutlet weak var dismissImage: UIImageView!
    @IBOutlet weak var successImage: UIImageView!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var subHeading: UILabel!
    @IBOutlet weak var popUpView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialisingView()
        getData()
        dispPopUpMethod()
    }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SyncActionPopUpViewController)
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func getData() {
        self.headingPopUp = (self.appSharedPrefernce.getAppSharedPreferences(key: "heading") as? String)!
        self.subHeadingPopUp = (self.appSharedPrefernce.getAppSharedPreferences(key: "subheading") as? String)!
        self.nextAction = (self.appSharedPrefernce.getAppSharedPreferences(key: "action") as?  String)!
        self.method = (self.appSharedPrefernce.getAppSharedPreferences(key: "method") as?  String)!
        //       self.module = (self.appSharedPrefernce.getAppSharedPreferences(key: "module") as?  String)! ?? ""
        self.prevAction = (self.appSharedPrefernce.getAppSharedPreferences(key: "prevaction") as?  String)!
//        self.headingPopUp = "Logout"
        self.heading.text = self.headingPopUp
//        self.subHeadingPopUp = "Are you sure, you want to Logout"
        self.subHeading.text = self.subHeadingPopUp
    }

    func initialisingView() {
        self.warningImage.isHidden = true
        self.dismissImage.isHidden = true
        self.successImage.isHidden = true
        self.heading.text = ""

        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0

        self.popUpView.layer.cornerRadius = 6
    }
    func dispPopUpMethod() {
        if (self.nextAction == "warning") {
            self.warningImage.isHidden = false
            self.dismissImage.isHidden = true
            self.successImage.isHidden = true
        } else if (self.nextAction == "success") {
            self.warningImage.isHidden = true
            self.dismissImage.isHidden = true
            self.successImage.isHidden = false
        } else if (self.nextAction == "failure") {
            self.warningImage.isHidden = true
            self.dismissImage.isHidden = false
            self.successImage.isHidden = true
        }
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        nextButtonFunction()
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        prevButtonFunction()
    }

    func prevButtonFunction() {
        let prev = self.prevAction

        let moduleNext = self.module
        let methodCalled = self.method

        switch prev {
        case "dismissMethod":
            self.dismiss(animated: true, completion: nil)
        default:
            break
        }
    }

    func nextButtonFunction() {
        let next = self.nextAction
        let moduleNext = self.module
        let methodCalled = self.method

        switch methodCalled {

        case "deleteAddress":

            var contactId  = self.appSharedPrefernce.getAppSharedPreferences(key: "contactObjToOpen") as? Int ?? 0
            if(contactId == 0) {
                contactId = appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            }

            var contactObj = ContactObj()
            contactObj.contactId = contactId
            if(self.noOfAddressToDelete == 0) {
               contactObj.addressString1 = ""
            } else if(self.noOfAddressToDelete == 1) {
                contactObj.addressString2 = ""
            }
            contactObj.isBackendSync = false
            DatabaseManagement.shared.deleteFirstAddress(contactObj: contactObj)
            ContactsSync.shared.getNonSyncedContacts()
            ContactsSync.shared.backendSync()
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: REFRESH_ADDRESSES_NOTIFICATION, object: nil)

            }
            break
        default:
            break

        }
    }
}
