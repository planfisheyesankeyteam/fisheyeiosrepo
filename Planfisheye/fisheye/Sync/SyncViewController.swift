//  SyncViewController.swift
    //  Planfisheye
    //
    //  Created by Keerthi on 07/08/17.
    //  Modified by Vaishali Sagvekar
    //  Copyright © 2017 fisheye. All rights reserved.
    //

    import UIKit
    import Contacts
    import Toast_Swift
    import Crashlytics

    class SyncViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

      /* ----------------------------------------------- Variable Declaration -------------------------------------------- */

      var recentlyAdded: [ContactObj] = []
      var fisheyeContacts: [ContactObj] =  []                                        // to store FE Contacts
      var nonFisheyeContacts: [ContactObj] =  []                                    // to store non FE Contacts
      var selectedContactIdArray=[String]()
      var idtoken = ""
      var timer = Timer()
      var expandedRows: Int = -1
      var expandedRowsSection: Int = -1
      var isBackendSyncinProgress: String = ""
      var seconds = 10
      var isTimerRunning = false
      let appSharedPrefernce = AppSharedPreference()
      var sourceCellIndex:  IndexPath = IndexPath(row: -1, section: -1)
      var contactWillExist = ContactObj()
      var withContactObj = ContactObj()
      var blockIndex: IndexPath = IndexPath(row: -1, section: -1)
      var deleteIndex: IndexPath = IndexPath(row: -1, section: -1)
      var sectionData: [Int: [ContactObj]] = [:]
      var sections = ["Recently Added", "Contacts on Fisheye", "Contacts from Phone"]
      var syncText = SyncToastMessages()
      var appService = AppService()
      var contactStore = CNContactStore()
      var totalContactsCount = 0
      /* Created By Keerthi */
      var snapshot: UIView?
      ///< A snapshot of the row user is moving.
      var sourceIndexPath: IndexPath?
      ///< Initial index path, where gesture begins.
      var isChanged: Bool = false
      var pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
      /* END */

      /* ----------Variable Declaration ENd ---------- */

      /* ---------- Outlet Variable Declaration ----------*/

      @IBOutlet weak var popUpCentreConstraint: NSLayoutConstraint!

      @IBOutlet weak var popUpSuccessImg: UIImageView!
      @IBOutlet weak var popUpDismissImg: UIImageView!
      @IBOutlet weak var popUpWarningImg: UIImageView!
      @IBOutlet weak var pupUpView: UIView!
      @IBOutlet weak var rectangleAddContactBtn: UIButton!
      @IBOutlet weak var noContactsView: UIView!
      @IBOutlet weak var searchBar: UISearchBar!
      @IBOutlet weak var syncBtn: UIButton!
      @IBOutlet weak var tableView: UITableView!
      @IBOutlet weak var mainview: UIView!
      @IBOutlet weak var backButton: UISearchBar!
      @IBOutlet weak var headerView: UIView!

      // Labels
      @IBOutlet weak var popUpMessage: UILabel!
      @IBOutlet weak var popUpHeading: UILabel!
      @IBOutlet weak var messageLable: UILabel!
      @IBOutlet weak var syncContactsHeader: UILabel!
      @IBOutlet weak var noContactsLbl: UILabel!
      @IBOutlet weak var popUpCancelLbl: UILabel!
      @IBOutlet weak var popUpOkLbl: UILabel!

      /* ---------- Outlet Variable Declaration End ---------- */

      /* ---------- View Did Load ---------- */

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            self.syncText = SyncToastMessages.shared
            self.initializeLabels()
            self.messageLable.isHidden = true
            self.noContactsView.isHidden = true
            self.applyViewShadow()
            self.applyBtnShadow()
            self.applyPopUpShadow()
        //ToastManager.isTapToDismissEnabled(false)
        ToastManager.shared.isTapToDismissEnabled = false


        self.tableView.rowHeight = UITableView.automaticDimension                      // to update the height of contact cell
            if(ContactsSync.shared.isSyncInProgress) {                                        // check is frontend sync is in progress or not, if yes then show toast msg
                self.view.makeToast(self.syncText.syncInProgress, duration: self.syncText.syncToastMaxTimeInterval, position: .bottom)
            }

            self.sectionData =  [0: self.recentlyAdded, 1: self.fisheyeContacts, 2: self.nonFisheyeContacts]

            if((ContactsSync.shared.recentlyAddedContacts.count + ContactsSync.shared.FEContacts.count + ContactsSync.shared.nonFEContacts.count) > 0 ) {
                  self.fisheyeContacts = ContactsSync.shared.FEContacts
                  self.nonFisheyeContacts = ContactsSync.shared.nonFEContacts
                  self.recentlyAdded = ContactsSync.shared.recentlyAddedContacts
                  self.sectionData =  [0: self.recentlyAdded, 1: self.fisheyeContacts, 2: self.nonFisheyeContacts]
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Show contact list", label: "Contact list fetched successful", value: 0)
            } else {
                  self.refreshContactList()                // refresh all contact list
            }
            if self.recentlyAdded != nil {
                self.totalContactsCount = self.recentlyAdded.count
            }
            if self.fisheyeContacts != nil {
                self.totalContactsCount = self.totalContactsCount + self.fisheyeContacts.count
            }
            if self.nonFisheyeContacts != nil {
                self.totalContactsCount = self.totalContactsCount + self.nonFisheyeContacts.count
            }

            self.isBackendSyncinProgress = self.appSharedPrefernce.getAppSharedPreferences(key: "isBackendSyncInProgress") as? String ?? ""

            /* check is network available or not & backend syncing is in progress or not, if network available
             & backendsync is not in progress then perform backend sync. */
            if(NetworkStatus.sharedManager.isNetworkReachable() && isBackendSyncinProgress != "yes") {
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
            }

            self.stopLoader()
            tableView.isEditing = false
            tableView.delegate = self
            tableView.estimatedRowHeight = 91.0

            //******** Created by Keerthi murti commented by vaishali because it is not using now

            let longTapGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(longPressed(sender:)))
            self.tableView.addGestureRecognizer(longTapGesture)

            //******** commented by vaishali ENd

            // Do any additional setup after loading the view.
            self.searchBar.returnKeyType = .done

            /* Created By Vaishali
             description -  observer to refresh contact after new contact is added */
            NotificationCenter.default.addObserver(forName: REFRESH_CONTACTS_NOTIFICATION, object: nil, queue: nil) { (_) in
                  OperationQueue.main.addOperation ({
                        if(self.searchBar.isHidden) {
                              self.refreshContactList()
                        }
                  })
            }
            self.stopLoader()

            NotificationCenter.default.addObserver(forName: SHOW_TOAST_MESSAGE, object: nil, queue: nil) { (notification) in
                  OperationQueue.main.addOperation ({
                        self.refreshContactList()
                        let toastMessage = notification.userInfo! ["toastMessage"] as? String
                        let isSyncCompleted = notification.userInfo! ["isSyncCompleted"] as? String
                        if(isSyncCompleted == "No") {
                              self.view.hideAllToasts()
                            self.view.makeToast(toastMessage, duration: SyncToastMessages.shared.syncToastMaxTimeInterval, position: .bottom)
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.secondTimeInterval) {
                                    self.view.hideToast()
                                    self.view.makeToast(self.syncText.syncInProgress, duration: SyncToastMessages.shared.syncToastMaxTimeInterval, position: .bottom)
                              }
                        } else if( isSyncCompleted == "Yes") {
                              DispatchQueue.main.async {
                                    self.view.hideAllToasts()
                              }
//                              self.view.hideToast()
                              //                              self.view.makeToast(toastMessage, duration: 12.0, position: Any?.self)
                              //                              let delayInSeconds = 2.0
                              //                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                              //                                    self.view.hideAllToasts()
                              //                              }
                        }
                  })
            }

            self.addObservers()
            self.setLocalizationText()

      }

      /* ---------- View Did Load End ---------- */

      /* ---------- View Will Appear ---------- */

      override func viewWillAppear(_ animated: Bool) {
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as?
                  String ?? ""
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SyncViewController)
      }

      /* ----------View WIll Appear End ---------- */

      /* ---------- IBActions(click actions) ----------*/

      /* Created by Keerthi
       Modified by vaishali
       Description - to navigate to dashboard */
      @IBAction func backclick(_ sender: Any) {
            self.navigateToDashboard()
      }
      /* END */

      @IBAction func rectangleAddContactTapped(_ sender: Any) {
            self.navigateToAddContactPersonalInfo()
      }

      @IBAction func dashboardFeIconTapped(_ sender: Any) {
            self.navigateToDashboard()
      }

      /* Created by Keerthi modified by vaishali */
      @IBAction func searchBtnTapped(_ sender: Any) {
            self.syncBtn.isHidden = true
            self.syncContactsHeader.isHidden = true
            self.backButton.isHidden = true
            self.searchBar.isHidden = false
            self.searchBar.setShowsCancelButton(true, animated: true)
            self.searchBar.becomeFirstResponder()
            self.searchBar.delegate = self
      }
      /* END */

      /* Created by Keerthi modified by vaishali */
      @IBAction func rotateImage(_ sender: Any) {
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Sync button clicked", label: "", value: 0)
            if(!ContactsSync.shared.isSyncInProgress) {
                  
                  self.popUpHeading.text = self.syncText.syncContacts
                  self.popUpMessage.text = self.syncText.areUSureToSync
                  self.popUpOkLbl.text = self.syncText.sync
                  self.popUpDismissImg.isHidden = true
                  self.popUpSuccessImg.isHidden = true
                  self.popUpCentreConstraint.constant = self.syncText.popUpCenterConstraint
                  self.tableView.isUserInteractionEnabled = false
                DispatchQueue.main.async{

                  UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                        self.view.layoutIfNeeded()
                  })
                }
            } else {
                  self.view.hideAllToasts()
                  self.view.makeToast(self.syncText.waitSyncInProgress)
                 DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + SyncToastMessages.shared.syncToastMaxTimeInterval) {
                        self.view.hideToast()
                    self.view.makeToast(self.syncText.syncInProgress, duration: SyncToastMessages.shared.syncToastMaxTimeInterval, position: .bottom)
                  }
                  self.refreshContactList()
            }
      }

      @IBAction func popUpCancelBtnPressed(_ sender: Any) {
            self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
            self.tableView.isUserInteractionEnabled = true
            let indexPath = self.sourceCellIndex
            let cell = self.tableView.cellForRow(at: indexPath)
            cell?.alpha = 1.0
        DispatchQueue.main.async{

            UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                  self.view.layoutIfNeeded()
            })
        }
      }

      @IBAction func popUpOKBtnPressed(_ sender: Any) {
            let heading = self.popUpHeading.text
            switch heading {
            case self.syncText.mergeContact?:
                  self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
                  self.tableView.isUserInteractionEnabled = true
                  DispatchQueue.main.async{

                  UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                        self.view.layoutIfNeeded()
                  })
                  }
                  self.mergeContact(contactWillExist: self.contactWillExist, withContactObj: self.withContactObj)
                  break

            case self.syncText.unblockContact?:
                  self.startLoader()
                  let indexPath = self.blockIndex
                  let row = self.blockIndex.row
                  let section = self.blockIndex.section
                  var contactToUnblock = self.sectionData[section]![row]
                  let cell: SyncTableViewCell = self.tableView.cellForRow(at: indexPath) as! SyncTableViewCell
                  if(NetworkStatus.sharedManager.isNetworkReachable()) {
                        if(contactToUnblock.isBlocked == false) {
                              self.sectionData[section]![row].isBlocked = true
                              cell.blockOrUnblockContactLabel.text = self.syncText.unblockContact
                              contactToUnblock = self.sectionData[section]![row]
                        } else {
                              self.sectionData[section]![row].isBlocked = false
                              cell.blockOrUnblockContactLabel.text = self.syncText.blockContact
                              contactToUnblock = self.sectionData[section]![row]
                        }
                        self.tableView.beginUpdates()
                        self.tableView.endUpdates()
                        contactToUnblock.updatedAt = getCurrentTImeStamp()
                        DatabaseManagement.shared.blockOrUnblockContact(contactObj: contactToUnblock)
                        var contactArray: [ContactObj] = []
                        let contact = DatabaseManagement.shared.getContact(contactId: self.sectionData[section]![row].contactId)
                        contactArray.append(contact)
                        self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
                        self.tableView.isUserInteractionEnabled = true
                    DispatchQueue.main.async{
                    
                        UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                              self.view.layoutIfNeeded()
                        })
                    }
                        self.blockUnblockContact(contacts: contactArray, isBlocked: false)

                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Unblock contact clicked", label: "Unblock contact successful", value: 0)

                  } else {
                        var toastMsg = ""
                        if(contactToUnblock.isBlocked) {
                              toastMsg = self.syncText.noInternetToUnblock
                        } else {
                              toastMsg = self.syncText.noInternetToBlock
                        }
                        self.view.makeToast(toastMsg)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.secondTimeInterval) {
                              self.view.hideToast()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Unblock contact clicked", label: "No internet connection", value: 0)
                  }
                  self.stopLoader()
                  break
            case self.syncText.blockContact?:
                  self.startLoader()
                  let indexPath = self.blockIndex
                  let row = self.blockIndex.row
                  let section = self.blockIndex.section
                  var contactToBlock = self.sectionData[section]![row]
                  let cell: SyncTableViewCell = self.tableView.cellForRow(at: indexPath) as! SyncTableViewCell
                  if(NetworkStatus.sharedManager.isNetworkReachable()) {
                        if(contactToBlock.isBlocked == false) {
                              self.sectionData[section]![row].isBlocked = true
                              cell.blockOrUnblockContactLabel.text = self.syncText.unblockContact
                              contactToBlock = self.sectionData[section]![row]
                        } else {
                              self.sectionData[section]![row].isBlocked = false
                              cell.blockOrUnblockContactLabel.text = self.syncText.blockContact
                              contactToBlock = self.sectionData[section]![row]
                        }
                        self.tableView.beginUpdates()
                        self.tableView.endUpdates()
                        contactToBlock.updatedAt = getCurrentTImeStamp()
                        DatabaseManagement.shared.blockOrUnblockContact(contactObj: contactToBlock)
                        var contactArray: [ContactObj] = []
                        let contact = DatabaseManagement.shared.getContact(contactId: self.sectionData[section]![row].contactId)
                        contactArray.append(contact)
                        self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
                        self.tableView.isUserInteractionEnabled = true
                    DispatchQueue.main.async{

                        UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                              self.view.layoutIfNeeded()
                        })
                    }
                        self.blockUnblockContact(contacts: contactArray, isBlocked: true)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Block contact clicked", label: "Block contact successful", value: 0)
                  } else {
                        var toastMsg = ""
                        if(contactToBlock.isBlocked) {
                              toastMsg = self.syncText.noInternetToUnblock
                        } else {
                              toastMsg = self.syncText.noInternetToBlock
                        }
                        self.view.makeToast(toastMsg)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.secondTimeInterval) {
                              self.view.hideToast()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Block contact clicked", label: "No internet connection", value: 0)
                  }
                  self.stopLoader()
                  break

            case self.syncText.deleteContact?:
                  let section = self.deleteIndex.section
                  let contactId = self.sectionData[section]![self.deleteIndex.row].contactId
                  let isDeleted: Bool = DatabaseManagement.shared.deleteSelectedContact(contactId: contactId! )
                  var contact = ContactObj()
                  contact.contactId = contactId
                  contact.updatedAt = getCurrentTImeStamp()
                  DatabaseManagement.shared.deleteRecentlyAddedSinglecontact(contact: contact)
                  if isDeleted {
                        self.sectionData[section]?.remove(at: self.deleteIndex.row)
                        self.tableView.deleteRows(at: [self.deleteIndex], with: .fade)
                        ContactsSync.shared.getNonSyncedContacts()
                        ContactsSync.shared.backendSync()
                  } else {
                        self.view.makeToast(self.syncText.failedToDelete)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                              self.view.hideToast()
                        }
                  }
                  self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
                  self.tableView.isUserInteractionEnabled = true
                  DispatchQueue.main.async{

                  UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                        self.view.layoutIfNeeded()
                  })
                  }
                  break

            case self.syncText.syncContacts?:

                  let authorizationStatus = ContactsSync.shared.getContactsPermissionStatus()
                  switch authorizationStatus {
                  case .authorized:
                        self.startFrontEndSync()
                        self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
                        self.tableView.isUserInteractionEnabled = true
                        DispatchQueue.main.async{

                        UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                              self.view.layoutIfNeeded()
                        })
                        }
                        break
                  case .denied:
                        self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
                        self.tableView.isUserInteractionEnabled = true
                        DispatchQueue.main.async{

                        UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                              self.view.layoutIfNeeded()
                        })
                        }
                        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Would you like to open settings and grant permission to contacts?", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                        })
                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                              alert.dismiss(animated: true, completion: nil)
                        })
                        present(alert, animated: true)
                        break
                  case .notDetermined:
                        requestAccessToContacts()
                        self.popUpCentreConstraint.constant = CGFloat(self.syncText.popUpLeftConstraint)
                        self.tableView.isUserInteractionEnabled = true
                        DispatchQueue.main.async{

                        UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                              self.view.layoutIfNeeded()
                        })
                        }
                        break
                  default:
                        break
                  }
                  break
            default:
                  break
            }

      }

      /* ----------  IBActions(click actions) End ----------*/

      /*----------  Functions called in view did load & IBActions ----------*/

      func initializeLabels() {
            self.syncContactsHeader.text = syncText.syncContacts
            self.sections = [syncText.recentlyAddedContacts, syncText.fishEyeContacts, syncText.phoneContacts]
            self.noContactsLbl.text = self.syncText.noContactsFound
      }

      func navigateToAddContactPersonalInfo() {
            guard !tableView.isEditing else {
                  return
            }
            self.appSharedPrefernce.setAppSharedPreferences(key: "addContactInProgress", value: 0)
            self.appSharedPrefernce.setAppSharedPreferences(key: "contactObjToOpen", value: 0)
            loadPersonalInfoPage()
      }

      /* Rotating timer for particular seconds */
      func runTimer() {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
      }
      /* END */

      /* Updating the count - down */
        @objc func updateTimer() {
            if seconds < 1 {
                  timer.invalidate()
                  seconds = 5
            } else {
                  seconds -= 1
            }
      }
      /* END */

      func navigateToDashboard() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
            self.present(nextVC, animated: true, completion: nil)
      }

      /* Created  by vaishali
       Descripation - loader functionality, start & stop loader */
      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }
      /* END */

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      /* Created by Keerthi modified by vaishali  */
      func applyBtnShadow() {
            self.rectangleAddContactBtn.layer.cornerRadius = 6
            // border
            self.rectangleAddContactBtn.layer.borderWidth = 0
            self.rectangleAddContactBtn.layer.borderColor = UIColor.black.cgColor
      }
      /* END */

      /* Created by Keerthi modified by vaishali  */
      func applyViewShadow() {
            self.mainview.layer.cornerRadius = 8
            self.headerView.layer.cornerRadius = 8
            self.tableView.clipsToBounds = true
            if #available(iOS 11.0, *) {
                  self.tableView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            } else {
                  // Fallback on earlier versions
            }
            self.tableView.layer.cornerRadius = 8
            // border
            self.mainview.layer.borderWidth = 0
            self.mainview.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.mainview.layer.shadowColor = UIColor.black.cgColor
            self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainview.layer.shadowOpacity = 0.7
            self.mainview.layer.shadowRadius = 10.0
      }
      /* END */

      /* Created by Keerthi modified by vaishali  */
      func applyPopUpShadow() {
            self.pupUpView.layer.cornerRadius = 8

            // border
            self.pupUpView.layer.borderWidth = 0
            self.mainview.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.pupUpView.layer.shadowColor = UIColor.black.cgColor
            self.pupUpView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.pupUpView.layer.shadowOpacity = 0.7
            self.pupUpView.layer.shadowRadius = 10.0
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
        @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.syncContactsHeader.text = self.syncText.syncContacts
                  self.popUpCancelLbl.text = self.syncText.cancel
                  self.popUpOkLbl.text = self.syncText.ok
            }
      }
      /* END */

      /** Check contact is FE-User or not to hide/show FE-Icon on contact cell **/
      func isFEContact(contact: ContactObj) -> Bool {
            if(contact.contactFEId?.isEmpty)! {
                  return true
            } else {
                  return false
            }
      }
      /** ENd */

      func startFrontEndSync() {
        self.view.makeToast(self.syncText.syncInProgress, duration: SyncToastMessages.shared.syncToastMaxTimeInterval, position: .bottom)
            DispatchQueue.global().async {
                  ContactsSync.shared.isToCheckUpdates = true
                  ContactsSync.shared.frontEndSync()
            }
      }

      /* --------------- Table view functions --------------- */

      func numberOfSections(in tableView: UITableView) -> Int {
            return self.sections.count
      }

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return (self.sectionData[section]?.count)!
      }

      func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let view = UIView()
            let rowsInSection = self.sectionData[section]?.count ?? 0
            if(rowsInSection > 0) {
                  let title = UILabel()
                  title.font = UIFont(name: "Montserrat-Medium", size: 18)
                  title.text = self.sections[section]
                  title.textColor = UIColor.black
                  title.font = UIFont.italicSystemFont(ofSize: 12)
                  let size = CGSize(width: 600, height: 1000)
                  let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimatedFrame = NSString(string: title.text!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], context: nil)
                  view.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 25)
                  title.frame = CGRect(x: (view.frame.width/2)-(estimatedFrame.width/2), y: 4, width: 200, height: estimatedFrame.height + 5)
                  let constarint = NSLayoutConstraint(item: title, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
                  view.addSubview(title)
                  view.addConstraint(constarint)
                  view.backgroundColor =  UIColor.contactsTypeBg()
            }
            return view
      }

      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            let rowsInSection = self.sectionData[section]?.count ?? 0
            var height = 25
            if(rowsInSection == 0) {
                  height = 0
            }
            return CGFloat(height)
      }

      /* Description :- Content to show on particular contact cell */
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "SyncTableViewCell", for: indexPath) as! SyncTableViewCell

            cell.selectionStyle = .none
            var contactObj = ContactObj()
            contactObj = sectionData[indexPath.section]![indexPath.row]
            let name = contactObj.name
            let phone = contactObj.phoneNumber ?? ""
            let email = contactObj.email ?? ""
            let picture = contactObj.picture ?? ""

            let contactFEId = contactObj.contactFEId ?? ""
            let privacyEnabled = contactObj.privacyEnabled ?? true
            let privacySetForContactUser = contactObj.privacySetForContactUser ?? true
            let sharedData = contactObj.sharedData ?? 2
            let isBlocked = contactObj.isBlocked ?? false

            // Show contacts on screen //
            if(name == " ") {
                  cell.nameLbl.text = "Unknown"
            } else {
                  cell.nameLbl.text = name
            }

            if(phone.isEmpty && !email.isEmpty) {
                  cell.stackViewDetails.isHidden = true
                  cell.singleDetailDisplay.isHidden = false
                  cell.singleDetailDisplay.text = email
            } else if(!phone.isEmpty && email.isEmpty) {
                  cell.stackViewDetails.isHidden = true
                  cell.singleDetailDisplay.isHidden = false
                  cell.singleDetailDisplay.text = phone
            } else if(!phone.isEmpty && !email.isEmpty) {
                  if(privacyEnabled && sharedData == 0) {
                        cell.singleDetailDisplay.text = email
                        cell.singleDetailDisplay.isHidden = false
                        cell.separator.isHidden = true
                        cell.stackViewDetails.isHidden = true
                  } else if(privacyEnabled && sharedData == 1) {
                        cell.singleDetailDisplay.text = phone
                        cell.singleDetailDisplay.isHidden = false
                        cell.separator.isHidden = true
                        cell.stackViewDetails.isHidden = true
                  } else if(privacyEnabled && sharedData == 3){
                        cell.phoneNumLbl.text = "n/a"
                        cell.emailLbl.text = "n/a"
                        cell.stackViewDetails.isHidden = false
                        cell.singleDetailDisplay.isHidden = true
                        cell.separator.isHidden = false
                  }else {
                        cell.singleDetailDisplay.isHidden = true
                        cell.stackViewDetails.isHidden = false
                        cell.phoneNumLbl.text = phone
                        cell.emailLbl.text = email
                        cell.separator.isHidden = false
                  }
            } else {
                  cell.phoneNumLbl.text = "n/a"
                  cell.emailLbl.text = "n/a"
                  cell.stackViewDetails.isHidden = false
                  cell.singleDetailDisplay.isHidden = true
                  cell.separator.isHidden = false
            }

            cell.profileImage?.sd_setImage(with: URL(string: picture ), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width / 2
            cell.profileImage.clipsToBounds = true
            cell.profileImage.layer.borderWidth = 1.5
            cell.profileImage.layer.borderColor = UIColor.clear.cgColor

            if(contactFEId != "") {
                  cell.fisheyePurpleStripe.backgroundColor = UIColor.purpleRing()
                  if privacySetForContactUser {
                        cell.privacyImage.image = UIImage(named: "crosseye")
                  } else {
                        cell.privacyImage.image = UIImage(named: "eye")
                  }
                  cell.privacyImage.isHidden = false
            } else {
                  cell.fisheyePurpleStripe.backgroundColor = UIColor.clear
                  cell.privacyImage.isHidden = true
            }

            if(isBlocked == false) {
                  cell.blockOrUnblockContactLabel.text = self.syncText.blockContact
            } else {
                  cell.blockOrUnblockContactLabel.text = self.syncText.unblockContact
            }

            cell.whatsAppCallBtn.row = indexPath.row
            cell.whatsAppCallBtn.section = indexPath.section
            cell.whatsAppCallBtn.addTarget(self, action: #selector(placeWhatsAppCall(_:)), for: .touchUpInside)

            cell.nativeCall.row = indexPath.row
            cell.nativeCall.section = indexPath.section
            cell.nativeCall.addTarget(self, action: #selector(placeNativeCall(_:)), for: .touchUpInside)

            cell.blockOrUnblockContact.row = indexPath.row
            cell.blockOrUnblockContact.section = indexPath.section
            cell.blockOrUnblockContact.addTarget(self, action: #selector(blockOrUnblockContact(_:)), for: .touchUpInside)

            cell.skypeCallBtn.row = indexPath.row
            cell.skypeCallBtn.section = indexPath.section
            cell.skypeCallBtn.addTarget(self, action: #selector(placeFacetimeAppCall), for: .touchUpInside)
        
            cell.pulseButton1.row = indexPath.row
            cell.pulseButton1.section = indexPath.section
            cell.pulseButton1.addTarget(self, action: #selector(placePulseRequestAppCall), for: .touchUpInside)
        
            cell.pulseButton2.row = indexPath.row
            cell.pulseButton2.section = indexPath.section
            cell.pulseButton2.addTarget(self, action: #selector(placePulseShareAppCall), for: .touchUpInside)

            //on single tap expand contact
            let expandOrCollapseContactGesture = SyncContactTapGestureRecognizer(target: self, action: #selector(showContactOptions(sender:)))
            expandOrCollapseContactGesture.row = indexPath.row
            expandOrCollapseContactGesture.section = indexPath.section
            expandOrCollapseContactGesture.numberOfTapsRequired = 1
            cell.detailContactViewButton.addGestureRecognizer(expandOrCollapseContactGesture)

            // on double tap navigate to contact details page
            let openDetailsGesture = SyncContactTapGestureRecognizer(target: self, action: #selector(openDetail(sender:)))
            openDetailsGesture.row = indexPath.row
            openDetailsGesture.section = indexPath.section
            openDetailsGesture.numberOfTapsRequired = 2
            cell.detailContactViewButton.addGestureRecognizer(openDetailsGesture)
            expandOrCollapseContactGesture.require(toFail: openDetailsGesture)

            cell.isExpanded = self.expandedRows == indexPath.row && self.expandedRowsSection == indexPath.section
            cell.callLbl.text = self.syncText.call
            cell.skypeCallLbl.text = self.syncText.facetimeCall
            cell.whatsAppCallLbl.text = self.syncText.whatsAppCall
            cell.pulseLabel1.text = self.syncText.pulseRequest
            cell.pulseLabel2.text = self.syncText.pulseShare
            return cell
      }
      /* ENd **/

        @objc func openDetail(sender: Any) {
            let gesture = sender as! SyncContactTapGestureRecognizer
            let row = gesture.row
            let section = gesture.section
            let contactDetails: ContactObj = self.sectionData[section!]![row!]
            self.appSharedPrefernce.setAppSharedPreferences(key: "contactObjToOpen", value: contactDetails.contactId)
            self.appSharedPrefernce.setAppSharedPreferences(key: "addContactInProgress", value: 0)
        loadDetailsPersonalInfo(isEditMode: false)
      }

        @objc func placeNativeCall(_ sender: SyncContactCellButtons) {
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Call button clicked", label: "", value: 0)
            let row = sender.row
            let section = sender.section
            if(self.sectionData[section!]![row!].phoneNumber != "" && self.sectionData[section!]![row!].phoneNumber != nil) {
                  if(self.sectionData[section!]![row!].isBlocked) {
                        self.view.makeToast(self.syncText.unblock + " " + self.sectionData[section!]![row!].name + " " + self.syncText.toPlaceCall )
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                              self.view.hideToast()
                        }
                  } else {
                        if let url = URL(string: "tel://" + self.sectionData[section!]![row!].phoneNumber), UIApplication.shared.canOpenURL(url) {
                              if #available(iOS 10, *) {
                                    UIApplication.shared.open(url)
                              } else {
                                    UIApplication.shared.openURL(url)
                              }
                        }
                  }
            } else {
                  self.view.makeToast(self.syncText.phoneRequired)
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                        self.view.hideToast()
                  }
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Phone number required", label: "", value: 0)
            }
      }

        @objc func blockOrUnblockContact(_ sender: SyncContactCellButtons) {
            let row = sender.row
            let section = sender.section
            self.blockIndex = IndexPath(row: row!, section: section!)
            let contactToBlockOrUnblock = self.sectionData[section!]![row!]
            var isAllowedToBlock = true
            if(contactToBlockOrUnblock.phoneNumber.isEmpty && contactToBlockOrUnblock.email.isEmpty) {
                  self.view.makeToast(self.syncText.phoneOrEmailRequired)
                  isAllowedToBlock = false
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.secondTimeInterval, execute: {
                        self.view.hideToast()
                        return
                  })
            } else if(contactToBlockOrUnblock.isBlocked == false) {
                  self.popUpHeading.text = self.syncText.blockContact
                  if  ( contactToBlockOrUnblock.name != " ") {
                        self.popUpMessage.text = self.syncText.areUSureToBlock + " " +  (contactToBlockOrUnblock.name) + " ?"
                  } else {
                        self.popUpMessage.text = self.syncText.areUSureToBlockContact
                  }
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Block contact clicked", label: "", value: 0)
            } else {
                  self.popUpHeading.text = self.syncText.unblockContact
                  if  ( contactToBlockOrUnblock.name != " ") {
                        self.popUpMessage.text = self.syncText.areUSureToUnblock + " " +  (contactToBlockOrUnblock.name) + " ?"
                  } else {
                        self.popUpMessage.text = self.syncText.areUSureToUnblockContact
                  }
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Unblock contact clicked", label: "", value: 0)
            }

            self.popUpDismissImg.isHidden = true
            self.popUpSuccessImg.isHidden = true
            self.popUpOkLbl.text = self.syncText.ok
            if(isAllowedToBlock) {
                  self.popUpCentreConstraint.constant = self.syncText.popUpCenterConstraint
                  self.tableView.isUserInteractionEnabled = false
                DispatchQueue.main.async{

                  UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                        self.view.layoutIfNeeded()
                  })
                }
            }
      }

        @objc func placeFacetimeAppCall(_ sender: SyncContactCellButtons) {
            let row = sender.row
            let section = sender.section
            let contactToPlaceFacetimeCall = self.sectionData[section!]![row!]

            var receipantAddress = ""
            if contactToPlaceFacetimeCall.phoneNumber != "" {
                  receipantAddress = contactToPlaceFacetimeCall.phoneNumber
            } else if contactToPlaceFacetimeCall.email != "" {
                  receipantAddress = contactToPlaceFacetimeCall.email
            }

            if receipantAddress != ""{
                  if(contactToPlaceFacetimeCall.isBlocked) {
                    self.view.makeToast(self.syncText.unblock + " " + contactToPlaceFacetimeCall.name + " " + self.syncText.toPlaceFacetimeCall, duration: self.appService.firstTimeInterval, position: .bottom)
                  } else {
                        if let facetimeURL: NSURL = NSURL(string: "facetime://" + receipantAddress) {
                              let application: UIApplication = UIApplication.shared
                              if  let installed: Bool = application.canOpenURL(URL(string: "facetime:")!) {
                                    if  let url = URL(string: "facetime://" + receipantAddress) {
                                          UIApplication.shared.open(url, completionHandler: nil)
                                    }
                              } else {
                                    if let url = URL(string: self.syncText.facetimeItunesUrl) {
                                          UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                    }
                              }
                        } else {
                              if let url = URL(string: self.syncText.facetimeItunesUrl) {
                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                              }
                        }
                  }
            } else {
                self.view.makeToast(self.syncText.phoneOrEmailRequired, duration: self.appService.secondTimeInterval, position: .bottom)
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Facetime call button clicked", label: "Phone number or email required", value: 0)
            }

      }

        @objc func placeWhatsAppCall(_ sender: SyncContactCellButtons) {
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Whatsapp call button clicked", label: "", value: 0)
            let row = sender.row
            let section = sender.section
            let contactToPlaceWhatsappCall = self.sectionData[section!]![row!]
            if(contactToPlaceWhatsappCall.phoneNumber != "") {
                  if(contactToPlaceWhatsappCall.isBlocked) {
                        self.view.makeToast(self.syncText.unblock + " " + contactToPlaceWhatsappCall.name + " " + self.syncText.toPlaceWhatsAppCall )
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                              self.view.hideToast()
                        }
                  } else {
                        let installed: Bool = UIApplication.shared.canOpenURL(URL(string: "whatsapp:")!)
                        if installed {
                              let whatsappURL = URL(string: "whatsapp://send?phone=" + contactToPlaceWhatsappCall.phoneNumber + "&abid=" + contactToPlaceWhatsappCall.phoneNumber + "&text=hi")
                              if UIApplication.shared.canOpenURL(whatsappURL!) {
                                    UIApplication.shared.openURL(whatsappURL!)
                              }
                        } else {
                              UIApplication.shared.openURL((URL(string: self.syncText.whatsappItunesUrl))!)
                        }
                  }
            } else {
                  self.view.makeToast(self.syncText.phoneRequired)
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.secondTimeInterval) {
                        self.view.hideToast()
                  }
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Whatsapp call button clicked", label: "Phone number required", value: 0)
            }
      }
        
        func  openInsertCapsulePopUp(sendPulseTo:String,pulseType:Int) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "capsuleInsertPopUpViewController")  as! capsuleInsertPopUpViewController
//            nextVC.pulseInSyncProtocol = self as PulseInSyncProtocol
            nextVC.sendAPulseToFromSync = sendPulseTo
            nextVC.pulseTypeFromSync = pulseType
            nextVC.isPulseNeedToSendFromSync = true
            self.present(nextVC, animated: true, completion: nil)
        }
        
        @objc func placePulseRequestAppCall(_ sender: SyncContactCellButtons) {
            let row = sender.row
            let section = sender.section
            let contactToPlacePulseRequest = self.sectionData[section!]![row!]
            var sendTo = String(contactToPlacePulseRequest.contactId)
            if(contactToPlacePulseRequest.phoneNumber == "" && contactToPlacePulseRequest.email == ""){
                self.view.makeToast(self.pulseMsgs.noPhoneNoEmail,duration: self.pulseMsgs.toastDelayTimeForPulse, position: .bottom)
            }else if(contactToPlacePulseRequest.isBlocked){
                self.view.makeToast(self.pulseMsgs.needToUnblockContToSendPulseReq,duration: self.pulseMsgs.toastDelayTimeForPulse, position: .bottom)
            }else{
                self.openInsertCapsulePopUp(sendPulseTo : sendTo,pulseType: 0)
            }
            
//            guard NetworkStatus.sharedManager.isNetworkReachable()
//                else {
//                    self.view.makeToast(self.pulseMsgs.networkFailureMsg,duration: self.pulseMsgs.toastDelayTimeForPulse, position: Any!)
//                    return
//            }
//            self.startLoader()
//            self.selectedContactIdArray.append(String(contactToPlacePulseRequest.contactId))
//            let feedbackOperation = RequestAPulseNow(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray)
//            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
//                DispatchQueue.main.async {
//                    self.stopLoader()
//                    if(operation.statusCode == "200") {
//                        self.view.makeToast(self.pulseMsgs.nowPulseRequest,duration: self.pulseMsgs.toastDelayTimeForPulse, position: Any!)
//                    } else {
//                        self.view.makeToast(self.pulseMsgs.serverErrorOfPulse,duration: self.pulseMsgs.toastDelayTimeForPulse, position: Any!)
//                    }
//                }
//                
//            }
//            AppDelegate.addProcedure(operation: feedbackOperation)
//            self.selectedContactIdArray=[]
        }
        
        
        @objc func placePulseShareAppCall(_ sender: SyncContactCellButtons) {
            let row = sender.row
            let section = sender.section
            let contactToPlacePulseRequest = self.sectionData[section!]![row!]
            var sendTo = String(contactToPlacePulseRequest.contactId)
            if(contactToPlacePulseRequest.phoneNumber == "" && contactToPlacePulseRequest.email == ""){
                self.view.makeToast(self.pulseMsgs.noPhoneNoEmail,duration: self.pulseMsgs.toastDelayTimeForPulse, position: .bottom)
            }else if(contactToPlacePulseRequest.isBlocked){
                self.view.makeToast(self.pulseMsgs.needToUnblockContToSendPulseShare,duration: self.pulseMsgs.toastDelayTimeForPulse, position: .bottom)
            }else{
                self.openInsertCapsulePopUp(sendPulseTo : sendTo,pulseType: 1)
            }
            
            
//            guard NetworkStatus.sharedManager.isNetworkReachable()
//                else {
//                    self.view.makeToast(self.pulseMsgs.networkFailureMsg,duration: self.pulseMsgs.toastDelayTimeForPulse, position: Any!)
//                    return
//            }
//            self.startLoader()
//            self.selectedContactIdArray.append(String(contactToPlacePulseRequest.contactId))
//            let feedbackOperation = RequestAPulseNow(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray)
//            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
//                DispatchQueue.main.async {
//                    self.stopLoader()
//                    if(operation.statusCode == "200") {
//                        self.view.makeToast(self.pulseMsgs.nowPulseRequest,duration: self.pulseMsgs.toastDelayTimeForPulse, position: Any!)
//                    } else {
//                        self.view.makeToast(self.pulseMsgs.serverErrorOfPulse,duration: self.pulseMsgs.toastDelayTimeForPulse, position: Any!)
//                    }
//                }
//
//            }
//            AppDelegate.addProcedure(operation: feedbackOperation)
//            self.selectedContactIdArray=[]
        }

//      func changePrivacySettingOfContact(_ sender: SyncContactCellButtons){
//            let row = sender.row
//            let section = sender.section
//            let contactToChangePrivacy = self.sectionData[section!]![row!]
//            var contact = DatabaseManagement.shared.getContact(contactId: contactToChangePrivacy.contactId)
//            contact.privacySetForContactUser = !contact.privacySetForContactUser
//            var contacts: [ContactObj] = []
//            contacts.append(contact)
//            self.changePrivacySetting(contacts: contacts)
//      }

      /* Description :- this method handles row deletion */

        private func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {
                  self.popUpHeading.text = self.syncText.deleteContact
                  self.deleteIndex = indexPath
                  let name = self.sectionData[indexPath.section]![indexPath.row].name
                  if name == " " || name == ""{
                        self.popUpMessage.text = self.syncText.areUSureToDeleteContact
                  } else {
                        self.popUpMessage.text = self.syncText.areUSureToDelete + " " + name! + "?"
                  }
                  self.popUpDismissImg.isHidden = true
                  self.popUpSuccessImg.isHidden = true
                  self.popUpOkLbl.text = self.syncText.ok
                  self.popUpCentreConstraint.constant = self.syncText.popUpCenterConstraint
                  self.tableView.isUserInteractionEnabled = false
                DispatchQueue.main.async{
                    UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                        self.view.layoutIfNeeded()
                    })
                }


            } else if editingStyle == .insert {
                  // Not used in our example, but if you were adding a new row, this is where you would do it.
            }
      }

      func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
            return self.syncText.delete
      }

      func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
            let desCell: SyncTableViewCell? = tableView.cellForRow(at: destinationIndexPath) as? SyncTableViewCell
            let cell: SyncTableViewCell? = tableView.cellForRow(at: sourceIndexPath) as? SyncTableViewCell
        UIView.animate(withDuration: 1, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
            }, completion: {finish in
                UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                        cell?.transform = CGAffineTransform(scaleX: 0.15, y: 0.15)
                        cell?.center = (desCell?.center)!
                  }, completion: {_ in
                        cell?.transform =  CGAffineTransform.identity
                        self.sectionData[sourceIndexPath.section]?.remove(at: sourceIndexPath.row)
                        DispatchQueue.main.async {
                              self.tableView.reloadData()
                        }
                  })
            })
      }

      /* Created by Keerthi removed by vaishali this code will be use later */
        @objc func longPressed(sender: UILongPressGestureRecognizer) {

            let location: CGPoint = sender.location(in: self.tableView)
            let indexPath: IndexPath? = self.tableView.indexPathForRow(at: location)

            switch sender.state {
            case .began:
                  if (indexPath != nil) {

                        sourceIndexPath = indexPath
                        let cell: SyncTableViewCell? = tableView.cellForRow(at: indexPath!) as? SyncTableViewCell

                        // Take a snapshot of the selected row using helper method.
                        snapshot = customSnapshot(from: cell!)
                        // Add the snapshot as subview, centered at cell's center...
                        var center: CGPoint? = cell?.center
                        snapshot?.center = center!
                        snapshot?.alpha = 0.0
                        self.tableView.addSubview(snapshot!)
                        UIView.animate(withDuration: 0.1, animations: {() -> Void in
                              // Offset for gesture location.

                              center?.y = location.y
                              self.snapshot?.center = center!

                              self.snapshot?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                              self.snapshot?.alpha = 0.98
                              // Fade out.
                              cell?.alpha = 0.0
                        }, completion: {(_ finished: Bool) -> Void in
                              cell?.isHidden = true
                        })
                  }

            case .changed:
                  if let snap = snapshot {
                        var center: CGPoint = snap.center
                        center.y = location.y
                        snap.center = center

                        // Is destination valid and is it different from source?
                        if let destPath = indexPath, destPath != sourceIndexPath {

                              let sourceCell: SyncTableViewCell? = tableView.cellForRow(at: sourceIndexPath!) as? SyncTableViewCell
                              self.sourceCellIndex = sourceIndexPath!
                              sourceCell?.alpha = 0.0

                              if center.y > self.tableView.frame.origin.y  &&
                                    center.y < self.tableView.frame.origin.y + self.tableView.contentSize.height {

                                    isChanged = true
                                    if indexPath!.row > 0 {

                                          let indexPath = IndexPath(row: indexPath!.row-1, section: indexPath!.section) //(forRow: indexPath!.row-1, inSection: 0)
                                          tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                                    } else if  indexPath!.row < self.totalContactsCount-1 {

                                          if self.sectionData[0]?.count == 1 {
                                                let indexPath = IndexPath(row: indexPath!.row, section: indexPath!.section)
                                          } else {
                                                let indexPath = IndexPath(row: indexPath!.row+1, section: indexPath!.section)
                                          }
                                          tableView.scrollToRow(at: indexPath!, at: .bottom, animated: true)

                                    }
                              }
                        } else {
                              isChanged = false
                        }
                  }

            case .ended:
                  // Clean up.

                  if isChanged {
                        isChanged = false
                        if let path = sourceIndexPath, let destinationIndexPath = indexPath {
                              let destCell: SyncTableViewCell? = tableView.cellForRow(at: destinationIndexPath) as? SyncTableViewCell

                              let sourceCell: SyncTableViewCell? = tableView.cellForRow(at: path) as? SyncTableViewCell
                              sourceCell?.isHidden = false
                              sourceCell?.alpha = 0.0
                              self.sourceCellIndex = sourceIndexPath!
                            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                                    if(destCell != nil) {
                                          self.snapshot?.center = (destCell?.center)!
                                          self.snapshot?.transform = CGAffineTransform(scaleX: 0.05, y: 0.05)
                                          self.snapshot?.alpha = 0.0
                                          // Undo fade out.
                                          sourceCell?.alpha = 0.0
                                    } else {
                                          return
                                    }

                              }, completion: {_ in
                                    self.sourceIndexPath = nil
                                    self.snapshot?.removeFromSuperview()
                                    self.snapshot = nil
                                    self.popUpOkLbl.text = self.syncText.ok
                                    self.popUpCentreConstraint.constant = self.syncText.popUpCenterConstraint
                                    self.tableView.isUserInteractionEnabled = false
                                    self.popUpHeading.text = self.syncText.mergeContact
                                    if(self.sectionData[path.section]![path.row].name != " " && self.sectionData[destinationIndexPath.section]![destinationIndexPath.row].name != " ") {
                                          self.popUpMessage.text = self.syncText.areUSureToMergeContact + " " +  (self.sectionData[path.section]![path.row].name) + " " + self.syncText.with + " " + (self.sectionData[destinationIndexPath.section]![destinationIndexPath.row].name) + " ?"
                                    } else {
                                          self.popUpMessage.text = self.syncText.areUSureToMeregeTheseContacts
                                    }
                                    self.popUpDismissImg.isHidden = true
                                    self.popUpSuccessImg.isHidden = true
                                DispatchQueue.main.async{

                                    UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                                          self.view.layoutIfNeeded()
                                    })
                                }
                                    self.contactWillExist = ContactObj()
                                    self.withContactObj = ContactObj()
                                    self.contactWillExist = DatabaseManagement.shared.getContact(contactId: self.sectionData[destinationIndexPath.section]![destinationIndexPath.row].contactId)
                                    self.withContactObj = DatabaseManagement.shared.getContact(contactId: self.sectionData[path.section]![path.row].contactId)
                              })
                        }
                  } else {
                        if let path = sourceIndexPath {

                              let cell: SyncTableViewCell? = tableView.cellForRow(at: path) as? SyncTableViewCell

                              UIView.animate(withDuration: 0.25, animations: {() -> Void in
                                    self.snapshot?.transform = CGAffineTransform.identity
                                    self.snapshot?.alpha = 0.0
                                    // Undo fade out.
                                    cell?.alpha = 1.0
                                    cell?.isHidden = false
                              }, completion: {(_ finished: Bool) -> Void in
                                    self.sourceIndexPath = nil
                                    self.snapshot?.removeFromSuperview()
                                    self.snapshot = nil
                              })
                        }
                  }
            default:
                  break
            }
      }
      /* END */

      /* Description :- Show SyncOptions on single click on contact */
        @objc func showContactOptions(sender: Any) {
            let gesture = sender as! SyncContactTapGestureRecognizer
            let row = gesture.row
            let section = gesture.section
            let indexPath = IndexPath(row: row!, section: section!)

            guard let cell = tableView.cellForRow(at: indexPath ) as? SyncTableViewCell else { return }
            let isExpanded = cell.isExpanded
            cell.isExpanded = !isExpanded

            let previousIndexRow: Int = self.expandedRows
            let previousIndexSection: Int = self.expandedRowsSection

            let previousIndexPath = IndexPath(row: previousIndexRow, section: previousIndexSection)
            let previouslyExpandedCell = tableView.cellForRow(at: previousIndexPath ) as? SyncTableViewCell
            previouslyExpandedCell?.isExpanded = false
            self.tableView.beginUpdates()
            self.tableView.endUpdates()

            let cellRect = self.tableView.rectForRow(at: indexPath)
            let completelyVisible = self.tableView.bounds.contains(cellRect)
            if(!completelyVisible) {
                  DispatchQueue.main.async {
                        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                  }
            }
            if(isExpanded == false) {
                  self.expandedRows = row!
                  self.expandedRowsSection = section!
            } else {
                  self.expandedRows = -1
                  self.expandedRowsSection = -1
            }
      }
      /* END */

      /* Created by Keerthi modified by vaishali  */
      func mergeContact(contactWillExist: ContactObj?, withContactObj: ContactObj?) {
            var withContactObj = withContactObj
            var mergedContact = ContactsSync.shared.mergeContacts(contactExisting: contactWillExist!, contactObjWillMerge: withContactObj!)
            if(withContactObj?.addressString1 != nil && withContactObj?.addressString1 != "") {
                  mergedContact.addressString1 = withContactObj?.addressString1
            }

            if(withContactObj?.addressString2 != nil && withContactObj?.addressString2 != "") {
                  mergedContact.addressString2 = withContactObj?.addressString2
            }

            if(withContactObj?.company != nil && withContactObj?.company != "") {
                  mergedContact.company = withContactObj?.company
            }

            if(withContactObj?.companyEmail != nil && withContactObj?.companyEmail != "") {
                  mergedContact.companyEmail = withContactObj?.companyEmail
            }

            if(withContactObj?.title != nil && withContactObj?.title != "") {
                  mergedContact.title = withContactObj?.title
            }

            if(withContactObj?.website != nil && withContactObj?.website != "") {
                  mergedContact.website = withContactObj?.website
            }

            if(withContactObj?.notes != nil && withContactObj?.notes != "") {
                  mergedContact.notes = withContactObj?.notes
            }
            mergedContact.isBackendSync = false
            mergedContact.updatedAt = getCurrentTImeStamp()
            DatabaseManagement.shared.updatePersonalInfo(contactObj: mergedContact)
            DatabaseManagement.shared.updateAddress(contactObj: mergedContact)
            DatabaseManagement.shared.updateProfessionalDetails(contactObj: mergedContact)
            DatabaseManagement.shared.updateGeneralInfo(contactObj: mergedContact)

            withContactObj?.mergeParentId = String(mergedContact.contactId)
            withContactObj?.updatedAt = getCurrentTImeStamp()
            DatabaseManagement.shared.mergeContacts(contactObj: withContactObj!)
            DatabaseManagement.shared.deleteRecentlyAddedSinglecontact(contact: withContactObj!)
            if(NetworkStatus.sharedManager.isNetworkReachable()) {
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
            }

            /* Notification to refresh contact list on SyncViewController*/
            DispatchQueue.main.async {
                  NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
            }
      }
      /* END */

      /* --------------- Table view functions End --------------- */

      /* ----------------------  API Calls ------------------------- */

      func changePrivacySetting(contacts: [ContactObj]) {
            self.startLoader()
            let changePrivacy = ChangePrivacySetting()
            changePrivacy.contactObjs = contacts
            changePrivacy.addDidFinishBlockObserver { [unowned self] (_, _) in
                  DispatchQueue.main.async {
                        NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                        self.stopLoader()
                  }
            }
            AppDelegate.addProcedure(operation: changePrivacy)
      }
      /* ----------------------  API Calls End ---------------------- */

    }

    /* ---------- Search contact functionality ---------- */

    /* Created by Keerthi modified by vaishali
     Description - To Search contact by name, mobile no, email id */
    extension SyncViewController: UISearchBarDelegate {
      // MARK: - UISearchBar Delegate

      func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            if( searchBar.text == "") {
                  self.refreshContactList()
            }
      }

      func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.syncBtn.isHidden = false
            self.syncContactsHeader.isHidden = false
            self.backButton.isHidden = false
            self.searchBar.text = ""
            self.searchBar.resignFirstResponder()
            self.searchBar.isHidden = true
            self.refreshContactList()
            self.expandedRows = -1
            self.expandedRowsSection = -1
      }

      func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            let searchText: String = searchBar.text!
            let searchFEContacts: [ContactObj] = DatabaseManagement.shared.searchFEContacts(searchText: searchText)
            let searchNonFEContacts: [ContactObj] = DatabaseManagement.shared.searchNonFEContacts(searchText: searchText)
            let searchRecentlyAddedContacts = DatabaseManagement.shared.searchRecentlyAddedContacts(searchText: searchText)
            self.refreshSearchedContactList(searchRecentlyAddedContacts: searchRecentlyAddedContacts, searchFEContacts: searchFEContacts, searchNonFEContacts: searchNonFEContacts, searchText: searchText)
            self.searchBar.resignFirstResponder()
            self.expandedRows = -1
            self.expandedRowsSection = -1
      }

      /* ---------- Search contact functionality End ---------- */

      /* ---------- Refresh contact list after search  ---------- */

      func refreshSearchedContactList (searchRecentlyAddedContacts: [ContactObj], searchFEContacts: [ContactObj], searchNonFEContacts: [ContactObj], searchText: String) {

            self.fisheyeContacts = searchFEContacts
            self.fisheyeContacts.sort { $0.name < $1.name }

            self.nonFisheyeContacts = searchNonFEContacts
            self.nonFisheyeContacts.sort { $0.name < $1.name }

            self.recentlyAdded = searchRecentlyAddedContacts
            self.recentlyAdded.sort { $0.name < $1.name }
            /* all contact list */
            self.sectionData =  [0: self.recentlyAdded, 1: self.fisheyeContacts, 2: self.nonFisheyeContacts]
            self.totalContactsCount = self.recentlyAdded.count + self.fisheyeContacts.count + self.nonFisheyeContacts.count
            /*** End **/
            DispatchQueue.main.async {
                  self.tableView.reloadData()
            }

            if(self.totalContactsCount == 0) {
                  self.messageLable.isHidden = false
                  self.messageLable.text = self.syncText.noContactsFoundFromSearch + " : " +  searchText
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Search bar serch button clicked", label: "Contacts search successful", value: 0)
            } else {
                  self.messageLable.isHidden = true
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Search bar serch button clicked", label: "No contacts found", value: 0)
            }
      }

      /* Created by Keerthi removed by vaishali can use afterwards if it is needed */
      func customSnapshot(from inputView: UIView) -> UIView {
            // Make an image from the input view.
            UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0)
            inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            // Create an image view.
            let snapshot: UIView? = UIImageView(image: image)
            snapshot?.layer.masksToBounds = false
            snapshot?.layer.cornerRadius = 0.0
            snapshot?.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
            snapshot?.layer.shadowRadius = 5.0
            snapshot?.layer.shadowOpacity = 0.4
            return snapshot!
      }
      /* END */

      func blockUnblockContact(contacts: [ContactObj], isBlocked: Bool) {
            self.startLoader()
            let blockUnblockAPI = blockUnblockOperation()
            blockUnblockAPI.contactObjs = contacts
            blockUnblockAPI.isBlocked = isBlocked
            blockUnblockAPI.addDidFinishBlockObserver { [unowned self] (_, _) in
                  DispatchQueue.main.async {
                        self.stopLoader()
                  }
            }
            AppDelegate.addProcedure(operation: blockUnblockAPI)
      }
    }
    /* END */

    /* --------------------------------------- Refresh contact list after search ENd -------------------------------------- */

    /** Created by vaishali
     Description - To Refresh all contact list **/
    extension SyncViewController {

      func refreshContactList() {
            let refreshOperation = RefreshContactList()
            refreshOperation.addDidFinishBlockObserver { [unowned self] (_, _) in
                  if ContactsSync.shared.FEContacts != nil {
                        self.fisheyeContacts = ContactsSync.shared.FEContacts
                  }
                  if ContactsSync.shared.nonFEContacts != nil {
                        self.nonFisheyeContacts = ContactsSync.shared.nonFEContacts
                  }
                  if ContactsSync.shared.recentlyAddedContacts != nil {
                        self.recentlyAdded = ContactsSync.shared.recentlyAddedContacts
                  }
                  self.sectionData =  [0: self.recentlyAdded, 1: self.fisheyeContacts, 2: self.nonFisheyeContacts]
                  self.totalContactsCount = self.recentlyAdded.count + self.fisheyeContacts.count + self.nonFisheyeContacts.count
                
                  if( self.totalContactsCount == 0) {
                        DispatchQueue.main.async {
                              self.noContactsView.isHidden = false
                              self.rectangleAddContactBtn.isUserInteractionEnabled = true
                              self.tableView.isHidden = true
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Show contact list", label: "No contats found", value: 0)
                        }
                  } else {
                        DispatchQueue.main.async {
                              self.noContactsView.isHidden = true
                              self.rectangleAddContactBtn.isUserInteractionEnabled = false
                              self.tableView.isHidden = false
                        }
                  }
                  DispatchQueue.main.async {
                        self.messageLable.isHidden = true
                        self.tableView.reloadData()
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync contacts", action: "Show contact list", label: "Contacts fetched successfully", value: 0)
                        self.syncBtn.layer.removeAnimation(forKey: "syncRotation")
                  }
            }
            AppDelegate.addProcedure(operation: refreshOperation)
      }
    }
    /* END */

    /*---------- Refresh Contacts list functionality End ---------- */

    /* Created by vaishali
     Description - TO remove blank spaces from phone number of the contacts
     that are fetched from the phone*/
    extension String {
      func removingWhitespaces() -> String {
            return components(separatedBy: .whitespaces).joined()
      }
    }

    extension SyncViewController: PulseInSyncProtocol {
        //request
        func toEnterCapsuleWhileSendingPulseRequestFromSync(toReload: Bool) {
           // self.openInsertCapsulePopUp(sendPulseTo:String,pulseType:Int)
        }
        //share
        func toEnterCapsuleWhileSendingPulseShareFromSync(toReload: Bool) {
            //self.openInsertCapsulePopUp(sendPulseTo:String,pulseType:Int)
        }
    }

    /* END */

    //    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
    //        return .none
    //    }
    //
    //    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
    //        return false
    //    }
    //

    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        guard !tableView.isEditing else {
    //            return
    //        }
    //
    //        let tempnameArray : newFEContact!

    //        if !self.searchBar.isHidden && searchBar.text != "" {
    ////            self.searchBar.resignFirstResponder()
    //            dict = filteredArray[indexPath.row]
    //        } else {
    //            dict = self.feContacts![indexPath.row]//        }

    //        let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
    //        contactVC = storyboard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
    ////        contactVC.contact = dict
    //        contactVC.delegate = self
    //        contactVC.view.frame = CGRect.init(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height) //self.view.bounds
    //        contactVC.mainview.clipsToBounds = true
    //        self.addChildViewController(contactVC)
    //        contactVC.didMove(toParentViewController: self)
    //
    //
    //        self.contactVC.view.frame.origin.x = self.view.frame.size.width + 30
    //
    //        self.view.addSubview(self.contactVC.view)
    //        self.view.bringSubview(toFront: self.contactVC.view)
    //
    //
    //        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: { _ in
    //            self.contactVC.view.frame.origin.x = self.view.bounds.origin.x
    //        }, completion: nil)
    //
    //    }
    //    func updateContactsCount(){
    ////         self.countLbl.text = "\(self.feContacts!.count)"
    //    }

    //    func filterContentForSearchText(_ searchText: String) {
    //
    //        filteredArray = self.feContacts!.filter({
    //            ($0.name?.lowercased().contains(searchText.lowercased()))! || ($0.phoneNumber?.contains(searchText))!
    //        })
    //        tableView.reloadData()
    //    }

    /* Created by Keerthi removed by vaishali  */
    // MARK: Delete Data Records

    //    func deleteAllRecords() -> Void {
    //        let moc = CodeDataManager.shared.getContext()
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
    //
    //        let result = try? moc.fetch(fetchRequest)
    //        let resultData = result as! [Contact]
    //
    //        for object in resultData {
    //            moc.delete(object)
    //        }
    //        do {
    //            try moc.save()

    //        } catch let error as NSError  {

    //        } catch {
    //
    //        }
    //    }
    /* END */

    /* Created by Keerthi removed by vaishali  */
    //    func deleteRecords(withContactId : String) {
    //        let moc =  CodeDataManager.shared.getContext()
    //        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
    //
    //        fetchRequest.predicate = NSPredicate.init(format: "contactId == %@", "\(withContactId)")
    //
    //        let result = try? moc.fetch(fetchRequest)
    //        let resultData = result as! [Contact]
    //
    //        for object in resultData {
    //            moc.delete(object)
    //        }
    //
    //        do {
    //            try moc.save()

    //        } catch let error as NSError  {
  
    //        } catch {
    //
    //        }
    //    }
    /* END */

    /* Created by Keerthi removed by vaishali  */
    //    func deleteContact(withContactId:String?,path:IndexPath){
    //        if let contactId = withContactId{
    //            WebServiceManager.shared().deleteContact(withContId:contactId){
    //                (result, error, success) in
    //                CommonMethods.hideProgressView()
    //                if success{
    //                    DispatchQueue.main.async {
    //
    //                        kSweetAlert.showAlert("Success", subTitle: "Contact Deleted Successfully", style: .success)
    //
    //                        self.deleteRecords(withContactId: contactId)
    //                        self.feContacts!.remove(at: path.row)
    //                        // delete the table view row
    //                        self.tableView.deleteRows(at: [path], with: .fade)
    //                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loader"), object: nil)
    ////                        self.tableView.reloadData()
    //                    }
    //
    //                }
    //            }
    //        }
    //    }

    /*
     @IBAction func mergeContacts(_ sender: Any) {
     if tableView.isEditing ==  false{
     
     tableView.isEditing = true
     
     }
     else{
     tableView.isEditing = false
     }
     
     }*/
    /* END */

    // Add this at the end of your .m file. It returns a customized snapshot of a given view.

    // facetime trial
    //    let phoneNumber = "+918976592389"
    //    if let facetimeURL:NSURL = NSURL(string: "facetime://\(phoneNumber)") {
    //      let application:UIApplication = UIApplication.shared
    //      if (application.canOpenURL(facetimeURL as URL)) {
    //            application.openURL(facetimeURL as URL);
    //      }
    //    }
