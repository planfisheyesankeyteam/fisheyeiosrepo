//
//  AddContactEmailCell.swift
//  fisheye
//
//  Created by SankeyProUser on 07/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class AddContactEmailCell: UITableViewCell {

      @IBOutlet weak var emailBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlackUnderline: UIImageView!
      @IBOutlet weak var emailAdd: UICustomTextField!

      override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
      }

      override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
      }

}
