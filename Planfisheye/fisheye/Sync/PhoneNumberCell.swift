//
//  PhoneNumberCell.swift
//  fisheye
//
//  Created by Keerthi on 13/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

@objc protocol PhoneNumberCellDelegate {
    func updateText(text: String, AtIndex: Int)
}

class PhoneNumberCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var addPhoneCellBtn: UIButton!
    @IBOutlet weak var countryCodeTextField: UICustomUnderLineTextField!
    @IBOutlet weak var phoneNumTextField: UICustomUnderLineTextField!
    var secPhoneCell: Bool = false
    weak var delegate: PhoneNumberCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // countryCodeTextField.delegate = self
        phoneNumTextField.delegate = self

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: Textfield Delegates

  /*  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == countryCodeTextField{
            
            let picker = MICountryPicker()
            
            // Optional: To pick from custom countries list
            // picker.customCountriesCode = ["EG", "US", "AF", "AQ", "AX"]
            
            // delegate
//            picker.delegate = self
            
            // Display calling codes
            picker.showCallingCodes = true
            
            // or closure
            
            let vc = self.tableView?.viewController()
            
            picker.didSelectCountryWithCallingCodeClosure = { name, code, dailCode in
                
                
                vc?.dismiss(animated: true, completion: nil)
                self.countryCodeTextField.text = "\(dailCode)"
//                self.code = code
                print(dailCode)
                self.phoneNumTextField.becomeFirstResponder()
            }
            
            vc?.present(picker, animated: true, completion: nil)
            
            return false;
        }
        return true
    }*/

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if self.phoneNumTextField.border !=  nil {

        switch textField {

        case phoneNumTextField:
            if secPhoneCell {
                self.phoneNumTextField.configOnFirstResponder(withPlaceHolder: "Sec Phone no.")
            } else {
            self.phoneNumTextField.configOnFirstResponder(withPlaceHolder: "Phone no.")
            }

        default:
            break
        }
    }
    }
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        
//        textField.resignFirstResponder()
//        let tag = textField.tag + 1
//        if let nextTextField = self.view.viewWithTag(tag){
//            if nextTextField.isKind(of: UICustomUnderLineTextField.self){
//                nextTextField.becomeFirstResponder()
//            }
//        }
//        
//        return true
//    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
        guard let method = delegate?.updateText(text: text, AtIndex: self.addPhoneCellBtn.tag) else {
            return
        }
        method
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumTextField {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10
        }
        return true
    }

}

extension UITableViewCell {
    var tableView: UITableView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UITableView.self) == false) {
            view = view!.superview
        }
        return view as? UITableView
    }
}
