//
//  ContactGenricCell.swift
//  fisheye
//
//  Created by Keerthi on 14/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

@objc protocol ContactGenricCellDelegate {
    func updateText(text: String, withType: Int)
}

class ContactGenricCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var custTextField: UICustomUnderLineTextField!

    weak var delegate: ContactGenricCellDelegate?

    enum CellType: Int {
        case nameField = 0, companyField
    }

    var cellType: CellType = .nameField

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         custTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: Textfield Delegates

    func textFieldDidBeginEditing(_ textField: UITextField) {

        switch cellType {
        case .nameField:
            self.custTextField.configOnFirstResponder(withPlaceHolder: "Name")
        default:
            self.custTextField.configOnFirstResponder(withPlaceHolder: "Company")
        }

    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            guard let method = delegate?.updateText(text: text, withType: cellType.rawValue) else {
                return
            }
            method
        }
    }

}
