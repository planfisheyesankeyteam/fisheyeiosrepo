//
//  addContactProfesionalInfoVC.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class addContactProfesionalInfoVC: UIViewController {

      @IBOutlet weak var personalInfoBtn: UIButton!
      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var emailTextField: UICustomTextField!
      @IBOutlet weak var titleTextField: UICustomTextField!
      @IBOutlet weak var companyNameTextField: UICustomTextField!
      @IBOutlet weak var titleBlueUnderline: UIImageView!
      @IBOutlet weak var titleBlackUnderline: UIImageView!
      @IBOutlet weak var companyNameBlueUnderline: UIImageView!
      @IBOutlet weak var companyNameBlackUnderline: UIImageView!
      @IBOutlet weak var emailBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlackUnderline: UIImageView!

      @IBOutlet weak var generalLbl: UILabel!
      @IBOutlet weak var professionalLbl: UILabel!
      @IBOutlet weak var postalLbl: UILabel!
      @IBOutlet weak var personalLbl: UILabel!
      @IBOutlet weak var addContactHeaderLbl: UILabel!

      let validationController = ValidationController()
      let appSharedPrefernce = AppSharedPreference()
      var idtoken = ""
      var contactId: Int = 0
      var placeHolderOne = ""
      var placeHolderTwo = ""
      var addedContact = ContactObj()
      var syncText = SyncToastMessages.shared
      var appService = AppService.shared

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            self.syncText = SyncToastMessages.shared
            self.applyViewShadow()
            self.validation()
            self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            self.addedContact = DatabaseManagement.shared.getContact(contactId: self.contactId)
            if(self.contactId != 0) {
                  self.assignDetails()
            }
            self.addObservers()
            self.setLocalizationText()
      }

      override func viewWillAppear(_ animated: Bool) {
            self.hideBlueUnderlines()
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.addContactProfesionalInfoVC)
      }
      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      func hideBlueUnderlines() {
            self.companyNameBlueUnderline.isHidden = true
            self.titleBlueUnderline.isHidden = true
            self.emailBlueUnderline.isHidden = true
            self.companyNameBlackUnderline.isHidden = false
            self.titleBlackUnderline.isHidden = false
            self.emailBlackUnderline.isHidden = false
      }

      func validation() {

            self.companyNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.companyNameTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.titleTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.titleTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.emailTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  companyNameTextField:
                  self.companyNameBlackUnderline.isHidden = true
                  self.companyNameBlueUnderline.isHidden = false

                  if (companyNameTextField.text?.characters.count)! <= 0 {
                        self.companyNameTextField.configOnError(withPlaceHolder: self.syncText.enterCompany)
                  } else {
                        self.companyNameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.companyName)
                  }

            case titleTextField:
                  self.titleBlackUnderline.isHidden = true
                  self.titleBlueUnderline.isHidden = false

                  if (titleTextField.text?.characters.count)! <= 0 {
                        self.titleTextField.configOnError(withPlaceHolder: self.syncText.enterTitle)
                  } else {
                        self.titleTextField.configOnErrorCleared(withPlaceHolder: self.syncText.title)
                  }

            case emailTextField:
                  self.emailBlackUnderline.isHidden = true
                  self.emailBlueUnderline.isHidden = false

                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.syncText.enterEmail)
                  } else {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  }
            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case  companyNameTextField:
                  self.companyNameBlackUnderline.isHidden = false
                  self.companyNameBlueUnderline.isHidden = true

                  if (companyNameTextField.text?.characters.count)! <= 0 {
                        self.companyNameTextField.configOnError(withPlaceHolder: self.syncText.enterCompany)

                  } else {
                        self.companyNameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.companyName)
                  }

            case titleTextField:
                  self.titleBlackUnderline.isHidden = false
                  self.titleBlueUnderline.isHidden = true

                  if (titleTextField.text?.characters.count)! <= 0 {
                        self.titleTextField.configOnError(withPlaceHolder: self.syncText.enterTitle)
                  } else {
                        self.titleTextField.configOnErrorCleared(withPlaceHolder: self.syncText.title)
                  }

            case emailTextField:
                  self.emailBlackUnderline.isHidden = false
                  self.emailBlueUnderline.isHidden = true

                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.syncText.enterEmail)
                  } else {
                        let emailText: String = emailTextField.text!
                        if validationController.isValidEmail(testStr: emailText) {
                              self.emailTextField.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                        } else {
                              self.emailTextField.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                        }
                  }
            default:
                  break
            }
      }

      /* Created by venkatesh modified by vaishali  */
      func applyViewShadow() {
            self.mainView.layer.cornerRadius = 8
            // border
            self.mainView.layer.borderWidth = 0
            self.mainView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.mainView.layer.shadowColor = UIColor.black.cgColor
            self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainView.layer.shadowOpacity = 0.7
            self.mainView.layer.shadowRadius = 10.0
      }
      /* END */

      func checkValidation(profEmail: String) {

            if (profEmail.characters.count) > 0 {
                let _: String = emailTextField.text!
                  if validationController.isValidEmail(testStr: profEmail) {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                        placeHolderOne = emailTextField.placeholder!
                  } else {
                        self.emailTextField.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                        placeHolderTwo = emailTextField.placeholder!
                        return
                  }
            }
            self.addProfessionalInfo()
      }

      @IBAction func backBtnTapped(_ sender: Any) {
            loadSyncViewController()
      }

      @IBAction func nextIconTapped(_ sender: Any) {
            self.checkValidation(profEmail: emailTextField.text!)
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      @IBAction func personalBtnTapped(_ sender: Any) {
            loadPersonalInfoPageWithContactId()
      }

      @IBAction func postalBtnTapped(_ sender: Any) {
            loadPostalInfoPage()
      }

      @IBAction func generalBtnTapped(_ sender: Any) {
            loadGeneralInfoPage()
      }

      func addProfessionalInfo() {

            if(self.contactId == 0) {
                  self.view.makeToast(self.syncText.addContactPersonalInfo)
                _ = 1.0
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                        self.view.hideToast()
                  }
            } else {
                  let companyName = companyNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let title = titleTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let email = emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

                  var contactObj = ContactObj()
                  contactObj.contactId = Int(self.contactId)
                  contactObj.companyEmail = email
                  contactObj.company = companyName
                  contactObj.title = title
                  contactObj.isBackendSync = false

                  let isOperationSuccessfull = DatabaseManagement.shared.updateProfessionalDetails(contactObj: contactObj)
                  if(isOperationSuccessfull) {
                        ContactsSync.shared.getNonSyncedContacts()
                        ContactsSync.shared.backendSync()
                        self.appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactGeneralInfoPage")
                        NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add contact", action: "Submit contact's professional information", label: "Add contact's professional information successful", value: 0)
                  } else {
                    self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add contact", action: "Submit contact's professional information", label: "Failed to add contact's professional information", value: 0)
                  }
            }
      }

      func assignDetails() {
            self.companyNameTextField.text = self.addedContact.company
            self.titleTextField.text = self.addedContact.title
            self.emailTextField.text = self.addedContact.companyEmail
      }

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.addContactHeaderLbl.text = self.syncText.addContact
                  self.personalLbl.text = self.syncText.personal
                  self.postalLbl.text = self.syncText.postal
                  self.professionalLbl.text = self.syncText.professional
                  self.generalLbl.text = self.syncText.general
                  self.companyNameTextField.placeholder = self.syncText.companyName
                  self.titleTextField.placeholder = self.syncText.title
                  self.emailTextField.placeholder = self.syncText.email
            }
      }
      /* END */
}
