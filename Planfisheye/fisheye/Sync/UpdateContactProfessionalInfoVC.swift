//
//  UpdateContactProfessionalInfoVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 17/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class UpdateContactProfessionalInfoVC: UIViewController {

      let validationController = ValidationController()

      @IBOutlet weak var personalInfoBtn: UIButton!
      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var emailTextField: UICustomTextField!
      @IBOutlet weak var titleTextField: UICustomTextField!
      @IBOutlet weak var companyNameTextField: UICustomTextField!
      @IBOutlet weak var titleBlueUnderline: UIImageView!
      @IBOutlet weak var titleBlackUnderline: UIImageView!
      @IBOutlet weak var companyNameBlueUnderline: UIImageView!
      @IBOutlet weak var companyNameBlackUnderline: UIImageView!
      @IBOutlet weak var emailBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlackUnderline: UIImageView!
      @IBOutlet weak var editIcon: UIImageView!
      @IBOutlet weak var finalSubmitImage: UIImageView!
      @IBOutlet weak var syncContactsLbl: UILabel!
      @IBOutlet var firstTabImage: UIImageView!
      @IBOutlet var secondTabImage: UIImageView!
      @IBOutlet var thirdTabImage: UIImageView!
      @IBOutlet var fourthTabImage: UIImageView!
      @IBOutlet var tab1: UIView!
      @IBOutlet var tab2: UIView!
      @IBOutlet var tab3: UIView!
      @IBOutlet var tab4: UIView!
      @IBOutlet var stackViewBtn1: UIButton!
      @IBOutlet var stackViewBtn2: UIButton!
      @IBOutlet var stackViewBtn3: UIButton!
      @IBOutlet var stackViewBtn4: UIButton!

      @IBOutlet var separatorView1: UIView!
      @IBOutlet var separatorView2: UIView!
      @IBOutlet var separatorView3: UIView!
      @IBOutlet weak var fourthTabLbl: UILabel!
      @IBOutlet weak var thirdTabLbl: UILabel!
      @IBOutlet weak var secondTabLbl: UILabel!
      @IBOutlet weak var firstTabLbl: UILabel!

      let appSharedPrefernce = AppSharedPreference()
      var idtoken = ""
      var contactId: Int = 0
      var contact = ContactObj()
      var contactObjToOpen: Int = 0
      var contactDetails =  ContactObj()
      var syncText = SyncToastMessages.shared
      var isEditMode: Bool = false
      var secondBtnMappedTo: String = ""
      var thirdBtnMappedTo: String = ""
      var appService = AppService.shared

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.applyViewShadow()
            self.validation()
            if self.isEditMode {
                  self.userInteractionOn()
                  self.finalSubmitImage.isHidden = false
                  self.editIcon.isHidden = true
            } else {
                  self.userInteractionOff()
                  self.finalSubmitImage.isHidden = true
                  self.editIcon.isHidden = false
            }
            self.contactObjToOpen = self.appSharedPrefernce.getAppSharedPreferences(key: "contactObjToOpen") as? Int ?? 0
            self.contactDetails = DatabaseManagement.shared.getContact(contactId: contactObjToOpen) as ContactObj
            self.assignDetails()
            self.addObservers()
            self.setLocalizationText()
            self.showProfilePicture()
            if self.isEditMode {
                  self.showAllTabs()
            } else {
                  self.showFilledTabs()
            }
      }

      func showProfilePicture() {
            let picture = self.contactDetails.picture ?? ""
            firstTabImage?.sd_setImage(with: URL(string: picture ), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            firstTabImage.layer.cornerRadius = firstTabImage.frame.size.width / 2
            firstTabImage.clipsToBounds = true
      }

      func showAllTabs() {
            self.tab1.isHidden = false
            self.tab2.isHidden = false
            self.tab3.isHidden = false
            self.tab4.isHidden = false
            self.separatorView1.isHidden = false
            self.separatorView2.isHidden = false
            self.separatorView3.isHidden = false
            self.firstTabLbl.isHidden = false
            self.secondTabLbl.isHidden = false
            self.thirdTabLbl.isHidden = false
            self.fourthTabLbl.isHidden = false
            self.stackViewBtn1.isHidden = false
            self.stackViewBtn2.isHidden = false
            self.stackViewBtn3.isHidden = false
            self.stackViewBtn4.isHidden = false
            self.secondBtnMappedTo = "postal"
            self.thirdBtnMappedTo = "professional"
            self.secondTabImage.image = UIImage(named: "postalInfo")
            self.thirdTabImage.image = UIImage(named: "activeProfessionalInfo")
            self.fourthTabImage.image = UIImage(named: "generalInfo")
            self.secondTabLbl.text = self.syncText.postal
            self.thirdTabLbl.text = self.syncText.professional
            self.fourthTabLbl.text = self.syncText.general
      }

      func showFilledTabs() {
            if self.contactDetails.addressStringArray1.count > 0 {

                  self.secondTabImage.image = UIImage(named: "postalInfo")
                  self.separatorView1.isHidden = false
                  self.secondTabLbl.text = self.syncText.postal
                  self.secondBtnMappedTo = "postal"

                  if (self.contactDetails.company != "" || self.contactDetails.title != "" || self.contactDetails.companyEmail != "") {
                        self.thirdTabImage.image = UIImage(named: "activeProfessionalInfo")
                        self.separatorView2.isHidden = false
                        self.thirdTabLbl.text = self.syncText.professional
                        self.thirdBtnMappedTo = "professional"

                        if self.contactDetails.website != "" || self.contactDetails.notes != ""{
                              self.fourthTabImage.image = UIImage(named: "generalInfo")
                              self.separatorView3.isHidden = false
                              self.fourthTabLbl.text = self.syncText.general
                        } else {
                              self.tab4.isHidden = true
                              self.separatorView3.isHidden = true
                              self.fourthTabLbl.isHidden = true
                              self.stackViewBtn4.isHidden = true
                        }

                  } else if (self.contactDetails.website != "" || self.contactDetails.notes != "") {
                        self.thirdTabImage.image = UIImage(named: "generalInfo")
                        self.separatorView2.isHidden = false
                        self.thirdTabLbl.text = self.syncText.general
                        self.tab4.isHidden = true
                        self.separatorView3.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn4.isHidden = true
                        self.thirdBtnMappedTo = "general"

                  } else {
                        self.tab3.isHidden = true
                        self.tab4.isHidden = true
                        self.separatorView2.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn3.isHidden = true
                        self.stackViewBtn4.isHidden = true

                  }
            } else if (self.contactDetails.company != "" || self.contactDetails.title != "" || self.contactDetails.companyEmail != "" ) {

                  self.secondTabImage.image = UIImage(named: "activeProfessionalInfo")
                  self.separatorView1.isHidden = false
                  self.secondTabLbl.text = self.syncText.professional
                  self.secondBtnMappedTo = "professional"

                  if self.contactDetails.website != "" || self.contactDetails.notes != ""{
                        self.thirdTabImage.image = UIImage(named: "generalInfo")
                        self.separatorView2.isHidden = false
                        self.tab4.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.text = self.syncText.general
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn4.isHidden = true
                        self.thirdBtnMappedTo = "general"

                  } else {
                        self.tab3.isHidden = true
                        self.tab4.isHidden = true
                        self.separatorView2.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn3.isHidden = true
                        self.stackViewBtn4.isHidden = true

                  }
            } else if self.contactDetails.website != "" || self.contactDetails.notes != ""{
                  self.secondTabImage.image = UIImage(named: "generalInfo")
                  self.separatorView1.isHidden = false
                  self.tab3.isHidden = true
                  self.tab4.isHidden = true
                  self.separatorView2.isHidden = true
                  self.separatorView3.isHidden = true
                  self.secondTabLbl.text = self.syncText.general
                  self.thirdTabLbl.isHidden = true
                  self.fourthTabLbl.isHidden = true
                  self.stackViewBtn3.isHidden = true
                  self.stackViewBtn4.isHidden = true

            } else {
                  self.tab2.isHidden = true
                  self.tab3.isHidden = true
                  self.tab4.isHidden = true
                  self.separatorView1.isHidden = true
                  self.separatorView2.isHidden = true
                  self.separatorView3.isHidden = true
                  self.secondTabLbl.isHidden = true
                  self.thirdTabLbl.isHidden = true
                  self.fourthTabLbl.isHidden = true
                  self.stackViewBtn2.isHidden = true
                  self.stackViewBtn3.isHidden = true
                  self.stackViewBtn4.isHidden = true

            }
      }

      func userInteractionOff() {
            self.emailTextField.isUserInteractionEnabled = false
            self.companyNameTextField.isUserInteractionEnabled = false
            self.titleTextField.isUserInteractionEnabled = false
      }

      func userInteractionOn() {
            self.emailTextField.isUserInteractionEnabled = true
            self.companyNameTextField.isUserInteractionEnabled = true
            self.titleTextField.isUserInteractionEnabled = true
      }

      func assignDetails() {
            self.companyNameTextField.text = self.contactDetails.company
            self.titleTextField.text = self.contactDetails.title
            self.emailTextField.text = self.contactDetails.companyEmail
      }

      override func viewWillAppear(_ animated: Bool) {
            self.hideBlueUnderlines()
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.UpdateContactProfessionalInfoVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      func hideBlueUnderlines() {

            self.companyNameBlueUnderline.isHidden = true
            self.titleBlueUnderline.isHidden = true
            self.emailBlueUnderline.isHidden = true
            self.companyNameBlackUnderline.isHidden = false
            self.titleBlackUnderline.isHidden = false
            self.emailBlackUnderline.isHidden = false

      }

      func validation() {

            self.companyNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.companyNameTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.titleTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.titleTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.emailTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  companyNameTextField:
                  self.companyNameBlackUnderline.isHidden = true
                  self.companyNameBlueUnderline.isHidden = false

                  if (companyNameTextField.text?.characters.count)! <= 0 {
                        self.companyNameTextField.configOnError(withPlaceHolder: self.syncText.enterCompany)

                  } else {
                        self.companyNameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.companyName)
                  }

            case titleTextField:
                  self.titleBlackUnderline.isHidden = true
                  self.titleBlueUnderline.isHidden = false

                  if (titleTextField.text?.characters.count)! <= 0 {
                        self.titleTextField.configOnError(withPlaceHolder: self.syncText.enterTitle)

                  } else {
                        self.titleTextField.configOnErrorCleared(withPlaceHolder: self.syncText.title)
                  }

            case emailTextField:
                  self.emailBlackUnderline.isHidden = true
                  self.emailBlueUnderline.isHidden = false

                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.syncText.enterEmail)

                  } else {
                    let _: String = emailTextField.text!
                        if (emailTextField.text?.characters.count)! <= 0 {
                              self.emailTextField.configOnError(withPlaceHolder: self.syncText.enterEmail)
                        } else {
                              self.emailTextField.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                        }
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case  companyNameTextField:
                  self.companyNameBlackUnderline.isHidden = false
                  self.companyNameBlueUnderline.isHidden = true

                  if (companyNameTextField.text?.characters.count)! <= 0 {
                        self.companyNameTextField.configOnError(withPlaceHolder: self.syncText.enterCompany)

                  } else {
                        self.companyNameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.companyName)
                  }

            case titleTextField:
                  self.titleBlackUnderline.isHidden = false
                  self.titleBlueUnderline.isHidden = true

                  if (titleTextField.text?.characters.count)! <= 0 {
                        self.titleTextField.configOnError(withPlaceHolder: self.syncText.enterTitle)

                  } else {
                        self.titleTextField.configOnErrorCleared(withPlaceHolder: self.syncText.title)
                  }

            case emailTextField:
                  self.emailBlackUnderline.isHidden = false
                  self.emailBlueUnderline.isHidden = true

                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.syncText.enterEmail)

                  } else {
                        let emailText: String = emailTextField.text!
                        if validationController.isValidEmail(testStr: emailText) {
                              self.emailTextField.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                        } else {
                              self.emailTextField.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                        }
                  }

            default:
                  break
            }
      }

      /* Created by venkatesh modified by vaishali  */
      func applyViewShadow() {
            self.mainView.layer.cornerRadius = 8
            // border
            self.mainView.layer.borderWidth = 0
            self.mainView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.mainView.layer.shadowColor = UIColor.black.cgColor
            self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainView.layer.shadowOpacity = 0.7
            self.mainView.layer.shadowRadius = 10.0
      }
      /* END */

      func checkValidation() {

            if (emailTextField.text?.characters.count)! > 0 {
                  let emailText: String = emailTextField.text!
                  if validationController.isValidEmail(testStr: emailText) {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  } else {
                        self.emailTextField.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                        return
                  }
            }
            self.addProfessionalInfo()
      }

      @IBAction func backBtnTapped(_ sender: Any) {
            loadSyncViewController()
      }

      @IBAction func nextIconTapped(_ sender: Any) {

            if(self.editIcon.isHidden == true) {
                  self.checkValidation()
            } else {
                  self.showAllTabs()
                  self.finalSubmitImage.isHidden = false
                  self.editIcon.isHidden = true
                  userInteractionOn()
            }
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      @IBAction func personalBtnTapped(_ sender: Any) {
            loadDetailsPersonalInfo(isEditMode: self.editIcon.isHidden)
      }

      @IBAction func postalBtnTapped(_ sender: Any) {
            loadDetailsPostalInfo(isEditMode: self.editIcon.isHidden)
      }

      @IBAction func generalBtnTapped(_ sender: Any) {
            loadDetailsGeneralInfo(isEditMode: self.editIcon.isHidden)
      }

      func openOtherPage(nameOfpage: String) {
            switch nameOfpage {
            case "postal":
                  loadDetailsPostalInfo(isEditMode: self.editIcon.isHidden)
                  break
            case "professional":
                  loadDetailsProfessionalInfo(isEditMode: self.editIcon.isHidden)
                  break
            default:
                  break
            }
      }
      func addProfessionalInfo() {

            let companyName = companyNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let title = titleTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            var contactObj = ContactObj()
            contactObj.contactId = Int(self.contactObjToOpen)
            contactObj.companyEmail = email
            contactObj.company = companyName
            contactObj.title = title
            contactObj.isBackendSync = false
            contactObj.updatedAt = getCurrentTImeStamp()
            let isOperationSuccessfull = DatabaseManagement.shared.updateProfessionalDetails(contactObj: contactObj)
            if(isOperationSuccessfull) {
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
                  self.contactDetails = DatabaseManagement.shared.getContact(contactId: contactObjToOpen) as ContactObj
                  self.userInteractionOff()
                  self.editIcon.isHidden = false
                  self.finalSubmitImage.isHidden = true
                  if (self.contactDetails.company != "" || self.contactDetails.title != "" || self.contactDetails.companyEmail != "") {
                        self.showFilledTabs()
                        self.showProfilePicture()
                  } else if self.contactDetails.addressStringArray1.count > 0 {
                        self.appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "UpdateContactPostalInfoVC")
                        NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
                  } else {
                        loadDetailsPersonalInfo(isEditMode: self.editIcon.isHidden)
                  }

                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update contact", action: "Submit professional information button clicked", label: "Update professional information successful", value: 0)
            } else {
                self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update contact", action: "Submit professional information button clicked", label: "Failed to update contact's professional information", value: 0)
            }

      }

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.syncContactsLbl.text = self.syncText.syncContacts
                  self.firstTabLbl.text = self.syncText.personal
                  self.secondTabLbl.text = self.syncText.postal
                  self.thirdTabLbl.text = self.syncText.professional
                  self.fourthTabLbl.text = self.syncText.general
                  self.companyNameTextField.placeholder = self.syncText.companyName
                  self.titleTextField.placeholder = self.syncText.title
                  self.emailTextField.placeholder = self.syncText.email
            }
      }
      /* END */
}
