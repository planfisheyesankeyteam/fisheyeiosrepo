//
//  SyncBaseViewController.swift
//  fisheye
//
//  Created by Sankey Solutions on 28/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class SyncBaseViewController: UIViewController {

      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var dashboardIcon: UIButton!
      @IBOutlet weak var addContactBtn: UIButton!
      @IBOutlet weak var addContactBtnImg: UIImageView!

    @IBOutlet weak var moto: UILabel!
    let appSharedPrefernce = AppSharedPreference()
      var isToOpenAddContact: Bool = false
      var PulseToast = PulseToastMsgHeadingSubheadingLabels.shared
      override func viewDidLoad() {
            super.viewDidLoad()
            ScreenLoader.shared.stopLoader()
            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
//            let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController
            self.moto.text = self.PulseToast.letsWorkTogetherLabel
//            vc.hideBottomviews()
            self.view.backgroundColor = UIColor.clear
            self.mainView.backgroundColor = UIColor.clear
            if isToOpenAddContact {
                  self.loadAddContactPage()
            } else {
                  self.loadSyncViewController()
            }

            /* Created By Vaishali
             description -  observer to load new page */
            NotificationCenter.default.addObserver(forName: LOADE_NEW_PAGE, object: nil, queue: nil) { (notification) in
                var isEditMode = false
                if let userInfo = notification.userInfo {
                    isEditMode = userInfo ["isEditMode"] as? Bool ?? false

                }
                self.openPage(isEditMode: isEditMode)
            }
            // Do any additional setup after loading the view.
      }

      @IBAction func navigateToDashboard(_ sender: Any) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "Dashboard")
        self.openPage(isEditMode: false)
      }

      @IBAction func navigateToAddContact(_ sender: Any) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "addContactInProgress", value: 0)
            loadPersonalInfoPage()
            self.appSharedPrefernce.setAppSharedPreferences(key: "contactObjToOpen", value: 0)
            loadPersonalInfoPage()
      }

      func showAddContactBtn(isAddContactBtnEnable: Bool) {
            self.addContactBtnImg.isHidden = !isAddContactBtnEnable
            self.addContactBtn.isUserInteractionEnabled = isAddContactBtnEnable
      }

      func  loadSyncViewController() {

            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: true)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }
    
      func loadAddContactPage() {

            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "addContactPersonalInfoVC") as! addContactPersonalInfoVC
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: false)
      }

      func loadAddContactPostalInfoPage() {
            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "addContactPostalInfoVC") as! addContactPostalInfoVC
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: false)
      }

      func loadAddContactProffesionalInfoPage() {
            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "addContactProfesionalInfoVC") as! addContactProfesionalInfoVC
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: false)
      }

      func loadAddContactGeneralInfoPage() {
            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "addContactGeneralInfoVC") as! addContactGeneralInfoVC
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: false)
      }

      func loadContactDetailsPersonalInfo(isEditMode: Bool) {

            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UpdateContactPersonalInfoVC") as! UpdateContactPersonalInfoVC
            vc.isEditMode = isEditMode
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = vc.view.frame.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: false)
      }

      func contactDetailsPostalInfo(isEditMode: Bool) {
        let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UpdateContactPostalInfoVC")
        self.appSharedPrefernce.setAppSharedPreferences(key: "isEditMode", value: isEditMode)
        vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
        self.mainView.addSubview(vc.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            vc.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
        self.showAddContactBtn(isAddContactBtnEnable: false)
      }

      func contactDetailsProfessionalInfo(isEditMode: Bool) {
            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UpdateContactProfessionalInfoVC") as! UpdateContactProfessionalInfoVC
            vc.isEditMode = isEditMode
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: false)
      }

      func contactDetailsGeneralInfo(isEditMode: Bool) {
            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "UpdateContactGeneralInfoVC") as! UpdateContactGeneralInfoVC
            vc.isEditMode = isEditMode
            vc.view.frame = self.mainView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            self.mainView.addSubview(vc.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
            self.showAddContactBtn(isAddContactBtnEnable: false)
      }

    func openPage(isEditMode: Bool) {
            let pageToLoad = self.appSharedPrefernce.getAppSharedPreferences(key: "syncPageToLoad") as? String ?? ""
            self.mainView.subviews.forEach({ (subview) in
                  subview.removeFromSuperview()
            })

            switch pageToLoad {
            case "addContactInSync":
                  self.loadAddContactPage()
                  break

            case "addContactPostalInfoPage":
                  self.loadAddContactPostalInfoPage()
                  break

            case "addContactProffesionalInfoPage":
                  self.loadAddContactProffesionalInfoPage()
                  break

            case "addContactGeneralInfoPage":
                  self.loadAddContactGeneralInfoPage()
                  break

            case "syncViewController":
                  self.loadSyncViewController()
                  break

            case "UpdateContactPersonalInfoVC":
                self.loadContactDetailsPersonalInfo(isEditMode: isEditMode)
                  break

            case "UpdateContactPostalInfoVC":
                  self.contactDetailsPostalInfo(isEditMode: isEditMode)
                  break

            case "UpdateContactProfessionalInfoVC":
                  self.contactDetailsProfessionalInfo(isEditMode: isEditMode)
                  break

            case "UpdateContactGeneralInfoVC":
                  self.contactDetailsGeneralInfo(isEditMode: isEditMode)
                  break

            case "Dashboard":
                  self.mainView.subviews.forEach({ (subview) in
                        subview.removeFromSuperview()
                  })
                  let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                  let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
                  self.present(nextVC, animated: true, completion: nil)

                  break

            default:
                  break
            }
      }

      /*
       // MARK: - Navigation
       
       // In a storyboard-based application, you will often want to do a little preparation before navigation
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       // Get the new view controller using segue.destinationViewController.
       // Pass the selected object to the new view controller.
       }
       */

}
