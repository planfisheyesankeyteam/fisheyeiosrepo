//
//  EmailCell.swift
//  fisheye
//
//  Created by Keerthi on 13/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
@objc protocol EmailCellDelegate {
    func updateEmailText(text: String, AtIndex: Int)
}

class EmailCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var emailTextField: UICustomUnderLineTextField!
     weak var delegate: EmailCellDelegate?

    @IBOutlet weak var addEmailCellBtn: UIButton!
    var secEmailCell: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        emailTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if self.emailTextField.border !=  nil {
        switch textField {

        case emailTextField:
            if secEmailCell {
                self.emailTextField.configOnFirstResponder(withPlaceHolder: "Sec Email")
            } else {
            self.emailTextField.configOnFirstResponder(withPlaceHolder: "Email")
            }

        default:
            break
        }
        }

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text {
            guard let method = delegate?.updateEmailText(text: text, AtIndex: self.addEmailCellBtn.tag) else {
                return
            }
            method
        }
    }

}
