//
//  ContactDetailTableViewCell.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 8/17/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class ContactDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.subTitleLbl.textColor = UIColor.black60Two87()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
