//
//  AddressLine1Cell.swift
//  fisheye
//
//  Created by Keerthi on 15/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class AddressLine1Cell: UITableViewCell {

    @IBOutlet weak var addressLine1: UICustomUnderlinedText!

    @IBOutlet weak var mapImageView: UIImageView!

    @IBOutlet weak var addressLine2: UICustomUnderLineTextField!
    @IBOutlet weak var searchMapLbl: UILabel!

    @IBOutlet weak var zipCode: UICustomUnderLineTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
