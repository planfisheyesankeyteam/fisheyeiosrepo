//
//  addContactPostalInfoVC.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class addContactPostalInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

      @IBOutlet weak var nextIconBtn: UIButton!
      @IBOutlet weak var rectangleAddAddressBtn: UIButton!
      @IBOutlet weak var noAddressView: UIView!
      @IBOutlet weak var addButton: UIButton!
      @IBOutlet weak var tableView: UITableView!
      @IBOutlet weak var addBtnImg: UIImageView!
      @IBOutlet weak var popUpView: UIView!
      @IBOutlet weak var popUpHeading: UILabel!
      @IBOutlet weak var popUpSubHeading: UILabel!
      @IBOutlet weak var popUpCentreConstraint: NSLayoutConstraint!

      //Labels
      @IBOutlet weak var popUpOkLbl: UILabel!
      @IBOutlet weak var popUpCancelLbl: UILabel!
      @IBOutlet weak var noAddressLbl: UILabel!
      @IBOutlet weak var generalLbl: UILabel!
      @IBOutlet weak var professionalLbl: UILabel!
      @IBOutlet weak var postalLbl: UILabel!
      @IBOutlet weak var personalLbl: UILabel!
      @IBOutlet weak var addContactHeaderLbl: UILabel!

      var addressIdArray: [String] = []
      var countryArray: [String] = []
      var localityArray: [String] = []
      var postalCodeArray: [String] = []
      var regionArray: [String] = []
      var streetAddressArray: [String] = []
      var  typeArray: [String] = []
      let appSharedPrefernce = AppSharedPreference()
      var addressList: [String] = []
      var addressCount = 0
      var idtoken = ""
      var contactId = ""
      var addContactPersonalInfoVC: addContactPersonalInfoVC!
      var addedContact = ContactObj()
      var addContactInProgress: Int = 0
      var addressToDelete: Int = -1
      var syncText = SyncToastMessages.shared
      var appService = AppService.shared

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if(self.addressList.count == 2) {
                  self.addButton.isHidden = true
                  self.addBtnImg.isHidden = true
                  self.rectangleAddAddressBtn.isUserInteractionEnabled = false
                  self.addButton.isUserInteractionEnabled = false
                  self.noAddressView.isHidden = true
            } else if(self.addressList.count == 1) {
                  self.addButton.isHidden = false
                  self.addBtnImg.isHidden = false
                  self.addButton.isUserInteractionEnabled = true
                  self.rectangleAddAddressBtn.isUserInteractionEnabled = false
                  self.noAddressView.isHidden = true
            } else {
                  self.noAddressView.isHidden = false
                  self.rectangleAddAddressBtn.isUserInteractionEnabled = true
                  self.addButton.isHidden = true
                  self.addBtnImg.isHidden = true
                  self.addButton.isUserInteractionEnabled = false
            }

            return self.addressList.count
      }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as! addressCell
            cell.addressTypeLbl.text = self.typeArray[indexPath.row]
            
            if(self.typeArray[indexPath.row] == self.syncText.home) {
                  cell.addressTypeImg.image = UIImage(named: "Home")
            } else {
                  cell.addressTypeImg.image = UIImage(named: "Work")
            }

            cell.addressView.text = self.addressList[indexPath.row]
            cell.addressView.isUserInteractionEnabled = false
            cell.updateAddressBtn.tag = indexPath.row
            cell.updateAddressBtn.addTarget(self, action: #selector(updateAddressBtnTapped), for: .touchUpInside)
            return cell
      }

      /* Description :- this method handles row deletion */
    private func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
            if editingStyle == .delete {

                  self.popUpHeading.text = self.syncText.deleteAddress
                  self.popUpSubHeading.text = self.syncText.areUSureToDeleteAddress + " "  + self.typeArray[indexPath.row] + " " +  self.syncText.address + "?"
                  self.popUpCentreConstraint.constant = self.syncText.popUpCenterConstraint
                  UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                        self.view.layoutIfNeeded()
                  })
                  self.addressToDelete = indexPath.row
                  self.appSharedPrefernce.setAppSharedPreferences(key: "addressToDelete", value: indexPath.row)
            } else if editingStyle == .insert {
                  // Not used in our example, but if you were adding a new row, this is where you would do it.
            }
      }

      func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
            return self.syncText.delete
      }

      @IBAction func popUpCancelBtnTapped(_ sender: Any) {
            self.dismissPopUpView()
      }

      func dismissPopUpView() {
            self.popUpCentreConstraint.constant = self.syncText.popUpLeftConstraint
            UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                  self.view.layoutIfNeeded()
            })
      }

      @IBAction func popUpOKBtnTapped(_ sender: Any) {
            let isOperationSuccessfull = DatabaseManagement.shared.deleteFirstAddress(contactObj: self.addedContact)
            if(isOperationSuccessfull) {
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
                  self.dismissPopUpView()
                  self.assignContactDetails()
            } else {
                self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
            }
      }

    @objc func updateAddressBtnTapped(_ sender: UIButton) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Sync", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "AdrressAddEditPopUpVC") as! AdrressAddEditPopUpVC

            nextVC.editMode = true
            nextVC.noOfAddressToEdit = sender.tag
            self.present(nextVC, animated: true, completion: nil)
      }

      func showAddressEditingPopUpView() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let addressEditingPopUpVC = storyBoard.instantiateViewController(withIdentifier: "ProfileAddressEditingPopUpVC")  as! ProfileAddressEditingPopUpVC

            addressEditingPopUpVC.editMode = false
            self.present(addressEditingPopUpVC, animated: true, completion: nil)

      }

      @IBAction func backBtnTapped(_ sender: Any) {
            //loadPersonalInfoPage()
            loadSyncViewController()

      }


      @IBOutlet weak var mainView: UIView!

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            self.syncText = SyncToastMessages.shared
            self.applyViewShadow()
            self.applyBtnShadow()
            self.applyPopUpShadow()
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.addContactInProgress  = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            if(self.addContactInProgress != 0) {
                  self.assignContactDetails()
            }

            self.nextIconBtn.isUserInteractionEnabled = false
            self.addObservers()
            self.setLocalizationText()
      }

      func loadList() {
            //load data here
            self.tableView.reloadData()
      }

      func registerAddressTableViewCell() {
            self.tableView.register(addressCell.self, forCellReuseIdentifier: "addressCell")
      }

      override func viewWillAppear(_ animated: Bool) {
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.addContactPostalInfoVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      func showAddressAddEditingPopUpView() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Sync", bundle: nil)
            let AdrressAddEditPopUpVC = storyBoard.instantiateViewController(withIdentifier: "AdrressAddEditPopUpVC")  as! AdrressAddEditPopUpVC

            AdrressAddEditPopUpVC.editMode = false
            self.present(AdrressAddEditPopUpVC, animated: true, completion: nil)
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      /* Created by venkatesh modified by vaishali  */
      func applyViewShadow() {
            self.mainView.layer.cornerRadius = 8
            // border
            self.mainView.layer.borderWidth = 0
            self.mainView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.mainView.layer.shadowColor = UIColor.black.cgColor
            self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainView.layer.shadowOpacity = 0.7
            self.mainView.layer.shadowRadius = 10.0
      }
      /* END */

      /* Created by venkatesh modified by vaishali  */
      func applyBtnShadow() {
            self.rectangleAddAddressBtn.layer.cornerRadius = 6
            // border
            self.rectangleAddAddressBtn.layer.borderWidth = 0
            self.rectangleAddAddressBtn.layer.borderColor = UIColor.black.cgColor
      }
      /* END */

      /* Created by venkatesh modified by vaishali  */
      func applyPopUpShadow() {
            self.popUpView.layer.cornerRadius = 8

            // border
            self.popUpView.layer.borderWidth = 0
            self.popUpView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.popUpView.layer.shadowColor = UIColor.black.cgColor
            self.popUpView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.popUpView.layer.shadowOpacity = 0.7
            self.popUpView.layer.shadowRadius = 10.0
      }
      /* END */

      @IBAction func addBtnTapped(_ sender: Any) {
            self.navigateToAddressPopUp()
      }

      @IBAction func rectangleAddressbtnTapped(_ sender: Any) {
            self.navigateToAddressPopUp()
      }
      func navigateToAddressPopUp() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Sync", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "AdrressAddEditPopUpVC") as! AdrressAddEditPopUpVC
            self.present(nextVC, animated: true, completion: nil)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      @IBAction func nextBtnTapped(_ sender: Any) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactProffesionalInfoPage")
            NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)

      }

      @IBAction func personalBtnTapped(_ sender: Any) {
            loadPersonalInfoPageWithContactId()
      }

      @IBAction func professionalBtnTapped(_ sender: Any) {
            loadProfessionalInfoPage()
      }

      @IBAction func generalBtnTapped(_ sender: Any) {
            loadGeneralInfoPage()
      }

      func navigateToPersonal() {
        self.didMove(toParent: nil)
            self.view.removeFromSuperview()
        self.removeFromParent()
            self.dismiss(animated: true, completion: nil)
            let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
            addContactPersonalInfoVC = storyboard.instantiateViewController(withIdentifier: "addContactPersonalInfoVC") as! addContactPersonalInfoVC
            addContactPersonalInfoVC.view.frame = CGRect.init(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height) //self.view.bounds
            addContactPersonalInfoVC.mainView.clipsToBounds = true

        self.addChild(addContactPersonalInfoVC)
        addContactPersonalInfoVC.didMove(toParent: self)

            self.addContactPersonalInfoVC.view.frame.origin.y = -self.view.frame.size.height

            self.view.addSubview(self.addContactPersonalInfoVC.view)
        self.view.bringSubviewToFront(self.addContactPersonalInfoVC.view)

            UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: {
                  self.addContactPersonalInfoVC.view.frame.origin.y = self.view.bounds.origin.x
            }, completion: nil)
      }

      func assignContactDetails() {
            self.addedContact = DatabaseManagement.shared.getContact(contactId: self.addContactInProgress)
            self.addressList = []
            self.typeArray = []
            if(self.addedContact.addressStringArray1.count > 0) {
                  self.typeArray.append(self.addedContact.addressStringArray1.first!)
                  self.addedContact.addressStringArray1.remove(at: 0)
                  let temp = self.addedContact.addressStringArray1.joined(separator: ",")
                  self.addressList.append(temp)
                
            }

            if(self.addedContact.addressStringArray2.count > 0) {
                  self.typeArray.append(self.addedContact.addressStringArray2.first!)
                  self.addedContact.addressStringArray2.remove(at: 0)
                  let temp = self.addedContact.addressStringArray2.joined(separator: ",")
                  self.addressList.append(temp)
            }

            self.tableView.reloadData()
      }

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)

            NotificationCenter.default.addObserver(forName: REFRESH_ADDRESSES_NOTIFICATION, object: nil, queue: nil) { (_) in
                  self.assignContactDetails()
            }
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.addContactHeaderLbl.text = self.syncText.addContact
                  self.personalLbl.text = self.syncText.personal
                  self.postalLbl.text = self.syncText.postal
                  self.professionalLbl.text = self.syncText.professional
                  self.generalLbl.text = self.syncText.general
                  self.popUpOkLbl.text = self.syncText.ok
                  self.popUpCancelLbl.text = self.syncText.cancel
                  self.noAddressLbl.text = self.syncText.emptyAddressViewtext
                  self.rectangleAddAddressBtn.setTitle(self.syncText.addAddress, for: .normal)
            }
      }
      /* END */

}
