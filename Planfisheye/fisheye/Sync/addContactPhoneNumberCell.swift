//
//  addContactPhoneNumberCell.swift
//  fisheye
//
//  Created by SankeyProUser on 07/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class addContactPhoneNumberCell: UITableViewCell {

      @IBOutlet weak var phoneNumber: UICustomTextField!
      @IBOutlet weak var phoneBlackUnderline: UIImageView!
      @IBOutlet weak var phoneBlueUnderline: UIImageView!
      @IBOutlet weak var countryCodeDropDown: UIButton!
      @IBOutlet weak var countryCode: UICustomTextField!
      override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
      }

      override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
      }

}
