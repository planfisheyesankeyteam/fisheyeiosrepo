//
//  addressCell.swift
//  fisheye
//
//  Created by SankeyIosMac on 11/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class addressCell: UITableViewCell {

    @IBOutlet weak var updateAddressBtn: UIButton!
    @IBOutlet weak var addressTypeLbl: UILabel!
    @IBOutlet weak var addressView: UITextView!
    @IBOutlet weak var addressTypeImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
