//
//  addContactGeneralInfoVC.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class addContactGeneralInfoVC: UIViewController {


      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var websiteTextField: UICustomTextField!
      @IBOutlet weak var notesTextField: UICustomTextField!

      @IBOutlet weak var notesBlueUnderline: UIImageView!
      @IBOutlet weak var notesBlackUnderline: UIImageView!
      @IBOutlet weak var websiteBlueUnderline: UIImageView!
      @IBOutlet weak var websiteblackUnderline: UIImageView!

      @IBOutlet weak var generalLbl: UILabel!
      @IBOutlet weak var professionalLbl: UILabel!
      @IBOutlet weak var postalLbl: UILabel!
      @IBOutlet weak var personalLbl: UILabel!
      @IBOutlet weak var addContactHeaderLbl: UILabel!
      @IBOutlet weak var submitBtn: UIButton!

      let appSharedPrefernce =  AppSharedPreference()
      var idtoken = ""
      var contactId: Int = 0
      var addedContact = ContactObj()
      let validationController = ValidationController()
      var syncText = SyncToastMessages.shared
      let appService = AppService.shared

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.applyViewShadow()
            self.validation()
            self.hideBlueUnderlines()
            self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            self.addedContact = DatabaseManagement.shared.getContact(contactId: self.contactId)
            if(self.contactId != 0) {
                  self.assignDetails()
            }
            self.addObservers()
            self.setLocalizationText()
      }

      override func viewWillAppear(_ animated: Bool) {
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.addContactGeneralInfoVC)
      }
      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      func hideBlueUnderlines() {

            self.websiteBlueUnderline.isHidden = true
            self.websiteblackUnderline.isHidden = false
            self.notesBlueUnderline.isHidden = true
            self.notesBlackUnderline.isHidden = false

      }

      func validation() {

            self.websiteTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.websiteTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.notesTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.notesTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  websiteTextField:

                  self.websiteBlueUnderline.isHidden = false
                  self.websiteblackUnderline.isHidden = true

                  if (websiteTextField.text?.characters.count)! <= 0 {
                        self.websiteTextField.configOnError(withPlaceHolder: self.syncText.enterWebsite)

                  } else {
                        self.websiteTextField.configOnErrorCleared(withPlaceHolder: self.syncText.website)
                  }

            case notesTextField:
                  self.notesBlueUnderline.isHidden = false
                  self.notesBlackUnderline.isHidden = true

                  if (notesTextField.text?.characters.count)! <= 0 {
                        self.notesTextField.configOnError(withPlaceHolder: self.syncText.enterNotes)

                  } else {
                        self.notesTextField.configOnErrorCleared(withPlaceHolder: self.syncText.notes)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case  websiteTextField:
                  self.websiteBlueUnderline.isHidden = true
                  self.websiteblackUnderline.isHidden = false

                  if (websiteTextField.text?.characters.count)! <= 0 {
                        self.websiteTextField.configOnError(withPlaceHolder: self.syncText.enterWebsite)

                  } else if (!validationController.isValidWebsite(url: self.websiteTextField.text!)) {
                        self.websiteTextField.configOnError(withPlaceHolder: self.syncText.invalidWebsite)
                  } else {
                        self.websiteTextField.configOnErrorCleared(withPlaceHolder: self.syncText.website)
                  }

            case notesTextField:
                  self.notesBlueUnderline.isHidden = true
                  self.notesBlackUnderline.isHidden = false

                  if (notesTextField.text?.characters.count)! <= 0 {
                        self.notesTextField.configOnError(withPlaceHolder: self.syncText.enterNotes)

                  } else {
                        self.notesTextField.configOnErrorCleared(withPlaceHolder: self.syncText.notes)
                  }

            default:
                  break
            }
      }

      @IBAction func backBtnTapped(_ sender: Any) {
            loadSyncViewController()
      }

      @IBAction func submitTapped(_ sender: Any) {
            self.addGeneralInfo()
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      @IBAction func personalBtnTapped(_ sender: Any) {
            loadPersonalInfoPageWithContactId()
      }

      @IBAction func postalBtnTapped(_ sender: Any) {
            loadPostalInfoPage()
      }

      @IBAction func professionalBtnTapped(_ sender: Any) {
            loadProfessionalInfoPage()
      }

      func addGeneralInfo() {
            if(self.contactId == 0) {
                  self.view.makeToast(self.syncText.addContactPersonalInfo)
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                        self.view.hideToast()
                  }
            } else {
                  let website = websiteTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let notes = notesTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

                  var contactObj = ContactObj()
                  contactObj.contactId = self.contactId
                  contactObj.website = website
                  contactObj.notes = notes
                  contactObj.isBackendSync = false
                  let isOperationSuccessfull = DatabaseManagement.shared.updateGeneralInfo(contactObj: contactObj)
                  if(isOperationSuccessfull) {
                        ContactsSync.shared.getNonSyncedContacts()
                        ContactsSync.shared.backendSync()
                        self.appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "syncViewController")
                        NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add contact", action: "Submit contact's general information", label: "Add contact's general information successful", value: 0)
                  } else {
                        self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add contact", action: "Submit contact's general information", label: "Failed to add contact's general information", value: 0)
                  }

            }
      }

      func assignDetails() {
            self.websiteTextField.text = self.addedContact.website
            self.notesTextField.text = self.addedContact.notes
      }

      /* Created by venkatesh modified by vaishali  */
      func applyViewShadow() {
            self.mainView.layer.cornerRadius = 8
            // border
            self.mainView.layer.borderWidth = 0
            self.mainView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.mainView.layer.shadowColor = UIColor.black.cgColor
            self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainView.layer.shadowOpacity = 0.7
            self.mainView.layer.shadowRadius = 10.0
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.addContactHeaderLbl.text = self.syncText.addContact
                  self.personalLbl.text = self.syncText.personal
                  self.postalLbl.text = self.syncText.postal
                  self.professionalLbl.text = self.syncText.professional
                  self.generalLbl.text = self.syncText.general
                  self.websiteTextField.placeholder = self.syncText.website
                  self.notesTextField.placeholder = self.syncText.notes
                  self.submitBtn.setTitle(self.syncText.submit, for: .normal)
            }
      }
      /* END */

}
