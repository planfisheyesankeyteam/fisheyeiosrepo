//
//  AddContactVC.swift
//  Planfisheye
//
//  Created by Keerthi on 09/09/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import MapKit

@objc protocol AddContactDelegate {
   @objc optional func didAddContact()
    @objc optional func didUpdateContact(contact: FEContact!)
}

class AddContactVC: UIViewController, PhoneNumberCellDelegate, EmailCellDelegate, ContactGenricCellDelegate {

    @IBOutlet weak var addContactLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addContactBtn: UIButton!
    @IBOutlet weak var mainView: UIView!

    weak var delegate: AddContactDelegate?

    @IBOutlet weak var bottomView: UIView!

    var selectedPlaceMark: MKPlacemark?

    var updateAddressDelegate: UpdateAddress?

    fileprivate var phoneNumArray: [String]!

    fileprivate var emailArray: [String]!

    fileprivate var addressArray: [FEAddress]!

    fileprivate var company: String!

    fileprivate var name: String!

    var contact: FEContact?

    var secArray: [String] = ["Name", "Phone No.", "Email", "Address", "Company Name"]

//    enum contactDetail:String {
//        case ["Name","Phone No.","Email","Address Line1(Street/Landmark)","Address Line2(City/Country)","Zip/Pin Code","Company Name","Title","Notes","Website"]
//    }

 //   var array = ["Name","Phone No.","Email","Address Line1(Street/Landmark)","Address Line2(City/Country)","Zip/Pin Code","Company Name"]//"Title","Notes","Website"

    override func viewDidLoad() {
        super.viewDidLoad()

        //closeBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)

        self.addContactBtn.setTitleColor(UIColor.white, for: .normal)

        self.closeBtn.isUserInteractionEnabled = true

        self.closeBtn.addTarget(self, action: #selector(backclick(_:)), for: .touchUpInside)

        self.view.bringSubview(toFront: self.mainView)

        self.view.bringSubview(toFront: self.bottomView)

        self.addContactBtn.backgroundColor = UIColor.frenchBlue()

        self.addContactBtn.layer.cornerRadius = 0.09467391304*self.addContactBtn.bounds.width

        self.addContactBtn.setTitleColor(UIColor.white, for: .normal)

        if let cont = self.contact {

            self.addContactLbl.text = "Update Contact"

            self.addContactBtn.setTitle("Update Contact", for: .normal)

            self.phoneNumArray = []

            self.emailArray = []

            self.phoneNumArray.insert(cont.phoneNumber ?? "", at: 0)

            if let secPhoneNum = cont.secondaryPhoneNumbers {

            self.phoneNumArray.append(contentsOf: secPhoneNum)

            }

            self.emailArray.insert(cont.email ?? "", at: 0)

            if let secEmails = cont.secondaryEmails {

                self.emailArray.append(contentsOf: secEmails)

            }
            self.addressArray = []

            self.addressArray.insert(cont.address ?? FEAddress(), at: 0)

            self.name = self.contact != nil ? self.contact!.name : ""

            self.company = self.contact != nil ? self.contact!.company : ""

           /* let phoneIndex = self.array.index(of: "Phone No.")
            
                for i in 0..<secPhoneNum.count{
                    self.array.insert("Sec Phone No.\(i)", at: (phoneIndex!+1)+i)
                }
            }
            
            let emailIndex = self.array.index(of: "Email")
            if let secEmails = self.contact!.secondaryEmails{
            for i in 0..<secEmails.count{
                self.array.insert("Sec Email\(i)", at: (emailIndex!+1)+i)
            }
            }
            
            //TODO : 
            phoneNumArray = [self.contact!.phoneNumber!]
            if let secPhoneNum = self.contact!.secondaryPhoneNumbers{
                phoneNumArray?.append(contentsOf: secPhoneNum)
            }*/

        } else {

            self.addContactLbl.text = "Add Contact"

            self.addContactBtn.setTitle("Add Contact", for: .normal)

            self.phoneNumArray = [""]

            self.emailArray = [""]

            self.addressArray = [FEAddress()]

            self.name = ""

            self.company = ""

        }

        self.addContactBtn.addTarget(self, action: #selector(addContact), for: .touchUpInside)

//        self.tableView.register(ContactGenricCell.self, forCellReuseIdentifier: "nameCell")

        self.tableView.dataSource = self

        self.tableView.delegate = self

        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

        vc.footerTitle.isHidden = true

        vc.footerSubTitle.isHidden = true

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        self.mainView.backgroundColor = UIColor.white
        applyViewShadow()

    }

    func updateText(text: String, AtIndex: Int) {

       /* if self.contact == nil{
            
            self.contact?.secondaryPhoneNumbers?[AtIndex] = text
        }
       */

       // self.tableView.reloadData()*/
        //else{
            if AtIndex < self.phoneNumArray.count {

                self.phoneNumArray[AtIndex] = text
            }
        //}

    }

    func updateText(text: String, withType: Int) {

        /* if self.contact == nil{
         
         self.contact?.secondaryPhoneNumbers?[AtIndex] = text
         }
         */

        // self.tableView.reloadData()*/
        //else{

        switch withType {
        case 0:
            self.name = text
        default:
            self.company = text
        }

    }

    func updateEmailText(text: String, AtIndex: Int) {

       /* if self.contact == nil{
           // self.contact = FEContact()
            self.contact?.secondaryEmails?[AtIndex] = text
        }
  
        
        //self.tableView.reloadData()
        else{
           
        }
        */

        if AtIndex < self.emailArray.count {
            self.emailArray[AtIndex] = text
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func createGradientLayer(inView view: UIView, colors: [CGColor]) {

        let gradientLayer = CAGradientLayer()

        // Hoizontal - commenting these two lines will make the gradient veritcal
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)

        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)

        let gradientLocations: [NSNumber] = [0.0, 1.0]

        gradientLayer.frame = view.bounds

        gradientLayer.colors = colors

        gradientLayer.locations = gradientLocations

        view.layer.addSublayer(gradientLayer)

    }

    func applyViewShadow() {

//        kApplicationDelegate.window?.bringSubview(toFront: self.view)

        self.view.layer.cornerRadius = 8

        // border
        self.view.layer.borderWidth = 0
        self.view.layer.borderColor = UIColor.black.cgColor

        self.mainView.layer.cornerRadius = 8

        // border
        self.mainView.layer.borderWidth = 0
        self.mainView.layer.borderColor = UIColor.black.cgColor

        self.mainView.layer.backgroundColor = UIColor.white.cgColor

        self.mainView.layer.shadowColor = UIColor.black.cgColor
        self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.mainView.layer.shadowOpacity = 0.7
        self.mainView.layer.shadowRadius = 4.0

        self.mainView.layer.masksToBounds = false

        self.bottomView.layer.masksToBounds = false
        self.bottomView.layer.cornerRadius = 8
        //self.bottomView.layer.forwardingTarget(for: #selector(addContact))

    //    self.view.layer.sublayers?.forEach { $0.removeFromSuperlayer() }

//        let tap = UITapGestureRecognizer.init(target: self, action: #selector(addContact))
//        
//       
//        
//        self.addContactLbl.isUserInteractionEnabled = true
//        
//        self.addContactLbl.addGestureRecognizer(tap)

        self.addContactBtn.isUserInteractionEnabled = true

    }

    func backclick(_ sender: Any) {

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

        vc.hideBottomviews()

    }

   /* override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self.bottomView) else { return }
        if !self.addContactBtn.frame.contains(location) {
            print("Tapped outside the view")
        }else {
            print("Tapped inside the view")
            addContact()
        }
    }*/

        func addContact() {

                var isValidated = true

                for (index, item) in secArray.enumerated() {

                    switch item {
                    case "Name":
                        if (self.tableView.indexPathsForVisibleRows?.contains(IndexPath(item: 0, section: index)))! {
                            let cell = self.tableView.cellForRow(at: IndexPath(item: 0, section: index)) as!
                            ContactGenricCell
                            if (cell.custTextField.text?.characters.count)! <= 0 {
                                cell.custTextField.configOnError(withPlaceHolder: "Enter UserName")
                                isValidated = false
                                break
                            } else {
                                name = cell.custTextField.text
                            }
                        } else {
                            self.tableView.scrollToRow(at: IndexPath(item: 0, section: index), at: .top, animated: false)
                            let cell = self.tableView.cellForRow(at: IndexPath(item: 0, section: index)) as!
                            ContactGenricCell
                            if (cell.custTextField.text?.characters.count)! <= 0 {
                                cell.custTextField.configOnError(withPlaceHolder: "Enter UserName")
                                isValidated = false
                                break
                            } else {
                                name = cell.custTextField.text
                            }
                        }
                    case "Phone No.":

                        for (row, num) in self.phoneNumArray.enumerated() {
                            if (self.tableView.indexPathsForVisibleRows?.contains(IndexPath(item: row, section: index)))! {
                            let cell = self.tableView.cellForRow(at: IndexPath(item: row, section: index)) as!
                            PhoneNumberCell
                            if row == 0 {
                                if (cell.phoneNumTextField.text?.characters.count)! < 10 {

                                    cell.phoneNumTextField.configOnError(withPlaceHolder: "Enter Valid Number")
                                    isValidated = false
                                    break
                                }
                            } else {
                                if (cell.phoneNumTextField.text?.characters.count)! > 0 && (cell.phoneNumTextField.text?.characters.count)! < 10 {

                                    cell.phoneNumTextField.configOnError(withPlaceHolder: "Enter Valid Number")
                                    isValidated = false
                                    break
                                }
                            }

                            } else {

                                self.tableView.scrollToRow(at: IndexPath(item: 0, section: index), at: .top, animated: false)
                                let cell = self.tableView.cellForRow(at: IndexPath(item: row, section: index)) as!
                                PhoneNumberCell
                                if row == 0 {
                                    if (cell.phoneNumTextField.text?.characters.count)! < 10 {

                                        cell.phoneNumTextField.configOnError(withPlaceHolder: "Enter Valid Number")
                                        isValidated = false
                                        break
                                    }
                                } else {
                                    if (cell.phoneNumTextField.text?.characters.count)! > 0 && (cell.phoneNumTextField.text?.characters.count)! < 10 {

                                        cell.phoneNumTextField.configOnError(withPlaceHolder: "Enter Valid Number")
                                        isValidated = false
                                        break
                                    }
                                }

                            }

                        }

                    case "Email":
                        for (row, num) in self.emailArray.enumerated() {
                            if (self.tableView.indexPathsForVisibleRows?.contains(IndexPath(item: row, section: index)))! {
                            let cell = self.tableView.cellForRow(at: IndexPath(item: row, section: index)) as!
                            EmailCell
                            if row == 0 {
                                if (!cell.emailTextField.text!.isValidEmail()) {
                                    cell.emailTextField.configOnError(withPlaceHolder: "Enter Valid Email")
                                    isValidated = false
                                    break
                                }
                            } else {
                                if (cell.emailTextField.text?.characters.count)! > 0 && (!cell.emailTextField.text!.isValidEmail()) {

                                    cell.emailTextField.configOnError(withPlaceHolder: "Enter Valid Email")
                                    isValidated = false
                                    break
                                }
                            }
                            } else {
                                self.tableView.scrollToRow(at: IndexPath(item: 0, section: index), at: .top, animated: false)
                                let cell = self.tableView.cellForRow(at: IndexPath(item: row, section: index)) as!
                                EmailCell
                                if row == 0 {
                                    if (!cell.emailTextField.text!.isValidEmail()) {
                                        cell.emailTextField.configOnError(withPlaceHolder: "Enter Valid Email")
                                        isValidated = false
                                        break
                                    }
                                } else {
                                    if (cell.emailTextField.text?.characters.count)! > 0 && (!cell.emailTextField.text!.isValidEmail()) {

                                        cell.emailTextField.configOnError(withPlaceHolder: "Enter Valid Email")
                                        isValidated = false
                                        break
                                    }
                                }
                            }
                        }

                    default:
                        break

                    }

                }
                if isValidated {

                    if let cont = self.contact {
                        cont.name = name
                        let primaryPhoneNum = self.phoneNumArray.remove(at: 0)
                        cont.phoneNumber = primaryPhoneNum
                        cont.secondaryPhoneNumbers = self.phoneNumArray.count > 0 ? self.phoneNumArray : nil

                        let primaryEmail = self.emailArray.remove(at: 0)
                        cont.email = primaryEmail
                        cont.secondaryEmails = self.emailArray.count > 0 ? self.emailArray : nil

                        cont.address = self.addressArray != nil ? self.addressArray[0] : nil

                        cont.company = company != nil ? company : nil
                        WebServiceManager.shared().editConatct(contactId: cont.contactId!, conatct: cont) {
                            (_, _, success) in
                            CommonMethods.hideProgressView()
                            if success {
                                DispatchQueue.main.async {

                                    guard let method = self.delegate?.didUpdateContact!(contact: cont) else {
                                        return
                                    }
                                    method

                                    self.backclick("")
                                }

                            }
                        }
                    } else {
                    self.contact = FEContact()
                    self.contact?.name = name

                    let primaryPhoneNum = self.phoneNumArray.remove(at: 0)
                    self.contact?.phoneNumber = primaryPhoneNum
                    self.contact?.secondaryPhoneNumbers = self.phoneNumArray.count > 0 ? self.phoneNumArray : nil

                    let primaryEmail = self.emailArray.remove(at: 0)
                    self.contact?.email = primaryEmail
                    self.contact?.secondaryEmails = self.emailArray.count > 0 ? self.emailArray : nil

                    self.contact?.address = self.addressArray != nil ? self.addressArray[0] : nil

                    self.contact?.company = company != nil ? company : nil

                    WebServiceManager.shared().addConatct(conatct: self.contact!) {
                        (_, _, success) in
                         CommonMethods.hideProgressView()
                        if success {
                            DispatchQueue.main.async {

                                guard let method = self.delegate?.didAddContact!() else {
                                    return
                                }
                                method
                                self.backclick("")
                            }

                        }
                    }
                    }
                }
            }

}

extension AddContactVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return secArray.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 54.0
        case 1:
            return 54
        case 2:
            return 54
        case 3:
            return 180
        default:
            return 54
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        switch section {
        case 0:
            return 1
        case 1:
            return self.phoneNumArray?.count ?? 1
        case 2:
            return self.emailArray?.count ?? 1
        case 3:
            return self.addressArray?.count ?? 1
        default:
            return 1
        }

//        return array.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()

      //  let phoneIndex = self.array.index(of: "Phone No.")
      //  let emailIndex = self.array.index(of: "Email")

      //  let addressIndex = self.array.index(of: "Address Line1(Street/Landmark)")

     //  let secEmailIndex = addressIndex!-emailIndex!-2

        switch self.secArray[indexPath.section] {
        case "Name":

            let genCell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! ContactGenricCell
          genCell.cellType = .nameField
            genCell.delegate = self as! ContactGenricCellDelegate
            genCell.custTextField.text = self.name
            cell = genCell

        case "Phone No.":
            switch indexPath.row {
            case 0:
                 cell = self.configurePhoneCell(withIndexPath: indexPath)

            default:
                cell = self.configureSecPhoneCell(withIndexPath: indexPath)
            }

        case "Email":

            switch indexPath.row {
            case 0:
                cell = self.confEmailCell(atIndexPath: indexPath)

            default:
                cell = self.configureSecEmailCell(atIndexPath: indexPath)
            }

        case "Address":

            let  addressCell = tableView.dequeueReusableCell(withIdentifier: "addLine1Cell", for: indexPath) as! AddressLine1Cell
             addressCell.mapImageView.isUserInteractionEnabled = true

             addressCell.searchMapLbl.isUserInteractionEnabled = true

             addressCell.addressLine1.textContainerInset = .zero

             addressCell.addressLine1.delegate = self

             addressCell.addressLine1.textContainer.lineFragmentPadding = 0.0

             let tap = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))

             tap.numberOfTapsRequired = 1

             let tap2 = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))

             tap2.numberOfTapsRequired = 1

             addressCell.mapImageView.addGestureRecognizer(tap2)

             addressCell.searchMapLbl.addGestureRecognizer(tap)

            if let streetAddress = self.addressArray[0].streetAddress {
                addressCell.addressLine1.text = streetAddress
            }

            if let locality = self.addressArray[0].locality {
                addressCell.addressLine1.text?.append(",\(locality)")
            }

           if let state = self.addressArray[0].region {
                addressCell.addressLine2.text = state
            }
            if let country = self.addressArray[0].country {
                addressCell.addressLine2.text?.append(",\(country)")
            }

            //addressCell.addressLine2.text = state + country //self.contact != nil? self.contact?.address?.streetAddress : addressCell.addressLine2.text
            if let postalCode = self.addressArray[0].postalCode {
            addressCell.zipCode.text = postalCode//self.contact != nil ? self.contact?.address?.postalCode : addressCell.zipCode.text
            }

            cell = addressCell

        case "Company Name":
            let genCell = tableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! ContactGenricCell
           genCell.cellType = .companyField
            genCell.delegate = self as! ContactGenricCellDelegate
            genCell.custTextField.text = self.company//self.contact != nil ? self.contact!.company : genCell.custTextField.text
                cell = genCell

        default:
            break
        }
        return cell
    }

    func configurePhoneCell(withIndexPath: IndexPath) -> PhoneNumberCell {

        let phoneCell = tableView.dequeueReusableCell(withIdentifier: "phoneNumCell", for: withIndexPath) as! PhoneNumberCell

        phoneCell.secPhoneCell = false

        phoneCell.addPhoneCellBtn.tag = withIndexPath.row

        phoneCell.addPhoneCellBtn.setImage(UIImage.init(named: "addCopy3"), for: .normal)

        phoneCell.addPhoneCellBtn.addTarget(self, action: #selector(addPhoneCell), for: .touchUpInside)

        phoneCell.phoneNumTextField.text = self.phoneNumArray[0]

        phoneCell.delegate = self

        phoneCell.phoneNumTextField.setPlaceholder("Phone No.", floatingTitle: "Phone No.")

       // phoneCell.phoneNumTextField.configOnFirstResponder(withPlaceHolder: "Phone No.")

        if let array = self.phoneNumArray, array.count > 1 {
            phoneCell.addPhoneCellBtn.isHidden = true
        } else {
            phoneCell.addPhoneCellBtn.isHidden = false
        }
        return phoneCell
    }

    func configureSecPhoneCell(withIndexPath: IndexPath) -> PhoneNumberCell {

        let phoneCell = tableView.dequeueReusableCell(withIdentifier: "phoneNumCell", for: withIndexPath) as! PhoneNumberCell
        phoneCell.addPhoneCellBtn.tag = withIndexPath.row
        phoneCell.delegate = self
        phoneCell.addPhoneCellBtn.isHidden = false
        if withIndexPath.row + 1 == self.phoneNumArray.count {

        phoneCell.addPhoneCellBtn.setImage(UIImage.init(named: "addCopy3"), for: .normal)

        } else {
            phoneCell.addPhoneCellBtn.setImage(UIImage.init(named: "addCopy3-1"), for: .normal)
        }
        phoneCell.phoneNumTextField.setPlaceholder("Sec Phone No.", floatingTitle: "Sec Phone No.")

        //phoneCell.phoneNumTextField.configOnFirstResponder(withPlaceHolder: "Sec Phone No.")

        phoneCell.secPhoneCell = true

        phoneCell.addPhoneCellBtn.addTarget(self, action: #selector(addPhoneCell), for: .touchUpInside)
        phoneCell.phoneNumTextField.text = self.phoneNumArray[withIndexPath.row]
        return phoneCell
    }

    func confEmailCell(atIndexPath: IndexPath) -> EmailCell {
        let emailCell = tableView.dequeueReusableCell(withIdentifier: "emailCell", for: atIndexPath) as! EmailCell
        emailCell.addEmailCellBtn.tag = atIndexPath.row
        emailCell.delegate = self

       emailCell.secEmailCell = false

        if let array = self.emailArray, array.count > 1 {
            emailCell.addEmailCellBtn.isHidden = true
        } else {
            emailCell.addEmailCellBtn.isHidden = false
        }

        emailCell.emailTextField.setPlaceholder("Email", floatingTitle: "Email")
        //emailCell.emailTextField.configOnFirstResponder(withPlaceHolder: "Email")
        emailCell.addEmailCellBtn.setImage(UIImage.init(named: "addCopy3"), for: .normal)
        emailCell.addEmailCellBtn.addTarget(self, action: #selector(addEmailCell), for: .touchUpInside)
        emailCell.emailTextField.text = emailArray[0]
        return emailCell

    }

    func configureSecEmailCell(atIndexPath: IndexPath) -> EmailCell {
    let emailCell = tableView.dequeueReusableCell(withIdentifier: "emailCell", for: atIndexPath) as! EmailCell

    emailCell.addEmailCellBtn.tag = atIndexPath.row

        emailCell.delegate = self

    emailCell.secEmailCell = true

        emailCell.addEmailCellBtn.isHidden = false

     /*   let emailIndex = self.array.index(of: "Email")
        
        let addressIndex = self.array.index(of: "Address Line1(Street/Landmark)")
        
        let secEmailIndex = addressIndex!-emailIndex!-2
        
  */
 if atIndexPath.row + 1 == emailArray.count {

            emailCell.addEmailCellBtn.setImage(UIImage.init(named: "addCopy3"), for: .normal)

        } else {
            emailCell.addEmailCellBtn.setImage(UIImage.init(named: "addCopy3-1"), for: .normal)
        }

        emailCell.emailTextField.setPlaceholder("Sec Email.", floatingTitle: "Sec Email")
       // emailCell.emailTextField.configOnFirstResponder(withPlaceHolder: "Sec Email")

    emailCell.addEmailCellBtn.addTarget(self, action: #selector(addEmailCell), for: .touchUpInside)
    emailCell.emailTextField.text =  emailArray[atIndexPath.row]
    return emailCell

    }

    func addPhoneCell(sender: UIButton) {

      /*  let indexPath = IndexPath(item: sender.tag, section: 1)
        
        let cell = self.tableView.cellForRow(at: indexPath) as! PhoneNumberCell
        
        cell.addPhoneCellBtn.setImage(UIImage.init(named: "addCopy3-1"), for: .normal)*/

        if sender.tag + 1 == self.phoneNumArray.count {

        self.phoneNumArray.append("")

       /*     let indexPath = IndexPath(item: sender.tag, section: 1)
            
            let cell = self.tableView.cellForRow(at: indexPath) as! PhoneNumberCell
            
            cell.addPhoneCellBtn.setImage(UIImage.init(named: "addCopy3-1"), for: .normal)*/

        //let newIndexPath = IndexPath(item: sender.tag+1, section: 1)

        //self.tableView.insertRows(at: [newIndexPath], with: .fade)
            self.tableView.reloadSections(IndexSet(integer: 1), with: .none)

            let newIndexPath = IndexPath(item: sender.tag+1, section: 1)

            let nextCell = self.tableView.cellForRow(at: newIndexPath) as! PhoneNumberCell

            nextCell.phoneNumTextField.becomeFirstResponder()

        } else {
            self.phoneNumArray.remove(at: sender.tag)
    self.tableView.reloadSections(IndexSet(integer: 1), with: .none)

        }

      /*  if cell.secPhoneCell == true {
            
            let newIndexPath = IndexPath(item: sender.tag+1, section: 0)
            
            let nextCell = self.tableView.cellForRow(at: newIndexPath)
            
            
            let emailIndex = self.array.index(of: "Email")
//            
            if sender.tag + 1 == emailIndex{
//
//

                self.array.insert("Sec Phone No.\(sender.tag-1)", at: sender.tag+1)
                
//                self.tableView.beginUpdates()
            
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
                
                
//                self.tableView.endUpdates()
            
                
//                let nextCell = self.tableView.cellForRow(at: newIndexPath) as! PhoneNumberCell
//
//                nextCell.addPhoneCellBtn.tag = newIndexPath.row
//                
//                nextCell.phoneNumTextField.setPlaceholder("Sec Phone No.", floatingTitle: "Sec Phone No.")
            
//                let emailIndexPath = IndexPath(item: newIndexPath.row+1, section: 0)
//                self.tableView.reloadRows(at: [emailIndexPath], with: .none)

                
                
            }
            
            else{
                
                cell.addPhoneCellBtn.setImage(UIImage.init(named: "addCopy3"), for: .normal)
                

                
                let emailIndex = self.array.index(of: "Email")
                
                self.array.remove(at:emailIndex!-1)
                
                
//                self.tableView.beginUpdates()
                
                self.tableView.deleteRows(at: [indexPath], with: .fade)
                
//                self.tableView.endUpdates()
       
//                
//                let nextCell = self.tableView.cellForRow(at: indexPath) as! PhoneNumberCell
//                
//                nextCell.addPhoneCellBtn.tag = indexPath.row
                
//                let emailIndexPath = IndexPath(item: indexPath.row+1, section: 0)
//                self.tableView.reloadRows(at: [emailIndexPath], with: .none)
                
            }
            
        }
        else{
            
//            cell.addPhoneCellBtn.isHidden = true
            
            let newIndexPath = IndexPath(item: sender.tag+1, section: 0)
            
            self.array.insert("Sec Phone No.\(sender.tag-1)", at: sender.tag+1)
            
//            self.tableView.beginUpdates()
        
            self.tableView.insertRows(at: [newIndexPath], with: .fade)
            
//            self.tableView.endUpdates()
            
            
//            let nextCell = self.tableView.cellForRow(at: newIndexPath) as! PhoneNumberCell
//            
//            nextCell.addPhoneCellBtn.tag = newIndexPath.row
//            
//            nextCell.phoneNumTextField.setPlaceholder("Sec Phone No.", floatingTitle: "Sec Phone No.")

//            let emailIndexPath = IndexPath(item: newIndexPath.row+1, section: 0)
//            self.tableView.reloadRows(at: [emailIndexPath], with: .none)
            
        }
        
        self.tableView.reloadData()*/
}

    func addEmailCell(sender: UIButton) {

         if sender.tag + 1 == self.emailArray.count {
            self.emailArray.append("")
            self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
            let newIndexPath = IndexPath(item: sender.tag+1, section: 2)

            let nextCell = self.tableView.cellForRow(at: newIndexPath) as! EmailCell
            nextCell.emailTextField.becomeFirstResponder()
        } else {
            self.emailArray.remove(at: sender.tag)
            self.tableView.reloadSections(IndexSet(integer: 2), with: .none)
        }

       /* let phoneIndex = self.array.index(of: "Phone No.")
        let emailIndex = self.array.index(of: "Email")
        
//        sender.tag = emailIndex!
    
        let indexPath = IndexPath(item:sender.tag , section: 0)
        
        let cell = self.tableView.cellForRow(at: indexPath) as! EmailCell
        
    
        let secEmailIndex = sender.tag-emailIndex!
        
        if cell.secEmailCell == true {
            
            let newIndexPath = IndexPath(item: sender.tag+1, section: 0)
            
            let nextCell = self.tableView.cellForRow(at: newIndexPath)
            
            if nextCell != nil{
            
            if nextCell!.isKind(of: EmailCell.self){
                
                cell.addEmailCellBtn.setImage(UIImage.init(named: "addCopy3"), for: .normal)
                
                self.array.remove(at:sender.tag)
                
                self.tableView.beginUpdates()
                
                self.tableView.deleteRows(at: [indexPath], with: .fade)
                
                self.tableView.endUpdates()
                
                let nextCell = self.tableView.cellForRow(at: indexPath) as! EmailCell
                
                nextCell.addEmailCellBtn.tag = indexPath.row
                

                
            }
            
                
            else{
                
                cell.addEmailCellBtn.setImage(UIImage.init(named: "addCopy3-1"), for: .normal)
                
                self.array.insert("Sec Email\(secEmailIndex)", at: sender.tag+1)
                
//                self.tableView.beginUpdates()
                
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
                
//                self.tableView.endUpdates()
                
                
//                let nextCell = self.tableView.cellForRow(at: newIndexPath) as! EmailCell
//                
//                
//                nextCell.emailTextField.setPlaceholder("Sec Email", floatingTitle: "Sec Email")
                
//                nextCell.addEmailCellBtn.tag = newIndexPath.row
//
//                let nextIndexPath = IndexPath(item: newIndexPath.row+1, section: 0)
//                self.tableView.reloadRows(at: [nextIndexPath], with: .none)
                
                
            }
            }
            
        }
        else{
            
            cell.addEmailCellBtn.isHidden = true
            
            let newIndexPath = IndexPath(item: sender.tag+1, section: 0)
            
            self.array.insert("Sec Email\(secEmailIndex)", at: sender.tag+1)
            
//            self.tableView.beginUpdates()
            
            self.tableView.insertRows(at: [newIndexPath], with: .fade)
            
//            self.tableView.endUpdates()
            
            
            
//            let nextCell = self.tableView.cellForRow(at: newIndexPath) as! EmailCell
//            
//            
//            
//            nextCell.emailTextField.setPlaceholder("Sec Email", floatingTitle: "Sec Email")
            
//            nextCell.addEmailCellBtn.tag = newIndexPath.row
            
            
            
//            let nextIndexPath = IndexPath(item: newIndexPath.row+1, section: 0)
//            self.tableView.reloadRows(at: [nextIndexPath], with: .none)
            
            
        }
        self.tableView.reloadData()*/

    }
    func navigateToMaps() {

        let mapVC = MapViewController.instantiateFromStoryboardWithIdentifier(identifier: "MapViewController")

      //  mapVC.addressSearchDelegate = self as AddressSearch

        let navController = UINavigationController(rootViewController: mapVC)

        navigationController?.present(navController, animated: true, completion: nil)

    }

    func addTapped(_ sender: Any) {

        //        address?.country = "India"
        //        address?.formatted = "jhgjg"
        //        address?.locality = "Banh"
        //        address?.postalCode = "5765765"
        //        address?.region = "hgjhgj"
        //        address?.streetAddress = "kjhkhihoi"
        //        address?.type = self.addressTypeTxtField.text

            if let placemark  = selectedPlaceMark {
                if let tit = placemark.title, let loc = placemark.locality, let subLoc = placemark.subLocality, let postalCode = placemark.postalCode, let state = placemark.administrativeArea, let country = placemark.country, let name = placemark.name, let thoroughfare = placemark.thoroughfare {

                    let address = FEAddress()
                    address?.formatted = "\(tit) \(name) \(loc) \(state) \(country) "
                    address?.streetAddress = thoroughfare
                    address?.locality = loc
                    address?.region = state
                    address?.postalCode = postalCode
                    address?.country = country

                    addressArray.append(address!)

                 //   self.tableView.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
                }
            }
//            else{
//                
//                self.view.endEditing(true)
//                
//                if (addressLine1.text?.characters.count)! <= 0 {
//                    
//                    self.addressLine1.configOnError(withPlaceHolder: "Enter Addressline 1")
//                }
//                    
//                    
//                else if  (self.addressLine2.text?.characters.count)! <= 0{
//                    
//                    self.addressLine2.configOnError(withPlaceHolder: "Enter Addressline 2")
//                }
//                    
////                else if  (self.cityTextField.text?.characters.count)! <= 0{
////                    
////                    self.cityTextField.configOnError(withPlaceHolder: "Enter City")
////                }
////                    
////                    
////                    
////                else if  (self.stateTextField.text?.characters.count)! <= 0{
////                    
////                    self.stateTextField.configOnError(withPlaceHolder: "Enter State")
////                }
//                    
//                    
//                else if  (self.postalCodeTextField.text?.characters.count)! <= 0{
//                    
//                    self.postalCodeTextField.configOnError(withPlaceHolder: "Enter Postal Code")
//                }
//                    
////                else if (self.countryTextField.text?.characters.count)! <= 0{
////                    self.countryTextField.configOnError(withPlaceHolder: "Enter Country")
////                    
////                }
//                    
//                    
//                else{
//                    self.address = FEAddress()
//                    
//                    self.address?.formatted = self.addressLine1.text
//                    self.address?.streetAddress = self.addressLine2.text
////                    address?.locality = self.cityTextField.text
////                    address?.region = self.stateTextField.text
//                    self.address?.postalCode = self.postalCodeTextField.text
////                    address?.country = self.countryTextField.text
//                    
//                }
//                
//            }

    }

}

extension AddContactVC: AddressSearch {

    func addrssObjectFrom(place: MKPlacemark?) {

        if let placemark = place {
            selectedPlaceMark = placemark

            let address = FEAddress()

            if let tit = placemark.title {
                address?.formatted = "\(tit)"
            }

            if let name = placemark.name {
                address?.streetAddress = "\(name)"
            }

            if let thoroughfare = placemark.thoroughfare {
                address?.streetAddress?.append(thoroughfare)
            }
            if let subLoc = placemark.subLocality {
                //self.addressLine2.text = "\(subLoc)"
            }
            if let loc = placemark.locality {
                address?.locality = "\(loc)"
            }
            if let postalCode = placemark.postalCode {
                address?.postalCode = "\(postalCode)"
            }
            if let state = placemark.administrativeArea {
                 address?.region = "\(state)"
            }
            if let country = placemark.country {
                address?.country = "\(country)"
            }

                addressArray[0] = address!

                self.tableView.reloadSections(IndexSet(integer: 3), with: .none)

            }
        }

}

extension AddContactVC: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.isKind(of: UICustomUnderlinedText.self) {
            let txtField = textView as! UICustomUnderlinedText
            txtField.configOnFirstResponder()
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.isKind(of: UICustomUnderlinedText.self) {
            let txtField = textView as! UICustomUnderlinedText
            txtField.configOnResignResponder()
        }
    }
}

extension UIImage {

    func isEqualToImage(image: UIImage) -> Bool {
        let data1: NSData = UIImagePNGRepresentation(self)! as NSData
        let data2: NSData = UIImagePNGRepresentation(image)! as NSData
        return data1.isEqual(data2)
    }

}
