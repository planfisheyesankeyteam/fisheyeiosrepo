//
//  UpdateContactGeneralInfoVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 17/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class UpdateContactGeneralInfoVC: UIViewController {

      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var websiteTextField: UICustomTextField!
      @IBOutlet weak var notesTextField: UICustomTextField!
      @IBOutlet weak var notesBlueUnderline: UIImageView!
      @IBOutlet weak var notesBlackUnderline: UIImageView!
      @IBOutlet weak var websiteBlueUnderline: UIImageView!
      @IBOutlet weak var websiteblackUnderline: UIImageView!
      @IBOutlet weak var finalSubmitImg: UIImageView!
      @IBOutlet weak var editIcon: UIImageView!
      @IBOutlet weak var syncContactsHeaderLbl: UILabel!
      @IBOutlet var firstTabImage: UIImageView!
      @IBOutlet var secondTabImage: UIImageView!
      @IBOutlet var thirdTabImage: UIImageView!
      @IBOutlet var fourthTabImage: UIImageView!
      @IBOutlet var tab1: UIView!
      @IBOutlet var tab2: UIView!
      @IBOutlet var tab3: UIView!
      @IBOutlet var tab4: UIView!
      @IBOutlet var stackViewBtn1: UIButton!
      @IBOutlet var stackViewBtn2: UIButton!
      @IBOutlet var stackViewBtn3: UIButton!
      @IBOutlet var stackViewBtn4: UIButton!

      @IBOutlet var separatorView1: UIView!
      @IBOutlet var separatorView2: UIView!
      @IBOutlet var separatorView3: UIView!
      @IBOutlet weak var fourthTabLbl: UILabel!
      @IBOutlet weak var thirdTabLbl: UILabel!
      @IBOutlet weak var secondTabLbl: UILabel!
      @IBOutlet weak var firstTabLbl: UILabel!

      let appSharedPrefernce =  AppSharedPreference()
      var idtoken = ""
      var contactId: Int = 0
      var contactDetails = ContactObj()
      var contactObjToOpen: Int = 0
      let validationController = ValidationController()
      var syncText = SyncToastMessages.shared
      var isEditMode: Bool = false
      var secondBtnMappedTo: String = ""
      var thirdBtnMappedTo: String = ""
      var appService = AppService.shared

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.applyViewShadow()
            self.validation()
            self.hideBlueUnderlines()
            if self.isEditMode {
                  self.userInteractionOn()
                  self.finalSubmitImg.isHidden = false
                  self.editIcon.isHidden = true
            } else {
                  self.userInteractionOff()
                  self.finalSubmitImg.isHidden = true
                  self.editIcon.isHidden = false
            }
            self.addObservers()
            self.setLocalizationText()
            if self.isEditMode {
                  self.showAllTabs()
            } else {
                  self.showFilledTabs()
            }
      }

      func showProfilePicture() {
            let picture = self.contactDetails.picture ?? ""
            firstTabImage?.sd_setImage(with: URL(string: picture ), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            firstTabImage.layer.cornerRadius = firstTabImage.frame.size.width / 2
            firstTabImage.clipsToBounds = true
      }

      func showAllTabs() {
            self.tab1.isHidden = false
            self.tab2.isHidden = false
            self.tab3.isHidden = false
            self.tab4.isHidden = false
            self.separatorView1.isHidden = false
            self.separatorView2.isHidden = false
            self.separatorView3.isHidden = false
            self.firstTabLbl.isHidden = false
            self.secondTabLbl.isHidden = false
            self.thirdTabLbl.isHidden = false
            self.fourthTabLbl.isHidden = false
            self.stackViewBtn1.isHidden = false
            self.stackViewBtn2.isHidden = false
            self.stackViewBtn3.isHidden = false
            self.stackViewBtn4.isHidden = false
            self.secondBtnMappedTo = "postal"
            self.thirdBtnMappedTo = "professional"
            self.secondTabImage.image = UIImage(named: "postalInfo")
            self.thirdTabImage.image = UIImage(named: "inactiveProfessionalInfo")
            self.fourthTabImage.image = UIImage(named: "activeGeneralInfo")
            self.secondTabLbl.text = self.syncText.postal
            self.thirdTabLbl.text = self.syncText.professional
            self.fourthTabLbl.text = self.syncText.general

      }

      func showFilledTabs() {
            if self.contactDetails.addressStringArray1.count > 0 {

                  self.secondTabImage.image = UIImage(named: "postalInfo")
                  self.separatorView1.isHidden = false
                  self.secondTabLbl.text = self.syncText.postal
                  self.secondBtnMappedTo = "postal"

                  if (self.contactDetails.company != "" || self.contactDetails.title != "" || self.contactDetails.companyEmail != "") {
                        self.thirdTabImage.image = UIImage(named: "inactiveProfessionalInfo")
                        self.separatorView2.isHidden = false
                        self.thirdTabLbl.text = self.syncText.professional
                        self.thirdBtnMappedTo = "professional"

                        if self.contactDetails.website != "" || self.contactDetails.notes != ""{
                              self.fourthTabImage.image = UIImage(named: "activeGeneralInfo")
                              self.separatorView3.isHidden = false
                              self.fourthTabLbl.text = self.syncText.general
                        } else {
                              self.tab4.isHidden = true
                              self.separatorView3.isHidden = true
                              self.fourthTabLbl.isHidden = true
                              self.stackViewBtn4.isHidden = true
                        }

                  } else if (self.contactDetails.website != "" || self.contactDetails.notes != "") {
                        self.thirdTabImage.image = UIImage(named: "activeGeneralInfo")
                        self.separatorView2.isHidden = false
                        self.thirdTabLbl.text = self.syncText.general
                        self.tab4.isHidden = true
                        self.separatorView3.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn4.isHidden = true
                  } else {
                        self.tab3.isHidden = true
                        self.tab4.isHidden = true
                        self.separatorView2.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn3.isHidden = true
                        self.stackViewBtn4.isHidden = true

                  }
            } else if (self.contactDetails.company != "" || self.contactDetails.title != "" || self.contactDetails.companyEmail != "" ) {

                  self.secondTabImage.image = UIImage(named: "inactiveProfessionalInfo")
                  self.separatorView1.isHidden = false
                  self.secondTabLbl.text = self.syncText.professional
                  self.secondBtnMappedTo = "professional"

                  if self.contactDetails.website != "" || self.contactDetails.notes != ""{
                        self.thirdTabImage.image = UIImage(named: "activeGeneralInfo")
                        self.separatorView2.isHidden = false
                        self.tab4.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.text = self.syncText.general
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn4.isHidden = true

                  } else {
                        self.tab3.isHidden = true
                        self.tab4.isHidden = true
                        self.separatorView2.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn3.isHidden = true
                        self.stackViewBtn4.isHidden = true

                  }
            } else if self.contactDetails.website != "" || self.contactDetails.notes != ""{
                  self.secondTabImage.image = UIImage(named: "activeGeneralInfo")
                  self.separatorView1.isHidden = false
                  self.tab3.isHidden = true
                  self.tab4.isHidden = true
                  self.separatorView2.isHidden = true
                  self.separatorView3.isHidden = true
                  self.secondTabLbl.text = self.syncText.general
                  self.thirdTabLbl.isHidden = true
                  self.fourthTabLbl.isHidden = true
                  self.stackViewBtn3.isHidden = true
                  self.stackViewBtn4.isHidden = true

            } else {
                  self.tab2.isHidden = true
                  self.tab3.isHidden = true
                  self.tab4.isHidden = true
                  self.separatorView1.isHidden = true
                  self.separatorView2.isHidden = true
                  self.separatorView3.isHidden = true
                  self.secondTabLbl.isHidden = true
                  self.thirdTabLbl.isHidden = true
                  self.fourthTabLbl.isHidden = true
                  self.stackViewBtn2.isHidden = true
                  self.stackViewBtn3.isHidden = true
                  self.stackViewBtn4.isHidden = true

            }
      }

      func userInteractionOff() {
            self.websiteTextField.isUserInteractionEnabled = false
            self.notesTextField.isUserInteractionEnabled = false
      }

      func userInteractionOn() {
            self.websiteTextField.isUserInteractionEnabled = true
            self.notesTextField.isUserInteractionEnabled = true
      }

      override func viewWillAppear(_ animated: Bool) {
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            self.contactObjToOpen = self.appSharedPrefernce.getAppSharedPreferences(key: "contactObjToOpen") as? Int ?? 0
            self.contactDetails = DatabaseManagement.shared.getContact(contactId: contactObjToOpen) as ContactObj
            self.showProfilePicture()
            self.assignDetails()
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.UpdateContactGeneralInfoVC)
      }

      func assignDetails() {
            self.websiteTextField.text = self.contactDetails.website
            self.notesTextField.text = self.contactDetails.notes
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      func hideBlueUnderlines() {

            self.websiteBlueUnderline.isHidden = true
            self.websiteblackUnderline.isHidden = false
            self.notesBlueUnderline.isHidden = true
            self.notesBlackUnderline.isHidden = false

      }

      func validation() {

            self.websiteTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.websiteTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.notesTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.notesTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  websiteTextField:

                  self.websiteBlueUnderline.isHidden = false
                  self.websiteblackUnderline.isHidden = true
                  if (websiteTextField.text?.characters.count)! <= 0 {
                        self.websiteTextField.configOnError(withPlaceHolder: self.syncText.enterWebsite)
                  } else {
                        self.websiteTextField.configOnErrorCleared(withPlaceHolder: self.syncText.website)
                  }

            case notesTextField:
                  self.notesBlueUnderline.isHidden = false
                  self.notesBlackUnderline.isHidden = true

                  if (notesTextField.text?.characters.count)! <= 0 {
                        self.notesTextField.configOnError(withPlaceHolder: self.syncText.enterNotes)

                  } else {
                        self.notesTextField.configOnErrorCleared(withPlaceHolder: self.syncText.notes)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case  websiteTextField:
                  self.websiteBlueUnderline.isHidden = true
                  self.websiteblackUnderline.isHidden = false
                  if (websiteTextField.text?.characters.count)! <= 0 {
                        self.websiteTextField.configOnError(withPlaceHolder: self.syncText.enterWebsite)
                  } else if (!validationController.isValidWebsite(url: self.websiteTextField.text!)) {
                        self.websiteTextField.configOnError(withPlaceHolder: self.syncText.invalidWebsite)
                  } else {
                        self.websiteTextField.configOnErrorCleared(withPlaceHolder: self.syncText.website)
                  }

            case notesTextField:
                  self.notesBlueUnderline.isHidden = true
                  self.notesBlackUnderline.isHidden = false

                  if (notesTextField.text?.characters.count)! <= 0 {
                        self.notesTextField.configOnError(withPlaceHolder: self.syncText.enterNotes)

                  } else {
                        self.notesTextField.configOnErrorCleared(withPlaceHolder: self.syncText.notes)
                  }

            default:
                  break
            }
      }

      func checkValidation() {
            self.addGeneralInfo()
      }

      @IBAction func backBtnTapped(_ sender: Any) {
            loadSyncViewController()
      }


      @IBAction func submitTapped(_ sender: Any) {
            if(self.editIcon.isHidden == true) {
                  self.checkValidation()
            } else {
                  self.finalSubmitImg.isHidden = false
                  self.editIcon.isHidden = true
                  userInteractionOn()
            }
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      @IBAction func personalBtnTapped(_ sender: Any) {
            loadDetailsPersonalInfo(isEditMode: self.editIcon.isHidden)
      }

      @IBAction func postalBtnTapped(_ sender: Any) {
            self.openOtherPage(nameOfpage: self.secondBtnMappedTo)
            //            loadDetailsPostalInfo(isEditMode: self.editIcon.isHidden)
      }

      @IBAction func professionalBtnTapped(_ sender: Any) {
            self.openOtherPage(nameOfpage: self.thirdBtnMappedTo)
            //            loadDetailsProfessionalInfo(isEditMode: self.editIcon.isHidden)
      }

      func openOtherPage(nameOfpage: String) {
            switch nameOfpage {
            case "postal":
                  loadDetailsPostalInfo(isEditMode: self.editIcon.isHidden)
                  break
            case "professional":
                  loadDetailsProfessionalInfo(isEditMode: self.editIcon.isHidden)
                  break
            default:
                  break
            }
      }

      func addGeneralInfo() {
            let website = websiteTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let notes = notesTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            var contactObj = ContactObj()
            contactObj.contactId = self.contactObjToOpen
            contactObj.website = website
            contactObj.notes = notes
            contactObj.isBackendSync = false
            contactObj.updatedAt = getCurrentTImeStamp()
            let isOperationSuccessfull = DatabaseManagement.shared.updateGeneralInfo(contactObj: contactObj)
            if(isOperationSuccessfull) {
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
                  self.showFilledTabs()
                  self.userInteractionOff()
                  self.editIcon.isHidden = false
                  self.finalSubmitImg.isHidden = true
                  self.appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "syncViewController")
                  NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update contact", action: "Submit general information button clicked", label: "Update general information successful", value: 0)
            } else {
                self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update contact", action: "Submit general information button clicked", label: "Failed to update contacts's general information", value: 0)
            }

      }

      /* Created by venkatesh modified by vaishali  */
      func applyViewShadow() {
            self.mainView.layer.cornerRadius = 8
            // border
            self.mainView.layer.borderWidth = 0
            self.mainView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.mainView.layer.shadowColor = UIColor.black.cgColor
            self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainView.layer.shadowOpacity = 0.7
            self.mainView.layer.shadowRadius = 10.0
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.syncContactsHeaderLbl.text = self.syncText.syncContacts
                  self.firstTabLbl.text = self.syncText.personal
                  self.secondTabLbl.text = self.syncText.postal
                  self.thirdTabLbl.text = self.syncText.professional
                  self.fourthTabLbl.text = self.syncText.general
                  self.websiteTextField.placeholder = self.syncText.website
                  self.notesTextField.placeholder = self.syncText.notes
            }
      }
      /* END */
}
