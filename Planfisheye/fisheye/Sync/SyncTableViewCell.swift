//
//  SyncTableViewCell.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 07/08/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class SyncTableViewCell: UITableViewCell {

    // IBOutlet declarations
      @IBOutlet weak var unExpandedContactCell: UIView!
      @IBOutlet weak var profileImage: UIImageView!
      @IBOutlet weak var fisheyePurpleStripe: UIView!
      @IBOutlet weak var contactsTypeLblView: UIView!
      @IBOutlet weak var isFisheyeIcon: UIImageView!
      @IBOutlet weak var emailLbl: UILabel!
      @IBOutlet weak var separator: UIView!
      @IBOutlet weak var contactView: UIView!
      @IBOutlet weak var syncOptionsView: UIView!
      @IBOutlet weak var syncOptionsBtn: SyncContactCellButtons!
      @IBOutlet weak var syncOptionViewHeight: NSLayoutConstraint!
      @IBOutlet weak var detailContactViewButton: SyncContactCellButtons!
      @IBOutlet weak var completeTableViewCellHeight: NSLayoutConstraint!
      @IBOutlet weak var lastUnderline: UIView!
      @IBOutlet weak var skypeCallBtn: SyncContactCellButtons!
      @IBOutlet weak var whatsAppCallBtn: SyncContactCellButtons!
      @IBOutlet weak var blockOrUnblockContact: SyncContactCellButtons!
      @IBOutlet weak var stackViewDetails: UIStackView!
      @IBOutlet weak var nativeCall: SyncContactCellButtons!
      @IBOutlet weak var nameLbl: UILabel!
      @IBOutlet weak var phoneNumLbl: UILabel!
      @IBOutlet weak var singleDetailDisplay: UILabel!
      @IBOutlet weak var privacyImage: UIImageView!

      // Labels
      @IBOutlet weak var blockOrUnblockContactLabel: UILabel!
      @IBOutlet weak var callLbl: UILabel!
      @IBOutlet weak var whatsAppCallLbl: UILabel!
      @IBOutlet weak var blockUnblockLbl: UILabel!
      @IBOutlet weak var skypeCallLbl: UILabel!
    
      @IBOutlet weak var pulseButton1: SyncContactCellButtons!
      @IBOutlet weak var pulseLabel1: UILabel!
      @IBOutlet weak var PulseView1: UIView!
    
      @IBOutlet weak var pulseView2: UIView!
      @IBOutlet weak var pulseButton2: SyncContactCellButtons!
      @IBOutlet weak var pulseLabel2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    /* Description :- To update the table view cell size on click of detailContactViewButton*/
    var isExpanded: Bool = false {
        didSet {
            if !isExpanded {
                    self.syncOptionViewHeight.constant = 0.0
                    self.syncOptionsView.isHidden = true
                    self.completeTableViewCellHeight.constant = 70.0
            } else {
                    self.syncOptionViewHeight.constant = 128.0
                    self.syncOptionsView.isHidden = false
                    self.completeTableViewCellHeight.constant = 200.0
            }
        }
    }
    /* End */

}

class SyncContactCellButtons: UIButton {
      var row: Int?
      var section: Int?
}

class SyncContactTapGestureRecognizer: UITapGestureRecognizer {
      var row: Int?
      var section: Int?
}
