//
//  ContactViewController.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 8/17/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

@objc protocol ContactViewDelegate {
    func updateSyncView()
}

class ContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AddContactDelegate {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var contactNameLbl: UILabel!

    @IBOutlet weak var reqLookOutBtn: UIButton!

    @IBOutlet weak var requestLookOutView: UIView!

    weak var delegate: ContactViewDelegate?

//    enum RowTitle:Int {
//        case primary=0,secondary,address,companyName
//    }

//    var titleOrder:RowTitle!

    var contact: FEContact!

    override func viewDidLoad() {
        super.viewDidLoad()

        //TODO: Get contact by ID
        if let id = self.contact.contactId {
            self.getContactDetail(withId: id)
        }
        self.contactNameLbl.text = self.contact.name

//        self.createGradientLayer(inView: self.view,colors:[UIColor.white.cgColor,UIColor.white.cgColor])

        tableview.estimatedRowHeight = 110
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.delegate = self

        self.view.bringSubview(toFront: self.mainview)

        self.view.bringSubview(toFront: self.requestLookOutView)

        self.reqLookOutBtn.backgroundColor = UIColor.frenchBlue()

        self.reqLookOutBtn.layer.cornerRadius = 0.09467391304*self.reqLookOutBtn.bounds.width

        self.reqLookOutBtn.setTitleColor(UIColor.white, for: .normal)

        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

        vc.footerTitle.isHidden = true

        vc.footerSubTitle.isHidden = true

//         self.createGradientLayer(inView: self.requestLookOutView,colors:[UIColor.duckEggBlue().cgColor,UIColor.white.cgColor])

//        applyViewBorder()

        // Do any additional setup after loading the view.
    }

    func didUpdateContact(contact: FEContact) {
        self.contact = contact
        self.contactNameLbl.text = self.contact.name
        self.tableview.reloadData()

        guard let method = delegate?.updateSyncView() else {
            return
        }
        method
    }

    func getContactDetail(withId: String) {
        WebServiceManager.shared().getConatct(contactId: withId) { (result, _, success) in

            CommonMethods.hideProgressView()

            if success {
                DispatchQueue.main.async {

                    let res = result as! FEContactDetailsResponse

                    self.contact =  res.contact

                    self.tableview.reloadData()
                }

            }
        }
    }

    @IBAction func backclick(_ sender: Any) {

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

        vc.hideBottomviews()

    }

    override func viewDidAppear(_ animated: Bool) {
        self.mainview.backgroundColor = UIColor.white
        applyViewBorder()
    }

    @IBAction func editTapped(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
        let addContactVC = storyboard.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
        addContactVC.contact = self.contact
        addContactVC.delegate = self
        addContactVC.view.frame = CGRect.init(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height) //self.view.bounds
        addContactVC.mainView.clipsToBounds = true
        self.addChildViewController(addContactVC)
        addContactVC.didMove(toParentViewController: self)

        addContactVC.view.frame.origin.x = self.view.frame.size.width + 30

        self.view.addSubview(addContactVC.view)
        self.view.bringSubview(toFront: addContactVC.view)

        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: { _ in
            addContactVC.view.frame.origin.x = self.view.bounds.origin.x
        }, completion: nil)

    }

    func applyViewBorder() {

        self.view.layer.cornerRadius = 8

        // border
        self.view.layer.borderWidth = 0
        self.view.layer.borderColor = UIColor.black.cgColor

        self.mainview.layer.cornerRadius = 8

        // border
        self.mainview.layer.borderWidth = 0
        self.mainview.layer.borderColor = UIColor.black.cgColor

        self.mainview.layer.backgroundColor = UIColor.white.cgColor

        self.mainview.layer.shadowColor = UIColor.black.cgColor
        self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.mainview.layer.shadowOpacity = 0.7
        self.mainview.layer.shadowRadius = 4.0

        self.mainview.layer.masksToBounds = false

        self.requestLookOutView.layer.masksToBounds = false
        self.requestLookOutView.layer.cornerRadius = 8

    }

    func createGradientLayer(inView view: UIView, colors: [CGColor]) {

        let gradientLayer = CAGradientLayer()

        // Hoizontal - commenting these two lines will make the gradient veritcal
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)

        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)

        let gradientLocations: [NSNumber] = [0.0, 1.0]

        gradientLayer.frame = view.bounds

        gradientLayer.colors = colors

        gradientLayer.locations = gradientLocations

        view.layer.addSublayer(gradientLayer)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var defautCell: UITableViewCell!

        switch indexPath.row {
        case 0:

            let cell = self.tableview.dequeueReusableCell(withIdentifier: "contactDetailCell", for: indexPath)as! ContactDetailTableViewCell

            cell.selectionStyle = .none

            cell.titleLbl.text = "Primary Phone Number"

            if(self.contact != nil && self.contact.phoneNumber != nil) {
            if let phoneNum = self.contact.phoneNumber {

                cell.subTitleLbl.text = "\(phoneNum)"

            }
            }
            defautCell = cell

        case 1:
            let cell = self.tableview.dequeueReusableCell(withIdentifier: "contactDetailCell", for: indexPath)as! ContactDetailTableViewCell

            cell.selectionStyle = .none
            cell.titleLbl.text = "Primary Email"

            if(self.contact != nil ) {
            if let email = self.contact.email {
                cell.subTitleLbl.text = "\(email)"
            }
            }
            defautCell = cell

        case 2:
            let cell = self.tableview.dequeueReusableCell(withIdentifier: "contactDetailCell", for: indexPath)as! ContactDetailTableViewCell

            cell.selectionStyle = .none
            cell.titleLbl.text = "Secondary Phone Number"
            if(self.contact != nil) {
            if let phoneNums = self.contact.secondaryPhoneNumbers, phoneNums.count > 0 {

                var str = String()
                for email in phoneNums {
                    str += (email)
                    str += ("\n")
                }
                cell.subTitleLbl.text =   str.dropLast(1)
            } else {
                cell.subTitleLbl.text = ""
            }
            }
            defautCell = cell

        case 3:
            let cell = self.tableview.dequeueReusableCell(withIdentifier: "contactDetailCell", for: indexPath)as! ContactDetailTableViewCell

            cell.selectionStyle = .none

            cell.titleLbl.text = "Secondary Emails"
            if(self.contact != nil ) {
                 if let emailAddresses = self.contact.secondaryEmails, emailAddresses.count > 0 {
                    var str = String()
                    for email in emailAddresses {
                        str += (email)
                        str += ("\n")
                    }

                    cell.subTitleLbl.text =  str.dropLast(1)
                } else {
                    cell.subTitleLbl.text = ""
            }
        }
            defautCell = cell

        case 4:
            let cell = self.tableview.dequeueReusableCell(withIdentifier: "contactDetailCell", for: indexPath)as! ContactDetailTableViewCell

            cell.selectionStyle = .none

            cell.titleLbl.text = "Address"
            if(self.contact != nil ) {
            cell.subTitleLbl.text = self.contact.address?.formatted
            }
            defautCell = cell

        case 5:
            let cell = self.tableview.dequeueReusableCell(withIdentifier: "contactDetailCell", for: indexPath)as! ContactDetailTableViewCell

            cell.selectionStyle = .none
            cell.titleLbl.text = "Company name"
            if(self.contact != nil ) {
            cell.subTitleLbl.text = contact.company
            }
            defautCell = cell
        default:
            break
        }
             return defautCell

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

//        if let secMails = self.contact.secondaryEmails  {
//            if let secPh
//        }
        return 6
    }
}

extension String {
    func dropLast(_ n: Int = 1) -> String {
        return String(characters.dropLast(n))
    }
    var dropLast: String {
        return dropLast()
    }
}
