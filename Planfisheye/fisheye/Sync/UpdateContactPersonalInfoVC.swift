//
//  UpdateContactPersonalInfoVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 17/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class UpdateContactPersonalInfoVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

      @IBOutlet weak var editButton: UIImageView!
      @IBOutlet weak var nextIconImg: UIImageView!
      @IBOutlet weak var emailTableView: UITableView!
      @IBOutlet weak var phoneNumberTableView: UITableView!
//      @IBOutlet weak var scrollView: UIScrollView!
      @IBOutlet weak var scrollOuterView: UIView!
      @IBOutlet weak var nameTextField: UICustomTextField!
      @IBOutlet weak var nextIcon: UIButton!
      @IBOutlet weak var containerView: UIView!
      @IBOutlet weak var nameBlackUnderline: UIImageView!
      @IBOutlet weak var nameBlueUnderline: UIImageView!
      @IBOutlet var buttonsStackView: UIStackView!
      @IBOutlet var tabsStackView: UIStackView!
      @IBOutlet var firstTabImage: UIImageView!
      @IBOutlet var secondTabImage: UIImageView!
      @IBOutlet var thirdTabImage: UIImageView!
      @IBOutlet var fourthTabImage: UIImageView!
      @IBOutlet var tab1: UIView!
      @IBOutlet var tab2: UIView!
      @IBOutlet var tab3: UIView!
      @IBOutlet var tab4: UIView!
      @IBOutlet var stackViewBtn1: UIButton!
      @IBOutlet var stackViewBtn2: UIButton!
      @IBOutlet var stackViewBtn3: UIButton!
      @IBOutlet var stackViewBtn4: UIButton!

      @IBOutlet var separatorView1: UIView!
      @IBOutlet var separatorView2: UIView!
      @IBOutlet var separatorView3: UIView!
      @IBOutlet weak var fourthTabLbl: UILabel!
      @IBOutlet weak var thirdTabLbl: UILabel!
      @IBOutlet weak var secondTabLbl: UILabel!
      @IBOutlet weak var firstTabLbl: UILabel!
      @IBOutlet weak var syncContactsLbl: UILabel!
      @IBOutlet weak var PrivacyDetail: UILabel!
      @IBOutlet weak var privacySwitchButton: UISwitch!
      @IBOutlet weak var privacyDetailTextView: UITextView!
      @IBOutlet weak var privacyMessage: UITextField!
      @IBOutlet weak var popupView: UIView!
      @IBOutlet weak var popupHeading: UILabel!
      @IBOutlet weak var popupMessage: UILabel!
      @IBOutlet weak var popUpCentreConstraint: NSLayoutConstraint!
      @IBOutlet weak var popupFirstOptionLbl: UILabel!
      @IBOutlet weak var popupSecondOptionLbl: UILabel!
      @IBOutlet var phoneNumberTableHeightConstraint: NSLayoutConstraint!
      @IBOutlet var emailTableHeightConstraint: NSLayoutConstraint!
      @IBOutlet var addPhoneBtn: UIButton!
      @IBOutlet var addEmailBtn: UIButton!
      @IBOutlet var scrollView: UIScrollView!
      @IBOutlet var addPhoneLbl: UILabel!
      @IBOutlet var addEmailLbl: UILabel!
    
      var addContactPostalInfoVC: addContactPostalInfoVC!
      var addContactProfesionalInfoVC: addContactProfesionalInfoVC!
      var addContactGeneralInfoVC: addContactGeneralInfoVC!
      var isFailedToSave: Bool = false
      let appSharedPrefernce = AppSharedPreference()
      let validationController = ValidationController()
      var idtoken = ""
      var contactId = ""
      var contactObjToOpen: Int = 0
      var addedContact: ContactObj  = ContactObj()
      var syncText = SyncToastMessages.shared
      var isEditable: Bool = false
      var currentCountryCode: String  = ""
      let index = IndexPath(row: 0, section: 0)
      let index1 = IndexPath(row: 1, section: 0)
      let index2 = IndexPath(row: 2, section: 0)
      var tempObj = ContactObj()
      var phoneNumber = ""
      var email = ""
      var userPhoneNumber: String = ""
      var userEmail: String = ""
      var userPhoneWithCountryCode: String = ""
      var personalView: UIView!
      var postalView: UIView!
      var professionalView: UIView!
      var generalView: UIView!
      var numberOfTabsFilled: Int = 4
      var separatorView: UIView!
      var isEditMode: Bool = true
      var secondBtnMappedTo: String = ""
      var thirdBtnMappedTo: String = ""
      var fourthBtnMappedTo: String = ""
      var appService = AppService.shared
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var previousPrivacySettingForContact: Bool = true
      var currentPrivacySettingForContact: Bool = true
      var obj: [String: Any]?
      var privacyInProfile: Bool = true
      var noOfPhoneRowsAdded: Int  = 0
      var noOfEmailRowsAdded: Int = 0
    
      override func viewDidLoad() {

            super.viewDidLoad()
            self.stopLoader()
            self.applyViewShadow()
            self.contactObjToOpen = self.appSharedPrefernce.getAppSharedPreferences(key: "contactObjToOpen") as? Int ?? 0
            self.addedContact = DatabaseManagement.shared.getContact(contactId: contactObjToOpen) as ContactObj
            self.assignDetails()
            self.setLocalizationText()
            self.phoneNumberTableView.setEditing(false, animated: false)
            self.emailTableView.setEditing(false, animated: false)

            self.currentCountryCode = getCurrentCountryCode()
            self.isEditable = self.isEditMode
            self.showProfilePicture()
            if self.isEditMode {
                  self.showAllTabs()
                  self.editButton.isHidden = true
                  self.nextIconImg.isHidden = false
//                  let image = UIImage(named: "FinalSubmit") as UIImage?
//                  self.nextIcon.setImage(image, for: .normal)

            } else {
                  self.showFilledTabs()
                  self.editButton.isHidden = false
                  self.nextIconImg.isHidden = true
//                  let image = UIImage(named: "editNewIcon") as UIImage?
//                  self.nextIcon.setImage(image, for: .normal)
            }

            self.modifyDesign()
            self.phoneNumberTableView.isHidden = false
            self.applyPopUpShadow()
            self.phoneNumberTableView.isScrollEnabled = false
            self.emailTableView.isScrollEnabled = false
            self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
            if let tempObj = obj {
                let privacy = tempObj["privacyEnabled"]  as? Bool ?? true
                self.privacyInProfile = privacy
            }
      }

      /* Created by venkatesh modified by vaishali  */
      func applyPopUpShadow() {
            self.popupView.layer.cornerRadius = 8

            // border
            self.popupView.layer.borderWidth = 0
            self.popupView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.popupView.layer.shadowColor = UIColor.black.cgColor
            self.popupView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.popupView.layer.shadowOpacity = 0.7
            self.popupView.layer.shadowRadius = 10.0
      }
      /* END */

      func showProfilePicture() {
            let picture = self.addedContact.picture ?? ""
            firstTabImage?.sd_setImage(with: URL(string: picture ), placeholderImage: #imageLiteral(resourceName: "personalINfo"))
            firstTabImage.layer.cornerRadius = firstTabImage.frame.size.width / 2
            firstTabImage.clipsToBounds = true
      }

      func modifyDesign() {
            self.privacySwitchButton.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
            self.privacySwitchButton.layer.cornerRadius = 16.0
      }

      func showAllTabs() {
            self.tab1.isHidden = false
            self.tab2.isHidden = false
            self.tab3.isHidden = false
            self.tab4.isHidden = false
            self.separatorView1.isHidden = false
            self.separatorView2.isHidden = false
            self.separatorView3.isHidden = false
            self.firstTabLbl.isHidden = false
            self.secondTabLbl.isHidden = false
            self.thirdTabLbl.isHidden = false
            self.fourthTabLbl.isHidden = false
            self.stackViewBtn1.isHidden = false
            self.stackViewBtn2.isHidden = false
            self.stackViewBtn3.isHidden = false
            self.stackViewBtn4.isHidden = false
            self.secondBtnMappedTo = "postal"
            self.thirdBtnMappedTo = "professional"
            self.fourthBtnMappedTo = "general"
            self.secondTabImage.image = UIImage(named: "postalInfo")
            self.thirdTabImage.image = UIImage(named: "inactiveProfessionalInfo")
            self.fourthTabImage.image = UIImage(named: "generalInfo")
            self.secondTabLbl.text = self.syncText.postal
            self.thirdTabLbl.text = self.syncText.professional
            self.fourthTabLbl.text = self.syncText.general

      }

      func showFilledTabs() {
            if self.addedContact.addressStringArray1.count > 0 {

                  self.secondTabImage.image = UIImage(named: "postalInfo")
                  self.separatorView1.isHidden = false
                  self.secondTabLbl.text = self.syncText.postal
                  self.secondBtnMappedTo = "postal"
                  if (self.addedContact.company != "" || self.addedContact.title != "" || self.addedContact.companyEmail != "") {
                        self.thirdTabImage.image = UIImage(named: "inactiveProfessionalInfo")
                        self.separatorView2.isHidden = false
                        self.thirdTabLbl.text = self.syncText.professional
                        self.thirdBtnMappedTo = "professional"

                        if self.addedContact.website != "" || self.addedContact.notes != ""{
                              self.fourthTabImage.image = UIImage(named: "generalInfo")
                              self.separatorView3.isHidden = false
                              self.fourthTabLbl.text = self.syncText.general
                              self.fourthBtnMappedTo = "general"

                        } else {
                              self.tab4.isHidden = true
                              self.separatorView3.isHidden = true
                              self.fourthTabLbl.isHidden = true
                              self.stackViewBtn4.isHidden = true
                        }

                  } else if (self.addedContact.website != "" || self.addedContact.notes != "") {
                        self.thirdTabImage.image = UIImage(named: "generalInfo")
                        self.separatorView2.isHidden = false
                        self.thirdTabLbl.text = self.syncText.general
                        self.tab4.isHidden = true
                        self.separatorView3.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn4.isHidden = true
                        self.thirdBtnMappedTo = "general"

                  } else {
                        self.tab3.isHidden = true
                        self.tab4.isHidden = true
                        self.separatorView2.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn3.isHidden = true
                        self.stackViewBtn4.isHidden = true

                  }
            } else if (self.addedContact.company != "" || self.addedContact.title != "" || self.addedContact.companyEmail != "" ) {

                  self.secondTabImage.image = UIImage(named: "inactiveProfessionalInfo")
                  self.separatorView1.isHidden = false
                  self.secondTabLbl.text = self.syncText.professional
                  self.secondBtnMappedTo = "professional"

                  if self.addedContact.website != "" || self.addedContact.notes != ""{
                        self.thirdTabImage.image = UIImage(named: "generalInfo")
                        self.separatorView2.isHidden = false
                        self.tab4.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.text = self.syncText.general
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn4.isHidden = true
                        self.thirdBtnMappedTo = "general"

                  } else {
                        self.tab3.isHidden = true
                        self.tab4.isHidden = true
                        self.separatorView2.isHidden = true
                        self.separatorView3.isHidden = true
                        self.thirdTabLbl.isHidden = true
                        self.fourthTabLbl.isHidden = true
                        self.stackViewBtn3.isHidden = true
                        self.stackViewBtn4.isHidden = true

                  }
            } else if self.addedContact.website != "" || self.addedContact.notes != ""{
                  self.secondTabImage.image = UIImage(named: "generalInfo")
                  self.separatorView1.isHidden = false
                  self.tab3.isHidden = true
                  self.tab4.isHidden = true
                  self.separatorView2.isHidden = true
                  self.separatorView3.isHidden = true
                  self.secondTabLbl.text = self.syncText.general
                  self.thirdTabLbl.isHidden = true
                  self.fourthTabLbl.isHidden = true
                  self.stackViewBtn3.isHidden = true
                  self.stackViewBtn4.isHidden = true
                  self.secondBtnMappedTo = "general"

            } else {
                  self.tab2.isHidden = true
                  self.tab3.isHidden = true
                  self.tab4.isHidden = true
                  self.separatorView1.isHidden = true
                  self.separatorView2.isHidden = true
                  self.separatorView3.isHidden = true
                  self.secondTabLbl.isHidden = true
                  self.thirdTabLbl.isHidden = true
                  self.fourthTabLbl.isHidden = true
                  self.stackViewBtn2.isHidden = true
                  self.stackViewBtn3.isHidden = true
                  self.stackViewBtn4.isHidden = true

            }
      }

      func assignDetails() {
            let name = self.addedContact.name ?? ""
            self.nameTextField.text = name
            self.nameBlueUnderline.isHidden = true
            self.phoneNumberTableView.reloadData()
            self.emailTableView.reloadData()
            self.phoneNumber = self.addedContact.phoneNumber
            self.email = self.addedContact.email
            self.privacySwitchButton.isOn = self.addedContact.privacySetForContactUser
            self.previousPrivacySettingForContact = self.addedContact.privacySetForContactUser
            self.currentPrivacySettingForContact = self.addedContact.privacySetForContactUser
            self.showPrivacySwitchBtnColor()
            self.changeUserInteraction()
      }

      func changeUserInteraction() {
            self.nameTextField.isUserInteractionEnabled = self.isEditMode
            self.privacySwitchButton.isUserInteractionEnabled = self.isEditMode
            self.addPhoneBtn.isUserInteractionEnabled = self.isEditMode
            self.addEmailBtn.isUserInteractionEnabled = self.isEditMode
      }

      func showPrivacySwitchBtnColor() {
            if self.addedContact.privacySetForContactUser {
                  self.privacySwitchButton.backgroundColor = UIColor.duckEggBlue()
            } else {
                  self.privacySwitchButton.backgroundColor = UIColor.red
            }
      }

      override func viewWillAppear(_ animated: Bool) {

            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "contactObjToOpen") as? String ?? ""
            self.userPhoneNumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            self.userEmail = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
            let countryCode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
            self.userPhoneWithCountryCode =  countryCode + self.userPhoneNumber
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.UpdateContactPersonalInfoVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      func hideBlueUnderlines() {

            self.nameBlueUnderline.isHidden = true
            self.nameBlackUnderline.isHidden = false
      }

      func validation() {

            self.nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.nameTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            guard let cell = (self.phoneNumberTableView.cellForRow(at: self.index) as? addContactPhoneNumberCell) else {return}
            guard let phoneCell1: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index1) as? addContactPhoneNumberCell else {return}
            guard let phoneCell2: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index2) as? addContactPhoneNumberCell else {return}

            guard let cell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index) as? AddContactEmailCell else {return}
            guard let emailcell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index1) as? AddContactEmailCell else {return}
            guard let emailcell2: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index2) as? AddContactEmailCell else {return}
            switch textField {

            case  nameTextField:
                  self.nameBlueUnderline.isHidden = false
                  self.nameBlackUnderline.isHidden = true
                  if (nameTextField.text?.characters.count)! <= 0 {
                        self.nameTextField.configOnError(withPlaceHolder: self.syncText.enterName)

                  } else {
                        self.nameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.name)
                    
                  }

            case cell.phoneNumber:
                  cell.phoneBlackUnderline.isHidden = true
                  cell.phoneBlueUnderline.isHidden = false
                  cell.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  break

            case phoneCell1.phoneNumber:
                  phoneCell1.phoneBlackUnderline.isHidden = true
                  phoneCell1.phoneBlueUnderline.isHidden = false
                  phoneCell1.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  break

            case phoneCell2.phoneNumber:
                  phoneCell2.phoneBlackUnderline.isHidden = true
                  phoneCell2.phoneBlueUnderline.isHidden = false
                  phoneCell2.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  break

            case cell1.emailAdd:
                  cell1.emailBlackUnderline.isHidden = true
                  cell1.emailBlueUnderline.isHidden = false
                  cell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  break

            case emailcell1.emailAdd:
                  emailcell1.emailBlackUnderline.isHidden = true
                  emailcell1.emailBlueUnderline.isHidden = false
                  emailcell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  break

            case emailcell2.emailAdd:
                  emailcell2.emailBlackUnderline.isHidden = true
                  emailcell2.emailBlueUnderline.isHidden = false
                  emailcell2.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  break

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {

            guard  let cell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index) as? addContactPhoneNumberCell else {return}
            guard  let phoneCell1: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index1) as? addContactPhoneNumberCell else {return}
            guard  let phoneCell2: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index2) as? addContactPhoneNumberCell else {return}

            guard let cell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index) as? AddContactEmailCell else {return}
            guard let emailcell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index1) as? AddContactEmailCell else {return}
            guard let emailcell2: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index2) as? AddContactEmailCell else {return}

            switch textField {

            case  nameTextField:
                  self.nameBlueUnderline.isHidden = true
                  self.nameBlackUnderline.isHidden = false
                  if (nameTextField.text?.characters.count)! <= 0 {
                        self.nameTextField.configOnError(withPlaceHolder: self.syncText.enterName)

                  } else {
                        self.nameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.name)
                }

            case cell.phoneNumber:
                  cell.phoneBlackUnderline.isHidden = false
                  cell.phoneBlueUnderline.isHidden = true
                  if (cell.phoneNumber.text?.characters.count)! > 0 && cell.phoneNumber.text != self.currentCountryCode && ((cell.phoneNumber.text?.characters.count)! < 6 || (cell.phoneNumber.text?.characters.count)! > 20) {
                        cell.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  } else {
                        cell.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  }

            case phoneCell1.phoneNumber:
                  phoneCell1.phoneBlackUnderline.isHidden = false
                  phoneCell1.phoneBlueUnderline.isHidden = true
                  if (phoneCell1.phoneNumber.text?.characters.count)! > 0 && phoneCell1.phoneNumber.text != self.currentCountryCode && ((phoneCell1.phoneNumber.text?.characters.count)! < 6 || (phoneCell1.phoneNumber.text?.characters.count)! > 20) {
                        phoneCell1.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  } else {
                        phoneCell1.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  }

            case phoneCell2.phoneNumber:
                  phoneCell2.phoneBlackUnderline.isHidden = false
                  phoneCell2.phoneBlueUnderline.isHidden = true
                  if (phoneCell2.phoneNumber.text?.characters.count)! > 0 && phoneCell2.phoneNumber.text != self.currentCountryCode && ((phoneCell2.phoneNumber.text?.characters.count)! < 6 || (phoneCell2.phoneNumber.text?.characters.count)! > 20) {
                        phoneCell2.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  } else {
                        phoneCell2.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  }

            case cell1.emailAdd:
                  cell1.emailBlackUnderline.isHidden = false
                  cell1.emailBlueUnderline.isHidden = true
                  if (cell1.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: cell1.emailAdd.text!) {
                        cell1.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  } else {
                        cell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  }
            case emailcell1.emailAdd:
                  emailcell1.emailBlackUnderline.isHidden = false
                  emailcell1.emailBlueUnderline.isHidden = true
                  if(emailcell1.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: emailcell1.emailAdd.text!) {
                        emailcell1.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  } else {
                        emailcell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  }
                  break

            case emailcell2.emailAdd:
                  emailcell2.emailBlackUnderline.isHidden = false
                  emailcell2.emailBlueUnderline.isHidden = true
                  if(emailcell2.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: emailcell2.emailAdd.text!) {
                        emailcell2.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  } else {
                        emailcell2.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  }
                  break
            default:
                  break
            }
      }

      /* Created by venkatesh modified by vaishali  */
      func applyViewShadow() {
            self.containerView.layer.cornerRadius = 8
            self.scrollOuterView.layer.cornerRadius = 8
            self.scrollView.layer.cornerRadius = 8
            self.phoneNumberTableView.layer.cornerRadius = 8
            self.emailTableView.layer.cornerRadius = 8
            // border
            self.containerView.layer.borderWidth = 0
            self.containerView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.containerView.layer.shadowColor = UIColor.black.cgColor
            self.containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.containerView.layer.shadowOpacity = 0.7
            self.containerView.layer.shadowRadius = 10.0
      }
      /* END */

      func checkValidation() {

            if (nameTextField.text?.characters.count)! <= 0 {
                  self.nameTextField.configOnError(withPlaceHolder: self.syncText.enterName)
                  return
            }
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      @IBAction func popupFirstBtnTapped(_ sender: Any) {
            self.privacySwitchButton.isOn = false
            self.showPrivacyMessages(privacySetForContact: false)
            self.hidePopUp()
            self.currentPrivacySettingForContact = false
      }

      @IBAction func popupSecondBtnTapped(_ sender: Any) {
            self.privacySwitchButton.isOn = true
            self.showPrivacyMessages(privacySetForContact: true)
            self.hidePopUp()
            self.currentPrivacySettingForContact = true
      }

      @IBAction func privacySwitchBtnTapped(_ sender: Any) {
            if(NetworkStatus.sharedManager.isNetworkReachable()) {
                  if self.currentPrivacySettingForContact {
                        self.popupHeading.text = self.syncText.privacyChange
                        self.popupMessage.text = self.syncText.areUSureToDisablePrivacyForContact
                        self.popupFirstOptionLbl.text = self.syncText.showPrivacy
                        self.popupSecondOptionLbl.text = self.syncText.keepPrivate
                  } else {
                        self.popupHeading.text = self.syncText.privacyChange
                        self.popupMessage.text = self.syncText.areUSureToEnablePrivacyForContact
                        self.popupFirstOptionLbl.text = self.syncText.showPrivacy
                        self.popupSecondOptionLbl.text = self.syncText.keepPrivate
                  }
                  self.showPopUp()
//                  self.changePivacy()
            } else {
                  self.privacySwitchButton.isOn = self.previousPrivacySettingForContact
                  self.showPrivacyMessages(privacySetForContact: self.previousPrivacySettingForContact)
                self.view.makeToast(self.appService.checkInternetConnectionMessage, duration: self.appService.firstTimeInterval, position: .bottom)
            }

      }

      func showPopUp() {
            self.popUpCentreConstraint.constant = self.syncText.popUpCenterConstraint
            self.nextIcon.isUserInteractionEnabled = false
            UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                  self.view.layoutIfNeeded()
            })
      }

      func hidePopUp() {
            self.popUpCentreConstraint.constant = self.syncText.popUpLeftConstraint
            self.nextIcon.isUserInteractionEnabled = true
            UIView.animate(withDuration: TimeInterval(self.syncText.popUpInOutTimePeriod), animations: {
                  self.view.layoutIfNeeded()
            })
      }

      @IBAction func backBtnTapped(_ sender: Any) {
            loadSyncViewController()
      }

    @IBAction func addPhoneBtnTapped(_ sender: Any) {
        self.phoneNumberTableView.beginUpdates()
        self.noOfPhoneRowsAdded = self.noOfPhoneRowsAdded + 1
        let numberOfrows = self.phoneNumberTableView.numberOfRows(inSection: 0)
        self.phoneNumberTableView.insertRows(at: [IndexPath.init(row: numberOfrows, section: 0)], with: .automatic)
        self.phoneNumberTableView.endUpdates()
        self.updateScrollViewContentSize()
    }
    
    @IBAction func addEmailBtnTapped(_ sender: Any) {
        self.emailTableView.beginUpdates()
        self.noOfEmailRowsAdded = self.noOfEmailRowsAdded + 1
        let numberOfrows = self.emailTableView.numberOfRows(inSection: 0)
        self.emailTableView.insertRows(at: [IndexPath.init(row: numberOfrows, section: 0)], with: .automatic)
        self.emailTableView.endUpdates()
        self.updateScrollViewContentSize()
    }
    
    @IBAction func nextIconTapped(_ sender: Any) {
            if(self.isEditMode == true) {
                  self.updateContactPersonalInfo()
            } else {
                  self.isEditMode = true
                  self.showAllTabs()
                  self.isEditable = true
                  self.changeUserInteraction()
                  self.addedContact = DatabaseManagement.shared.getContact(contactId: self.addedContact.contactId)
                  self.phoneNumberTableView.reloadData()
                  self.emailTableView.reloadData()
                                    self.editButton.isHidden = true
                                    self.nextIconImg.isHidden = false
//                  let image = UIImage(named: "FinalSubmit") as UIImage?
//                  self.nextIcon.setImage(image, for: .normal)
                  self.phoneNumberTableView.setEditing(true, animated: false)
                  self.emailTableView.setEditing(true, animated: false)
            }
      }

    func updateContactPersonalInfo() {
        self.nextIcon.isUserInteractionEnabled = false
        var isValidationCorrect: Bool = true
        
        if (nameTextField.text?.characters.count)! <= 0 {
            self.nameTextField.configOnError(withPlaceHolder: self.syncText.enterName)
            isValidationCorrect = false
        }
        
        for phoneRow in 0..<self.phoneNumberTableView.numberOfRows(inSection: 0) {
            let phoneIndex = IndexPath(row: phoneRow, section: 0)
            let phoneCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: phoneIndex) as! addContactPhoneNumberCell
            if (phoneCell.phoneNumber.text?.characters.count)! > 0 && phoneCell.phoneNumber.text != self.currentCountryCode && ((phoneCell.phoneNumber.text?.characters.count)! < 6 || (phoneCell.phoneNumber.text?.characters.count)! > 20) {
                phoneCell.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                isValidationCorrect = false
            }else{
                phoneCell.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
            }
        }
        
        for emailRow in 0..<self.emailTableView.numberOfRows(inSection: 0) {
            let emailIndex = IndexPath(row: emailRow, section: 0)
            let emailCell: AddContactEmailCell = self.emailTableView.cellForRow(at: emailIndex) as! AddContactEmailCell
            if (emailCell.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: emailCell.emailAdd.text!) {
                emailCell.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                isValidationCorrect = false
            }else{
                emailCell.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
            }
        }
        

        if isValidationCorrect {

            var primaryNum: String = ""
            var primaryMail: String = ""
            var secondaryPhones: String = ""
            var secondaryEmails: String = ""
            var secondaryPhoneArr: [String] = []
            var secondaryEmailArr: [String] = []
            
            let name = self.nameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            for phoneRow in 0..<self.phoneNumberTableView.numberOfRows(inSection: 0) {
                let phoneIndex = IndexPath(row: phoneRow, section: 0)
                let phoneCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: phoneIndex) as! addContactPhoneNumberCell
                if phoneRow == 0{
                    if(phoneCell.phoneNumber.text != self.currentCountryCode) {
                        primaryNum = phoneCell.phoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        if(primaryNum.first == "0") {
                            primaryNum = String(primaryNum.dropFirst(1))
                        }
                    }
                }else{
                    var tempPhone = phoneCell.phoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                    if(tempPhone.first == "0") {
                        tempPhone = String(tempPhone.dropFirst(1))
                    }
                    if tempPhone != ""{
                        secondaryPhoneArr.append(tempPhone)
                    }
                }
            }
            
            for emailRow in 0..<self.emailTableView.numberOfRows(inSection: 0) {
                let emailIndex = IndexPath(row: emailRow, section: 0)
                let emailCell: AddContactEmailCell = self.emailTableView.cellForRow(at: emailIndex) as! AddContactEmailCell
                if emailRow == 0{
                    if(emailCell.emailAdd.text != self.currentCountryCode) {
                        primaryMail = emailCell.emailAdd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        primaryMail = primaryMail.lowercased()
                    }
                }else{
                    var tempEmail = emailCell.emailAdd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                    tempEmail = tempEmail.lowercased()
                    if tempEmail != ""{
                        secondaryEmailArr.append(tempEmail)
                    }
                }
            }

            if(primaryNum == self.userPhoneNumber || primaryNum == self.userPhoneWithCountryCode || primaryMail == self.userEmail) {
                self.view.makeToast(self.syncText.notAllowedToAddSelfPhoneAndEmailInSync)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                    self.view.hideToast()
                }
                return
            }
            
            if (primaryNum.count == 0 && secondaryPhoneArr.count > 0) {
                primaryNum = secondaryPhoneArr[0]
                secondaryPhoneArr.remove(at: 0)
            }
            
            if (primaryMail.count == 0 && secondaryEmailArr.count > 0) {
                primaryMail = secondaryEmailArr[0]
                secondaryEmailArr.remove(at: 0)
            }
            
            self.tempObj = ContactObj()
            self.tempObj.contactId = self.addedContact.contactId
            self.tempObj.name = name
            self.tempObj.phoneNumber = primaryNum
            self.tempObj.secondaryPhoneNumbers = secondaryPhoneArr
            self.tempObj.email = primaryMail
            self.tempObj.secondaryEmails = secondaryEmailArr
            
            secondaryPhones = secondaryPhoneArr.map { String($0) }
                .joined(separator: ",")
            secondaryEmails = secondaryEmailArr.map { String($0) }
                .joined(separator: ",")
            
            
            var existingContacts: [ContactObj] = []
            existingContacts  = DatabaseManagement.shared.getAlreadyExistsContactsFromInUseContacts(tempPhoneNumber: primaryNum, tempEmail: primaryMail)
            
            self.addedContact.name = name
            self.addedContact.stringSecondaryPhones = secondaryPhones
            self.addedContact.stringSecondaryEmails = secondaryEmails
            self.addedContact.phoneNumber = primaryNum
            self.addedContact.email = primaryMail
            self.addedContact.privacySetForContactUser = self.privacySwitchButton.isOn
            self.addedContact.isBackendSync = false
            
            if primaryNum  != self.phoneNumber || primaryMail != self.email{
                if (self.addedContact.sharedData == 0 && primaryNum  != self.phoneNumber && primaryNum.characters.count > 0) {
                    self.addedContact.sharedData = 2
                } else if( self.addedContact.sharedData == 1 &&  primaryMail != self.email && primaryMail.characters.count > 0) {
                    self.addedContact.sharedData = 2
                }else if(self.addedContact.sharedData == 3 && (primaryNum  != self.phoneNumber) && (primaryNum.characters.count > 0) && (primaryMail != self.email) && (primaryMail.characters.count > 0)){
                    self.addedContact.sharedData = 2
                }else if(self.addedContact.sharedData == 3 && (((primaryNum  == self.phoneNumber) && (primaryNum.characters.count > 0)) || ((primaryMail == self.email) && (primaryMail.characters.count > 0)))){
                    if primaryNum == "" || primaryMail == ""{
                        self.addedContact.sharedData = 3
                    }else if primaryNum  != self.phoneNumber{
                        self.addedContact.sharedData = 1
                    }else if primaryMail != self.email{
                        self.addedContact.sharedData = 0
                    }
                }else{
                    self.addedContact.sharedData = 2
                }
                
                if self.addedContact.sharedData != 3{
                    
                    if(primaryNum == "") {
                        self.addedContact.sharedData = 0
                    } else if(primaryMail == "") {
                        self.addedContact.sharedData = 1
                    }
                    
                    if(self.addedContact.phoneNumber == "") {
                        self.addedContact.sharedData = 0
                    } else if(self.addedContact.email == "") {
                        self.addedContact.sharedData = 1
                    }
                }
            }


            
            if(existingContacts.count == 0 ) {
                self.checkConditions()
//                self.editButton.isHidden = false
//                self.nextIconImg.isHidden = true
                //                        let image = UIImage(named: "editNewIcon") as UIImage?
                //                        self.nextIcon.setImage(image, for: .normal)
                self.phoneNumberTableView.setEditing(false, animated: false)
                self.emailTableView.setEditing(false, animated: false)
            } else if(existingContacts.count == 1 && existingContacts[0].contactId == self.addedContact.contactId) {
                self.checkConditions()
//                self.editButton.isHidden = false
//                self.nextIconImg.isHidden = true
                //                        let image = UIImage(named: "editNewIcon") as UIImage?
                //                        self.nextIcon.setImage(image, for: .normal)
                self.phoneNumberTableView.setEditing(false, animated: false)
                self.emailTableView.setEditing(false, animated: false)
            } else if (existingContacts.count > 1) {
                var isAllHaveSameContactFEId: Bool = true
                for i in 0..<existingContacts.count {
                    if existingContacts[i].contactFEId != self.addedContact.contactFEId {
                        isAllHaveSameContactFEId = false
                    }else{
                        self.view.makeToast(self.syncText.contactAlreadyExistsByPhoneOrEmail, duration: self.appService.firstTimeInterval, position: .bottom)
                        break
                    }
                    if isAllHaveSameContactFEId && i == existingContacts.count - 1{
                        self.checkConditions()
//                        self.editButton.isHidden = false
//                        self.nextIconImg.isHidden = true
                        //                                    let image = UIImage(named: "editNewIcon") as UIImage?
                        //                                    self.nextIcon.setImage(image, for: .normal)
                        self.phoneNumberTableView.setEditing(false, animated: false)
                        self.emailTableView.setEditing(false, animated: false)
                    }
                }
            } else {
                self.nextIcon.isUserInteractionEnabled = true
                self.isFailedToSave = true
                self.phoneNumberTableView.reloadData()
                self.emailTableView.reloadData()
                self.view.makeToast(self.syncText.contactAlreadyExists)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                    self.view.hideToast()
                }
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update Contact", action: "Update personal information clicked", label: "Contact already exists by Phone Number or Email id", value: 0)
            }
        }else{
            self.nextIcon.isUserInteractionEnabled = true
        }
    }
    
    func checkConditions(){
        if self.previousPrivacySettingForContact != self.privacySwitchButton.isOn{
            let count  = DatabaseManagement.shared.getCountOfFEContactsWhoesPrivacyIsSameAsInProfile(privacyEnabled: self.privacyInProfile)
            var alert = UIAlertController()
            if count == 1{
                if self.privacyInProfile{
                    alert = UIAlertController(title: ProfileToastMsgHeadingSubheadingLabels.shared.privacydisabled, message: self.syncText.confirmationOfPrivacyWillDisabledInProfile, preferredStyle: UIAlertController.Style.alert)
                }else{
                    alert = UIAlertController(title: ProfileToastMsgHeadingSubheadingLabels.shared.privacyenabled, message: self.syncText.confirmationOfPrivacyWillEnabledInProfile, preferredStyle: UIAlertController.Style.alert)
                }
                
                
                alert.addAction(UIAlertAction(title: self.syncText.cancel, style: UIAlertAction.Style.cancel, handler: { action in
                    switch action.style{
                        
                    case .default:
                        self.saveProfileData()
                        break
                    case .cancel:
                        alert.dismiss(animated: true, completion: nil)
                        self.nextIcon.isUserInteractionEnabled = true
                        break
                    case .destructive:
                        self.nextIcon.isUserInteractionEnabled = true
                        break
                    }
                }))
                
                self.present(alert, animated: true, completion: nil)
                alert.addAction(UIAlertAction(title: self.appService.continue2, style: .default, handler: { action in
                    switch action.style{
                        
                    case .default:
                        self.saveProfileData()
                        break
                    case .cancel:
                        alert.dismiss(animated: true, completion: nil)
                        self.nextIcon.isUserInteractionEnabled = true
                        break
                    case .destructive:
                        self.nextIcon.isUserInteractionEnabled = true
                        break
                    }
                }))
                
            }else{
                self.updateInSQLiteDatabase()
            }
        }else{
            self.updateInSQLiteDatabase()
        }
    }

    func saveProfileData(){
        
        if NetworkStatus.sharedManager.isNetworkReachable() {
            
            if let tempObj = self.obj {
                self.privacyInProfile = tempObj["privacyEnabled"]  as? Bool ?? true
                var profile = Profile()
                profile.name = tempObj["name"]  as? String ?? ""
                profile.email =  tempObj["email"]  as? String ?? ""
                profile.phonenumber = tempObj["phonenumber"]  as? String ?? ""
                profile.picture = tempObj["picture"]  as? String ?? ""
                profile.countryCode = tempObj["countryCode"]  as? String ?? ""
                profile.privacyEnabled = self.addedContact.privacySetForContactUser
                profile.gender = tempObj["gender"]  as? String ?? ""
                profile.isMarketingMessagingEnabled = tempObj["isMarketingMessagingEnabled"]  as? Bool ?? false
                
                
                self.appSharedPrefernce.setAppSharedPreferences(key: "editBasicProfileStatusCode", value: "")
                let editProfile = editBasicInformationOperation(profile: profile)
                editProfile.isImportantDetailsChanged = false
                self.startLoader()
                editProfile.addDidFinishBlockObserver { [unowned self] (operation, _) in
                    let status = self.appSharedPrefernce.getAppSharedPreferences(key: "editBasicProfileStatusCode") as? String ?? ""
                    
                    if status == "200" {
                        DispatchQueue.main.async {
                            self.stopLoader()
                            self.updateInSQLiteDatabase()
                            self.view.makeToast(self.profilemsg.profileUpdate, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync edit contact - Personal information", action: "Update Contact details click", label: "Privacy changed in Profile from Sync", value: 0)
                    } else {
                        
                        self.stopLoader()
                        self.nextIcon.isUserInteractionEnabled = true
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.profileupdateerror, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sync edit contact - Personal information", action: "Update Contact details click", label: "Failed to privacy in Profile from Sync", value: 0)
                    }
                }
                AppDelegate.addProcedure(operation: editProfile)
            } else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.profilemsg.profileupdateerror, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                }
                self.nextIcon.isUserInteractionEnabled = true
            }
            
        }else{
            DispatchQueue.main.async {
                self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.profilemsg.toastDelayTime, position: .bottom)
            }
        }
    }
    
      func updateInSQLiteDatabase() {
            if(!NetworkStatus.sharedManager.isNetworkReachable()) {
                  self.privacySwitchButton.isOn = self.previousPrivacySettingForContact
                  self.addedContact.privacySetForContactUser = self.previousPrivacySettingForContact
            }
            self.addedContact.updatedAt = getCurrentTImeStamp()
            let isOperationSuccessfull = DatabaseManagement.shared.updatePersonalInfo(contactObj: self.addedContact)
            if(isOperationSuccessfull) {
                  self.appSharedPrefernce.setAppSharedPreferences(key: "contactObjToOpen", value: self.addedContact.contactId)
                  self.appSharedPrefernce.setAppSharedPreferences(key: "addContactInProgress", value: 0)
                  self.isEditable = false
                  self.addedContact = DatabaseManagement.shared.getContact(contactId: self.addedContact.contactId)
                  self.noOfPhoneRowsAdded = 0
                  self.noOfEmailRowsAdded = 0
                  self.phoneNumberTableView.reloadData()
                  self.emailTableView.reloadData()
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
                  if self.previousPrivacySettingForContact != self.privacySwitchButton.isOn {
                      self.changePrivacySetting()
                  }else{
                      self.nextIcon.isUserInteractionEnabled = true
                      self.editButton.isHidden = false
                      self.nextIconImg.isHidden = true
                  }
                  self.previousPrivacySettingForContact = self.addedContact.privacySetForContactUser
                  self.currentPrivacySettingForContact = self.addedContact.privacySetForContactUser
                  NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                  self.showFilledTabs()
                  self.showProfilePicture()
                  self.isEditMode = false
                  self.changeUserInteraction()
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update Contact", action: "Update personal information clicked", label: "Update personal information successful", value: 0)
                  self.phoneNumber = self.addedContact.phoneNumber
                  self.email = self.addedContact.email
            } else {
                self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                self.nextIcon.isUserInteractionEnabled = true
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update Contact", action: "Update personal information clicked", label: "Failed to update contact's personal information", value: 0)
            }
      }

      @IBAction func postalBtnTapped(_ sender: UIButton) {
            //        let button = sender as! UIButton

            self.openOtherPage(nameOfpage: self.secondBtnMappedTo)
            //        loadDetailsPostalInfo(isEditMode: self.isEditMode)
      }

      @IBAction func professionalBtnTapped(_ sender: Any) {
            self.openOtherPage(nameOfpage: self.thirdBtnMappedTo)

            //        loadDetailsProfessionalInfo(isEditMode: self.editButton.isHidden)
      }

      @IBAction func generalBtnTapped(_ sender: Any) {
            self.openOtherPage(nameOfpage: self.fourthBtnMappedTo)

            //        loadDetailsGeneralInfo(isEditMode: self.editButton.isHidden)
      }
    
    func openOtherPage(nameOfpage: String) {
            switch nameOfpage {
            case "postal":
                  loadDetailsPostalInfo(isEditMode: self.isEditMode)
                  break
            case "professional":
                  loadDetailsProfessionalInfo(isEditMode: self.isEditMode)
                  break
            case "general":
                  loadDetailsGeneralInfo(isEditMode: self.isEditMode)
                  break
            default:
                  break
            }
      }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.phoneNumberTableView {
            if self.isFailedToSave{
                if self.tempObj.phoneNumber != nil && self.tempObj.phoneNumber != "" {
                    self.phoneNumberTableHeightConstraint.constant = CGFloat(75 * (self.tempObj.secondaryPhoneNumbers.count + 1))
                    return self.tempObj.secondaryPhoneNumbers.count + 1
                }else{
                    self.phoneNumberTableHeightConstraint.constant = CGFloat(75 * (self.tempObj.secondaryPhoneNumbers.count))
                    return self.tempObj.secondaryPhoneNumbers.count
                }
            }else{
                if self.addedContact.phoneNumber != nil && self.addedContact.phoneNumber != "" {
                    self.phoneNumberTableHeightConstraint.constant = CGFloat(75 * (self.addedContact.secondaryPhoneNumbers.count + noOfPhoneRowsAdded + 1))
                    return self.addedContact.secondaryPhoneNumbers.count + noOfPhoneRowsAdded + 1
                }else{
                    self.phoneNumberTableHeightConstraint.constant = CGFloat(75 * (self.addedContact.secondaryPhoneNumbers.count + noOfPhoneRowsAdded))
                    return self.addedContact.secondaryPhoneNumbers.count + noOfPhoneRowsAdded
                }
            }
        }else if tableView == self.emailTableView {
            if self.isFailedToSave{
                if self.tempObj.email != nil && self.tempObj.email != "" {
                    self.emailTableHeightConstraint.constant = CGFloat(75 * (self.tempObj.secondaryEmails.count + 1))
                    return self.tempObj.secondaryEmails.count + 1
                }else{
                    self.emailTableHeightConstraint.constant = CGFloat(75 * (self.tempObj.secondaryEmails.count))
                    return self.tempObj.secondaryEmails.count
                }
            }else{
                if self.addedContact.email != nil && self.addedContact.email != "" {
                    self.emailTableHeightConstraint.constant = CGFloat(75 * (self.addedContact.secondaryEmails.count + noOfEmailRowsAdded + 1))
                    return self.addedContact.secondaryEmails.count + noOfEmailRowsAdded + 1
                }else{
                    self.emailTableHeightConstraint.constant = CGFloat(75 * (self.addedContact.secondaryEmails.count + noOfEmailRowsAdded))
                    return self.addedContact.secondaryEmails.count + noOfEmailRowsAdded
                }
            }
        }else{
            return 0
        }
    }
    
    override func viewDidLayoutSubviews(){
        self.updateScrollViewContentSize()
    }
    
    func updateScrollViewContentSize(){
        var totalPhoneNumbersCount = 0
        var totalEmailsCount = 0
        let screenSize: CGRect = UIScreen.main.bounds


        if self.isFailedToSave{
            if self.tempObj.phoneNumber != nil && self.tempObj.phoneNumber != "" {
                totalPhoneNumbersCount = self.tempObj.secondaryPhoneNumbers.count + 1
            }else{
                totalPhoneNumbersCount = self.tempObj.secondaryPhoneNumbers.count
            }

            if self.tempObj.email != nil && self.tempObj.email != "" {
                totalEmailsCount = self.tempObj.secondaryEmails.count + 1
            }else{
                totalEmailsCount = self.tempObj.secondaryEmails.count
            }
        }else{
            if self.addedContact.phoneNumber != nil && self.addedContact.phoneNumber != "" {
                totalPhoneNumbersCount = self.addedContact.secondaryPhoneNumbers.count + self.noOfPhoneRowsAdded + 1
            }else{
                totalPhoneNumbersCount = self.addedContact.secondaryPhoneNumbers.count + self.noOfPhoneRowsAdded
            }

            if self.addedContact.email != nil && self.addedContact.email != "" {
                totalEmailsCount = self.addedContact.secondaryEmails.count + self.noOfEmailRowsAdded + 1
            }else{
                totalEmailsCount = self.addedContact.secondaryEmails.count + self.noOfEmailRowsAdded
            }
        }
        self.scrollView.contentSize = CGSize(width: CGFloat(screenSize.width), height: CGFloat(320 + (75 * (totalPhoneNumbersCount + totalEmailsCount))))
    }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if tableView == self.phoneNumberTableView {
            var cell = addContactPhoneNumberCell()
            cell = self.phoneNumberTableView.dequeueReusableCell(withIdentifier: "addContactPhoneNumberCell", for: indexPath) as!  addContactPhoneNumberCell
            cell.phoneNumber.placeholder = self.syncText.phone
            
            if(!self.isFailedToSave) {
                if(indexPath.row == 0) {
                    cell.phoneNumber.text = self.addedContact.phoneNumber
                } else {
                    cell.phoneNumber.text = ""
                    if(self.addedContact.secondaryPhoneNumbers.count > 0) && ((indexPath.row - 1) < self.addedContact.secondaryPhoneNumbers.count) {
                        cell.phoneNumber.text = self.addedContact.secondaryPhoneNumbers[indexPath.row - 1]
                    }
                }
            } else {
                if(indexPath.row == 0) {
                    cell.phoneNumber.text = self.tempObj.phoneNumber
                } else{
                    cell.phoneNumber.text = ""
                    if(self.tempObj.secondaryPhoneNumbers.count > 0) && ((indexPath.row - 1) < self.tempObj.secondaryPhoneNumbers.count){
                        cell.phoneNumber.text = self.tempObj.secondaryPhoneNumbers[indexPath.row - 1]
                    }
                }
            }
            
            if(self.isEditable == false) {
                cell.phoneNumber.isUserInteractionEnabled = false
            } else {
                cell.phoneNumber.isUserInteractionEnabled = true
            }
            
            cell.phoneBlackUnderline.isHidden = false
            cell.phoneBlueUnderline.isHidden = true
            
            cell.phoneNumber.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            cell.phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            
            return cell
        } else if(tableView == self.emailTableView ) {
            var cell = AddContactEmailCell()
            cell = self.emailTableView.dequeueReusableCell(withIdentifier: "AddContactEmailCell", for: indexPath) as!  AddContactEmailCell
            cell.emailAdd.placeholder = self.syncText.email
            if(!self.isFailedToSave) {
                if(indexPath.row == 0) {
                    cell.emailAdd.text = self.addedContact.email
                } else {
                    cell.emailAdd.text = ""
                    if(self.addedContact.secondaryEmails.count > 0) && ((indexPath.row - 1) < self.addedContact.secondaryEmails.count){
                        cell.emailAdd.text = self.addedContact.secondaryEmails[indexPath.row - 1]
                    }
                }
            } else {
                if(indexPath.row == 0) {
                    cell.emailAdd.text = self.tempObj.email
                } else {
                    cell.emailAdd.text = ""
                    if(self.tempObj.secondaryEmails.count > 0 ) && ((indexPath.row - 1) < self.tempObj.secondaryEmails.count) {
                        cell.emailAdd.text = self.tempObj.secondaryEmails[indexPath.row - 1]
                    }
                }
            }
            
            if(self.isEditable == false) {
                cell.emailAdd.isUserInteractionEnabled = false
            } else {
                cell.emailAdd.isUserInteractionEnabled = true
            }
            
            cell.emailAdd.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            cell.emailAdd.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        } else {
            return cell
        }
      }

    private func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
            return .none
      }

      func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

            //       let sourceCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: sourceIndexPath) as! addContactPhoneNumberCell
        
            //      let destinationCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: destinationIndexPath) as! addContactPhoneNumberCell
        
            //      self.changePlaceHolder()
            //      let firstIndexPath : IndexPath = IndexPath(row : 0, section: 0)
            //      let secondIndexPath : IndexPath = IndexPath(row : 1, section: 0)
            //      let thirdIndexPath : IndexPath = IndexPath(row : 2, section: 0)
            //
            //       let firstCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: firstIndexPath) as! addContactPhoneNumberCell
            //      let secondCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: secondIndexPath) as! addContactPhoneNumberCell
            //      let thirdCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: thirdIndexPath) as! addContactPhoneNumberCell
        
            //      firstCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Main No.", attributes: placeholderAttr)
            //      secondCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Alternate No.", attributes: placeholderAttr)
            //      thirdCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Alternate No.", attributes: placeholderAttr)

            //       let cell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index) as! addContactPhoneNumberCell
            //      destinationCell = self.phoneNumberTableView.cellForRow(at: destinationIndexPath) as! addContactPhoneNumberCell
            //
            //      var sourceCell = addContactPhoneNumberCell()
            //      sourceCell = self.phoneNumberTableView.dequeueReusableCell(withIdentifier: "addContactPhoneNumberCell", for: sourceIndexPath) as!  addContactPhoneNumberCell
            //      if(destinationIndexPath.row == 0){
        
            //            sourceCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Main No.", attributes: placeholderAttr)
            //      }else{
        
            //            sourceCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Alternate No.", attributes: placeholderAttr)
            //      }
            //      destinationCell.phoneNumber.textAlignment = NSTextAlignment.left
            //
            //      if(sourceIndexPath.row == 0){
        
            //            destinationCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Main No.", attributes: placeholderAttr)
            //      }else{
        
            //            destinationCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Alternate No.", attributes: placeholderAttr)
            //      }
            //      sourceCell.phoneNumber.textAlignment = NSTextAlignment.left
            //      self.phoneNumberTableView.beginUpdates()
            //      self.phoneNumberTableView.endUpdates()
            
      }

      func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
            return false
      }

      //create the attribute dictionary
      let placeholderAttr = [
        NSAttributedString.Key.foregroundColor: UIColor.lightGray,
        NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Light", size: 22.0)!
            ]

      func changePlaceHolder() {
            let firstIndexPath: IndexPath = IndexPath(row: 0, section: 0)
            let secondIndexPath: IndexPath = IndexPath(row: 1, section: 0)
            let thirdIndexPath: IndexPath = IndexPath(row: 2, section: 0)

            let firstCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: firstIndexPath) as! addContactPhoneNumberCell
            let secondCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: secondIndexPath) as! addContactPhoneNumberCell
            let thirdCell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: thirdIndexPath) as! addContactPhoneNumberCell

            firstCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Main No.", attributes: placeholderAttr)
            secondCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Alternate No.", attributes: placeholderAttr)
            thirdCell.phoneNumber.attributedPlaceholder = NSAttributedString(string: "Alternate No.", attributes: placeholderAttr)
            self.phoneNumberTableView.beginUpdates()
            self.phoneNumberTableView.endUpdates()
      }

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.syncContactsLbl.text = self.syncText.syncContacts
                  self.firstTabLbl.text = self.syncText.personal
                  self.secondTabLbl.text = self.syncText.postal
                  self.thirdTabLbl.text = self.syncText.professional
                  self.fourthTabLbl.text = self.syncText.general
                  self.addPhoneLbl.text = self.syncText.addPhone
                  self.addEmailLbl.text = self.syncText.addEmail
                  self.showPrivacyMessages(privacySetForContact: self.addedContact.privacySetForContactUser)
                  self.nameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.name)
            }
      }
      /* END */

      func showPrivacyMessages(privacySetForContact: Bool) {
            if privacySetForContact {
                  self.PrivacyDetail.text = self.profilemsg.privacyenabled
                  self.privacyDetailTextView.text = self.profilemsg.privacydetailenabled
                  self.privacyMessage.text = self.profilemsg.privacymessagenotshared
            } else {
                  self.PrivacyDetail.text = self.profilemsg.privacydisabled
                  self.privacyDetailTextView.text = self.profilemsg.privacydetaildisable
                  self.privacyMessage.text = self.profilemsg.privacyinfoshared
            }
      }

      func changePrivacySetting() {
            DispatchQueue.main.async {
                self.startLoader()
            }
            let changePrivacy = ChangePrivacySetting()
            var fisheyeContacts: [ContactObj] =  []                                        // to store FE Contacts
            fisheyeContacts.append(self.addedContact)
            changePrivacy.contactObjs = fisheyeContacts
            changePrivacy.addDidFinishBlockObserver { [unowned self] (_, _) in
                  DispatchQueue.main.async {
                        NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                        self.stopLoader()
                        self.nextIcon.isUserInteractionEnabled = true
                        self.editButton.isHidden = false
                        self.nextIconImg.isHidden = true
                  }
            }
            AppDelegate.addProcedure(operation: changePrivacy)
      }
}
