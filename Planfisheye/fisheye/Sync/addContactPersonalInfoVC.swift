//
//  addContactPersonalInfoVC.swift
//  fisheye
//
//  Created by SankeyIosMac on 04/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
//import Toast_Swift

class addContactPersonalInfoVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

      // Outlet variable declaration
      @IBOutlet weak var emailTableView: UITableView!
      @IBOutlet weak var phoneNumberTableView: UITableView!
      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var nameTextField: UICustomTextField!
      @IBOutlet weak var scrollView: UIScrollView!
      @IBOutlet weak var nextIcon: UIButton!
      @IBOutlet weak var nameBlackUnderline: UIImageView!
      @IBOutlet weak var nameBlueUnderline: UIImageView!
      @IBOutlet weak var scrollOuterView: UIView!

      @IBOutlet weak var professionalLbl: UILabel!
      @IBOutlet weak var generalLbl: UILabel!
      @IBOutlet weak var postalLbl: UILabel!
      @IBOutlet weak var personalLbl: UILabel!
      @IBOutlet weak var addContactHeaderLbl: UILabel!
      @IBOutlet var buttonsStackView: UIStackView!
      @IBOutlet var tabsStackView: UIStackView!

      // variable declaration
      var addContactPostalInfoVC: addContactPostalInfoVC?
      var addContactProfesionalInfoVC: addContactProfesionalInfoVC!
      var addContactGeneralInfoVC: addContactGeneralInfoVC!
      let appSharedPrefernce = AppSharedPreference()
      let validationController = ValidationController()
      var counter: Int = 0
      var idtoken = ""
      var contactId = 0
      var addedContact = ContactObj()
      var currentCountryCode: String  = "+"
      let index = IndexPath(row: 0, section: 0)
      let index1 = IndexPath(row: 1, section: 0)
      let index2 = IndexPath(row: 2, section: 0)
      var tempObj = ContactObj()
      var isFailedToSave: Bool = false
      var userPhoneNumber: String = ""
      var userEmail: String = ""
      var userPhoneWithCountryCode: String = ""
      var phoneNumber: String = ""
      var email: String = ""
      var sharedData: Int = 2
      var profileObj: [String: Any]?
      var privacyEnabledForContacts: Bool = true
      var syncText = SyncToastMessages()
      var appService = AppService()

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            self.syncText = SyncToastMessages.shared
            self.applyViewShadow()
            self.validation()               // adding target for add target
            self.phoneNumberTableView.delegate = self
            self.phoneNumberTableView.dataSource = self
            self.emailTableView.delegate = self
            self.emailTableView.dataSource = self
            self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            if(self.contactId != 0) {
                  self.assignContactDetails()
            }
            self.currentCountryCode = getCurrentCountryCode()
            self.setLocalizationText()
            self.addObservers()
            self.nameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.name)
            self.profileObj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
            if let tempObj = self.profileObj {
                  self.privacyEnabledForContacts = tempObj["privacyEnabled"] as? Bool ?? true
            }
      }

      func assignContactDetails() {
            self.addedContact = DatabaseManagement.shared.getContact(contactId: self.contactId)
            self.nameTextField.text = self.addedContact.name
            self.phoneNumberTableView.reloadData()
            self.emailTableView.reloadData()
      }

      override func viewWillAppear(_ animated: Bool) {
            self.hideBlueUnderlines()
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.userPhoneNumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            self.userEmail = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
            let countryCode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
            self.userPhoneWithCountryCode =  countryCode + self.userPhoneNumber
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.addContactPersonalInfoVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      func hideBlueUnderlines() {
            self.nameBlueUnderline.isHidden = true
            self.nameBlackUnderline.isHidden = false
      }

      func validation() {
            self.nameTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            guard let cell = (self.phoneNumberTableView.cellForRow(at: self.index) as? addContactPhoneNumberCell) else {return}
            guard let phoneCell1: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index1) as? addContactPhoneNumberCell else {return}
            guard let phoneCell2: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index2) as? addContactPhoneNumberCell else {return}

            let cell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index) as! AddContactEmailCell
            let emailcell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index1) as! AddContactEmailCell
            let emailcell2: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index2) as! AddContactEmailCell

            switch textField {

            case  nameTextField:
                  self.nameBlueUnderline.isHidden = false
                  self.nameBlackUnderline.isHidden = true
                  if (nameTextField.text?.characters.count)! <= 0 {
                        self.nameTextField.configOnError(withPlaceHolder: self.syncText.enterName)
                  } else {
                        self.nameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.name)
                  }
                  break

            case cell.phoneNumber:
                  cell.phoneBlackUnderline.isHidden = true
                  cell.phoneBlueUnderline.isHidden = false
                  cell.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  break

            case phoneCell1.phoneNumber:
                  phoneCell1.phoneBlackUnderline.isHidden = true
                  phoneCell1.phoneBlueUnderline.isHidden = false
                  phoneCell1.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  break

            case phoneCell2.phoneNumber:
                  phoneCell2.phoneBlackUnderline.isHidden = true
                  phoneCell2.phoneBlueUnderline.isHidden = false
                  phoneCell2.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  break

            case cell1.emailAdd:
                  cell1.emailBlackUnderline.isHidden = true
                  cell1.emailBlueUnderline.isHidden = false
                  cell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  break

            case emailcell1.emailAdd:
                  emailcell1.emailBlackUnderline.isHidden = true
                  emailcell1.emailBlueUnderline.isHidden = false
                  emailcell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  break

            case emailcell2.emailAdd:
                  emailcell2.emailBlackUnderline.isHidden = true
                  emailcell2.emailBlueUnderline.isHidden = false
                  emailcell2.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  break

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {

            guard let cell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index) as? addContactPhoneNumberCell else {return}
            guard let phoneCell1: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index1) as? addContactPhoneNumberCell else {return}
            guard let phoneCell2: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index2) as? addContactPhoneNumberCell else {return}

            guard let cell1: AddContactEmailCell = (self.emailTableView.cellForRow(at: self.index) as? AddContactEmailCell) else {return}
            guard let emailcell1: AddContactEmailCell = (self.emailTableView.cellForRow(at: self.index1) as? AddContactEmailCell) else {return}
            guard let emailcell2: AddContactEmailCell = (self.emailTableView.cellForRow(at: self.index2) as? AddContactEmailCell) else {return}

            switch textField {

            case  nameTextField:
                  self.nameBlueUnderline.isHidden = true
                  self.nameBlackUnderline.isHidden = false
                  if (nameTextField.text?.characters.count)! <= 0 {
                        self.nameTextField.configOnError(withPlaceHolder: self.syncText.enterName)
                  } else {
                        self.nameTextField.configOnErrorCleared(withPlaceHolder: self.syncText.name)
                  }
                  break

            case cell.phoneNumber:
                  cell.phoneBlackUnderline.isHidden = false
                  cell.phoneBlueUnderline.isHidden = true
                  if (cell.phoneNumber.text?.characters.count)! > 0 && cell.phoneNumber.text != self.currentCountryCode && ((cell.phoneNumber.text?.characters.count)! < 6 || (cell.phoneNumber.text?.characters.count)! > 20) {
                        cell.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  } else {
                        cell.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  }
                  break

            case phoneCell1.phoneNumber:
                  phoneCell1.phoneBlackUnderline.isHidden = false
                  phoneCell1.phoneBlueUnderline.isHidden = true
                  if (phoneCell1.phoneNumber.text?.characters.count)! > 0 && phoneCell1.phoneNumber.text != self.currentCountryCode && ((phoneCell1.phoneNumber.text?.characters.count)! < 6 || (phoneCell1.phoneNumber.text?.characters.count)! > 20) {
                        phoneCell1.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  } else {
                        phoneCell1.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  }
                  break

            case phoneCell2.phoneNumber:
                  phoneCell2.phoneBlackUnderline.isHidden = false
                  phoneCell2.phoneBlueUnderline.isHidden = true
                  if (phoneCell2.phoneNumber.text?.characters.count)! > 0 && phoneCell2.phoneNumber.text != self.currentCountryCode && ((phoneCell2.phoneNumber.text?.characters.count)! < 6 || (phoneCell2.phoneNumber.text?.characters.count)! > 20) {
                        phoneCell2.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  } else {
                        phoneCell2.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  }
                  break

            case cell1.emailAdd:
                  cell1.emailBlackUnderline.isHidden = false
                  cell1.emailBlueUnderline.isHidden = true
                  if (cell1.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: cell1.emailAdd.text!) {
                        cell1.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  } else {
                        cell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  }
                  break

            case emailcell1.emailAdd:
                  emailcell1.emailBlackUnderline.isHidden = false
                  emailcell1.emailBlueUnderline.isHidden = true
                  if (emailcell1.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: emailcell1.emailAdd.text!) {
                        emailcell1.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  } else {
                        emailcell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  }
                  break

            case emailcell2.emailAdd:
                  emailcell2.emailBlackUnderline.isHidden = false
                  emailcell2.emailBlueUnderline.isHidden = true
                  if (emailcell2.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: emailcell2.emailAdd.text!) {
                        emailcell2.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  } else {
                        emailcell2.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  }
                  break

            default:
                  break
            }
      }

      func checkValidation() {
            self.addPersonalInfo()
      }

      /* Created by venkatesh modified by vaishali  */
      func applyViewShadow() {
            self.mainView.layer.cornerRadius = 8
            self.scrollOuterView.layer.cornerRadius = 8
            self.scrollView.layer.cornerRadius = 8
            self.emailTableView.layer.cornerRadius = 8
            // border
            self.mainView.layer.borderWidth = 0
            self.mainView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.mainView.layer.shadowColor = UIColor.black.cgColor
            self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainView.layer.shadowOpacity = 0.7
            self.mainView.layer.shadowRadius = 10.0
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.addContactHeaderLbl.text = self.syncText.addContact
                  self.personalLbl.text = self.syncText.personal
                  self.postalLbl.text = self.syncText.postal
                  self.professionalLbl.text = self.syncText.professional
                  self.generalLbl.text = self.syncText.general
            }
      }
      /* END */

      @IBAction func postalBtnTapped(_ sender: Any) {
            loadPostalInfoPage()
      }

      @IBAction func professionalBtnTapped(_ sender: Any) {
            loadProfessionalInfoPage()
      }

      @IBAction func generalBtnTapped(_ sender: Any) {
            loadGeneralInfoPage()
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      // back button click to navigate to sync contact list screen
      @IBAction func backBtnTapped(_ sender: Any) {
            loadSyncViewController()
      }

      // next icon tapped action to submit the details & navigate to postal info page
      @IBAction func nextIconTapped(_ sender: Any) {
            self.checkValidation()
      }

      // Add contact's Personal info submit
      func addPersonalInfo() {
            self.nextIcon.isUserInteractionEnabled = false
            var isValidationCorrect: Bool = true
            let cell: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index) as! addContactPhoneNumberCell
            let phoneCell1: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index1) as! addContactPhoneNumberCell
            let phoneCell2: addContactPhoneNumberCell = self.phoneNumberTableView.cellForRow(at: self.index2) as! addContactPhoneNumberCell

            let cell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index) as! AddContactEmailCell
            let emailcell1: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index1) as! AddContactEmailCell
            let emailcell2: AddContactEmailCell = self.emailTableView.cellForRow(at: self.index2) as! AddContactEmailCell

            if (nameTextField.text?.characters.count)! <= 0 {
                  self.nameTextField.configOnError(withPlaceHolder: self.syncText.enterName)
                  isValidationCorrect = false
            }

            if (cell.phoneNumber.text?.characters.count)! > 0 && cell.phoneNumber.text != self.currentCountryCode && ((cell.phoneNumber.text?.characters.count)! < 6 || (cell.phoneNumber.text?.characters.count)! > 20) {
                  cell.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  isValidationCorrect = false
            }
            if (phoneCell1.phoneNumber.text?.characters.count)! > 0 && phoneCell1.phoneNumber.text != self.currentCountryCode && ((phoneCell1.phoneNumber.text?.characters.count)! < 6 || (phoneCell1.phoneNumber.text?.characters.count)! > 20) {
                  phoneCell1.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  isValidationCorrect = false
            }
            if (phoneCell2.phoneNumber.text?.characters.count)! > 0 && phoneCell2.phoneNumber.text != self.currentCountryCode && ((phoneCell2.phoneNumber.text?.characters.count)! < 6 || (phoneCell2.phoneNumber.text?.characters.count)! > 20) {
                  phoneCell2.phoneNumber.configOnError(withPlaceHolder: self.syncText.invalidPhone)
                  isValidationCorrect = false
            }
            if (cell1.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: cell1.emailAdd.text!) {
                  cell1.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  isValidationCorrect = false
            }
            if (emailcell1.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: emailcell1.emailAdd.text!) {
                  emailcell1.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  isValidationCorrect = false
            }
            if (emailcell2.emailAdd.text?.characters.count)! > 0  && !validationController.isValidEmail(testStr: emailcell2.emailAdd.text!) {
                  emailcell2.emailAdd.configOnError(withPlaceHolder: self.syncText.invalidEmail)
                  isValidationCorrect = false
            }
            if isValidationCorrect {
                  cell.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  phoneCell1.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  phoneCell2.phoneNumber.configOnErrorCleared(withPlaceHolder: self.syncText.phone)
                  cell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  emailcell1.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)
                  emailcell2.emailAdd.configOnErrorCleared(withPlaceHolder: self.syncText.email)

                  var primaryNum: String = ""
                  var secondaryNum1: String = ""
                  var secondaryNum2: String = ""

                  var primaryMail: String = ""
                  var secondaryMail1: String = ""
                  var secondaryMail2: String = ""

                  var secondaryPhones: String = ""
                  var secondaryEmails: String = ""

                  if(cell.phoneNumber.text != self.currentCountryCode) {
                        primaryNum = cell.phoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  }

                  if(phoneCell1.phoneNumber.text != self.currentCountryCode) {
                        secondaryNum1 = phoneCell1.phoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  }

                  if(phoneCell2.phoneNumber.text != self.currentCountryCode) {
                        secondaryNum2 = phoneCell2.phoneNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  }

                  if(primaryNum.first == "0") {
                        primaryNum = String(primaryNum.dropFirst(1))
                  }

                  if(secondaryNum1.first == "0") {
                        secondaryNum1 = String(secondaryNum1.dropFirst(1))
                  }

                  if(secondaryNum2.first == "0") {
                        secondaryNum2 = String(secondaryNum2.dropFirst(1))
                  }

                  let name = nameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  primaryMail = cell1.emailAdd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  secondaryMail1 = emailcell1.emailAdd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  secondaryMail2 = emailcell2.emailAdd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

                  primaryMail = primaryMail.lowercased()
                  secondaryMail1 = secondaryMail1.lowercased()
                  secondaryMail2 = secondaryMail2.lowercased()

                  if(primaryNum == self.userPhoneNumber || primaryNum == self.userPhoneWithCountryCode || primaryMail == self.userEmail) {
                        self.view.makeToast(self.syncText.notAllowedToAddSelfPhoneAndEmailInSync)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                              self.view.hideToast()
                        }
                        self.nextIcon.isUserInteractionEnabled = true
                        return
                  }

                  self.tempObj = ContactObj()
                  self.tempObj.contactId = self.addedContact.contactId
                  self.tempObj.name = name
                  self.tempObj.phoneNumber = primaryNum
                  self.tempObj.secondaryPhoneNumbers.append(secondaryNum1)
                  self.tempObj.secondaryPhoneNumbers.append(secondaryNum2)
                  self.tempObj.email = primaryMail
                  self.tempObj.secondaryEmails.append(secondaryMail1)
                  self.tempObj.secondaryEmails.append(secondaryMail2)

                  if (primaryNum.count == 0) {
                        if (secondaryNum1.count != 0) {
                              primaryNum = secondaryNum1
                              phoneCell1.phoneNumber.text = ""
                              secondaryNum1 = ""
                        } else  if (secondaryNum2.count != 0) {
                              primaryNum = secondaryNum2
                              phoneCell2.phoneNumber.text = ""
                              secondaryNum2 = ""
                        }
                  }

                  if (secondaryNum1.count == 0) {
                        if (secondaryNum2.count != 0) {
                              secondaryNum1 = secondaryNum2
                              secondaryPhones = secondaryNum1
                              phoneCell2.phoneNumber.text = ""
                              secondaryNum2 = ""
                        }
                  } else {
                        secondaryPhones = secondaryNum1
                  }

                  if(secondaryNum2.count != 0) {
                        secondaryPhones = secondaryPhones + "," + secondaryNum2
                  }
                  //-----------------------------------
                  if (primaryMail.count == 0) {
                        if (secondaryMail1.count != 0) {
                              primaryMail = secondaryMail1
                              emailcell1.emailAdd.text = ""
                              secondaryMail1 = ""
                        } else  if (secondaryMail2.count != 0) {
                              primaryMail = secondaryMail2
                              emailcell2.emailAdd.text = ""
                              secondaryMail2 = ""
                        }
                  }

                  if (secondaryMail1.count == 0) {
                        if (secondaryMail2.count != 0) {
                              secondaryMail1 = secondaryMail2
                              secondaryEmails = secondaryMail1
                              emailcell2.emailAdd.text = ""
                              secondaryMail2 = ""
                        }
                  } else {
                        secondaryEmails = secondaryMail1
                  }

                  if(secondaryMail2.count != 0) {
                        secondaryEmails = secondaryEmails + "," + secondaryMail2
                  }

                  let primaryPhoneNumber: String = primaryNum
                  let primaryEmail: String = primaryMail

                  if(self.contactId != 0) {
                        var existingContacts: [ContactObj] = []
                        existingContacts  = DatabaseManagement.shared.getAlreadyExistsContactsFromInUseContacts(tempPhoneNumber: primaryNum, tempEmail: primaryMail)

                        self.addedContact.name = name
                        self.addedContact.stringSecondaryPhones = secondaryPhones
                        self.addedContact.stringSecondaryEmails = secondaryEmails
                        self.addedContact.phoneNumber = primaryNum
                        self.addedContact.email = primaryMail
                        self.addedContact.isBackendSync = false

                        if (self.addedContact.sharedData == 0 && primaryNum  != self.phoneNumber) {
                              self.addedContact.sharedData = 2
                        } else if( self.addedContact.sharedData == 1 &&  primaryMail != self.email) {
                              self.addedContact.sharedData = 2
                        }

                        if(primaryNum == "") {
                              self.addedContact.sharedData = 0
                              self.sharedData = 0
                        } else if(primaryMail == "") {
                              self.addedContact.sharedData = 1
                              self.sharedData = 1
                        }

                        if(self.addedContact.phoneNumber == "") {
                              self.addedContact.sharedData = 0
                        } else if(self.addedContact.email == "") {
                              self.addedContact.sharedData = 1
                        }

                        if(existingContacts.count == 0 ) {
                              self.updateInSQLiteDatabase()
                              self.phoneNumberTableView.setEditing(false, animated: false)
                              self.emailTableView.setEditing(false, animated: false)
                        } else if(existingContacts.count == 1 && existingContacts[0].contactId == self.addedContact.contactId) {
                              self.updateInSQLiteDatabase()
                              self.phoneNumberTableView.setEditing(false, animated: false)
                              self.emailTableView.setEditing(false, animated: false)
                        } else {
                              self.nextIcon.isUserInteractionEnabled = true
                              self.isFailedToSave = true
                              self.phoneNumberTableView.reloadData()
                              self.emailTableView.reloadData()
                              self.view.makeToast(self.syncText.contactAlreadyExistsByPhoneOrEmail)
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update Contact", action: "Update personal information clicked", label: "Contact already exists by Phone Number or Email id", value: 0)
                        }
                  } else {
                        var contactObj = ContactObj()
                        contactObj.sharedData = self.sharedData
                        contactObj.contactId = getTime()
                        contactObj.name = name
                        contactObj.stringSecondaryPhones = secondaryPhones
                        contactObj.stringSecondaryEmails = secondaryEmails
                        contactObj.userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as! String
                        contactObj.phoneNumber = primaryPhoneNumber
                        contactObj.email = primaryEmail
                        contactObj.isdeleted = false
                        contactObj.ismerged = false
                        contactObj.companyEmail = ""
                        contactObj.title = ""
                        contactObj.company = ""
                        contactObj.source = "addedInFESync"
                        contactObj.picture = ""
                        contactObj.website = ""
                        contactObj.notes = ""
                        contactObj.privacyEnabled = true
                        contactObj.privacySetForContactUser = self.privacyEnabledForContacts
                        contactObj.contactFEId = ""
                        contactObj.isBackendSync = false
                        contactObj.mergeParentId = ""
                        contactObj.picture = ""
                        contactObj.isBlocked = false
                        contactObj.isBlockedByContact = false
                        contactObj.createdAt = getCurrentTImeStamp()
                        contactObj.updatedAt = getCurrentTImeStamp()

                        if(contactObj.phoneNumber == "") {
                              contactObj.sharedData = 0
                        } else if(contactObj.email == "") {
                              contactObj.sharedData = 1
                        }

                        let existingContacts: [ContactObj] = DatabaseManagement.shared.getAlreadyExistsContactsFromInUseContacts(tempPhoneNumber: contactObj.phoneNumber, tempEmail: contactObj.email)
                        if(existingContacts.count == 0) {
                              let isOperationSuccessfullOnMainTable = DatabaseManagement.shared.insertNewContactInSQLiteStorage(contactObj: contactObj)
                              let isOperationSuccessfullOnAnotherTable = DatabaseManagement.shared.insertNewContactInRecentlyAdded(contactObj: contactObj)
                              if(isOperationSuccessfullOnMainTable && isOperationSuccessfullOnAnotherTable) {
                                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "addContactInProgress", value: contactObj.contactId)
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "contactObjToOpen", value: 0)
                                    ContactsSync.shared.getNonSyncedContacts()
                                    ContactsSync.shared.backendSync()
                                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                                    loadSyncViewController()
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add contact", action: "Submit contact's personal information", label: "Add contact's personal information successfully", value: 0)
                              } else {
                                    self.nextIcon.isUserInteractionEnabled = true
                                self.view.makeToast(self.syncText.failedToAddContact, duration: self.appService.secondTimeInterval, position: .bottom)
                              }
                        } else {
                              self.nextIcon.isUserInteractionEnabled = true
                              self.view.makeToast(SyncToastMessages.shared.contactAlreadyExists)
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add contact", action: "Submit contact's general information", label: "Contact already exists by Phone Number or Email id", value: 0)
                        }
                  }
            } else {
                  self.nextIcon.isUserInteractionEnabled = true
            }
      }

      func updateInSQLiteDatabase() {
            let isOperationSuccessfull = DatabaseManagement.shared.updatePersonalInfo(contactObj: self.addedContact)
            DatabaseManagement.shared.updatePersonalInfoInRecentlyAdded(contactObj: self.addedContact)
            if(isOperationSuccessfull) {
                  self.appSharedPrefernce.setAppSharedPreferences(key: "contactObjToOpen", value: self.addedContact.contactId)
                  self.appSharedPrefernce.setAppSharedPreferences(key: "addContactInProgress", value: 0)
                  self.addedContact = DatabaseManagement.shared.getContact(contactId: self.addedContact.contactId)
                  self.phoneNumberTableView.reloadData()
                  self.emailTableView.reloadData()
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
                  loadSyncViewController()
                  NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update Contact", action: "Update personal information clicked", label: "Update personal information successful", value: 0)
                  self.phoneNumber = self.addedContact.phoneNumber
                  self.email = self.addedContact.email
            } else {
                  DatabaseManagement.shared.deleteRecentlyAddedSinglecontact(contact: self.addedContact)
                  self.nextIcon.isUserInteractionEnabled = true
                self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Update Contact", action: "Update personal information clicked", label: "Failed to update contact's personal information", value: 0)
            }
      }

      // navigate to add contact's postal info page
      func navigateToPostal() {
            self.appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactPostalInfoPage")
            NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
      }

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
      }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = UITableViewCell()

            if tableView == self.phoneNumberTableView {
                  var cell = addContactPhoneNumberCell()
                  cell = self.phoneNumberTableView.dequeueReusableCell(withIdentifier: "addContactPhoneNumberCell", for: indexPath) as!  addContactPhoneNumberCell
                  cell.phoneNumber.placeholder = self.syncText.phone
                  if(indexPath.row == 0) {
                        cell.phoneNumber.text = self.addedContact.phoneNumber
                        if (cell.phoneNumber.text == "") {
                              cell.phoneNumber.text = self.currentCountryCode
                        }
                  } else if(indexPath.row == 1) {
                        cell.phoneNumber.text = ""
                        if(self.addedContact.secondaryPhoneNumbers.count > 0) {
                              cell.phoneNumber.text = self.addedContact.secondaryPhoneNumbers[0]
                        }
                        if (cell.phoneNumber.text == "") {
                              cell.phoneNumber.text = self.currentCountryCode
                        }
                  } else if(indexPath.row == 2) {
                        if(self.addedContact.secondaryPhoneNumbers.count > 1) {
                              cell.phoneNumber.text = ""
                              cell.phoneNumber.text = self.addedContact.secondaryPhoneNumbers[1]
                        }
                        if (cell.phoneNumber.text == "") {
                              cell.phoneNumber.text = self.currentCountryCode
                        }
                  }
                  cell.phoneNumber.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
                  cell.phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                  return cell
            } else if(tableView == self.emailTableView ) {
                  var cell = AddContactEmailCell()
                  cell = self.emailTableView.dequeueReusableCell(withIdentifier: "AddContactEmailCell", for: indexPath) as!  AddContactEmailCell
                  cell.emailAdd.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
                  cell.emailAdd.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
                  cell.emailAdd.placeholder = self.syncText.email
                  if(!self.isFailedToSave) {
                        if(indexPath.row == 0) {
                              cell.emailAdd.text = self.tempObj.email
                        } else if(indexPath.row == 1) {
                              cell.emailAdd.text = ""
                              if(self.tempObj.secondaryEmails.count > 0) {
                                    cell.emailAdd.text = self.tempObj.secondaryEmails[0]
                              }
                        } else if(indexPath.row == 2) {
                              if(self.tempObj.secondaryEmails.count > 1) {
                                    cell.emailAdd.text = ""
                                    cell.emailAdd.text = self.tempObj.secondaryEmails[1]
                              }
                        }
                  } else {
                        if(indexPath.row == 0) {
                              cell.emailAdd.text = self.tempObj.email
                        } else if(indexPath.row == 1) {
                              cell.emailAdd.text = ""
                              if(self.tempObj.secondaryEmails.count > 0) {
                                    cell.emailAdd.text = self.tempObj.secondaryEmails[0]
                              }
                        } else if(indexPath.row == 2) {
                              if(self.tempObj.secondaryEmails.count > 1) {
                                    cell.emailAdd.text = ""
                                    cell.emailAdd.text = self.tempObj.secondaryEmails[1]
                              }
                        }
                  }
                  return cell
            } else {
                  return cell
            }
      }
}
