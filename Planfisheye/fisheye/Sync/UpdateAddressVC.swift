//
//  UpdateAddressVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 17/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class UpdateAddressVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var postalCodeTextField: UICustomTextField!
    @IBOutlet weak var addBtn: UIButton!

    @IBOutlet weak var mapImageView: UIButton!
    @IBOutlet weak var countryTextField: UICustomTextField!
    @IBOutlet weak var stateTextField: UICustomTextField!
    @IBOutlet weak var cityTextField: UICustomTextField!
    @IBOutlet weak var addressLine2: UICustomTextField!
    @IBOutlet weak var addressLine1: UICustomTextField!
    @IBOutlet weak var addressTypeTxtField: UICustomTextField!
    @IBOutlet weak var submitBtn: UIButton!

    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var idtoken = ""
    var contactId = ""

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var typeArray: [String]!
    let appSharedPrefernce =  AppSharedPreference()
    var editMode: Bool = false

    var address: FEAddress?
    var addressNew = AddressList()
    var addressId: String?
    var selectedPlaceMark: MKPlacemark?
    var reloadProtocol: ReloadProtocol?
    var updateAddressDelegate: UpdateAddress?

    //    @IBOutlet weak var searchMapLbl: UILabel!

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.typeArray.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.addressTypeTxtField.text = self.typeArray[indexPath.row]
        self.tableView.isHidden = true
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = self.typeArray[indexPath.row]
        return cell
    }
    @IBOutlet weak var tableView: UITableView!
    @IBAction func tableViewDropDown(_ sender: Any) {
        self.tableView.isHidden = false
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        if editMode {
            self.addressId = addressNew.addressId
            addressLine1.text = addressNew.formatted
            addressLine2.text = addressNew.street_address
            cityTextField.text =  addressNew.locality
            stateTextField.text = addressNew.region
            postalCodeTextField.text = addressNew.postal_code
            countryTextField.text = addressNew.country
            addressTypeTxtField.text = addressNew.type
            print("addressId is ", self.addressId)
        }
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        // Do any additional setup after loading the view.
        self.addressTypeTxtField.textColor = UIColor.black
        self.tableView.isHidden = true
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.typeArray = ["Home", "Work", "Mobile"]

        self.tableView.delegate = self as UITableViewDelegate
        self.tableView.dataSource = self as UITableViewDataSource
        self.addressTypeTxtField.delegate = self as? UITextFieldDelegate

        self.containerView.layer.borderColor = UIColor.duckEggBlue().cgColor

        self.containerView.layer.borderWidth = 1.0

        self.containerView.layer.cornerRadius = 5

        self.containerView.layer.masksToBounds = true

        //        self.addressLine1.textContainerInset = .zero
        //
        //        self.addressLine1.delegate = self

        //        self.addressLine1.textContainer.lineFragmentPadding = 0.0

        // Turns out we still have 5px of extra padding on the left of text view. Code below gets rid of it too.
        //        self.addressLine1.contentInset = UIEdgeInsetsMake(0, -5, 0, 0)

        //        self.addBtn.layer.cornerRadius = 0.09467391304*self.addBtn.bounds.width
        //
        //        self.addBtn.setTitleColor(UIColor.white, for: .normal)

        let tap = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))

        tap.numberOfTapsRequired = 1

        let tap2 = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))

        tap2.numberOfTapsRequired = 1

        mapImageView.isUserInteractionEnabled = true

        //        searchMapLbl.isUserInteractionEnabled = true

        mapImageView.addGestureRecognizer(tap2)

        //        searchMapLbl.addGestureRecognizer(tap)

        //        self.tableView.reloadData()
        if address != nil {

            self.addressLine1.text = address?.formatted
            self.addressLine2.text = address?.streetAddress
            self.cityTextField.text = address?.locality
            self.stateTextField.text = address?.region
            self.postalCodeTextField.text = address?.postalCode
            self.countryTextField.text = address?.country
            self.addressTypeTxtField.text = address?.type

            if(address?.addressId != nil) {
                self.addressId = address?.addressId
            }
        }

        self.addressLine1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.cityTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.stateTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.postalCodeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.countryTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        self.addressLine1.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.cityTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.stateTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.postalCodeTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.countryTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        //        self.addressTypeTxtField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
    }

    override func viewWillAppear(_ animated: Bool) {

        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactContactId") as? String ?? ""

    }
    func textFieldDidChange(_ textField: UICustomTextField) {

        switch textField {

        case  addressLine1:
            if (addressLine1.text?.characters.count)! <= 0 {
                self.addressLine1.configOnError(withPlaceHolder: "Enter Addressline 1")

            } else {
                self.addressLine1.configOnErrorCleared(withPlaceHolder: "Addressline 1")
            }

        case cityTextField:
            print("*************4*************")
            if (cityTextField.text?.characters.count)! <= 0 {
                print("")
                self.cityTextField.configOnError(withPlaceHolder: "Enter City")

            } else {
                print("*************5*************")
                self.cityTextField.configOnErrorCleared(withPlaceHolder: "City")
            }

        case stateTextField:

            print("*************6*************")
            if (stateTextField.text?.characters.count)! <= 0 {
                print("*************1*************")
                self.stateTextField.configOnError(withPlaceHolder: "Enter State")

            } else {
                self.stateTextField.configOnErrorCleared(withPlaceHolder: "State")
            }

        case postalCodeTextField:
            if (postalCodeTextField.text?.characters.count)! <= 0 {
                self.postalCodeTextField.configOnError(withPlaceHolder: "Enter Pin Code")

            } else {
                print("*************9*************")
                self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: "Pin Code")
            }

        case countryTextField:
            print("*************10*************")
            if (countryTextField.text?.characters.count)! <= 0 {
                print("*************1*************")
                self.countryTextField.configOnError(withPlaceHolder: "Enter Country")

            } else {
                print("*************11*************")
                self.countryTextField.configOnErrorCleared(withPlaceHolder: "Country")
            }

            //        case addressTypeTxtField:
            //            print("*************12*************")
            //            if (addressTypeTxtField.text?.characters.count)! <= 0   {
            //print("*************13*************")
            //                self.addressTypeTxtField.configOnError(withPlaceHolder: "Enter Address Type")
            //
            //            }
            //            else{
            //            print("*************14*************")
            //                self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: "Address Type")
            //            }

        default:
            break
        }
    }

    func textFieldDidEnd(_ textField: UICustomTextField) {
        print("Inside validation")
        switch textField {

        case  addressLine1:
            print("*************1*************")
            if (addressLine1.text?.characters.count)! <= 0 {
                print("*************2*************")
                self.addressLine1.configOnError(withPlaceHolder: "Enter Addressline 1")

            } else {
                print("*************3*************")
                self.addressLine1.configOnErrorCleared(withPlaceHolder: "Addressline 1")
            }

        case cityTextField:
            print("*************4*************")
            if (cityTextField.text?.characters.count)! <= 0 {
                print("")
                self.cityTextField.configOnError(withPlaceHolder: "Enter City")

            } else {
                print("*************5*************")
                self.cityTextField.configOnErrorCleared(withPlaceHolder: "City")
            }

        case stateTextField:

            print("*************6*************")
            if (stateTextField.text?.characters.count)! <= 0 {
                print("*************1*************")
                self.stateTextField.configOnError(withPlaceHolder: "Enter State")

            } else {
                print("*************7*************")
                self.stateTextField.configOnErrorCleared(withPlaceHolder: "State")
            }

        case postalCodeTextField:
            print("*************8*************")
            if (postalCodeTextField.text?.characters.count)! <= 0 {
                print("*************1*************")
                self.postalCodeTextField.configOnError(withPlaceHolder: "Enter Pin Code")

            } else {
                print("*************9*************")
                self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: "Pin Code")
            }

        case countryTextField:
            print("*************10*************")
            if (countryTextField.text?.characters.count)! <= 0 {
                print("*************1*************")
                self.countryTextField.configOnError(withPlaceHolder: "Enter Country")

            } else {
                print("*************11*************")
                self.countryTextField.configOnErrorCleared(withPlaceHolder: "Country")
            }

            //        case addressTypeTxtField:
            //            print("*************12*************")
            //            if (addressTypeTxtField.text?.characters.count)! <= 0   {
            //print("*************13*************")
            //                self.addressTypeTxtField.configOnError(withPlaceHolder: "Enter Address Type")
            //
            //            }
            //            else{
            //            print("*************14*************")
            //                self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: "Address Type")
        //            }
        default:
            break
        }
    }

    @IBAction func closeBtnTapped(_ sender: Any) {
        self.didMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()

        self.dismiss(animated: true, completion: nil)

    }

    func startLoader() {
        activityIndicator.center=self.view.center
        activityIndicator.hidesWhenStopped=true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)

        activityIndicator.startAnimating()
    }

    func stopLoader() {

        activityIndicator.stopAnimating()
    }

    func removeLayer() {
        let overlay = containerView.viewWithTag(100)
        overlay?.removeFromSuperview()
    }

    @IBAction func submitTapped(_ sender: Any) {

        if (addressLine1.text?.characters.count)! <= 0 {

            self.addressLine1.configOnError(withPlaceHolder: "Enter Addressline 1")
            return
        } else if  (self.cityTextField.text?.characters.count)! <= 0 {

            self.cityTextField.configOnError(withPlaceHolder: "Enter City")
            return
        } else if  (self.stateTextField.text?.characters.count)! <= 0 {

            self.stateTextField.configOnError(withPlaceHolder: "Enter State")
            return
        } else if  (self.postalCodeTextField.text?.characters.count)! <= 0 {

            self.postalCodeTextField.configOnError(withPlaceHolder: "Enter Pin Code")
            return
        } else if (self.countryTextField.text?.characters.count)! <= 0 {
            self.countryTextField.configOnError(withPlaceHolder: "Enter Country")
            return

        } else {
            if editMode {
                self.editAddress()
            } else {
                self.saveAddress()
            }
        }

    }

    func saveAddress() {
        self.startLoader()
        print("save function")

        let formatted = self.addressLine1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let streetAddress = self.addressLine2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let locality = self.cityTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let region = self.stateTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let postalCode = self.postalCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let country = self.countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let type = self.addressTypeTxtField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        let headers: HTTPHeaders =
            [

                "Content-Type": "application/json",
                "Accept": "application/json"

        ]
        print("Headers", headers)

        let parameters: Parameters =
            [
                "action": "addPostalInfo",
                "idtoken": self.idtoken,
                "contact": [
                    "contactId": self.contactId,
                    "address": [
                        [
                            "formatted": formatted,
                            "street_address": streetAddress,
                            "locality": locality,
                            "region": region,
                            "postal_code": postalCode,
                            "country": country,
                            "type": type
                        ]
                    ]
                ]
        ]

        var awsURL = "https://60sru49x84.execute-api.eu-west-2.amazonaws.com/dev"
        var endpoints = "/contact/addpostalinfo"

        Alamofire.request(awsURL+endpoints, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                self.stopLoader()
                switch response.result {
                case .success:
                    let data = JSON(response.result.value!)
                    print("success in saving address  ", data)
                    kSweetAlert.showAlert("Successful", subTitle: "Address Added Successfully", style: AlertStyle.none)
                    self.dismiss(animated: true, completion: nil)

                case .failure(let error):
                    print("errorrr in saving address")
                    kSweetAlert.showAlert("Failure", subTitle: "Address not added Successfully", style: AlertStyle.none)

                }
        }
    }

    func editAddress() {
        self.startLoader()
        print("edit function")

        let formatted = self.addressLine1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let streetAddress = self.addressLine2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let locality = self.cityTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let region = self.stateTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let postalCode = self.postalCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let country = self.countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let type = self.addressTypeTxtField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        let headers: HTTPHeaders =
            [

                "Content-Type": "application/json",
                "Accept": "application/json"

        ]
        print("Headers", headers)

        let parameters: Parameters =
            [

                "action": "addPostalInfo",
                "idtoken": self.idtoken,
                "contact": [
                    "contactId": self.contactId,
                    "address":
                        [
                            "formatted": formatted,
                            "street_address": streetAddress,
                            "locality": locality,
                            "region": region,
                            "postalCode": postalCode,
                            "country": country,
                            "type": type
                    ]

                ]
        ]
        print("Parameters", parameters)
        print("*********parameters**********", parameters)
        var awsURL = "https://60sru49x84.execute-api.eu-west-2.amazonaws.com/dev"
        var endpoints = "/contact/addpostalinfo"

        Alamofire.request(awsURL+endpoints, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                self.stopLoader()
                switch response.result {
                case .success:
                    let data = JSON(response.result.value!)
                    print("success in saving address  ", data)

                    self.reloadProtocol?.reloadData(toReload: true)
                    self.dismiss(animated: true, completion: nil)

                case .failure(let error):
                    print("errorrr in saving address")
                    kSweetAlert.showAlert("Failure", subTitle: "Address not added Successfully", style: AlertStyle.none)

                }
        }
    }

    // MARK: Textfield Delegates

    func textFieldDidBeginEditing(_ textField: UITextField) {

        switch textField {

        case addressTypeTxtField:
            self.addressTypeTxtField.configOnFirstResponder(withPlaceHolder: "Select/Enter Address Type")

        default:
            if textField.isKind(of: UICustomUnderLineTextField.self) {
                let txtField = textField as! UICustomUnderLineTextField
                txtField.configOnFirstResponder()
            }
            if textField.tag == 0 {
                textField.text = ""
                textField.textColor = UIColor.black
            }
            break
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        if textField.isKind(of: UICustomUnderLineTextField.self) {
            let txtField = textField as! UICustomUnderLineTextField
            txtField.configOnResignResponder()
        }

    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        let tag = textField.tag + 1
        if let nextTextField = self.view.viewWithTag(tag) {
            if nextTextField.isKind(of: UICustomUnderLineTextField.self) {
                nextTextField.becomeFirstResponder()
            }
        }

        return true
    }

    func navigateToMaps() {

        let mapVC = MapViewController.instantiateFromStoryboardWithIdentifier(identifier: "MapViewController")

        mapVC.addressSearchDelegate = self as! AddressSearchNew

        let navController = UINavigationController(rootViewController: mapVC)

        self.present(navController, animated: true, completion: nil)
        //navigationController?.present(navController, animated: true, completion: nil)

    }

}

//extension AddressEditingPopUpVC:UITextViewDelegate{
//
//    func textViewDidBeginEditing(_ textView: UITextView){
//        if textView.isKind(of: UICustomUnderlinedText.self){
//            let txtField = textView as! UICustomUnderlinedText
//            txtField.configOnFirstResponder()
//        }
//    }
//
//
//    func textViewDidEndEditing(_ textView: UITextView){
//        if textView.isKind(of: UICustomUnderlinedText.self){
//            let txtField = textView as! UICustomUnderlinedText
//            txtField.configOnResignResponder()
//        }
//    }
//}

//extension AddressEditingPopUpVC: UITableViewDelegate,UITableViewDataSource{
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return typeArray != nil ?  typeArray.count : 0
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = self.tableView.dequeueReusableCell(withIdentifier: "secQuesCell", for: indexPath)
//
//        cell.textLabel?.text = typeArray[indexPath.row]
//
//
//        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showAddressEditingPopUpView))
//        //
//        //        cell.editImageView.isUserInteractionEnabled = true
//        //
//        //        cell.editImageView.addGestureRecognizer(tapGesture)
//
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        print("de select")
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("select")
//
//        self.removeLayer()
//        UIView.animate(withDuration: 1) {
//            self.tableHeightConstraint.constant = 0
//        }
//
//        self.addressTypeTxtField.text = self.typeArray[indexPath.row]
//        self.addressTypeTxtField.configOnFirstResponder(withPlaceHolder: "Select/Enter Address Type")
//
//    }
//}
//
//

//extension AdrressAddEditPopUpVC: AddressSearchNew {
//    
//    func addrssObjectFrom(place: MKPlacemark?) {
//        
//        if let placemark = place{
//            selectedPlaceMark = placemark
//            
//            if let name = placemark.name{
//                self.addressLine1.text = "\(name)"
//            }
//            if let thoroughfare = placemark.thoroughfare{
//                self.addressLine2.text="\(thoroughfare)"
//            }
//            if let loc = placemark.locality{
//                self.cityTextField.text = "\(loc)"
//            }
//            if let postalCode = placemark.postalCode{
//                self.postalCodeTextField.text = "\(postalCode)"
//            }
//            if let state = placemark.administrativeArea{
//                self.stateTextField.text = "\(state)"
//            }
//            if let country = placemark.country{
//                self.countryTextField.text = "\(country)"
//            }
//            
//        }else{
//            selectedPlaceMark = nil
//        }
//    }

//}
