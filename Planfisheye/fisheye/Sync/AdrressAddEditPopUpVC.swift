//
//  AdrressAddEditPopUpVC.swift
//  fisheye
//
//  Created by SankeyIosMac on 08/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit
import DropDown
import Toast_Swift

class AdrressAddEditPopUpVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

      @IBOutlet weak var containerView: UIView!
      @IBOutlet weak var postalCodeTextField: UICustomTextField!
      @IBOutlet weak var addBtn: UIButton!
      
      @IBOutlet weak var mapImageView: UIButton!
      @IBOutlet weak var countryTextField: UICustomTextField!
      @IBOutlet weak var stateTextField: UICustomTextField!
      @IBOutlet weak var cityTextField: UICustomTextField!
      @IBOutlet weak var addressLine2: UICustomTextField!
      @IBOutlet weak var addressLine1: UICustomTextField!
      @IBOutlet weak var addressTypeTxtField: UICustomTextField!
      @IBOutlet weak var submitBtn: UIButton!

      var idtoken = ""
      var contactId: Int = 0
      var contactObj = ContactObj()
      var noOfAddressToEdit: Int = 0
      var latitude = 0.0
      var longitude = 0.0
      var syncText = SyncToastMessages.shared
      var appService = AppService.shared

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      var typeArray: [String] = AppService.shared.addressTypes
      //      var englishTypeArray : [String] = []
      let appSharedPrefernce =  AppSharedPreference()
      let addressTypeDropdown: DropDown = DropDown()
      var editMode: Bool = false

      var address: FEAddress?
      var addressNew = AddressType()
      var addressId: String?
      var selectedPlaceMark: MKPlacemark?
      var selectedAddressTypeIndex: Int = 0
      var selectedText: String!

      public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.typeArray.count
      }

      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.addressTypeTxtField.text = self.typeArray[indexPath.row]
            self.tableView.isHidden = true
            self.selectedAddressTypeIndex = indexPath.row
      
//            let objArray = self.appService.localAddressType.filter( { return ($0.addressTranslatedText == self.addressTypeTxtField.text) } )
//            if objArray != nil && objArray.count > 0
//            {
//                self.selectedText = objArray[0].addressActualText
//            }else{
//                self.selectedText = self.appService.localAddressType[0].addressActualText
//            }
      }
      
      public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = self.typeArray[indexPath.row]

            cell.textLabel?.isUserInteractionEnabled = false
            return cell
      }
      @IBOutlet weak var tableView: UITableView!
      @IBAction func tableViewDropDown(_ sender: Any) {
            if  addressTypeDropdown.isHidden {
                  self.addressTypeDropdown.show()
            } else {
                  self.addressTypeDropdown.hide()
            }
      }

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            
            self.addObservers()
            self.setLocalizationText()
            
            self.appService = AppService.shared
            self.syncText = SyncToastMessages.shared
            self.typeArray = AppService.shared.addressTypes
            //            self.englishTypeArray = self.typeArray
            self.contactId  = self.appSharedPrefernce.getAppSharedPreferences(key: "contactObjToOpen") as? Int ?? 0
            if(self.contactId == 0) {
                  self.contactId = self.appSharedPrefernce.getAppSharedPreferences(key: "addContactInProgress") as? Int ?? 0
            }
            self.contactObj = DatabaseManagement.shared.getContact(contactId: self.contactId)
            if editMode {
                
                  if(self.noOfAddressToEdit == 0) {
                        addressTypeTxtField.text = self.contactObj.addressStringArray1[0]
                        
                        let objArray = self.appService.localAddressType.filter( { return ($0.addressActualText == self.contactObj.addressStringArray1[0]) } )
                        self.selectedText = self.contactObj.addressStringArray1[0]
                        if objArray != nil && objArray.count > 0
                        {
                             addressTypeTxtField.text = objArray[0].addressTranslatedText
                        }else{
                              addressTypeTxtField.text = self.contactObj.addressStringArray1[0]
                        }
                        addressLine1.text = self.contactObj.addressStringArray1[1]
                        addressLine2.text = self.contactObj.addressStringArray1[2]
                        cityTextField.text =  self.contactObj.addressStringArray1[3]
                        stateTextField.text = self.contactObj.addressStringArray1[4]
                        countryTextField.text = self.contactObj.addressStringArray1[5]
                        postalCodeTextField.text = self.contactObj.addressStringArray1[6]

                  } else {
                        addressTypeTxtField.text = self.contactObj.addressStringArray2[0]
                        
                        let objArray = self.appService.localAddressType.filter( { return ($0.addressActualText == self.contactObj.addressStringArray2[0]) } )
                        self.selectedText = self.contactObj.addressStringArray2[0]
                        if objArray != nil && objArray.count > 0
                        {
                              addressTypeTxtField.text = objArray[0].addressTranslatedText
                             

                        }else{
                              addressTypeTxtField.text = self.contactObj.addressStringArray2[0]
                        }
 
                        addressLine1.text = self.contactObj.addressStringArray2[1]
                        addressLine2.text = self.contactObj.addressStringArray2[2]
                        cityTextField.text =  self.contactObj.addressStringArray2[3]
                        stateTextField.text = self.contactObj.addressStringArray2[4]
                        countryTextField.text = self.contactObj.addressStringArray2[5]
                        postalCodeTextField.text = self.contactObj.addressStringArray2[6]

                  }
                  self.applyTableViewShadow()

            }else{
                  self.selectedText = self.appService.addressTypeArray[0].addressActualText
            }
            
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            // Do any additional setup after loading the view.
            self.addressTypeTxtField.textColor = UIColor.black
            self.tableView.isHidden = true
            self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.delegate = self as UITableViewDelegate
            self.tableView.dataSource = self as UITableViewDataSource
            self.addressTypeTxtField.delegate = self as? UITextFieldDelegate

            self.containerView.layer.borderColor = UIColor.duckEggBlue().cgColor

            self.containerView.layer.borderWidth = 1.0

            self.containerView.layer.cornerRadius = 5

            self.containerView.layer.masksToBounds = true

            self.addressTypeTxtField.isUserInteractionEnabled = false

            let tap = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))

            tap.numberOfTapsRequired = 1

            let tap2 = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))

            tap2.numberOfTapsRequired = 1

            mapImageView.isUserInteractionEnabled = true

            mapImageView.addGestureRecognizer(tap2)

            if address != nil {

                  self.addressLine1.text = address?.formatted
                  self.addressLine2.text = address?.streetAddress
                  self.cityTextField.text = address?.locality
                  self.stateTextField.text = address?.region
                  self.postalCodeTextField.text = address?.postalCode
                  self.countryTextField.text = address?.country
                  self.addressTypeTxtField.text = address?.type

                  if(address?.addressId != nil) {
                        self.addressId = address?.addressId
                  }
            }

            self.addressTypeTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.addressLine1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.cityTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.stateTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.postalCodeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.countryTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

            self.addressTypeTxtField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.addressLine1.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.cityTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.stateTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.postalCodeTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.countryTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            
            addressTypeDropdown.anchorView = tableView
            addressTypeDropdown.dataSource = typeArray
            self.addressTypeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                  self.addressTypeTxtField.text = item
                  self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: self.syncText.addressType)
                  self.selectedAddressTypeIndex = index
                  
                  let objArray = self.appService.localAddressType.filter( { return ($0.addressTranslatedText == item ) } )
                  
                  if objArray != nil && objArray.count > 0
                  {
                        self.addressTypeTxtField.text = objArray[0].addressTranslatedText
                        self.selectedText = objArray[0].addressActualText
                  }else {
                        self.addressTypeTxtField.text = self.appService.addressTypeArray[0].addressTranslatedText
                        self.selectedText =  self.appService.addressTypeArray[0].addressActualText
                  }
                  
            }

           
      }

    @objc func keyboardWillAppear() {
            self.tableView.isHidden = true
      }

      override func viewWillAppear(_ animated: Bool) {

            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.AdrressAddEditPopUpVC)
      }

      /* Created by venkatesh modified by vaishali  */
      func applyTableViewShadow() {
            self.tableView.layer.cornerRadius = 8
            // border
            self.tableView.layer.borderWidth = 0
            self.tableView.layer.borderColor = UIColor.black.cgColor

            // shadow
            self.tableView.layer.shadowColor = UIColor.black.cgColor
            self.tableView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.tableView.layer.shadowOpacity = 0.7
            self.tableView.layer.shadowRadius = 10.0
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: self.syncText.addressType)
                  self.addressLine1.configOnErrorCleared(withPlaceHolder: self.syncText.addressLine1)
                  self.addressLine2.configOnErrorCleared(withPlaceHolder: self.syncText.addressLine2)
                  self.cityTextField.configOnErrorCleared(withPlaceHolder: self.syncText.city)
                  self.stateTextField.configOnErrorCleared(withPlaceHolder: self.syncText.state)
                  self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.syncText.pincode)
                  self.countryTextField.configOnErrorCleared(withPlaceHolder: self.syncText.country)
                  self.typeArray = AppService.shared.addressTypes
                  
            }
      }
      /* END */

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  addressTypeTxtField:
                  if (addressTypeTxtField.text?.characters.count)! <= 0 {
                        self.addressTypeTxtField.configOnError(withPlaceHolder: self.syncText.enterAddressType)

                  } else {
                        self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: self.syncText.addressType)
                  }

            case  addressLine1:
                  if (addressLine1.text?.characters.count)! <= 0 {
                        self.addressLine1.configOnError(withPlaceHolder: self.syncText.enterAddressLine1)

                  } else {
                        self.addressLine1.configOnErrorCleared(withPlaceHolder: self.syncText.addressLine1)
                  }

            case cityTextField:
                  if (cityTextField.text?.characters.count)! <= 0 {
                        self.cityTextField.configOnError(withPlaceHolder: self.syncText.enterCity)

                  } else {
                        self.cityTextField.configOnErrorCleared(withPlaceHolder: self.syncText.city)
                  }

            case stateTextField:

                  if (stateTextField.text?.characters.count)! <= 0 {
                        self.stateTextField.configOnError(withPlaceHolder: self.syncText.enterState)

                  } else {
                        self.stateTextField.configOnErrorCleared(withPlaceHolder: self.syncText.state)
                  }

            case postalCodeTextField:
                  if (postalCodeTextField.text?.characters.count)! <= 0 {
                        self.postalCodeTextField.configOnError(withPlaceHolder: self.syncText.enterPincode)

                  } else {
                        self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.syncText.pincode)
                  }

            case countryTextField:
                  if (countryTextField.text?.characters.count)! <= 0 {
                        self.countryTextField.configOnError(withPlaceHolder: self.syncText.enterCountry)

                  } else {
                        self.countryTextField.configOnErrorCleared(withPlaceHolder: self.syncText.country)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case addressTypeTxtField:
                  if (addressTypeTxtField.text?.characters.count)! <= 0 {
                        self.addressTypeTxtField.configOnError(withPlaceHolder: self.syncText.enterAddressType)
                  } else {
                        self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: self.syncText.addressType)
                  }

            case  addressLine1:
                  if (addressLine1.text?.characters.count)! <= 0 {
                        self.addressLine1.configOnError(withPlaceHolder: self.syncText.enterAddressLine1)

                  } else {
                        self.addressLine1.configOnErrorCleared(withPlaceHolder: self.syncText.addressLine1)
                  }

            case cityTextField:
                  if (cityTextField.text?.characters.count)! <= 0 {
                        self.cityTextField.configOnError(withPlaceHolder: self.syncText.enterCity)

                  } else {
                        self.cityTextField.configOnErrorCleared(withPlaceHolder: self.syncText.city)
                  }

            case stateTextField:

                  if (stateTextField.text?.characters.count)! <= 0 {
                        self.stateTextField.configOnError(withPlaceHolder: self.syncText.enterState)

                  } else {
                        self.stateTextField.configOnErrorCleared(withPlaceHolder: self.syncText.state)
                  }

            case postalCodeTextField:
                  if (postalCodeTextField.text?.characters.count)! <= 0 {
                        self.postalCodeTextField.configOnError(withPlaceHolder: self.syncText.enterPincode)
                  } else if ((postalCodeTextField.text?.characters.count)! < 3 ||  (postalCodeTextField.text?.characters.count)!  > 10) {
                        self.postalCodeTextField.configOnError(withPlaceHolder: self.syncText.invalidPinCode)
                  } else {
                        self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.syncText.pincode)
                  }

            case countryTextField:
                  if (countryTextField.text?.characters.count)! <= 0 {
                        self.countryTextField.configOnError(withPlaceHolder: self.syncText.enterCountry)

                  } else {
                        self.countryTextField.configOnErrorCleared(withPlaceHolder: self.syncText.country)
                  }

            default:
                  break
            }
      }

      @IBAction func closeBtnTapped(_ sender: Any) {
        self.didMove(toParent: nil)
            self.view.removeFromSuperview()
        self.removeFromParent()
            self.dismiss(animated: true, completion: nil)
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      func removeLayer() {
            let overlay = containerView.viewWithTag(100)
            overlay?.removeFromSuperview()
      }

      @IBAction func submitTapped(_ sender: Any) {
            var isValidationCorrect: Bool = true
            if (addressTypeTxtField.text?.characters.count)! <= 0 {
                  self.addressTypeTxtField.configOnError(withPlaceHolder: self.syncText.enterAddressType)
                  isValidationCorrect = false
            } else {
                  self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: self.syncText.addressType)
            }

            if (addressLine1.text?.characters.count)! <= 0 {
                  self.addressLine1.configOnError(withPlaceHolder: self.syncText.enterAddressLine1)
                  isValidationCorrect = false
            }
            if  (self.cityTextField.text?.characters.count)! <= 0 {
                  self.cityTextField.configOnError(withPlaceHolder: self.syncText.enterCity)
                  isValidationCorrect = false
            }
            if  (self.stateTextField.text?.characters.count)! <= 0 {
                  self.stateTextField.configOnError(withPlaceHolder: self.syncText.enterState)
                  isValidationCorrect = false
            }
            if  (self.postalCodeTextField.text?.characters.count)! <= 0 {
                  self.postalCodeTextField.configOnError(withPlaceHolder: self.syncText.enterPincode)
                  isValidationCorrect = false
            }
            if  (self.postalCodeTextField.text?.characters.count)! < 3 ||  (self.postalCodeTextField.text?.characters.count)! > 10 {
                  self.postalCodeTextField.configOnError(withPlaceHolder: self.syncText.invalidPinCode)
                  isValidationCorrect = false
            }
            if (self.countryTextField.text?.characters.count)! <= 0 {
                  self.countryTextField.configOnError(withPlaceHolder: self.syncText.enterCountry)
                  isValidationCorrect = false
            }
            if isValidationCorrect {
                  if editMode {
                        self.editAddress()
                  } else {
                        self.saveAddress()
                  }
            }
      }

      func saveAddress() {
            if(self.contactId == 0) {
                  self.view.makeToast(self.syncText.addContactPersonalInfo)
                _ = 1.0
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.firstTimeInterval) {
                        self.view.hideToast()
                  }
            } else {
                  let formatted = self.addressLine1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let streetAddress = self.addressLine2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let locality = self.cityTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let region = self.stateTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let postalCode = self.postalCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let country = self.countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                
                  var addressString = self.selectedText  + "," + formatted + "," + streetAddress + ","
                  addressString = addressString + locality + "," + region + "," + country + "," + postalCode

                  var contactObj = ContactObj()
                  contactObj.contactId = self.contactId
                  if(self.contactObj.addressStringArray1.count > 0) {
                        contactObj.addressString2 = addressString
                  } else {
                        contactObj.addressString1 = addressString
                  }

                  contactObj.isBackendSync = false
                  contactObj.updatedAt = getCurrentTImeStamp()
                  let isOperationSuccessfull = DatabaseManagement.shared.updateAddress(contactObj: contactObj)
                  if(isOperationSuccessfull) {
                        ContactsSync.shared.getNonSyncedContacts()
                        ContactsSync.shared.backendSync()
                        self.dismiss(animated: false) {
                              NotificationCenter.default.post(name: REFRESH_ADDRESSES_NOTIFICATION, object: nil)
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Address add / edit pop-up", action: "Add address clicked", label: "Add address successful", value: 0)
                  } else {
                    self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Address add / edit pop-up", action: "Add address clicked", label: "Failed to add address", value: 0)
                  }
            }
      }

      func editAddress() {
            let formatted = self.addressLine1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let streetAddress = self.addressLine2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let locality = self.cityTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let region = self.stateTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let postalCode = self.postalCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let country = self.countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
 
            let addressString =  self.selectedText  + "," + formatted + "," + streetAddress + "," + locality + "," + region + "," + country + "," + postalCode

            var contactObj = ContactObj()
            contactObj.contactId = self.contactId
            if(self.noOfAddressToEdit == 0) {
                  contactObj.addressString1 = addressString
            } else if(self.noOfAddressToEdit == 1) {
                  contactObj.addressString2 = addressString
            }

            contactObj.isBackendSync = false
            contactObj.updatedAt = getCurrentTImeStamp()
            let isOperationSuccessfull = DatabaseManagement.shared.updateAddress(contactObj: contactObj)
            if(isOperationSuccessfull) {
                  ContactsSync.shared.getNonSyncedContacts()
                  ContactsSync.shared.backendSync()
                  self.dismiss(animated: false) {
                        NotificationCenter.default.post(name: REFRESH_ADDRESSES_NOTIFICATION, object: nil)
                  }
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Address add / edit pop-up", action: "Edit address clicked", label: "Edit address successful", value: 0)
            } else {
                  self.view.makeToast(self.syncText.failedToUpdateContact, duration: self.appService.secondTimeInterval, position: .bottom)
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Address add / edit pop-up", action: "Edit address clicked", label: "Failed to edit address", value: 0)
            }
      }

      // MARK: Textfield Delegates

      func textFieldDidBeginEditing(_ textField: UITextField) {

            switch textField {

            case addressTypeTxtField:
                  self.addressTypeTxtField.configOnFirstResponder(withPlaceHolder: self.syncText.selectAddressType)

            default:
                  if textField.isKind(of: UICustomUnderLineTextField.self) {
                        let txtField = textField as! UICustomUnderLineTextField
                        txtField.configOnFirstResponder()
                  }
                  if textField.tag == 0 {
                        textField.text = ""
                        textField.textColor = UIColor.black
                  }
                  break
            }
      }

      func textFieldDidEndEditing(_ textField: UITextField) {

            if textField.isKind(of: UICustomUnderLineTextField.self) {
                  let txtField = textField as! UICustomUnderLineTextField
                  txtField.configOnResignResponder()
            }
      }

      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

            return true
      }

      func textFieldShouldReturn(_ textField: UITextField) -> Bool {

            textField.resignFirstResponder()
            let tag = textField.tag + 1
            if let nextTextField = self.view.viewWithTag(tag) {
                  if nextTextField.isKind(of: UICustomUnderLineTextField.self) {
                        nextTextField.becomeFirstResponder()
                  }
            }

            return true
      }

    @objc func navigateToMaps() {

            let mapVC = MapViewController.instantiateFromStoryboardWithIdentifier(identifier: "MapViewController")

            mapVC.addressSearchDelegate = self as? AddressSearchNew
            if  self.latitude != 0.0 &&  self.longitude != 0.0 {
                  mapVC.originalLong = self.longitude
                  mapVC.originalLat = self.latitude
            }
            let navController = UINavigationController(rootViewController: mapVC)

            self.present(navController, animated: true, completion: nil)

      }
}

extension AdrressAddEditPopUpVC: AddressSearchNew {
      func addrssObjectFrom(place: MKPlacemark?) {
            if let placemark = place {
                  selectedPlaceMark = placemark
                  if let name = placemark.name {
                        self.addressLine1.text = "\(name)"
                        self.addressLine1.configOnErrorCleared(withPlaceHolder: self.syncText.addressLine1)
                  }
                  if let thoroughfare = placemark.thoroughfare {
                        self.addressLine2.text="\(thoroughfare)"
                  }
                  if let loc = placemark.locality {
                        self.cityTextField.text = "\(loc)"
                        self.cityTextField.configOnErrorCleared(withPlaceHolder: self.syncText.city)
                  }
                  if let postalCode = placemark.postalCode {
                        self.postalCodeTextField.text = "\(postalCode)"
                        self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.syncText.pincode)
                  }
                  if let state = placemark.administrativeArea {
                        self.stateTextField.text = "\(state)"
                        self.stateTextField.configOnErrorCleared(withPlaceHolder: self.syncText.state)
                  }
                  if let country = placemark.country {
                        self.countryTextField.text = "\(country)"
                        self.countryTextField.configOnErrorCleared(withPlaceHolder: self.syncText.country)
                  }
                  if let latitude = placemark.location?.coordinate.latitude {
                        self.latitude = latitude
                  }
                  if let longitude = placemark.location?.coordinate.longitude {
                        self.longitude = longitude
                  }
            } else {
                  selectedPlaceMark = nil
            }
      }
}
