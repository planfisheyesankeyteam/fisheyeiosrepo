//
//  WebServiceManager.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 06/01/17.
//  Copyright (c) 2017 Keerthi Chinivar. All rights reserved.
//

import UIKit
import AWSCore
//import AWSAPIGateway

let kServerBaseUrl = ""// kApplicationDelegate.appConfig.baseUrl!

typealias completionHandler = (_ responseData: Any?, NSError?, _ isSucess: Bool) -> Void

class WebServiceManager: NSObject {

    fileprivate static let sharedManager: WebServiceManager = {

      NotificationCenter.default.addObserver(self, selector: #selector(callbackgroundAPI), name: Notification.Name.reachabilityChanged, object: nil)

        let manager = WebServiceManager.init()

        return manager
    }()

    // Initialization

    override init() {

    }

    // MARK: - Accessors

    class func shared() -> WebServiceManager {

        return sharedManager
    }

//    static let shared = WebServiceManager()

    let network = NetworkStatus.sharedManager

    let client = FEUserAPIsClient.default()

    let contactClient = FEContactAPIsClient.default()

    let pulseClient = FEPulseAPIsClient.default()

//    var kAWSAPIGatewayClient : [AWSAPIGatewayClient]!

    var feOfflineReqs: [OfflineRequest]?

    func verifyPhoneNumber(handler:@escaping completionHandler) {

        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }

        client.phonenumberVerifyGet(idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in
            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func sendOTP(phoneNumber num: String, countryCode code: String, handler:@escaping completionHandler) {

        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        client.phonenumberSendotpGet(phonenumber: num, countryCode: code, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func verifyOTP(phoneNumber num: String, otp: String, handler:@escaping completionHandler) {

       CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        client.phonenumberVerifyotpGet(phonenumber: num, otp: otp, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func resendOTP(phoneNumber num: String, countryCode code: String, handler: @escaping completionHandler) {

        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)

        }
        client.phonenumberResendotpGet(phonenumber: num, countryCode: code, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func postAddress(addressReq address: FEAddressesRequest, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }

        client.usersUserAddressPost(body: address, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in
            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func putAddress(addressReq address: FEAddress, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }

        client.usersUserAddressPut(body: address, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in
            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func postNotification(notification: FENotification, handler: @escaping completionHandler) {
        //CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }

       // notification.userId = "114877484203518682270"

        client.notificationsPost(body: notification, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in
            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func getUserProfile(handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        client.usersUserGet(idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func postContacts(syncReq: FESyncContactsRequest, handler: @escaping completionHandler) {

//        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }

        contactClient.contactsPost(body: syncReq, idtoken: CommonMethods.retriveIdToken()).continueWith(executor: AWSExecutor.init(dispatchQueue: DispatchQueue.init(label: "background", qos: .background, attributes: .concurrent, autoreleaseFrequency: .workItem, target: nil))) { (task) -> Any? in
            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func getSyncContacts(handler: @escaping completionHandler) {

        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        contactClient.contactsGet(idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func pulseShareAfterRequest(pulseId: String, fepulserequest: FEPulseRequestDataNowData, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        pulseClient.pulseNowRequestPulseidPost(pulseid: pulseId, body: fepulserequest, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }
    }

    func pulseLogbook(handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        pulseClient.pulseLogbooksGet(idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }
    }

    func pulseShare(fepulserequest: FEPulseShareLocation, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        pulseClient.pulseNowSharePost(body: fepulserequest, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }
    }

    func pulseRequest(fepulserequest: FEPulseRequestNow, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        pulseClient.pulseNowRequestPost(body: fepulserequest, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }
    }

    func mergeContact(mergeContId: String?, withContId: String?, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        contactClient.contactsMergePut(mergeContactId: mergeContId, contactId: withContId, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func deleteContact(withContId: String?, handler: @escaping completionHandler) {

        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            self.schedule(task: contactClient.contactsContactIdGet(contactId: withContId!, idtoken: CommonMethods.retriveIdToken()) as! AWSTask<AnyObject>)
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)

        }
        contactClient.contactsContactIdDelete(contactId: withContId!, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }

    }

    func getConatct(contactId: String, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        contactClient.contactsContactIdGet(contactId: contactId, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }
    }

    func addConatct(conatct: FEContact, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        contactClient.contactsContactPost(body: conatct, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }
    }

    func editConatct(contactId: String, conatct: FEContact, handler: @escaping completionHandler) {
        CommonMethods.showProgressView()

        guard network.isNetworkReachable() else {
            return handler(nil, NSError.init(domain: "FE", code: kNetworkNotReachableCode, userInfo: nil), false)
        }
        contactClient.contactsContactIdPut(contactId: contactId, body: conatct, idtoken: CommonMethods.retriveIdToken()).continueWith { (task) -> Any? in

            return self.showResult(task: task as! AWSTask<AnyObject>, handler: handler)
        }
    }

    func showResult(task: AWSTask<AnyObject>, handler:@escaping completionHandler) {
        if let error = task.error {
            DispatchQueue.main.async {
                kSweetAlert.showAlert("Message!", subTitle: "Oops! there seems to be some problem", style: AlertStyle.error)

            }
            return handler(nil, error as NSError, false)
        } else if let result = task.result {

            if result is FEPhoneNumberResponse {
                let res = result as! FEPhoneNumberResponse
//                guard !self.displayAlert(statusCode: res.statusCode) else{
//                    return handler(result, nil, false)
//                }
                return handler(result, nil, true)

            } else if result is FESendOTPResponse {
                let res = result as! FESendOTPResponse

                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }
              
                return handler(result, nil, true)
            } else if result is FEVerifyOTPResponse {
                let res = result as! FEVerifyOTPResponse
                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }
                
                return handler(result, nil, true)
            } else if result is FEResendOTPResponse {
                let res = result as! FEResendOTPResponse
                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }
              
                return handler(result, nil, true)

            } else if result is FEAddressesResponse {
                let res = result as! FEAddressesResponse
                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }
               
                return handler(result, nil, true)
            } else if result is FEDisplayContactsResponse {
                let res = result as! FEDisplayContactsResponse
                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }
                
                return handler(result, nil, true)
            } else if result is FEContactDetailsResponse {
                let res = result as! FEContactDetailsResponse
                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }
               
                return handler(result, nil, true)
            } else if result is FEResponse {
                let res = result as! FEResponse
                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }

                return handler(result, nil, true)
            } else if result is FELogBookResponse {
                let res = result as! FELogBookResponse
                guard !self.displayAlert(statusCode: res.statusCode, statusMsg: res.statusMessage) else {
                    return handler(result, nil, false)
                }
                return handler(result, nil, true)
            } else if result is NSDictionary {
                let res = result as! NSDictionary
                
            }
            return handler(result, nil, true)
        }
    }

    // MARK: Get - Parser Method
    //    func request(method methodType : eHTTPMethod, postData : String , withKey key : String , handler:@escaping completionHandler){
    //        let network = NetworkStatus.sharedManager
    //        if network.isNetworkReachable() == false
    //        {
    //            handler(nil,NSError.init(domain: "TA", code: kNetworkNotReachableCode, userInfo: nil),false)
    //
    //            let noInternrtVC = NoInternetController.instantiateFromStoryboardWithIdentifier(identifier: "NoInternetController")
    //
    //            kApplicationDelegate.nVC.topViewController?.present(noInternrtVC, animated: true, completion: nil)
    //
    //
    //        }
    //
    ////        self.post(data: postData, handler:handler)
    
    //
    //
    //    }

    //    func gh(result:T,handler:@escaping completionHandler){
    //        if self.displayAlert(statusCode: res.statusCode,statusMsg:res.statusMessage){
    //            return handler(result, nil, false)
    //        }
    //        else{
    //            return handler(result, nil, true)
    //        }
    //    }

    func displayAlert(statusCode: String?, statusMsg: String?=nil) -> Bool {
        if let code = statusCode, code != "200" {
            DispatchQueue.main.async {
                kSweetAlert.showAlert("Message!", subTitle: statusMsg, style: AlertStyle.error)
            }
            return true
        }
        return false

    }

    func loadNoInternetVC() {

        let noInternrtVC = NoInternetController.instantiateFromStoryboardWithIdentifier(identifier: "NoInternetController")

        kApplicationDelegate.nVC.topViewController?.present(noInternrtVC, animated: true, completion: nil)

    }

    func post(data withData: String, handler:@escaping completionHandler) {

        var request =  URLRequest.init(url: URL.init(string: kServerBaseUrl)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        request.httpMethod = "POST"
        request.addValue("application/xml", forHTTPHeaderField: "Content-Type")
        request.addValue("gzip", forHTTPHeaderField: "Content-Encoding")
        request.httpBody = withData.data(using: .utf8)

        DispatchQueue.main.async {
            let dataTask: URLSessionDataTask  = URLSession.shared.dataTask(with: request, completionHandler: {
                (data, response, error) in

                if error != nil {
                    DispatchQueue.main.async {

                        //  kSweetAlert.showAlert(, subTitle: "Oops! there seems to be some problem", style: AlertStyle.error)
                        handler(nil, error as NSError?, false)
                    }
                } else {
                   

                    var dataToString = String(data: data!, encoding: String.Encoding.ascii)
                    //                        response.
                    //                        dataToString = self.stringByRemovingControlCharacters2(string: dataToString!)
                    //                        let ndata = dataToString!.data(using: String.Encoding.utf8)

                    var jsonDictionary: Dictionary<String, Any>
                    do {
                        jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! Dictionary<String, Any>

                       
                        if let httpResponse = jsonDictionary["Response_Code"] as? String {
                            if httpResponse.characters.count == 4 {
                                DispatchQueue.main.async {
                                    handler(jsonDictionary, nil, true)
                                }
                            } else {

                                DispatchQueue.main.async {
                                    handler(jsonDictionary, nil, false)
                                }
                            }
                        }
                    } catch let JSONError as NSError {

                        let str = JSONError.userInfo["NSDebugDescription"] as! String
                        if str.contains("Unescaped control character around character 145.") {

                            let str1 = String.init(data: data!, encoding: String.Encoding.ascii)

                            let abc = str1?.trimmingCharacters(in: .newlines)

                            let array = abc?.components(separatedBy: "\n\t")

                            let rsa = array?[3]

                            var dict: [String: Any] = ["": ""]

                            if let rsakey = rsa?.components(separatedBy: "\"") {
                                dict = [ "\(rsakey[1])": rsakey[3]]

                            }

                            handler(dict, nil, true)

                        } else {
                            DispatchQueue.main.async {
                                //  kSweetAlert.showAlert("Message!", subTitle: "Oops! there seems to be some problem", style: AlertStyle.error)
                                handler(nil, JSONError, false)
                            }
                        }

                    }

                }

            })
            dataTask.resume()
        }

    }

    func stringByRemovingControlCharacters2(string: String) -> String {

        let controlChars = NSCharacterSet.controlCharacters
        var range = string.rangeOfCharacter(from: controlChars)
        var mutable = string
        while let removeRange = range {
            mutable.removeSubrange(removeRange)
            range = mutable.rangeOfCharacter(from: controlChars)
        }

        return mutable
    }

    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            } catch {
              
            }
        }
        return nil
    }

    func schedule(task: AWSTask<AnyObject>) {

//        if feOfflineReqs == nil{
//            feOfflineReqs = []
//        }
//
//
//        let offliineReq = FEOfflineReq(task: task, id: self.feOfflineReqs!.count)
//
//        feOfflineReqs!.append(offliineReq)
        self.saveAWSTask(task)

    }

    @objc func callbackgroundAPI() {

        guard network.isNetworkReachable() else {
            return
        }

        DispatchQueue.global(qos: .background).async { // sends registration to background queue
            if let fetchAllOfflineRequests = self.fetchAllOfflineRequests() {

              //  self.copyLocally(mObjs : fetchAllOfflineRequests)

                for req in self.feOfflineReqs! {
                   let awsTask = req.value(forKeyPath: "requestTask") as? AWSTask<AnyObject>
                    awsTask?.continueWith { (task) -> Any? in
                        req.setValue(task, forKeyPath: "requestTask")
                        return self.parseResult(forRequest: req)

                    }
                }
            }

        }

    }

//    func copyLocally(mObjs : [NSManagedObject])  {
//
//
//        feOfflineReqs = []
//
//        for reqObj in mObjs{
//            let offlineReq = FEOfflineReq()
//            offlineReq.awsReqTask = reqObj.value(forKeyPath: "requestTask") as? AWSTask<AnyObject>
//            offlineReq.awsReqId = reqObj.value(forKeyPath: "requestTaskId") as! Int
//            self.feOfflineReqs?.append(offlineReq)
//        }
//
//
//    }

    func parseResult(forRequest: OfflineRequest) {

        if let task = forRequest.value(forKeyPath: "requestTask") as? AWSTask<AnyObject> {

            if let error = task.error {

            } else if let result = task.result {

                if result is FEContactDetailsResponse {

                    let res = result as! FEContactDetailsResponse

                    if let code = res.statusCode, code == "200" {

                       let id = forRequest.value(forKeyPath: "requestTaskId") as! Int

                        self.deleteTask(withId: id)
                    }

                }

            }
        }
    }

    func saveAWSTask(_ task: AWSTask<AnyObject>) {

        let managedContext = CodeDataManager.shared.getContext()

        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: "OfflineRequest",
                                       in: managedContext)!

            let offlineReqMO = NSManagedObject(entity: entity,
                                          insertInto: managedContext)

            // 3
            offlineReqMO.setValue(task, forKeyPath: "requestTask")
        offlineReqMO.setValue(feOfflineReqs?.count == nil ? -1 : feOfflineReqs?.count, forKeyPath: "requestTaskId")

            // 4
            do {
                try managedContext.save()

            } catch let error as NSError {
                
            }

    }

    func fetchAllOfflineRequests() -> [NSManagedObject]? {
        //1
        var offlineReqs: [NSManagedObject]?
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {                return nil

        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest =  NSFetchRequest<NSManagedObject>(entityName: "OfflineRequest")                //3
        do {
            offlineReqs = try managedContext.fetch(fetchRequest) as! [OfflineRequest]

        } catch let error as NSError {
           

        }
        guard offlineReqs != nil else {
            return nil

        }
        return offlineReqs

    }

        func deleteTask(withId: Int) {
            let moc = CodeDataManager.shared.getContext()
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineRequest")
            fetchRequest.predicate = NSPredicate.init(format: "requestTaskId == %d", "\(withId)")
            let result = try? moc.fetch(fetchRequest)
            let resultData = result as! [OfflineRequest]
            for object in resultData {
                moc.delete(object)

            }
            do {
                try moc.save()
                

            } catch let error as NSError {
                

            } catch {

            }

    }

}

