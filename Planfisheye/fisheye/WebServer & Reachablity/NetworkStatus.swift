//
//  NetworkStatus.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class NetworkStatus {

    static let sharedManager = NetworkStatus()

    var reachability: Reachability? {
        return Reachability()
    }

    init() {
        do {
            try reachability?.startNotifier()
        } catch {
            
        }
    }

    func isNetworkReachable() -> Bool {
        if (reachability?.isReachable)! {
            return  true
        }
        return false
    }
}
