//
//  InsertCapsuleViewController.swift
//  fisheye
//
//  Created by Keerthi on 28/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import CoreLocation

class InsertCapsuleViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var sendlockedBtn: UIButton!
    var selectedContactID: String = ""
    var isshare: Bool = false
    var locManager = CLLocationManager()

    @IBOutlet weak var sendopenBtn: UIButton!
    var isEncrypted: NSNumber = 1
    @IBOutlet weak var messageTextField: DesignableUITextField!
    @IBAction func sendRequestBtnClick(_ sender: Any) {

        if(isshare) {
            sendShare()
        } else {
        sendRequest()
        }

    }

    func sendShare() {

        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied {

            // present an alert indicating location authorization required
            // and offer to take the user to Settings for the app via
            // UIApplication -openUrl: and UIApplicationOpenSettingsURLString
            DispatchQueue.main.async(execute: {
                let alert = UIAlertController(title: "Error!", message: "GPS access is restricted. In order to use tracking, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertActionStyle.default, handler: { (_: UIAlertAction!) in
                    // print("")
                    //                                            UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)

                    if !CLLocationManager.locationServicesEnabled() {
                        //                                                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)

                        if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
                            // If general location settings are disabled then open general location settings
                            UIApplication.shared.open(url)
                        }
                    } else {

                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                        //
                        //                                                if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
                        //                                                    // If general location settings are enabled then open location settings for the app
                        //                                                    UIApplication.shared.openURL(url)
                        //                                                }
                    }
                }))
                // self.presentViewController(alert, animated: true, completion: nil)
                UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
            })

        } else {
            self.locManager.requestAlwaysAuthorization()
            self.locManager.requestWhenInUseAuthorization()

            self.locManager.startUpdatingLocation()
            let lat = self.locManager.location?.coordinate.latitude
            let long = self.locManager.location?.coordinate.longitude

            let finallat: String = "\(lat!)"
            let finallong: NSNumber = NSNumber(value: long!)

            let pulseShare = FEPulseShareLocation()
            pulseShare?.contactId = selectedContactID
            pulseShare?.isencrypted = isEncrypted
            pulseShare?.message = messageTextField.text
            pulseShare?.latitude = finallat
            pulseShare?.longitude = finallong
            WebServiceManager.shared().pulseShare(fepulserequest: pulseShare!) { (_, error, success) in
                CommonMethods.hideProgressView()

                if success {
                    DispatchQueue.main.async {
                        kSweetAlert.showAlert("Message!", subTitle: "Location Shared Success!", style: AlertStyle.success, buttonTitle: "OK") {(_) -> Void in
                            let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

                            vc.unHideBottomviews()
                            vc.didTapOnLogo()

                        }

                    }
                } else {
                    kSweetAlert.showAlert("Message!", subTitle: "Location Shared Failed!", style: AlertStyle.error)

                }
                }

        }

    }

    func sendRequest() {

        let pulseRequest = FEPulseRequestNow()
        pulseRequest?.contactId = selectedContactID
        pulseRequest?.isencrypted = isEncrypted
        pulseRequest?.message = messageTextField.text
        WebServiceManager.shared().pulseRequest(fepulserequest: pulseRequest!) { (_, error, success) in
            CommonMethods.hideProgressView()

            if success {
                DispatchQueue.main.async {
                    kSweetAlert.showAlert("Message!", subTitle: "Pulse Request Success!", style: AlertStyle.success, buttonTitle: "OK") {(_) -> Void in
                        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

                        vc.unHideBottomviews()
                        vc.didTapOnLogo()

                    }

                }
            } else {
                kSweetAlert.showAlert("Message!", subTitle: "Pulse Request Failed!", style: AlertStyle.error)

            }
        }
    }
    @IBOutlet weak var sendRequestBtn: UIButton!
    @IBAction func sendLockBtnClick(_ sender: Any) {
        isEncrypted = 1
        sendlockedBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")

        sendopenBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
    }
    @IBAction func sendOpenBtnClick(_ sender: Any) {

        isEncrypted = 0
        sendopenBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")

        sendlockedBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
//        let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
//        let profileBaseviewController = storyboard.instantiateViewController(withIdentifier: "PulseLogbookViewController") as! PulseLogbookViewController
//        profileBaseviewController.view.frame = self.view.bounds
//        self.addChildViewController(profileBaseviewController)
//        profileBaseviewController.didMove(toParentViewController: self)
//        
//        //  profileBaseviewController.delegate = self
//        
//        profileBaseviewController.view.frame.origin.y = -self.view.frame.size.height
//        
//        
//        self.view.addSubview(profileBaseviewController.view)
//        
//        
//        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: { _ in
//            profileBaseviewController.view.frame.origin.y = self.view.bounds.origin.y
//        }, completion: nil)
    }
    @IBOutlet weak var capsulelockunlockView: UIView!

    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var textfield: DesignableUITextField!

    @IBAction func backclick(_ sender: Any) {

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        applyViewShadow()

        if(isshare) {
            sendRequestBtn.setTitle("Share Location", for: .normal)
        } else {
            sendRequestBtn.setTitle("Send request", for: .normal)
        }

        textfield.delegate = self

        self.locManager.requestAlwaysAuthorization()
        self.locManager.requestWhenInUseAuthorization()
        // Do any additional setup after loading the view.
    }
    func applyViewShadow() {
        self.mainview.layer.cornerRadius = 8

        // border
        self.mainview.layer.borderWidth = 0
        self.mainview.layer.borderColor = UIColor.black.cgColor
        //        self.mainview.clipsToBounds = true

        // shadow
        self.mainview.layer.shadowColor = UIColor.black.cgColor
        self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.mainview.layer.shadowOpacity = 0.7
        self.mainview.layer.shadowRadius = 10.0

        self.textfield.layer.masksToBounds = false
        self.textfield.layer.shadowRadius = 1.0
        self.textfield.layer.shadowColor = UIColor.black.cgColor
        self.textfield.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.textfield.layer.shadowOpacity = 1.0

        self.capsulelockunlockView.layer.cornerRadius = 17.0

        self.capsulelockunlockView.layer.borderWidth = 0
        self.capsulelockunlockView.layer.borderColor = UIColor.black.cgColor
        //        self.mainview.clipsToBounds = true

        // shadow
        self.capsulelockunlockView.layer.shadowColor = UIColor.black.cgColor
        self.capsulelockunlockView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.capsulelockunlockView.layer.shadowOpacity = 0.7
        self.capsulelockunlockView.layer.shadowRadius = 10.0

        self.sendRequestBtn.layer.cornerRadius = 18.0

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

      /* --------------- Track Screen Google Analytics --------------- */
      func trackScreenOnGoogleAnalytics() {
            guard let tracker = GAI.sharedInstance().defaultTracker else { return }
            tracker.set(kGAIScreenName, value: AppConfig.capsuleInsertPopUpViewController)

            guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
            tracker.send(builder.build() as [NSObject: AnyObject])
      }
      /* --------------- Track Screen Google Analytics  End--------------- */

}
