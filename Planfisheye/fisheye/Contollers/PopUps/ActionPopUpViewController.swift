//
//  ActionPopUpViewController.swift
//  Created by Sankey Solution on 12/1/17.
//  Copyright © 2017 Fisheye. All rights reserved.
//  Description: The View controller is designed to handle the Logout functionality of the Fisheye App

import UIKit

class ActionPopUpViewController: UIViewController {

    let appSharedPrefernce = AppSharedPreference()
    var headingPopUp: String = ""
    var subHeadingPopUp: String = ""
    var nextAction: String = ""
    var prevAction = ""
    var method: String = ""
    var module: String = ""
    let appService = AppService.shared
    var modifySignUpDataProtocol: ModifySignUpDataProtocol?
    var idtoken = ""
    
    @IBOutlet weak var warningImage: UIImageView!
    @IBOutlet weak var dismissImage: UIImageView!
    @IBOutlet weak var successImage: UIImageView!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var subHeading: UILabel!
    @IBOutlet weak var popUpView: UIView!
      @IBOutlet weak var cancel: UILabel!
      @IBOutlet weak var ok: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        initialisingView()
        getData()
        dispPopUpMethod()
        self.addObservers()
        self.setLocalizationText()
    }

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.cancel.text = self.appService.cancel
                  self.ok.text = self.appService.ok
            }
      }
      /* END */

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ActionPopUpViewController)
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func getData() {
      let heading = (self.appSharedPrefernce.getAppSharedPreferences(key: "heading") as? String)!
      self.headingPopUp = heading
      let subHeading = (self.appSharedPrefernce.getAppSharedPreferences(key: "subheading") as? String)!
      self.subHeadingPopUp = subHeading
      self.nextAction = (self.appSharedPrefernce.getAppSharedPreferences(key: "action") as?  String)!
      self.method = (self.appSharedPrefernce.getAppSharedPreferences(key: "method") as?  String)!
      self.prevAction = (self.appSharedPrefernce.getAppSharedPreferences(key: "prevaction") as?  String)!
      self.heading.text = heading
      self.subHeading.text = subHeading
    }

    func initialisingView() {
        self.warningImage.isHidden = true
        self.dismissImage.isHidden = true
        self.successImage.isHidden = true
        self.heading.text = ""

        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0

        self.popUpView.layer.cornerRadius = 6
    }
    func dispPopUpMethod() {
        if (self.nextAction == "warning") {
            self.warningImage.isHidden = false
            self.dismissImage.isHidden = true
            self.successImage.isHidden = true
        } else if (self.nextAction == "success") {
            self.warningImage.isHidden = true
            self.dismissImage.isHidden = true
            self.successImage.isHidden = false
        } else if (self.nextAction == "failure") {
            self.warningImage.isHidden = true
            self.dismissImage.isHidden = false
            self.successImage.isHidden = true
        }
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        nextButtonFunction()
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        prevButtonFunction()
    }

    func prevButtonFunction() {
        let prev = self.prevAction

        switch prev {
        case "dismissMethod":
            let baseVC  = BaseViewController.instantiateFromStoryboardWithIdentifier(identifier: "BaseViewController")
            kApplicationDelegate.nVC = UINavigationController(rootViewController: baseVC)
            kApplicationDelegate.nVC.navigationBar.isHidden  = true
            kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC
            break

        case "sendOTPVC":
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
            self.present(nextVC, animated: true, completion: nil)
            break

        case "sendTermsNConditionsVC":
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
            self.present(nextVC, animated: true, completion: nil)
            break

        default: break
        }
    }

    func nextButtonFunction() {
        let methodCalled = self.method

        switch methodCalled {
        case "logout":
//            self.startLoader()
                  let loginPreference = self.appSharedPrefernce.getAppSharedPreferences(key: "loginPreferenceUsed") as? String ?? ""
//                  let introductaryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "dashboardIntroducataryPages") as? String ?? "1"
//                  let syncIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "syncIntroductoryVariableIndex") as? String ?? "1"
//                  let pulseIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "pulseIntroductoryVariableIndex") as? String ?? "1"
//                  let meetMeIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIntroductoryVariableIndex") as? String ?? "1"

                  let isFetchFromBackendCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isFetchFromBackendCompleted") as? String ?? "no"

                  let isfronendSyncFullyCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isfronendSyncFullyCompleted") as? String ?? "no"

                  let fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
                  let preferredLanguage = self.appSharedPrefernce.getAppSharedPreferences(key: "preferredLanguage") as? String ?? ""

                  self.appSharedPrefernce.removeAllAppSharedPreferences()
                  self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: loginPreference)
//                  self.appSharedPrefernce.setAppSharedPreferences(key: "dashboardIntroducataryPages", value: "1")
                  self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeId", value: fisheyeId)

                  self.appSharedPrefernce.setAppSharedPreferences(key: "isFetchFromBackendCompleted", value: isFetchFromBackendCompleted)
                  self.appSharedPrefernce.setAppSharedPreferences(key: "isfronendSyncFullyCompleted", value: isfronendSyncFullyCompleted)
                  self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: preferredLanguage)
//            self.startLoader()
            self.removeNotificationIdFromDb(fisheyeId : fisheyeId)
            
//                  if(syncIntroductoryVariableIndex == "0") {
//                        self.appSharedPrefernce.setAppSharedPreferences(key: "syncIntroductoryVariableIndex", value: "0")
//                  } else {
//                        self.appSharedPrefernce.setAppSharedPreferences(key: "syncIntroductoryVariableIndex", value: "1")
//                  }

//                  if(pulseIntroductoryVariableIndex == "0") {
//                        self.appSharedPrefernce.setAppSharedPreferences(key: "pulseIntroductoryVariableIndex", value: "0")
//                  } else {
//                        self.appSharedPrefernce.setAppSharedPreferences(key: "pulseIntroductoryVariableIndex", value: "1")
//                  }

//                  if(meetMeIntroductoryVariableIndex == "0") {
//                        self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIntroductoryVariableIndex", value: "0")
//                  } else {
//                        self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIntroductoryVariableIndex", value: "1")
//                  }
  
            
//            self.stopLoader()
            break

        case "modifySignUPData":
                  modifySignUpDataProtocol?.modifyData(toReload: true)
                  self.dismiss(animated: true, completion: nil)
                  break

        default: break
        }
    }

      func startLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.stopLoader()
            }
      }
    
    func removeNotificationIdFromDb(fisheyeId : String){
        let parameters =
            [
                "idtoken": self.idtoken
                ] as [String: Any]
        
        let awsURL = AppConfig.deleteNotificationToken
        
        guard let URL = URL(string: awsURL) else {
            
            return }
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        DispatchQueue.main.async {
            self.startLoader()
        }
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {

            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                DispatchQueue.main.async {
                    self.navigateToLogin()
                    self.stopLoader()
                }                
            }else{
                DispatchQueue.main.async {
                    self.navigateToLogin()
                    self.stopLoader()
                }
            }
            }
    })
        
        task.resume()
        session.finishTasksAndInvalidate()
}

func navigateToLogin(){
    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    self.present(nextVC, animated: true, completion: nil)
    self.heading.text = self.headingPopUp
    self.subHeading.text = self.subHeadingPopUp
    AMZNAuthorizationManager.shared().signOut({(_ error: Error?) -> Void in
        if error == nil {
            // error from the SDK or Login with Amazon authorization server.
        }
    })
}
}
