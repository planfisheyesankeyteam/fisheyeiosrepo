//  BasicPopUpViewController.swift
//  fisheye
//  Created by Hitesh Patil on 12/1/17.
//

import UIKit

class BasicPopUpViewController: UIViewController {

    let appSharedPrefernce = AppSharedPreference()
    var headingPopUp: String = ""
    var subHeadingPopUp: String = ""
    var nextAction: String = ""
    var method: String = ""
    var module: String = ""
    var closeStatus: String = ""
    @IBOutlet weak var warningImage: UIImageView!
    @IBOutlet weak var dismissImage: UIImageView!
    @IBOutlet weak var successImage: UIImageView!
    @IBOutlet weak var underlineImageView: UIImageView!
    @IBOutlet weak var underLineShadow: UIView!
    @IBOutlet weak var okLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var subHeading: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBAction func closeButtonTapped(_ sender: Any) {

    }
    @IBOutlet weak var closeButtonImage: UIImageView!
    var deleteUserProtocol: DeleteUserProtocol?
    var reloadProtocol: ReloadProtocol?
    var redirectProtocol: RedirectToPrivacyControl?
    var insertCapsuleProtocol: InsertCapsuleProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        initialisingView()
        getData()
        dispPopUpMethod()

    }

    func getData() {

        self.headingPopUp = self.appSharedPrefernce.getAppSharedPreferences(key: "heading") as? String ?? ""
        self.subHeadingPopUp = self.appSharedPrefernce.getAppSharedPreferences(key: "subheading") as? String ?? ""
        self.nextAction = self.appSharedPrefernce.getAppSharedPreferences(key: "action") as? String ?? ""
        self.method = self.appSharedPrefernce.getAppSharedPreferences(key: "method") as? String ?? ""
        self.module = self.appSharedPrefernce.getAppSharedPreferences(key: "module") as? String ?? ""
        self.closeStatus = self.appSharedPrefernce.getAppSharedPreferences(key: "closestatus") as? String ?? ""
        self.heading.text = self.headingPopUp
        self.subHeading.text = self.subHeadingPopUp
    }

    func initialisingView() {
       self.warningImage.isHidden = true
        self.dismissImage.isHidden = true
        self.successImage.isHidden = true
        self.heading.text = ""

//        self.heading.text = self.headingPopUp
//        self.subHeading.text = self.subHeadingPopUp

        self.popUpView.layer.cornerRadius = 6
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0
    }
    func dispPopUpMethod() {
        if (self.nextAction == "warning") {
            self.warningImage.isHidden = false
            self.dismissImage.isHidden = true
            self.successImage.isHidden = true
        } else if (self.nextAction == "success") {
            self.warningImage.isHidden = true
            self.dismissImage.isHidden = true
            self.successImage.isHidden = false
        } else if (self.nextAction == "failure") {
            self.warningImage.isHidden = true
            self.dismissImage.isHidden = false
            self.successImage.isHidden = true
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func okButtonTapped(_ sender: Any) {
         self.nextButtonFunction()
    }

    func nextButtonFunction() {

        let next = self.nextAction
        let moduleNext = self.module
        let methodCalled = self.method

        if (methodCalled == "login") {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "DashBoardPageViewController") as! DashBoardPageViewController
            self.present(nextVC, animated: true, completion: nil)
        } else if (methodCalled == "dismiss") {
            self.dismiss(animated: true, completion: nil)
        } else  if (methodCalled == "SignUpSecurityQuestionVC") {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpSecurityQuestionVC") as! SignUpSecurityQuestionVC
            self.present(nextVC, animated: true, completion: nil)
        } else  if (methodCalled == "SignUpMasterKeyVC") {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpMasterKeyVC") as! SignUpMasterKeyVC
            self.present(nextVC, animated: true, completion: nil)
        } else  if (methodCalled == "SendOtpVC") {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
            self.present(nextVC, animated: true, completion: nil)
        } else  if (methodCalled == "securityPageRedirect") {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ForgetPWDSecurityQuestionVC") as! ForgetPWDSecurityQuestionVC
            self.present(nextVC, animated: true, completion: nil)
        } else if (methodCalled == "deleteProfile") {
            deleteUserProtocol?.deleteData(toReload: true)
            self.dismiss(animated: true, completion: nil)
        } else  if (methodCalled == "deleteSmsVerification") {
            // self.dismiss(animated: true, completion: nil)

            //*********************** after delteing DeleteVerificationVC ******************//

//            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle:nil)
//            let privacynextVC = storyBoard.instantiateViewController(withIdentifier: "DeleteVerificationVC")  as! DeleteVerificationVC
//            self.present(privacynextVC, animated:true, completion: nil)

            //*********************** end******************//

           // self.dismiss(animated: true, completion: nil)
        } else  if (methodCalled == "addressupdate") {
            reloadProtocol?.reloadData(toReload: true)
            self.dismiss(animated: true, completion: nil)
        } else  if (methodCalled == "createMeetSuccess") {
            redirectProtocol?.redirect(toRedirect: true)
            self.dismiss(animated: true, completion: nil)
        } else  if (methodCalled == "redirectToVerification") {
            redirectProtocol?.redirect(toRedirect: true)
            self.dismiss(animated: true, completion: nil)
        } else  if (methodCalled == "goToPulseLogbook") {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController")  as! BaseViewController
            self.present(nextVC, animated: false, completion: {
                nextVC.cellTappedAt(index: 3)
            })

        } else if (methodCalled == "goToShareRequest") {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController")  as! BaseViewController
            self.present(nextVC, animated: false, completion: {
                nextVC.cellTappedAt(index: 3)
            })
        } else  if (methodCalled == "home") {
            self.appSharedPrefernce.removeAllAppSharedPreferences()
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextVC, animated: true, completion: nil)
        } else  if (methodCalled == "openInsertCapsulePopUp") {
            self.appSharedPrefernce.removeAllAppSharedPreferences()
            let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "capsuleInsertPopUpViewController") as! capsuleInsertPopUpViewController
            self.present(nextVC, animated: true, completion: nil)
        } else if (methodCalled == "dismissSelfAndDismissInsert") {
            self.dismiss(animated: true, completion: {
                self.insertCapsuleProtocol?.dismissInsertCapsulePopUp(toReload: true)
            })
        }
    }
}
