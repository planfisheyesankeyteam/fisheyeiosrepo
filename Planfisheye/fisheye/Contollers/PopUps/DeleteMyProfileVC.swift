//
//  DeleteMyProfileVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 18/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class DeleteMyProfileVC: UIViewController {

    let appSharedPrefernce = AppSharedPreference()
    var deleteUserProtocol: DeleteUserProtocol?
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    var sync = SyncToastMessages.shared

    @IBOutlet weak var deleteProfileButton: UIButton!
    @IBOutlet weak var deleteProfileSubheading: UILabel!
    @IBOutlet weak var deleteProfileHeading: UILabel!
    @IBOutlet weak var deleteButton: UILabel!
    @IBOutlet weak var cancelButton: UILabel!
    @IBAction func deleteButtonTapped(_ sender: Any) {
        self.nextVCCall()
    }
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.prevVCCall()
    }
    @IBOutlet weak var popUpView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.setLocalizationText()
        self.addObservers()

        self.popUpView.layer.cornerRadius = 6
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0
    }

    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
      DispatchQueue.main.async {
            self.deleteProfileHeading.text = self.profilemsg.deleteprofileheading
            self.deleteProfileSubheading.text = self.profilemsg.deleteprofilesubheading
            self.deleteProfileButton.setTitle(self.profilemsg.deleteButtonTitle, for: .normal)
            self.cancelButton.text = self.sync.cancel
      }
    }
    /* END */

    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */

    func nextVCCall() {
        deleteUserProtocol?.deleteData(toReload: true)
        self.dismiss(animated: true, completion: nil)
    }
    func prevVCCall() {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
