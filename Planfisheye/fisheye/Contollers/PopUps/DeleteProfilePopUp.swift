//
//  DeleteProfilePopUp.swift
//  fisheye
//
//  Created by Sankey Solutions on 18/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
class deleteProfilePopUp: UIViewController {

    @IBOutlet weak var closeButton: UIButton!
    let appSharedPrefernce = AppSharedPreference()
    var deleteUserProtocol: DeleteUserProtocol?

    @IBOutlet weak var popUpView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.popUpView.layer.cornerRadius = 6
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0
    }

    func nextVCCall() {
        deleteUserProtocol?.deleteData(toReload: true)
        self.dismiss(animated: true, completion: nil)
    }
    func prevVCCall() {
            self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
