//
//  AllPopupDualActionVC.swift
//  Created by Sankey Solution on 12/1/17.
//  Copyright © 2017 Fisheye. All rights reserved.
//  Description: The View controller is designed to handle the Logout functionality of the Fisheye App

import UIKit

class AllPopupDualActionVC: UIViewController {

    let appSharedPrefernce = AppSharedPreference()
    let appService = AppService.shared
    var headingPopUp: String = ""
    var subHeadingPopUp: String = ""
    var nextAction: String = ""
    var prevAction = ""
    var method: String = ""
    var module: String = ""
    @IBOutlet weak var warningImage: UIImageView!
    @IBOutlet weak var dismissImage: UIImageView!
    @IBOutlet weak var successImage: UIImageView!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var subHeading: UILabel!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var quitButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var continueButtonLabel: UILabel!
    @IBOutlet weak var quitButtonLabel: UILabel!
    var redirectProtocol: RedirectToPrivacyControl?
    var reloadLogbook: RedirectedToMeetmeLogbookVC?
    var deletePulseProtocol: DeletePulseProtocol?
    var insertCapsuleProtocol: InsertCapsuleProtocol?
    var pulseListingProtocol: PulseListingProtocol?
    var pulseInSyncProtocol : PulseInSyncProtocol?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        initialisingView()
        getData()
        dispPopUpMethod()

    }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.AllPopupDualActionVC)
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    func getData() {
        self.headingPopUp = (self.appSharedPrefernce.getAppSharedPreferences(key: "heading") as? String)!
        self.subHeadingPopUp = (self.appSharedPrefernce.getAppSharedPreferences(key: "subheading") as? String)!
        self.nextAction = (self.appSharedPrefernce.getAppSharedPreferences(key: "action") as?  String)!
        self.method = (self.appSharedPrefernce.getAppSharedPreferences(key: "method") as?  String)!
        self.module = (self.appSharedPrefernce.getAppSharedPreferences(key: "module") as?  String)!

        self.quitButtonLabel.text = self.appService.quit

        self.heading.text = self.headingPopUp
        self.subHeading.text = self.subHeadingPopUp
        continueButtonLabel.text = self.appService.continue2
        if  method == "deleteMeetme"  || method == "declineMeetme" || method=="removeParticipant"  || method=="stopMeetme"{

           quitButtonLabel.text = self.appService.cancel

        }
        if method == "previousPageConfirmation"{
//            var cancel: String = "Cancel"
//            var continue2: String = "Continue"
            quitButtonLabel.text = self.appService.continue2
            continueButtonLabel.text = self.appService.cancel
        }
        if method=="deletePulse" {
            quitButtonLabel.text = self.appService.cancel
            continueButtonLabel.text = self.appService.continue2
        }
//        if method == ""
    }

    func initialisingView() {
        self.warningImage.isHidden = true
        self.dismissImage.isHidden = true
        self.successImage.isHidden = true
        self.heading.text = ""

        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0

        self.popUpView.layer.cornerRadius = 6
    }

    func dispPopUpMethod() {
        self.warningImage.isHidden = false
        self.dismissImage.isHidden = true
        self.successImage.isHidden = true
    }

    @IBAction func okButtonTapped(_ sender: Any) {
        nextButtonFunction()
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        prevButtonFunction()
    }

    func prevButtonFunction() {
        switch method {
        case "disagreeConfirmation":
            self.dismiss(animated: true, completion: nil)

        case "previousPageConfirmation":
            self.dismiss(animated: true, completion: nil)
            break

        case "deleteMeetme":
            self.appSharedPrefernce.removeAppSharedPreferences(key: "meetMeIndex")
            self.dismiss(animated: true, completion: nil)
            break

        case "declineMeetme":
            self.appSharedPrefernce.removeAppSharedPreferences(key: "meetMeIndex")
            self.dismiss(animated: true, completion: nil)
            break

        case "stopMeetme":
            self.dismiss(animated: true, completion: nil)
            break

        case "deletePulse":
            self.dismiss(animated: true, completion: nil)
            break
            
        case "deletePulseFromList":
            self.dismiss(animated: true, completion: nil)
            break

        case "declinePulse":
            self.dismiss(animated: true, completion: nil)
            break

        case "removeParticipant":
            self.appSharedPrefernce.removeAppSharedPreferences(key: "sectionToDelete")
            self.appSharedPrefernce.removeAppSharedPreferences(key: "rowToDelete")
            self.dismiss(animated: true, completion: nil)
            break

        case "declinePulseAtList":
            self.dismiss(animated: true, completion: nil)
            break

        case "acceptPulse":
            self.dismiss(animated: true, completion: nil)
            break

        case "acceptPulseAtDetails":
            self.dismiss(animated: true, completion: nil)
            break
            
        case "sendPulseReqFromSync":
            self.dismiss(animated: true, completion: nil)
            break
            
        case "sendPulseShareFromSync":
            self.dismiss(animated: true, completion: nil)
            break

        case "addReqContToRespToPulseReq":
            self.dismiss(animated: true, completion: nil)
            break

        default:
            self.dismiss(animated: true, completion: nil)
            break
        }

    }

    func nextButtonFunction() {
        let methodCalled = self.method

        switch methodCalled {

        case "disagreeConfirmation":
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Disagree Terms & Conditions", label: "Continue to Disagree Terms & Conditions - Close app", value: 0)
            UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)

        case "previousPageConfirmation":
            redirectProtocol?.redirect(toRedirect: true)
            self.dismiss(animated: true, completion: nil)
            break

        case "editPagePreviousPage":
            self.dismiss(animated: true, completion: nil)
            break

        case "deleteMeetme":
            reloadLogbook?.nextAction(method: "deleteMeetme")
            self.dismiss(animated: true, completion: nil)
            break

        case "declineMeetme":
            reloadLogbook?.nextAction(method: "declineMeetme")
            self.dismiss(animated: true, completion: nil)
            break

        case "stopMeetme":
             reloadLogbook?.nextAction(method: "stopMeetme")
            self.dismiss(animated: true, completion: nil)
            break

        case "removeParticipant":
            reloadLogbook?.nextAction(method: "removeParticipant")
            self.dismiss(animated: true, completion: nil)
            break

        case "deletePulse":
            self.dismiss(animated: true, completion: {
                self.deletePulseProtocol?.toDeletePulse(toReload: true)
            })
            break
            
        case "deletePulseFromList":
            self.dismiss(animated: true, completion: {
                self.pulseListingProtocol?.toDeletePulse(toReload: true)
            })
            break

        case "declinePulse":
            self.dismiss(animated: true, completion: {
                self.deletePulseProtocol?.toDeclinePulse(toReload: true)
            })
            break

        case "declinePulseAtList":
            self.dismiss(animated: true, completion: {
                self.pulseListingProtocol?.toDeclinePulse(toReload: true)
            })
            break

        case "acceptPulse":
            self.dismiss(animated: true, completion: {
               self.pulseListingProtocol?.toEnterCapsuleWhileAcceptingPulse(toReload: true)
            })
            break
            
        case "sendPulseReqFromSync":
            self.dismiss(animated: true, completion: {
                self.pulseInSyncProtocol?.toEnterCapsuleWhileSendingPulseRequestFromSync(toReload: true)
            })
            break
        
        case "sendPulseShareFromSync":
            self.dismiss(animated: true, completion: {
                self.pulseInSyncProtocol?.toEnterCapsuleWhileSendingPulseShareFromSync(toReload: true)
            })
            break


        case "acceptPulseAtDetails":
            self.dismiss(animated: true, completion: {
                self.deletePulseProtocol?.toEnterCapsuleWhileAcceptingPulse(toReload: true)
            })
            break

        case "addReqContToRespToPulseReq":
            //open add contact page of sync module,so that user will add requester to his contact list
            self.navigateToDashboard()
            break

        default:
            print("Default Statement Called")
        }
    }

    func navigateToDashboard() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
        nextVC.isToOpenAddContact=true
        self.present(nextVC, animated: true, completion: nil)
    }
}
