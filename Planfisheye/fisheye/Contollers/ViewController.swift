//
//  ViewController.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import AWSAuthCore

class ViewController: UIViewController {

    var isCalled = false
    var userInfo: NSDictionary?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

//        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//        }
//        
//        
//        let locale = NSLocale.current
//        
//        let unsortedCountries = NSLocale.isoCountryCodes.map { locale.localizedString(forRegionCode: $0)! }
//        let sortedCountries = unsortedCountries.sorted()
//        
//        
//        let countries = NSLocale.isoCountryCodes.map { (code:String) -> String in
//            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
//            return NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
//        }

            self.loadWindow()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - View lifecycle

    func onSignIn (_ success: Bool) {
        // handle successful sign in
        if (success) {

//            if(userInfo != nil && (userInfo?.count)!>0)
//            {
//                DispatchQueue.main.async {
//                    kSweetAlert.showAlert("Message!", subTitle: "Access Success!", style: AlertStyle.success)
//                    
//                }
//            }
//            self.view.alpha = 1
            self.loadBaseViewController()

        } else {
            // handle cancel operation from user
        }
    }

    func loadWindow() {

        kApplicationDelegate.nVC = self.navigationController
    CommonMethods.showProgressView()
//        if CommonMethods.retriveIdToken() != nil {
        AWSSignInManager.sharedInstance().resumeSession(completionHandler: { (result: Any?, error: Error?) in

            CommonMethods.hideProgressView()
            if !self.isCalled {
                self.isCalled = true
            if  AWSSignInManager.sharedInstance().isLoggedIn {

                self.verifyNumberandLoadViewController()

            } else {

                self.loadLoginViewController()
            }
            }
        })

//             let task = AWSGoogleSignInProvider.sharedInstance().token().continueWith(block: { (response) -> Any? in
////                    CommonMethods.saveIdToken(response.result! as String)
//                if response.error != nil {
//                    
//                    self.verifyNumberandLoadViewController()
//                }
//             
//                    return response.result
//                })

//                task.waitUntilFinished()

//            else{
//                AWSCognitoUserPoolsSignInProvider.sharedInstance().token().continueWith(block: { (response) -> Any? in
////                    self.verifyNumberandLoadViewController()
//                    return nil
//                })

//                let pool = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool()
//                pool.configuration
//                
//                AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool().configuration.credentialsProvider.credentials().continueWith(block: { (res) -> Any? in
//                })

//                AWSSignInManager.sharedInstance().login(signInProviderKey: AWSCognitoUserPoolsSignInProvider.sharedInstance().identityProviderName, completionHandler: { (result, authState, error) in
//                    
//                    if let res = result{
//                    }
//                    
//                    
//                })
//            }

//        }

    }

    func verifyNumberandLoadViewController() {
        WebServiceManager.shared().verifyPhoneNumber(handler: { (result, _, success) in
            if success {
                CommonMethods.hideProgressView()
                DispatchQueue.main.async {
                    let res = result as! FEPhoneNumberResponse
                    if res.phoneNumberExist == 1 {
                        if res.phonenumberVerified == 1 {
                            //                            self.view.alpha = 1.0
                            self.loadBaseViewController()
                        } else {
                            self.loadOTPViewConrollerWith(phoneNum: res.phonenumber, countryCode: res.countryCode)
                        }
                    } else {
                        self.loadSendOTPVC()
                    }
                }
            }
        })
    }

    func loadBaseViewController() {

//        DispatchQueue.main.async {
////            kSweetAlert.showAlert("Message!\(self.userInfo)", subTitle: "Access denied!", style: AlertStyle.error)
//            kSweetAlert.showAlert("Message!", subTitle: "Access \(self.userInfo)", style: AlertStyle.warning, buttonTitle: "OK")  {(sucess)->Void in
//
//            }
//        }
        let baseVC  = BaseViewController.instantiateFromStoryboardWithIdentifier(identifier: "BaseViewController")
        baseVC.userInfo = self.userInfo
        kApplicationDelegate.nVC = UINavigationController(rootViewController: baseVC)
        kApplicationDelegate.nVC.navigationBar.isHidden  = true
        kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC

    }

    func loadLoginViewController() {
        let loginVC  = LoginViewController.instantiateFromStoryboardWithIdentifier(identifier: "LoginViewController")
        kApplicationDelegate.nVC = UINavigationController(rootViewController: loginVC)
        kApplicationDelegate.nVC.navigationBar.isHidden  = true
        kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC

    }

    func loadOTPViewConrollerWith(phoneNum: String?, countryCode: String?) {

        let loginVC  = LoginViewController.instantiateFromStoryboardWithIdentifier(identifier: "LoginViewController")
        loginVC.phoneNumber = phoneNum
        loginVC.countryCode = countryCode
        loginVC.showOTPScreen = true
        kApplicationDelegate.nVC = UINavigationController(rootViewController: loginVC)
        kApplicationDelegate.nVC.navigationBar.isHidden  = true
        kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC

    }

    func loadSendOTPVC() {

        let loginVC  = LoginViewController.instantiateFromStoryboardWithIdentifier(identifier: "LoginViewController")
        loginVC.showSendOTPScreen = true
        kApplicationDelegate.nVC = UINavigationController(rootViewController: loginVC)
        kApplicationDelegate.nVC.navigationBar.isHidden  = true
        kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC
    }

}
