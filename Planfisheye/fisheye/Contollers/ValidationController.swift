//
//  ValidationController.swift
//  fisheye
//
//  Created by Sankey Solutions on 16/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

public class ValidationController: NSObject {

    func isValidEmail(testStr: String) -> Bool {
        var emailRegEx = "";
        
        if(ProfileToastMsgHeadingSubheadingLabels.shared.emailRegX == nil || ProfileToastMsgHeadingSubheadingLabels.shared.emailRegX == ""){
            emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        }else{
            emailRegEx = ProfileToastMsgHeadingSubheadingLabels.shared.emailRegX!
        }
        

        let emailTest = NSPredicate(format: "SELF MATCHES %@", (emailRegEx))
        return emailTest.evaluate(with: testStr)
    }

    func isValidPassword(testStr: String) -> Bool {
//        let passwordRegEx="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!|@#$&?_+=%^(){}:;'<>.,/`~*])(?=\\S+$).{6,15}$"
        var passwordRegEx = "";
        if(ProfileToastMsgHeadingSubheadingLabels.shared.passwordRegX == nil || ProfileToastMsgHeadingSubheadingLabels.shared.passwordRegX == ""){
            passwordRegEx = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,15}$"
        }else{
            passwordRegEx = ProfileToastMsgHeadingSubheadingLabels.shared.passwordRegX!
        }
//      let passwordRegEx="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&%()+,-./:;<=>?$:€£¥₩₹¢&:§:„»«.:…?:¿!:¡%:{}^_~`*])(?=\\S+$).{6,15}$"

//       let passwordRegEx="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&%()+,-./:;<=>?[o:ôöòóœøōõ]a:àáâäæãåās:ßśše:èéêëēėęy:ÿu:ûüùúūi:îïíīįì$:€£¥₩₹¢&:§“:””„»«.:…?:¿!:¡%:‰l:łz:žźżc:çćčn:ñń0:°-:–—•*])(?=\\S+$).{6,15}$"

        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: testStr)

    }

      func isValidWebsite(url: String) -> Bool {
            let urlRegEx = "((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
            let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
            let result = urlTest.evaluate(with: url)
            return result
      }

    func getFirstChar(givenString: String!) -> String {
        var stringNeed = ""
        if givenString != ""{
            let stringInputArr = givenString.components(separatedBy: " ")

           stringNeed = stringInputArr[0]
        } else {
            stringNeed = "X"
        }
        return stringNeed
    }

    func isValidMeetDate(givenString: String!) -> Bool {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/YYYY"

        if dateFormatterGet.date(from: givenString!) != nil {

            return true
        } else {

            return false
        }
    }

    func getDateFromString(givenStringDate: String!) -> Date {
        let dateFormatterGet = DateFormatter()
        let dateFormatterConverter = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/YYYY"
        dateFormatterConverter.dateFormat = "dd/MM/YYYY"
        let formattedDate = dateFormatterGet.date(from: givenStringDate)
        let convertedDate = dateFormatterConverter.string(from: formattedDate!)
        let reconvertedDate = dateFormatterConverter.date(from: convertedDate)
        return reconvertedDate!
    }

    func getTodaysDate() -> String {
        let todaysDate = Date()

        let dateFormatterGet = DateFormatter()
        let dateFormatterConvert = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy"
        dateFormatterConvert.dateFormat = "dd/MM/yyyy"
        let formattedDate = dateFormatterGet.string(from: todaysDate)
        return formattedDate
    }

    func getTodaysDateInTimeStamp() -> Double {
        let date = Double(Date().timeIntervalSince1970)*1000

        return  date.roundToDecimal(0)
    }

    func getTodaysDateTime() -> String {
        let todaysDate = Date()

        let dateFormatterGet = DateFormatter()
        let dateFormatterConvert = DateFormatter()

        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatterConvert.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let formattedDate = dateFormatterGet.string(from: todaysDate)

        return formattedDate
    }
    func getTodaysDateTimeForDigitalSignature() -> String {
        let todaysDate = Date()

        let dateFormatterDate = DateFormatter()
        let dateFormatterHour = DateFormatter()
        let dateFormatterMinute = DateFormatter()
        let dateFormatterSecond = DateFormatter()

        dateFormatterDate.timeZone =  TimeZone(abbreviation: "UTC")
        dateFormatterHour.timeZone =  TimeZone(abbreviation: "UTC")
        dateFormatterMinute.timeZone =  TimeZone(abbreviation: "UTC")
        dateFormatterSecond.timeZone =  TimeZone(abbreviation: "UTC")

        dateFormatterDate.dateFormat = "yyyyMMdd"
        dateFormatterHour.dateFormat = "HH"
        dateFormatterMinute.dateFormat = "mm"
        dateFormatterSecond.dateFormat = "ss"

        let formattedDate = dateFormatterDate.string(from: todaysDate as Date)
        let formattedHour = dateFormatterHour.string(from: todaysDate as Date)
        let formattedMinute = dateFormatterMinute.string(from: todaysDate as Date)
        let formattedSecond = dateFormatterSecond.string(from: todaysDate as Date)
         let finalString = formattedDate + "T" + formattedHour + formattedMinute + formattedSecond + "Z"

        return finalString
    }
    func getTodaysDateTimeForDigitalSignature2() -> String {
        let todaysDate = Date()

        let dateFormatterDate = DateFormatter()

        dateFormatterDate.timeZone =  TimeZone(abbreviation: "UTC")

        dateFormatterDate.dateFormat = "yyyyMMdd"

        let formattedDate = dateFormatterDate.string(from: todaysDate as Date)

        return formattedDate
    }

    func isValidMeetTime(givenString: String!) -> Bool {
        let dateFormatterGet = DateFormatter()
         dateFormatterGet.dateFormat = "HH:mm"

        if dateFormatterGet.date(from: givenString!) != nil {
            return true
        } else {
            return false
        }
    }

    //  Converting date and time into UTC format
    func get_Date_time_from_UTC_time(givenDateTime: Double) -> String {

        let fractionDate = givenDateTime/1000
        let dateformattor = DateFormatter()
        dateformattor.dateFormat = "HH:mm dd-MM-yyyy"
        dateformattor.timeZone = NSTimeZone.local
        let date = Date(timeIntervalSince1970: fractionDate)

        return dateformattor.string(from: date)
    }

    func get_Date_time_from_UTC_timeSwift(givenDateTime: Double) -> String {

        let dateformattor = DateFormatter()
        dateformattor.dateFormat = "HH:mm dd-MM-yyyy"
        dateformattor.timeZone = NSTimeZone.local
        let date = Date(timeIntervalSince1970: givenDateTime)

        return dateformattor.string(from: date)
    }

    func convert_time_from_UTC_time(givenUTCtime: Double) -> String {
         let fractionDate = givenUTCtime/1000
        let dateFormatter = DateFormatter()
        let date = Date(timeIntervalSince1970: fractionDate)
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm"

        return dateFormatter.string(from: date)
    }

    func convert_date_from_UTC_time(givenUTCdate: Double) -> String {
        let dateFormatter = DateFormatter()
        let fractionDate = givenUTCdate/1000
        let date = Date(timeIntervalSince1970: fractionDate)
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style

        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }

    func calculateDaysBetweenTwoDates(start: String, end: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone =  NSTimeZone.local
        let currentCalendar = Calendar.current
        let startDate = dateFormatter.date(from: start)
        let endDate = dateFormatter.date(from: end)

        guard let start = currentCalendar.ordinality(of: .day, in: .era, for: startDate!) else {
            return 0
        }
        guard let end = currentCalendar.ordinality(of: .day, in: .era, for: endDate!) else {
            return 0
        }
        return end - start
    }

    func getTimeStampFromStringDate(stringDate: String) -> Double {
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="HH:mm dd-MM-yyyy"
        let date = dfmatter.date(from: stringDate)
        let dateStamp = date!.timeIntervalSince1970

        return dateStamp
    }

    func getTodaysDateTimeValidation() -> String {
        let todaysDate = Date()

        let dateFormatterGet = DateFormatter()

        dateFormatterGet.dateFormat = "HH:mm dd-MM-yyyy"

        let formattedDate = dateFormatterGet.string(from: todaysDate)
       return formattedDate
    }

    /* --------------------------------- used in pulse module -------------------------------------*/

    func convert_time_from_UTC_time_fromStringIO(givenUTCtime: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: givenUTCtime)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm"

        return dateFormatter.string(from: dt!)
    }

    func convert_date_from_UTC_time_fromStringIO(givenUTCdate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: givenUTCdate)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "dd/MM/yyyy"

        return dateFormatter.string(from: dt!)
    }
}
