//
//  AppConfig.swift
//  fisheye
//
//  Created by Sankey Solutions on 10/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
struct AppConfig {
// URL CONSTATS
    private struct Domains {
        static let Dev = "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/dev"
        static let Prod = ""
   }

    private static let Domain = Domains.Dev

    static let BaseURL = Domain

    // API URLS

    //To Get Profile Url
    static var getFisheyeUserProfile: String {
        return BaseURL + "/users/getuserdetails"
    }
    //End of to Get Profile Url

    //To update Profile Url
    static var updateFisheyeBasicUserProfile: String {
        return BaseURL + "/users/updateuserdetails"
    }
     //End of to update Profile Url

    //To Get Terms And Conditions Url
    static var termsAndConditions: String {
        return "https://s3.eu-west-2.amazonaws.com/fisheye-content-storage/ProfileImages/TermsAndConditionsFisheye.html"
    }
     //End of to Get Terms And Conditions Url

    //To Add New Fisheye User Basic Url
    static var addBasicInfo: String {
        return BaseURL + "/users/adduserdetails"
    }
    //End of to Add New Fisheye User Basic Url

    //To Send Otp Url
    static var sendOTP: String {
        return BaseURL + "/phonenumber/sendotp"
    }
    //End of to Send Otp Url

    //To update Terms and Condition flag
    static var updateTNC: String {
        return BaseURL + "/users/user/accepttermsnconditions"
    }
    //End of to Send Otp Url

    //To modifyUserDetails
    static var modifyUserDetails: String {
        return BaseURL + "/users/updateuserdetailsforregistration"
    }
    //End of modifyUserDetails

    //To reset Password
    static var resetPwdDetails: String {
        return BaseURL + "/password/changepassword"
    }
    //End of reset Password

    //To reset MasterKey
    static var resetMasterKeyDetails: String {
        return BaseURL + "/users/user/updatemasterkey"
    }
    //End of reset MasterKey

    //To reset Add Address
    static var addAddressDetails: String {
        return BaseURL + "/address/addaddress"
    }
    //End of Add Address

    //To reset Update Address
    static var updateAddress: String {
        return BaseURL + "/address/updateaddress"
    }
    //End of Update Address

    //To reset Profile Mobile and Email
    static var updateBasicDetails: String {
        return BaseURL + "/phonenumber/newnumbersendotp"
    }
    //End of To reset Profile Mobile and Email
}
