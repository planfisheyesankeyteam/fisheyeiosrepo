//
//  ConnectionManager.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 08/09/2018.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation
import Reachability

let SEND_UNSEND_MESSAGES = Notification.Name("SendUnsendMessages")

class ConnectionManager {

      static let sharedInstance = ConnectionManager()
      private var reachability: Reachability!
      var meetMeCapsuleVC = MeetMeCapsuleVC.shared

      func observeReachability() {
            self.reachability = Reachability()
            NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
            do {
                  try self.reachability.startNotifier()
            } catch(let error) {
            }
      }

      @objc func reachabilityChanged(note: Notification) {
             let reachability = note.object as! Reachability
            switch reachability.connection {
            case .cellular:
                self.meetMeCapsuleVC.addUnsentCapToDBOnNetworkDetection()
                  break
            case .wifi:
                  self.meetMeCapsuleVC.addUnsentCapToDBOnNetworkDetection()
                  break
            case .none:
                  do {
                        try self.reachability.startNotifier()
                  } catch(let error) {
                        
                  }
                  break
            }
      }
}
