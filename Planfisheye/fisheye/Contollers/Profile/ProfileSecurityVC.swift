//
//  ProfileSecurityVC.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 14/01/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class ProfileSecurityVC: UIViewController {

      let appService = AppService.shared
      let appSharedPrefernce = AppSharedPreference()
      var obj: [String: Any]?
      var idtoken = ""
      var fisheyeId = ""
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared

      @IBOutlet weak var changePwdUnderLine: UIView!
      @IBOutlet weak var masterKeyUnderLine: UIView!
      @IBOutlet weak var masterKeyView: UIView!
      @IBOutlet weak var deleteButtonUnderline: UIView!
      @IBOutlet weak var socialLoginDeleteButonView: UIView!
      @IBOutlet weak var changePwdButtonView: UIView!
      @IBOutlet weak var changeMasterKeyButton: UIButton!

      @IBOutlet weak var changePasswordLabel: UILabel!
      @IBOutlet weak var deleteProfileLabel2: UILabel!
      @IBOutlet weak var deleteProfileLabel: UILabel!
      @IBOutlet weak var chnageMasterKeyLabel: UILabel!
      @IBOutlet weak var securityLabel: UILabel!
      @IBAction func changeMasterKeyButtonTapped(_ sender: Any) {
        if self.chnageMasterKeyLabel.text == self.profilemsg.chnageMasterKey{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let privacynextVC = storyBoard.instantiateViewController(withIdentifier: "ProfileChangeMasterKeyVC")  as! ProfileChangeMasterKeyVC
            self.present(privacynextVC, animated: true, completion: nil)
        }else{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let privacynextVC = storyBoard.instantiateViewController(withIdentifier: "ProfileAddMasterKeyVC")  as! ProfileAddMasterKeyVC
            privacynextVC.toSmallTextChange = self as ToSmallTextChange
            self.present(privacynextVC, animated: true, completion: nil)
        }
      }
      @IBOutlet weak var changePasswordButton: UIButton!
      @IBAction func changePasswordButtonTapped(_ sender: Any) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let privacynextVC = storyBoard.instantiateViewController(withIdentifier: "ProfileChangePasswordVC")  as! ProfileChangePasswordVC
            self.present(privacynextVC, animated: true, completion: nil)
      }
      @IBOutlet weak var normalDeleteAccountView: UIView!
      @IBOutlet weak var fisheyeDeleteAccount: UIButton!
      @IBAction func fisheyeDeleteAccountTapped(_ sender: Any) {
            self.deleteVCController()
      }
      @IBOutlet weak var socialDeleteAccount: UIButton!
      @IBOutlet weak var mainView: UIView!
      @IBAction func socialDeleteAccountTapped(_ sender: Any) {
            self.deleteVCController()
      }
      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()

            self.setLocalizationText()
            self.masterKeyUnderLine.isHidden = true
            self.changePwdUnderLine.isHidden = true
            self.deleteButtonUnderline.isHidden = true
            self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as? [String: Any]
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "id") as? String ?? ""

            let appLoginMethod = self.appSharedPrefernce.getAppSharedPreferences(key: "loginMethod") ?? ""

            if(appLoginMethod as! String == "amazon" || appLoginMethod as! String == "google" || appLoginMethod as! String == "facebook") {
                  self.socialLoginPwdFieldHide()
                  self.deleteButtonUnderline.isHidden = true
            } else {
                  self.normalLoginPwdFieldHide()
            }
            self.masterKeyView.layer.cornerRadius = 6
            self.changePwdButtonView.layer.cornerRadius = 6
            self.socialLoginDeleteButonView.layer.cornerRadius = 6
            self.normalDeleteAccountView.layer.cornerRadius = 6
            // Do any additional setup after loading the view.
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.securityLabel.text = self.profilemsg.securityLabel
                  self.changePasswordLabel.text = self.profilemsg.chnagePasswordLabel
                  self.deleteProfileLabel.text = self.profilemsg.deleteProfileLabel
                  self.deleteProfileLabel2.text = self.profilemsg.deleteProfileLabel
                  let obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
                if let tempObj = obj {
                        let masterKey = tempObj["masterKey"]  as? String
                        if masterKey != nil && masterKey != ""{
                            self.chnageMasterKeyLabel.text = self.profilemsg.chnageMasterKey
                        }else{
                            self.chnageMasterKeyLabel.text = self.profilemsg.addMasterKey
                        }
                    }
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileSecurityVC)
      }

      //Function for redirection Verification page where Otp will be send to respective email and mobile number where it will be verified and allow the user to update its email id and mobile number
      func  profileVerificationPageRedirection(process: String) {

            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ProfileOTPVerificationVC")  as! ProfileOTPVerificationVC
            nextVC.redirectProtocol = self as? RedirectProtocol
            nextVC.process = process
            self.present(nextVC, animated: true, completion: nil)

      }
      //End of Function for redirection Verification page where Otp will be send to respective email and mobile number where it will be verified and allow the user to update its email id and mobile number

      //Function to Call Send OTP Service
      func sendOtpToRegisteredNumber(process: String) {
            self.startLoader()
            DispatchQueue.main.async {
                  self.changeMasterKeyButton.isUserInteractionEnabled = false
                  self.fisheyeDeleteAccount.isUserInteractionEnabled = false
                  self.socialDeleteAccount.isUserInteractionEnabled = false
            }
        if let userObj = self.obj {
                  let email = userObj["email"] as? String ?? ""
                  let countrycode = userObj["countryCode"] as? String ?? ""
                  let mobile = userObj["phonenumber"] as? String ?? ""
                  var sendOtpObject = SendOTPObject()
                  sendOtpObject.email = email
                  sendOtpObject.countryCode = countrycode
                  sendOtpObject.phonenumber = mobile

                  let sendOtp = sendOtpUpdateOperation(sendOtp: sendOtpObject)

                  sendOtp.addDidFinishBlockObserver { [unowned self] (_, _) in
                        let status = self.appSharedPrefernce.getAppSharedPreferences(key: "sendOtpStatus") as? String ?? ""
                        self.stopLoader()
                        DispatchQueue.main.async {
                              self.changeMasterKeyButton.isUserInteractionEnabled = true
                              self.fisheyeDeleteAccount.isUserInteractionEnabled = true
                              self.socialDeleteAccount.isUserInteractionEnabled = true
                        }
                        if status == "200" {
                              self.stopLoader()
                              if process == "changeMasterKey"{
                                    DispatchQueue.main.async {
                                          self.profileVerificationPageRedirection(process: "changeMasterKey")

                                    }

                              }
                              if process == "deleteProfile"{
                                    DispatchQueue.main.async {
                                          self.profileVerificationPageRedirection(process: "deleteProfile")
                                    }

                              }
                        } else if status == "NetworkError" {
                              self.stopLoader()
                              //   self.popUpDetails(heading : "Network Failure",  subheading : "Please check for Internet connection.",  action : "failure", method : "dismiss", module : "NA",  status: "close")
                              self.showToastMsg(toastMsg: self.profilemsg.networkfailureMsg)

                        } else {
                              self.stopLoader()
                              // self.popUpDetails(heading : "Server Error",  subheading : "Server Failure Occurred.Please try Again Later",  action : "failure", method : "dismiss", module : "NA",  status: "close")
                              self.showToastMsg(toastMsg: self.profilemsg.networkfailureMsg)

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Delete fisheye account", label: "Delete fisheye account - OTP sending failed", value: 0)
                        }
                        DispatchQueue.main.async {
                                    //                        self.popUpVCController()
                                    //                        self.appSharedPrefernce.removeAppSharedPreferences(key: "sendOtpStatus")
                                    self.stopLoader()
                        }
                  }
                  AppDelegate.addProcedure(operation: sendOtp)
            }
      }

      func showToastMsg(toastMsg: String) {
            DispatchQueue.main.async {
                  self.view.makeToast(toastMsg)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                  self.view.hideToast()
            })
      }
      //End of Function to Call Send OTP Service

      //Function to start and stop the loader
      func startLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }

      }

      func stopLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.stopLoader()
            }

      }
      //End of Function to start and stop the loader

      // Function to fill up the Pop up details
      func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, status: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.appSharedPrefernce.setAppSharedPreferences(key: "closestatus", value: status)
      }
      // End of Function to fill up the Pop up details

      //Function to call PopUp
      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
            nextVC.deleteUserProtocol  = self as DeleteUserProtocol
            self.present(nextVC, animated: true, completion: nil)
      }
      // End of Function to call PopUp

      //Function to call Delete PopUp
      func deleteVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "DeleteMyProfileVC") as! DeleteMyProfileVC
            nextVC.deleteUserProtocol  = self as DeleteUserProtocol
            self.present(nextVC, animated: true, completion: nil)
      }
      // End of Function to call Delete PopUp

      // Functions created to hide or show the fields depending on the type of Login ie. Social Login or Normal Fisheye Login
      func socialLoginPwdFieldHide() {
            self.normalDeleteAccountView.isHidden = true
            self.changePwdButtonView.isHidden = true
            self.socialLoginDeleteButonView.isHidden = false
      }

      func normalLoginPwdFieldHide() {
            self.socialLoginDeleteButonView.isHidden = true
      }
      // End of Functions created to hide or show the fields depending on the type of Login ie. Social Login or Normal Fisheye Login

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

}

//Call to protocol to initiate Delete User Profile through Verification Process
extension ProfileSecurityVC: DeleteUserProtocol {
      func deleteData(toReload: Bool) {
            if toReload == true {
                  self.sendOtpToRegisteredNumber(process: "deleteProfile")
            }
      }
}
//End of Call to protocol to initiate Delete User Profile through Verification Process

extension ProfileSecurityVC: ToSmallTextChange {
    func masteraKeyTextChange() {
        self.chnageMasterKeyLabel.text = self.profilemsg.chnageMasterKey
    }
}
