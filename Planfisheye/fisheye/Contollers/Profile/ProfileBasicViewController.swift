//
//  ProfileTestViewController.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 01/09/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import AWSS3
import ProcedureKit
import MRCountryPicker
import Toast_Swift
import  DropDown
import SDWebImage

class ProfileBasicViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MRCountryPickerDelegate, UITableViewDelegate, UITableViewDataSource {

      let appService = AppService.shared
      var profileList = Profile()
      let appSharedPrefernce = AppSharedPreference()
      let validationController = ValidationController()
      var idtoken = ""
      var fisheyeId = ""
      var profileImageUrl = ""
      var verifyFlag = "0"
      var verificationStatus = "done"
      var oldMobileNumber = ""
      var newMobileNumber = ""
      var oldEmailId = ""
      var newEmailId = ""
      var oldcountryCode = ""
      var newcountryCode = ""
      var placeHolderOne = ""
      var placeHolderTwo = ""
      var placeHolderThree = ""
      var placeHolderFour = ""
      var placeHolderFive = ""
      var placeHolderSix = ""
      var placeHolderSeven = ""
      var placeHolderEight = ""
    let imagePickerController = UIImagePickerController()
    var minMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBelesserThan
    var maxMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBeGreaterThan

      var privacy: Bool? = false
      var validFlagName = true
      var validFlagCountryCode = true
      var validFlagPhoneNumber = true
      var isMarketingMessagingEnabled: Bool? = false
      //var genderArray=[String]()

      var maleflag = true
      var femaleflag = false
    
      var obj: [String: Any]?
      var profileenabled = ""
      var genderSelection = ""
      var oldProfileObject: [String: Any]?
      var isImportantDetailsChanged: Bool = false
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var previousProfileObject = PreviousProfileObject()
      let genderDropdown: DropDown = DropDown()
      var genderArray = ["Male", "Female", "Trans-Female", "Bi-Gender", "Non-Binary", "Gender nonconforming", "Undisclosed", "Rather not say" ]
      var englishGenderArray: [String] = []
      var selectedGenderIndex: Int = 7
      var selectedText: String!
      @IBOutlet weak var saveProfileButton: UIButton!
      @IBOutlet weak var saveProfileImage: UIImageView!
      @IBOutlet weak var zeroLabelUnderlineView: UIView!

      @IBOutlet weak var promotionalCommNote: UITextView!
      @IBOutlet weak var genderBlankUnderline: UIImageView!

      @IBOutlet var languagesTableView: UITableView!
      @IBOutlet var languageBlackUnderline: UIImageView!
      @IBOutlet weak var genderTextField: UICustomTextField!
      @IBOutlet weak var genderTableView: UITableView!
      @IBOutlet weak var genderBtn: UIButton!
      @IBOutlet weak var profileImageEditButton: UIButton!
      @IBOutlet weak var profilename: UICustomTextField!
      @IBOutlet weak var profileemail: UICustomTextField!
      @IBOutlet weak var profilenumber: UICustomTextField!
      @IBOutlet weak var profileimage: UIImageView!
      @IBOutlet weak var countryCodeField: UICustomTextField!
      @IBOutlet weak var privacySwitchButton: UISwitch!
      @IBOutlet weak var editProfileButton: UIButton!
      @IBOutlet weak var privacyDetail: UILabel!
      @IBOutlet weak var mobileNumberBlueUnderline: UIImageView!
      @IBOutlet weak var mobileNumberBlackUnderline: UIImageView!
      @IBOutlet weak var nameBlueUnderline: UIImageView!
      @IBOutlet weak var codeBlueUnderline: UIImageView!
      @IBOutlet weak var codeBlackUnderline: UIImageView!
      @IBOutlet weak var emailBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlackunderline: UIImageView!
      @IBOutlet weak var privacyDetailTextView: UITextView!
      
      @IBOutlet weak var privacyMessage: UILabel!
      //  @IBOutlet weak var privacyMessage: UITextField!
      @IBOutlet weak var dropDownButton: UIButton!
      @IBOutlet weak var nameBlackUnderline: UIImageView!
      @IBOutlet weak var verifyButton: UIButton!
      weak var delegate: Profiledelegate?
      @IBOutlet weak var editButtonImage: UIImageView!
      @IBOutlet weak var countryPicker: MRCountryPicker!
      @IBOutlet weak var scrollViewBasicProfile: UIScrollView!
      @IBOutlet weak var phoneNumberVerifyImage: UIImageView!
      @IBOutlet weak var Promotional: UISwitch!

      @IBOutlet weak var basicInformationLabel: UILabel!

      @IBOutlet weak var promotionalCommTextField: UITextView!
      @IBOutlet weak var promotionalCommLabel: UILabel!

      @IBOutlet weak var LanguageTextField: UICustomTextField!
      var LanguageArray = ["English", "Spanish", "Marathi", "Hindi", "Bengali", "French", "Arabic", "German", "Italian", "Dutch", "Japenese", "Russia", "Korean"]
      let languageDropDown: DropDown = DropDown()
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared

      @IBOutlet var LanguageButton: UIButton!

      var shouldRefresh: Bool!

      @IBAction func dropDownTapped(_ sender: Any) {
            scrollViewBasicProfile.isHidden = true
            countryPicker.isHidden = false
      }

      @IBAction func privacySwitchButtonTapped(_ sender: Any) {
            if(self.privacy == true && self.verifyButton.isHidden) {
                  self.privacy = false
                  self.privacyDetailTextView.text = self.profilemsg.privacydetaildisable
                  self.privacyMessage.text = self.profilemsg.privacyinfoshared
                  self.changePrivacySetting(isPrivacyEnable: self.privacy!)
            } else if(self.verifyButton.isHidden) {
                  self.privacyDetailTextView.text = self.profilemsg.privacydetailenabled
                  self.privacyMessage.text = self.profilemsg.privacymessagenotshared
                  if(self.verifyButton.isHidden) {
                        self.privacy = true
                        self.changePrivacySetting(isPrivacyEnable: self.privacy!)
                  }
            }
      }

      @IBAction func promotionalBtnTapped(_ sender: Any) {
            if(self.isMarketingMessagingEnabled == true && self.verifyButton.isHidden) {
                  self.isMarketingMessagingEnabled = false
                  self.savedata()
            } else if(self.verifyButton.isHidden) {
                  self.isMarketingMessagingEnabled = true
                  self.savedata()
            }
      }

      @IBAction func editProfileButtonTapped(_ sender: Any) {
            self.saveProfileButton.isHidden = false
            self.saveProfileImage.isHidden = false
            self.editButtonImage.isHidden = true
            self.nameBlackUnderline.isHidden = false
            self.emailBlackunderline.isHidden = false
            self.mobileNumberBlackUnderline.isHidden = false
            self.codeBlackUnderline.isHidden = false
            self.editProfileButton.isHidden = true
            self.zeroLabelUnderlineView.isHidden = false
            self.profileImageEditButton.isHidden = false
            self.genderBlankUnderline.isHidden = false
            self.languageBlackUnderline.isHidden = false

            self.userInterctionOn()
      }
      @IBAction func verifyButtonTapped(_ sender: Any) {
            verifybuttonTapped(newPhonenumber: profilenumber.text!, newEmail: profileemail.text!)

      }
      func verifybuttonTapped(newPhonenumber: String, newEmail: String) {
            self.verificationStatus = "pending"
            var isDetailsValidEmail = false
            var isDetailsValidPhoneNumber = false
            var isDetailsValidCountryCode = false
            if (countryCodeField.text?.characters.count)! <= 0 {
                  isDetailsValidCountryCode = false
                  self.countryCodeField.configOnError(withPlaceHolder: self.profilemsg.entercode)
            } else {
                  isDetailsValidCountryCode = true
                  self.countryCodeField.configOnErrorCleared(withPlaceHolder: self.profilemsg.code)
            }
        if (newPhonenumber.characters.count) > self.minMobNumDigits && (newPhonenumber.characters.count) < self.maxMobNumDigits {
                self.profilenumber.configOnErrorCleared(withPlaceHolder: self.profilemsg.phone)
                isDetailsValidPhoneNumber = true
                placeHolderSix = profilenumber.placeholder!

            } else {
                isDetailsValidPhoneNumber = false
                self.profilenumber.configOnError(withPlaceHolder: self.profilemsg.invalidphone)
                placeHolderFive = profilenumber.placeholder!
            }

            if  !validationController.isValidEmail(testStr: newEmail) {
                  self.profileemail.configOnError(withPlaceHolder: self.profilemsg.invalidemail)
                
                  placeHolderSeven = profileemail.placeholder!
                  isDetailsValidEmail = false
            } else {
                  self.profileemail.configOnErrorCleared(withPlaceHolder: self.profilemsg.email)
                  placeHolderEight = profileemail.placeholder!
                  isDetailsValidEmail = true
            }

            if(isDetailsValidEmail && isDetailsValidPhoneNumber && isDetailsValidCountryCode) {
                  sendotpFunctionCall()
            }

      }

      @IBAction func saveProfileButtonTappedNow(_ sender: Any) {
            savebuttonTapped(newProfileName: profilename.text!, newPhoneNumber: profilenumber.text! )
      }
      func savebuttonTapped(newProfileName: String, newPhoneNumber: String) {
            if (newProfileName.characters.count) <= 0 {
                  self.validFlagName = false
                  self.profilename.configOnError(withPlaceHolder: self.profilemsg.entername)
                  placeHolderOne = profilename.placeholder!
            } else {
                  self.validFlagName = true
            }

            if (countryCodeField.text?.characters.count)! <= 0 {
                  self.validFlagCountryCode = false
                  self.countryCodeField.configOnError(withPlaceHolder: self.profilemsg.entercode)
            } else {
                  self.validFlagCountryCode = true
            }
        
        if (newPhoneNumber.characters.count) > self.minMobNumDigits && (newPhoneNumber.characters.count) < self.maxMobNumDigits {
                self.validFlagPhoneNumber = true
            } else {
                self.validFlagPhoneNumber = false
                self.profilenumber.configOnError(withPlaceHolder: self.profilemsg.invalidphone)
                placeHolderTwo = profilenumber.placeholder!
            }

            if validFlagName &&  validFlagPhoneNumber && validFlagCountryCode {
                  self.savedata()
            }
      }

      @IBAction func profileImageEditButtonTapped(_ sender: Any) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
      }

      func getPreviousLocalization() {
            let preferredLanguage = self.appSharedPrefernce.getAppSharedPreferences(key: "preferredLanguage") as? String ?? "English"

            self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: preferredLanguage)
            self.LanguageTextField.text = preferredLanguage
      }

      func changeLanguage() {

            languageDropDown.anchorView = LanguageButton
            languageDropDown.dataSource = AppService.shared.languages
            languageDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                 self.getTranslatedText(updatelanguage: item)
                self.LanguageTextField.text = item
                self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: item)
                self.languageDropDown.hide()
                self.languageDropDown.dataSource = AppService.shared.languages
            }
      }

      //Function called when View will Load
      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.englishGenderArray = self.genderArray
            self.getPreviousLocalization()
            self.setLocalizationText()
            self.initializeGender()
            self.addObservers()
//            self.genderArray = [self.profilemsg.male, self.profilemsg.female, self.profilemsg.transFemale, self.profilemsg.biGender, self.profilemsg.nonBinary, self.profilemsg.genderNonConforming, self.profilemsg.undisclosed, self.profilemsg.ratherNotSay]

            self.userInterctionOff()
            self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
            self.verifyButton.isHidden = true
            self.oldProfileObject = self.obj
            self.profileImageEditButton.isHidden = true
            ScreenLoader.shared.stopLoader()
            self.saveProfileButton.setImage(UIImage(named: "AcceptFilled"), for: [])
            self.saveProfileButton.isHidden = true
            self.saveProfileImage.isHidden = true
            self.scrollViewBasicProfile.isDirectionalLockEnabled = true
            self.privacyMessage.isUserInteractionEnabled = false
            if scrollViewBasicProfile.contentOffset.x>0 {
                  scrollViewBasicProfile.contentOffset.x = 0

            }
            self.LanguageTextField.isUserInteractionEnabled = false
            countryPicker.countryPickerDelegate = self
            countryPicker.showPhoneNumbers = true

            // set country by its code
            countryPicker.setCountry("GB")

            // optionally set custom locale; defaults to system's locale
            countryPicker.setLocale("gb_GB")

            // set country by its name
            countryPicker.setCountryByName("London")
            if(self.obj == nil) {
                  fetchProfileDetails()
            } else {
                  showProfileDetails()
            }
        
            imagePickerController.delegate = self

            self.privacySwitchButton.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
            self.Promotional.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)

            self.profilename.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.profilename.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.profileemail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.profileemail.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.profilenumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.profilenumber.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.nameBlackUnderline.isHidden = true
            self.nameBlueUnderline.isHidden = true

            self.emailBlackunderline.isHidden = true
            self.emailBlueUnderline.isHidden = true

            self.mobileNumberBlackUnderline.isHidden = true
            self.mobileNumberBlueUnderline.isHidden = true

            self.codeBlackUnderline.isHidden = true
            self.codeBlueUnderline.isHidden = true
            self.genderBlankUnderline.isHidden = true
            self.languageBlackUnderline.isHidden = true

            self.zeroLabelUnderlineView.isHidden = true
            self.saveProfileButton.isUserInteractionEnabled = false
            self.Promotional.layer.cornerRadius = 16.0
            self.genderTextField.font = UIFont(name: "Montserrat-Regular", size: 16)
            self.LanguageTextField.font = UIFont(name: "Montserrat-Regular", size: 16)
            self.changeLanguage()

      }
      //End of Function called when View will Load

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.basicInformationLabel.text = self.profilemsg.basicInformation
                  self.promotionalCommNote.text = self.profilemsg.promotionalCommNote
                  self.promotionalCommLabel.text = self.profilemsg.promotionalCommTitle
                  self.promotionalCommTextField.text = self.profilemsg.promotionalCommText
                  self.showPrivacyMessages(privacySetForContact: self.privacy!)

                  self.privacyDetailTextView.text = self.profilemsg.privacyEnaDisaText
                  self.privacyMessage.text = self.profilemsg.personalInfoNotShared
                  self.LanguageTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.selectLanguage)
                  self.profilename.configOnErrorCleared(withPlaceHolder: self.profilemsg.name)
                  self.profileemail.configOnErrorCleared(withPlaceHolder: self.profilemsg.email)
                  self.countryCodeField.configOnErrorCleared(withPlaceHolder: self.profilemsg.code)
                  self.profilenumber.configOnErrorCleared(withPlaceHolder: self.profilemsg.phone)
                    self.genderTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.selectGenderPlaceholder)
                  self.genderArray = self.signupmsg.genderArrayList
                  self.genderDropdown.dataSource = self.genderArray
                  let objArray = self.signupmsg.localGenderArrayList.filter( { return ($0.genderActualText == self.selectedText ) } )
                 
                  if objArray != nil && objArray.count > 0
                  {
                        self.genderTextField.text = objArray[0].genderTranslatedText
                  }else {
                        self.genderTextField.text = self.selectedText
                  }
            }
      }
      /* END */
       func showPrivacyMessages(privacySetForContact: Bool)
       {
                              if (privacySetForContact == true){
                                    DispatchQueue.main.async{
                                          self.privacyDetail.text = self.profilemsg.privacyenabled
                                          self.privacySwitchButton.isOn = true
                                          self.privacyMessage.isUserInteractionEnabled = false
                                          self.privacySwitchButton.backgroundColor = UIColor.duckEggBlue()
                                          self.privacySwitchButton.layer.cornerRadius = 16.0
                                    }
                              } else {
                                    DispatchQueue.main.async{
                                          self.privacyDetail.text = self.profilemsg.privacydisabled
                                          self.privacySwitchButton.isOn = false
                                          self.privacyMessage.isUserInteractionEnabled = false
                                          self.privacySwitchButton.backgroundColor = UIColor.red
                                          self.privacySwitchButton.layer.cornerRadius = 16.0
                                    }
                              }
      }

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      func initializeGender() {

            self.genderArray = signupmsg.genderArrayList
            self.genderTableView.isHidden = true
            self.genderTextField.isUserInteractionEnabled = false
            self.genderDropdown.anchorView = self.genderTableView
            self.genderDropdown.dataSource = self.genderArray
            self.genderDropdown.width = 200
            self.genderDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                 
                  self.genderTextField.text = item
                  self.selectedGenderIndex = index
                
                let objArray = self.signupmsg.localGenderArrayList.filter( { return ($0.genderTranslatedText == item ) } )
                if objArray != nil && objArray.count > 0
                {
                 
                  self.genderTextField.text = objArray[0].genderTranslatedText
                  self.selectedText = objArray[0].genderActualText

                }else{
                  
                  self.genderTextField.text = self.signupmsg.gender[self.signupmsg.gender.count-1].genderTranslatedText
                  self.selectedText = self.signupmsg.gender[self.signupmsg.gender.count-1].genderActualText

                  }
               
               
            }
      }

      public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            var count: Int?
            if tableView == self.genderTableView {
                  count = self.genderArray.count
            } else if tableView == self.languagesTableView {
                  count = self.LanguageArray.count
            }
            return count!
      }

      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if tableView == self.genderTableView {
                 
                  self.genderTextField.text = self.genderArray[indexPath.row]
                  //            self.genderTextField.font = UIFont(name: "Montserrat-Regular", size: 16)
                  self.genderTableView.isHidden = true
            } else if tableView == self.genderTableView {
                  self.LanguageTextField.text = self.LanguageArray[indexPath.row]
                  self.LanguageTextField.isHidden = true
            }

      }

      public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var cell: UITableViewCell?
            if tableView == self.genderTableView {
                  let genderCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                  genderCell.textLabel?.text = self.genderArray[indexPath.row]
                  cell = genderCell
            } else if tableView == self.genderTableView {
                  let languageCell = tableView.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath)
                  languageCell.textLabel?.text = self.LanguageArray[indexPath.row]
                  cell = languageCell
            }
            return cell!
      }

      @IBAction func genderBtnTapped(_ sender: Any) {
            if  genderDropdown.isHidden {
                  self.genderDropdown.show()
            } else {
                  self.genderDropdown.hide()
            }
      }

      //Language Button Tapped
      @IBAction func LanguageBtnTapped(_ sender: Any) {
            if  languageDropDown.isHidden {
                  self.languageDropDown.show()
            } else {
                  self.languageDropDown.hide()
            }
      }

      //Function called when view will appear
      override func viewWillAppear(_ animated: Bool) {
            if shouldRefresh == true {
                  fetchProfileDetails()
            }
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileBasicViewController)
      }
      //End of Function called when view will appear

      //Function called when view did appear
      override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            profileimage.layer.cornerRadius =  self.profileimage.frame.size.height / 2
            profileimage.layer.masksToBounds = true
      }
      //End of Function called when view did appear

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      //Function to assign the details from the picker
      func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
            self.countryCodeField.text = phoneCode

            if oldcountryCode == ""{

            } else if oldcountryCode == self.countryCodeField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) {
                  self.verifyButton.isHidden = true
                  self.phoneNumberVerifyImage.isHidden = true
                  self.privacySwitchButton.isUserInteractionEnabled = true
                  self.Promotional.isUserInteractionEnabled = true
                  self.saveProfileButton.isUserInteractionEnabled = true
                  self.saveProfileButton.isHidden = false
                  self.saveProfileImage.isHidden = false
            } else {
                  self.verifyButton.isHidden = false
                  self.phoneNumberVerifyImage.isHidden = false
                  self.saveProfileButton.isHidden = true
                  self.saveProfileImage.isHidden = true
                  self.verificationStatus = "pending"
                  self.privacySwitchButton.isUserInteractionEnabled = false
                  self.Promotional.isUserInteractionEnabled = false
                  self.saveProfileButton.isUserInteractionEnabled = false
            }
            countryPicker.isHidden = true
            scrollViewBasicProfile.isHidden = false
      }
      //End of Function to assign the details from the picker
      //Function to Call Send OTP Service
      func sendotpFunctionCall() {

            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {

                    self.view.makeToast(profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        return
            }
            self.startLoader()
            self.verifyButton.isUserInteractionEnabled = false
            let countryCod = self.countryCodeField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let emailIdd = self.profileemail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let profilename = self.profilename.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let mobileNumber1 = self.profilenumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.newMobileNumber)
            self.appSharedPrefernce.setAppSharedPreferences(key: "countrycode", value: countryCod)
            self.appSharedPrefernce.setAppSharedPreferences(key: "email", value: emailIdd)
            self.appSharedPrefernce.setAppSharedPreferences(key: "profilename", value: profilename)
            self.appSharedPrefernce.setAppSharedPreferences(key: "profileurl", value: self.profileImageUrl)
            self.appSharedPrefernce.setAppSharedPreferences(key: "profileenabled", value: self.privacy)

            var sendOtpObject = SendOTPObject()
            sendOtpObject.email = emailIdd
            sendOtpObject.countryCode = countryCod
            sendOtpObject.phonenumber = mobileNumber1

            let sendOtp = sendOtpUpdateOperation(sendOtp: sendOtpObject)
            sendOtp.addDidFinishBlockObserver { [unowned self] (_, _) in
                  let status = self.appSharedPrefernce.getAppSharedPreferences(key: "sendOtpStatus") as? String ?? ""
                  self.stopLoader()
                  if status == "200" {

                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.otpsent, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)

                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                              self.profileVerificationPageRedirection()
                              self.verifyButton.isUserInteractionEnabled = true
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify Phone no & Email clicked", label: "OTP sent successfully", value: 0)
                  } else if status == "NetworkError" {
                        DispatchQueue.main.async {

                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                              self.verifyButton.isUserInteractionEnabled = true
                        }
                  } else if status == "1" {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify Phone no & Email clicked", label: "Social login users are not allowed to change Email Id", value: 0)
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.socialloginemail, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                              self.verifyButton.isUserInteractionEnabled = true
                        }
                  } else if status == "2" {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify Phone no & Email clicked", label: "User already exists by this phone number or email-id", value: 0)
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.userexitphonenumber, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                              self.verifyButton.isUserInteractionEnabled = true
                        }
                  } else {

                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify Phone no & Email clicked", label: "Failed to sent OTP", value: 0)
                        DispatchQueue.main.async {

                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                              self.verifyButton.isUserInteractionEnabled = true
                        }
                  }
                  self.appSharedPrefernce.removeAppSharedPreferences(key: "sendOtpStatus")

            }
            AppDelegate.addProcedure(operation: sendOtp)
      }
      //End of Function to Call Send OTP Service

      // Function to fill up the Pop up details
      func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, status: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.appSharedPrefernce.setAppSharedPreferences(key: "closestatus", value: status)
      }
      // End of Function to fill up the Pop up details

      //Function to call PopUp
      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
            nextVC.redirectProtocol = self as RedirectToPrivacyControl
            self.present(nextVC, animated: true, completion: nil)
      }
      // End of Function to call PopUp

      //Function for redirection Verification page where Otp will be send to respective email and mobile number where it will be verified and allow the user to update its email id and mobile number
      func  profileVerificationPageRedirection() {

            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ProfileOTPVerificationVC")  as! ProfileOTPVerificationVC
            nextVC.reloadProtocol = self as ReloadProtocol
            nextVC.process = "verifyPhoneNumber"
            self.present(nextVC, animated: true, completion: nil)

      }
      //End of Function for redirection Verification page where Otp will be send to respective email and mobile number where it will be verified and allow the user to update its email id and mobile number

      //Function call to hide show the Underines and show validations for the Field Information
    @objc func textFieldDidChange(_ textField: UICustomTextField) {
            self.newMobileNumber = self.profilenumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.newEmailId = self.profileemail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.newcountryCode = self.countryCodeField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            switch textField {
            case  profilename:
                  self.nameBlackUnderline.isHidden = true
                  self.nameBlueUnderline.isHidden = false
                  if (profilename.text?.characters.count)! <= 0 {
                        self.profilename.configOnError(withPlaceHolder: self.profilemsg.entername)
                  } else {
                        self.profilename.configOnErrorCleared(withPlaceHolder: self.profilemsg.name)
                  }

            case profileemail:
                  self.emailBlackunderline.image = UIImage(named: "BlueUnderLine")
                  if validationController.isValidEmail(testStr: profileemail.text!) {
                        self.profileemail.configOnErrorCleared(withPlaceHolder: self.profilemsg.email)
                  } else {
                        self.profileemail.configOnError(withPlaceHolder: self.profilemsg.invalidemail)
                  }

                  if (oldEmailId == newEmailId && oldMobileNumber == newMobileNumber && oldcountryCode == newcountryCode) {
                        self.verifyButton.isHidden = true
                        self.phoneNumberVerifyImage.isHidden = true
                        self.privacySwitchButton.isUserInteractionEnabled = true
                        self.Promotional.isUserInteractionEnabled = true
                        self.saveProfileButton.isUserInteractionEnabled = true
                        self.saveProfileButton.isHidden = false
                        self.saveProfileImage.isHidden = false
                  } else {
                        self.verifyButton.isHidden = false
                        self.phoneNumberVerifyImage.isHidden = false
                        self.saveProfileButton.isHidden = true
                        self.saveProfileImage.isHidden = true
                        self.verificationStatus = "pending"
                        self.privacySwitchButton.isUserInteractionEnabled = false
                        self.Promotional.isUserInteractionEnabled = false
                        self.saveProfileButton.isUserInteractionEnabled = false
                  }

            case profilenumber:
                  self.mobileNumberBlackUnderline.isHidden = true
                  self.mobileNumberBlueUnderline.isHidden = false
                  self.newMobileNumber = self.profilenumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  if (profilenumber.text?.characters.count)! > self.minMobNumDigits && (profilenumber.text?.characters.count)! < self.maxMobNumDigits {
                    self.profilenumber.configOnErrorCleared(withPlaceHolder: self.profilemsg.phone)
                  } else {
                    self.profilenumber.configOnError(withPlaceHolder: self.profilemsg.invalidphone)
                  }
                  if (oldEmailId == newEmailId && oldMobileNumber == newMobileNumber && oldcountryCode == newcountryCode) {
                        self.verifyButton.isHidden = true
                        self.phoneNumberVerifyImage.isHidden = true
                        self.privacySwitchButton.isUserInteractionEnabled = true
                        self.Promotional.isUserInteractionEnabled = true
                        self.saveProfileButton.isUserInteractionEnabled = true
                        self.saveProfileButton.isHidden = false
                        self.saveProfileImage.isHidden = false
                  } else {
                        self.verifyButton.isHidden = false
                        self.phoneNumberVerifyImage.isHidden = false
                        self.saveProfileButton.isHidden = true
                        self.saveProfileImage.isHidden = true
                        self.verificationStatus = "pending"
                        self.privacySwitchButton.isUserInteractionEnabled = false
                        self.Promotional.isUserInteractionEnabled = false
                        self.saveProfileButton.isUserInteractionEnabled = false
                  }

            case countryCodeField:
                  if (oldEmailId == newEmailId && oldMobileNumber == newMobileNumber && oldcountryCode == newcountryCode) {
                        self.verifyButton.isHidden = true
                        self.phoneNumberVerifyImage.isHidden = true
                        self.privacySwitchButton.isUserInteractionEnabled = true
                        self.Promotional.isUserInteractionEnabled = true
                        self.saveProfileButton.isUserInteractionEnabled = true
                        self.saveProfileButton.isHidden = false
                        self.saveProfileImage.isHidden = false
                  } else {
                        self.verifyButton.isHidden = false
                        self.phoneNumberVerifyImage.isHidden = false
                        self.saveProfileButton.isHidden = true
                        self.saveProfileImage.isHidden = true
                        self.verificationStatus = "pending"
                        self.privacySwitchButton.isUserInteractionEnabled = false
                        self.Promotional.isUserInteractionEnabled = false
                        self.saveProfileButton.isUserInteractionEnabled = false
                  }
                  break

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {
            case  profilename:
                  self.nameBlackUnderline.isHidden = false
                  self.nameBlueUnderline.isHidden = true
                  if (profilename.text?.characters.count)! <= 0 {
                        self.profilename.configOnError(withPlaceHolder: self.profilemsg.entername)
                  } else {
                        self.profilename.configOnErrorCleared(withPlaceHolder: self.profilemsg.name)
                  }

            case profileemail:
                  self.emailBlackunderline.image = UIImage(named: "BlackUnderLine")
                  //  self.emailBlueUnderline.isHidden = true

            case profilenumber:
                  self.mobileNumberBlackUnderline.isHidden = false
                  self.mobileNumberBlueUnderline.isHidden = true
                  self.newMobileNumber = self.profilenumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  if (profilenumber.text?.characters.count)! > self.minMobNumDigits && (profilenumber.text?.characters.count)! < self.maxMobNumDigits {
                    self.profilenumber.configOnErrorCleared(withPlaceHolder: self.profilemsg.phone)
                  } else {
                     self.profilenumber.configOnError(withPlaceHolder: self.profilemsg.invalidphone)
                  }
                  if (oldMobileNumber == newMobileNumber) {
                        self.verifyButton.isHidden = true
                        self.phoneNumberVerifyImage.isHidden = true
                        self.saveProfileButton.isUserInteractionEnabled = true
                        self.privacySwitchButton.isUserInteractionEnabled = true
                        self.Promotional.isUserInteractionEnabled = true
                  } else {
                        self.verifyButton.isHidden = false
                        self.phoneNumberVerifyImage.isHidden = false
                        self.privacySwitchButton.isUserInteractionEnabled = false
                        self.Promotional.isUserInteractionEnabled = false
                        self.saveProfileButton.isHidden = true
                        self.saveProfileImage.isHidden = true
                        self.verificationStatus = "pending"
                        self.saveProfileButton.isUserInteractionEnabled = false
                    self.view.makeToast(self.signupmsg.phoneNumberShouldBeValid, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                  }
                

            default:
                  break
            }
      }
      //End of Function call to hide show the Underines and show validations for the Field Information

      //Function to save the data that is edited by the user
      func savedata() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {

                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        return
            }
            if NetworkStatus.sharedManager.isNetworkReachable() {
                  self.userInterctionOff()
                  self.startLoader()
                  let name = self.profilename.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let email = self.profileemail.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  var mobile = self.profilenumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  
                  if(mobile.first == "0") {
                        mobile = String(mobile.dropFirst(1))
                  }
                  let countryCode = self.countryCodeField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  var profile = Profile()
                  profile.name = name
                  profile.email =  email
                  profile.phonenumber = mobile
                  profile.picture = self.profileImageUrl
                  profile.countryCode = countryCode
                  profile.privacyEnabled = self.privacy
                  profile.isMarketingMessagingEnabled = self.isMarketingMessagingEnabled
                 
                  if genderSelection == ""{
                        profile.gender = self.selectedText
                  } else {
                        profile.gender = self.selectedText
                  }
                  
                  self.saveProfileButton.isHidden = true
                  self.saveProfileImage.isHidden = true
                  
                  self.appSharedPrefernce.setAppSharedPreferences(key: "editBasicProfileStatusCode", value: "")
                  let editProfile = editBasicInformationOperation(profile: profile)
                  
                  if((mobile != self.previousProfileObject.phonenumber) || (countryCode != self.previousProfileObject.countryCode) || (email != self.previousProfileObject.email) || (self.previousProfileObject.privacyEnabled != self.privacy)) {
                        editProfile.isImportantDetailsChanged = true
                  }
                  editProfile.addDidFinishBlockObserver { [unowned self] (operation, _) in
                        let status = self.appSharedPrefernce.getAppSharedPreferences(key: "editBasicProfileStatusCode") as? String ?? ""
                        DispatchQueue.main.async {
                              self.Promotional.isUserInteractionEnabled = true
                              self.privacySwitchButton.isUserInteractionEnabled = false
                        }

                        if status == "200" {
                              self.profileList = operation.profileObject

                              DispatchQueue.main.async {
                                          self.assignDetails()
                                          self.stopLoader()
                                          self.onApiCallOff()

                                self.view.makeToast(self.profilemsg.profileUpdate, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Update profile details button clicked", label: "Profile basic details updated successfully", value: 0)
                        } else {

                              self.stopLoader()
                              DispatchQueue.main.async {
                                self.view.makeToast(self.profilemsg.profileupdateerror, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Update profile details button clicked", label: "Failed to update profile basic details", value: 0)
                        }
                  }
                  AppDelegate.addProcedure(operation: editProfile)
            } else {
                  self.assignDetails()
                  DispatchQueue.main.async {
                    self.view.makeToast(self.profilemsg.profileupdateerror, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                  }
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                        self.view.hideToast()
                  }
            }

      }
      //End of Function to save the data that is edited by the user

      //Function to make user fields interaction on and off
      func userInterctionOff() {
            DispatchQueue.main.async {
                  self.profilename.isUserInteractionEnabled = false
                  self.profileemail.isUserInteractionEnabled = false
                  self.countryCodeField.isUserInteractionEnabled = false
                  self.profilenumber.isUserInteractionEnabled = false
                  self.profileImageEditButton.isUserInteractionEnabled = false
                  self.dropDownButton.isUserInteractionEnabled = false
                  self.genderBtn.isUserInteractionEnabled = false
                  self.saveProfileButton.isUserInteractionEnabled = false
                  self.LanguageButton.isUserInteractionEnabled = false
                  self.privacySwitchButton.isUserInteractionEnabled = false
            }

      }

      func userInterctionOn() {
            DispatchQueue.main.async {
                  self.profilename.isUserInteractionEnabled = true
                  self.profileemail.isUserInteractionEnabled = true
                  self.countryCodeField.isUserInteractionEnabled = true
                  self.profilenumber.isUserInteractionEnabled = true
                  self.privacySwitchButton.isUserInteractionEnabled = true
                  self.Promotional.isUserInteractionEnabled = true

                  self.profileImageEditButton.isUserInteractionEnabled = true
                  self.dropDownButton.isUserInteractionEnabled = true
                  self.genderBtn.isUserInteractionEnabled = true
                  self.saveProfileButton.isUserInteractionEnabled = true
                  self.editProfileButton.isUserInteractionEnabled = true
                  self.LanguageButton.isUserInteractionEnabled = true
            }

      }
      //End of Function to make user fields interaction on and off

      //Function to start and stop the loader
      func startLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }

      }

      func stopLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.stopLoader()
            }

      }
      //End of Function to start and stop the loader

      //Function to initiate the image upload when image is selected from the picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.profileimage.image = image
        do {
            try FileManager.default.createDirectory(
                at: NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("upload")!,
                withIntermediateDirectories: true,
                attributes: nil)
            } catch {
                 
            }

        let fileName = ProcessInfo.processInfo.globallyUniqueString.appending(".jpg")
        let fileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("upload")?.appendingPathComponent(fileName)
        let imageData = image?.jpegData(compressionQuality: 0.6)
        uploadImage(imageData: imageData!, fileName: fileName)
            picker.dismiss(animated: true, completion: nil)
      }
      //End of Function to initiate the image upload when image is selected from the picker

      //Function to initiate when the picker is cancelled
      func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            picker.dismiss(animated: true, completion: nil)
      }
      //End Function to initiate when the picker is cancelled

      //Function to upload image to AWS Bucket
      func uploadImage(imageData: Data, fileName: String) {
            self.startLoader()
            let access = "AKIAIM73RR6SXIK6YS6A"
            let secret = "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"
            let credentials = AWSStaticCredentialsProvider(accessKey: access, secretKey: secret)
            let configuration = AWSServiceConfiguration(region: AWSRegionType.EUWest2, credentialsProvider: credentials)
        
            AWSServiceManager.default().defaultServiceConfiguration = configuration
            //let uploadingFileURL = imageData
            let s3BucketName = "fisheye-content-storage"
            let remoteName = "ProfileImages/\(self.fisheyeId).jpg"
            let expression = AWSS3TransferUtilityUploadExpression()

            expression.progressBlock = { (task, progress) in
                DispatchQueue.main.async(execute: {
                // Update a progress bar
                })
            }

            var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
            completionHandler = { (task, error) -> Void in
                DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
                })
            }

            let transferUtility = AWSS3TransferUtility.default()
            transferUtility.uploadData(imageData, bucket:s3BucketName , key: "ProfileImages/\(self.fisheyeId).jpg", contentType: "image/png", expression: expression,  completionHandler: completionHandler).continueWith { (task) -> Any? in
                if let error = task.error {
                    
                print("Error for image upload: \(error.localizedDescription)")
                }

                if task.result != nil {
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(s3BucketName).appendingPathComponent(remoteName)
                   // if let absoluteString = publicURL?.absoluteString {
                    // Set image with URL
                        let absoluteString = publicURL?.absoluteString
                    self.profileImageUrl = ""
                    self.profileImageUrl = absoluteString!
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyePhoto", value: self.profileImageUrl)
                        self.stopLoader()
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Selected image to set profile picture", label: "Uploaded profile picture", value: 0)
                    //}
                }

               
                return nil
            }

        }
      //End of Function to upload image to AWS Bucket

      //Function to show the profile details from local storage
      func showProfileDetails() {
            self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
        if let tempObj = self.obj {
                  self.profilename.text = tempObj["name"] as? String ?? ""
                  self.profileemail.text = tempObj["email"] as? String ?? ""
                  self.oldEmailId = tempObj["email"] as? String ?? ""
                  self.newEmailId = tempObj["email"] as? String ?? ""
                  self.countryCodeField.text = tempObj["countryCode"] as? String ?? ""
                  self.profilenumber.text = tempObj["phonenumber"] as? String ?? ""
                  self.oldMobileNumber = tempObj["phonenumber"] as? String ?? ""
                  self.newMobileNumber = tempObj["phonenumber"] as? String ?? ""
                  self.profileImageUrl = tempObj["picture"] as? String ?? ""
                  self.oldcountryCode = tempObj["countryCode"] as? String ?? ""
                  self.privacy = tempObj["privacyEnabled"]  as? Bool
                  self.isMarketingMessagingEnabled = tempObj["isMarketingMessagingEnabled"] as? Bool ?? false

                  let genderSelected = tempObj["gender"] as? String ?? ""
                   self.selectedText = genderSelected

                    let objArray = self.signupmsg.localGenderArrayList.filter( { return ($0.genderActualText == genderSelected ) } )
                   if objArray != nil && objArray.count > 0
                   {
                    self.genderSelection = (objArray[0].genderTranslatedText!)
                        self.genderTextField.text = objArray[0].genderTranslatedText
                   }
                   else{
                        self.genderTextField.text = genderSelected
                  }
            
            SDImageCache.shared.clearMemory()
            SDImageCache.shared.clearDisk()
                  
                  if profileImageUrl == "" || profileImageUrl == nil {
                        if genderSelected == "Female" || genderSelected == "Trans-Female" {
                              self.profileimage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "female"))
                        } else {
                              self.profileimage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Male"))
                        }
                  } else {
                        self.profileimage?.sd_setImage(with: URL(string: self.profileImageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                  }

                  if(self.privacy == true) {
                    DispatchQueue.main.async{
                        self.privacyDetail.text = self.profilemsg.privacyenabled
                        self.privacySwitchButton.isOn = true
                        self.privacyMessage.isUserInteractionEnabled = false
                        self.privacySwitchButton.backgroundColor = UIColor.duckEggBlue()
                        self.privacySwitchButton.layer.cornerRadius = 16.0
                    }
                  } else {
                    DispatchQueue.main.async{
                        self.privacyDetail.text = self.profilemsg.privacydisabled
                        self.privacySwitchButton.isOn = false
                        self.privacyMessage.isUserInteractionEnabled = false
                        self.privacySwitchButton.backgroundColor = UIColor.red
                        self.privacySwitchButton.layer.cornerRadius = 16.0
                    }
                  }

                  if(self.isMarketingMessagingEnabled == true) {
                        self.Promotional.isOn = true
                        self.Promotional.backgroundColor = UIColor.duckEggBlue()
                  } else {
                        self.Promotional.isOn = false
                        self.Promotional.backgroundColor = UIColor.red
                  }

                  self.initializePreviousProfileObj(phonenumber: tempObj["phonenumber"] as? String ?? "", countryCode: tempObj["countryCode"] as? String ?? "", email: tempObj["email"] as? String ?? "", privacyEnabled: tempObj["privacyEnabled"] as? Bool)
            }

         
            self.genderTextField.text = self.genderSelection
      }
      //End of Function to show the profile details from local storage

      func initializePreviousProfileObj(phonenumber: String, countryCode: String, email: String, privacyEnabled: Bool?) {
            self.previousProfileObject.phonenumber = phonenumber
            self.previousProfileObject.countryCode = countryCode
            self.previousProfileObject.email = email
            self.previousProfileObject.privacyEnabled = privacyEnabled
      }

      func onApiCallOff() {
            DispatchQueue.main.async {
                  self.editProfileButton.isUserInteractionEnabled = true
                  self.nameBlackUnderline.isHidden = true
                  self.emailBlackunderline.isHidden = true
                  self.mobileNumberBlackUnderline.isHidden = true
                  self.codeBlackUnderline.isHidden = true
                  self.saveProfileButton.isHidden = true
                  self.saveProfileButton.isHidden = true
                  self.profileImageEditButton.isHidden = false
                  self.editProfileButton.isHidden = false
                  self.editButtonImage.isHidden = false
                  self.zeroLabelUnderlineView.isHidden = true
                  self.genderBlankUnderline.isHidden = true
                  self.languageBlackUnderline.isHidden = true

            }
      }

      //Function to show the profile details from api call
      func assignDetails() {
            self.profilename.text = self.profileList.name
            self.profileemail.text = self.profileList.email
            self.oldEmailId = self.profileList.email ?? ""
            self.newEmailId = self.profileList.email ?? ""
            self.countryCodeField.text = self.profileList.countryCode
            self.profilenumber.text = self.profileList.phonenumber
            self.oldMobileNumber = self.profileList.phonenumber ?? ""
            self.newMobileNumber = self.profileList.phonenumber ?? ""
            self.profileImageUrl = self.profileList.picture ?? ""
            self.oldcountryCode = self.profileList.countryCode ?? ""
            self.privacy = self.profileList.privacyEnabled ?? false
            self.isMarketingMessagingEnabled = self.profileList.isMarketingMessagingEnabled
            let genderSelected = self.profileList.gender ?? ""
            self.genderSelection = genderSelected
           
            self.genderTextField.text = self.genderSelection
             let objArray = self.signupmsg.localGenderArrayList.filter( { return ($0.genderActualText == self.genderSelection) } )
             if objArray != nil && objArray.count > 0
             {
                  
                  self.genderTextField.text = objArray[0].genderTranslatedText
             }else{
                  
                  self.genderTextField.text = self.signupmsg.genderArrayList[self.signupmsg.genderArrayList.count-1]
            }
        SDImageCache.shared.clearMemory()
        SDImageCache.shared.clearDisk()
            
            if profileImageUrl == "" || profileImageUrl == nil {
                  if genderSelected == "Female" || genderSelected == "Trans-Female" {
                        self.profileimage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "female"))
                  } else {
                        self.profileimage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Male"))
                  }
            } else {
                  self.profileimage?.sd_setImage(with: URL(string: self.profileImageUrl ), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            }

            if(self.privacy == true) {
                  self.privacyDetail.text = self.profilemsg.privacyenabled
                  self.privacySwitchButton.isOn = true
                  self.privacyMessage.isUserInteractionEnabled = false
                  self.privacySwitchButton.backgroundColor = UIColor.duckEggBlue()
                  self.privacySwitchButton.layer.cornerRadius = 16.0
            } else {
                  self.privacyDetail.text = self.profilemsg.privacydisabled
                  self.privacySwitchButton.isOn = false
                  self.privacyMessage.isUserInteractionEnabled = false
                  self.privacySwitchButton.backgroundColor = UIColor.red
                  self.privacySwitchButton.layer.cornerRadius = 16.0
            }

            if(self.isMarketingMessagingEnabled == true) {
                  self.Promotional.isOn = true
                  self.Promotional.backgroundColor = UIColor.duckEggBlue()
            } else {
                  self.Promotional.isOn = false
                  self.Promotional.backgroundColor = UIColor.red
            }

            self.initializePreviousProfileObj(phonenumber: self.profileList.phonenumber ?? "", countryCode: self.profileList.countryCode ?? "", email: self.profileList.email ?? "", privacyEnabled: self.profileList.privacyEnabled ?? false)
      }

      // Description - Get translated text from backend
      // Author - Vaishali
      func getTranslatedText(updatelanguage: String) {
            self.startLoader()
           let getTranslation = GetTranslatedText(language: updatelanguage)
            getTranslation.addDidFinishBlockObserver { [unowned self] (_, _) in
                  self.stopLoader()
                  DispatchQueue.main.async {
                        if(self.obj == nil) {
                              self.fetchProfileDetails()
                        } else {
                              self.showProfileDetails()
                        }
                  }
                  NotificationCenter.default.post(name: CHANGE_LANGUAGE, object: nil)
            }
            AppDelegate.addProcedure(operation: getTranslation)
      }
      // End

      func changePrivacySetting(isPrivacyEnable: Bool) {
            self.startLoader()
            let changePrivacy = ChangePrivacySetting()
            var fisheyeContacts: [ContactObj] =  []                                        // to store FE Contacts
            for contact in DatabaseManagement.shared.getWholeFisheyeContacts() {
                  var tempContact = ContactObj()
                  tempContact = contact
                  tempContact.privacySetForContactUser = isPrivacyEnable
                  fisheyeContacts.append(tempContact)
            }
            changePrivacy.contactObjs = fisheyeContacts
            changePrivacy.addDidFinishBlockObserver { [unowned self] (_, _) in
                  DispatchQueue.main.async {
                        NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                        self.savedata()
//                        self.stopLoader()
                  }
            }
            AppDelegate.addProcedure(operation: changePrivacy)
      }
    func generateRandomStringWithLength(length: Int) -> String {
        let randomString: NSMutableString = NSMutableString(capacity: length)
        let letters: NSMutableString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var i: Int = 0
        
        while i < length {
            let randomIndex: Int = Int(arc4random_uniform(UInt32(letters.length)))
            randomString.append("\(Character( UnicodeScalar( letters.character(at: randomIndex))!))")
            i += 1
        }
        return String(randomString)
    }
}
//End of Function to show the profile details from api call

//Extension to the class to reload the profile data after updating the profile details manly the mobile and email details update
extension ProfileBasicViewController: ReloadProtocol {
      func reloadData(toReload: Bool) {
            if toReload  == true {
                  //fetchProfileDetails()
                  self.verifyButton.isHidden = true
                  self.phoneNumberVerifyImage.isHidden = true
                  self.savedata()
            }
      }
}
//End Extension to the class to reload the profile data after updating the profile details manly the mobile and email details update

//Extension to the class to reload the profile data after pivacy update
//extension ProfileBasicViewController: privacySaveDataProtocol {
//      func saveData(toReload: Bool) {
//            if toReload == true {
//                  self.privacy = true
//                  self.privacyDetail.text = self.profilemsg.privacyinfoshared
//                  self.savedata()
//            } else {
//                  self.privacy = false
//                  self.privacyDetail.text = self.profilemsg.persInfoshared
//                  self.savedata()
//            }
//      }
//}
//End of Extension to the class to reload the profile data after pivacy update

//Extension to the class to reload the profile data after pivacy update
extension ProfileBasicViewController: RedirectToPrivacyControl {
      func redirect(toRedirect: Bool) {
            if toRedirect == true {
            }
      }
}
//End of Extension to the class to reload the profile data after pivacy update

//Extension to the class to fetch the profile details from the get profile operations
extension ProfileBasicViewController {
      func fetchProfileDetails() {
            DispatchQueue.main.async {
                  self.profileImageEditButton.isHidden = true
                  self.saveProfileButton.isHidden = true
            }

            self.userInterctionOff()
            let getProfile = getProfileOperation()

            getProfile.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.profileList = operation.profileObject
                  DispatchQueue.main.async {
                              self.profileImageEditButton.isHidden = true
                              self.userInterctionOn()
                              self.saveProfileButton.isHidden = true
                              self.assignDetails()
                  }
            }
            AppDelegate.addProcedure(operation: getProfile)
      }
}
//End of Extension to the class to fetch the profile details from the get profile operations
