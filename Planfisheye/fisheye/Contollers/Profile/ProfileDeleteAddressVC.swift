//
//  ProfileDeleteAddressVC.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 23/01/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class ProfileDeleteAddressVC: UIViewController {
    let appSharedPrefernce = AppSharedPreference()
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    
    var addressDeleteProtocol: AddressDeleteProtocol?

    @IBOutlet weak var deleteSubHeading: UILabel!
    @IBOutlet weak var deleteHeading: UILabel!
    @IBOutlet weak var deleteButton: UILabel!
    @IBOutlet weak var cancelButton: UILabel!
    @IBAction func deleteButtonTapped(_ sender: Any) {
        self.nextVCCall()
    }
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.prevVCCall()
    }
    @IBOutlet weak var popUpView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.popUpView.layer.cornerRadius = 6
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0
        self.setLocalizationText()
        
    }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileDeleteAddressVC)
      }

    func nextVCCall() {
        addressDeleteProtocol?.deleteAddress(toDelete: true)
        self.dismiss(animated: true, completion: nil)
    }
    func prevVCCall() {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setLocalizationText() {
        DispatchQueue.main.async {
            self.deleteHeading.text = self.profilemsg.deleteaddheading
            self.deleteSubHeading.text = self.profilemsg.deleteaddsubheading
            self.cancelButton.text=self.profilemsg.cancel
            self.deleteButton.text = self.profilemsg.delete
        }
}
}
