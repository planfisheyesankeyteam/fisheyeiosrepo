//
//  ProfileAddressViewController.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 01/09/17.
//  Modified by Hitesh Patil
//  Last updated by Hitesh Patil on 23/01/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import ProcedureKit
import Toast_Swift

class ProfileAddressViewController: UIViewController {
      var addressIdArray: [String] = []
      var countryArray: [String] = []
      var localityArray: [String] = []
      var postalCodeArray: [String] = []
      var regionArray: [String] = []
      var streetAddressArray: [String] = []
      var typeArray: [String] = []
      var obj: [String: Any]?
      var profileList = Profile()
      var users: FEUser?
      var idtoken = ""
      let appSharedPrefernce = AppSharedPreference()
      var myaddress: [FEAddress]? = []
      var addressList: [AddressList] = []
      var deleteAddress: String = ""
      weak var delegate: Profiledelegate?
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      let appService = AppService.shared

      @IBOutlet weak var addaddressbtn: UIButton!
      @IBOutlet weak var rectangleAddAddressBtn: UIButton!
      @IBOutlet weak var noAddressView: UIView!
      @IBOutlet weak var addresstableview: UITableView!
      @IBOutlet weak var skipbtn: UIButton!
      @IBAction func addaddressClick(_ sender: Any) {
            self.showAddressEditingPopUpView()
      }

      @IBOutlet weak var emptyAddressListLabel: UILabel!

      @IBOutlet weak var addressLabel: UILabel!
      @IBAction func addAddressTapped(_ sender: Any) {
            self.showAddressEditingPopUpView()
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.addressLabel.text = self.profilemsg.addressLabel

                  self.emptyAddressListLabel.text = self.profilemsg.emptyAddressListlabel
                  self.rectangleAddAddressBtn.setTitle(self.profilemsg.addAddressLabel, for: .normal)
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.setLocalizationText()
            self.addObservers()
            self.applyBtnCornerRadius()
            self.addaddressbtn.layer.cornerRadius = 6
            self.noAddressView.isHidden = true
            self.idtoken =  self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as? [String: Any]

            if(self.obj != nil) {

                  showAddressDetails()
            } else {

                  fetchProfileDetails()
            }

            if(self.users?.addresses != nil) {
                  self.myaddress = self.users?.addresses!
            }
        self.addresstableview.rowHeight = UITableView.automaticDimension
            self.addresstableview.estimatedRowHeight = 80
            self.addaddressbtn.layer.borderColor = UIColor.black27().cgColor
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileAddressViewController)
      }

      func  showAddressDetails() {
        if let userObj = self.obj {
                  if let getAddressList = userObj["addresses"] as? [[String: Any]] {

                        if getAddressList.count == 0 {

                              self.noAddressView.isHidden = false
                              self.addresstableview.isHidden = true
                        } else {
                              self.noAddressView.isHidden = true
                              self.addresstableview.isHidden = false
                              for i  in 0 ..< getAddressList.count {
                                    if getAddressList.count>0 {

                                          let userAddress =  AddressList()
                                          userAddress.addressId =  getAddressList[i]["addressId"] as? String ?? ""
                                          userAddress.country = getAddressList[i]["country"] as? String ?? ""
                                          userAddress.locality = getAddressList[i]["locality"] as? String ?? ""
                                          userAddress.street_address = getAddressList[i]["street_address"] as? String ?? ""
                                          userAddress.postal_code = getAddressList[i]["postal_code"] as? String ?? ""
                                          userAddress.region = getAddressList[i]["region"] as? String ?? ""
                                          userAddress.type = getAddressList[i]["type"] as? String ?? ""
                                          userAddress.formatted = getAddressList[i]["formatted"] as? String ?? ""
                                          self.addressList.append(userAddress)
                                    }
                                    DispatchQueue.main.async {

                                          self.addresstableview.reloadData()
                                    }
                              }
                        }

                  } else {
                        self.noAddressView.isHidden = false
                        self.addresstableview.isHidden = true
                  }
            } else {
                  self.noAddressView.isHidden = false
                  self.addresstableview.isHidden = true
            }
      }
      func reloadAddressRoutineData() {
            fetchProfileDetails()
      }

      /* Created by venkatesh modified by vaishali  */
      func applyBtnCornerRadius() {
            self.rectangleAddAddressBtn.layer.cornerRadius = 6
            // border
            self.rectangleAddAddressBtn.layer.borderWidth = 0
            self.rectangleAddAddressBtn.layer.borderColor = UIColor.black.cgColor
      }
      /* END */

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func assignDetails() {
            let getAddressList = self.profileList.addresses
       
            self.addressList = []
            if getAddressList.count == 0 {
                  self.noAddressView.isHidden = false
                  self.addresstableview.isHidden = true
            } else {
                  self.addresstableview.isHidden = false
                  self.noAddressView.isHidden = true
                  for i in 0..<getAddressList.count {
                        if getAddressList.count>0 {
                              let userAddress =  AddressList()
                              userAddress.addressId = String(describing: self.profileList.addresses[i].addressId!)
                              userAddress.country = String(describing: self.profileList.addresses[i].country!)
                              userAddress.locality = String(describing: self.profileList.addresses[i].locality!)
                              userAddress.street_address = String(describing: self.profileList.addresses[i].street_address!)
                              userAddress.postal_code = String(describing: self.profileList.addresses[i].postal_code!)
                              userAddress.region = String(describing: self.profileList.addresses[i].region!)
                              userAddress.type = String(describing: self.profileList.addresses[i].type!)
                              userAddress.formatted = String(describing: self.profileList.addresses[i].formatted!)
                              self.addressList.append(userAddress)
                        }
                        DispatchQueue.main.async {
                              self.addresstableview.reloadData()
                        }
                  }
            }

      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      func updateView(address: FEAddress, editMode: Bool, atIndex: Int) {
            delegate?.addressupdated!()
      }
}

extension ProfileAddressViewController: UITableViewDelegate, UITableViewDataSource {
      func numberOfSections(in tableView: UITableView) -> Int {
            return 1

      }

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return addressList.count
      }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = self.addresstableview.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath)as! AddressTableViewCell
            let addressObj =  self.addressList[indexPath.row]
            if(addressObj.type == "Work") {
                  cell.workImage.isHidden = false
                  cell.homeImage.isHidden = true
            } else {
                  cell.workImage.isHidden = true
                  cell.homeImage.isHidden = false
            }
            cell.addressType.text = addressObj.type as? String ?? ""
        
        let objArray = self.appService.localAddressType.filter( { return ($0.addressActualText == cell.addressType.text ) } )
        
        if objArray != nil && objArray.count > 0
        {
            cell.addressType.text = objArray[0].addressTranslatedText
        }else {
                cell.addressType.text = appService.localAddressType[0].addressTranslatedText
        }
       
       
        
        
       
            cell.address.text = " \(addressObj.formatted ?? ""),\(addressObj.street_address ?? "") \n \(addressObj.locality ?? ""), \(addressObj.region ?? ""), \(addressObj.country ??  "") - \(addressObj.postal_code  ??  "")"

            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(editingAddressPopUpView(sender:)))
            cell.editImageView.tag = indexPath.row
            cell.editImageView.addGestureRecognizer(tapGesture)

            let deleteGesture = UITapGestureRecognizer(target: self, action: #selector(deleteAddressPopUpView(sender:)))
            cell.deleteAddress.tag = indexPath.row
            cell.deleteAddress.addGestureRecognizer(deleteGesture)
            return cell
      }

    func tableView(_tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
                  addresstableview.reloadData()
            }
      }

    @objc func editingAddressPopUpView(sender: UITapGestureRecognizer) {
            let address = addressList[sender.view!.tag]
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let addressEditingPopUpVC = storyBoard.instantiateViewController(withIdentifier: "ProfileAddressEditingPopUpVC")  as! ProfileAddressEditingPopUpVC
            addressEditingPopUpVC.editMode = true
            addressEditingPopUpVC.reloadProtocol = self as ReloadProtocol
            addressEditingPopUpVC.addressNew = address
            addressEditingPopUpVC.view.frame = self.view.bounds
            self.modalPresentationStyle = .overCurrentContext
            self.present(addressEditingPopUpVC, animated: true, completion: nil)
      }

    @objc func deleteAddressPopUpView(sender: UITapGestureRecognizer) {
            let address = addressList[sender.view!.tag]
            self.deleteAddress = address.addressId!
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let addressDeletePopUpVC = storyBoard.instantiateViewController(withIdentifier: "ProfileDeleteAddressVC")  as! ProfileDeleteAddressVC
            addressDeletePopUpVC.addressDeleteProtocol = self as AddressDeleteProtocol
            self.present(addressDeletePopUpVC, animated: true, completion: nil)
      }

      func showAddressEditingPopUpView() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
            let addressEditingPopUpVC = storyBoard.instantiateViewController(withIdentifier: "ProfileAddressEditingPopUpVC")  as! ProfileAddressEditingPopUpVC
            addressEditingPopUpVC.editMode = false
            addressEditingPopUpVC.reloadProtocol = self as ReloadProtocol
            self.present(addressEditingPopUpVC, animated: true, completion: nil)
      }
}

extension ProfileAddressViewController: ReloadProtocol {
      func reloadData(toReload: Bool) {
            if toReload {
                  self.fetchProfileDetails()
            }
      }
}

extension ProfileAddressViewController: AddressDeleteProtocol {
      func deleteAddress(toDelete: Bool) {
            if toDelete {
                  self.deleteAddressDetails()
            }
      }
}

extension ProfileAddressViewController {
      func fetchProfileDetails() {
            self.startLoader()
            self.profileList = Profile()
            let getProfile = getProfileOperation()
            getProfile.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.profileList = operation.profileObject
                  DispatchQueue.main.async {
                    self.stopLoader()
                    self.assignDetails()
                  }
            }
            AppDelegate.addProcedure(operation: getProfile)
      }
}

extension ProfileAddressViewController {
      func deleteAddressDetails() {
            self.startLoader()
            var delteAddressObject = DeleteAddress()
            delteAddressObject.addressId = self.deleteAddress
            let deleteAddress = deleteAddressOperation(deleteAddress: delteAddressObject)
            deleteAddress.addDidFinishBlockObserver { [unowned self] (_, _) in
                  let status = self.appSharedPrefernce.getAppSharedPreferences(key: "deleteAddress") as? String ?? ""
                  if status == "0" {
                        DispatchQueue.main.async {
                            self.stopLoader()
                            self.view.makeToast(self.profilemsg.addressdelete, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.fetchProfileDetails()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Addresses", action: "Delete address clicked", label: "Address deleted successful", value: 0)
                  } else if status == "NetworkError" {
                        DispatchQueue.main.async {
                            self.stopLoader()
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                        }
                  } else {
                        DispatchQueue.main.async {
                            self.stopLoader()

                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                    _ = 0.75
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.stopLoader()
                              self.view.hideToast()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Addresses", action: "Delete address clicked", label: "Failed to delete address", value: 0)
                  }
                  DispatchQueue.main.async {
                              self.appSharedPrefernce.removeAppSharedPreferences(key: "deleteAddress")

                  }
            }
            AppDelegate.addProcedure(operation: deleteAddress)
      }
}
