//
//  ProfileAddressEditingPopUpVC.swift
//  fisheye
//
//  Created by SankeyIosMac on 02/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit
import DropDown
//import Toast_Swift

protocol AddressSearchNew {
      func addrssObjectFrom(place: MKPlacemark?)
}

class ProfileAddressEditingPopUpVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

      var typeArray: [String] = AppService.shared.addressTypes
      //    var englishTypeArray : [String]!
      var selectedAddressTypeIndex: Int = 0
      let appSharedPrefernce =  AppSharedPreference()
      let addressTypeDropdown: DropDown = DropDown()
      var appService = AppService.shared
      var idtoken = ""
      var editMode: Bool = false
      var address: FEAddress?
      var addressNew = AddressList()
      var addressId: String?
      var selectedPlaceMark: MKPlacemark?
      var reloadProtocol: ReloadProtocol?
      var latitude = 0.0
      var longitude = 0.0
    var selectedText: String!

      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      

      @IBOutlet weak var containerView: UIView!
      @IBOutlet weak var postalCodeTextField: UICustomTextField!
      @IBOutlet weak var addBtn: UIButton!
      @IBOutlet weak var mapImageView: UIButton!
      @IBOutlet weak var countryTextField: UICustomTextField!
      @IBOutlet weak var stateTextField: UICustomTextField!
      @IBOutlet weak var cityTextField: UICustomTextField!
      @IBOutlet weak var addressLine2: UICustomTextField!
      @IBOutlet weak var addressLine1: UICustomTextField!
      @IBOutlet weak var addressTypeTxtField: UICustomTextField!
      @IBOutlet weak var submitBtn: UIButton!
      @IBOutlet weak var tableView: UITableView!
      @IBOutlet weak var addressSelectorButton: UIButton!
      @IBAction func tableViewDropDown(_ sender: Any) {
            if  addressTypeDropdown.isHidden {
                  self.addressTypeDropdown.show()
            } else {
                  self.addressTypeDropdown.hide()
            }
      }
      @IBAction func closeBtnTapped(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.typeArray.count
      }

      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.addressTypeTxtField.text = self.typeArray[indexPath.row]
            self.tableView.isHidden = true
            self.selectedAddressTypeIndex = indexPath.row

      }

      public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = self.typeArray[indexPath.row]
            return cell
      }

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            
            //        self.englishTypeArray = self.typeArray
            self.setLocalizationText()
            self.addObservers()

            if editMode {
                  self.addressId = addressNew.addressId
                  addressLine1.text = addressNew.formatted
                  addressLine2.text = addressNew.street_address
                  cityTextField.text =  addressNew.locality
                  stateTextField.text = addressNew.region
                  postalCodeTextField.text = addressNew.postal_code
                  countryTextField.text = addressNew.country
                addressTypeTxtField.text = addressNew.type ?? ""
                
                let objArray = self.appService.localAddressType.filter( { return ($0.addressActualText == addressTypeTxtField.text ) } )
                  
                 if  objArray != nil &&  objArray.count > 0
                 {
                  addressTypeTxtField.text = objArray[0].addressTranslatedText
                  self.selectedText = objArray[0].addressActualText
                 }else{
                     addressTypeTxtField.text = self.appService.addressTypeArray[0].addressTranslatedText
                  
                  }
                  
                
                  
                  
                
                //self.selectedAddressTypeIndex  = self.typeArray.firstIndex(of: addressTypeTxtField.text!) as? Int ?? 0
                self.selectedAddressTypeIndex  = self.typeArray.index(of: addressTypeTxtField.text!) ?? 0
                
            }

            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.addressTypeTxtField.textColor = UIColor.black
            self.tableView.isHidden = true
            self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.delegate = self as UITableViewDelegate
            self.tableView.dataSource = self as UITableViewDataSource
            self.addressTypeTxtField.delegate = self as? UITextFieldDelegate
            self.containerView.layer.borderColor = UIColor.duckEggBlue().cgColor
            self.containerView.layer.borderWidth = 1.0
            self.containerView.layer.cornerRadius = 5
            self.containerView.layer.masksToBounds = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))
            tap.numberOfTapsRequired = 1
            let tap2 = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))
            tap2.numberOfTapsRequired = 1
            mapImageView.isUserInteractionEnabled = true
            mapImageView.addGestureRecognizer(tap2)
            self.addressLine1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.cityTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.stateTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.postalCodeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.countryTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.addressLine1.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.cityTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.stateTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.postalCodeTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.countryTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.addressTypeTxtField.isUserInteractionEnabled = false

            addressTypeDropdown.anchorView = tableView
            addressTypeDropdown.dataSource = typeArray
            self.addressTypeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                  self.addressTypeTxtField.text = item
                  self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: self.profilemsg.addrestype)
                  self.selectedAddressTypeIndex = index
                
                let objArray = self.appService.localAddressType.filter( { return ($0.addressTranslatedText == item ) } )
                
                if objArray != nil && objArray.count > 0
                {
                  self.addressTypeTxtField.text = objArray[0].addressTranslatedText
                  self.selectedText = objArray[0].addressActualText
                }else {
                  self.addressTypeTxtField.text = self.appService.addressTypeArray[0].addressTranslatedText
                  self.selectedText =  self.appService.addressTypeArray[0].addressActualText
                  }
                
            }
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileAddressEditingPopUpVC)
      }

    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.addressTypeTxtField.placeholder=self.profilemsg.addrestype
                  self.addressLine1.placeholder = self.profilemsg.addressonee
                  self.addressLine2.placeholder = self.profilemsg.addressTwo
                  self.cityTextField.placeholder=self.profilemsg.city
                  self.stateTextField.placeholder=self.profilemsg.state
                  self.countryTextField.placeholder=self.profilemsg.country
                  self.postalCodeTextField.placeholder=self.profilemsg.pincode
                  self.typeArray = AppService.shared.addressTypes
                  //self.typeArray = [self.profilemsg.home,self.profilemsg.work]
            }
      }

      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {
            switch textField {
            case  addressLine1:
                  if (addressLine1.text?.characters.count)! <= 0 {
                        self.addressLine1.configOnError(withPlaceHolder: self.profilemsg.enteraddresssone)
                  } else {
                        self.addressLine1.configOnErrorCleared(withPlaceHolder: self.profilemsg.addressonee)
                  }

            case cityTextField:
                  if (cityTextField.text?.characters.count)! <= 0 {
                        self.cityTextField.configOnError(withPlaceHolder: self.profilemsg.Entercity)
                  } else {
                        self.cityTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.city)
                  }

            case stateTextField:
                  if (stateTextField.text?.characters.count)! <= 0 {
                        self.stateTextField.configOnError(withPlaceHolder: self.profilemsg.enterstate)
                  } else {
                        self.stateTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.state)
                  }

            case postalCodeTextField:
                  if (postalCodeTextField.text?.characters.count)! <= 0 {
                        self.postalCodeTextField.configOnError(withPlaceHolder: self.profilemsg.enterpincode)
                    
                  } else {
                        self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.pincode)
                  }

            case countryTextField:
                  if (countryTextField.text?.characters.count)! <= 0 {
                        self.countryTextField.configOnError(withPlaceHolder: self.profilemsg.entercountry)
                  } else {
                        self.countryTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.country)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {
            case  addressLine1:
                  if (addressLine1.text?.characters.count)! <= 0 {
                        self.addressLine1.configOnError(withPlaceHolder: self.profilemsg.enteraddresssone)
                  } else {
                        self.addressLine1.configOnErrorCleared(withPlaceHolder: self.profilemsg.addressonee)
                  }

            case cityTextField:
                  if (cityTextField.text?.characters.count)! <= 0 {
                        self.cityTextField.configOnError(withPlaceHolder: self.profilemsg.Entercity)
                  } else {
                        self.cityTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.city)
                  }

            case stateTextField:
                  if (stateTextField.text?.characters.count)! <= 0 {
                        self.stateTextField.configOnError(withPlaceHolder: self.profilemsg.enterstate)
                  } else {
                        self.stateTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.state)
                  }

            case postalCodeTextField:
                  if (postalCodeTextField.text?.characters.count)! <= 0 {
                        self.postalCodeTextField.configOnError(withPlaceHolder: self.profilemsg.enterpincode)
                         
                  } else {
                        self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.pincode)
                  }

            case countryTextField:
                  if (countryTextField.text?.characters.count)! <= 0 {
                        self.countryTextField.configOnError(withPlaceHolder: self.profilemsg.entercountry)
                  } else {
                        self.countryTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.country)
                  }

            default:
                  break
            }
      }

      func startLoader() {
            DispatchQueue.main.async {
                self.submitBtn.isUserInteractionEnabled = false
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }

      }

      func stopLoader() {
            DispatchQueue.main.async {
                self.submitBtn.isUserInteractionEnabled = true
                ScreenLoader.shared.stopLoader()
            }

      }

      func removeLayer() {
            let overlay = containerView.viewWithTag(100)
            overlay?.removeFromSuperview()
      }

      @IBAction func submitTapped(_ sender: Any) {
            var validAddressType = false
            var validAddressLine1 = false
            var validState = false
            var validCity = false
            var validPincode = false
            var validCountry = false
        
            if (addressTypeTxtField.text?.characters.count)! <= 0 {
                  self.addressTypeTxtField.configOnError(withPlaceHolder: self.profilemsg.addrestype)
                  validAddressType = false
            } else {
                  validAddressType = true
                  self.addressTypeTxtField.configOnErrorCleared(withPlaceHolder: self.profilemsg.addrestype)
            }

            if (addressLine1.text?.characters.count)! <= 0 {
                  self.addressLine1.configOnError(withPlaceHolder: self.profilemsg.enteraddresssone)
                  validAddressLine1 = false

            } else {

                  validAddressLine1 = true
                  self.addressLine1.configOnErrorCleared(withPlaceHolder: self.profilemsg.addressonee)
            }

            if  (self.cityTextField.text?.characters.count)! <= 0 {

                  self.cityTextField.configOnError(withPlaceHolder: self.profilemsg.Entercity)
                  validCity = false
            } else {

                  self.cityTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.city)
                  validCity = true
            }
            if  (self.stateTextField.text?.characters.count)! <= 0 {

                  self.stateTextField.configOnError(withPlaceHolder: self.profilemsg.enterstate)
                  validState = false
            } else {

                  self.stateTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.state)
                  validState = true
            }
            if  (self.postalCodeTextField.text?.characters.count)! <= 0 {

                  self.postalCodeTextField.configOnError(withPlaceHolder: self.profilemsg.enterpincode)
                  
                  validPincode = false
            } else {

                  self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.pincode)
                  validPincode = true
            }
            if (self.countryTextField.text?.characters.count)! <= 0 {

                  self.countryTextField.configOnError(withPlaceHolder: self.profilemsg.entercountry)
                  
                  validCountry = false
            } else {

                  self.countryTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.country)
                  validCountry = true
            }

            if validAddressLine1 && validState && validCity && validPincode && validCountry && validAddressType {

                  if editMode {

                        self.editAddress()
                  } else {

                        self.saveAddress()
                  }
            }
      }

      func saveAddress() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        return
            }
            let formatted = self.addressLine1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let street_address = self.addressLine2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let locality = self.cityTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let region = self.stateTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let postal_code = self.postalCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let country = self.countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        _=self.addressTypeTxtField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
            let type = selectedText
           
            

            self.startLoader()
            var addAddressObject = AddressType()
            addAddressObject.formatted = formatted
            addAddressObject.country = country
            addAddressObject.locality = locality
            addAddressObject.postal_code = postal_code
            addAddressObject.region = region
            addAddressObject.street_address = street_address
            addAddressObject.type = type
            let addAddress = addAddressOperation(address: addAddressObject)
            addAddress.addDidFinishBlockObserver { [unowned self] (_, _) in
                  let status = self.appSharedPrefernce.getAppSharedPreferences(key: "addAddressStatus") as? String ?? ""
                  self.stopLoader()
                  if status == "200" {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Addresses", action: "Add address", label: "Address saved successfully", value: 0)
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.addressaddedsuccess, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                              self.reloadProtocol?.reloadData(toReload: true)
                              self.dismiss(animated: true, completion: nil)
                        }
                  } else if status == "NetworkError" {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        self.appSharedPrefernce.removeAppSharedPreferences(key: "addAddressStatus")
                        self.stopLoader()
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.dismiss(animated: true, completion: nil)
                              self.view.hideToast()
                            
                        }
                  } else {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Addresses", action: "Add address", label: "Failed to add new address", value: 0)
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        self.appSharedPrefernce.removeAppSharedPreferences(key: "addAddressStatus")
                        self.stopLoader()
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.dismiss(animated: true, completion: nil)
                            
                              self.view.hideToast()
                        }
                  }
            }
            AppDelegate.addProcedure(operation: addAddress)
      }

      func editAddress() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        return
            }
        
            let formatted = self.addressLine1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let street_address = self.addressLine2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let locality = self.cityTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let region = self.stateTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let postal_code = self.postalCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let country = self.countryTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let type = self.selectedText
            

            self.startLoader()
            var updateAddressObject = AddressType()
            updateAddressObject.addressId = self.addressId
            updateAddressObject.formatted = formatted
            updateAddressObject.country = country
            updateAddressObject.locality = locality
            updateAddressObject.postal_code = postal_code
            updateAddressObject.region = region
            updateAddressObject.street_address = street_address
            updateAddressObject.type = type

            let updateAddress = updateAddressOperation(address: updateAddressObject)
            updateAddress.addDidFinishBlockObserver { [unowned self] (_, _) in
                  let status = self.appSharedPrefernce.getAppSharedPreferences(key: "updateAddressStatus") as? String ?? ""
                
                  if status == "200" {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.addressaddedsuccess, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }

                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()  + AppService.shared.firstTimeInterval) {
                              self.dismiss(animated: true, completion: {
                                    self.reloadProtocol?.reloadData(toReload: true)
                                    self.view.hideToast()
                                    self.stopLoader()
                                
                              })
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Addresses", action: "Update address", label: "Address updated successfully", value: 0)
                  } else if status == "NetworkError" {
                    self.stopLoader()
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.addressupatefailure, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()  + AppService.shared.firstTimeInterval) {
                              self.dismiss(animated: true, completion: {
                                    self.appSharedPrefernce.removeAppSharedPreferences(key: "updateAddressStatus")
                                
                                    self.view.hideToast()
                                    self.dismiss(animated: true, completion: nil)
                              })
                        }

                  } else {
                    self.stopLoader()
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()  + AppService.shared.firstTimeInterval) {
                              self.dismiss(animated: true, completion: {
                                    self.appSharedPrefernce.removeAppSharedPreferences(key: "updateAddressStatus")
                                
                                    self.view.hideToast()
                                    self.dismiss(animated: true, completion: nil)
                              })
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Addresses", action: "Update address", label: "Failed to update address", value: 0)
                  }

            }
            AppDelegate.addProcedure(operation: updateAddress)
      }

      // Function to fill up the Pop up details
      func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, status: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.appSharedPrefernce.setAppSharedPreferences(key: "closestatus", value: status)
      }
      // End of Function to fill up the Pop up details

      //Function to call PopUp
      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
            self.present(nextVC, animated: true, completion: nil)
      }
      // End of Function to call PopUp

      // MARK: Textfield Delegates
      func textFieldDidBeginEditing(_ textField: UITextField) {
            switch textField {
            case addressTypeTxtField:
                  self.addressTypeTxtField.configOnFirstResponder(withPlaceHolder: self.profilemsg.selctaddresstype)

            default:
                  if textField.isKind(of: UICustomUnderLineTextField.self) {
                        let txtField = textField as! UICustomUnderLineTextField
                        txtField.configOnFirstResponder()
                  }

                  if textField.tag == 0 {
                        textField.text = ""
                        textField.textColor = UIColor.black
                  }
                  break
            }
      }

      func textFieldDidEndEditing(_ textField: UITextField) {
            if textField.isKind(of: UICustomUnderLineTextField.self) {
                  let txtField = textField as! UICustomUnderLineTextField
                  txtField.configOnResignResponder()
            }
      }

      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            return true
      }

      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            let tag = textField.tag + 1
            if let nextTextField = self.view.viewWithTag(tag) {
                  if nextTextField.isKind(of: UICustomUnderLineTextField.self) {
                        nextTextField.becomeFirstResponder()
                  }
            }
            return true
      }

    @objc func navigateToMaps() {
            let mapVC = MapViewController.instantiateFromStoryboardWithIdentifier(identifier: "MapViewController")
        mapVC.addressSearchDelegate = self as AddressSearchNew
            if  self.latitude != 0.0 &&  self.longitude != 0.0 {
                  mapVC.originalLong = self.longitude
                  mapVC.originalLat = self.latitude
            }
            let navController = UINavigationController(rootViewController: mapVC)
            self.present(navController, animated: true, completion: nil)
      }
}

extension ProfileAddressEditingPopUpVC: AddressSearchNew {
      func addrssObjectFrom(place: MKPlacemark?) {
            if let placemark = place {
                  selectedPlaceMark = placemark
                  if let name = placemark.name {
                        self.addressLine1.text = "\(name)"
                        self.addressLine1.configOnErrorCleared(withPlaceHolder: self.profilemsg.addressonee)
                  }
                  if let thoroughfare = placemark.thoroughfare {
                        self.addressLine2.text="\(thoroughfare)"
                  }
                  if let loc = placemark.locality {
                        self.cityTextField.text = "\(loc)"
                        self.cityTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.city)
                  }
                  if let postalCode = placemark.postalCode {
                        self.postalCodeTextField.text = "\(postalCode)"
                        self.postalCodeTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.pincode)
                  }
                  if let state = placemark.administrativeArea {
                        self.stateTextField.text = "\(state)"
                        self.stateTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.state)
                  }
                  if let country = placemark.country {
                        self.countryTextField.text = "\(country)"
                        self.countryTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.country)
                  }
                  if let latitude = placemark.location?.coordinate.latitude {
                        self.latitude = latitude
                  }
                  if let longitude = placemark.location?.coordinate.longitude {
                        self.longitude = longitude
                  }
            } else {
                  selectedPlaceMark = nil
            }
      }
}
