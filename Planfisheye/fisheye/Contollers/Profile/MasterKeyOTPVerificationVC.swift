//MasterKeyOTPVerificationVC
//
//  MasterKeyOTPVerificationVC.swift
//  fisheye
//
//  Created by Sankey Solution on 12/2/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MasterKeyOTPVerificationVC: UIViewController {

    var phonenumber = ""
    var idtoken = ""
    var countrycode = ""
    var emailIdd = ""
    var numbertoverify = ""
    var verificationstatus = ""
    var verificationflag = ""
    var profilename = ""
    var profileurl = ""
    var  profileprivacy = false
    var remSeconds: Double = 1800
    var timer = Timer()
    var reloadProtocol: ReloadProtocol?
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let appSharedPrefernce = AppSharedPreference()

    @IBOutlet weak var emailTextField: UICustomTextField!
    @IBOutlet weak var emailInvalidOTPTextField: UILabel!
    @IBOutlet weak var viewSeperator: RadialGradientView!
    @IBOutlet weak var timerImageView: UIImageView!
    @IBOutlet weak var invalidOTPTextField: UILabel!
    @IBOutlet weak var timeRemainingTextField: UILabel!
    @IBOutlet weak var otpTextField: UICustomTextField!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var doneButtton: UIButton!
    @IBOutlet weak var otpScreenSubView: UIView!
    @IBOutlet weak var otpBackButton: UIButton!
    @IBOutlet weak var passcodeUnderline: RadialGradientView!
    @IBOutlet weak var sigInLabel: UILabel!
    @IBOutlet weak var fishEyeLabel: UILabel!
    @IBOutlet weak var itsAllAboutYou: UILabel!
    @IBAction func resendButtonTapped(_ sender: Any) {
        self.timerImageView.isHidden = false
        self.scheduleTimer()
        resendOtpFunctionCalled()
    }
    @IBAction func doneButtonTapped(_ sender: Any) {
        if (emailTextField.text?.characters.count)! <= 0 {
            self.emailTextField.configOnError(withPlaceHolder: "Invalid Email OTP")
        } else if(otpTextField.text?.characters.count)!<=0 {
            self.otpTextField.configOnError(withPlaceHolder: "Invalid Mobile OTP")
        } else  if (otpTextField.text?.characters.count)!<6 || (otpTextField.text?.characters.count)!>6 {
            self.otpTextField.configOnError(withPlaceHolder: "Invalid Mobile OTP")
        } else  if (emailTextField.text?.characters.count)!<6 || (emailTextField.text?.characters.count)!>6 {
            self.emailTextField.configOnError(withPlaceHolder: "Invalid Email OTP")
        } else {
            self.otpVerificationButtonTapped()
        }
    }
    @IBAction func otpBackButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.scheduleTimer()

        self.sigInLabel.textColor = UIColor.frenchBlue70()
        self.sigInLabel.font = UIFont(name: "Myriad Pro Regular", size: CGFloat(20))
        self.fishEyeLabel.font = UIFont(name: "ArialRoundedMTBold", size: CGFloat(28))
        self.fishEyeLabel.textColor = UIColor.purplyBlue10()
        self.itsAllAboutYou.textColor = UIColor.frenchBlue70()
        self.itsAllAboutYou.font = UIFont(name: "Helvetica", size: CGFloat(14))
        self.otpScreenSubView.layer.cornerRadius = 6
        self.otpScreenSubView.layer.cornerRadius = 6
        self.otpScreenSubView.layer.cornerRadius = 6
        self.otpScreenSubView.layer.shadowColor = UIColor.black.cgColor
        self.otpScreenSubView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.otpScreenSubView.layer.shadowOpacity = 0.4
        self.otpScreenSubView.layer.shadowRadius = 6.0
        self.otpScreenSubView.layer.masksToBounds = false
        self.phonenumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.countrycode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
        self.emailIdd = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
        self.profilename = self.appSharedPrefernce.getAppSharedPreferences(key: "profilename") as? String ?? ""
        self.profileurl = self.appSharedPrefernce.getAppSharedPreferences(key: "profileurl") as? String ?? ""
        self.profileprivacy = self.appSharedPrefernce.getAppSharedPreferences(key: "profileenabled") as? Bool ?? false
        self.invalidOTPTextField.isHidden = true
        self.otpTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.emailInvalidOTPTextField.isHidden = true
    }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.MasterKeyOTPVerificationVC)
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }

    func resendOtpFunctionCalled() {
        self.startLoader()
        let headers: HTTPHeaders =
            [
                "Content-Type": "application/json",
                "Accept": "application/json",
                "x-api-key": "eKPHyXZPXN9cMoHHJESLZ5jOJAUseCZn5QhrmuH0"
        ]

        let parameters: Parameters =
            [
                "action": "resendotp",
                "idtoken": idtoken,
                "phonenumber": phonenumber,
                "countryCode": countrycode,
                "email": emailIdd
        ]

        let awsURL = "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/dev"
        let endpoints = "/phonenumber/resendotp"

        Alamofire.request(awsURL+endpoints, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                switch response.result {
                case .success:
                    self.stopLoader()
                    self.showToast(message: "OTP is send to the registered emailId and Mobile number")
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Resend OTP", label: "OTP sent successfully", value: 0)
                case .failure:
                    self.stopLoader()
                    self.showToast(message: "Server Failure Occurred.Please try Again Later")
                     GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Resend OTP", label: "Failed to send OTP", value: 0)
                }
        }
    }

    func nextPageFunction() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpMasterKeyVC") as! SignUpMasterKeyVC
        self.present(nextVC, animated: true, completion: nil)
    }

    func changeMasterKey() {
        self.startLoader()
        var newMasterKey = self.appSharedPrefernce.getAppSharedPreferences(key: "newMasterKey")

        let headers: HTTPHeaders =
            [
                "Content-Type": "application/json",
                "Accept": "application/json"
        ]

        let parameters: Parameters =
            [
                "action": "changeMasterKeyWithoutOldMasterkey",
                "idtoken": idtoken,
                "newMasterKey": newMasterKey
          ]

        var awsURL = "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/dev"
        var endpoints = "/users/user/changemasterkeywithoutoldmasterkey"
        Alamofire.request(awsURL+endpoints, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                switch response.result {
                case .success:
                    self.stopLoader()
                    let response = JSON(response.result.value!)
                    if(response["statusCode"].stringValue == "0") {
                        self.showToast(message: "Master key updated successfully")
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
                              self.dismiss(animated: true, completion: {
                                    NotificationCenter.default.post(name: DISMISS_CHANGE_MASTER_KEY_PAGE, object: nil)
                              })
                        }

                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Master key changed after OTP verification", value: 0)
                    } else {
                         self.showToast(message: "Server Failure Occurred.Please try Again Later")
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Failed to change master key after OTP verification", value: 0)
                    }

                case .failure:
                    self.stopLoader()
                    self.showToast(message: "Server Failure Occurred.Please try Again Later")
                   GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Failed to change master key after OTP verification", value: 0)
                }
        }
    }

    func otpVerificationButtonTapped() {
        self.startLoader()
        var otpEntered = otpTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var emailEntered = emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        let headers: HTTPHeaders =
            [
                "Content-Type": "application/json",
                "Accept": "application/json"
        ]

        let parameters: Parameters =
            [
                "action": "verifyotp",
                "idtoken": idtoken,
                "phonenumber": phonenumber,
                "countryCode": countrycode,
                "otpValue": otpEntered,
                "mailOTPValue": emailEntered
        ]

        var awsURL = "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/dev"
        var endpoints = "/phonenumber/verifyotp"

        Alamofire.request(awsURL+endpoints, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                switch response.result {
                case .success:
                    self.stopLoader()
                    let smsresponse = JSON(response.result.value!)

                    if(smsresponse["statusCode"].stringValue == "200") {
                        self.showToast(message: "OTP verification successfull. Updating new master key...")
                       self.changeMasterKey()
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Verify OTP", label: "OTP verification successful - update new master key API called", value: 0)
                    } else {
                        self.showToast(message: "Server Failure Occurred.Please try Again Later")
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Verify OTP", label: "OTP verification failed", value: 0)
                    }

                case .failure(let error):
                    self.stopLoader()
                    self.showToast(message: "Server Failure Occurred.Please try Again Later")
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Verify OTP", label: "OTP verification failed", value: 0)
                }
        }
    }

    // Function to fill up the Pop up details
    func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, status: String) {
        self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
        self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
        self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
        self.appSharedPrefernce.setAppSharedPreferences(key: "closestatus", value: status)
    }
    // End of Function to fill up the Pop up details

    //Function to call PopUp
    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
        self.present(nextVC, animated: true, completion: nil)
    }
    // End of Function to call PopUp

    //Code for checking validation while the user edits TextFields
    func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case otpTextField:
            self.otpTextField.configOnErrorCleared(withPlaceHolder: "Enter Mobile OTP")
            if (otpTextField.text?.characters.count)!<6 || (otpTextField.text?.characters.count)!>6 {
                self.otpTextField.configOnError(withPlaceHolder: "Invalid Mobile OTP")
            } else {
                self.otpTextField.configOnErrorCleared(withPlaceHolder: "Enter Mobile OTP")
            }

        case emailTextField:
            self.emailTextField.configOnErrorCleared(withPlaceHolder: "Enter Email OTP")
            if (emailTextField.text?.characters.count)!<6 || (emailTextField.text?.characters.count)!>6 {
                self.emailTextField.configOnError(withPlaceHolder: "Invalid Email OTP")
            } else {
                self.emailTextField.configOnErrorCleared(withPlaceHolder: "Enter Email OTP")
            }

        default:
            break
        }
    }

    func scheduleTimer() {
        self.remSeconds = 1800
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        resendButton.isUserInteractionEnabled = false
    }

    func nextVCCall() {
        reloadProtocol?.reloadData(toReload: true)
        self.dismiss(animated: true, completion: nil)
    }

    func update() {
        self.remSeconds -= 1
        self.animate()
        if self.remSeconds == 0 {
            timer.invalidate()
            timeRemainingTextField.text = ""
            resendButton.alpha = 1.0
            self.timerImageView.isHidden = true
            resendButton.isUserInteractionEnabled = true
            return
        }
        let remSecondsStr = String(self.remSeconds.minuteSecond)
        timeRemainingTextField.text = "OTP valid for "+remSecondsStr!
    }

    func startLoader() {
        activityIndicator.center=self.view.center
        activityIndicator.hidesWhenStopped=true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
       }
    }

    func stopLoader() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
       }
    }
    func animate() {
        UIView.animate(withDuration: 0.5) { () -> Void in

            self.timerImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
    }

    class RadialGradientLayer: CALayer {

        var center: CGPoint {
            return CGPoint(x: bounds.width/2, y: bounds.height/2)

        }

        var radius: CGFloat {
            return (bounds.width + bounds.height)/2
        }

        var colors: [UIColor] = [] {
            didSet {
                setNeedsDisplay()
            }
        }

        var cgColors: [CGColor] {
            return colors.map({ (color) -> CGColor in
                return color.cgColor
            })
        }

        override init() {
            super.init()
            needsDisplayOnBoundsChange = true
        }

        required init(coder aDecoder: NSCoder) {
            super.init()
        }

        override func draw(in ctx: CGContext) {
            ctx.saveGState()
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let locations: [CGFloat] = [0.0, 1.0]
            guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
                return
            }
            ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
        }

    }

    class RadialGradientView: UIView {

        private let gradientLayer = RadialGradientLayer()

        public var colors: [UIColor] {
            get {
                return gradientLayer.colors
            }
            set {
                gradientLayer.colors = newValue
            }
        }

        override func layoutSubviews() {
            super.layoutSubviews()
            if gradientLayer.superlayer == nil {
                layer.insertSublayer(gradientLayer, at: 0)
            }
            gradientLayer.frame = bounds
        }
    }

      func showToast(message: String) {
            DispatchQueue.main.async {
                  self.view.makeToast(message)
            }

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                  self.view.hideToast()
            }
      }
}
