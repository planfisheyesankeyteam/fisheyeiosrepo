//
//  ProfileAddMasterKeyVC.swift
//  fisheye
//
//  Created by venkatesh murthy on 25/10/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import UIKit
import Toast_Swift

class ProfileAddMasterKeyVC: UIViewController {

    let appSharedPrefernce = AppSharedPreference()
    let validationController = ValidationController()
    var iconClick: Bool!
    var idtoken =  ""
    var masterKey = ""
    var reEnterMasterKeyValue = ""
    var signUpMethod: String = ""
//    var validation: Bool = false
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    
    var toSmallTextChange: ToSmallTextChange?

    
    @IBOutlet weak var masterKeyLabel: UILabel!
    @IBOutlet weak var masterKeyMessage: UILabel!
    @IBOutlet weak var masterKeyText: UICustomTextField!
    
    @IBOutlet weak var masterKeyPurpose: UILabel!
    @IBOutlet weak var reEnterMasterKey: UICustomTextField!
    @IBOutlet weak var reEnterMasterKeyUnderLine: UIView!
    
    @IBOutlet weak var reTypeMasterKeyBlueUnderline: UIImageView!
    @IBOutlet weak var reTypemasterkeyBlackUnderline: UIImageView!
    @IBOutlet weak var masterKeyBlueUnderline: UIImageView!
    @IBOutlet weak var masterKeyBlackUnderline: UIImageView!
    @IBOutlet weak var masterKeySubView: UIView!
    @IBOutlet weak var previousButtonOutlet: UIButton!
    @IBOutlet weak var backButtonImg: UIImageView!
    @IBOutlet weak var submitButtonOutlet: UIButton!
    
    @IBAction func submitButtonAction(_ sender: Any) {
        self.validationCheck(masterkeyText: masterKeyText.text!, reentermasterKeyText: reEnterMasterKey.text!)
    }
    
    @IBAction func previousButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength=4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func validationCheck(masterkeyText: String, reentermasterKeyText: String) {
        let masterKey = masterKeyText.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let reenterMasterKey = reEnterMasterKey.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var isValidationSuccessful: Bool = true
        if((masterkeyText.count) <= 0) {
            self.masterKeyText.configOnError(withPlaceHolder: self.signupmsg.entermasterkey)
            isValidationSuccessful = false
        } else {
            if(masterkeyText.count) < 4 {
                self.masterKeyText.configOnError(withPlaceHolder: self.signupmsg.invalidmk)
                isValidationSuccessful = false
            }
        }
        
        if (reentermasterKeyText.count) <= 0 {
            self.reEnterMasterKey.configOnError(withPlaceHolder: self.signupmsg.reentermasterkey)
            isValidationSuccessful = false
        } else if(masterKey != reenterMasterKey) {
            self.reEnterMasterKey.configOnError(withPlaceHolder: self.signupmsg.masterkeymismatch)
            isValidationSuccessful = false
        }
//        validation = isValidationSuccessful
        if isValidationSuccessful {
            self.masterKeyStorageFuncCall()
        }
    }
    
    func startLoader() {
        DispatchQueue.main.async {
            self.submitButtonOutlet.isUserInteractionEnabled=false
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            self.submitButtonOutlet.isUserInteractionEnabled=true
            ScreenLoader.shared.stopLoader()
        }
    }
    
    func masterKeyStorageFuncCall() {
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                }
                return
        }
        self.startLoader()
        let masterKey = masterKeyText.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let reenterMasterKey = reEnterMasterKey.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if(masterKey == reenterMasterKey) {
            let parameters =
                [
                    "action": "addmasterkey",
                    "masterkey": masterKey,
                    "idtoken": self.idtoken
                    ] as [String: Any]
            
            let awsURL = AppConfig.addMasterKeyAPI
            
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                return
            }
            
            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    let data = json
                    let statusCode = data["statusCode"] as? String ?? ""
                    
                    if(statusCode == "0") {
                        if let user = data["user"] as? [String: Any] {
                            self.masterKey = user["masterKey"] as? String ?? ""
                        }
                        saveFisheyeUserObject(data: data)
                        DispatchQueue.main.async {
                              self.view.makeToast(self.profilemsg.masterKeyAdded, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() +  self.signupmsg.toastDelayTime, execute: {
                            self.view.hideToast()
                            self.dismiss(animated: true, completion: {
                                 self.stopLoader()
                                self.toSmallTextChange?.masteraKeyTextChange()
                            })
                        })
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Profile - Add Master key", action: "Add Master key", label: "Master key registered successfully", value: 0)

                    } else {
                        self.stopLoader()
                        DispatchQueue.main.async {
                            self.view.makeToast(self.signupmsg.errormasterkey, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Register master key", label: "Failed to register master key - Redirection to Security questions screen", value: 0)
                    }
                    
                } else {
                    self.stopLoader()
                    DispatchQueue.main.async {
                        self.view.makeToast(self.signupmsg.errormasterkey, duration: self.signupmsg.toastDelayTime, position: .bottom)
                    }                    
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Register master key", label: "Failed to register master key", value: 0)
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
        } else {
            DispatchQueue.main.async {
                self.view.makeToast(self.signupmsg.keymismatch, duration: self.signupmsg.toastDelayTime, position: .bottom)
            }
            
        }
        
    }
    
    //Code for checking validation while the user edits TextFields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {
        
        switch textField {
            
        case masterKeyText:
            self.masterKeyBlackUnderline.isHidden = true
            self.masterKeyBlueUnderline.isHidden = false
            if((masterKeyText.text?.characters.count)! > 0 ) {
                self.masterKeyText.configOnErrorCleared(withPlaceHolder: self.signupmsg.masterkey)
                if((masterKeyText.text?.characters.count)! > 4) {
                    masterKeyText.text = String((masterKeyText.text?.dropLast())!)
                }
            } else {
                self.masterKeyText.configOnError(withPlaceHolder: self.signupmsg.entermasterkey)
            }
            
        case reEnterMasterKey:
            self.reTypemasterkeyBlackUnderline.isHidden = true
            self.reTypeMasterKeyBlueUnderline.isHidden = false
            if((reEnterMasterKey.text?.characters.count)! > 0 ) {
                self.reEnterMasterKey.configOnErrorCleared(withPlaceHolder: self.signupmsg.reentermasterkey)
                if((reEnterMasterKey.text?.characters.count)! > 4) {
                    reEnterMasterKey.text = String((reEnterMasterKey.text?.dropLast())!)
                }
            } else {
                self.reEnterMasterKey.configOnError(withPlaceHolder: self.signupmsg.reentermasterkey)
            }
        default:
            break
        }
    }
    
    //Code for checking validation when the user is done editing TextFields
    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
        switch textField {
            
        case reEnterMasterKey:
            let reEnterMasterKeyString: String = textField.text!
            let masterKeyString: String = masterKeyText.text!
            self.reTypemasterkeyBlackUnderline.isHidden = false
            self.reTypeMasterKeyBlueUnderline.isHidden = true
            if(reEnterMasterKeyString.count > 0) {
                if masterKeyString != reEnterMasterKeyString {
                    self.reEnterMasterKey.configOnError(withPlaceHolder: self.signupmsg.masterkeymismatch)
                }
            } else {
                self.reEnterMasterKey.configOnError(withPlaceHolder: self.signupmsg.reentermasterkey)
            }
            
        case masterKeyText:
            self.masterKeyBlackUnderline.isHidden = false
            self.masterKeyBlueUnderline.isHidden = true
            if (masterKeyText.text?.characters.count)! > 0 {
                if (masterKeyText.text?.characters.count)! != 4 {
                    self.masterKeyText.configOnError(withPlaceHolder: self.signupmsg.invalidmk)
                }
            } else {
                self.masterKeyText.configOnError(withPlaceHolder: self.signupmsg.entermasterkey)
            }
            
        default:
            break
        }
    }
    
    @IBAction func visibilityTapped(_ sender: Any) {
        
        if(iconClick == true) {
            self.masterKeyText.isSecureTextEntry = false
            iconClick = false
        } else {
            self.masterKeyText.isSecureTextEntry = true
            iconClick = true
        }
        
    }
    
    @IBOutlet weak var itsAboutYouLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
        self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
        self.masterKeySubView.layer.cornerRadius = 6
        self.masterKeyText.text = self.masterKey
        self.reEnterMasterKey.text = self.reEnterMasterKeyValue
        self.masterKeyText.isSecureTextEntry = true
        self.reEnterMasterKey.isSecureTextEntry = true
        iconClick = true
        self.masterKeySubView.layer.shadowColor = UIColor.black.cgColor
        self.masterKeySubView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.masterKeySubView.layer.shadowOpacity = 0.4
        self.masterKeySubView.layer.shadowRadius = 6.0
        self.masterKeySubView.layer.masksToBounds = false
        masterKeyText.delegate=self as? UITextFieldDelegate
        self.masterKeyText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.reEnterMasterKey.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.reEnterMasterKey.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.masterKeyText.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        
        self.masterKeyBlackUnderline.isHidden = false
        self.masterKeyBlueUnderline.isHidden = true
        
        self.reTypemasterkeyBlackUnderline.isHidden = false
        self.reTypeMasterKeyBlueUnderline.isHidden = true
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.setLocalizationText()
        self.addObserver()
        
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    
    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.masterKeyLabel.text = self.signupmsg.enterMasterKeyHeader
            self.masterKeyMessage.text = self.signupmsg.enterFourDigitMasterKey
            self.masterKeyPurpose.text = self.signupmsg.masterKeyPurpose
            self.masterKeyText.configOnErrorCleared(withPlaceHolder: self.signupmsg.masterkey)
            self.reEnterMasterKey.configOnErrorCleared(withPlaceHolder: self.signupmsg.reentermasterkey)
        }
    }
    /* END */
    
    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SignUpMasterKeyVC)
    }
    
    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBOutlet weak var fishEyeHeading: UILabel!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
