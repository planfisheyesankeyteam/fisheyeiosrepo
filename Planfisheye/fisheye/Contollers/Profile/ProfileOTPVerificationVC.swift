//
//  ProfileOTPVerificationVC.swift
//  fisheye
//
//  Created by SankeyMacPro on 30/05/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class ProfileOTPVerificationVC: UIViewController {

      var phonenumber = ""
      var idtoken = ""
      var countrycode = ""
      var emailIdd = ""
      var numbertoverify = ""
      var verificationstatus = ""
      var verificationflag = ""
      var profilename = ""
      var profileurl = ""
      var placeholderone = ""
      var placeholdertwo = ""
      var placeholderthree = ""
      var placeholderfour = ""
      var placeholderfive = ""
      var placeholdersix = ""
      var profileprivacy = false
      var remSeconds: Double = 900
      var timer = Timer()
      var reloadProtocol: ReloadProtocol?
      var redirectProtocol: RedirectProtocol?
      var process: String?
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      let appSharedPrefernce = AppSharedPreference()
      @IBOutlet weak var emailTextField: UICustomTextField!
      @IBOutlet weak var emailInvalidOTPTextField: UILabel!
      @IBOutlet weak var viewSeperator: RadialGradientView!
      @IBOutlet weak var timerImageView: UIImageView!
      @IBOutlet weak var invalidOTPTextField: UILabel!
      @IBOutlet weak var timeRemainingTextField: UILabel!
      @IBOutlet weak var otpTextField: UICustomTextField!
      @IBOutlet weak var resendButton: UIButton!
      @IBOutlet weak var doneButtton: UIButton!
      @IBOutlet weak var otpScreenSubView: UIView!
      @IBOutlet weak var otpBackButton: UIButton!
      @IBOutlet weak var passcodeUnderline: RadialGradientView!
      @IBOutlet weak var sigInLabel: UILabel!
      @IBOutlet weak var fishEyeLabel: UILabel!
      @IBOutlet weak var itsAllAboutYou: UILabel!

      @IBOutlet weak var pleaseEnterPart2Label: UILabel!
      @IBOutlet weak var pleaseEnterPart1Label: UILabel!
      @IBOutlet weak var securityCodeLabel: UILabel!
      @IBOutlet weak var smsOTPBlueUnderline: UIImageView!
      @IBOutlet weak var smsOTPBlackUnderline: UIImageView!
      @IBOutlet weak var emailOTPBlueUnderline: UIImageView!
      @IBOutlet weak var emailOTPBlackUnderline: UIImageView!
      @IBAction func resendButtonTapped(_ sender: Any) {
            resendOtpFunctionCalled()
      }
      @IBAction func doneButtonTapped(_ sender: Any) {
            self.doneButton(emailOtp: emailTextField.text!, smsOtp: otpTextField.text!)
      }
      func doneButton(emailOtp: String, smsOtp: String) {
            var validationSuccessfull: Bool = true
            if (emailTextField.text?.characters.count)! > 0 {
                  if (emailTextField.text?.characters.count)! != 6 {
                        self.emailTextField.configOnError(withPlaceHolder: self.profilemsg.invalidemailotp)
                    
                        validationSuccessfull = false
                  } else {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                  }
            } else {
                  self.emailTextField.configOnError(withPlaceHolder: self.profilemsg.enteremailOTP)
                
                  validationSuccessfull = false
            }

            if (otpTextField.text?.characters.count)! > 0 {
                  if (otpTextField.text?.characters.count)! != 6 {
                        self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidmobotp)
                        validationSuccessfull = false
                  } else {
                        self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                  }
            } else {
                  self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.entermobotp)
                
                  validationSuccessfull = false
            }

            if validationSuccessfull {
                  if process == "deleteProfile"{
                        deleteUser()
                  } else {
                        self.otpVerificationButtonTapped()
                  }

            }
      }

      @IBAction func otpBackButtonTapped(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
        
            self.scheduleTimer()

            self.setLocalizationText()
            self.addObservers()

            //self.itsAllAboutYou.font = UIFont(name: "Helvetica", size: CGFloat(14))
            self.otpScreenSubView.layer.cornerRadius = 6
            self.otpScreenSubView.layer.cornerRadius = 6
            self.otpScreenSubView.layer.cornerRadius = 6
            self.otpScreenSubView.layer.shadowColor = UIColor.black.cgColor
            self.otpScreenSubView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.otpScreenSubView.layer.shadowOpacity = 0.4
            self.otpScreenSubView.layer.shadowRadius = 6.0
            self.otpScreenSubView.layer.masksToBounds = false
            self.phonenumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.countrycode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
            self.emailIdd = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
            self.profilename = self.appSharedPrefernce.getAppSharedPreferences(key: "profilename") as? String ?? ""
            self.profileurl = self.appSharedPrefernce.getAppSharedPreferences(key: "profileurl") as? String ?? ""
            self.profileprivacy = self.appSharedPrefernce.getAppSharedPreferences(key: "profileenabled") as? Bool ?? false
            self.invalidOTPTextField.isHidden = true
            self.otpTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.otpTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.emailTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.emailInvalidOTPTextField.isHidden = true

      }

    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.sigInLabel.text=self.profilemsg.verificationLabel
                  self.pleaseEnterPart1Label.text = self.profilemsg.pleaseEnterPart1Label
                  self.securityCodeLabel.text = self.profilemsg.securityLabel
                  self.timeRemainingTextField.text = self.profilemsg.otpTimerPart1+" 14:59  "+self.profilemsg.minutes
                  self.itsAllAboutYou.text = self.profilemsg.letsWorkTogether
                  self.resendButton.setTitle(self.signupmsg.resend, for: .normal)
                  self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                  self.emailTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileVerificationVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.

      }

      func resendOtpFunctionCalled() {
            self.startLoader()

            let parameters =
                  [
                        "action": "resendotp",
                        "idtoken": idtoken,
                        "phonenumber": phonenumber,
                        "countryCode": countrycode,
                        "email": emailIdd
                        ] as [String: Any]

            let awsURL = AppConfig.resendOTPAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        self.stopLoader()
                        let statusCode = data["statusCode"] as? String ?? ""
                        DispatchQueue.main.async {
                              self.view.makeToast(self.profilemsg.otpsendtoemailPhone, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix, position: .bottom)
                              self.timerImageView.isHidden = false
                              self.scheduleTimer()
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                              self.view.hideToast()
                        })
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Resend OTP", label: "OTP sent successfully", value: 0)

                  } else {
                        self.stopLoader()
                        self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Resend OTP", label: "Failed to sent OTP", value: 0)

                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }

      func nextPageFunction() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpMasterKeyVC") as! SignUpMasterKeyVC
            self.present(nextVC, animated: true, completion: nil)
      }

      func otpVerificationButtonTapped() {
            self.startLoader()
            let otpEntered = otpTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let emailEntered = emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            let parameters =
                  [
                        "action": "verifyotp",
                        "idtoken": idtoken,
                        "phonenumber": phonenumber,
                        "countryCode": countrycode,
                        "otpValue": otpEntered,
                        "mailOTPValue": emailEntered
                        ] as [String: Any]

            let awsURL = AppConfig.verifyOTPAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        self.stopLoader()
                        let statusCode = data["statusCode"] as? String ?? ""
                        if(statusCode == "200") {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.verificationsuccess, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                                    self.view.hideToast()
                                    self.nextVCCall()
                              })

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify OTP", label: "OTP verification successful", value: 0)
                        } else if(statusCode == "3") {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.otpverificationfailed, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                                    self.view.hideToast()

                              })

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify OTP", label: "OTP verification failed", value: 0)
                        } else if(statusCode == "4") {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.otpExpired, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP expired", label: "Mobile Verification Failed", value: 0)
                        } else {
                              self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify OTP", label: "failure occured", value: 0)
                        }

                  } else {
                        self.stopLoader()
                        DispatchQueue.main.async {
                              self.view.makeToast(self.profilemsg.otpverificationfailed, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                              self.view.hideToast()

                        })

                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Proile - Basic information", action: "Verify OTP", label: "failure occured", value: 0)

                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }

      // Function to fill up the Pop up details
      func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, status: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.appSharedPrefernce.setAppSharedPreferences(key: "closestatus", value: status)
      }
      // End of Function to fill up the Pop up details

      //Function to call PopUp
      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
            self.present(nextVC, animated: true, completion: nil)
      }
      // End of Function to call PopUp

      //Code for checking validation while the user edits TextFields
    @objc func textFieldDidChange(_ textField: UITextField) {
            switch textField {
            case otpTextField:
                  self.smsOTPBlueUnderline.isHidden = false
                  self.smsOTPBlackUnderline.isHidden = true
                  if (otpTextField.text?.characters.count)! > 0 {
                        self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                        if((otpTextField.text?.characters.count)! > 6) {
                              otpTextField.text = String((otpTextField.text?.dropLast())!)
                        } else if (otpTextField.text?.characters.count)! < 6 {
                              self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidmobotp)
                        }
                  } else {
                        self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.entermobotp)
                  }

            case emailTextField:
                  self.emailOTPBlueUnderline.isHidden = false
                  self.emailOTPBlackUnderline.isHidden = true
                  if (emailTextField.text?.characters.count)! > 0 {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                        if((emailTextField.text?.characters.count)! > 6) {
                              emailTextField.text = String((emailTextField.text?.dropLast())!)
                        } else if (emailTextField.text?.characters.count)! < 6 {
                              self.emailTextField.configOnError(withPlaceHolder: self.profilemsg.invalidemailotp)
                        }
                  } else {
                        self.emailTextField.configOnError(withPlaceHolder: self.profilemsg.enteremailOTP)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {
            case  emailTextField:
                  self.emailOTPBlueUnderline.isHidden = true
                  self.emailOTPBlackUnderline.isHidden = false
                  if (emailTextField.text?.characters.count)! > 0 {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                        if((emailTextField.text?.characters.count)! > 6) {
                              emailTextField.text = String((emailTextField.text?.dropLast())!)
                        } else if (emailTextField.text?.characters.count)! < 6 {
                              self.emailTextField.configOnError(withPlaceHolder: self.profilemsg.invalidemailotp)
                        }
                  } else {
                        self.emailTextField.configOnError(withPlaceHolder: self.profilemsg.enteremailOTP)
                  }

            case  otpTextField:

                  self.smsOTPBlueUnderline.isHidden = true
                  self.smsOTPBlackUnderline.isHidden = false
                  if (otpTextField.text?.characters.count)! > 0 {
                        self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                        if((otpTextField.text?.characters.count)! > 6) {
                              otpTextField.text = String((otpTextField.text?.dropLast())!)
                        } else if (otpTextField.text?.characters.count)! < 6 {
                              self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidmobotp)
                        }
                  } else {
                        self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.entermobotp)
                  }
            default:
                  break
            }
      }

      func scheduleTimer() {
            self.remSeconds = 900
            timer.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
            resendButton.isUserInteractionEnabled = false
      }

      /* Created by by vaishali */
      func rotateAnimation(button: UIImageView, duration: CFTimeInterval = 2.0) {
            DispatchQueue.main.async {
                  let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
                  rotateAnimation.fromValue = 0.0
                  rotateAnimation.toValue = CGFloat(.pi * 2.0)
                  rotateAnimation.duration = duration
                  rotateAnimation.repeatCount = Float.greatestFiniteMagnitude
                  button.layer.add(rotateAnimation, forKey: "syncRotation")
            }
      }
      /* END */

      func changeMasterKey() {
        
            self.startLoader()
            var newMasterKey = self.appSharedPrefernce.getAppSharedPreferences(key: "newMasterKey")

            let parameters =
                  [
                        "action": "changeMasterKeyWithoutOldMasterkey",
                        "idtoken": idtoken,
                        "newMasterKey": newMasterKey
                        ] as [String: Any]

            let awsURL = AppConfig.changeMasterWithoutOldAPI
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        DispatchQueue.main.async {
                              self.stopLoader()
                        }
                        let responseStatus = data["statusCode"] as? String ?? ""
                    
                        if(responseStatus == "0") {
                             
                              DispatchQueue.main.async {

                                    self.dismiss(animated: true, completion: nil)
                                    self.redirectProtocol?.nextAction(method: "changeMasterKey")
                              }

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Master key changed after OTP verification", value: 0)
                        } else {

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Failed to change master key after OTP verification", value: 0)
                        }

                  } else {
                        DispatchQueue.main.async {
                              self.stopLoader()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Failed to change master key after OTP verification", value: 0)

                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }

      func nextVCCall() {
            switch(self.process) {
            case "changeMasterKey":
                  self.changeMasterKey()
                  break
            case "verifyPhoneNumber":
                  reloadProtocol?.reloadData(toReload: true)
                  self.dismiss(animated: true, completion: nil)
                  break
            default:
                  reloadProtocol?.reloadData(toReload: true)
                  self.dismiss(animated: true, completion: nil)
            }

      }
    
      /**************************************Delete profile api call starts here*************************************/
    
      func deleteUser() {
            //self.doneButtton.isUserInteractionEnabled=false
            self.startLoader()
            let otpEntered = otpTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let mailOtpEntered = emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            let parameters =
                  [
                        "idtoken": idtoken,
                        "phonenumber": phonenumber,
                        "countryCode": countrycode,
                        "otpValue": otpEntered,
                        "mailOTPValue": mailOtpEntered
                        ] as [String: Any]
        
          

            let awsURL = AppConfig.deleteUserAPI
        
          
            guard let URL = URL(string: awsURL) else {return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        let responseStatus = data["statusCode"] as? String ?? ""
                 
                        if responseStatus == "200"{
                              DispatchQueue.main.async {
                                    fetchingContactsFromBackendGlobalOperation.cancel()
                              }
                              DatabaseManagement.shared.deleteAllContactsFromSQLite()
                              DatabaseManagement.shared.deleteAllRecentlyAddedContactsFromSQLite()
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.profiledeletesuccess, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix+2, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                                    self.view.hideToast()
                                    self.stopLoader()
                                    let preferredLanguage = self.appSharedPrefernce.getAppSharedPreferences(key: "preferredLanguage") as? String ?? ""
                                    self.appSharedPrefernce.removeAllAppSharedPreferences()
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: preferredLanguage)
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                                    self.present(nextVC, animated: true, completion: nil)
                              })

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Delete fisheye account", label: "Fisheye account deleted - Redirection to login page", value: 0)
                        } else if(responseStatus == "4") {
                              DispatchQueue.main.async {
                                    self.stopLoader()
                                    self.view.makeToast(self.profilemsg.otpExpired, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP expired", label: "Mobile Verification Failed", value: 0)
                        } else if responseStatus == "3"{
                              DispatchQueue.main.async {
                                    self.stopLoader()
                                    self.view.makeToast(self.profilemsg.otpverificationfailed, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Delete fisheye account", label: "Mobile and Email Verification both Failed", value: 0)
                        } else if responseStatus == "2"{
                              self.stopLoader()
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.otpverificationfailed, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Delete fisheye account", label: "Email Id  verification failed", value: 0)
                        } else if responseStatus == "1"{
                              self.stopLoader()
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.otpverificationfailed, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Delete fisheye account", label: "Mobile Verification failed", value: 0)
                        } else {
                              DispatchQueue.main.async {
                                    self.stopLoader()
                                    self.view.makeToast(self.profilemsg.unabletodelete, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime+2, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Delete fisheye account", label: "Failed to delete fisheye account", value: 0)
                        }
//                        DispatchQueue.main.async {
//                            self.doneButtton.isUserInteractionEnabled=true
//                        }
                  } else {
                    //self.doneButtton.isUserInteractionEnabled=true
                        DispatchQueue.main.async {
                              self.stopLoader()
                              self.view.makeToast(self.profilemsg.unabletodelete, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix+2, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, execute: {
                              self.view.hideToast()
                        })
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Delete fisheye account", label: "Failed to delete fisheye account", value: 0)
//                    DispatchQueue.main.async {
//                        self.doneButtton.isUserInteractionEnabled=true
//                    }

                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }
    
      /************************************** Delete profile api call ends here *************************************/
    
    @objc func update() {
            self.remSeconds -= 1
            if self.remSeconds == 0 {
                  timer.invalidate()
                  self.timerImageView.isHidden = true
                  timeRemainingTextField.text = ""
                  resendButton.alpha = 1.0
                  resendButton.isUserInteractionEnabled = true
                  return
            }
            self.animate()
            let remSecondsStr = String(self.remSeconds.minuteSecond)
        timeRemainingTextField.text = self.profilemsg.otpTimerPart1+remSecondsStr+self.profilemsg.minutes
      }

      func startLoader() {
            DispatchQueue.main.async {
                self.emailTextField.isUserInteractionEnabled=false
                self.otpTextField.isUserInteractionEnabled=false
                self.doneButtton.isUserInteractionEnabled=false
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }

      }

      func stopLoader() {
            DispatchQueue.main.async {
                self.emailTextField.isUserInteractionEnabled=true
                self.otpTextField.isUserInteractionEnabled=true
                self.doneButtton.isUserInteractionEnabled=true
                ScreenLoader.shared.stopLoader()
            }

      }
    
      func animate() {
            UIView.animate(withDuration: 0.5) { () -> Void in
                  self.rotateAnimation(button: self.timerImageView)
                  //            self.timerImageView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
            }
      }

      class RadialGradientLayer: CALayer {

            var center: CGPoint {
                  return CGPoint(x: bounds.width/2, y: bounds.height/2)

            }

            var radius: CGFloat {
                  return (bounds.width + bounds.height)/2
            }

            var colors: [UIColor] = [] {
                  didSet {
                        setNeedsDisplay()
                  }
            }

            var cgColors: [CGColor] {
                  return colors.map({ (color) -> CGColor in
                        return color.cgColor
                  })
            }

            override init() {
                  super.init()
                  needsDisplayOnBoundsChange = true
            }

            required init(coder aDecoder: NSCoder) {
                  super.init()
            }

            override func draw(in ctx: CGContext) {
                  ctx.saveGState()
                  let colorSpace = CGColorSpaceCreateDeviceRGB()
                  let locations: [CGFloat] = [0.0, 1.0]
                  guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
                        return
                  }
                  ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
            }

      }

      class RadialGradientView: UIView {

            private let gradientLayer = RadialGradientLayer()

            public var colors: [UIColor] {
                  get {
                        return gradientLayer.colors
                  }
                  set {
                        gradientLayer.colors = newValue
                  }
            }

            override func layoutSubviews() {
                  super.layoutSubviews()
                  if gradientLayer.superlayer == nil {
                        layer.insertSublayer(gradientLayer, at: 0)
                  }
                  gradientLayer.frame = bounds
            }
      }

}

extension ProfileOTPVerificationVC: ReloadProtocol {
      func reloadData(toReload: Bool) {
            if toReload  == true {
                  //fetchProfileDetails()
                  self.dismiss(animated: true, completion: nil)
            }
      }
}
