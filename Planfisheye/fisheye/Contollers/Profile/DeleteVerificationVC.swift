//
//  DeleteVerificationVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 15/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DeleteVerificationVC: UIViewController {

    var phonenumber = ""
    var idtoken = ""
    var countrycode = ""
    var emailIdd = ""
    var numbertoverify = ""
    var verificationstatus = ""
    var verificationflag = ""
    var profilename = ""
    var profileurl = ""
    var  profileprivacy = false
    /*Defined Variables*/
    var remSeconds: Double = 60
    var timer = Timer()
    var reloadProtocol: ReloadProtocol?

    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let appSharedPrefernce = AppSharedPreference()

    @IBOutlet weak var timerImageView: UIImageView!
    @IBOutlet weak var invalidOTPTextField: UILabel!
    @IBOutlet weak var timeRemainingTextField: UILabel!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var emailOTPField: UITextField!
    @IBAction func resendButtonTapped(_ sender: Any) {

        self.timerImageView.isHidden = false
        self.scheduleTimer()
        resendOtpFunctionCalled()

    }
    @IBOutlet weak var emailOtpInvalid: UILabel!
    @IBOutlet weak var doneButtton: UIButton!
    @IBAction func doneButtonTapped(_ sender: Any) {
        otpVerificationButtonTapped()
     }
    @IBOutlet weak var otpScreenSubView: UIView!
    @IBAction func otpBackButtonTapped(_ sender: Any) {
        otpBackFunctionCalled()
    }
    @IBOutlet weak var otpBackButton: UIButton!

    @IBOutlet weak var passcodeUnderline: RadialGradientView!
    func otpBackFunctionCalled() {

        self.dismiss(animated: true, completion: nil)
    }

    @IBOutlet weak var sigInLabel: UILabel!
    func resendOtpFunctionCalled() {
        self.startLoader()
        let headers: HTTPHeaders =
        [
                "Content-Type": "application/json",
                "Accept": "application/json"
         ]
        print("Headers", headers)

        let parameters: Parameters =
            [
                "action": "resendotp",
                "idtoken": idtoken,
                "phonenumber": phonenumber,
                "countryCode": countrycode,
                "email": emailIdd
       ]

        let awsURL = "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/dev"
        let endpoints = "/phonenumber/resendotp"

        Alamofire.request(awsURL+endpoints, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                switch response.result {
                case .success:
                    self.stopLoader()
                    print("User Found", response)
                    kSweetAlert.showAlert("Successful", subTitle: "SMS Sent Successfully", style: AlertStyle.none)

                case .failure(let error):
                    self.stopLoader()
                    kSweetAlert.showAlert("Failure", subTitle: "SMS Sending Failed", style: AlertStyle.none)
                }
        }
    }

    func nextPageFunction() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(nextVC, animated: true, completion: nil)
     }

    func otpVerificationButtonTapped() {
        self.startLoader()
        var otpEntered = otpTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var mailOtpEntered = emailOTPField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let headers: HTTPHeaders =
        [
                "Content-Type": "application/json",
                "Accept": "application/json"
         ]

        let parameters: Parameters =
        [
                "action": "verifyotp",
                "idtoken": idtoken,
                "phonenumber": phonenumber,
                "otpValue": otpEntered,
                "mailOTPValue": mailOtpEntered
        ]

        var awsURL = "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/dev"
        var endpoints = "/users/deleteuser"

        Alamofire.request(awsURL+endpoints, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                switch response.result {
                case .success:
                    self.stopLoader()
                    print("User Found", response)
                    let smsresponse = JSON(response.result.value!)
                    print(smsresponse)

                    if(smsresponse["statusCode"].stringValue == "200") {
                         self.nextPageFunction()
                    } else {
                        kSweetAlert.showAlert("Failure", subTitle: "SMS Verification Failed", style: AlertStyle.none)
                    }

                case .failure(let error):
                    self.stopLoader()
                    print("Something went wrong", response.result)
                }
        }
    }

    //Code for checking validation while the user edits TextFields
    func textFieldDidChange(_ textField: UITextField) {

        switch textField {

        case otpTextField:
            if (otpTextField.text?.characters.count)!<6 || (otpTextField.text?.characters.count)!>6 {
                self.invalidOTPTextField.textColor = UIColor.red
                self.invalidOTPTextField.text = "Invalid OTP"
            } else {
                self.invalidOTPTextField.text = "Enter Mobile OTP"
                self.invalidOTPTextField.textColor = UIColor.black27()
            }

        case emailOTPField:
            if (emailOTPField.text?.characters.count)!<6 || (emailOTPField.text?.characters.count)!>6 {
                self.emailOtpInvalid.textColor = UIColor.red
                self.emailOtpInvalid.text = "Invalid OTP"
            } else {
                self.emailOtpInvalid.text = "Enter Mobile OTP"
                self.emailOtpInvalid.textColor = UIColor.black27()
            }

        default:
            break
        }
    }

    func scheduleTimer() {
        self.remSeconds = 60
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        resendButton.isUserInteractionEnabled = false
    }

    func update() {
        self.remSeconds -= 1
        self.animate()
        if self.remSeconds == 0 {
            timer.invalidate()
            timeRemainingTextField.text = ""
            resendButton.alpha = 1.0
            self.timerImageView.isHidden = true
            resendButton.isUserInteractionEnabled = true
            return
        }
        let remSecondsStr = String(self.remSeconds.minuteSecond)
        timeRemainingTextField.text = "OTP valid for "+remSecondsStr!
    }

    func startLoader() {
        activityIndicator.center=self.view.center
        activityIndicator.hidesWhenStopped=true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)

        activityIndicator.startAnimating()
    }

    func stopLoader() {
        activityIndicator.stopAnimating()
    }
    func animate() {
        UIView.animate(withDuration: 0.5) { () -> Void in
            self.timerImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        }
    }

    @IBOutlet weak var fishEyeLabel: UILabel!
    @IBOutlet weak var itsAllAboutYou: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scheduleTimer()
        self.sigInLabel.textColor = UIColor.frenchBlue70()
        self.sigInLabel.font = UIFont(name: "Myriad Pro Regular", size: CGFloat(20))
        self.fishEyeLabel.font = UIFont(name: "ArialRoundedMTBold", size: CGFloat(28))
        self.fishEyeLabel.textColor = UIColor.purplyBlue10()
        self.itsAllAboutYou.textColor = UIColor.frenchBlue70()
        self.itsAllAboutYou.font = UIFont(name: "Helvetica", size: CGFloat(14))
        self.otpScreenSubView.layer.cornerRadius = 6
        self.otpScreenSubView.layer.cornerRadius = 6
        self.otpScreenSubView.layer.cornerRadius = 6
        self.otpScreenSubView.layer.shadowColor = UIColor.black.cgColor
        self.otpScreenSubView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.otpScreenSubView.layer.shadowOpacity = 0.4
        self.otpScreenSubView.layer.shadowRadius = 6.0
        self.otpScreenSubView.layer.masksToBounds = false
        self.phonenumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.countrycode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
        self.emailIdd = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
        self.profilename = self.appSharedPrefernce.getAppSharedPreferences(key: "profilename") as? String ?? ""
        self.profileurl = self.appSharedPrefernce.getAppSharedPreferences(key: "profileurl") as? String ?? ""
        self.profileprivacy = self.appSharedPrefernce.getAppSharedPreferences(key: "profileenabled") as? Bool ?? false

        self.invalidOTPTextField.text = "Enter Mobile OTP"
        self.emailOtpInvalid.text = "Enter Email OTP"
        self.invalidOTPTextField.textColor = UIColor.black27()
          self.emailOtpInvalid.textColor = UIColor.black27()
        self.otpTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    class RadialGradientLayer: CALayer {

        var center: CGPoint {
            return CGPoint(x: bounds.width/2, y: bounds.height/2)

        }

        var radius: CGFloat {
            return (bounds.width + bounds.height)/2
        }

        var colors: [UIColor] = [] {
            didSet {
                setNeedsDisplay()
            }
        }

        var cgColors: [CGColor] {
            return colors.map({ (color) -> CGColor in
                return color.cgColor
            })
        }

        override init() {
            super.init()
            needsDisplayOnBoundsChange = true
        }

        required init(coder aDecoder: NSCoder) {
            super.init()
        }

        override func draw(in ctx: CGContext) {
            ctx.saveGState()
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let locations: [CGFloat] = [0.0, 1.0]
            guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
                return
            }
            ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
        }

    }

    @IBOutlet weak var viewSeperator: RadialGradientView!

    class RadialGradientView: UIView {

        private let gradientLayer = RadialGradientLayer()

        public var colors: [UIColor] {
            get {
                return gradientLayer.colors
            }
            set {
                gradientLayer.colors = newValue
            }
        }

        override func layoutSubviews() {
            super.layoutSubviews()
            if gradientLayer.superlayer == nil {
                layer.insertSublayer(gradientLayer, at: 0)
            }
            gradientLayer.frame = bounds
        }
    }
}
