//
//  ProfileChangePasswordVC.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 14/01/2018.
//  Modified by Anagha Meshram on 31/01/2018
//  Modified by Anagha Meshram on 01/02/2018
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift
import ProcedureKit

class ProfileChangePasswordVC: UIViewController {

    let appService = AppService()
    let validationController = ValidationController()
    let appSharedPrefernce = AppSharedPreference()
    var iconClickRePWD = true
    var pwdVieww: Bool! = false
    var flag: Bool = false
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    var placeHolderOne = ""
    var placeHolderTwo = ""
    var placeHolderThree = ""
    var placeHolderFour = ""
    var placeHolderFive = ""
    var placeHolderSix = ""
    @IBOutlet weak var closeButton: UIImageView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var oldPwd: UICustomTextField!
    @IBOutlet weak var newPwd: UICustomTextField!
    @IBOutlet weak var confirmPwd: UICustomTextField!
    @IBOutlet weak var showPwd: UIImageView!
    @IBOutlet weak var pwdTextView: UITextView!
    @IBOutlet weak var oldPwdBlackUnderLine: UIImageView!
    @IBOutlet weak var oldPwdBlueUnderLine: UIImageView!
    @IBOutlet weak var newPwdBlackUnderLine: UIImageView!
    @IBOutlet weak var newPwdBlueUnderLine: UIImageView!
    @IBOutlet weak var reEnterBlackUnderLine: UIImageView!
    @IBOutlet weak var reEnterBlueUnderLine: UIImageView!

    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var changePasswordLabel: UILabel!

    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func passwordVisibilityTapped(_ sender: Any) {
        if(iconClickRePWD == true) {
            self.newPwd.isSecureTextEntry = false
            iconClickRePWD = false
        } else {
            self.newPwd.isSecureTextEntry = true
            iconClickRePWD = true
        }
    }
    @IBAction func pwdDetailViewTapped(_ sender: Any) {
        if(pwdVieww == true) {
            self.pwdTextView.isHidden = false
            pwdVieww = false
        } else {
            self.pwdTextView.isHidden = true
            pwdVieww = true
        }
    }
    //  Applied shadow effects on Pop Up View
    func profileCardShadowView() {
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.7
        self.popUpView.layer.shadowRadius = 6.0
        self.popUpView.layer.masksToBounds = false
    }

    //  Applied password validations on old,new and reEnterred Passwords
    @IBAction func submitButtonTapped(_ sender: Any) {

        submitbuttonTapped(oldPassword: oldPwd.text!, newPassword: newPwd.text!, retypePassword: confirmPwd.text!)
    }

    func submitbuttonTapped(oldPassword: String, newPassword: String, retypePassword: String) {
        //        let oldPwd = self.oldPwd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //        let newPwd = self.newPwd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //        let retypePwd = self.confirmPwd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //  Modified by Anagha Meshram on 31/01/2018
        //  Modified validations as they were not working properly so the code was commented

        //  Modified by Anagha Meshram on 01/02/2018
        //  Validations for individual fields

        var oldPaswordValid = false
        var newPaswordValid = false
        var confirmPaswordValid = false
        var passwordMatch = false
        if oldPassword.count>0 {
            if validationController.isValidPassword(testStr: oldPassword) {
                self.oldPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.currentpassword)
                oldPaswordValid = true
            } else {
                self.oldPwd.configOnError(withPlaceHolder: self.profilemsg.invalidcurrentpswd)
                self.view.makeToast(self.signupmsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium, position: .bottom)
                oldPaswordValid = false
            }
        } else {
            self.oldPwd.configOnError(withPlaceHolder: self.profilemsg.entercurrentpswd)
            oldPaswordValid = false
        }

        if newPassword.count>0 {
            if validationController.isValidPassword(testStr: newPassword) {
                self.newPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.newpassword)
                newPaswordValid = true
            } else {
                self.newPwd.configOnError(withPlaceHolder: self.profilemsg.invalidnewpwd)
                self.view.makeToast(self.signupmsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium, position: .bottom)
                newPaswordValid = false
            }
        } else {
            self.newPwd.configOnError(withPlaceHolder: self.profilemsg.enternewpwd)
            newPaswordValid = false
        }

        if retypePassword.count>0 {
            if validationController.isValidPassword(testStr: retypePassword) {
                self.confirmPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.retypepassword)
                confirmPaswordValid = true
            } else {
                self.confirmPwd.configOnError(withPlaceHolder: self.profilemsg.invalidpwd)
                self.view.makeToast(self.signupmsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium, position: .bottom)
                confirmPaswordValid = false
            }
        } else {
            confirmPaswordValid = false
            self.confirmPwd.configOnError(withPlaceHolder: self.profilemsg.confirmpswd)
        }

        if (self.confirmPwd.text != self.newPwd.text) {
            self.confirmPwd.configOnError(withPlaceHolder: self.profilemsg.newandretypepswd)
            passwordMatch = false
        } else {
            self.confirmPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.retypepassword)
            passwordMatch = true

        }

        if oldPaswordValid && newPaswordValid && confirmPaswordValid && passwordMatch {
            changePwdCalled()
        }

        //  End of Validations for individual fields

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        self.popUpView.layer.cornerRadius = 10
        self.setLocalizationText()
        self.addObservers()
        self.popUpDesign()
        pwdVieww = true
        self.pwdTextView.isHidden = true
        self.confirmPwd.isSecureTextEntry = true
        self.newPwd.isSecureTextEntry = true
        self.oldPwd.isSecureTextEntry = true

        //  Modified by Anagha Meshram on 31/01/2018
        //  Applied validations on old, new, reEntered Password fields over underlined view images

        self.oldPwd.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.oldPwd.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.newPwd.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.newPwd.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.confirmPwd.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.confirmPwd.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        profileCardShadowView()

        self.oldPwdBlueUnderLine.isHidden = true
        self.oldPwdBlackUnderLine.isHidden = false

        self.newPwdBlueUnderLine.isHidden = true
        self.newPwdBlackUnderLine.isHidden = false

        self.reEnterBlueUnderLine.isHidden = true
        self.reEnterBlackUnderLine.isHidden = false

        //  End of Applied validations on old, new, reEntered Password fields over underlined view images

        // Do any additional setup after loading the view.

        self.pwdTextView.isUserInteractionEnabled = false
    }

    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileChangePasswordVC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func setLocalizationText() {
      DispatchQueue.main.async {
            self.changePasswordLabel.text = self.profilemsg.chnagePasswordLabel
            self.pwdTextView.text = self.profilemsg.passwordPolicy
            self.oldPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.currentpassword)
            self.newPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.newpassword)
            self.confirmPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.retypepassword)
      }
        // self.timer
    }
    /* END */

    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }

    // Function to Design the PopUp
    func popUpDesign() {
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0

        self.popUpView.layer.cornerRadius = 6
    }
    // End of Function to Design the PopUp

    // API call to change the Password
    func changePwdCalled() {
        self.startLoader()
        let newPwd = self.newPwd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let oldPwd = self.oldPwd.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

        var passwordObject = Password()
        passwordObject.newPassword = newPwd
        passwordObject.password = oldPwd

        let chngPwd = changePasswordOperation(password: passwordObject)
        chngPwd.addDidFinishBlockObserver { [unowned self] (_, _) in
            let status = self.appSharedPrefernce.getAppSharedPreferences(key: "editChangePassword") as? String ?? ""
            
            if status == "0" {
                DispatchQueue.main.async {
                    self.view.makeToast(self.profilemsg.paswordchangesuccess, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                    self.view.hideToast()
                    self.stopLoader()
                    let loginPreference = self.appSharedPrefernce.getAppSharedPreferences(key: "loginPreferenceUsed") as? String ?? ""
                    let preferredLanguage = self.appSharedPrefernce.getAppSharedPreferences(key: "preferredLanguage") as? String ?? ""
                    self.appSharedPrefernce.removeAllAppSharedPreferences()
                    self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: preferredLanguage)
                    self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: loginPreference)
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.present(nextVC, animated: true, completion: nil)
                }

                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change password", label: "Password updated successfully", value: 0)
            } else if status == "3" {
                DispatchQueue.main.async {
                    self.view.makeToast(self.profilemsg.passwordnotchanged, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position:.bottom)
                }

                self.stopLoader()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                    self.view.hideToast()
                }

                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change password", label: "Old password mismatch occured", value: 0)
            } else if status == "NetworkError" {
                DispatchQueue.main.async {
                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                }
                self.stopLoader()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                    self.view.hideToast()
                }
            } else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                    self.stopLoader()
                    self.view.hideToast()
                }

                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change password", label: "Failed to change password due to internal server error", value: 0)
            }
            DispatchQueue.main.async {
                    self.appSharedPrefernce.removeAppSharedPreferences(key: "editChangePassword")

            }
        }
        AppDelegate.addProcedure(operation: chngPwd)
    }
    // End of API call to change the Password

    //Function to start and end Loader
    func startLoader() {
        DispatchQueue.main.async {
            self.oldPwd.isUserInteractionEnabled=false
            self.newPwd.isUserInteractionEnabled=false
            self.confirmPwd.isUserInteractionEnabled=false
            self.submitButton.isUserInteractionEnabled=false
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
    }

    func stopLoader() {
        DispatchQueue.main.async {
            self.oldPwd.isUserInteractionEnabled=true
            self.newPwd.isUserInteractionEnabled=true
            self.confirmPwd.isUserInteractionEnabled=true
            self.submitButton.isUserInteractionEnabled=true
            ScreenLoader.shared.stopLoader()
        }

    }
    //End of Function to start and end Loader

    //  Modified by Anagha Meshram on 31/01/2018
    //  To change the color of the underlined image when the focus is on the field
    @objc func textFieldDidChange(_ textField: UICustomTextField) {

        switch textField {

        case oldPwd:
            self.oldPwdBlueUnderLine.isHidden = false
            self.oldPwdBlackUnderLine.isHidden = true
            let oldPasswordText: String = textField.text!
            if oldPasswordText.count>0 {
                self.oldPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.currentpassword)

            } else {
                self.oldPwd.configOnError(withPlaceHolder: self.profilemsg.entercurrentpswd)
            }

        case newPwd:
            self.newPwdBlueUnderLine.isHidden = false
            self.newPwdBlackUnderLine.isHidden = true
            let newPasswordText: String = textField.text!
            if newPasswordText.count>0 {
                self.newPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.newpassword)

            } else {
                self.newPwd.configOnError(withPlaceHolder: self.profilemsg.enternewpwd)
            }

        case confirmPwd:
            self.reEnterBlueUnderLine.isHidden = false
            self.reEnterBlackUnderLine.isHidden = true
            let reEnterPassword = textField.text!
            if reEnterPassword.count>0 {
                self.confirmPwd.configOnErrorCleared(withPlaceHolder: self.profilemsg.retypepassword)

            } else {
                self.confirmPwd.configOnError(withPlaceHolder: self.profilemsg.retypepassword)
            }

        default:
            break
        }
    }
    //  End of To change the color of the underlined image when the focus is on the field

    //Function to Handle Hide show of the underline

    //  Modified by Anagha Meshram on 31/01/2018
    //  To change the color of the underlined image when the focus is not on the field
    @objc func textFieldDidEnd(_ textField: UICustomTextField) {

        switch textField {

        case oldPwd:

            self.oldPwdBlueUnderLine.isHidden = true
            self.oldPwdBlackUnderLine.isHidden = false
            let oldPasswordText: String = textField.text!
            if oldPasswordText.count>0 {
                if validationController.isValidPassword(testStr: oldPasswordText) {
                    textField.configOnErrorCleared(withPlaceHolder: self.profilemsg.currentpassword)

                } else {
                    textField.configOnError(withPlaceHolder: self.profilemsg.invalidcurrentpswd)
                    self.view.makeToast(self.profilemsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium, position: .bottom)
                }
            } else {
                textField.configOnError(withPlaceHolder: self.profilemsg.entercurrentpswd)
            }
        case newPwd:

            self.newPwdBlueUnderLine.isHidden = true
            self.newPwdBlackUnderLine.isHidden = false
            let newPasswordText: String = textField.text!
            if newPasswordText.count>0 {
                if validationController.isValidPassword(testStr: newPasswordText) {
                    textField.configOnErrorCleared(withPlaceHolder: self.profilemsg.newpassword)

                } else {
                    textField.configOnError(withPlaceHolder: self.profilemsg.invalidnewpwd)
                    self.view.makeToast( self.signupmsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium, position: .bottom)
                }
            } else {
                textField.configOnError(withPlaceHolder: self.profilemsg.enternewpwd)
            }
        case confirmPwd:

            self.reEnterBlueUnderLine.isHidden = true
            self.reEnterBlackUnderLine.isHidden = false
            let confirmPasswordTxt: String = textField.text!
            if confirmPasswordTxt.count>0 {
                if validationController.isValidPassword(testStr: confirmPasswordTxt) {
                    textField.configOnErrorCleared(withPlaceHolder: self.profilemsg.retypepassword)

                } else {
                    textField.configOnError(withPlaceHolder: self.profilemsg.invalidpwd)
                    self.view.makeToast(self.signupmsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium, position: .bottom)
                }
            } else {
                textField.configOnError(withPlaceHolder: self.profilemsg.retypepassword)
            }

        default:
            break
        }
    }
    //  End of to change the color of the underlined image when the focus is not on the field

}
