//
//  ProfileChangeMasterKeyVC.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 14/01/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class ProfileChangeMasterKeyVC: UIViewController, UITextFieldDelegate {

      var iconClick = true
      let appService = AppService.shared
      let appSharedPrefernce = AppSharedPreference()
      var reloadProtocol: ReloadProtocol?
      var obj: [String: Any]?
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      let networkStatus = NetworkStatus()
      @IBOutlet weak var oldMasterKey: UICustomTextField!
      @IBOutlet weak var popUpHeading: UILabel!
      @IBOutlet weak var newMasterKey: UICustomTextField!
      @IBOutlet weak var closePopUpButton: UIImageView!

      @IBOutlet weak var roundCorneredView: UIView!

      @IBOutlet weak var chnageMasterKeyLabel: UILabel!
      @IBOutlet weak var masterKeyBlueUnderline: UIImageView!
      @IBOutlet weak var masterKeyBlackUnderline: UIImageView!
      @IBOutlet weak var titleView: UIView!
      @IBOutlet weak var reEnterMasterKeyBlueUnderline: UIImageView!
      @IBOutlet weak var reEnterMasterKeyBlackUnderline: UIImageView!
      var newMasterKeyRelease = ""
      var placeholderone = ""
      var placeholdertwo = ""

      @IBAction func hideShowMstrKeyDetailsButton(_ sender: Any) {
            if(iconClick == true) {
                  // oldMasterKey.isSecureTextEntry = false
                  newMasterKey.isSecureTextEntry = false
                  iconClick = false
            } else {
                  //  oldMasterKey.isSecureTextEntry = true
                  newMasterKey.isSecureTextEntry = true
                  iconClick = true
            }
      }

      @IBOutlet weak var FinalSubmitButton: UIButton!

      @IBAction func closePopUpButtonTapped(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      func profileCardShadowView() {
            self.roundCorneredView.layer.shadowColor = UIColor.black.cgColor
            self.roundCorneredView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.roundCorneredView.layer.shadowOpacity = 0.7
            self.roundCorneredView.layer.shadowRadius = 6.0
            self.roundCorneredView.layer.masksToBounds = false
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.chnageMasterKeyLabel.text = self.profilemsg.chnageMasterKey
                  self.oldMasterKey.configOnErrorCleared(withPlaceHolder: self.profilemsg.masterkeyplaceholder)
                  self.newMasterKey.configOnErrorCleared(withPlaceHolder: self.profilemsg.newmasterkey)
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */
      @IBAction func submitButton(_ sender: Any) {

            self.submitButtontapped(oldKey: self.oldMasterKey.text!, newKey: newMasterKey.text!)

      }

      func submitButtontapped(oldKey: String, newKey: String) {

            if (oldKey.characters.count) != 4 {
                  self.oldMasterKey.configOnError(withPlaceHolder: self.profilemsg.entercurrentmasterkey)
                  placeholderone = oldMasterKey.placeholder!

            } else if (newKey.characters.count) != 4 {
                  self.newMasterKey.configOnError(withPlaceHolder: self.profilemsg.enternewmk)
                  placeholdertwo = newMasterKey.placeholder!
            } else {
                  changeMasterKeyCalled()
            }

      }

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.setLocalizationText()
            self.addObservers()

            self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as? [String: Any]

            self.roundCorneredView.layer.cornerRadius = 10
            self.titleView.layer.cornerRadius = 10

            self.oldMasterKey.isSecureTextEntry = true
            self.newMasterKey.isSecureTextEntry = true

            iconClick = true
            profileCardShadowView()
            self.subscribeToNotifications()

            self.oldMasterKey.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.newMasterKey.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.oldMasterKey.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.newMasterKey.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.oldMasterKey.delegate = self
            self.newMasterKey.delegate = self
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ProfileChangeMasterKeyVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      func subscribeToNotifications() {
            NotificationCenter.default.addObserver(forName: DISMISS_CHANGE_MASTER_KEY_PAGE, object: nil, queue: nil) { (_) in
                  OperationQueue.main.addOperation ({
                        self.dismiss(animated: true, completion: nil)
                  })
            }
      }

      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                     replacementString string: String) -> Bool {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case self.oldMasterKey:
                  self.masterKeyBlueUnderline.isHidden = false
                  self.masterKeyBlackUnderline.isHidden = true

                  let oldPasswordText: String = textField.text!
                  if oldPasswordText.count>0 {
                        self.oldMasterKey.configOnErrorCleared(withPlaceHolder: self.profilemsg.masterkeyplaceholder)
                        if oldPasswordText.count>4 {
                              self.oldMasterKey.configOnError(withPlaceHolder: self.profilemsg.Invalidcurrentmasterkey)
                        } else {
                              self.oldMasterKey.configOnErrorCleared(withPlaceHolder: self.profilemsg.masterkeyplaceholder)
                        }
                  } else {
                        self.oldMasterKey.configOnError(withPlaceHolder: self.profilemsg.entercurrentmasterkey)
                  }

            case newMasterKey:
                  self.reEnterMasterKeyBlueUnderline.isHidden = false
                  self.reEnterMasterKeyBlackUnderline.isHidden = true
                  let newPasswordText: String = textField.text!
                  if  newPasswordText.count>0 {
                        self.newMasterKey.configOnErrorCleared(withPlaceHolder: self.profilemsg.newmasterkey)
                        if newPasswordText.count>4 {
                              self.newMasterKey.configOnError(withPlaceHolder: self.profilemsg.invalidnewmasterkey)
                        } else {
                              self.newMasterKey.configOnErrorCleared(withPlaceHolder: self.profilemsg.newmasterkey)
                        }
                  } else {
                        self.newMasterKey.configOnError(withPlaceHolder: self.profilemsg.enternewmk)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {

            switch textField {

            case oldMasterKey:

                  self.masterKeyBlueUnderline.isHidden = true
                  self.masterKeyBlackUnderline.isHidden = false

            case newMasterKey:

                  self.reEnterMasterKeyBlueUnderline.isHidden = true
                  self.reEnterMasterKeyBlackUnderline.isHidden = false

            default:
                  break
            }
      }

      // API call to change the Master Key
      func changeMasterKeyCalled() {
            guard networkStatus.isNetworkReachable()
                  else {
                        DispatchQueue.main.async {
                              self.view.makeToast(self.appService.checkInternetConnectionMessage)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0, execute: {
                              self.view.hideToast()
                        })
                        return
            }

            self.startLoader()
            var masterKeyObject = MasterKey()
            masterKeyObject.masterkey = self.oldMasterKey.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            masterKeyObject.newMasterKey = self.newMasterKey.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.appSharedPrefernce.setAppSharedPreferences(key: "newMasterKey", value: self.newMasterKey.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
            let chngmasterKey = changeMasterKeyOperation(masterKey: masterKeyObject)
            chngmasterKey.addDidFinishBlockObserver { [unowned self] (_, _) in
                  let statusCodeRecieved = self.appSharedPrefernce.getAppSharedPreferences(key: "editChangeMasterKey") as? String ?? ""
                  if(statusCodeRecieved == "0" || statusCodeRecieved == "200") {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.updatemasterkey, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                            
                            
                        }

                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Master key changed successfully", value: 0)

                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                              self.dismiss(animated: true, completion: nil)
                              self.reloadProtocol?.reloadData(toReload: true)
                              self.stopLoader()
                        }

                  } else if(statusCodeRecieved == "3") {
                     self.stopLoader()
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.masterkeymismatch, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              self.sendotpFunctionCall()
                        }

                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                            
                        }

                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Old master key mismatch occured - OTP verification necessary to change master key", value: 0)

                  } else if statusCodeRecieved == "NetworkError" {
                    self.stopLoader()
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.masterkeyFailure, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                        }
                  } else {
                     self.stopLoader()
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.masterkeyFailure, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                              self.view.hideToast()
                        }

                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Change Master key", label: "Failed to change master key", value: 0)

                  }
                self.appSharedPrefernce.removeAppSharedPreferences(key: "editChangeMasterKey")
            }
            AppDelegate.addProcedure(operation: chngmasterKey)
      }
      // End of API call to change the Master Key

      //Function to start and end Loader
      func startLoader() {
            DispatchQueue.main.async {
                self.oldMasterKey.isUserInteractionEnabled=false
                self.newMasterKey.isUserInteractionEnabled=false
                self.FinalSubmitButton.isUserInteractionEnabled=false
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                self.oldMasterKey.isUserInteractionEnabled=true
                self.newMasterKey.isUserInteractionEnabled=true
                self.FinalSubmitButton.isUserInteractionEnabled=true
                ScreenLoader.shared.stopLoader()
            }
      }
      //End of Function to start and end Loader

      //Function to Call Send OTP Service
      func sendotpFunctionCall() {
//            DispatchQueue.main.async {
                  self.startLoader()
//            }
        if let userObj = self.obj {
                  let email = userObj["email"] as? String ?? ""
                  let countrycode = userObj["countryCode"] as? String ?? ""
                  let mobile = userObj["phonenumber"] as? String ?? ""

                  var sendOtpObject = SendOTPObject()
                  sendOtpObject.email = email
                  sendOtpObject.countryCode = countrycode
                  sendOtpObject.phonenumber = mobile
//                  self.stopLoader()
                  let sendOtp = sendOtpUpdateOperation(sendOtp: sendOtpObject)
                  sendOtp.addDidFinishBlockObserver { [unowned self] (_, _) in
                        let status = self.appSharedPrefernce.getAppSharedPreferences(key: "sendOtpStatus") as? String ?? ""
//                        DispatchQueue.main.async {
                              self.stopLoader()
//                        }
                        if status == "200" {
                              //*********************** after deleting MasterKeyOTPVerificationVC *****************//

                              DispatchQueue.main.async {
                                    self.newMasterKeyRelease = self.newMasterKey.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "newMasterKey", value: self.newMasterKeyRelease )
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
                                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "ProfileOTPVerificationVC") as! ProfileOTPVerificationVC
                                    nextVC.redirectProtocol = self as? RedirectProtocol
                                    nextVC.process = "changeMasterKey"
                                    self.present(nextVC, animated: true, completion: nil)

                              }

                              //*********************** end *****************//
                        } else if status == "NetworkError" {
                              DispatchQueue.main.async {
                                self.view.makeToast(self.profilemsg.networkfailureMsg, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                              }
                        } else {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.networkfailureMsg)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Profile - Security", action: "Old master key mismatch - send OTP API called", label: "Failed to send OTP", value: 0)
                        }
                        self.appSharedPrefernce.removeAppSharedPreferences(key: "sendOtpStatus")
//                        self.stopLoader()
//                        DispatchQueue.main.async {
                                    self.appSharedPrefernce.removeAppSharedPreferences(key: "sendOtpStatus")
//                                    self.stopLoader()
//                        }
                  }
                  AppDelegate.addProcedure(operation: sendOtp)
            }

      }
      //End of Function to Call Send OTP Service

}

extension ProfileChangeMasterKeyVC: RedirectProtocol {
      func nextAction(method: String) {
            switch(method) {
            case "changeMasterKey":
                  self.dismiss(animated: true, completion: nil)
                  break
            default:
                  break

            }
      }

}
