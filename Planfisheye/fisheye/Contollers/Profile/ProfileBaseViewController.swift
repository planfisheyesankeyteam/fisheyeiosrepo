//
//  ProfileViewController.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 04/08/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

@objc protocol Profiledelegate {
      @objc optional func updateParent()
      @objc optional func updateChild(user: FEUser)
      @objc optional func addressupdated()
      @objc optional func enableMasterKey()
}

class ProfileBaseViewController: UIViewController, Profiledelegate {
      let appService = AppService.shared
      let appSharedPrefernce = AppSharedPreference()
      var idtoken = ""
      var fisheyeId = ""
      var ProfileMsgs = ProfileToastMsgHeadingSubheadingLabels.shared
      @IBAction func privacySwitchButtonTapped(_ sender: Any) {
      }
      var redirectToDashboard: RedirectToDashboard?
      @IBAction func getProfileButtonTapped(_ sender: Any) {
            loadProfileBasicView()
      }
      @IBAction func getProfileAddressTapped(_ sender: Any) {
            loadProfileAddressView()
      }
      @IBAction func getProfileMasterKey(_ sender: Any) {
            loadProfilePasswordView()
      }
      @IBAction func fisheyeIconTapped(_ sender: Any) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
            self.present(nextVC, animated: true, completion: nil)
      }
      @IBOutlet weak var fisheyeIcon: UIButton!
      @IBOutlet weak var profileBaseView: UIView!
      @IBOutlet weak var bottomViewlayer: UIView!
      @IBOutlet weak var mainview: UIView!
      @IBOutlet weak var baseView: UIView!
      @IBOutlet weak var profilePersonal: UIButton!
      @IBOutlet weak var profileAddress: UIButton!
      @IBOutlet weak var profileChange: UIButton!

      @IBOutlet weak var letsWorkTogetherLabel: UILabel!
      @IBOutlet weak var fisheyeLabel: UILabel!
      @IBOutlet weak var myProfileLabel: UILabel!
      weak var delegate: Profiledelegate?
      @IBAction func backclick(_ sender: Any) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
            self.present(nextVC, animated: true, completion: nil)
      }

      @IBAction func profilePersonalDetailsclick(_ sender: Any) {
            loadProfileBasicView()
      }

      func roundCorners(myview: UIView, id: Int) {
            if(id==0) {
                  let path = UIBezierPath(roundedRect: myview.bounds, byRoundingCorners: [.bottomLeft], cornerRadii: CGSize(width: 10, height: 10))
                  let mask = CAShapeLayer()
                  mask.path = path.cgPath
                  myview.layer.mask = mask
            }

            if(id==1) {
            }

            if(id==2) {
                  let path = UIBezierPath(roundedRect: myview.bounds, byRoundingCorners: [.bottomRight], cornerRadii: CGSize(width: 10, height: 10))
                  let mask = CAShapeLayer()
                  mask.path = path.cgPath
                  myview.layer.mask = mask
            }
      }

      @IBAction func profileAddressDetailsclick(_ sender: Any) {
            profilePersonal.backgroundColor = UIColor.clear
            profileChange.backgroundColor = UIColor.clear
            profileAddress.backgroundColor = UIColor.rebrandedPurple()
        profileAddress.setImage(UIImage(named: "profileaddresswhit.png"), for: UIControl.State.normal)
        profilePersonal.setImage(UIImage(named: "profilegrey.png"), for: UIControl.State.normal)
        profileChange.setImage(UIImage(named: "keygrey.png"), for: UIControl.State.normal)
            loadProfileAddressView()
      }

      @IBAction func profileChangeDetailsclick(_ sender: Any) {
        profilePersonal.backgroundColor = UIColor.clear
        profileAddress.backgroundColor = UIColor.clear
        profileChange.backgroundColor = UIColor.rebrandedPurple()
        profileChange.setImage(UIImage(named: "keywhite.png"), for: UIControl.State.normal)
        profilePersonal.setImage(UIImage(named: "profilegrey.png"), for: UIControl.State.normal)
        profileAddress.setImage(UIImage(named: "profileaddressgrey.png"), for: UIControl.State.normal)
        loadProfilePasswordView()
      }

      override func viewDidLoad() {
            super.viewDidLoad()
            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.setLocalizationText()
            self.addObservers()

            applyViewShadow()
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "id") as? String ?? ""
            loadProfileBasicView()
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.myProfileLabel.text = self.ProfileMsgs.myProfile
                  self.fisheyeLabel.text = self.ProfileMsgs.fisheye
                  self.letsWorkTogetherLabel.text = self.ProfileMsgs.letsWorkTogether
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      func loadProfilePasswordView() {
            let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileSecurityVC") as! ProfileSecurityVC
            vc.view.frame = self.baseView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            vc.view.frame.origin.y = -self.view.frame.size.height
            self.baseView.addSubview(vc.view)
            vc.view.frame.origin.y = self.baseView.bounds.origin.y
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  vc.view.frame.origin.y = self.baseView.bounds.origin.y
            }, completion: nil)
      }

      func loadProfileBasicView() {
        profileAddress.backgroundColor = UIColor.clear
        profileChange.backgroundColor = UIColor.clear
        profilePersonal.backgroundColor = UIColor.rebrandedPurple()
        profilePersonal.setImage(UIImage(named: "profilewhite.png"), for: UIControl.State.normal)
        profileAddress.setImage(UIImage(named: "profileaddressgrey.png"), for: UIControl.State.normal)
        profileChange.setImage(UIImage(named: "keygrey.png"), for: UIControl.State.normal)

        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileBasicViewController") as! ProfileBasicViewController
        vc.view.frame = self.baseView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
        vc.view.frame.origin.y = -self.view.frame.size.height
        self.baseView.addSubview(vc.view)
        vc.view.frame.origin.y = self.baseView.bounds.origin.y
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            vc.view.frame.origin.y = self.baseView.bounds.origin.y
        }, completion: nil)
      }

      func loadProfileAddressView() {
            let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileAddressViewController") as! ProfileAddressViewController
            vc.view.frame = self.baseView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
            vc.delegate = self
            vc.view.frame.origin.y = -self.view.frame.size.height
            self.baseView.addSubview(vc.view)
            vc.view.frame.origin.y = self.baseView.bounds.origin.y
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: { 
                  vc.view.frame.origin.y = self.baseView.bounds.origin.y
            }, completion: nil)
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

      override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            self.updateViewConstraints()
      }

      func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
            URLSession.shared.dataTask(with: url) {
                  (data, response, error) in
                  completion(data, response, error)
                  }.resume()
      }

      func applyViewShadow() {
//            let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController
//            vc.hideBottomviews()
            self.mainview.layer.cornerRadius = 6
            self.profileBaseView.layer.cornerRadius = 6
            self.profileBaseView.layer.borderWidth = 0
            self.profileBaseView.layer.borderColor = UIColor.black.cgColor
            self.profileBaseView.layer.shadowColor = UIColor.black.cgColor
            self.profileBaseView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.profileBaseView.layer.shadowOpacity = 0.4
            self.profileBaseView.layer.shadowRadius = 6.0
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }
}
