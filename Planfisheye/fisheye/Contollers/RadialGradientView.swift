//
//  RadialGradientView.swift
//  fisheye
//
//  Created by Sankey Solution on 11/14/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
@IBDesignable
class RadialGradientAllView: UIView {
    @IBInspectable var InsideColor: UIColor = UIColor.clear
    @IBInspectable var OutsideColor: UIColor = UIColor.clear

    override func draw(_ rect: CGRect) {
        let colors = [InsideColor.cgColor, OutsideColor.cgColor] as CFArray
        let endRadius = min(frame.width, frame.height)  * 0.1
        let center = CGPoint(x: (bounds.size.width  * 0.9), y: (bounds.size.height * 0.9))
        let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        UIGraphicsGetCurrentContext()!.drawRadialGradient(gradient!, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: endRadius, options: CGGradientDrawingOptions.drawsBeforeStartLocation)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
