//
//  AppVersionAndTermsAndConditions.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 16/04/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class AppVersionAndTermsAndConditions: UIViewController {

     @IBOutlet weak var mainView: UIView!
     @IBOutlet weak var versionLabel: UILabel!

      @IBOutlet weak var termsAndConditionsLink: UILabel!

      override func viewDidLoad() {
        super.viewDidLoad()
        self.applyPopUpShadow()
        self.getVersion()
//        self.showTermsAndConditionsLink()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

      @IBAction func crossBtnTapped(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      /* Created by Keerthi modified by vaishali  */
      func applyPopUpShadow() {

                  self.mainView.layer.cornerRadius = 8

                  // border
                  self.mainView.layer.borderWidth = 0
                  self.mainView.layer.borderColor = UIColor.black.cgColor

                  // shadow
                  self.mainView.layer.shadowColor = UIColor.black.cgColor
                  self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
                  self.mainView.layer.shadowOpacity = 0.7
                  self.mainView.layer.shadowRadius = 10.0
      }
         /* END */

      func getVersion() {
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                  if let bundle = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                        self.versionLabel.text = "Version " + version + " (" + bundle + ")"
                  } else {
                        self.versionLabel.text = ""
                  }
            } else {
                  self.versionLabel.text = ""
            }
      }

      func showTermsAndConditionsLink() {
            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: "T & C", attributes: underlineAttribute)
            termsAndConditionsLink.attributedText = underlineAttributedString
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
