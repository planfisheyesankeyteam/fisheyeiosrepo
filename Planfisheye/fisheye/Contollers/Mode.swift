//
//  Mode.swift
//  fisheye
//
//  Created by SankeyMacPro on 28/11/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

struct Mode {
      
      var translatedText:String!
      var actualText:String!
      
}
