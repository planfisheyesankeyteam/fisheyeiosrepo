//
//  ProfileToastMsgHeadingSubheadingLabels.swift
//  fisheye
//
//  Created by SankeyMacPro on 09/06/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

class ProfileToastMsgHeadingSubheadingLabels {
      static let shared: ProfileToastMsgHeadingSubheadingLabels = ProfileToastMsgHeadingSubheadingLabels()
      var toastDelayTime: Double = 4.0
      var toastdelatimebysix: Double = 6.0
      var mobNumLengthCantBeGreaterThan : NSInteger = 14
      var mobNumLengthCantBelesserThan : NSInteger = 3
      var emailRegX : String?
      var passwordRegX : String?

      /************************************* Toast Messages *********************************/
      //--------------------------------- Common Toast messages -------------------------//

      // ------ profilebasicVC ------ //
    
    var cancel: String = "Cancel"
    
    var delete: String = "Delete"
    


      //Network Failure
      var networkfailureMsg: String  = "Please check your Internet Connection and try again later!"

      //OTP sent
      var otpsent: String = "Security key sent successfully"
      var otpExpired: String = "OTP expired"
      //Network Failure
      var networkfailureMsgtwo: String = "Please check your Internet Connection and try again later!"

      //social login user not allowed to change the email
      var socialloginemail: String = "Sorry! You are using your social login from “Amazon”, therefore you  email address from Amazon cannot be changed"

      //User exist with phonenumber
      var userexitphonenumber: String = "This Contact already exists on Fisheye by Phone Number or Email id"

      //server error
      var servererror: String = "We are experiencing higher than expected traffic. Please bear with us, and try again in a few minutes"

      //profile update Successfully
      var profileUpdate: String  = "Your profile has been updated"

      //Profile update error
      var profileupdateerror: String = "We are experiencing higher than expected traffic, and unable to Sync your profile. Please bear with us, and try again in a few minutes"

      // ------ ProfileAddressVC ------ //

      //profile address delete
      var addressdelete: String = "Address deleted"

      //Network failure
      var networkfailuremssg3: String = "Please check your Internet Connection and try again later!"

      //Server Failure
      var serverfailure: String = "We are experiencing higher than expected traffic, and unable to Sync your profile. Please bear with us, and try again in a few minutes"

      // ------ ProfileAddressEditingPopupVc ------  //
    
    

      //check Internet Connection
      var internetconnection: String = "Please check your Internet Connection and try again later!"

      //address added successfully
      var addressaddedsuccess: String = "Address updated"

      //address update successfully
      var addressupdatesuccess: String = "Address updated"

      //Address Update failure
      var addressupatefailure: String = "We are experiencing higher than expected traffic, and unable to Sync your profile. Please bear with us, and try again in a few minutes"

      // ----- ProfileChangeMasterKeyVC ----- //

      // update master key
      var updatemasterkey: String = "Master key updated"

      var masterKeyAdded: String = "Master key added"
    
      // masterkey mismatch
      var masterkeymismatch: String = "Incorrect! Please provide the correct Master Key"

      // master key failure
      var masterkeyFailure: String = "Please check your Internet Connection and try again later!"

      // ----  ProfleChangePasswordVC ----- //

      // Incorrect password
      var incorrectPassword: String = "Incorrect Password"

      // Invalid password
      var inValidPassword: String = "Invalid Password"

      // password change successfully
      var paswordchangesuccess: String = "Your password has been changed. Please login with the new password."

      //password not changed
      var passwordnotchanged: String  = "The current password is incorrect. Please enter the correct password "

      // ----  ProfleOTPverificationVc ----- //
      //OTP send to registered Email and Mobile
      var otpsendtoemailPhone: String = "We are now sending two separate 6 digit security keys, to your registered email address and mobile number"

      // verification done sucessfully
      var verificationsuccess: String = "Verification completed"

      // profile delete suceesfully
      var profiledeletesuccess: String = "Sorry to see you leave. Your profile and your data uploaded by you have been deleted from Fisheye system. All Fisheye backups will also be updated with the deletion during the next scheduled backup cycle. You no longer exist on Fisheye!"

      //OTP verification failed
      var otpverificationfailed: String = "Security Verification failed"

      //unable to delete profile
      var unabletodelete: String = "Unable to complete your profile deletion. There seems to be an unknown error on the system. It may be due to the network connection to Fisheye systems, or due to more than expected traffic. We apologise for this error. Please try again in a few minutes"

      /************************************* Heading and Subheading *****************************/

      // Delete address popup

      var deleteaddheading: String = "Delete Address"
      var deleteaddsubheading: String = "Are you sure you want to delete your address?"

      // Delte profile popup
      var deleteprofileheading: String = "Delete Profile"
      var deleteprofilesubheading: String = "Are you sure you want to delete your fisheye profile?"
      var deleteButtonTitle: String = "Delete"

        var otpTimerPart1: String = "Security Key valid for "
        var minutes: String = " minutes"

      /***********************************  Labels ***************************************/

      // ---- ProfileBasicViewController --- //

      //privacy detail
      var privacydetaildisable: String  = "When this button is disabled, you personal information will be visible to other users."

      var privacyinfoshared: String = "* Your personal information is now visible to other users"

      var privacydetailenabled: String = "When this button is enabled, you personal information sharing is blocked"

      //privacy message
      var privacymessagenotshared: String = "* Your personal information sharing is now blocked"

      //Enter Code
      var entercode: String = "Enter code"

      //code
      var code: String = "Code"

      //Invalid phone no placeholder
      var invalidphone = "Invalid Mobile number"

      //Phone no placeholder
      var phone: String  = "Phone no."

      //Invalid email
      var invalidemail: String  = "Invalid email address"

      //email id
      var emailid: String = "Email address"

      //Enter name
      var entername: String = "Enter name"

      //Name
      var name: String = "Name"

      //Email placeholder
      var email: String = "Email"

      //inavlid email
      var invalidemailplace: String = "Invalid email address"

      //privacy enabled
      var privacyenabled: String  = "Privacy Enabled"

      //privacy disabled
      var privacydisabled: String  = "Privacy Disabled"

      //* Personal Information is private
      var persInfoPrivate: String = "Your personal information is now blocked"

      //* Personal Information is shared
      var persInfoshared: String = "* Your personal information is now visible to other users"

      //select address type placeholder
      var addrestype: String = "Select Address Type"

      //Enter city
      var Entercity: String = "Enter City"

      //city
      var city: String = "City"

      //Enter state placeholder
      var enterstate: String = "Enter State"

      //State
      var state: String = "State"

      // Enter Picode placeholder
      var enterpincode: String = "Enter Postcode"

      //Enter pincode placeholder
      var pincode: String = "Postcode"

      //Enter Country placeholder
      var entercountry: String = "Enter Country"

      // Entery country placeholder
      var country: String = "Country"

      //address line 1 placeholder
      var addressonee: String = "Address Line 1"

        //address line 1 placeholder
      var addressTwo: String = "Address Line 2"

      //enter addressline one placeholder
      var enteraddresssone: String = "Enter Addressline 1"

      //enter address line 2 placeholder
       var enteraddressstwo: String = "Enter Addressline 2"

      // select address type placeholder
      var selctaddresstype: String  = "Select/Enter Address Type"

      //--- ProfileChangeMasterKeyVc -- //

      // Invalid Current master key placeholder
      var Invalidcurrentmasterkey: String = "Incorrect key"

      //Invalid new master key placeholder
      var invalidnewmasterkey: String = "Incorrect key"

      //Current masterkey placeholder
      var masterkeyplaceholder: String = "Current master key"

      //Enter current master key placeholder
      var entercurrentmasterkey: String = "Enter current master key"

      //new master key place holder
      var newmasterkey: String = "New master key"

      //Enter new master key placeholder
      var enternewmk: String = "Enter new master key"

      //--- ProfileChangePasswordVc label ---//

      //current password placeholder
      var currentpassword: String = "Current password"

      // Invalid current password placeholder
      var invalidcurrentpswd: String = "Incorrect current password"

      //enter current password placeholder
      var entercurrentpswd: String = "Enter current password"

      //New password placeholder
      var newpassword: String = "New password"

      //Invalid new password placeholder
      var invalidnewpwd: String = "Incorrect new password"

      //Enter new Password placeholder
      var  enternewpwd: String = "Enter new password"

      //Retype password placeholder
      var retypepassword: String = "Re-enter new password"

      // password placeholder
      var invalidpwd: String = "New password mismatch"

      //Confirm password placeholder
      var confirmpswd: String = "Confirm password"

      //New and retype password placeholder
      var newandretypepswd: String = "New passwords Mismatch"

    var passwordPolicy: String = "Password should be of length 6 to 15. It must contain atleast 1 Capital,1 Small Alphabets, 1 Numerical e.g Abcdef12"

      //--- ProfileOTPVerificationVc ---//

      //invalid email otp placeholder
      var invalidemailotp: String = "Invalid Email Key"

      //Email OTP placeholder
      var emailotp: String = "Email security key"

      //enter Email email OTP placeholder
      var enteremailOTP: String = "Your Email key"

      //Invalid mobile otp placeholder
      var invalidmobotp: String = "Invalid SMS Security Key"

      //mobile otp placeholder
      var mobileotp: String = "SMS Security Key"

      //enter mobile otp
      var entermobotp: String = "Your SMS key"

      //Invalid otp
      var invalidotp: String = "Invalid security key"

      //Enter you Otp
      var enterotp: String = "Enter security key"

      var myProfile: String = "My Profile"

      /**************************** Labels ************************/

        var letsWorkTogether: String  = "Let's work together"

        var fisheye: String = "Fisheye Hub"

        var basicInformation: String = "Basic Information"

        var profileNamePlaceholder: String = "Name"

        var profileEmail: String = "Email"

        var codePlaceholder: String = "Code"

        var phoneNumberPlaceholder: String = "Phone no."

        var selectGenderPlaceholder: String = "Select Gender"

        var selectLanguage: String = "Select Language"

        var promotionalCommText: String = "Option to receive messages from Fisheye to your communications details provided, so as to notify you about new and enhanced features, or any updates about the Fisheye ecosystem."

        var promotionalCommNote: String = "Note- Fisheye operates on a zero tolerance for selling or sharing any of your details to any 3rd party suppliers."

        var promotionalCommTitle: String = "Promotional communication"

        var personalInfoNotShared: String = "* Personal Information is not shared"

        var privacyEnaDisaText: String = "By enabling the privacy your personal information will not be shown to the other users."

        var privacyEnaDisTitle: String = ""

        var addressLabel: String = "Address"

        var emptyAddressListlabel: String = "You can add multiple addresses such as home address, office address etc"

        var addAddressLabel: String = "Add Address"

        var securityLabel: String = "Security"

        var chnageMasterKey: String = "Change master key"
    
        var addMasterKey: String = "Add master key"

        var deleteProfileLabel: String = "Delete Fisheye account"

        var chnagePasswordLabel: String = "Change password"

        var deleteProfileLabel2: String = "Delete Fisheye account"

        var verificationLabel: String = "Verification"

        var securityCodeLabel: String = "Security Code"

        //var otpTimerTimeLabel : String = "OTP valid for 14:59 minutes"

        var pleaseEnterPart1Label: String = "Please enter 6 digit code sent to your registered mobile and E-mail"

       // var
      var meetMe: String = "Meet me"

      var sync: String = "Sync"

      var pulse: String = "Pulse"

      var lookout: String = "Lookout"

      var logout: String = "Logout"

      var logoutConfirmation: String = "Are you sure, you want to Logout?"

      var male: String = "Male"

      var female: String = "Female"

      var transFemale: String = "Trans-Female"

      var biGender: String = "Bi-Gender"

      var nonBinary: String = "Non-Binary"

      var genderNonConforming: String = "Gender nonconforming"

      var undisclosed: String = "Undisclosed"

      var ratherNotSay: String = "Rather not say"

      var home: String  = "Home"

      var work: String = "Work"

}
