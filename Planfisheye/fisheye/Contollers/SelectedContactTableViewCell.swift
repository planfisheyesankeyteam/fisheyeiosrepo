//
//  SelectedContactTableViewCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 23/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

protocol  SelectedContactTableViewCellDelegate: class {
    func didRemoveContactTapped(_ sender: SelectedContactTableViewCell)
}

class SelectedContactTableViewCell: UITableViewCell {
    weak var selectedContactTableViewCellDelegate: SelectedContactTableViewCellDelegate?

    @IBOutlet weak var emailInsteadOfPhoneNumber: UILabel!
    @IBOutlet weak var isfisheyeIndicator: UIView!
    @IBOutlet weak var SelectedCellBaseView: UIView!
    @IBOutlet weak var selectedContactIamge: UIImageView!
    @IBOutlet weak var selectedContactEmail: UILabel!
    @IBOutlet weak var sepration: UIView!
    @IBOutlet weak var selectedContactPhoneNumber: UILabel!
    @IBOutlet weak var selectedContactName: UILabel!
    @IBOutlet weak var stackViewDetails: UIView!

    @IBAction func selectedContactRemoveBtnClick(_ sender: Any) {

        self.selectedContactTableViewCellDelegate?.didRemoveContactTapped(self)
    }

    @IBOutlet weak var selectedContactRemoveBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
