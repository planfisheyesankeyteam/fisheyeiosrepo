//
//  TrackingOption.swift
//  fisheye
//
//  Created by SankeyMacPro on 03/12/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

public struct TrackingOption {
      
      //public var isDeleted : Int!
      public var isTimeRequired : Bool?
      public var optionName : String!
      public var optionTranslatedName : String!
      public var trackingTime : NSArray!
      
}
