//
//  SyncReusableFunctions.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 11/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import Contacts

func getTime() -> Int {
      let time: Int = Int (Date().timeIntervalSince1970)
      return time * 1000
}

func loadSyncViewController() {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "syncViewController")
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
}

func loadPersonalInfoPage() {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactInSync")
      appSharedPrefernce.setAppSharedPreferences(key: "addContactInProgress", value: 0)
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
}

func loadPersonalInfoPageWithContactId() {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactInSync")
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
}

func loadPostalInfoPage() {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactPostalInfoPage")
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
}

func loadProfessionalInfoPage() {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactProffesionalInfoPage")
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
}

func loadGeneralInfoPage() {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "addContactGeneralInfoPage")

      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil)
}

func loadDetailsPersonalInfo(isEditMode: Bool) {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "UpdateContactPersonalInfoVC")
    let obj: [String: Bool] = ["isEditMode": isEditMode]
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil, userInfo: obj)
}

func loadDetailsPostalInfo(isEditMode: Bool) {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "UpdateContactPostalInfoVC")
    let obj: [String: Bool] = ["isEditMode": isEditMode]
    NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil, userInfo: obj)
}

func loadDetailsProfessionalInfo(isEditMode: Bool) {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "UpdateContactProfessionalInfoVC")
    let obj: [String: Bool] = ["isEditMode": isEditMode]
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil, userInfo: obj)
}

func loadDetailsGeneralInfo(isEditMode: Bool) {
      let appSharedPrefernce = AppSharedPreference()
      appSharedPrefernce.setAppSharedPreferences(key: "syncPageToLoad", value: "UpdateContactGeneralInfoVC")
    let obj: [String: Bool] = ["isEditMode": isEditMode]
      NotificationCenter.default.post(name: LOADE_NEW_PAGE, object: nil, userInfo: obj)
}

func getCurrentCountryCode() -> String {
      var currentCountryCode = "+"
      if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            currentCountryCode = currentCountryCode + getCountryCallingCode(countryRegionCode: countryCode)
      }
      return currentCountryCode
}

func getCurrentTImeStamp() -> String {
      let dateformatter = DateFormatter()
      dateformatter.dateFormat = "MM/dd/yy h:mm a Z"
      let now = dateformatter.string(from: Date())
      return now
}

func getCountryCallingCode(countryRegionCode: String) -> String {

      let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS": "1", "AD": "376", "AO": "244", "AI": "1", "AG": "1", "AR": "54", "AM": "374", "AW": "297", "AU": "61", "AT": "43", "AZ": "994", "BS": "1", "BH": "973", "BF": "226", "BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE": "32", "BZ": "501", "BJ": "229", "BM": "1", "BT": "975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD": "243", "CI": "225", "KH": "855", "CM": "237", "CA": "1", "CV": "238", "KY": "345", "CF": "236", "CH": "41", "CL": "56", "CN": "86", "CX": "61", "CO": "57", "KM": "269", "CG": "242", "CK": "682", "CR": "506", "CU": "53", "CY": "537", "CZ": "420", "DE": "49", "DK": "45", "DJ": "253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG": "20", "ER": "291", "EE": "372", "ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI": "358", "FR": "33", "GB": "44", "GF": "594", "GA": "241", "GS": "500", "GM": "220", "GE": "995", "GH": "233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD": "1", "GP": "590", "GU": "1", "GT": "502", "GN": "224", "GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN": "504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO": "246", "IS": "354", "IN": "91", "ID": "62", "IQ": "964", "IE": "353", "IT": "39", "JM": "1", "JP": "81", "JO": "962", "JE": "44", "KP": "850", "KR": "82", "KZ": "77", "KE": "254", "KI": "686", "KW": "965", "KG": "996", "KN": "1", "LC": "1", "LV": "371", "LB": "961", "LK": "94", "LS": "266", "LR": "231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY": "218", "MO": "853", "MK": "389", "MG": "261", "MW": "265", "MY": "60", "MV": "960", "ML": "223", "MT": "356", "MH": "692", "MQ": "596", "MR": "222", "MU": "230", "MX": "52", "MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA": "212", "MM": "95", "MF": "590", "MD": "373", "MZ": "258", "NA": "264", "NR": "674", "NP": "977", "NL": "31", "NC": "687", "NZ": "64", "NI": "505", "NE": "227", "NG": "234", "NU": "683", "NF": "672", "NO": "47", "OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG": "675", "PY": "595", "PE": "51", "PH": "63", "PL": "48", "PN": "872", "PT": "351", "PR": "1", "PS": "970", "QA": "974", "RO": "40", "RE": "262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA": "966", "SN": "221", "SC": "248", "SL": "232", "SG": "65", "SK": "421", "SI": "386", "SB": "677", "SH": "290", "SD": "249", "SR": "597", "SZ": "268", "SE": "46", "SV": "503", "ST": "239", "SO": "252", "SJ": "47", "SY": "963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG": "228", "TK": "690", "TO": "676", "TT": "1", "TN": "216", "TR": "90", "TM": "993", "TC": "1", "TV": "688", "UG": "256", "UA": "380", "US": "1", "UY": "598", "UZ": "998", "VA": "379", "VE": "58", "VN": "84", "VG": "1", "VI": "1", "VC": "1", "VU": "678", "WS": "685", "WF": "681", "YE": "967", "YT": "262", "ZA": "27", "ZM": "260", "ZW": "263"]
      let countryDialingCode = prefixCodes[countryRegionCode]
      return countryDialingCode!

}

func getFormattedContacts(contacts: [ContactObj]) -> [AnyObject] {

      var contactObj = [:] as [String: Any]
      var mainContactObj: [AnyObject] = []
      for obj in contacts {
            var tempsecondaryPhoneNumbers: [String] = []
            var tempsecondaryEmails: [String] = []
            tempsecondaryPhoneNumbers = obj.secondaryPhoneNumbers
            tempsecondaryEmails = obj.secondaryEmails
            if(tempsecondaryPhoneNumbers.count == 0) {
                  tempsecondaryPhoneNumbers = ["n/a"]
            }

            if(tempsecondaryEmails.count == 0) {
                  tempsecondaryEmails =  ["n/a"]
            }

            if(obj.addressStringArray1.count != 0 && obj.addressStringArray2.count != 0) {
                  var  type = ""
                  var formmated = ""
                  var streetAddress = ""
                  var locality = ""
                  var region = ""
                  var country = ""
                  var postalCode = ""

                  var  type1 = ""
                  var formmated1 = ""
                  var streetAddress1 = ""
                  var locality1 = ""
                  var region1 = ""
                  var country1 = ""
                  var postalCode1 = ""

                  if obj.addressStringArray1 == nil || obj.addressStringArray1[0] == "" || obj.addressStringArray1.count < 1 {
                        type = "n/a"
                  } else {
                        type = obj.addressStringArray1[0]
                  }

                  if obj.addressStringArray1 == nil || obj.addressStringArray1[1] == "" || obj.addressStringArray1.count < 2 {
                        formmated = "n/a"
                  } else {
                        formmated = obj.addressStringArray1[1]
                  }

                  if obj.addressStringArray1 == nil || obj.addressStringArray1[2] == "" || obj.addressStringArray1.count < 3 {
                        streetAddress = obj.addressStringArray1[2] ?? "n/a"
                  } else {
                        streetAddress = obj.addressStringArray1[2] ?? "n/a"
                  }

                  if obj.addressStringArray1 == nil || obj.addressStringArray1[3] == "" || obj.addressStringArray1.count < 4 {
                        locality = obj.addressStringArray1[3] ?? "n/a"
                  } else {
                        locality = obj.addressStringArray1[3] ?? "n/a"
                  }

                  if obj.addressStringArray1 == nil || obj.addressStringArray1[4] == "" || obj.addressStringArray1.count < 5 {
                        region = obj.addressStringArray1[4] ?? "n/a"
                  } else {
                        region = obj.addressStringArray1[4] ?? "n/a"
                  }

                  if obj.addressStringArray1 == nil || obj.addressStringArray1[5] == "" || obj.addressStringArray1.count < 6 {
                        country = obj.addressStringArray1[5] ?? "n/a"
                  } else {
                        country = obj.addressStringArray1[5] ?? "n/a"
                  }

                  if obj.addressStringArray1 == nil || obj.addressStringArray1[6] == "" || obj.addressStringArray1.count < 7 {
                        postalCode = obj.addressStringArray1[6] ?? "n/a"
                  } else {
                        postalCode = obj.addressStringArray1[6] ?? "n/a"
                  }

                  if obj.addressStringArray2 == nil || obj.addressStringArray2[0] == "" || obj.addressStringArray2.count < 1 {
                        type1 = "n/a"
                  } else {
                        type1 = obj.addressStringArray2[0]
                  }

                  if obj.addressStringArray2 == nil || obj.addressStringArray2[1] == "" || obj.addressStringArray2.count < 2 {
                        formmated1 = "n/a"
                  } else {
                        formmated1 = obj.addressStringArray2[1]
                  }

                  if obj.addressStringArray2 == nil || obj.addressStringArray2[2] == "" || obj.addressStringArray2.count < 3 {
                        streetAddress1 = obj.addressStringArray2[2] ?? "n/a"
                  } else {
                        streetAddress1 = obj.addressStringArray2[2] ?? "n/a"
                  }

                  if obj.addressStringArray2 == nil || obj.addressStringArray2[3] == "" || obj.addressStringArray2.count < 4 {
                        locality1 = obj.addressStringArray2[3] ?? "n/a"
                  } else {
                        locality1 = obj.addressStringArray2[3] ?? "n/a"
                  }

                  if obj.addressStringArray2 == nil || obj.addressStringArray2[4] == "" || obj.addressStringArray2.count < 5 {
                        region1 = obj.addressStringArray2[4] ?? "n/a"
                  } else {
                        region1 = obj.addressStringArray2[4] ?? "n/a"
                  }

                  if obj.addressStringArray2 == nil || obj.addressStringArray2[5] == "" || obj.addressStringArray2.count < 6 {
                        country1 = obj.addressStringArray2[5] ?? "n/a"
                  } else {
                        country1 = obj.addressStringArray2[5] ?? "n/a"
                  }

                  if obj.addressStringArray2 == nil || obj.addressStringArray2[6] == "" || obj.addressStringArray2.count < 7 {
                        postalCode1 = obj.addressStringArray2[6] ?? "n/a"
                  } else {
                        postalCode1 = obj.addressStringArray2[6] ?? "n/a"
                  }

                  contactObj  =
                        [
                              "userId": obj.userId,
                              "name": obj.name,
                              "email": obj.email,
                              "phoneNumber": obj.phoneNumber,
                              "addressList": [
                                          [
                                                "type": type,
                                                "formatted": formmated1,
                                                "street_address": streetAddress,
                                                "locality": locality,
                                                "region": region,
                                                "country": country,
                                                "postal_code": postalCode
                                          ],
                                          [
                                                "type": type1,
                                                "formatted": formmated1,
                                                "street_address": streetAddress1,
                                                "locality": locality1,
                                                "region": region1,
                                                "country": country1,
                                                "postal_code": postalCode1
                                          ]
                              ],
                              "secondaryPhoneNumbers": tempsecondaryPhoneNumbers,
                              "secondaryEmails": tempsecondaryEmails,
                              "company": obj.company,
                              "companyEmail": obj.companyEmail,
                              "title": obj.title,
                              "website": obj.website,
                              "notes": obj.notes,
                              "isdeleted": obj.isdeleted,
                              "ismerged": obj.ismerged,
                              "mergeParentId": obj.mergeParentId,
                              "source": obj.source,
                              "contactFEId": obj.contactFEId,
                              "contactId": String(obj.contactId),
                              "isBlocked": obj.isBlocked,
                              "isBlockedByContact": obj.isBlockedByContact,
                              "sharedData": obj.sharedData,
                              "privacyEnabled": obj.privacyEnabled,
                              "privacySetForContactUser": obj.privacySetForContactUser,
                              "picture": obj.picture,
                              "createdAt": obj.createdAt,
                              "updatedAt": obj.updatedAt

                        ] as [String: Any]
            } else if(obj.addressStringArray1.count != 0) {
                  var  type = ""
                  var formmated = ""
                  var streetAddress = ""
                  var locality = ""
                  var region = ""
                  var country = ""
                  var postalCode = ""
                  if obj.addressStringArray1[0] != ""{

                        if obj.addressStringArray1 == nil || obj.addressStringArray1[0] == "" || obj.addressStringArray1.count < 1 {
                              type = "n/a"
                        } else {
                              type = obj.addressStringArray1[0]
                        }

                        if obj.addressStringArray1 == nil || obj.addressStringArray1[1] == "" {
                              formmated = "n/a"

                        } else {
                              formmated = obj.addressStringArray1[1]
                        }

                        if obj.addressStringArray1 == nil || obj.addressStringArray1[2] == "" || obj.addressStringArray1.count < 3 {
                              streetAddress = obj.addressStringArray1[2] ?? "n/a"
                        } else {
                              streetAddress = obj.addressStringArray1[2] ?? "n/a"
                        }

                        if obj.addressStringArray1 == nil || obj.addressStringArray1[3] == "" || obj.addressStringArray1.count < 4 {
                              locality = obj.addressStringArray1[3] ?? "n/a"
                        } else {
                              locality = obj.addressStringArray1[3] ?? "n/a"
                        }

                        if obj.addressStringArray1 == nil || obj.addressStringArray1[4] == "" || obj.addressStringArray1.count < 5 {
                              region = obj.addressStringArray1[4] ?? "n/a"
                        } else {
                              region = obj.addressStringArray1[4] ?? "n/a"
                        }

                        if obj.addressStringArray1 == nil || obj.addressStringArray1[5] == "" || obj.addressStringArray1.count < 6 {
                              country = obj.addressStringArray1[5] ?? "n/a"
                        } else {
                              country = obj.addressStringArray1[5] ?? "n/a"
                        }

                        if obj.addressStringArray1 == nil || obj.addressStringArray1[6] == "" || obj.addressStringArray1.count < 7 {
                              postalCode = obj.addressStringArray1[6] ?? "n/a"
                        } else {
                              postalCode = obj.addressStringArray1[6] ?? "n/a"
                        }
                  }

                  contactObj  =
                        [
                              "userId": obj.userId,
                              "name": obj.name,
                              "email": obj.email,
                              "phoneNumber": obj.phoneNumber,
                              "secondaryPhoneNumbers": tempsecondaryPhoneNumbers,
                              "secondaryEmails": tempsecondaryEmails,
                              "addressList": [
                                          [
                                                "type": type,
                                                "formatted": formmated,
                                                "street_address": streetAddress,
                                                "locality": locality,
                                                "region": region,
                                                "country": country,
                                                "postal_code": postalCode
                                          ]
                              ],
                              "company": obj.company,
                              "companyEmail": obj.companyEmail,
                              "title": obj.title,
                              "website": obj.website,
                              "notes": obj.notes,
                              "isdeleted": obj.isdeleted,
                              "ismerged": obj.ismerged,
                              "mergeParentId": obj.mergeParentId,
                              "source": obj.source,
                              "contactFEId": obj.contactFEId,
                              "contactId": String(obj.contactId),
                              "isBlocked": obj.isBlocked,
                              "isBlockedByContact": obj.isBlockedByContact,
                              "sharedData": obj.sharedData,
                              "privacyEnabled": obj.privacyEnabled,
                              "privacySetForContactUser": obj.privacySetForContactUser,
                              "picture": obj.picture,
                              "createdAt": obj.createdAt,
                              "updatedAt": obj.updatedAt
                        ] as [String: Any]
            } else {
                  contactObj  =
                        [
                              "userId": obj.userId,
                              "name": obj.name,
                              "email": obj.email,
                              "phoneNumber": obj.phoneNumber,
                              "secondaryPhoneNumbers": tempsecondaryPhoneNumbers,
                              "secondaryEmails": tempsecondaryEmails,
                              "company": obj.company,
                              "companyEmail": obj.companyEmail,
                              "title": obj.title,
                              "website": obj.website,
                              "notes": obj.notes,
                              "isdeleted": obj.isdeleted,
                              "ismerged": obj.ismerged,
                              "mergeParentId": obj.mergeParentId,
                              "source": obj.source,
                              "contactFEId": obj.contactFEId,
                              "contactId": String(obj.contactId),
                              "isBlocked": obj.isBlocked,
                              "isBlockedByContact": obj.isBlockedByContact,
                              "sharedData": obj.sharedData,
                              "privacyEnabled": obj.privacyEnabled,
                              "privacySetForContactUser": obj.privacySetForContactUser,
                              "picture": obj.picture,
                              "createdAt": obj.createdAt,
                              "updatedAt": obj.updatedAt
                        ] as [String: Any]
            }

            mainContactObj.append(contactObj as AnyObject)
      }

      return mainContactObj
}

func requestAccessToContacts(){
      let contactStore = CNContactStore()
      contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
            let authorizationStatus = ContactsSync.shared.getContactsPermissionStatus()
            switch authorizationStatus {
            case .authorized:
                  ContactsSync.shared.isToCheckBlockedByContacts = true
                  ContactsSync.shared.frontEndSync()
                  break
            default:
                  break
            }
      })
}

func takePermissionToSyncNFrontendSync(isToFrontEndSync: Bool){
      let authorizationStatus = ContactsSync.shared.getContactsPermissionStatus()
      switch authorizationStatus {
      case .authorized:
            if isToFrontEndSync{
                  ContactsSync.shared.isToCheckBlockedByContacts = true
                  ContactsSync.shared.frontEndSync()
            }
            break
      case  .notDetermined:
            if isToFrontEndSync{
                  requestAccessToContacts()
            }
            break
      default:
            break
      }
}
