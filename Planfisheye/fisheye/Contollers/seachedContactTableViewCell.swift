//
//  seachedContactTableViewCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 22/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class seachedContactTableViewCell: UITableViewCell {

    @IBOutlet weak var viewEqualToCell: UIView!
    @IBOutlet weak var imageOfSearchContact: UIImageView!
    @IBOutlet weak var emailOfSearchedContact: UILabel!
    @IBOutlet weak var phoneNumberOfSearchedContact: UILabel!
    @IBOutlet weak var sepBtnSearchedNumAndEmail: UILabel!
    @IBOutlet weak var stackViewDetails: UIView!
    @IBOutlet weak var seachedContactName: UILabel!
    @IBOutlet weak var isFisheyeIndication: UIImageView!
    @IBOutlet weak var emailInsteadOfPhoneNumber: UILabel!

    var isFisheyeContact: Bool!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
