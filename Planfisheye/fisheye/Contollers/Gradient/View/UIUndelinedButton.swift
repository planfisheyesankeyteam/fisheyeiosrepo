//
//  UIUndelinedButton.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class UIUndelinedButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func draw(_ rect: CGRect) {
        // Drawing code

        let border = CALayer.init(layer: self)
        border.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        border.backgroundColor = UIColor.gray.cgColor
        self.layer.addSublayer(border)
    }

    required public init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
       // self.setTitleColor(UIColor.secondary_text(), for: .normal)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        self.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 14)!
        self.setTitleColor(UIColor.lightGray, for: .normal)
        self.titleLabel?.textAlignment = .left
        self.contentHorizontalAlignment = .left
    }

    func setText(text: String) {

        self.setTitle(text, for: .normal)
      //  self.setTitleColor(UIColor.primary_text(), for: .normal)
    }

    func resetText(text: String) {

        self.setTitle(text, for: .normal)
        self.setTitleColor(UIColor.lightGray, for: .normal)
    }

}
