//
//  UIRoundedButton.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class UIRoundedButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
  */

    override var intrinsicContentSize: CGSize {
        get {
            let labelSize = titleLabel?.sizeThatFits(CGSize(width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude)) ?? CGSize.zero

            let desiredButtonSize = CGSize(width: labelSize.width + titleEdgeInsets.left + titleEdgeInsets.right + 50, height: self.frame.size.height + 20)

            return desiredButtonSize
        }
    }

    required init(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)!
        //self.backgroundColor = UIColor.primary()
        //self.setTitleColor(UIColor.icons(), for: .normal)
        self.titleLabel?.textAlignment = .center
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        self.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 14)!

    }

   // let rect =  .boundingRectWithSize(boundingSize, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes:[NSFontAttributeName : textFont], context: nil)

   // return CGSizeMake(rect.width < 50 ? 50 : rect.width, rect.height)

}
