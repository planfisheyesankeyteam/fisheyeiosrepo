//
//  CardView.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 06/07/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

@objc protocol CardViewDelegate {

    func didSelectCard(withTitle: String)
    func moveCard(position: Int)
    func logbookClick(withTitle: String)
}

class CardView: UIViewController {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    var loadingComplete: ((_ success: Bool) -> Void)?

    enum Position: Int {
        case up = 0
        case right
        case down
        case left
    }

    @IBOutlet weak var bkgImgView: UIImageView!

   // @IBOutlet weak var cardNameView: UIView!
    @IBOutlet weak var titleLblConstraints: NSLayoutConstraint!

    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!

    var position: Position!

    weak var cardViewDelegate: CardViewDelegate?

    @IBOutlet weak var imageView: UIImageView!

//    @IBOutlet weak var inviteView: UIView!

    @IBOutlet weak var createView: UIView!

    @IBOutlet weak var createLbl: UILabel!

    @IBOutlet weak var titleLbl: UILabel!

    //@IBOutlet weak var subTitleLbl: UILabel!

    @IBOutlet weak var btn: UIButton!

    @IBOutlet weak var inviteCountLbl: UILabel!

//    @IBOutlet weak var inviteLbl: UILabel!

    @IBOutlet weak var notificationBtn: UIButton!

    @IBOutlet var main: UIView!
    func hideViewAfterloadingComplete(_ success: Bool) {
        // handle successful sign in
        if (success) {
            self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
            //self.imageViewTopConstraint.constant = self.bkgImgView.frame.size.height * CGFloat(0.1)
            self.cardNameView.layer.cornerRadius = 6
            self.cardNameView.layer.shadowColor = UIColor.black.cgColor
            self.cardNameView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.cardNameView.layer.shadowOpacity = 0.4
            self.cardNameView.layer.shadowRadius = 6.0
            self.cardNameView.layer.masksToBounds = false
           // self.titleLblConstraints.constant = self.bkgImgView.frame.size.height * CGFloat(0.6)
            self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
            self.view.updateConstraints()

            self.view.layoutSubviews()

        } else {
            // handle cancel operation from user
//self.imageViewTopConstraint.constant = self.bkgImgView.frame.size.height * CGFloat(0.13)
           self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
            self.cardNameView.layer.cornerRadius = 6
            self.cardNameView.layer.shadowColor = UIColor.black.cgColor
            self.cardNameView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.cardNameView.layer.shadowOpacity = 0.4
            self.cardNameView.layer.shadowRadius = 6.0
            self.cardNameView.layer.masksToBounds = false
     //       self.titleLblConstraints.constant = self.bkgImgView.frame.size.height * CGFloat(0.45)

            self.view.updateConstraints()

            self.view.layoutSubviews()
        }
    }

    @IBOutlet weak var cardNameView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
//        self.main.backgroundColor = UIColor.duckEggBlue()
        self.titleLbl.textColor = UIColor.frenchBlue()

       // self.subTitleLbl.textColor = UIColor.frenchBlue54()

//        self.btn.layer.cornerRadius = 8
//        self.btn.layer.masksToBounds = true
//        self.btn.layer.shadowColor = UIColor.lightGray.cgColor
//        self.btn.layer.shadowOpacity = 1
//        self.btn.layer.shadowOffset = CGSize.zero
//        self.btn.layer.shadowRadius = 10
//        self.btn.layer.shouldRasterize = false

        //self.bkgImgView.isHidden = true
        self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
        self.cardNameView.layer.cornerRadius = 6
        self.cardNameView.layer.shadowColor = UIColor.black.cgColor
        self.cardNameView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.cardNameView.layer.shadowOpacity = 0.4
        self.cardNameView.layer.shadowRadius = 6.0
        self.cardNameView.layer.masksToBounds = false

        self.titleLbl.adjustsFontSizeToFitWidth = true

        self.inviteCountLbl.adjustsFontSizeToFitWidth = true

       // self.subTitleLbl.adjustsFontSizeToFitWidth = true

//        self.inviteLbl.adjustsFontSizeToFitWidth = true

        self.createLbl.adjustsFontSizeToFitWidth = true

        self.inviteCountLbl.backgroundColor = UIColor.lightUrple()

//        self.inviteLbl.textColor = UIColor.lightUrple()

        self.createLbl.textColor = UIColor.black60Two54()

        self.inviteCountLbl.layer.cornerRadius = self.inviteCountLbl.frame.size.width/2

        self.inviteCountLbl.layer.masksToBounds = true

        self.createView.isUserInteractionEnabled = true

        let gesture = UITapGestureRecognizer(target: self, action: #selector(createTapped))

        self.createView.addGestureRecognizer(gesture)

        self.btn.isUserInteractionEnabled = true

        self.btn.addTarget(self, action: #selector(navigateTo), for: .touchUpInside)
        self.notificationBtn.addTarget(self, action: #selector(notificationBtnClick), for: .touchUpInside)

    }

    func createTapped() {

    }

    func notificationBtnClick() {
   cardViewDelegate?.logbookClick(withTitle: self.titleLbl.text!)

    }

    func navigateTo() {
        if self.position == .down {

            guard let method = cardViewDelegate?.didSelectCard(withTitle: self.titleLbl.text!) else {
                return
            }
            method
        } else {
            guard let method = cardViewDelegate?.moveCard(position: self.position.rawValue) else {
                return
            }
            method
        }
    }

}

extension UIView {

    @discardableResult   // 1
    func fromNib<T: UIView>() -> T? {   // 2
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as? T else {    // 3
            // xib not loaded, or it's top view is of the wrong type
            return nil
        }
        self.addSubview(view)     // 4
        view.translatesAutoresizingMaskIntoConstraints = false   // 5
      //  view.layouta(to: self)   // 6
        return view   // 7
    }
}
