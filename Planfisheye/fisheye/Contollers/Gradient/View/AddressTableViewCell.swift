//
//  AddressTableViewCell.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 05/07/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!

    @IBOutlet weak var editImageView: UIImageView!
    @IBOutlet weak var workImage: UIImageView!
    @IBOutlet weak var homeImage: UIImageView!

    @IBOutlet weak var addressType: UILabel!

    @IBOutlet weak var deleteAddress: UIImageView!
    @IBOutlet weak var address: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.view.layer.shadowColor = UIColor.lightGray.cgColor
        self.view.layer.shadowOpacity = 1
        self.view.layer.shadowOffset = CGSize.zero
        self.view.layer.shadowRadius = 5
        self.view.layer.shouldRasterize = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
