//
//  UICustomTextField.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class UICustomTextField: JVFloatLabeledTextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    required public init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

     //   self.floatingLabelYPadding = -5.0

        self.textColor = UIColor.black60Two87()

        self.floatingLabelActiveTextColor = UIColor.frenchBlue()

        self.floatingLabelTextColor = UIColor.frenchBlue()

        self.animateEvenIfNotFirstResponder = true

        self.font = UIFont(name: "ROBOTO-REGULAR", size: 16)

        let attrs: [String: AnyObject] = [NSFontAttributeName: self.font!, NSForegroundColorAttributeName: UIColor.lavender54()]

        let cString = NSAttributedString(string: self.placeholder!, attributes: attrs)

        self.setAttributedPlaceholder(cString, floatingTitle: self.placeholder!)

    }

    func configOnFirstResponder(withPlaceHolder text: String) {

        if self.isFirstResponder == true {

            self.floatingLabelActiveTextColor = UIColor.frenchBlue87()

            self.floatingLabelTextColor = UIColor.black60Two54()

            self.placeholder = text
        }
    }

    func configOnError(withPlaceHolder text: String) {

        let attrbStr = NSMutableAttributedString()

        let placeholderStr = attrbStr.colorString(text, withColor: UIColor.red50())

        self.attributedPlaceholder = placeholderStr

        self.floatingLabelTextColor = UIColor.red

        self.floatingLabelActiveTextColor = UIColor.red

    }
    func configOnErrorCleared(withPlaceHolder text: String) {

        let attrbStr = NSMutableAttributedString()

        let placeholderStr = attrbStr.colorString(text, withColor: UIColor.frenchBlue70())

        self.attributedPlaceholder = placeholderStr

        self.floatingLabelTextColor = UIColor.frenchBlue70()

        self.floatingLabelActiveTextColor = UIColor.frenchBlue70()

    }

}
