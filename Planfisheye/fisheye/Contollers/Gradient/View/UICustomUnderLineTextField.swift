//
//  UICustomUnderLineTextField.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class UICustomUnderLineTextField: JVFloatLabeledTextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
 
 
    */

    var border: UIView!

    let transition = CATransition()

    let gradientLayer = CAGradientLayer()

    override func draw(_ rect: CGRect) {
        // Drawing code
        border = UIView()
        border.frame = CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        self.addSubview(border)
        self.createGradientLayer(inView: self.border, colors: [UIColor.black.cgColor, UIColor.white.cgColor])

    }

    func createGradientLayer(inView view: UIView, colors: [CGColor]) {

        // Hoizontal - commenting these two lines will make the gradient veritcal
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)

        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)

        let gradientLocations: [NSNumber] = [0.0, 1.0]

        gradientLayer.frame = view.bounds

        gradientLayer.colors = colors

        gradientLayer.locations = gradientLocations

        view.layer.addSublayer(gradientLayer)

    }

    func annimateLayer(inView view: UIView) {
        transition.duration = 0.2
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        view.layer.add(transition, forKey: kCATransition)
    }

    required public init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        self.borderStyle = .none

        self.textColor = UIColor.black60Two87()

        self.floatingLabelActiveTextColor = UIColor.frenchBlue87()

        self.floatingLabelTextColor = UIColor.black60Two54()

        self.animateEvenIfNotFirstResponder = true

        self.font = UIFont(name: "ROBOTO-REGULAR", size: 16)

        let attrs: [String: AnyObject] = [NSFontAttributeName: self.font!, NSForegroundColorAttributeName: UIColor.lavender54()]

        let cString = NSAttributedString(string: self.placeholder!, attributes: attrs)

        self.setAttributedPlaceholder(cString, floatingTitle: self.placeholder!)

        }

    override var text: String? {
        didSet {
           // self.floatingLabelActiveTextColor = UIColor.accent()
           // self.floatingLabelTextColor = UIColor.accent()
//            border.backgroundColor = UIColor.red.cgColor
        }
    }

    func configOnFirstResponder() {

       if self.isFirstResponder == true {

            self.createGradientLayer(inView: self.border, colors: [UIColor.frenchBlue().cgColor, UIColor.white.cgColor])

            annimateLayer(inView: self.border)

        }
    }

    func configOnFirstResponder(withPlaceHolder text: String) {

        if self.isFirstResponder == true {
            self.createGradientLayer(inView: self.border, colors: [UIColor.frenchBlue().cgColor, UIColor.white.cgColor])
            self.floatingLabelActiveTextColor = UIColor.frenchBlue87()

            self.floatingLabelTextColor = UIColor.black60Two54()
            self.placeholder = text
        }
    }

    func configOnResignResponder() {
        self.createGradientLayer(inView: self.border, colors: [UIColor.black.cgColor, UIColor.white.cgColor])
    }

    func configOnError(withPlaceHolder text: String) {

        let attrbStr = NSMutableAttributedString()

        let placeholderStr = attrbStr.colorString(text, withColor: UIColor.red50())

        self.createGradientLayer(inView: self.border, colors: [UIColor.red.cgColor, UIColor.white.cgColor])

        self.attributedPlaceholder = placeholderStr

        self.floatingLabelTextColor = UIColor.red

        self.floatingLabelActiveTextColor = UIColor.red

        self.border.backgroundColor = UIColor.red
    }

}

extension UITextField {

    func setLeftPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }

    func setRightPaddingPoints(_ amount: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
