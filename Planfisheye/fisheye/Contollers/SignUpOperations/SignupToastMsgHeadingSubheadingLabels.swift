//
//  SignupToastMsgHeadingSubheadingLabels.swift
//  fisheye
//
//  Created by SankeyMacPro on 11/06/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

class SignupToastMsgHeadingSubheadingLabels {
    static let shared: SignupToastMsgHeadingSubheadingLabels = SignupToastMsgHeadingSubheadingLabels()

      var toastDelayTime: Double = 4.0
       var toastdelatimebysix: Double = 6.0
      var toastDelayMedium: Double = 5.0

      /************************************* Toast Messages *********************************/
      //--------------------------------- Common Toast messages -------------------------//
//      ghhghghg
    
        var genderArrayList = ["Male", "Female", "Trans-Female", "Bi-Gender", "Non-Binary", "Gender nonconforming", "Undisclosed", "Rather not say" ]
      
       var gender = [GenderListStructure(genderTranslatedText:"Male",genderActualText:"Male"), GenderListStructure(genderTranslatedText:"Female",genderActualText:"Female"),
            GenderListStructure(genderTranslatedText:"Trans-Female",genderActualText:"Trans-Female"),
      GenderListStructure(genderTranslatedText:"Non-Binary",genderActualText:"Non-Binary"),
      GenderListStructure(genderTranslatedText:"Gender nonconfirming",genderActualText:"Gender nonconfirming"),
      GenderListStructure(genderTranslatedText:"Undisclose",genderActualText:"Undisclose"),
      GenderListStructure(genderTranslatedText:"Rather not say",genderActualText:"Rather not say")]
    
        var localGenderArrayList = [GenderListStructure]();
    
    
 //--- SignUpMasterKey ---//
      //Error while registering master key
      var errormasterkey: String =  "We are experiencing higher than expected traffic, and unable to register your master key. Please bear with us, and try again in a few minutes"

      // key mismatch
      var keymismatch: String = "Incorrect Master Key"

//--- SignUpSecuritQuestionVc --//

      //Registration success
      var registrationsuccess: String = "Registration completed"
//--- SignUpOtpVc --//

      // failed to resend otp
      var resndotp: String = "Failed to resend Security Key due to a technical error. Please try again"

      // user exist , complete sign up process
      var userexist: String = "This User ID has already started registration. Please complete your sign-up process"

      //user exist ,login to continue
      var userexistlogin: String = "This User ID is already registered on Fisheye. Please login to continue"

      // sms sending failed
      var smssendingfailed: String = "Failed to send security key due to a technical error. Please try again."

//--- SignUpwithSocialLogin --//
    var phoneNumberShouldBeValid: String = "Please provide a valid email address and mobile number for registration and verification, to helps maintain your account security"

      //user exist with phone try with different phone number
      var userexistwithphonenumber: String = "This phone number is already registered. Please use a different phone number or log in as the registered user with the other phone number"

      //user exist with email
      var userexistwithemail: String = "This email address is already registered. Please use a different email address or log in as a registered user with the other email address"
      /************************************* Heading and Subheading *****************************/
//--- SignUpMyProfileVc ---//
      var registrationheading: String = "Registration"
      var registrationsubheading: String = "This phone number or email address is already registered on Fisheye. Do you wish to update these details now?. Press Ok to modify"

 //--- ForgetPasswordscreen1VC ---//

      //user not found
      var usernotfound: String  = "User ID provided Not Found"

      //email or phone number doesnot match
      var emailphonenumbernotmatch: String = "This combination of Email or phone number do not match with the registered Email or phone number"

      //two users found while sign-up my profile
      var twoUsersFoundWhileSignUp: String = "Another user with this email or Mobile number already registered on Fisheye"

      //you have signed using social login
     // var sociallogintoast:String = "You are registered on Fisheye with your \(signUpMethod) ID. Please try logging in again with the \(signUpMethod) ID"

 //--- LoginViewController ---/
        //registration process incomplete
      var registrationincomplete: String  = "Looks like you started your registration and never completed it. Please complete your registration"

      //your credintial are invalid
      var credintialsinvalid: String = "Your login details are incorrect"

      //user not found
      var usernotfoundcred: String = "Your login details are incorrect. Request you to please check the details provided"

      //please login with registered email or social account
      var pleaseloginwithsocial: String = "Please use your registered login details or registered social account"

      //please enter valid credentilas
      var entervalidcredentials: String = "Please enter the correct login details!"

//--- ResetpasswordnewVc ---//
      //password mistmacth
      var paswordmismatch: String = "Incorrect Password"

      //error while changing password
      var errorpasswordchange: String = "We are experiencing higher than expected traffic, and unable to complete the password change. Please bear with us, and try again in a few minutes"
    
      var updateSecurityAnswerToRecoverAccount: String = "You are required to update your security questions with responses, as you will need this to recover access to your account if you forget password"
     /**************************************  Labels *********************************************/
//--- SignUpMasterKeyVc ---//

      // master key placeholder
      var masterkey: String = "Master Key"

      //Enter master key placeholder
      var entermasterkey: String = "Provide a 4 digit Master key"

      //Inavlid master key placeholder
      var invalidmk: String  = "Incorrect Master Key"

      //Re enter master key placeholder
      var reentermasterkey: String = "Re-enter 4 digit Master key"

      //master key mismatch
      var masterkeymismatch: String = "Incorrect Master Key"

      //enter mobile number
      var entermobilenumber: String = "Enter Mobile Number"

//--- SignupSecurityQuestionVc --//

      //Answer 1
      var answerone: String = "Answer"

      //Question 1
      var questionone: String = "Provide answer"

//--- SignUpMyProfileVC ---//
      //inavalid Reenter password
      var renterpassword: String = "Password mismatch"

      //Enter email
      var enteremail: String  = "Enter email"

      // name
      var name: String = "Name"

      // email
      var email: String = "Email"

      //invalid email
      var invalidemail: String = "Invalid email address"

      // enter country code
      var countrcode: String = "Enter country code "

      //password
      var password: String = "Password"

      //inavalid password
      var invalidpassword: String =  "Invalid password"

      //Re enter password
      var reenterpassword: String = "Re-enter new Password"

      // enter re-enter password
      var enterReEnterPassword: String = "Re-enter new Password"

 //-- ResetPasswordVc --//

      // enter passcode
      var enterpasscode: String = "Enter passcode"

      //passcode
      var passcode: String = "Passcode"

      //new password
      var newpassword: String = "New password"

 //---SendOtpVc---//

      //enter valid phone number
      var entervalidphone: String = "Enter valid phone number"

 //--- SignupVc ---//

      //enter usernmae
      var username: String = "Enter Username"

      //enter valid email id
      var entervalidemailid: String = "Enter valid email id"

      //Enter password
      var enterpassword: String = "Enter new password"

//--- forgetpasswordscreen1vc --//

      //enter your email id
      var enteremailid: String = "Enter email"

      //enter your phone number
      var enterphonenumber: String = "Enter phone number"

      //enter otp
      var enterotp: String = "Enter Security key"

      //enter phonenumber
      var enterphonrnumber: String = "Enter Phone number"

//--- forgetpasswordscreen1vc --//

      //security question failed
      var securiyquestionfailed: String = "Security questions loading failed"

      //incorrect answer
      var incorrectanswer: String = "Incorrect answer"

      //enter phonenumber
      var entrphonenumber: String = "Enter mobile number"

      //enter your country code
       var entercountrycode: String  = "Enter country Code"

      //code
      var code: String  = "Country Code"

      var securityKey: String = "Security Key"
//--- RedirectionController ---//
      
      var pleaseUpdateSecurityQuestion: String = "Please update your security question answers"

      //Login successfully
      var loginsuccess: String = "Login Successful"

      //invalid username placeholder
      var invalidplaceholder: String = "Incorrect Email address"

      //logib usrname
      var loginusernmae: String = "Username"

    // -------Signupwithsocaial-----------//
    //enter phone number
    var Phoneno: String = "Mobile Number"

    //invalid phone number
    var invalidphone: String = "Invalid Mobile number"

    var passwordPolicy: String = "Password should be of length 6 to 15. It must contain atleast 1 Capital,1 Small Alphabets, 1 Numerical e.g Abcdef12"

    var noInternet: String = "Please check your network connectivity and try again later!"

    var signIn: String = "Sign In"
    var orLbl: String = "OR"
    var userProfile: String = "User Profile"
    var letsWorkTogether: String = "Let's work together"
    var selectGender: String = "Select Gender"
    var male: String = "Male"
    var female: String = "Female"
    var transFemale: String = "Trans-Female"
    var biGender: String = "Bi-Gender"
    var nonBinary: String = "Non-Binary"
    var genderNonConforming: String = "Gender nonconforming"
    var undisclosed: String = "Undisclosed"
    var ratherNotSay: String = "Rather not say"
    var fisheyeStatement: String = "Fisheye Statement"
    var privacyPolicy: String  = "Privacy policy"
    var termsOfUse: String = "Terms of Use for Fisheye Ecosystem"
    var cookiePolicyFor: String = "Cookie Policy for Fisheye Ecosystem"
    var disclaimerPolicy: String = "Disclaimer Policy for Fisheye Ecosystem"
    var acceptanceOfTNCText = "By accepting this terms and conditions I agree to the Privacy policy, Terms of Use for Fisheye Ecosystem,  Cookie Policy for Fisheye Ecosystem,  Disclaimer Policy for Fisheye Ecosystem"
    var acceptanceOFTNCHeader: String = "Acceptance of Terms & Conditions"
    var disagree: String  = "Disagree"
    var agree: String = "Agree"

    var otpValidFor: String =  "Security Key valid for"
    var minutes: String = "minutes"
    var enterSixDigitKey: String = "Please enter 6 digit key sent to your registered mobile and Email"
    var resend: String = "RESEND"

    var enterMasterKeyHeader: String = "Enter Master Key"
    var enterFourDigitMasterKey: String = "Enter 4 digit Master key"
    var masterKeyPurpose: String = "It will be used to authenticate your private messages"

    var securityQuestions: String = "Security Questions"
    var securityQuestionOne: String = "Your School Name?"
    var securityQuestionTwo: String = "Your favorite Book?"
    var securityQuestionThree: String = "Memorable City Name?"

    var newUserSignUp: String = "New User? Sign Up"
    var forgotPassword: String = "Forgot Password?"
    var noEmailForSocialAccount: String = "There is no email associated with this social account, try registering with new user sign up"

    var enterValidCredentials: String = "Please enter valid credentials!"

    var user: String = "User"
    var skip: String = "SKIP"
}
