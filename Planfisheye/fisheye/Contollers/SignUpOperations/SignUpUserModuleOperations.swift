//
//  SignUpUserModuleOperations.swift
//  fisheye
//
//  Created by Sankey Solutions on 14/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

// Struct to Add basic Information to setup user for FishEye Platform
struct BasicAddProfile {
      init() {

      }
      var name: String?
      var phonenumber: String?
      var password: String?
      var countryCode: String?
      var signupMethod: String?
      var email: String?
      var privacyEnabled: Bool?
}
// End of creating Struct to Add basic Information to setup user for FishEye Platform

struct sendSMSOperation {
      init() {

      }
      var action = "sendotp"
      var idtoken: String?
      var phonenumber: String?
      var countryCode: String?
      var email: String?
}

struct otpVerificationOperation {
      init() {

      }
      var name: String?
      var phonenumber: String?
      var password: String?
      var countryCode: String?
      var signupMethod: String?
      var email: String?
      var privacyEnabled: Bool?
}
