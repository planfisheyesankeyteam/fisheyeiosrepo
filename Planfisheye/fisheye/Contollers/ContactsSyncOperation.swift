//
//  FrontendSync.swift
//  fisheye
//
//  Created by SankeyProUser on 16/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//
/* Description :- This file is for following purpose
 1) Frontend Syncing : - fetching contacts from iphone & inserting into the ownerContacts table of SQlite database
 & showing that contacts on Sync contact list screen
 2) Backend Sync : - Fetching the non sync contacts from owner contacts table of SQlite database & sending
 it to the backend using Sync API */

import Foundation
import Contacts

class ContactsSync {

      /* ------------------------------------------------ vaiable declaration -------------------------------------------- */

      static let shared: ContactsSync = ContactsSync()
      let appSharedPrefernce = AppSharedPreference()
      var globalNonSyncedContcts: [ContactObj] = []
      var globalBlanckContactFEIdContacts: [ContactObj] = []
      var nonSyncStartIndex: Int = 0
      var nonSyncEndIndex: Int = 100
      var blankContactFEIdStartIndex: Int = 0
      var blankContactFEIdEndIndex: Int = 0
      var store = CNContactStore()
      var idtoken: String = ""
      var userPhoneNumber: String = ""
      var userEmail: String = ""
      var countryCode: String = ""
      var userPhoneWithCountryCode: String = ""
      var currentCountryCode: String  = "+"
      var recentlyAddedContacts: [ContactObj] = []
      var recentlyAddedContactsIdArray: [Int] = []
      var FEContacts: [ContactObj] = []
      var nonFEContacts: [ContactObj] = []
      var isToCheckUpdates: Bool = false
      var isToCheckBlockedByContacts: Bool = false
      var isSyncInProgress: Bool = false
      let syncText = SyncToastMessages()
      var profileObj: [String: Any]?
      var privacyEnabledForContacts: Bool = true

      /* ---------------------------------------------- vaiable declaration End ------------------------------------------ */

      /* --------------------------------------------- FrontEnd Sync Operation -------------------------------------------- */

      /* Description :- Frontend Syncing : - fetching contacts from iphone & inserting into the ownerContacts table of SQlite database
       & showing that contacts on Sync contact list screen*/
      func frontEndSync() {
            self.isSyncInProgress = true
            self.userPhoneNumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            self.userEmail = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
            let userCountryCode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? "" + self.userPhoneNumber
            self.userPhoneWithCountryCode = userCountryCode + self.userPhoneNumber
            self.appSharedPrefernce.setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "yes")
            self.appSharedPrefernce.setAppSharedPreferences(key: "isfronendSyncFullyCompleted", value: "no")
            self.currentCountryCode = getCurrentCountryCode()
            self.profileObj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
            if let tempObj = self.profileObj {
                  self.privacyEnabledForContacts = tempObj["privacyEnabled"] as? Bool ?? true
            }
            DispatchQueue.global().async {
                        let array = self.fetchAllPhoneContact()
                        let cnt = array.count
                        for i in 0..<cnt {
                              self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                              if(self.idtoken == "") {
                                    break
                              }
                              var contactObj = ContactObj()
                              contactObj.sharedData = 2
                              /* Extracting the name */
                              var fullName = ""
                              fullName = "\(array[i].givenName) \(array[i].familyName)"
                              contactObj.name = fullName
                              if array[i].imageData != nil {

                                    let imageData = UIImage(data: array[i].imageData!)!.jpegData(compressionQuality:0.6)
                                var err: Error?
                                    var paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
                                    //find the cache dir. You might want to consider using the doc dir instead
                                    var path: String = paths[0]
                                    path = URL(fileURLWithPath: path).appendingPathComponent(contactObj.name).absoluteString
                                    do {
                                        try? imageData?.write(to: URL(string: path) ?? "", options: .atomic)
                                          if err == nil {
                                                contactObj.picture = path
                                          } else {
                                            
                                          }
                                    } catch {
                                          
                                    }
                                    
                              } else {
                                    contactObj.picture = ""
                              }
                              
                              /* Extracting the  Primary phoneNo. */
                              var phoneNo = "n/a"
                              if !array[i].phoneNumbers.isEmpty {
                                    for j in 0..<array[i].phoneNumbers.count {
                                          
                                          let phoneString = ((((array[i].phoneNumbers[j] as AnyObject).value(forKey: "labelValuePair") as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "stringValue")) ?? ""
                                          phoneNo = phoneString as! String
                                          phoneNo = phoneNo.removingWhitespaces()
                                          phoneNo = phoneNo.replacingOccurrences(of: "-", with: "")
                                          phoneNo = phoneNo.replacingOccurrences(of: "(", with: "")
                                          phoneNo = phoneNo.replacingOccurrences(of: ")", with: "")
                                          
                                          if(phoneNo.first == "0") {
                                                phoneNo = String(phoneNo.dropFirst(1))
                                                phoneNo = self.currentCountryCode + phoneNo
                                          }
                                          if j == 0 {
                                                contactObj.phoneNumber = phoneNo
                                          } else {
//                                                phoneNo = phoneNo.removingWhitespaces()
//                                                phoneNo = phoneNo.replacingOccurrences(of: "-", with: "")
                                                if (!contactObj.secondaryPhoneNumbers.contains(phoneNo) && contactObj.phoneNumber != phoneNo){
                                                      contactObj.secondaryPhoneNumbers.append(phoneNo)
                                                }
                                          }
                                    }
                              }

                              contactObj.stringSecondaryPhones = contactObj.secondaryPhoneNumbers.map { String($0) }
                                    .joined(separator: ", ")
                              contactObj.stringSecondaryPhones = contactObj.stringSecondaryPhones.replacingOccurrences(of: "[", with: "")
                              contactObj.stringSecondaryPhones = contactObj.stringSecondaryPhones.replacingOccurrences(of: "]", with: "")
                              if(contactObj.phoneNumber == nil || contactObj.phoneNumber == "" || phoneNo == "") {
                                    contactObj.phoneNumber = ""
                                    contactObj.sharedData = 0
                              } else if(contactObj.phoneNumber.count < 8 ) {
                                    continue
                              }

                              /* Extracting the  Primary Email Id. */
                              var email = "n/a"
                              if !array[i].emailAddresses.isEmpty {
                                    for j in 0..<array[i].emailAddresses.count {
                                        
                                          let emailString = (((array[i].emailAddresses[j] as AnyObject).value(forKey: "labelValuePair") as AnyObject).value(forKey: "value")) ?? ""
                                          email = emailString as! String
                                          if j == 0 {
                                                contactObj.email = email
                                          } else {
                                                contactObj.secondaryEmails.append(email)
                                          }
                                    }
                              }

                              if(email == "" || email == "n/a" || email == nil ) {
                                    contactObj.email = ""
                                    contactObj.sharedData = 1
                              }

                              contactObj.stringSecondaryEmails = contactObj.secondaryEmails.map { String($0) }
                                    .joined(separator: ", ")
                              contactObj.stringSecondaryEmails = contactObj.stringSecondaryEmails.replacingOccurrences(of: "[", with: "")
                              contactObj.stringSecondaryEmails = contactObj.stringSecondaryEmails.replacingOccurrences(of: "]", with: "")

                              if(contactObj.email == nil ) {
                                    contactObj.email = ""
                              }

                              /* Extracting Addresses */
                              if !array[i].postalAddresses.isEmpty {
                                    for k in 0..<array[i].postalAddresses.count {
                                          if(k == 2) {
                                                break
                                          }

                                          let street_address = ((array[i].postalAddresses[k] as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "street") as! String
                                          let region = ((array[i].postalAddresses[k] as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "state") as! String
                                          let locality = ((array[i].postalAddresses[k] as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "city") as! String
                                          let string1 = ((array[i].postalAddresses[k] as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "subLocality") as! String
                                          let string2 = ((array[i].postalAddresses[k] as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "subAdministrativeArea") as! String
                                          let formatted
                                                = string1 + string2
                                          let country = ((array[i].postalAddresses[k] as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "country") as! String
                                          let postal_code
                                                = ((array[i].postalAddresses[k] as AnyObject).value(forKey: "value") as AnyObject).value(forKey: "postalCode") as! String

                                          var addressString = formatted + "," + street_address + ","
                                          addressString = addressString + locality + "," + region + "," + country + "," + postal_code

                                          if(k == 0) {
                                                contactObj.addressString1 = "Home" + "," + addressString
                                          } else if (k == 1) {
                                                contactObj.addressString2 = "Work" + "," + addressString
                                          }

                                    }

                              } else {
                                    contactObj.addressString1 = ""
                                    contactObj.addressString2 = ""
                              }

                              /* Making complete contact obj */
                              contactObj.contactId = getTime()
                              if(self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken")  == nil) {
                                    break
                              } else {
                                    contactObj.userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as! String
                              }
                              // contactObj.primaryCountryCode = ""
                              contactObj.isdeleted = false
                              contactObj.ismerged = false
                              contactObj.companyEmail = ""
                              contactObj.title = ""
                              contactObj.company = ""
                              contactObj.source = "fromPhone"
                              contactObj.website = ""
                              contactObj.notes = ""
                              contactObj.privacyEnabled = true
                              contactObj.privacySetForContactUser = self.privacyEnabledForContacts
                              contactObj.contactFEId = ""
                              contactObj.isBackendSync = false
                              contactObj.mergeParentId = ""
                              contactObj.isBlocked = false
                              contactObj.createdAt = getCurrentTImeStamp()
                              contactObj.updatedAt = getCurrentTImeStamp()

                              if(contactObj.phoneNumber == self.userPhoneNumber || contactObj.phoneNumber == self.userPhoneWithCountryCode || contactObj.email == self.userEmail) {
                                    continue
                              }

                              let tempPhone = contactObj.phoneNumber
                              let tempEmail = contactObj.email

                              /* Check contact is already present in SQLite ownerContacts table or not */
                              var existingContacts: [ContactObj] = self.getAlreadyExistsContactsFromAllContacts(tempPhoneNo: tempPhone as! String, tempEmail: tempEmail as! String)
                              /* Contact doesn't exists in SQLite ownerContacts Table */
                              if(existingContacts.isEmpty && existingContacts == nil || existingContacts.count == 0) {
                                    if(!contactObj.name.isEmpty && (!(contactObj.phoneNumber == "") || !(contactObj.email == ""))) {
                                          DatabaseManagement.shared.insertContactsInSQLiteStorage(contactObj: contactObj)
                                    }

                              } else {                 /* Contact already exists in SQLite ownerContacts Table, now merge both contacts */
                                    var contactToUpdate = ContactObj()
                                    contactToUpdate = self.mergeContacts(contactExisting: existingContacts[0], contactObjWillMerge: contactObj)
                                    if(contactToUpdate.sharedData == 0 &&  contactToUpdate.phoneNumber != existingContacts[0].phoneNumber) {
                                          contactToUpdate.sharedData = 2
                                    } else if(contactToUpdate.sharedData == 1 && contactToUpdate.email != existingContacts[0].email) {
                                          contactToUpdate.sharedData = 2
                                    }

                                    if !contactToUpdate.isBackendSync {
                                          /* Update existing contact in SQLite ownerContacts Table */
                                          contactToUpdate.updatedAt = getCurrentTImeStamp()
                                          DatabaseManagement.shared.updateAlreadyExistingContact(contacToUpdate: contactToUpdate)
                                    }
                                    /* End */
                              }
                              if((i % 100) == 0) {
                                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                              }

                              if(i == cnt - 1) {
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "isfronendSyncFullyCompleted", value: "yes")
                                    let toastObj: [String: String] = ["toastMessage": self.syncText.frontendSyncCompleted, "isSyncCompleted": "No"]
                                    NotificationCenter.default.post(name: SHOW_TOAST_MESSAGE, object: nil, userInfo: toastObj)

                              }
                        }

                        self.appSharedPrefernce.setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "no")

                        if self.isToCheckBlockedByContacts {
                              self.getBlockedByContactsFromBackend()
                        }

                        /* Backend Sync */
                        if(NetworkStatus.sharedManager.isNetworkReachable()) {
                              self.appSharedPrefernce.setAppSharedPreferences(key: "isBackendSyncInProgress", value: "yes")
                              ContactsSync.shared.getNonSyncedContacts()
                              ContactsSync.shared.backendSync()
                        }
            }
      }

      /* -------------------- Fetch Contacts from iphone ----------------------- */

      /* Created by vaishali
       Description - To fetch the contacts from iphone */
      func fetchAllPhoneContact() -> [CNContact] {
            var contactss: [CNContact] = []

            let contactStore = CNContactStore()
            let fetchReq = CNContactFetchRequest.init(keysToFetch: [CNContactVCardSerialization.descriptorForRequiredKeys(), CNContactImageDataKey as CNKeyDescriptor])

            do {
                  try contactStore.enumerateContacts(with: fetchReq) { (contact, _) in
                        contactss.append(contact)
                  }
            } catch {

            }

            return contactss
      }

      /* Created by vaishali */
      func getAlreadyExistsContactsFromAllContacts(tempPhoneNo: String, tempEmail: String) -> [ContactObj] {
            let existingContacts: [ContactObj] = DatabaseManagement.shared.getAlreadyExistsContactsFromAllContacts(tempPhoneNumber: tempPhoneNo, tempEmail: tempEmail)
            return existingContacts
      }
      /* END */

      /* ---------------------- Fetch contacts from iphone End --------------------- */

      /* -------------------------- Merge Contacts  ------------------------------- */

      func mergeContacts(contactExisting: ContactObj, contactObjWillMerge: ContactObj) -> ContactObj {
            var contactExisting = contactExisting
            let tempPhone = contactObjWillMerge.phoneNumber
            let tempEmail = contactObjWillMerge.email

            /* merge phone nos */
            if ((contactExisting.phoneNumber.isEmpty)) {
                  if(!(tempPhone?.isEmpty)!) {
                        contactExisting.phoneNumber = tempPhone as! String
                        contactExisting.isBackendSync = false
                  }
            } else if(!(tempPhone?.isEmpty)! && (!(tempPhone == contactExisting.phoneNumber) && !contactExisting.secondaryPhoneNumbers.contains(tempPhone!))) {
                  contactExisting.secondaryPhoneNumbers.append(tempPhone!)
                  contactExisting.isBackendSync = false
            }

            // merge secondary phone numbers
            if contactObjWillMerge.secondaryPhoneNumbers.count > 0 {
                  for phoneIndex in 0..<Int(contactObjWillMerge.secondaryPhoneNumbers.count) {

                        if(contactExisting.secondaryPhoneNumbers.count == 2) {
                              break
                        } else if(!(contactExisting.phoneNumber == contactObjWillMerge.secondaryPhoneNumbers[phoneIndex]) && !contactExisting.secondaryPhoneNumbers.contains(contactObjWillMerge.secondaryPhoneNumbers[phoneIndex])) {
                              contactExisting.secondaryPhoneNumbers.append(contactObjWillMerge.secondaryPhoneNumbers[phoneIndex])
                              contactExisting.isBackendSync = false
                        }
                  }
            }

            contactExisting.stringSecondaryPhones = contactExisting.secondaryPhoneNumbers.map { String($0) }
                  .joined(separator: ", ")
            contactExisting.stringSecondaryPhones = contactExisting.stringSecondaryPhones.replacingOccurrences(of: "[", with: "")
            contactExisting.stringSecondaryPhones = contactExisting.stringSecondaryPhones.replacingOccurrences(of: "]", with: "")
            /* End */

            /* merge Email Ids */
            if ((contactExisting.email.isEmpty) || contactExisting.email.characters.count == 0) {
                  if(!(tempEmail?.isEmpty)!) {
                    contactExisting.email = tempEmail ?? ""
                        contactExisting.isBackendSync = false
                  }
            } else if(!(tempEmail?.isEmpty)! && (!(tempEmail == contactExisting.email) && !contactExisting.secondaryEmails.contains(tempEmail!))) {
                  contactExisting.secondaryEmails.append(tempEmail!)
                  contactExisting.isBackendSync = false
            }

            /* merge secondary email Ids */
            if contactObjWillMerge.secondaryEmails.count > 0 {
                  for emailIndex in 0..<Int(contactObjWillMerge.secondaryEmails.count) {
                        if(contactExisting.secondaryEmails.count == 2) {
                              break
                        } else if(!(contactExisting.email == contactObjWillMerge.secondaryEmails[emailIndex]) && !contactExisting.secondaryEmails.contains(contactObjWillMerge.secondaryEmails[emailIndex])) {
                              contactExisting.secondaryEmails.append(contactObjWillMerge.secondaryEmails[emailIndex])
                              contactExisting.isBackendSync = false
                        }
                  }
            }

            contactExisting.stringSecondaryEmails = contactExisting.secondaryEmails.map { String($0) }
                  .joined(separator: ", ")
            contactExisting.stringSecondaryEmails = contactExisting.stringSecondaryEmails.replacingOccurrences(of: "[", with: "")
            contactExisting.stringSecondaryEmails = contactExisting.stringSecondaryEmails.replacingOccurrences(of: "]", with: "")
            //        contactExisting.isBackendSync = false

            if contactExisting.picture != contactObjWillMerge.picture {
                  contactExisting.picture = contactObjWillMerge.picture
                  contactExisting.isBackendSync = false
            }
            return contactExisting
      }

      /* ------------------------- Merge Contacts End ----------------------------- */

      /* ------------------------------------------------ FrontENd Sync operation ENd -------------------------------------------- */

      /* ------------------------------------------- Backend Sync  -------------------------------------------------------*/

      /* Description :- get all non sync contacts from ownerContacts table of SQLite database */
      func getNonSyncedContacts() {

            self.globalNonSyncedContcts = DatabaseManagement.shared.getNonSyncedContacts()
            self.nonSyncStartIndex = 0
            self.nonSyncEndIndex = 100
            if(self.globalNonSyncedContcts.count < 100) {
                  self.nonSyncEndIndex = self.globalNonSyncedContcts.count
            }
      }

      /* Description :- get one batch of contacts to send it in SYnc API */
      func getPartOfNonSyncContacts(startIndex: Int, endIndex: Int) -> [ContactObj] {
            var partOfNonSyncContacts: [ContactObj] = []
            if(!(startIndex > endIndex)) {
                  for i in startIndex..<endIndex {
                        partOfNonSyncContacts.append(self.globalNonSyncedContcts[i])
                  }
            }
            return partOfNonSyncContacts
      }

      /* Description - To sync contacts to Backend Sync Functionality */
      func backendSync() {
            var nonSyncedContcts: [ContactObj] = []
            nonSyncedContcts =  getPartOfNonSyncContacts(startIndex: self.nonSyncStartIndex, endIndex: self.nonSyncEndIndex)
            if( nonSyncedContcts.count > 0  && nonSyncedContcts != nil) {

                  let syncOperation = SyncContactsAPIOperation()
                  syncOperation.contactObjs = nonSyncedContcts
                  syncOperation.addDidFinishBlockObserver { [unowned self] (_, _) in

                        // Backend sync operation will continue in the batch of hundreads till all the contacts are not sync to backend
                        self.nonSyncStartIndex = self.nonSyncStartIndex + 100
                        if(self.nonSyncStartIndex < self.globalNonSyncedContcts.count && self.globalNonSyncedContcts.count - self.nonSyncStartIndex >= 100) {
                              self.nonSyncEndIndex = self.nonSyncEndIndex + 100
                              self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                              if(self.idtoken != "") {
                                    self.backendSync()
                              }
                        } else if(self.nonSyncStartIndex < self.globalNonSyncedContcts.count && self.globalNonSyncedContcts.count - self.nonSyncStartIndex < 100 && self.globalNonSyncedContcts.count - self.nonSyncEndIndex != 0) {
                              self.nonSyncEndIndex = self.nonSyncEndIndex + self.globalNonSyncedContcts.count - self.nonSyncStartIndex
                              self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                              if(self.idtoken != "") {
                                    self.backendSync()
                              }
                        } else {
                              self.appSharedPrefernce.setAppSharedPreferences(key: "isBackendSyncInProgress", value: "No")
                              let toastObj: [String: String] = ["toastMessage": self.syncText.syncCompleted, "isSyncCompleted": "Yes"]
                              NotificationCenter.default.post(name: SHOW_TOAST_MESSAGE, object: nil, userInfo: toastObj)
                              if self.isToCheckUpdates {
                                    self.getUpdatedContactsFromBackend()
                              }
                              self.nonSyncStartIndex = 0
                              self.nonSyncEndIndex = 100
                              self.isSyncInProgress = false
                        }
                  }
                  AppDelegate.addProcedure(operation: syncOperation)
            } else {
                  self.nonSyncStartIndex = 0
                  self.nonSyncEndIndex = 100
                  let toastObj: [String: String] = ["toastMessage": self.syncText.syncCompleted, "isSyncCompleted": "Yes"]
                  NotificationCenter.default.post(name: SHOW_TOAST_MESSAGE, object: nil, userInfo: toastObj)
                  self.appSharedPrefernce.setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "no")
                  self.isSyncInProgress = false
                  if self.isToCheckUpdates {
                        self.getUpdatedContactsFromBackend()
                  }
            }
            self.appSharedPrefernce.setAppSharedPreferences(key: "isBackendSyncInProgress", value: "yes")
      }
      /* END */

      /* ------------------------------------------- Backend Sync END ------------------------------------------------------*/

      /* --------------------------------------------- Update contactFEIds END -------------------------------------------*/

      /* ----------------------------- Check contacts updated due to other FEUser's modification --------------------------- */

      func getUpdatedContactsFromBackend() {
            let getUpdatedContacts = getUpdatedContactsAPIOperation()
            getUpdatedContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
            }
            AppDelegate.addProcedure(operation: getUpdatedContacts)
      }

      /*  ----------------------- Check contacts updated due to other FEUser's modification END --------------------------- */

      /* ---------------------------------- Check contacts that have blocked to FEUser  ------------------------------------- */

      func getBlockedByContactsFromBackend() {
            let getBlockedByContacts = getBlockedByContactsAPIOperation()
            getBlockedByContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
            }
            AppDelegate.addProcedure(operation: getBlockedByContacts)
      }

      /* ---------------------------------- Check contacts that have blocked to FEUser END ------------------------------------- */

      func getContactsPermissionStatus() -> CNAuthorizationStatus {
              return CNContactStore.authorizationStatus(for: .contacts)
      }
}
