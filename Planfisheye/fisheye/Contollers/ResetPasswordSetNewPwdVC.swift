//
//  ResetPasswordSetNewPwdVC.swift
//  fisheye
//
//  Created by Sylveon on 12/11/17.
//  Modified by Anagha Meshram on 02/02/2018
//  Changed the text color for headerForSubView
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class ResetPasswordSetNewPwdVC: UIViewController, UITextFieldDelegate {

      var emailId = ""
      var mobile = ""
      var ccode = ""
      var otp = ""
      var idtoken = ""
      var mobileNumber1 = ""
      var countryCod = ""
      var userName = ""
    //enter password
      var placeholder = ""
    //reenter password
      var placeholder2 = ""
    //invalid password
       var placeholder3 = ""
      var pwdVieww: Bool!
      var code: String!
      let appSharedPrefernce = AppSharedPreference()
      let validationController = ValidationController()
      var showDisabledOTPView: ShowDisabledOTPView?

      var iconClick: Bool!
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      var forgotPwdAllText = ForgotPasswordTexts.shared
      var appService = AppService.shared

      @IBOutlet weak var resetPasswordSubView: UIView!
      @IBOutlet var resetPwdMainView: UIView!
      @IBOutlet weak var resetPwdHeadingLabel: UILabel!
      @IBOutlet weak var fisheyeLabel: UIImageView!
      @IBOutlet weak var headerForSubView: UILabel!
      @IBOutlet weak var userTextField: UICustomTextField!

      @IBOutlet weak var reTypePasswordBlueUnderline: UIImageView!
      @IBOutlet weak var fisheyeMotto: UILabel!
      @IBOutlet weak var reTypePasswordBlackUnderline: UIImageView!
      @IBOutlet weak var passwordBlueUnderline: UIImageView!
      @IBOutlet weak var passwordBlackunderline: UIImageView!

      @IBAction func PwdHideShowTapped(_ sender: Any) {
            if(pwdVieww == true) {
                  self.pwdPolicyView.isHidden = false
                  pwdVieww = false
            } else {
                  self.pwdPolicyView.isHidden = true
                  pwdVieww = true
            }
      }
      @IBAction func eyeButtonTapped(_ sender: Any) {

            if(iconClick == true) {
                  self.passwordTextField.isSecureTextEntry = false
                  iconClick = false
            } else {
                  self.passwordTextField.isSecureTextEntry = true
                  iconClick = true
            }

      }

      @IBOutlet weak var backButton: UIButton!
      @IBAction func backButtonTapped(_ sender: Any) {
            self.backPageCalled()
      }
      @IBOutlet weak var backButtonTapped: UIButton!
      @IBOutlet weak var policyDetailsView: UITextView!
      @IBAction func submitButtonTapped(_ sender: Any) {
        DispatchQueue.main.async{
            self.startLoader()
            self.submitButton.isUserInteractionEnabled=false
            self.passwordTextField.isUserInteractionEnabled=false
            self.retypeTextField.isUserInteractionEnabled=false
            self.userTextField.isUserInteractionEnabled=false
        }
        resetPasswordFunctionCalled(newPassword: passwordTextField.text!, reEnterpassword: retypeTextField.text!)
      }
      @IBAction func policyBtnTapped(_ sender: Any) {
            self.policyDetailsView.isHidden = !self.policyDetailsView.isHidden
      }
      @IBOutlet weak var submitButton: UIButton!
      @IBOutlet weak var eyeButton: UIImageView!
      @IBOutlet weak var retypeTextField: UICustomTextField!
      @IBOutlet weak var passwordTextField: UICustomTextField!

    func resetPasswordFunctionCalled(newPassword: String, reEnterpassword: String) {
            let newPwd = newPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let reenternewPwd = reEnterpassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            var isValidationSuccessful: Bool = true
            if (passwordTextField.text?.characters.count)! <= 0 {
                  self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.enterpassword)
                 placeholder = passwordTextField.placeholder!

                  isValidationSuccessful = false
            } else {
                  if validationController.isValidPassword(testStr: newPwd) {
                        self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)

                  } else {
                        self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.invalidpassword)
                            placeholder3 = passwordTextField.placeholder!

                    self.view.makeToast(self.signupmsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium,position:.bottom)
                        isValidationSuccessful = false
                  }
            }

            if (reenternewPwd.characters.count) <= 0 {
                  self.retypeTextField.configOnError(withPlaceHolder: self.signupmsg.enterReEnterPassword)
                placeholder2 = retypeTextField.placeholder!

                  isValidationSuccessful = false
            } else {
                  if (newPwd != reenternewPwd) {
                        self.retypeTextField.configOnError(withPlaceHolder: self.signupmsg.paswordmismatch)
                          placeholder = retypeTextField.placeholder!
                        isValidationSuccessful = false
                  }
            }

            if isValidationSuccessful {
                  let parameters =
                        [
                              "action": "resetpassword",
                              "idtoken": idtoken,
                              "newPassword": newPwd
                  ] as [String: Any]

                  let awsURL = AppConfig.resetPasswordAPI

                guard let URL = URL(string: awsURL) else { return }
                let sessionConfig = URLSessionConfiguration.default
                let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
                var request = URLRequest(url: URL)
                request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
                request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
                request.httpMethod = AppConfig.httpMethodPost
                guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                    return
                }
                request.httpBody = httpBody

                guard let signedRequest = URLRequestSigner().sign(request: request) else {
                    return
                }

                let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                    if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
//                        self.stopLoader()
                        let statusCode = data["statusCode"] as? String ?? ""
                        if(statusCode == "0") {
                            DispatchQueue.main.async {
                                self.view.makeToast(self.profilemsg.paswordchangesuccess, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                            }
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                                self.view.hideToast()
                                self.stopLoader()
                                self.submitButton.isUserInteractionEnabled=true
                                self.passwordTextField.isUserInteractionEnabled=true
                                self.retypeTextField.isUserInteractionEnabled=true
                                self.userTextField.isUserInteractionEnabled=true
                                self.loginPageCalled()
                            }
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Set new password", label: "Password reset successfully", value: 0)
                        } else {
                            DispatchQueue.main.async {
                            self.view.makeToast(self.signupmsg.errorpasswordchange, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                            }; GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Set new password", label: "Failed to change password", value: 0)
                            DispatchQueue.main.async{
                                self.stopLoader()
                                self.submitButton.isUserInteractionEnabled=true
                                self.passwordTextField.isUserInteractionEnabled=true
                                self.retypeTextField.isUserInteractionEnabled=true
                                self.userTextField.isUserInteractionEnabled=true
                            }
                        }

                    } else {
                        DispatchQueue.main.async{
                            self.stopLoader()
                            self.submitButton.isUserInteractionEnabled=true
                        }
                        DispatchQueue.main.async {
                            self.view.makeToast(self.signupmsg.errorpasswordchange, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                            self.view.hideToast()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Set new password", label: "Failed to reset password", value: 0)

                    }
                })
                task.resume()
                session.finishTasksAndInvalidate()
            }else{
                DispatchQueue.main.async{
                    self.stopLoader()
                    self.submitButton.isUserInteractionEnabled=true
                    self.passwordTextField.isUserInteractionEnabled=true
                    self.retypeTextField.isUserInteractionEnabled=true
                    self.userTextField.isUserInteractionEnabled=true
                }
            }
      }

      func backPageCalled() {
            self.showDisabledOTPView?.toDisableOTPView()
            self.dismiss(animated: true, completion: nil)
      }

      func loginPageCalled() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextVC, animated: true, completion: nil)
      }

      func startLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.stopLoader()
        }
      }

      //Code to put validation on all textfields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case passwordTextField:
                  self.passwordBlackunderline.isHidden = true
                  self.passwordBlueUnderline.isHidden = false

                  if (passwordTextField.text?.characters.count)! <= 0 {
                        self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.enterpassword)
                  } else {
                        self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
                  }

            case retypeTextField:
                  self.reTypePasswordBlackUnderline.isHidden = true
                  self.reTypePasswordBlueUnderline.isHidden = false
                  if (retypeTextField.text?.characters.count)! <= 0 {
                        self.retypeTextField.configOnError(withPlaceHolder: self.signupmsg.enterReEnterPassword)
                  } else {
                        self.retypeTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.enterReEnterPassword)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case passwordTextField:
                  self.passwordBlackunderline.isHidden = false
                  self.passwordBlueUnderline.isHidden = true
                  let passwordText: String = textField.text!
                  if (passwordTextField.text?.characters.count)! <= 0 {
                        self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.enterpassword)
                  } else {
                        if validationController.isValidPassword(testStr: passwordText) {
                              self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
                        } else {
                              self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.invalidpassword)
                            placeholder3 = passwordTextField.placeholder!
                            self.view.makeToast(self.signupmsg.passwordPolicy, duration: SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium, position: .bottom)
                        }
                  }
            case retypeTextField:
                  self.reTypePasswordBlackUnderline.isHidden = false
                  self.reTypePasswordBlueUnderline.isHidden = true
                  if (retypeTextField.text?.characters.count)! <= 0 {
                        self.retypeTextField.configOnError(withPlaceHolder: self.signupmsg.enterReEnterPassword)
                  } else {
                        let password = passwordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        let reenterPassword = retypeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        if (password != reenterPassword) {
                              self.retypeTextField.configOnError(withPlaceHolder: self.signupmsg.paswordmismatch)
                        }
                  }

            default:
                  break
            }
      }

      @IBOutlet weak var pwdPolicyView: UITextView!
      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.profilemsg  = ProfileToastMsgHeadingSubheadingLabels.shared
            self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
            self.forgotPwdAllText  = ForgotPasswordTexts.shared
            self.appService = AppService.shared
            iconClick = true
            self.pwdVieww = true
            self.passwordTextField.isSecureTextEntry = true
            self.retypeTextField.isSecureTextEntry = true
            resetPasswordSubView.layer.cornerRadius = 6
            self.passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.retypeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.passwordTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.retypeTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.userTextField.text = self.userName
            self.userTextField.isUserInteractionEnabled = false

            self.resetPasswordSubView.layer.cornerRadius = 6
            self.resetPasswordSubView.layer.cornerRadius = 6
            self.resetPasswordSubView.layer.shadowColor = UIColor.black.cgColor
            self.resetPasswordSubView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.resetPasswordSubView.layer.shadowOpacity = 0.4
            self.resetPasswordSubView.layer.shadowRadius = 6.0
            self.resetPasswordSubView.layer.masksToBounds = false

            self.passwordBlackunderline.isHidden = false
            self.passwordBlueUnderline.isHidden = true

            self.reTypePasswordBlackUnderline.isHidden = false
            self.reTypePasswordBlueUnderline.isHidden = true
            self.policyDetailsView.isHidden = true

            self.policyDetailsView.isUserInteractionEnabled = false
            // Do any additional setup after loading the view.
            self.setLocalizationText()
            self.addObservers()

      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ResetPasswordSetNewPwdVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      // add all observers to this viewController here
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      // End

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.resetPwdHeadingLabel.text = self.forgotPwdAllText.resetPasswordHeader
                  self.headerForSubView.text = self.forgotPwdAllText.enterNewPasswordHeader
                  self.fisheyeMotto.text = self.forgotPwdAllText.letsWorkTogether
                  self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
                  
                  self.userTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.user)
                  self.retypeTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.reenterpassword)
                  self.policyDetailsView.text = self.signupmsg.passwordPolicy
            }
      }
      /* END */
}
