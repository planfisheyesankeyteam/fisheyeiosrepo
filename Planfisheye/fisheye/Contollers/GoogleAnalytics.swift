//
//  GoogleAnalytics.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 27/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

class GoogleAnalytics {

      static let shared: GoogleAnalytics = GoogleAnalytics()
      var userId: String = ""
      var appSharedPrefernce = AppSharedPreference()
      let tracker = GAI.sharedInstance().defaultTracker

      /* --------------- Track Screen Google Analytics --------------- */
      func trackScreenOnGoogleAnalytics(screenName: String) {
            self.userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
            tracker?.set(kGAIScreenName, value: screenName)
            tracker?.set(GAIFields.customDimension(for: 1), value: self.userId as String!)
            tracker?.set(kGAIClientId, value: self.userId as String!)

            guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
            tracker?.send(builder.build() as [NSObject: AnyObject])
      }
      /* --------------- Track Screen Google Analytics  End--------------- */

      /* --------------- Track Event on Google Analytics --------------- */
      func trackEventOnGoogleAnalytics (category: String, action: String, label: String, value: NSNumber) {
            self.userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
            guard let tracker = GAI.sharedInstance().defaultTracker else { return }
            tracker.set(GAIFields.customDimension(for: 1), value: self.userId as String!)
            tracker.set(kGAIClientId, value: self.userId as? String ?? "")

            let event =  GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: value).build()
            tracker.send(event as! [NSObject: AnyObject])
      }
      /* --------------- Track Screen on Google Analytics  End --------------- */

}
