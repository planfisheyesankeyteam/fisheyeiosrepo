//
//  ResetPasswordVC.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 25/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {

//    @IBOutlet var radialView: RadialGradientView!

    @IBOutlet weak var reEnterPassTextField: UICustomUnderLineTextField!

    @IBOutlet weak var newPasswordTextField: UICustomUnderLineTextField!

    @IBOutlet weak var passcodeTextFiled: UICustomUnderLineTextField!

    @IBOutlet weak var fisheyeLbl: UILabel!

    @IBOutlet weak var changePasswordBtn: UIButton!

    @IBOutlet weak var reserPasssTitle: UILabel!

    @IBOutlet weak var descLabel: UILabel!

    var iconClick: Bool!

     var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
     var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared

    @IBOutlet weak var visiblePassImage: UIImageView!

    override func viewDidLoad() {

        super.viewDidLoad()

        // Do any additional setup after loading the view.

        let viewTapGesture = UITapGestureRecognizer(target: self, action: #selector(endEditing))

//        self.radialView.isUserInteractionEnabled = true
//
//        self.radialView.addGestureRecognizer(viewTapGesture)

        iconClick = true

        self.visiblePassImage.isUserInteractionEnabled = true

//        radialView.colors = [UIColor.white, UIColor.duckEggBlue()]

        self.fisheyeLbl.textColor = UIColor.purplyBlue10()

        self.reserPasssTitle.textColor = UIColor.frenchBlue()

        self.descLabel.textColor = UIColor.black60Two()

        self.changePasswordBtn.backgroundColor = UIColor.frenchBlue()

        self.changePasswordBtn.layer.cornerRadius = 0.09467391304*self.changePasswordBtn.bounds.width

        self.changePasswordBtn.setTitleColor(UIColor.white, for: .normal)

    }

    override func viewWillAppear(_ animated: Bool) {
        kApplicationDelegate.setStatusBarBackgroundColor(color: UIColor.clear)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func changePasswordTapped(_ sender: Any) {

        self.view.endEditing(true)

        if  (self.passcodeTextFiled.text?.characters.count)! <= 0 {

            self.passcodeTextFiled.configOnError(withPlaceHolder: self.signupmsg.enterpasscode)
        } else if  (self.newPasswordTextField.text?.characters.count)! <= 0 {

            self.newPasswordTextField.configOnError(withPlaceHolder: self.profilemsg.enternewpwd)
        } else if  (self.reEnterPassTextField.text?.characters.count)! <= 0 {

            self.reEnterPassTextField.configOnError(withPlaceHolder: self.signupmsg.reenterpassword)
        } else if (self.newPasswordTextField.text != self.reEnterPassTextField.text) {

            kSweetAlert.showAlert("Password mismatch!", subTitle: "", style: AlertStyle.error)
        } else {

        }

    }

    @IBAction func backBtnTapped(_ sender: Any) {
        self.popViewController()
    }

    // MARK: Textfield Delegates

    func textFieldDidBeginEditing(_ textField: UITextField) {

        switch textField {
        case newPasswordTextField:
            self.newPasswordTextField.configOnFirstResponder(withPlaceHolder: self.signupmsg.newpassword)
        case passcodeTextFiled:
            self.passcodeTextFiled.configOnFirstResponder(withPlaceHolder: self.signupmsg.passcode)
        case reEnterPassTextField:
            self.reEnterPassTextField.configOnFirstResponder(withPlaceHolder: self.signupmsg.reenterpassword)
        default:
            break
        }

    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        if textField.isKind(of: UICustomUnderLineTextField.self) {
            let txtField = textField as! UICustomUnderLineTextField
            txtField.configOnResignResponder()
        }

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        let tag = textField.tag + 1
        if let nextTextField = self.view.viewWithTag(tag) {
            if nextTextField.isKind(of: UICustomUnderLineTextField.self) {
                nextTextField.becomeFirstResponder()
            }
        }

        return true
    }

    // MARK: Private Methods

    func showPassword() {

        if(iconClick == true) {
            self.newPasswordTextField.isSecureTextEntry = false
            iconClick = false
        } else {
            self.newPasswordTextField.isSecureTextEntry = true
            iconClick = true
        }

    }

    @objc func endEditing() {
        self.view.endEditing(true)
    }

}
