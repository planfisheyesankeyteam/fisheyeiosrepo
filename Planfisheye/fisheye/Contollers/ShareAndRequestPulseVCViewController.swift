//
//  ShareAndRequestPulseVCViewController.swift
//  fisheye
//
//  Created by Anuradha on 12/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

struct ContactData {
      var contactId: String!
      var contactName: String!
      var contactEmailId: String!
      var contactPhoneNumber: String!
      var contactImage: String!
      var contactFEId: String!
      var privacyEnabled: Bool!
      var sharedData: Int!
      var isBlocked: Bool!
}

class ShareAndRequestPulseVCViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, SelectedContactTableViewCellDelegate {

      /********************************************* Outlet variables *****************************************/

      @IBOutlet weak var addContactLabel: UILabel!
      @IBOutlet weak var pulseLabelUV: UILabel!
      @IBOutlet weak var upperView: UIView!
      @IBOutlet weak var underlineOfUV: UIView!
      //selected contact table view
      @IBOutlet weak var selectedContactTableView: UITableView!
      //stack view request share
      @IBOutlet weak var stackViewShareRequest: UIStackView!
      @IBOutlet weak var requestButton: UIButton!
      @IBOutlet weak var shareButton: UIButton!
      //search Contact View with add and search btn,underline
      @IBOutlet weak var searchContactToAddView: UIView!
      //@IBOutlet weak var addContactTextField: UITextField!
      @IBOutlet weak var baseView: UIView!
      @IBOutlet weak var ViewWithShadow: UIView!
      //searchbar overlapping to search contact view
      @IBOutlet weak var seachBarForContact: UISearchBar!

      @IBOutlet weak var noContactFoundLabel: UILabel!
      //table view on search
      @IBOutlet weak var tablePopulatedOnSearch: UITableView!
      //now view
      @IBOutlet weak var nowView: UIView!
      @IBOutlet weak var NowIsUnslected: UIButton!
      @IBOutlet weak var NowIsSelected: UIButton!
      //schedule view
      @IBOutlet weak var scheduledView: UIView!
      @IBOutlet weak var ScheduledIsUnselected: UIButton!
      @IBOutlet weak var ScheduledIsSelected: UIButton!
      //scheduling view
      @IBOutlet weak var scheduleHereView: UIView!
      @IBOutlet weak var endDayCalenderButton: UIButton!
      @IBOutlet weak var repeatButton: UIButton!
      @IBOutlet weak var watchIcon: UIButton!
      @IBOutlet weak var calenderIcon: UIButton!
      @IBOutlet weak var schedulingDateLabel: UILabel!
      @IBOutlet weak var schedulingTimeLabel: UILabel!
      @IBOutlet weak var schedulingEndDateLabel: UILabel!
      //view above now viw
      @IBOutlet weak var label1ToShowNoContactChoosen: UILabel!
      @IBOutlet weak var viewAboveNowView: UIView!
      //profile images listing
      @IBOutlet weak var profileImageStackView: UIStackView!
      @IBOutlet weak var profileImageOne: UIView!
      @IBOutlet weak var profileImageThree: UIView!
      @IBOutlet weak var profileImageTwo: UIView!
      @IBOutlet weak var profileImageFour: UIView!
      @IBOutlet weak var profileViewFive: UIView!
      @IBOutlet weak var profileImageViewOne: UIImageView!
      @IBOutlet weak var profileImageViewTwo: UIImageView!
      @IBOutlet weak var profileImageViewFour: UIImageView!
      @IBOutlet weak var profileImageViewThree: UIImageView!
      @IBOutlet weak var profileImageViewFive: UIImageView!
      @IBOutlet weak var profileFirstName: UILabel!
      @IBOutlet weak var profileTwoName: UILabel!
      @IBOutlet weak var profileThreeLabel: UILabel!
      @IBOutlet weak var profileFourLabel: UILabel!
      @IBOutlet weak var hideSelContactList: UIImageView!
      @IBOutlet weak var showSelContactList: UIImageView!
      @IBOutlet weak var hideSelectedContactListBtn: UIButton!
      @IBOutlet weak var viewContactListBtn: UIButton!
      //single contact is selected then show that contacts image,name...
      @IBOutlet weak var selectedSingleContactView: UIView!
      @IBOutlet weak var selectedSingleContactImage: UIImageView!
      @IBOutlet weak var selectedSingleContactName: UILabel!
      @IBOutlet weak var selectedSingleContactPhoneNo: UILabel!
      @IBOutlet weak var selectedSingleContactStackViewDetails: UIView!
      @IBOutlet weak var selectedSingleContactSeperation: UIView!
      @IBOutlet weak var selectedSingleContactEmail: UILabel!
      @IBOutlet weak var selectedSingleContactFisheyeStrip: UIView!
      @IBOutlet weak var selectedSingleContactDeleteBtn: UIButton!
      @IBOutlet weak var onlyOneDetailFoSingleSelectedContact: UILabel!
      @IBOutlet weak var topBorderOfSelectedContactsTable: UIView!
      //day stack view and buttons
      @IBOutlet weak var daysStackView: UIStackView!
      @IBOutlet weak var sundayBtn: UIButton!
      @IBOutlet weak var mondayBtn: UIButton!
      @IBOutlet weak var tuesdayBtn: UIButton!
      @IBOutlet weak var wensdayBtn: UIButton!
      @IBOutlet weak var ThuresdayBtn: UIButton!
      @IBOutlet weak var fridayBtn: UIButton!
      @IBOutlet weak var saturdayBtn: UIButton!
      @IBOutlet weak var insertCapsuleButton: UIButton!
      @IBOutlet weak var floatindBtn: UIButton!
      @IBOutlet weak var scheduledLabel: UILabel!
      @IBOutlet weak var nowLabel: UILabel!

      @IBOutlet weak var fisheyeLabel: UILabel!
      @IBOutlet weak var letsWorkTogetherLabel: UILabel!

      /*************************8*************** IB Actions ****************************************/

      @IBAction func viewAndHideSelContactList(_ sender: Any) {
      }

      @IBAction func calenderIconClick(_ sender: Any) {
            self.calenderButtonClicked()
      }

      @IBAction func endDayCalenderButtonClick(_ sender: Any) {
            self.endDateForSchedular()
      }

      @IBAction func watchIconClick(_ sender: Any) {
            self.watchButtonClicked()
      }

      @IBAction func repeatButtonClick(_ sender: Any) {
            self.daysStackView.isUserInteractionEnabled=(!(self.daysStackView.isUserInteractionEnabled))
            if(self.daysStackView.isUserInteractionEnabled) {
                self.repeatButton.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
            } else {
                  self.unSelectRepeater()
            }
      }

      @IBAction func sundayBtnClick(_ sender: Any) {
            self.addOrRemoveDayAccIndex(indexOfDay: 0)
      }

      @IBAction func mondayBtnClick(_ sender: Any) {
            self.addOrRemoveDayAccIndex(indexOfDay: 1)
      }

      @IBAction func tuesdayBtnClick(_ sender: Any) {
            self.addOrRemoveDayAccIndex(indexOfDay: 2)
      }

      @IBAction func wensdayBtnClick(_ sender: Any) {
            self.addOrRemoveDayAccIndex(indexOfDay: 3)
      }

      @IBAction func thuesdayBtnClick(_ sender: Any) {
            self.addOrRemoveDayAccIndex(indexOfDay: 4)
      }

      @IBAction func fridayBtnClick(_ sender: Any) {
            self.addOrRemoveDayAccIndex(indexOfDay: 5)
      }

      @IBAction func saturdayBtnClick(_ sender: Any) {
            self.addOrRemoveDayAccIndex(indexOfDay: 6)
      }

      @IBAction func selectedSingleContactDeleteBtnClick(_ sender: Any) {
            self.removeSelectedSingleContact()
      }

      @IBAction func hideSelectedContactListBtnClick(_ sender: Any) {
            self.hideSelectedContactList()
      }

      @IBAction func viewContactListBtnClick(_ sender: Any) {
            self.viewSelectedContactListBtn()
      }

      @IBAction func scheduledUnselected(_ sender: Any) {
            self.schedulingOptionSelected()
      }

      @IBAction func nowUnselected(_ sender: Any) {
            self.nowOptionSelected()
      }

      @IBAction func requestBtn(_ sender: UIButton) {
            self.pulseTypeRequestChoosen()
      }

      @IBAction func shareBtn(_ sender: UIButton) {
            self.pulseTypeShareChoosen()
      }

      @IBAction func addConatctBtn(_ sender: Any) {
            self.noContactFoundLabel.isHidden=true
            self.addContactLabel.textColor=UIColor.unSelectedTextColor()
            if(self.tablePopulatedOnSearch.isHidden) {
                  self.addContactToWhomPulseNeedToBeSend()
            } else {
                  self.tablePopulatedOnSearch.isHidden=true
                  self.floatindBtn.isHidden=true
                  if(self.selectedContactArray.count==0) {
                        self.viewAboveNowView.isHidden=false
                        self.label1ToShowNoContactChoosen.isHidden=false
                  } else {
                    self.theseCotactsAreChoosen((Any).self)
                  }
            }
      }

      @IBAction func backBtnUV(_ sender: Any) {
            self.openPulseListingPage()
      }

      @IBAction func fishEyeIconBtn(_ sender: Any) {
            self.loadDashBoard()
      }

      @IBAction func searchContactBtn(_ sender: Any) {
            self.searchContactClick()
      }

      @IBAction func insertcapsuleBtn(_ sender: Any) {
            self.checkforSelectedArrayCount(fromNextOrInsert: "fromInsert")
      }

      @IBAction func goToNextBtn(_ sender: Any) {
            self.checkforSelectedArrayCount(fromNextOrInsert: "fromNext")
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.pulseLabelUV.text = self.pulseMsgs.shareAndRequestPageTitle
                  self.requestButton.setTitle(self.pulseMsgs.requestTabOfShareAndRequestScrren, for: .normal)
                  self.sundayBtn.setTitle(self.pulseMsgs.sundayOneLetter, for: .normal)

                  self.mondayBtn.setTitle(self.pulseMsgs.mondayOneLetter, for: .normal)
                  self.tuesdayBtn.setTitle(self.pulseMsgs.tuesdayOneLetter, for: .normal)

                  self.wensdayBtn.setTitle(self.pulseMsgs.wedOneLetter, for: .normal)

                  self.ThuresdayBtn.setTitle(self.pulseMsgs.thruOneLetter, for: .normal)

                  self.fridayBtn.setTitle(self.pulseMsgs.fridayOneLetter, for: .normal)

                  self.saturdayBtn.setTitle(self.pulseMsgs.saturedayOneLetter, for: .normal)

                  self.shareButton.setTitle(self.pulseMsgs.shareTabOfShareAndRequestScrren, for: .normal)

                  self.addContactLabel.text = self.pulseMsgs.addContactLabel

                  self.label1ToShowNoContactChoosen.text = self.pulseMsgs.noContactChoosenLabel

                  self.insertCapsuleButton.setTitle(self.pulseMsgs.insertCapsuleLabel, for: .normal)

                  self.fisheyeLabel.text = self.pulseMsgs.fisheyeLabel

                  self.letsWorkTogetherLabel.text = self.pulseMsgs.letsWorkTogetherLabel

                  self.nowLabel.text = self.pulseMsgs.nowPulse

                  self.scheduledLabel.text = self.pulseMsgs.schedulePulse

                  self.schedulingTimeLabel.text = self.pulseMsgs.pulsetime

                  self.schedulingDateLabel.text = self.pulseMsgs.startDate

                  self.schedulingEndDateLabel.text = self.pulseMsgs.endDate

                  self.repeatButton.setTitle(self.pulseMsgs.pulseRepeaterLabel, for: .normal)
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /******************************************** Global variables **********************************************/

      var contactArray = [ContactData]()
      var selectedContactArray=[ContactData]()
      var selectedContactIdArray=[String]()
      var requestType="1"
      var appService = AppService.shared
      var isScheduledProperly: Bool = false
      let appSharedPrefernce = AppSharedPreference()
      var pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
      var idtoken: String?
      //var floatindBtn=UIButton(frame:CGRect(origin:CGPoint(x:290,y:420),size:CGSize(width:50,height:50)))
      //var noContactFoundLabel=UILabel(frame:CGRect(origin:CGPoint(x:80,y:300),size:CGSize(width:250,height:15)))
      var pulseType: String?
      var selectedContactId: String?
      var scheduledOrNow: String?
      var searchKey: String?
      var reloadPulseLogbook: ReloadPulseLogbook?
      var repeartersArray=[String]()
      var fisheyeContacts=[ContactObj] ()
      var nonFisheyeContacts=[ContactObj] ()
      var allContactList=[ContactObj] ()
      var schedulingUTCTime: String?
      var reachedToEditPulse: Bool = false
      var pulseDetailsNeedToBeEdited=PulseDetails()
      var validationController = ValidationController()
      var startDate = ""
      var endDate = ""
      var startDateUTC: Date?
      var endDateUTC: Date?
      var pulseTimeUTC: Date?
      var nowTimeUTC: Date?
      var pulseTime = ""
      var nowTime = ""
      var isSearched: Bool = false
      //section related
      var sections = ["", ""]
      var sections2 = ["", ""]
      var sectionData: [Int: [ContactData]] = [:]
      var sectionData2: [Int: [ContactData]] = [:]
      var hideTableSection0 = false
      var hideTableSection1 = false
      var hideTableSection20 = false
      var hideTableSection21 = false
      var fishEyeContactArr=[ContactData]()
      var phoneContactArr=[ContactData]()
      var selectedPhoneContactArr=[ContactData]()
      var selectedFisheyeContactArr=[ContactData]()
      var startDatetest = ""
      var endDatetest = ""
      var startDateinvalidtest = ""
      var EndDateinvalidtest = ""
    
      /********************************************* preDefined VC functions *********************************************/

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.appService = AppService.shared
            self.pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
            self.setLocalizationText()
            self.addObservers()

            self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

            sections = [self.pulseMsgs.fishEyeContactLabel, self.pulseMsgs.phoneContactLabel]
            sections2 = [self.pulseMsgs.fishEyeContactLabel, self.pulseMsgs.phoneContactLabel]

            self.floatindBtn.isHidden=true

            self.noContactFoundLabel.textColor=UIColor.lightGray
            self.noContactFoundLabel.isHidden=true
            self.noContactFoundLabel.text=self.pulseMsgs.noContactFound
            // self.view.addSubview(self.noContactFoundLabel)

        let tapFloatingBtn = UITapGestureRecognizer(target: self, action: Selector(("theseCotactsAreChoosen:")))
            floatindBtn.addGestureRecognizer(tapFloatingBtn)

            self.daysStackView.isUserInteractionEnabled=false
            self.scheduleHereView.isUserInteractionEnabled=false

            self.tablePopulatedOnSearch.allowsMultipleSelection=true
            self.tablePopulatedOnSearch.allowsSelectionDuringEditing = true
            self.noContactFoundLabel.isHidden=true
            tablePopulatedOnSearch.delegate = self

            sectionData =  [0: fishEyeContactArr, 1: phoneContactArr]
            sectionData2 = [0: selectedFisheyeContactArr, 1: selectedPhoneContactArr]

            self.showTheseAsDefault()
            self.applyViewShadow()
            self.hideTheseOnViewLoad()

            NotificationCenter.default.addObserver(forName: DISMISS_LOADER_WHEN_LOCATION_SETTING_OPENED, object: nil, queue: nil) { (_) in
                  self.stopLoader()
            }

            if(self.reachedToEditPulse) {
                DispatchQueue.main.async {
                    self.editPulseData()
                }
            }
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ShareAndRequestPulseVCViewController)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      /************************************* search contact related functions *********************************/

      func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            self.contactArray = []
            self.fishEyeContactArr = []
            self.phoneContactArr = []
            self.searchKey=self.seachBarForContact.text
      }

      func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.seachBarForContact.endEditing(true)
            self.seachBarForContact.resignFirstResponder()
            self.seachBarForContact.isHidden = true
            self.tablePopulatedOnSearch.isHidden=true
            self.noContactFoundLabel.isHidden=true
            self.floatindBtn.isHidden=true
            if(self.selectedContactArray.count==0) {
                  self.viewAboveNowView.isHidden=false
                  self.label1ToShowNoContactChoosen.isHidden=false
            } else {
                self.theseCotactsAreChoosen((Any).self)
            }
            self.searchKey=""
            self.seachBarForContact.text = ""
      }

      func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText: String = searchBar.text!
            self.seachBarForContact.endEditing(true)
            self.contactArray = []
            self.fishEyeContactArr = []
            self.phoneContactArr = []
        let searchFEContacts: [ContactObj] = DatabaseManagement.shared.searchUnblockedFEContacts(searchText: searchText)
        let searchNonFEContacts: [ContactObj] = DatabaseManagement.shared.searchUnblockedNonFEContacts(searchText: searchText)
            self.seachBarForContact.resignFirstResponder()
            self.viewAboveNowView.isHidden=true
            self.tablePopulatedOnSearch.isHidden=false
            self.floatindBtn.isHidden=false
            self.refreshSearchedContactList(searchFEContacts: searchFEContacts, searchNonFEContacts: searchNonFEContacts, searchText: searchText)
            self.beforeTablePopulatedOnSearch()
      }

      func refreshSearchedContactList (searchFEContacts: [ContactObj], searchNonFEContacts: [ContactObj], searchText: String) {
            self.startLoader()
            self.fisheyeContacts = searchFEContacts
            self.fisheyeContacts.sort { $0.name < $1.name }
            self.nonFisheyeContacts = searchNonFEContacts
            self.nonFisheyeContacts.sort { $0.name < $1.name }
            self.allContactList  = []
            for i in 0..<self.fisheyeContacts.count {
                  self.allContactList.append(self.fisheyeContacts[i])
            }
            for i in 0..<self.nonFisheyeContacts.count {
                  self.allContactList.append(self.nonFisheyeContacts[i])
            }
            self.contactArray = []
            self.fishEyeContactArr = []
            self.phoneContactArr = []
            if(self.allContactList.count == 0) {
                  self.stopLoader()
                  self.noContactFoundLabel.isHidden=false
            } else {
                  self.noContactFoundLabel.isHidden=true
                  for contactObj in self.allContactList {
                        var contData = ContactData()
                        var stringContactId: String = ""
                        stringContactId = String(contactObj.contactId)
                        contData.contactId = stringContactId
                        contData.contactName = contactObj.name
                        contData.contactEmailId = contactObj.email
                        contData.contactPhoneNumber = contactObj.phoneNumber
                        contData.contactImage = contactObj.picture
                        contData.contactFEId = contactObj.contactFEId
                        contData.privacyEnabled = contactObj.privacyEnabled
                        contData.isBlocked = contactObj.isBlocked
                        contData.sharedData = contactObj.sharedData

                        if(contactObj.contactFEId.isEmpty || contactObj.contactFEId=="" || contactObj.contactFEId==" " || contactObj.contactFEId==nil) {
                              self.phoneContactArr.append(contData)
                        } else {
                              self.fishEyeContactArr.append(contData)
                        }
                        self.contactArray.append(contData)
                  }
                  DispatchQueue.main.async {
                        self.stopLoader()
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "get contacts", action: "get contact list on search", label: "Contacts fetched successfully", value: 0)
                        self.beforeTablePopulatedOnSearch()
                  }
            }
      }

      /*********************************** Now/Scheduled and Request/Share **************************************/

      //when user wants to share a pulse,and share get clicked
      func pulseTypeShareChoosen() {
            self.requestButton.backgroundColor=UIColor.selectedTextColor()
        self.requestButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
            self.shareButton.backgroundColor=UIColor.white
        self.shareButton.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
            self.pulseType="1"
      }

      //when user wants to request a pulse,and request is clicked
      func pulseTypeRequestChoosen() {
            self.shareButton.backgroundColor=UIColor.selectedTextColor()
        self.shareButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
            self.requestButton.backgroundColor=UIColor.white
        self.requestButton.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
            self.pulseType="0"
      }

      //now option is slected by user then
      func nowOptionSelected() {
            self.setDefaultColorForScheLabels()
            if(self.scheduledOrNow=="Now") {

            } else {
                  self.NowIsSelected.isHidden=false
                  self.NowIsUnslected.isHidden=true
                  self.ScheduledIsSelected.isHidden=true
                  self.ScheduledIsUnselected.isHidden=false
                  self.scheduleHereView.isUserInteractionEnabled=false
                  self.nowView.backgroundColor=UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                  self.scheduledView.backgroundColor=UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                  self.scheduledOrNow="Now"
                  self.unSelectRepeater()
                  self.schedulingTimeLabel.text=self.pulseMsgs.pulsetime
                  self.schedulingDateLabel.text=self.pulseMsgs.startDate
                  self.schedulingEndDateLabel.text=self.pulseMsgs.endDate
            }
      }

      //scheduling option is selected  then
      func schedulingOptionSelected() {
            self.setDefaultColorForScheLabels()
            if(self.scheduledOrNow=="Scheduled") {

            } else {
                  self.ScheduledIsSelected.isHidden=false
                  self.ScheduledIsUnselected.isHidden=true
                  self.scheduleHereView.isHidden=false
                  self.scheduleHereView.isUserInteractionEnabled=true
                  self.scheduledView.backgroundColor=UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                  self.nowView.backgroundColor=UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                  self.scheduledOrNow="Scheduled"
                  self.NowIsSelected.isHidden=true
                  self.NowIsUnslected.isHidden=false
            }
      }

      func setDefaultColorForScheLabels() {
            self.schedulingTimeLabel.textColor=UIColor.unSelectedTextColor()
            self.schedulingDateLabel.textColor=UIColor.unSelectedTextColor()
            self.schedulingEndDateLabel.textColor=UIColor.unSelectedTextColor()
            self.repeatButton.setTitleColor(UIColor.unSelectedTextColor(), for: .normal)
      }

      /****************************************** Validation functions ****************************************/

      //check selected contact array count before sending a pulse(waring)
      func sendPulseAccordingToSelectedParameters() {
            if((self.reachedToEditPulse) && (self.selectedContactArray.count>0) && (self.scheduledOrNow=="Scheduled") && (self.isScheduledProperly) ) {
                  self.updateScheduledPulse(pulseId: self.pulseDetailsNeedToBeEdited.pulseId)
            } else {
                  if(self.pulseType=="0" && self.selectedContactIdArray.count>0) {
                        if(self.scheduledOrNow=="Now") {
                              self.requestPulseNow()
                        } else if (self.scheduledOrNow=="Scheduled" && self.isScheduledProperly) {
                              self.requestPulseAtScheduled()
                        }
                  } else if(self.pulseType=="1" && self.selectedContactIdArray.count>0) {
                        if(self.scheduledOrNow=="Now") {
                              self.sharePulseNow()
                        } else if(self.scheduledOrNow=="Scheduled" && self.isScheduledProperly) {
                              self.sharePulseAtScheduled()
                        }
                  }
            }
      }

      //this is check selected array count before sending a pulse(waring)
      func checkforSelectedArrayCount(fromNextOrInsert: String) {
            if(self.selectedContactArray.count<=0) {
                  self.addContactLabel.textColor=UIColor.red
            } else {
                  self.checkforProperScheduling(fromNextOrInsert: fromNextOrInsert)
            }
      }

      //this function loads all the user contacts,when clicked on add contact btn,to choose contacts to whom pulse need to send
      func addContactToWhomPulseNeedToBeSend() {
            self.hideSelectedContactListBtn.isHidden=true
            self.hideSelContactList.isHidden=true
            self.searchKey=""
            self.viewAboveNowView.isHidden=true
            self.viewContactListBtn.isHidden=true
            self.seachBarForContact.isHidden=true
            self.tablePopulatedOnSearch.isHidden=false
            self.floatindBtn.isHidden=false
            if self.contactArray.isEmpty {
                  self.getFishEyeContacts()
            } else {
                  self.beforeTablePopulatedOnSearch()
            }
      }

      func beforeTablePopulatedOnSearch() {
            if self.fishEyeContactArr.count==0 {
                  self.hideTableSection0 = true
            } else {
                  self.hideTableSection0 = false
            }

            if  self.phoneContactArr.count==0 {
                  self.hideTableSection1 = true
            } else {
                  self.hideTableSection1 = false
            }
            self.sectionData =  [0: self.fishEyeContactArr, 1: self.phoneContactArr]
            self.tablePopulatedOnSearch.reloadData()

      }

      func beforeTableSelectedContact() {
            if self.selectedFisheyeContactArr.count==0 {
                  self.hideTableSection20 = true
            } else {
                  self.hideTableSection20 = false
            }

            if  self.selectedPhoneContactArr.count==0 {
                  self.hideTableSection21 = true
            } else {
                  self.hideTableSection21 = false
            }
            self.sectionData2 =  [0: self.selectedFisheyeContactArr, 1: self.selectedPhoneContactArr]
            self.selectedContactTableView.isHidden=false
            self.selectedContactTableView.reloadData()
      }

      //to go to dashboard
      func loadDashBoard() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
            self.present(nextVC, animated: true, completion: nil)
      }

      //this to hide and remove selected single contact when selected single contact delete btn is cliked
      func removeSelectedSingleContact() {
            //removed one which was selected,so,arrya should be empty now
            var contData = ContactData()
            contData = self.selectedContactArray[0]

            if(contData.contactFEId.isEmpty || contData.contactFEId=="" || contData.contactFEId==" ") {
                  self.selectedPhoneContactArr=self.selectedPhoneContactArr.filter {$0.contactId != contData.contactId}
            } else {
                  self.selectedFisheyeContactArr=self.selectedFisheyeContactArr.filter {$0.contactId != contData.contactId}
            }
            self.selectedContactArray=self.selectedContactArray.filter {$0.contactId != contData.contactId}
            self.selectedContactIdArray=self.selectedContactIdArray.filter {$0 != contData.contactId}

            self.profileImageStackView.isHidden=true
            self.hideSelectedContactListBtn.isHidden=true
            self.hideSelContactList.isHidden=true
            self.viewContactListBtn.isHidden=true
            self.showSelContactList.isHidden = true
            self.selectedSingleContactView.isHidden=true
            self.selectedContactArray=[]
            self.selectedContactIdArray=[]
            self.selectedContactTableView.isHidden=true
            self.scheduleHereView.isHidden=false
            self.topBorderOfSelectedContactsTable.isHidden = true
            self.label1ToShowNoContactChoosen.isHidden=false
      }

      //this will used to list selected contacts in a tabular form
      func viewSelectedContactListBtn() {
            self.scheduleHereView.isHidden = true
            self.viewContactListBtn.isHidden=true
            self.showSelContactList.isHidden = true
            self.selectedContactTableView.isHidden = false
            self.scheduleHereView.isHidden=true
            self.topBorderOfSelectedContactsTable.isHidden = false
            self.topBorderOfSelectedContactsTable.isHidden = false
            self.hideSelectedContactListBtn.isHidden = false
            self.hideSelContactList.isHidden=false
            self.beforeTableSelectedContact()
      }

      //if selected Array size is greater than four then
      func isSelectedArrayCountGreaterThanFourThen() {

            self.selectedSingleContactView.isHidden=true
            self.viewContactListBtn.isHidden=false
            self.showSelContactList.isHidden = false
            self.label1ToShowNoContactChoosen.isHidden=true
            self.profileImageStackView.isHidden=false

            self.profileFirstName.isHidden=false
            self.profileTwoName.isHidden=false
            self.profileThreeLabel.isHidden=false
            self.profileFourLabel.isHidden=false

            if(self.selectedContactArray[0].contactName=="" || self.selectedContactArray[0].contactName.isEmpty || self.selectedContactArray[0].contactName==" ") {
                  self.profileFirstName.text="Unknown"
            } else {
                  self.profileFirstName.text=self.selectedContactArray[0].contactName
            }

            if(self.selectedContactArray[1].contactName=="" || self.selectedContactArray[1].contactName.isEmpty || self.selectedContactArray[1].contactName==" ") {
                  self.profileTwoName.text="Unknown"
            } else {
                  self.profileTwoName.text=self.selectedContactArray[1].contactName
            }

            if(self.selectedContactArray[2].contactName=="" || self.selectedContactArray[2].contactName.isEmpty || self.selectedContactArray[2].contactName==" ") {
                  self.profileThreeLabel.text="Unknown"
            } else {
                  self.profileThreeLabel.text=self.selectedContactArray[2].contactName
            }

            if(self.selectedContactArray[3].contactName=="" || self.selectedContactArray[3].contactName.isEmpty || self.selectedContactArray[3].contactName==" ") {
                  self.profileFourLabel.text="Unknown"
            } else {
                  self.profileFourLabel.text=self.selectedContactArray[3].contactName
            }

            //one
            self.profileImageOne.isHidden=false
            self.profileImageViewOne.isHidden=false
            var contactImageUrl1=self.selectedContactArray[0].contactImage
//            self.profileImageViewOne?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
        
            self.profileImageViewOne?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            //two
            self.profileImageTwo.isHidden=false
            self.profileImageViewTwo.isHidden=false
            var contactImageUrl2=self.selectedContactArray[1].contactImage
//            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            //three
            self.profileImageThree.isHidden=false
            self.profileImageViewThree.isHidden=false
            var contactImageUrl3=self.selectedContactArray[2].contactImage
//            self.profileImageViewThree?.sd_setImage(with: URL(string: contactImageUrl3?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            self.profileImageViewThree?.sd_setImage(with: URL(string: contactImageUrl3?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            //four
            self.profileImageFour.isHidden=false
            self.profileImageViewFour.isHidden=false
            var contactImageUrl4=self.selectedContactArray[3].contactImage
//            self.profileImageViewFour?.sd_setImage(with: URL(string: contactImageUrl4?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            self.profileImageViewFour?.sd_setImage(with: URL(string: contactImageUrl4?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            self.profileViewFive.isHidden=false
            self.profileImageViewFive.isHidden=false
      }

      //if the selected array size is equal to one then
      func isSelectedArrayCountEQualToOneThen() {
            self.selectedSingleContactView.isHidden=false
            self.scheduleHereView.isHidden=false
            self.selectedContactTableView.isHidden=true
            self.topBorderOfSelectedContactsTable.isHidden = true
            var contactImageUrl=self.selectedContactArray[0].contactImage
            if(self.selectedContactArray[0].contactName=="" || self.selectedContactArray[0].contactName.isEmpty || self.selectedContactArray[0].contactName==" " || self.selectedContactArray[0].contactName==nil) {
                  self.selectedSingleContactName.text="Unknown"
            } else {
                  self.selectedSingleContactName.text=self.selectedContactArray[0].contactName
            }

            if(self.selectedContactArray[0].contactFEId != "") {
                  self.selectedSingleContactFisheyeStrip.backgroundColor = UIColor.purpleRing()
            } else {
                  self.selectedSingleContactFisheyeStrip.backgroundColor = UIColor.clear
            }

            let phone = self.selectedContactArray[0].contactPhoneNumber!
            let email = self.selectedContactArray[0].contactEmailId!
            let privacyEnabled = self.selectedContactArray[0].privacyEnabled ?? false
            let sharedData = self.selectedContactArray[0].sharedData

            if(phone.isEmpty && !email.isEmpty) {
                  self.selectedSingleContactStackViewDetails.isHidden = true
                  self.onlyOneDetailFoSingleSelectedContact.isHidden = false
                  self.onlyOneDetailFoSingleSelectedContact.text = email
            } else if(!phone.isEmpty && email.isEmpty) {
                  self.selectedSingleContactStackViewDetails.isHidden = true
                  self.onlyOneDetailFoSingleSelectedContact.isHidden = false
                  self.onlyOneDetailFoSingleSelectedContact.text = phone
            } else if(!phone.isEmpty && !email.isEmpty) {
                  self.onlyOneDetailFoSingleSelectedContact.isHidden = true
                  self.selectedSingleContactEmail.isHidden=false
                  self.selectedSingleContactPhoneNo.isHidden=false
                  self.selectedSingleContactSeperation.isHidden=false
                  self.selectedSingleContactPhoneNo.text = phone
                  self.selectedSingleContactEmail.text = email
                  if(privacyEnabled && sharedData == 0) {
                        self.onlyOneDetailFoSingleSelectedContact.text = email
                        self.onlyOneDetailFoSingleSelectedContact.isHidden = false
                        self.selectedSingleContactSeperation.isHidden = true
                        self.selectedSingleContactStackViewDetails.isHidden = true
                  } else if(privacyEnabled && sharedData == 1) {
                        self.onlyOneDetailFoSingleSelectedContact.text = phone
                        self.onlyOneDetailFoSingleSelectedContact.isHidden = false
                        self.selectedSingleContactSeperation.isHidden = true
                        self.selectedSingleContactStackViewDetails.isHidden = true
                  } else {
                        self.onlyOneDetailFoSingleSelectedContact.isHidden = true
                        self.selectedSingleContactStackViewDetails.isHidden = false
                        self.selectedSingleContactPhoneNo.text = phone
                        self.selectedSingleContactEmail.text = email
                        self.selectedSingleContactSeperation.isHidden = false
                  }
            } else {
                  self.selectedSingleContactPhoneNo.text = "n/a"
                  self.selectedSingleContactEmail.text = "n/a"
                  self.selectedSingleContactSeperation.isHidden = false
                  self.selectedSingleContactStackViewDetails.isHidden = false
                  self.onlyOneDetailFoSingleSelectedContact.isHidden = true
            }

//            self.selectedSingleContactImage?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            self.selectedSingleContactImage?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            self.viewContactListBtn.isHidden=true
            self.showSelContactList.isHidden = true
            self.profileImageViewFive.isHidden=true
            self.label1ToShowNoContactChoosen.isHidden=true
      }

      //if the selected array size is equal to two then
      func isSelectedArrayCountEqualToTwo() {
            self.profileImageOne.isHidden=false
            self.profileImageTwo.isHidden=false
            self.profileImageThree.isHidden=false
            self.profileImageFour.isHidden=false
            self.profileViewFive.isHidden=false
            self.profileImageViewOne.isHidden=false
            self.profileImageViewTwo.isHidden=false
            self.profileImageViewThree.isHidden=true
            self.profileImageViewFour.isHidden=true
            self.profileImageViewFive.isHidden=true
            if(self.selectedContactArray[1].contactName.isEmpty || self.selectedContactArray[1].contactName == "" || self.selectedContactArray[1].contactName == " ") {
                  self.profileTwoName.text="Unknown"
            } else {
                  self.profileTwoName.text=self.selectedContactArray[1].contactName
            }
            self.profileFirstName.isHidden=false
            self.profileTwoName.isHidden=false
            self.profileThreeLabel.isHidden=true
            self.profileFourLabel.isHidden=true
            var contactImageUrl2=self.selectedContactArray[1].contactImage
        
//            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
    }

      //if the selected Array size is equal three then
      func isSelectedArrayCountEqualToThree() {
            self.profileImageOne.isHidden=false
            self.profileImageTwo.isHidden=false
            self.profileImageThree.isHidden=false
            self.profileImageFour.isHidden=false
            self.profileViewFive.isHidden=false
            self.profileImageViewOne.isHidden=false
            self.profileImageViewTwo.isHidden=false
            self.profileImageViewThree.isHidden=false
            self.profileImageViewFour.isHidden=true
            self.profileImageViewFive.isHidden=true
            if(self.selectedContactArray[1].contactName.isEmpty || self.selectedContactArray[1].contactName == "" || self.selectedContactArray[1].contactName == " ") {
                  self.profileTwoName.text="Unknown"
            } else {
                  self.profileTwoName.text=self.selectedContactArray[1].contactName
            }
            if(self.selectedContactArray[2].contactName.isEmpty || self.selectedContactArray[2].contactName == "" || self.selectedContactArray[2].contactName == " ") {
                  self.profileThreeLabel.text="Unknown"
            } else {
                  self.profileThreeLabel.text=self.selectedContactArray[2].contactName
            }
            self.profileFirstName.isHidden=false
            self.profileTwoName.isHidden=false
            self.profileThreeLabel.isHidden=false
            self.profileFourLabel.isHidden=true
            var contactImageUrl2=self.selectedContactArray[1].contactImage
//            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            var contactImageUrl3=self.selectedContactArray[2].contactImage
//            self.profileImageViewThree?.sd_setImage(with: URL(string: contactImageUrl3?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            self.profileImageViewThree?.sd_setImage(with: URL(string: contactImageUrl3?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
    }

      //if the selected Array size is eqal to four then
      func isSelectedArrayCountEqualToFour() {
            self.profileImageOne.isHidden=false
            self.profileImageTwo.isHidden=false
            self.profileImageThree.isHidden=false
            self.profileImageFour.isHidden=false
            self.profileViewFive.isHidden=false

            self.profileImageViewOne.isHidden=false
            self.profileImageViewTwo.isHidden=false
            self.profileImageViewThree.isHidden=false
            self.profileImageViewFour.isHidden=false
            self.profileImageViewFive.isHidden=true

            if(self.selectedContactArray[1].contactName.isEmpty || self.selectedContactArray[1].contactName == "" || self.selectedContactArray[1].contactName == " ") {
                  self.profileTwoName.text="Unknown"
            } else {
                  self.profileTwoName.text=self.selectedContactArray[1].contactName
            }
            if(self.selectedContactArray[2].contactName.isEmpty || self.selectedContactArray[2].contactName == "" || self.selectedContactArray[2].contactName == " ") {
                  self.profileThreeLabel.text="Unknown"
            } else {
                  self.profileThreeLabel.text=self.selectedContactArray[2].contactName
            }
            if(self.selectedContactArray[3].contactName.isEmpty || self.selectedContactArray[3].contactName == "" || self.selectedContactArray[3].contactName == " ") {
                  self.profileFourLabel.text="Unknown"
            } else {
                  self.profileFourLabel.text=self.selectedContactArray[3].contactName
            }
            self.profileFirstName.isHidden=false
            self.profileTwoName.isHidden=false
            self.profileThreeLabel.isHidden=false
            self.profileFourLabel.isHidden=false

            var contactImageUrl2=self.selectedContactArray[1].contactImage
//            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            self.profileImageViewTwo?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            var contactImageUrl3=self.selectedContactArray[2].contactImage
//            self.profileImageViewThree?.sd_setImage(with: URL(string: contactImageUrl3?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            self.profileImageViewThree?.sd_setImage(with: URL(string: contactImageUrl3?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            var contactImageUrl4=self.selectedContactArray[3].contactImage
//            self.profileImageViewFour?.sd_setImage(with: URL(string: contactImageUrl4?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            self.profileImageViewFour?.sd_setImage(with: URL(string: contactImageUrl4?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
    }

      //if the selected array size is equal to zero then
      func isSelectedArrayCountEqualToZero() {
            self.profileImageStackView.isHidden=true
            self.profileImageViewOne.isHidden=true
            self.profileImageViewFour.isHidden=true
            self.profileImageViewTwo.isHidden=true
            self.profileImageViewOne.isHidden=true
            self.profileImageOne.isHidden=true
            self.profileImageTwo.isHidden=true
            self.profileImageThree.isHidden=true
            self.profileImageFour.isHidden=true
            self.nowView.isHidden=false
            self.scheduledView.isHidden=false
            self.scheduleHereView.isHidden=false
            self.selectedContactTableView.isHidden=true
            self.topBorderOfSelectedContactsTable.isHidden = true
            self.label1ToShowNoContactChoosen.isHidden=false
            self.viewContactListBtn.isHidden=true
            self.showSelContactList.isHidden = true
            self.hideSelectedContactListBtn.isHidden=true
            self.hideSelContactList.isHidden=true
            self.profileViewFive.isHidden=true
            self.profileImageViewFive.isHidden=true
            self.label1ToShowNoContactChoosen.isHidden=false
      }

      //this is called when user says to these contacts i want to send a pulse(clicking on Tick)
      func theseCotactsAreChoosen(_ sender: Any) {
            self.contactArray=[]
            self.fishEyeContactArr = []
            self.phoneContactArr = []
            self.seachBarForContact.isHidden=true
            self.viewAboveNowView.isHidden=false
            self.hideSelectedContactListBtn.isHidden=true
            self.hideSelContactList.isHidden=true
            self.selectedContactTableView.isHidden=true
            self.scheduleHereView.isHidden=false
            self.topBorderOfSelectedContactsTable.isHidden = true
            self.noContactFoundLabel.isHidden=true

            if(self.selectedContactIdArray.count>4) {
                  self.isSelectedArrayCountGreaterThanFourThen()
            } else if(self.selectedContactIdArray.count>1 && self.selectedContactIdArray.count<=4) {
                  self.viewAboveNowView.isHidden=false
                  self.profileImageStackView.isHidden=false
                  self.profileImageOne.isHidden=false
                  let contactImageUrl1=self.selectedContactArray[0].contactImage
//                  self.profileImageViewOne?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                 self.profileImageViewOne?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEncoding(withAllowedCharacters:
                    NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                if(self.selectedContactArray[0].contactName.isEmpty || self.selectedContactArray[0].contactName == ""
                        || self.selectedContactArray[0].contactName == " ") {
                        self.profileFirstName.text="Unknown"
                  } else {
                        self.profileFirstName.text=self.selectedContactArray[0].contactName
                  }

                  if(self.selectedContactIdArray.count==2 && self.selectedContactArray.count==2 ) {
                        self.isSelectedArrayCountEqualToTwo()
                  } else if(self.selectedContactIdArray.count==3 && self.selectedContactArray.count==3 ) {
                        self.isSelectedArrayCountEqualToThree()
                  } else if(self.selectedContactIdArray.count==4 && self.selectedContactArray.count==4 ) {
                        self.isSelectedArrayCountEqualToFour()
                  }
                  self.selectedSingleContactView.isHidden=true
                  self.profileViewFive.isHidden=true
                  self.profileImageViewFive.isHidden=true
                  self.viewContactListBtn.isHidden=false
                  self.showSelContactList.isHidden = false
                  self.label1ToShowNoContactChoosen.isHidden=true
            } else if(self.selectedContactIdArray.count==1 && self.selectedContactArray.count==1) {
                  self.isSelectedArrayCountEQualToOneThen()
            } else {
                  self.isSelectedArrayCountEqualToZero()
            }
            self.tablePopulatedOnSearch.isHidden=true
            self.floatindBtn.isHidden=true
      }

      //hide selected contact list on click of drop up
      func hideSelectedContactList() {
            self.viewContactListBtn.isHidden = false
            self.showSelContactList.isHidden = false
            self.scheduleHereView.isHidden = false
            self.selectedContactTableView.isHidden = true
            self.topBorderOfSelectedContactsTable.isHidden = true
            self.hideSelectedContactListBtn.isHidden = true
            self.hideSelContactList.isHidden=true
      }

      //
      func showTheseAsDefault() {
            //Request is bydefault choosen
            self.shareButton.backgroundColor=UIColor.selectedTextColor()
        self.shareButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
            self.requestButton.backgroundColor=UIColor.white
        self.requestButton.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
            self.pulseType="0"
            //now is bydefault choosen
            self.NowIsSelected.isHidden=false
            self.NowIsUnslected.isHidden=true
            self.ScheduledIsSelected.isHidden=true
            self.ScheduledIsUnselected.isHidden=false
            self.nowView.backgroundColor=UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
            self.scheduledView.backgroundColor=UIColor.white
            self.scheduledOrNow="Now"
            //So,All sceduling related disabled
      }

      func hideTheseOnViewLoad() {
            //selected single contact view hide
            self.selectedSingleContactView.isHidden=true
            //selected contact profile images view
            self.hideSelectedContactListBtn.isHidden = true
            self.hideSelContactList.isHidden=true
            self.profileImageStackView.isHidden=true
            self.showSelContactList.isHidden=true
            self.viewContactListBtn.isHidden=true
            self.showSelContactList.isHidden = true
            //hide search bar
            self.seachBarForContact.isHidden=true
            //hide table view shown on search
            self.tablePopulatedOnSearch.isHidden=true
            self.selectedContactTableView.isHidden = true
            self.scheduleHereView.isHidden=false
            self.topBorderOfSelectedContactsTable.isHidden = true
      }

      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            //check for second table as well
            if tableView == self.tablePopulatedOnSearch {
                  // let indexSelectetd = indexPath.row
                  var contData = ContactData()
                  contData = sectionData[indexPath.section]![indexPath.row]
                  if(self.selectedContactIdArray.contains(contData.contactId)) {
                  } else {
                        self.selectedContactArray.append(contData)
                        self.selectedContactIdArray.append(contData.contactId)
                        if(contData.contactFEId.isEmpty || contData.contactFEId=="" || contData.contactFEId==" " || contData.contactFEId==nil) {
                              self.selectedPhoneContactArr.append(contData)
                        } else {
                              self.selectedFisheyeContactArr.append(contData)
                        }
                  }
            } else if tableView == self.selectedContactTableView {
            }
      }

      func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
            //check for second table as well
            if tableView == self.tablePopulatedOnSearch {
                  var contData = ContactData()
                  contData = sectionData[indexPath.section]![indexPath.row]
                  self.selectedContactArray=self.selectedContactArray.filter {$0.contactId != contData.contactId}
                  self.selectedContactIdArray=self.selectedContactIdArray.filter {$0 != contData.contactId}
                  if(contData.contactFEId.isEmpty) {
                        self.selectedPhoneContactArr=self.selectedPhoneContactArr.filter {$0.contactId != contData.contactId}
                  } else {
                        self.selectedFisheyeContactArr=self.selectedFisheyeContactArr.filter {$0.contactId != contData.contactId}
                  }
                  self.beforeTableSelectedContact()
            } else if tableView == self.selectedContactTableView {

            }
      }

      //to provide name to a section,need to return a uiView always
      func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let view = UIView()
            var rowsInSection = 0
            if tableView == self.tablePopulatedOnSearch {
                  rowsInSection = self.sectionData[section]?.count ?? 0
            } else {
                  rowsInSection = self.sectionData2[section]?.count ?? 0
            }
            if(rowsInSection > 0) {
                  let title = UILabel()
                  title.font = UIFont(name: "Helvetica Neue", size: 18)
                  title.text = self.sections[section]
                  title.textColor = UIColor.black
                  title.font = UIFont.italicSystemFont(ofSize: 12)
                  let size = CGSize(width: 600, height: 1000)
                  let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                let estimatedFrame = NSString(string: title.text!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], context: nil)
                  view.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 25)
                  title.frame = CGRect(x: (view.frame.width/2)-(estimatedFrame.width/2), y: 4, width: 200, height: estimatedFrame.height + 5)
                  let constarint = NSLayoutConstraint(item: title, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
                  view.addSubview(title)
                  view.addConstraint(constarint)
                  view.backgroundColor =  UIColor.contactsTypeBg()
            }
            return view
      }

      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if tableView == self.tablePopulatedOnSearch {
                  let rowsInSection = self.sectionData[section]?.count ?? 0
                  var height = 25
                  if(rowsInSection == 0) {
                        height = 0
                  }
                  return CGFloat(height)
            } else {
                  let rowsInSection = self.sectionData2[section]?.count ?? 0
                  var height = 25
                  if(rowsInSection == 0) {
                        height = 0
                  }
                  return CGFloat(height)
            }
      }

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            var count: Int?
            if tableView == self.tablePopulatedOnSearch {
                  switch (section) {
                  case 0 :
                        if hideTableSection0 {
                              return 0
                        } else {
                              return (sectionData[section]?.count)!
                        }
                  case 1:
                        if hideTableSection1 {
                              return 0
                        } else {
                              return (sectionData[section]?.count)!
                        }
                  default:
                        return (sectionData[section]?.count)!
                  }
            }
            if tableView == self.selectedContactTableView {
                  switch (section) {
                  case 0 :
                        if hideTableSection20 {
                              return 0
                        } else {
                              return (sectionData2[section]?.count)!
                        }
                  case 1:
                        if hideTableSection21 {
                              return 0
                        } else {
                              return (sectionData2[section]?.count)!
                        }
                  default:
                        return (sectionData2[section]?.count)!
                  }
            }

            return count!
      }

      func numberOfSections(in tableView: UITableView) -> Int {
            var count: Int?
            if tableView == self.tablePopulatedOnSearch {
                  count=self.sections.count
            }
            if tableView == self.selectedContactTableView {
                  count=self.sections2.count
            }
            return count!
      }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var cell: UITableViewCell?
            if tableView == self.tablePopulatedOnSearch {
                  let onSearchCell = tableView.dequeueReusableCell(withIdentifier: "seachedContactTableViewCell", for: indexPath) as! seachedContactTableViewCell
                  let contactObj = sectionData[indexPath.section]![indexPath.row]
                  let privacyEnabled = contactObj.privacyEnabled ?? false
                  let sharedData = contactObj.sharedData ?? 2
                  let isBlocked = contactObj.isBlocked ?? false
                  var contactImageUrl=contactObj.contactImage

                  onSearchCell.imageOfSearchContact.layer.cornerRadius = onSearchCell.imageOfSearchContact.frame.size.width / 2
                  onSearchCell.imageOfSearchContact.clipsToBounds = true
//                  onSearchCell.imageOfSearchContact?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                  onSearchCell.imageOfSearchContact?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                  if(contactObj.contactFEId == "" || contactObj.contactFEId == " " || contactObj.contactFEId.isEmpty || contactObj.contactFEId == nil) {
                        onSearchCell.isFisheyeContact=false
                        onSearchCell.isFisheyeIndication.isHidden = true
                  } else {
                        onSearchCell.isFisheyeIndication.image = UIImage(named: "purpleStrip")
                        onSearchCell.isFisheyeContact=true
                        onSearchCell.isFisheyeIndication.isHidden = false
                  }

                  //if search contact name is empty then make it as unknown
                  if(contactObj.contactName == "" || contactObj.contactName == " " || contactObj.contactName == nil || contactObj.contactName.isEmpty) {
                        onSearchCell.seachedContactName.text = "Unknown"
                  } else {
                        onSearchCell.seachedContactName.text = contactObj.contactName
                  }

                  let phone = contactObj.contactPhoneNumber ?? ""
                  let email = contactObj.contactEmailId ?? ""

                  if(phone.isEmpty && !email.isEmpty) {
                        onSearchCell.stackViewDetails.isHidden = true
                        onSearchCell.emailInsteadOfPhoneNumber.isHidden = false
                        onSearchCell.emailInsteadOfPhoneNumber.text = email
                  } else if(!phone.isEmpty && email.isEmpty) {
                        onSearchCell.stackViewDetails.isHidden = true
                        onSearchCell.emailInsteadOfPhoneNumber.isHidden = false
                        onSearchCell.emailInsteadOfPhoneNumber.text = phone
                  } else if(!phone.isEmpty && !email.isEmpty) {
                        if(privacyEnabled && sharedData == 0) {
                              onSearchCell.emailInsteadOfPhoneNumber.text = email
                              onSearchCell.emailInsteadOfPhoneNumber.isHidden = false
                              onSearchCell.sepBtnSearchedNumAndEmail.isHidden = true
                              onSearchCell.stackViewDetails.isHidden = true
                        } else if(privacyEnabled && sharedData == 1) {
                              onSearchCell.emailInsteadOfPhoneNumber.text = phone
                              onSearchCell.emailInsteadOfPhoneNumber.isHidden = false
                              onSearchCell.sepBtnSearchedNumAndEmail.isHidden = true
                              onSearchCell.stackViewDetails.isHidden = true
                        } else {
                              onSearchCell.emailInsteadOfPhoneNumber.isHidden = true
                              onSearchCell.stackViewDetails.isHidden = false
                              onSearchCell.phoneNumberOfSearchedContact.text = phone
                              onSearchCell.emailOfSearchedContact.text = email
                              onSearchCell.sepBtnSearchedNumAndEmail.isHidden = false
                        }
                  } else {
                        onSearchCell.phoneNumberOfSearchedContact.text = "n/a"
                        onSearchCell.emailOfSearchedContact.text = "n/a"
                        onSearchCell.emailInsteadOfPhoneNumber.isHidden = true
                        onSearchCell.sepBtnSearchedNumAndEmail.isHidden = false
                        onSearchCell.stackViewDetails.isHidden = false
                  }
                  cell=onSearchCell
            } else if tableView == self.selectedContactTableView {
                  let onSelectCell = tableView.dequeueReusableCell(withIdentifier: "SelectedContactTableViewCell", for: indexPath) as! SelectedContactTableViewCell
                  onSelectCell.selectedContactTableViewCellDelegate = self
                  let contactObj = sectionData2[indexPath.section]![indexPath.row]
                  let privacyEnabled = contactObj.privacyEnabled ?? false
                  let sharedData = contactObj.sharedData ?? 2
                  let isBlocked = contactObj.isBlocked ?? false

                  onSelectCell.selectedContactIamge.layer.cornerRadius = onSelectCell.selectedContactIamge.frame.size.width / 2
                  onSelectCell.selectedContactIamge.clipsToBounds = true
                  var contactImageUrl=contactObj.contactImage
//                  onSelectCell.selectedContactIamge?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                  onSelectCell.selectedContactIamge?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))

                  onSelectCell.selectedContactEmail.text = contactObj.contactEmailId
                  onSelectCell.selectedContactPhoneNumber.text = contactObj.contactPhoneNumber
                  onSelectCell.selectedContactName.text = contactObj.contactName

                  if(contactObj.contactFEId != ""  && contactObj.contactFEId != " ") {
                        onSelectCell.isfisheyeIndicator.backgroundColor = UIColor.purpleRing()
                  } else {
                        onSelectCell.isfisheyeIndicator.backgroundColor = UIColor.clear
                  }

                  //if search contact name is empty then make it as unknown
                  if(contactObj.contactName == "" || contactObj.contactName == " " || contactObj.contactName == nil || contactObj.contactName.isEmpty) {
                        onSelectCell.selectedContactName.text = "Unknown"
                  } else {
                        onSelectCell.selectedContactName.text = contactObj.contactName
                  }

                  let phone = contactObj.contactPhoneNumber ?? ""
                  let email = contactObj.contactEmailId ?? ""

                  if(phone.isEmpty && !email.isEmpty) {
                        onSelectCell.stackViewDetails.isHidden = true
                        onSelectCell.emailInsteadOfPhoneNumber.isHidden = false
                        onSelectCell.emailInsteadOfPhoneNumber.text = email
                  } else if(!phone.isEmpty && email.isEmpty) {
                        onSelectCell.stackViewDetails.isHidden = true
                        onSelectCell.emailInsteadOfPhoneNumber.isHidden = false
                        onSelectCell.emailInsteadOfPhoneNumber.text = phone
                  } else if(!phone.isEmpty && !email.isEmpty) {
                        if(privacyEnabled && sharedData == 0) {
                              onSelectCell.emailInsteadOfPhoneNumber.text = email
                              onSelectCell.emailInsteadOfPhoneNumber.isHidden = false
                              onSelectCell.sepration.isHidden = true
                              onSelectCell.stackViewDetails.isHidden = true
                        } else if(privacyEnabled && sharedData == 1) {
                              onSelectCell.emailInsteadOfPhoneNumber.text = phone
                              onSelectCell.emailInsteadOfPhoneNumber.isHidden = false
                              onSelectCell.sepration.isHidden = true
                              onSelectCell.stackViewDetails.isHidden = true
                        } else {
                              onSelectCell.emailInsteadOfPhoneNumber.isHidden = true
                              onSelectCell.stackViewDetails.isHidden = false
                              onSelectCell.selectedContactPhoneNumber.text = phone
                              onSelectCell.selectedContactEmail.text = email
                              onSelectCell.sepration.isHidden = false
                        }
                  } else {
                        onSelectCell.selectedContactPhoneNumber.text = "n/a"
                        onSelectCell.selectedContactEmail.text = "n/a"
                        onSelectCell.emailInsteadOfPhoneNumber.isHidden = false
                        onSelectCell.sepration.isHidden = false
                        onSelectCell.stackViewDetails.isHidden = false
                  }
                  cell=onSelectCell
            }
            return cell!
      }

      func didRemoveContactTapped(_ sender: SelectedContactTableViewCell) {
            guard let indexPath=selectedContactTableView.indexPath(for: sender) else { return }
            var contData = ContactData()
            contData = sectionData2[indexPath.section]![indexPath.row]
            self.sectionData2[indexPath.section]?.remove(at: indexPath.row)
            if(indexPath.section==1) {
                  self.selectedPhoneContactArr=self.selectedPhoneContactArr.filter {$0.contactId != contData.contactId}
            } else {
                  self.selectedFisheyeContactArr=self.selectedFisheyeContactArr.filter {$0.contactId != contData.contactId}
            }
            self.selectedContactArray=self.selectedContactArray.filter {$0.contactId != contData.contactId}
            self.selectedContactIdArray=self.selectedContactIdArray.filter {$0 != contData.contactId}
            self.viewContactListBtn.isHidden=true
            self.showSelContactList.isHidden = true

            self.beforeTableSelectedContact()
            self.tablePopulatedOnSearch.isHidden=true
            self.contactArray=[]
            self.fishEyeContactArr = []
            self.phoneContactArr = []
            if(self.selectedContactArray.count>4 && self.selectedContactIdArray.count>4) {
                  self.selectedSingleContactView.isHidden=true
                  self.viewContactListBtn.isHidden=true
                  self.showSelContactList.isHidden = true
                  self.label1ToShowNoContactChoosen.isHidden=true
                  self.profileImageStackView.isHidden=false
                  self.isSelectedArrayCountEqualToFour()
                  self.profileImageViewFive.isHidden=false
                  self.profileViewFive.isHidden=false
            } else if(self.selectedContactIdArray.count>1 && self.selectedContactIdArray.count<=4) {
                  self.viewAboveNowView.isHidden=false
                  self.profileImageStackView.isHidden=false
                  self.profileImageOne.isHidden=false
                  var contactImageUrl1=self.selectedContactArray[0].contactImage
//                  self.profileImageViewOne?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                self.profileImageViewOne?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                  if(self.selectedContactIdArray.count==2 && self.selectedContactArray.count==2 ) {
                        self.isSelectedArrayCountEqualToTwo()
                  } else if(self.selectedContactIdArray.count==3 && self.selectedContactArray.count==3 ) {
                        self.isSelectedArrayCountEqualToThree()
                  } else if(self.selectedContactIdArray.count==4 && self.selectedContactArray.count==4 ) {
                        self.isSelectedArrayCountEqualToFour()
                  }
                  self.selectedSingleContactView.isHidden=true
                  self.profileImageViewFive.isHidden=true
                  self.profileViewFive.isHidden=true
                  self.viewContactListBtn.isHidden=true
                  self.showSelContactList.isHidden=true
                  self.label1ToShowNoContactChoosen.isHidden=true
            } else if(self.selectedContactIdArray.count==1 && self.selectedContactArray.count==1) {
                  self.isSelectedArrayCountEQualToOneThen()
            } else {
                  self.isSelectedArrayCountEqualToZero()
            }
      }

      func showDropdownBtn() {
            self.viewContactListBtn.isHidden = false
            self.showSelContactList.isHidden = false
            self.hideSelectedContactListBtn.isHidden = true
            self.hideSelContactList.isHidden = true
      }

      func showUpArraowBtn() {
            self.viewContactListBtn.isHidden = true
            self.showSelContactList.isHidden = true
            self.hideSelectedContactListBtn.isHidden = false
            self.hideSelContactList.isHidden = false
      }

      func getFishEyeContacts() {
            self.startLoader()
            /* get fisheye contacts */
            let fisheyeContacts: [ContactObj]
            fisheyeContacts = DatabaseManagement.shared.getUnblockedFEContacts()
            self.fisheyeContacts = []
            self.fisheyeContacts = fisheyeContacts
            self.fisheyeContacts.sort { $0.name < $1.name }
            /** End **/

            /* get non fisheye contacts */
            let nonFisheyeContacts: [ContactObj]
            nonFisheyeContacts = DatabaseManagement.shared.getUnblockedNonFEContacts()
            self.nonFisheyeContacts = []
            self.nonFisheyeContacts = nonFisheyeContacts
            self.nonFisheyeContacts.sort { $0.name < $1.name }
            /** End **/

            /* all contact list */
            self.allContactList  = []
            for i in 0..<fisheyeContacts.count {  // append nonfisheye contacts to fisheye contact list
                  self.allContactList.append(self.fisheyeContacts[i])
            }
            for i in 0..<nonFisheyeContacts.count {  // append nonfisheye contacts to fisheye contact list
                  self.allContactList.append(self.nonFisheyeContacts[i])
            }
            /*** End **/

            if(self.allContactList.count == 0) {
                  self.noContactFoundLabel.isHidden=false
            } else {
                  self.contactArray = []
                  self.fishEyeContactArr = []
                  self.phoneContactArr = []
                  for contactObj in self.allContactList {
                        var contData = ContactData()
                        var stringContactId: String = ""
                        stringContactId = String(contactObj.contactId)
                        contData.contactId = stringContactId
                        contData.contactName = contactObj.name
                        contData.contactEmailId = contactObj.email
                        contData.contactPhoneNumber = contactObj.phoneNumber
                        contData.contactImage = contactObj.picture
                        contData.contactFEId = contactObj.contactFEId
                        contData.privacyEnabled = contactObj.privacyEnabled
                        contData.isBlocked = contactObj.isBlocked
                        contData.sharedData = contactObj.sharedData

                        if(contactObj.contactFEId.isEmpty || contactObj.contactFEId == "" || contactObj.contactFEId == " " || contactObj.contactFEId == nil) {
                              self.phoneContactArr.append(contData)
                        } else {
                              self.fishEyeContactArr.append(contData)
                        }
                        self.contactArray.append(contData)
                  }

                  DispatchQueue.main.async {
                        self.stopLoader()
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "get contacts", action: "get contact list", label: "Contacts fetched successfully", value: 0)
                        self.beforeTablePopulatedOnSearch()
                  }
            }
      }

      func showToastMsg(toastMsg: String, toPulseListing: Bool) {
            DispatchQueue.main.async {
                  self.view.makeToast(toastMsg)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse, execute: {
                  if(toPulseListing) {
                        self.openPulseListingPage()
                  }
                  self.view.hideToast()
            })
      }

      //To send a pulse now,to choosen contacts,without master key and capsule
      func requestPulseNow() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
            let feedbackOperation = RequestAPulseNow(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.showToastMsg(toastMsg: self.pulseMsgs.nowPulseRequest, toPulseListing: true)
                  } else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      func sharePulseNow() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()

            let status = CLLocationManager.authorizationStatus()
        

            switch status {
            case .authorized, .authorizedWhenInUse, .authorizedAlways :
                  let feedbackOperation = ShareAPulseNow(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray)
                  feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                        self.stopLoader()
                        if(operation.statusCode == "200") {
                              self.showToastMsg(toastMsg: self.pulseMsgs.nowPulseShare, toPulseListing: true)
                        } else {
                              self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                        }
                  }
                  AppDelegate.addProcedure(operation: feedbackOperation)
            case .denied,.notDetermined:
                  
                  self.stopLoader()
                  let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                  })
                  alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                        alert.dismiss(animated: true, completion: nil)
                  })
                  present(alert, animated: true)
                  break
            default :
                  self.showToastMsg(toastMsg: self.pulseMsgs.unableToFetchLastLocation, toPulseListing: false)
                  self.stopLoader()
                  break
            }
      }

      //To request a pulse at scheduled time
      func requestPulseAtScheduled() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
        
        if schedulingEndDateLabel.text! == "dd/MM/yyyy" || !validationController.isValidMeetDate(givenString: schedulingEndDateLabel.text!) || schedulingEndDateLabel.text! == "End Date"{
            // end date is not entered
            let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray, startDateTimeStamp: validationController.getTimeStampFromStringDate(stringDate: startDate)*1000, endDateTimeStamp: 0.0, pulseTimeTimeStamp: validationController.getTimeStampFromStringDate(stringDate: pulseTime)*1000, repeatersArray: [], pulseType: "0")
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                self.stopLoader()
                if(operation.statusCode == "200") {
                    self.showToastMsg(toastMsg: self.pulseMsgs.scheduledPulseRequest, toPulseListing: true)
                } else {
                    self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
        } else {
            let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray, startDateTimeStamp: validationController.getTimeStampFromStringDate(stringDate: startDate)*1000, endDateTimeStamp: validationController.getTimeStampFromStringDate(stringDate: endDate)*1000, pulseTimeTimeStamp: validationController.getTimeStampFromStringDate(stringDate: pulseTime)*1000, repeatersArray: self.repeartersArray, pulseType: "0")
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                self.stopLoader()
                if(operation.statusCode == "200") {
                    self.showToastMsg(toastMsg: self.pulseMsgs.scheduledPulseRequest, toPulseListing: true)
                } else {
                    self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)

        }

      }

      //To request a pulse at scheduled time
      func sharePulseAtScheduled() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
            if schedulingEndDateLabel.text! == "dd/MM/yyyy" || !validationController.isValidMeetDate(givenString: schedulingEndDateLabel.text!) || schedulingEndDateLabel.text! == "End Date"{
                let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray, startDateTimeStamp: validationController.getTimeStampFromStringDate(stringDate: startDate)*1000, endDateTimeStamp: 0.0, pulseTimeTimeStamp: validationController.getTimeStampFromStringDate(stringDate: pulseTime)*1000, repeatersArray: [], pulseType: "1")

                feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                    self.stopLoader()
                    if(operation.statusCode == "200") {
                        self.showToastMsg(toastMsg: self.pulseMsgs.scheduledPulseShare, toPulseListing: true)
                    } else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                    }
                }
                AppDelegate.addProcedure(operation: feedbackOperation)
            } else {
                let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: false, message: "", recipientsIdArray: self.selectedContactIdArray, startDateTimeStamp: validationController.getTimeStampFromStringDate(stringDate: startDate)*1000, endDateTimeStamp: validationController.getTimeStampFromStringDate(stringDate: endDate)*1000, pulseTimeTimeStamp: validationController.getTimeStampFromStringDate(stringDate: pulseTime)*1000, repeatersArray: self.repeartersArray, pulseType: "1")

                feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                    self.stopLoader()
                    if(operation.statusCode == "200") {
                        self.showToastMsg(toastMsg: self.pulseMsgs.scheduledPulseShare, toPulseListing: true)
                    } else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                    }
                }
                AppDelegate.addProcedure(operation: feedbackOperation)
            }
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
            DispatchQueue.main.async {
                  self.ViewWithShadow.isUserInteractionEnabled=true
                ScreenLoader.shared.stopLoader()
            }
      }

      func  openInsertCapsulePopUp() {
            let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
            let  nextVC = storyboard.instantiateViewController(withIdentifier: "capsuleInsertPopUpViewController") as! capsuleInsertPopUpViewController
            nextVC.contactIdArray=self.selectedContactIdArray
            nextVC.scheduledOrNow=self.scheduledOrNow
            if(scheduledOrNow=="Scheduled") {
               
                
                  nextVC.pulseTime=validationController.getTimeStampFromStringDate(stringDate: pulseTime)*1000
                  nextVC.pulseStartDate=validationController.getTimeStampFromStringDate(stringDate: startDate)*1000
               
                if(endDate == "") {
                    nextVC.pulseEndDate=0.0
                    nextVC.pulseRepeatersArray=[]
                } else {
                    nextVC.pulseEndDate=validationController.getTimeStampFromStringDate(stringDate: endDate)*1000
                    nextVC.pulseRepeatersArray=self.repeartersArray
                }
            }
            if(self.reachedToEditPulse) {
                  nextVC.pulseToBeUpdated=self.pulseDetailsNeedToBeEdited.pulseId
                  nextVC.reachedToEditPulseFromSR=true
                  nextVC.capsuleToBeEdited=self.pulseDetailsNeedToBeEdited.message
            }
            nextVC.pulseType=self.pulseType
            self.present(nextVC, animated: true, completion: nil)
      }

      func searchContactClick() {
            self.addContactLabel.textColor=UIColor.unSelectedTextColor()
            self.noContactFoundLabel.isHidden=true
            self.seachBarForContact.isHidden=false
            self.seachBarForContact.setShowsCancelButton(true, animated: true)
            self.seachBarForContact.becomeFirstResponder()
            self.seachBarForContact.delegate = self
      }

      func openPulseListingPage() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "PulseListingViewController")  as! PulseListingViewController
            nextVC.view.frame = CGRect.init(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height) //self.view.bounds
            nextVC.view.clipsToBounds = true
            nextVC.view.isUserInteractionEnabled = true
        self.addChild(nextVC)
        nextVC.didMove(toParent: self)
            self.view.addSubview(nextVC.view)
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  nextVC.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
      }

      func applyViewShadow() {
            self.ViewWithShadow.layer.cornerRadius = 8
            self.ViewWithShadow.layer.borderWidth = 0
            self.ViewWithShadow.layer.borderColor = UIColor.black.cgColor
            self.ViewWithShadow.layer.shadowColor = UIColor.black.cgColor
            self.ViewWithShadow.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.ViewWithShadow.layer.shadowOpacity = 0.7
            self.ViewWithShadow.layer.shadowRadius = 10.0

            self.profileImageViewOne.layer.cornerRadius = self.profileImageViewOne.frame.size.width / 2
            self.profileImageViewOne.clipsToBounds = true
            // self.profileImageViewOne.layer.borderWidth = 1.5
            self.profileImageViewTwo.layer.cornerRadius = self.profileImageViewTwo.frame.size.width / 2
            self.profileImageViewTwo.clipsToBounds = true
            //self.profileImageViewTwo.layer.borderWidth = 1.5
            self.profileImageViewThree.layer.cornerRadius = self.profileImageViewThree.frame.size.width / 2
            self.profileImageViewThree.clipsToBounds = true
            // self.profileImageViewThree.layer.borderWidth = 1.5
            self.profileImageViewFour.layer.cornerRadius = self.profileImageViewFour.frame.size.width / 2
            self.profileImageViewFour.clipsToBounds = true
            //self.profileImageViewFour.layer.borderWidth = 1.5
            self.selectedSingleContactImage.layer.cornerRadius = self.selectedSingleContactImage.frame.size.width / 2
            self.selectedSingleContactImage.clipsToBounds = true
            //self.selectedSingleContactImage.layer.borderWidth = 1.5
      }

      /******************************************Scheduling related Functions*****************************************/

      func watchButtonClicked() {
            self.schedulingTimeLabel.textColor=UIColor(red: 11.0/255.0, green: 11.0/255.0, blue: 11.0/255.0, alpha: 0.48)
            DatePickerDialog().show(self.pulseMsgs.timePickerTitle, doneButtonTitle: self.pulseMsgs.pickerDone, cancelButtonTitle: self.pulseMsgs.pickerCancel, datePickerMode: .time) {
                  (date) -> Void in
                  if let dt = date {
                        let formatterForEndtime = DateFormatter()
                        formatterForEndtime.dateFormat = "HH:mm"
                        self.schedulingTimeLabel.text = formatterForEndtime.string(from: dt)
                        self.pulseTime="\(dt)"
                  }
            }
      }

      func calenderButtonClicked() {
            self.schedulingDateLabel.textColor=UIColor(red: 11.0/255.0, green: 11.0/255.0, blue: 11.0/255.0, alpha: 0.48)
            DatePickerDialog().show(self.pulseMsgs.startDatePickerTitle, doneButtonTitle: self.pulseMsgs.pickerDone, cancelButtonTitle: self.pulseMsgs.pickerCancel, minimumDate: Date(), datePickerMode: .date) {
                  (date) -> Void in
                  if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd/MM/YYYY"
                        self.schedulingDateLabel.text = formatter.string(from: dt)
                    
                  }
            }
      }

      func endDateForSchedular() {
            self.schedulingEndDateLabel.textColor=UIColor(red: 11.0/255.0, green: 11.0/255.0, blue: 11.0/255.0, alpha: 0.48)
            DatePickerDialog().show(self.pulseMsgs.endDatePickerTitle, doneButtonTitle: self.pulseMsgs.pickerDone, cancelButtonTitle: self.pulseMsgs.pickerCancel, minimumDate: Date(), datePickerMode: .date) {
                  (date) -> Void in
                  if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd/MM/YYYY"
                        self.schedulingEndDateLabel.text = formatter.string(from: dt)
                    
                  }
            }
      }

      func checkforProperScheduling(fromNextOrInsert: String) {
            if(self.scheduledOrNow=="Scheduled") {
                  var validStartDate=false
                  var validEndDate=false
                  var validStartAndEndDate=false
                  var invalidPulseTime=false

                  let now = Date()
                  let formatter = DateFormatter()
                  formatter.timeZone = TimeZone.current
                  formatter.dateFormat = "dd-MM-yyyy HH:mm"
                  let dateString = formatter.string(from: now)
                let fiveCharacters = dateString.suffix(5)

                  if self.schedulingTimeLabel.text! == "HH:mm" || !validationController.isValidMeetTime(givenString: schedulingTimeLabel.text!) || self.schedulingTimeLabel.text! == "hh:mm"{
                        self.schedulingTimeLabel.textColor = UIColor.red
                  }

                  //start date validation
                  if  schedulingDateLabel.text! == "dd/MM/yyyy" || !validationController.isValidMeetDate(givenString: schedulingDateLabel.text!) || schedulingDateLabel.text! == "Start Date"{
                        self.schedulingDateLabel.textColor = UIColor.red
                        startDateinvalidtest = "invalid date"
                  } else {
                        startDatetest = "valid startdate"
                        validStartDate=true
                  }

                  //end date validation
                  if schedulingEndDateLabel.text! == "dd/MM/yyyy" || !validationController.isValidMeetDate(givenString: schedulingEndDateLabel.text!) || schedulingEndDateLabel.text! == "End Date"{
                    //end date is not entered
                        //self.schedulingEndDateLabel.textColor = UIColor.red
                        EndDateinvalidtest = "Invalid enddate"
                  } else {
                    //end date is entered
                        endDatetest = "valid date"
                        validEndDate=true
//                    repeaters array validation
                      if self.repeartersArray==[] {
                            self.repeatButton.setTitleColor(UIColor.red, for: .normal)
                      }
                  }

                //validate only start date,end date is not entered
                if(validStartDate && !validEndDate) {
                    let tempStartDate = "\(schedulingTimeLabel.text!)" + " " + "\(schedulingDateLabel.text!)"
                   
                    let tempPulseTime = "\(schedulingTimeLabel.text!)" + " " + "02/12/1994"
                    let tempNowTime = fiveCharacters + " " + "02/12/1994"

                    startDate = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempStartDate) )

                    nowTime = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempNowTime) )

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
                    dateFormatter.timeZone = NSTimeZone.local

                    startDateUTC = dateFormatter.date(from: startDate)!
                    nowTimeUTC = dateFormatter.date(from: nowTime)!
                    
                    if schedulingTimeLabel.text! != "hh:mm"{
                        //self.schedulingTimeLabel.textColor = UIColor.red
                        pulseTime=validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempPulseTime) )
                        pulseTimeUTC = dateFormatter.date(from: pulseTime)!
                    }

                    if(pulseTimeUTC != nil) {
                        if (pulseTimeUTC! < nowTimeUTC!) {
                            self.schedulingTimeLabel.textColor = UIColor.red
                            invalidPulseTime=true
                        } else {
                            self.schedulingTimeLabel.textColor = UIColor(red: 11.0/255.0, green: 11.0/255.0, blue: 11.0/255.0, alpha: 0.48)
                            invalidPulseTime=false
                        }
                    } else {
                        invalidPulseTime=false
                    }

                    if schedulingTimeLabel.text! != "HH:mm" &&  schedulingDateLabel.text! != "dd/MM/yyyy" && schedulingDateLabel.text! != "Start Date" && schedulingTimeLabel.text! != "hh:mm" && !invalidPulseTime {
                        self.isScheduledProperly=true
                    } else {
                        self.isScheduledProperly=false
                    }

                }

                  //start date and end date validation
                  if(validStartDate && validEndDate) {
                    if self.repeartersArray==[] {
                        self.repeatButton.setTitleColor(UIColor.red, for: .normal)
                    }
                        let tempStartDate = "\(schedulingTimeLabel.text!)" + " " + "\(schedulingDateLabel.text!)"
                        let tempEndDate = "\(schedulingTimeLabel.text!)" + " " + "\(schedulingEndDateLabel.text!)"
                        let tempPulseTime = "\(schedulingTimeLabel.text!)" + " " + "02/12/1994"
                        let tempNowTime = fiveCharacters + " " + "02/12/1994"

                        startDate = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempStartDate) )

                        endDate = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempEndDate) )

                        nowTime = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempNowTime) )

                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
                        dateFormatter.timeZone = NSTimeZone.local
                        endDateUTC = dateFormatter.date(from: endDate)!
                        startDateUTC = dateFormatter.date(from: startDate)!
                        nowTimeUTC = dateFormatter.date(from: nowTime)!
                    
                    

                        if schedulingTimeLabel.text! != "hh:mm"{
                              //self.schedulingTimeLabel.textColor = UIColor.red
                              pulseTime=validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempPulseTime) )
                              pulseTimeUTC = dateFormatter.date(from: pulseTime)!
                        }

                        if (startDateUTC! > endDateUTC!) {
                              self.schedulingEndDateLabel.textColor = UIColor.red
                              validStartAndEndDate=false
                        } else {
                              //self.schedulingEndDateLabel.textColor = UIColor.black
                              if((startDate.suffix(10)==endDate.suffix(10)) && (dateString.prefix(10)==endDate.suffix(10))) {
                                    if(pulseTimeUTC != nil) {
                                          if (pulseTimeUTC! < nowTimeUTC!) {
                                                self.schedulingTimeLabel.textColor = UIColor.red
                                                invalidPulseTime=true
                                          } else {
                                                self.schedulingTimeLabel.textColor = UIColor(red: 11.0/255.0, green: 11.0/255.0, blue: 11.0/255.0, alpha: 0.48)
                                                invalidPulseTime=false
                                          }
                                    } else {
                                          invalidPulseTime=false
                                    }
                              }
                              validStartAndEndDate=true
                        }
                    if schedulingTimeLabel.text! != "HH:mm" &&  schedulingDateLabel.text! != "dd/MM/yyyy" && schedulingEndDateLabel.text! != "dd/MM/yyyy" && schedulingDateLabel.text! != "Start Date" && schedulingEndDateLabel.text! != "End Date" && schedulingTimeLabel.text! != "hh:mm" && self.repeartersArray != [] && validStartAndEndDate && !invalidPulseTime {
                        self.isScheduledProperly=true
                    } else {
                        self.isScheduledProperly=false
                    }
                  }

//                  if schedulingTimeLabel.text! != "HH:mm" &&  schedulingDateLabel.text! != "dd/MM/yyyy" && schedulingEndDateLabel.text! != "dd/MM/yyyy" && schedulingDateLabel.text! != "Start Date" && schedulingEndDateLabel.text! != "End Date" && schedulingTimeLabel.text! != "hh:mm" && self.repeartersArray != [] && validStartAndEndDate && !invalidPulseTime{
//                        self.isScheduledProperly=true
//                  }else{
//                        self.isScheduledProperly=false
//                  }

            }

            if(fromNextOrInsert=="fromInsert") {
                  if((self.selectedContactArray.count>0) && (self.scheduledOrNow=="Now")) {
                        self.openInsertCapsulePopUp()
                  }
                  if((self.selectedContactArray.count>0) && (self.scheduledOrNow=="Scheduled") && (self.isScheduledProperly)) {
                        self.openInsertCapsulePopUp()
                  }
            } else {
                  // if((self.selectedContactArray.count>0) && (self.scheduledOrNow=="Scheduled") && (self.isScheduledProperly)){
                  self.sendPulseAccordingToSelectedParameters()
                  //  }
            }

      }

      /******************************************* Helping Functions *********************************************/

      func unSelectRepeater() {
        self.repeatButton.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
            self.repeartersArray=[]
        self.sundayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
        self.mondayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
        self.tuesdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
        self.wensdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
        self.ThuresdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
        self.fridayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
        self.saturdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
      }

      func editPulseData() {
        DispatchQueue.main.async {
            self.nowView.isUserInteractionEnabled=false
            if(self.reachedToEditPulse && self.pulseDetailsNeedToBeEdited.isPulsed) {
                  //showing pulse type according to data from pulse details
                  if(self.pulseDetailsNeedToBeEdited.pulseType=="0") {
                        self.pulseTypeRequestChoosen()
                  } else {
                        self.pulseTypeShareChoosen()
                  }
                  //shoeing  now or scheduled according to data from pulse deatils
                  if(self.pulseDetailsNeedToBeEdited.isScheduled) {
                        self.schedulingOptionSelected()
                        //showing scheduled data set while creating a pulse that need to be updated
                        self.scheduleHereView.isUserInteractionEnabled=true
                    self.schedulingDateLabel.text=self.validationController.convert_date_from_UTC_time(givenUTCdate: self.pulseDetailsNeedToBeEdited.pulseStartDate!)
                        self.schedulingTimeLabel.text=self.validationController.convert_time_from_UTC_time(givenUTCtime: self.pulseDetailsNeedToBeEdited.pulseTime!)
                    
                    let oldDate=self.validationController.convert_date_from_UTC_time(givenUTCdate: self.pulseDetailsNeedToBeEdited.pulseEndDate!)
                        if(oldDate == "01/01/1970") {
                            self.schedulingEndDateLabel.text="End Date"
                        } else {
                            self.schedulingEndDateLabel.text=oldDate
                            if(self.pulseDetailsNeedToBeEdited.pulseRepeatersForDays.count>0) {
                                self.repeartersArray=self.pulseDetailsNeedToBeEdited.pulseRepeatersForDays
                                self.repeatButton.setTitleColor(UIColor.selectedTextColor(), for: .normal)
                                self.daysStackView.isUserInteractionEnabled=true
                                for repeatOnDay in self.repeartersArray {
                                    self.dayOfAweekFromPulseDeatilsOnShareRequest(dayOfWeekArg: repeatOnDay)
                                }
                            }
                        }
                  } else {
                        self.nowOptionSelected()
                  }
                  //showing previously choosen recipients
                  for responderData in self.pulseDetailsNeedToBeEdited.respondersArray {
                        var contactObj = ContactData()
                        contactObj.contactImage=responderData.responderImage
                        contactObj.contactName=responderData.responderName
                        contactObj.contactId=responderData.contactId
                        contactObj.contactPhoneNumber=responderData.responderPhoneNumber
                        contactObj.contactEmailId=responderData.responderEmail
                        if(responderData.isFisheye) {
                              contactObj.contactFEId = responderData.contactId
                            
                              self.selectedFisheyeContactArr.append(contactObj)
                        } else {
                              contactObj.contactFEId = " "
                              self.selectedPhoneContactArr.append(contactObj)
                        }
                        self.selectedContactArray.append(contactObj)
                        self.selectedContactIdArray.append(contactObj.contactId)
                  }

                self.theseCotactsAreChoosen((Any).self)

                  if(self.pulseDetailsNeedToBeEdited.message != "") {
                        self.insertCapsuleButton.setTitle(self.pulseMsgs.editMessage, for: .normal)
                  } else {

                  }
            }
       
      }
    }

      func updateScheduledPulse(pulseId: String) {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
        let capsuleMessage=self.pulseDetailsNeedToBeEdited.message
        let isProtected=self.pulseDetailsNeedToBeEdited.isEncrypted
            let feedbackOperation = UpdatePulseData(isEncrypted: isProtected!, message: capsuleMessage!, recipientsIdArray: self.selectedContactIdArray, toUpdatePulseId: pulseId, pulseTime: validationController.getTimeStampFromStringDate(stringDate: pulseTime)*1000, pulseStartDate: validationController.getTimeStampFromStringDate(stringDate: startDate)*1000, pulseEndDate: validationController.getTimeStampFromStringDate(stringDate: endDate)*1000, pulseDayRepeaters: self.repeartersArray, pulseType: self.pulseType!)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.showToastMsg(toastMsg: self.pulseMsgs.pulseUpdated, toPulseListing: true)
                  } else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      func dayOfAweekFromPulseDeatilsOnShareRequest(dayOfWeekArg: String) {
            switch (dayOfWeekArg) {
            case "Sun":
                self.sundayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  break
            case "Mon":
                self.mondayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  break
            case "Tue":
                self.tuesdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  break
            case "Wed":
                self.wensdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  break
            case "Thu":
                self.ThuresdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  break
            case "Fri":
                self.fridayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  break
            case "Sat":
                self.saturdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  break
            default:
                  break

            }
      }

      func addOrRemoveDayAccIndex(indexOfDay: Int) {
            switch indexOfDay {
            case 0:
                  if(self.repeartersArray.contains("Sun")) {
                        var index=self.repeartersArray.index(of: "Sun")
                        self.repeartersArray.remove(at: index!)
                    self.sundayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
                  } else {
                        self.repeartersArray.append("Sun")
                    self.sundayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  }
            case 1:
                  if(self.repeartersArray.contains("Mon")) {
                        var index=self.repeartersArray.index(of: "Mon")
                        self.repeartersArray.remove(at: index!)
                    self.mondayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
                  } else {
                        self.repeartersArray.append("Mon")
                    self.mondayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  }
            case 2:
                  if(self.repeartersArray.contains("Tue")) {
                        var index=self.repeartersArray.index(of: "Tue")
                        self.repeartersArray.remove(at: index!)
                    self.tuesdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
                  } else {
                        self.repeartersArray.append("Tue")
                    self.tuesdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  }
            case 3:
                  if(self.repeartersArray.contains("Wed")) {
                        var index=self.repeartersArray.index(of: "Wed")
                        self.repeartersArray.remove(at: index!)
                    self.wensdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
                  } else {
                        self.repeartersArray.append("Wed")
                    self.wensdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  }
            case 4:
                  if(self.repeartersArray.contains("Thu")) {
                        var index=self.repeartersArray.index(of: "Thu")
                        self.repeartersArray.remove(at: index!)
                    self.ThuresdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
                  } else {
                        self.repeartersArray.append("Thu")
                    self.ThuresdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  }
            case 5:
                  if(self.repeartersArray.contains("Fri")) {
                        var index=self.repeartersArray.index(of: "Fri")
                        self.repeartersArray.remove(at: index!)
                    self.fridayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
                  } else {
                        self.repeartersArray.append("Fri")
                    self.fridayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  }
            case 6:
                  if(self.repeartersArray.contains("Sat")) {
                        var index=self.repeartersArray.index(of: "Sat")
                        self.repeartersArray.remove(at: index!)
                    self.saturdayBtn.setTitleColor(UIColor.unSelectedTextColor(), for: UIControl.State.normal)
                  } else {
                        self.repeartersArray.append("Sat")
                    self.saturdayBtn.setTitleColor(UIColor.selectedTextColor(), for: UIControl.State.normal)
                  }
            default:
                  break
            }
      }

}
