//
//  PulseSearchSelectedViewController.swift
//  fisheye
//
//  Created by venkatesh murthy on 17/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class PulseSearchSelectedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var phoneNumberTableView: UITableView!
    @IBOutlet weak var emailTableView: UITableView!
    @IBOutlet weak var emaildIDBtn: UIButton!
    @IBOutlet weak var mobileBtn: UIButton!
    @IBAction func changeClick(_ sender: Any) {

        if((!selectedEmailID.isEmpty) || (!selectedPhonenumber.isEmpty)) {
        print("\(selectedEmailID) \(selectedPhonenumber)")
        let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
        let profileBaseviewController = storyboard.instantiateViewController(withIdentifier: "PulseRequestViewController") as! PulseRequestViewController

        profileBaseviewController.selectedEmailID = selectedEmailID
        profileBaseviewController.selectedPhoneNumber = selectedPhonenumber
        profileBaseviewController.selectedName = primaryNameLabel.text!
        //        profileBaseviewController.contatcs = self.searchContatcs?[indexPath.row]
        //        profileBaseviewController.primaryName = self.searchContatcs?[indexPath.row].name
        profileBaseviewController.view.frame = self.view.bounds
        self.addChildViewController(profileBaseviewController)
        profileBaseviewController.didMove(toParentViewController: self)

        //        //  profileBaseviewController.delegate = self
        //
        profileBaseviewController.view.frame.origin.y = -self.view.frame.size.height
        //
        //

        self.view.addSubview(profileBaseviewController.view)
        //
        //
        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: { _ in
            profileBaseviewController.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
        } else {

        }

    }
    var contatcs: FEContact?

    var emailorphone: Bool = false

    var selectedPhonenumber: String = ""

    var selectedEmailID: String = ""

    @IBAction func emailIDClick(_ sender: Any) {
        emailorphone = true
        emailTableView.reloadData()
        mobileBtn.backgroundColor = UIColor.white
        emaildIDBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
        emaildIDBtn.titleLabel?.textColor = UIColor.white
        mobileBtn.titleLabel?.textColor = UIColor.black

        phoneNumberTableView.isHidden = true
        emailTableView.isHidden = false

    }
    @IBAction func mobileClick(_ sender: Any) {
        emailorphone = false
        phoneNumberTableView.reloadData()
        emaildIDBtn.backgroundColor = UIColor.white
        mobileBtn.titleLabel?.textColor = UIColor.white
        emaildIDBtn.titleLabel?.textColor = UIColor.black

        mobileBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")

        emailTableView.isHidden = true
        phoneNumberTableView.isHidden = false
    }
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var primaryNameLabel: UILabel!

    @IBOutlet weak var primarylabelView: UIView!
    var primaryName: String?

    @IBAction func cancelClick(_ sender: Any) {
        //  self.dismiss(animated: true, completion: nil)

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    @IBOutlet weak var whiteview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        applyViewShadow()
        DispatchQueue.main.async {
            self.emaildIDBtn.titleLabel?.textColor = UIColor.black
        }
        primaryNameLabel.text = primaryName

        mobileBtn.sendActions(for: UIControlEvents.touchUpInside)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func applyViewShadow() {

        self.whiteview.layer.cornerRadius = 10.0
        self.primarylabelView.layer.cornerRadius = 10.0

        self.bottomView.layer.cornerRadius = 10.0

        //
        //        // border
        //        self.mainview.layer.borderWidth = 0
        //        self.mainview.layer.borderColor = UIColor.black.cgColor
        //        //        self.mainview.clipsToBounds = true
        //
        //        // shadow
                self.bottomView.layer.shadowColor = UIColor.black.cgColor
                self.bottomView.layer.shadowOffset = CGSize(width: 3, height: 3)
                self.bottomView.layer.shadowOpacity = 0.7
               self.bottomView.layer.shadowRadius = 10.0

         self.bottomView.layer.borderWidth = 1
        // self.whiteview.layer.borderColor = UIColor.black.cgColor
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0

        if(emailorphone == false) {
            if( (self.contatcs?.secondaryPhoneNumbers != nil) && (self.contatcs?.secondaryPhoneNumbers?.count)!>0) {
                count  = (self.contatcs?.secondaryPhoneNumbers?.count)!
            }
        } else {
            if((self.contatcs?.secondaryEmails != nil) && (self.contatcs?.secondaryEmails?.count)!>0) {
                count  = (self.contatcs?.secondaryEmails?.count)!
            }
        }

        if(self.contatcs?.phoneNumber != nil) {
            count = count + 1
        }

        return count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag = (tableView.cellForRow(at: indexPath)?.tag)

        //self.contatcs?.selectedCell = true

        if(tableView == emailTableView) {
         if(self.contatcs?.email != nil) {
            selectedEmailID = (self.contatcs?.email)!
            self.emailTableView.reloadData()
            }
        } else {

            if(self.contatcs?.phoneNumber != nil) {
            selectedPhonenumber = (self.contatcs?.phoneNumber)!
            self.phoneNumberTableView.reloadData()
            }
        }

        //        if(tag == 1)
        //        {
        //           // print("\(tag) \(self.contatcs?.email)")
        //
        //            if(self.contatcs?.email != nil)
        //            {
        //            selectedEmailID = (self.contatcs?.email)!
        //            }
        //
        //        self.tableview.reloadData()
        //        }else{
        //            //print("\(tag) \(self.contatcs?.phoneNumber)")
        //
        //            if(self.contatcs?.phoneNumber != nil)
        //            {
        //            selectedPhonenumber = c
        //            }
        //
        //            self.tableview.reloadData()
        //        }

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let tag = (tableView.cellForRow(at: indexPath)?.tag)

        if(tableView == emailTableView) {
            let cell = self.emailTableView.dequeueReusableCell(withIdentifier: "PulseSearchSelectedTableViewCell", for: indexPath)as! PulseSearchSelectedTableViewCell
            cell.phoneoremailLabel.text = self.contatcs?.email
            if(selectedEmailID == cell.phoneoremailLabel.text) {
                cell.selectedRadioBtn.isSelected = true
            } else {
                cell.selectedRadioBtn.isSelected = false

            }

            return cell

        } else {
            let cell = self.phoneNumberTableView.dequeueReusableCell(withIdentifier: "PulseSearchSelectedTableViewCell", for: indexPath)as! PulseSearchSelectedTableViewCell
            cell.phoneoremailLabel.text = self.contatcs?.phoneNumber
            if(selectedPhonenumber == cell.phoneoremailLabel.text) {
                cell.selectedRadioBtn.isSelected = true
            } else {
                cell.selectedRadioBtn.isSelected = false
            }
            return cell
        }

        //        if(self.contatcs?.selectedCell)!
        //        {
        //            cell.selectionStyle = .gray
        //
        //        }else{
        //            cell.selectionStyle = .none
        //        }

        //        if(emailorphone)
        //        {
        //
        //            let emailIDcell = self.tableview.dequeueReusableCell(withIdentifier: "PulseSearchSelectedTableViewCell", for: indexPath)as! PulseSearchSelectedTableViewCell
        //            emailIDcell.phoneoremailLabel.text = self.contatcs?.email
        //            emailIDcell.tag = 1
        //
        //            if(selectedEmailID == emailIDcell.phoneoremailLabel.text)
        //            {
        //                emailIDcell.selectionStyle = .gray
        //            }else
        //            {
        //                emailIDcell.selectionStyle = .none
        //
        //            }
        //            return emailIDcell
        //
        //        }else{
        //        let emailIDcell = self.tableview.dequeueReusableCell(withIdentifier: "PulseSearchSelectedTableViewCell", for: indexPath)as! PulseSearchSelectedTableViewCell
        //            emailIDcell.phoneoremailLabel.text = self.contatcs?.phoneNumber
        //            emailIDcell.tag = 2
        //
        //            if(selectedPhonenumber == emailIDcell.phoneoremailLabel.text)
        //            {
        //                emailIDcell.selectionStyle = .gray
        //                emailIDcell.selectedRadioBtn.isSelected = true
        //            }else
        //            {
        //                emailIDcell.selectionStyle = .none
        //                emailIDcell.selectedRadioBtn.isSelected = false
        //
        //
        //            }
        //            return emailIDcell
        //        }

        //  return cell

    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
