//
//  MasterKeyPopUpViewController.swift
//  fisheye
//
//  Created by Anuradha on 12/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import CoreLocation
import Toast_Swift

class MasterKeyPopUpViewController: UIViewController, CLLocationManagerDelegate {

      /******************************************* Outlet variables *******************************************/

      @IBOutlet weak var popUpView: UIView!
      @IBOutlet weak var underlineAfterUpperView: UIView!
      @IBOutlet weak var upperViewOfMasterKeyPopUp: UIView!
      @IBOutlet weak var enterMasterKeyLabel: UILabel!
      @IBOutlet weak var youMayNotProceedLabel: UILabel!
      @IBOutlet weak var masterKeyText: UICustomTextField!
      @IBOutlet weak var blackUnderLine: UIImageView!
      @IBOutlet weak var blueUnderLine: UIImageView!
      @IBOutlet weak var baseView: UIView!
      @IBOutlet weak var backButtonIconOfMasterKeyPopUp: UIImageView!
      @IBOutlet weak var backButtonOfMasterKeyPopUp: UIButton!
      @IBOutlet weak var closeButtonOnMasterKeyPopUp: UIButton!
      @IBOutlet weak var closeIconOnMasterKeyPo: UIImageView!
      @IBOutlet weak var goButton: UIButton!

      /********************************************* globle variables ***************************************/

      var contactIdArray=[String]()
      var capsuleMessage: String?
      var idtoken: String?
      var pulseType: String?
      var isMasterKeyVerifiedSuccefully: Bool = false
      var appService = AppService()
      let networkStatus = NetworkStatus()
      let appSharedPrefernce = AppSharedPreference()
      var pulseMsgs = PulseToastMsgHeadingSubheadingLabels()
      var reloadProtocol: RedirectedToMeetmeLogbookVC?
      var scheduledOrNow: String?
      var pulseTime: Double = 0.0
      var pulseStartDate: Double = 0.0
      var pulseEndDate: Double = 0.0
      var pulseRepeatersArray=[String]()
      var fromAcceptPulse: Bool = false
      var fromMeetMeForCapsuleLock = false
      var fromMeetMeForCapsuleOpening = false
      let appDelegete = UIApplication.shared.delegate as! AppDelegate
      var acceptThisPulse: String!
      var respondToId: String!
      var respondToPulseIdIsScheduled: Bool = false
      var deletePulseProtocol: DeletePulseProtocol?
      var insertCapsuleProtocol: InsertCapsuleProtocol?
      var pulseListingProtocol: PulseListingProtocol?
      var fromListingWhileAccepting: Bool = false
      var fromDetailsWhileAccepting: Bool = false
      var fromUnlockCapsule: Bool = false
      var fromUnlockCapsuleofDetails: Bool = false
      var unlockCapsuleOfType: String!
      var acceptThisPulseObj = PulseDetails()
      var pulseToBeUpdated: String = ""
      var reachedToEditPulseFromSR: Bool = false
      var placeHolderOne = ""
      var placeHolderTwo = ""
      var sendAPulseToFromSync: String = ""
      var pulseTypeFromSync: Int = 0
      var isPulseNeedToSendFromSync: Bool = false
      var dismissCapsulePopUp: DismissCapsulePopUpToShowSync?
      var isFromCapsuleChatNotification: Bool = false
      var isCapsuleLocked: Bool = false
      var notificationMeetMeId = ""

    
      /***************************************** IB Actions **********************************************/

      @IBAction func closeButtonOnMasterKeyClicked(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      @IBAction func backBtnOnMasterKeyPopup(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      @IBAction func goToNextBtn(_ sender: Any) {
            self.validationCheck(PulsemasterKey: masterKeyText.text! )
            self.actionAccToKeyVerifiedOfNot(verifyMasterKey: masterKeyText.text!)
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.enterMasterKeyLabel.text = self.pulseMsgs.enterMasterKeyTitle
                  self.masterKeyText.placeholder = self.pulseMsgs.configOnErrorClearedMasterKey
                  self.youMayNotProceedLabel.text = self.pulseMsgs.warningToEnterMasterKey
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*********************************** predefined functions *****************************************/

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            self.pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
            self.setLocalizationText()
            self.addObservers()

            self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

            if(self.fromAcceptPulse || self.isPulseNeedToSendFromSync || self.fromUnlockCapsule || self.fromUnlockCapsuleofDetails
                  //meet me conditions
                  || self.fromMeetMeForCapsuleLock || self.fromMeetMeForCapsuleOpening) {
                  self.backButtonOfMasterKeyPopUp.isHidden=true
                  self.backButtonIconOfMasterKeyPopUp.isHidden=true
            } else {
                  self.closeIconOnMasterKeyPo.isHidden=true
                  self.closeButtonOnMasterKeyPopUp.isHidden=true
            }

            NotificationCenter.default.addObserver(forName: DISMISS_LOADER_WHEN_LOCATION_SETTING_OPENED, object: nil, queue: nil) { (_) in
                  self.stopLoader()
            }

            self.popUpView.layer.cornerRadius=5
            self.blackUnderLine.isHidden = false
            self.blueUnderLine.isHidden = true
            self.onViewLoadToBeShowAs()
        
//            if(self.isPulseNeedToSendFromSync){
//                self.contactIdArray=[]
//                self.contactIdArray.append(self.sendAPulseToFromSync)
//            }
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.MasterKeyPopUpViewController)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      /***************************************** text fielf related functions *********************************/

      func onViewLoadToBeShowAs() {
            self.masterKeyText.isSecureTextEntry = true
            self.masterKeyText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.masterKeyText.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
      }

      func validationCheck(PulsemasterKey: String) {
            var masterKey = PulsemasterKey.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if (masterKey.characters.count) <= 0 {
                  self.masterKeyText.configOnError(withPlaceHolder: self.pulseMsgs.configOnErrorEnterMasterKey)
                  placeHolderOne = masterKeyText.placeholder!
            } else {

            }
      }

      //Code for checking validation while the user edits TextFields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {
            self.blackUnderLine.isHidden = true
            self.blueUnderLine.isHidden = false
            if (self.masterKeyText.text?.characters.count) != 4 {
                  if (self.masterKeyText.text?.characters.count)! <= 0 {
                        self.masterKeyText.configOnError(withPlaceHolder: self.pulseMsgs.configOnErrorEnterMasterKey)
                  } else {
                        self.masterKeyText.configOnError(withPlaceHolder: self.pulseMsgs.configOnErrorInvalidMasterKey)
                        if((masterKeyText.text?.characters.count)! > 4) {
                              masterKeyText.text = String((masterKeyText.text?.dropLast())!)
                              self.masterKeyText.configOnErrorCleared(withPlaceHolder: self.pulseMsgs.configOnErrorClearedMasterKey)
                        }
                  }
            } else {
                  self.masterKeyText.configOnErrorCleared(withPlaceHolder: self.pulseMsgs.configOnErrorClearedMasterKey)
            }
      }

      //Code for checking validation when the user is done editing TextFields
    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            self.blackUnderLine.isHidden = false
            self.blueUnderLine.isHidden = true
      }

      /***************************************** Alearts(Pop-up/toast) functions********************************/

      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
            self.present(nextVC, animated: true, completion: nil)
      }

      func showToastMsgOnMasterKeyScreen(toastMsg: String, toPulseListing: Bool) {
            DispatchQueue.main.async {
                  self.view.makeToast(toastMsg)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse, execute: {
                  if(toPulseListing && !self.isPulseNeedToSendFromSync) {
                        self.openPulseListingPage()
                  }else if(self.isPulseNeedToSendFromSync && self.isMasterKeyVerifiedSuccefully){
                    self.dismiss(animated: true, completion: {
                        self.dismissCapsulePopUp?.dismissCapsule()
                    })
                  }
                  self.stopLoader()
                  self.view.hideToast()
            })
      }

      func addContactToYourContactListToRespond() {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: self.pulseMsgs.addContactHeading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: self.pulseMsgs.addContactSubHeading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: "warning")
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: "dismiss")
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: "N/A")
            self.popUpVCController()
      }

      func openPulseListingPage() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController")  as! BaseViewController
            nextVC.isToOpenPulse = true
            self.present(nextVC, animated: false, completion: nil)
      }

      //function to check whether master key empty,verified,if yes API call to share and request
      //a pulse,else show error msg acc
      func actionAccToKeyVerifiedOfNot(verifyMasterKey: String) {
        let masterKey = verifyMasterKey.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if(masterKey=="") {
                  self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.enterMasterKey, toPulseListing: false)
                  placeHolderTwo = self.pulseMsgs.enterMasterKey
            } else if(!(masterKey=="") && masterKey.count==4) {
                  self.verifyMasterKey()
            } else {
            }
      }

      //verify master key with master key of user in DB,if both equal success,else error message accordigly
      func verifyMasterKey() {
            guard networkStatus.isNetworkReachable()
                  else {

                    self.view.makeToast(appService.checkInternetConnectionMessage, duration: AppService.shared.secondTimeInterval, position: .bottom)
                        return
            }
//            Dispatch
            self.startLoader()
            var awsURL = AppConfig.getMasterKeyAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.addValue(self.idtoken!, forHTTPHeaderField: "idtoken")
            request.httpBody = "".data(using: String.Encoding.utf8)
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        //self.stopLoader()
                        if let data = json as? [String: Any] {
                              let statusCodeRecieved = data["statusCode"] as? String ?? ""
                              if(statusCodeRecieved == "0" || statusCodeRecieved == "200") {
                                    if let userObject = data["user"] as? [String: Any] {
                                          DispatchQueue.main.async {
                                            if self.isFromCapsuleChatNotification{
                                                self.reloadProtocol?.nextAction(method: "navigateToChatScreen")
                                                self.dismiss(animated: true, completion: nil)

                                            }
                                            if let masterKeyFromBackEnd = userObject["masterKey"] as? String {
                                                let masterKey = self.masterKeyText.text!
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "get master key", action: "get logged in user's master key", label: "Success while getting user's master key", value: 0)
                                                      if(masterKey == masterKeyFromBackEnd) {
                                                        
                                                            /************************Code for Meet me starts here********************/
                                                            if self.fromMeetMeForCapsuleLock {
                                                                  self.stopLoader()
                                                                  self.reloadProtocol?.nextAction(method: "changeLockStatus")
                                                                  self.dismiss(animated: true, completion: nil)
                                                            } else if self.fromMeetMeForCapsuleOpening {
                                                                  self.stopLoader()
                                                                  self.reloadProtocol?.nextAction(method: "openMeetmeCapsule")
                                                                  self.dismiss(animated: true, completion: nil)
                                                                  /************************Code for Meet me ends here********************/
                                                            } else {
                                                                  //self.stopLoader()
                                                                  self.isMasterKeyVerifiedSuccefully = true
                                                                  if(self.reachedToEditPulseFromSR) {
                                                                        self.stopLoader()
                                                                        self.updateScheduledPulse(pulseId: self.pulseToBeUpdated)
                                                                  } else if(self.fromAcceptPulse) {
                                                                        if(self.respondToPulseIdIsScheduled) {
                                                                            
                                                                              self.stopLoader()
                                                                              self.acceptPulseAndShareLocAtScheduledTime()
                                                                        } else {
                                                                              self.stopLoader()
                                                                              self.acceptPulseAndShareCapsuleWithLocation()
                                                                        }
                                                                  } else if(self.fromUnlockCapsule) {
                                                                        self.stopLoader()
                                                                        self.dismiss(animated: true, completion: {
                                                                              self.pulseListingProtocol?.unlockedCapsuleDisplay(toReload: true)
                                                                        })
                                                                  } else if(self.fromUnlockCapsuleofDetails) {
                                                                        self.stopLoader()
                                                                        self.dismiss(animated: true, completion: {
                                                                              self.deletePulseProtocol?.unlockedCapsuleDisplay(toReload: true)
                                                                        })
                                                                  } else if(self.isPulseNeedToSendFromSync){
                                                                        if(self.pulseTypeFromSync == 0) {
                                                                             self.requestPulseNow()
                                                                        } else {
                                                                            self.sharePulseNow()
                                                                        }
                                                                  }else {
                                                                        if(self.pulseType=="0") {
                                                                              if(self.scheduledOrNow=="Now") {
                                                                                    self.stopLoader()
                                                                                    self.requestPulseNow()
                                                                              } else if (self.scheduledOrNow=="Scheduled") {
                                                                                    self.stopLoader()
                                                                                    self.requestPulseAtScheduled()
                                                                              }
                                                                        } else if(self.pulseType=="1") {
                                                                              if(self.scheduledOrNow=="Now") {
                                                                                    self.stopLoader()
                                                                                    self.sharePulseNow()
                                                                              } else if(self.scheduledOrNow=="Scheduled") {
                                                                                    self.stopLoader()
                                                                                    self.sharePulseAtScheduled()
                                                                              }
                                                                        }
                                                                  }
                                                            }//if master correct condition completed here
                                                            // self.stopLoader()
                                                         GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Verify master key", action: "match entered master key by user and master key while setting account details", label: "Success while verifying user's master key", value: 0)
                                                      } else//correct master key case ends here
                                                      {
                                                            self.stopLoader()
                                                            self.isMasterKeyVerifiedSuccefully = false
                                                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Verify master key", action: "match entered master key by user and master key added while setting account details", label: "Failure while verifying user's master key", value: 0)
                                                            self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.wrongMasterKey, toPulseListing: false)
                                                      }//master key doesnot match case ends here
                                                } else//user master got case ends here
                                                {
                                                      self.stopLoader()
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "get master key", action: "get logged in user's master key", label: "Failure while getting user's master key", value: 0)
                                                      self.isMasterKeyVerifiedSuccefully = false
                                                      self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                                                }
                                          }
                                    } else//user object got case ends here
                                    {
                                          self.stopLoader()
                                          GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "get master key", action: "get logged in user's master key", label: "Failure while getting user's master key", value: 0)
                                          self.isMasterKeyVerifiedSuccefully = false
                                          self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                                    }
                              } else if(statusCodeRecieved == "2" || statusCodeRecieved == "408")//status code 200 condition ends here
                              {
                                    self.stopLoader()
                                     self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.addMasterKeyInProfile, toPulseListing: false)
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "get master key", action: "get logged in user's master key", label: "Failure while getting user's master key", value: 0)
                                    self.isMasterKeyVerifiedSuccefully = false
                              }
                        } else//data object not got from backend
                        {
                              self.stopLoader()
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "get master key", action: "get logged in user's master key", label: "Failure while getting user's master key", value: 0)
                              self.isMasterKeyVerifiedSuccefully = false
                              self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                        }
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }

      /**************************************** share/request now/scheduled Api calls *******************************/

      func requestPulseNow() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
            let feedbackOperation = RequestAPulseNow(isEncrypted: true, message: self.capsuleMessage!, recipientsIdArray: self.contactIdArray)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
//                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.nowPulseRequest, toPulseListing: true)
                  } else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      func sharePulseNow() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()

            let status = CLLocationManager.authorizationStatus()

            switch status {
            case .authorized, .authorizedWhenInUse, .authorizedAlways :
                  let feedbackOperation = ShareAPulseNow(isEncrypted: true, message: self.capsuleMessage!, recipientsIdArray: self.contactIdArray)
                  feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
//                        self.stopLoader()
                        if(operation.statusCode == "200") {
                              self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.nowPulseShare, toPulseListing: true)
                        } else {
                              self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                        }
                  }
                  AppDelegate.addProcedure(operation: feedbackOperation)
            case .denied, .notDetermined:
                  self.stopLoader()
                  let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                  })
                  alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                        alert.dismiss(animated: true, completion: nil)
                  })
                  present(alert, animated: true)
                  break
            default :
                  self.stopLoader()
                  break
            }
      }

      func requestPulseAtScheduled() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
            let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: true, message: self.capsuleMessage!, recipientsIdArray: self.contactIdArray, startDateTimeStamp: self.pulseStartDate, endDateTimeStamp: self.pulseEndDate, pulseTimeTimeStamp: self.pulseTime, repeatersArray: self.pulseRepeatersArray, pulseType: "0")
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.scheduledPulseRequest, toPulseListing: true)
                  } else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      func sharePulseAtScheduled() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
            let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: true, message: self.capsuleMessage!, recipientsIdArray: self.contactIdArray, startDateTimeStamp: self.pulseStartDate, endDateTimeStamp: self.pulseEndDate, pulseTimeTimeStamp: self.pulseTime, repeatersArray: self.pulseRepeatersArray, pulseType: "1")
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.scheduledPulseShare, toPulseListing: true)
                  } else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      func updateScheduledPulse(pulseId: String) {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()
            let feedbackOperation = UpdatePulseData(isEncrypted: true, message: self.capsuleMessage!, recipientsIdArray: self.contactIdArray, toUpdatePulseId: self.pulseToBeUpdated, pulseTime: self.pulseTime, pulseStartDate: self.pulseStartDate, pulseEndDate: self.pulseEndDate, pulseDayRepeaters: self.pulseRepeatersArray, pulseType: self.pulseType!)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.pulseUpdated, toPulseListing: true)
                  } else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      /********************************** Accept pulse request now/scheduled Api calls *****************************/

      func acceptPulseAndShareCapsuleWithLocation() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            self.startLoader()

            let status = CLLocationManager.authorizationStatus()

            switch status {
            case .authorized, .authorizedWhenInUse, .authorizedAlways :
                  let feedbackOperation = AcceptNowPulseRequest(isEncrypted: true, message: self.capsuleMessage!, respondToPulseOf: self.respondToId, pulseToWhichUserResponding: self.acceptThisPulse)
                  feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                        self.stopLoader()
                        if(operation.statusCode == "200") {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.pulseMsgs.nowPulseReqAccepted)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse, execute: {
                                    self.view.hideToast()
                                    self.dismiss(animated: true, completion: {
                                          self.insertCapsuleProtocol?.dismisCapsulePopUpAndUpdateAccRec(toReload: true)
                                    })
                              })

                        } else if(operation.statusCode == "409") {
                              DispatchQueue.main.async {
                                    self.dismiss(animated: false, completion: {
                                          self.insertCapsuleProtocol?.toAlertToAddRequestInContactList(toReload: true)
                                    })
                              }
                        } else {
                              self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                        }
                  }
                  AppDelegate.addProcedure(operation: feedbackOperation)
            case .denied, .notDetermined:
                  self.stopLoader()
                  let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                  })
                  alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                        alert.dismiss(animated: true, completion: nil)
                  })
                  present(alert, animated: true)
                  break
            default :
                  self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.unableToFetchLastLocation, toPulseListing: false)
                  self.stopLoader()
                  break
            }
      }

      func acceptPulseAndShareLocAtScheduledTime() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.networkFailureMsg, toPulseListing: false)
                        return
            }
            DispatchQueue.main.async {
                self.startLoader()
            }
            let feedbackOperation = AcceptAScheduledPulseRequest(isEncrypted: true, message: self.capsuleMessage!, respondToPulseOf: self.respondToId, pulseToWhichUserResponding: self.acceptThisPulse, acceptingPulseStartDate: self.acceptThisPulseObj.pulseStartDate, acceptingPulseEndDate: self.acceptThisPulseObj.pulseEndDate, acceptingPulsePulseTime: self.acceptThisPulseObj.pulseTime, acceptingPulseRepeatersArray: self.acceptThisPulseObj.pulseRepeatersForDays)

            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        DispatchQueue.main.async {
                              self.view.makeToast(self.pulseMsgs.schedPulseReqAccepted)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                              self.dismiss(animated: true, completion: {
                                    self.insertCapsuleProtocol?.dismisCapsulePopUpAndUpdateAccRec(toReload: true)
                              })
                        })
                  } else if(operation.statusCode == "409") {
                        self.dismiss(animated: false, completion: {
                              self.insertCapsuleProtocol?.toAlertToAddRequestInContactList(toReload: true)
                        })
                  } else {
                        self.showToastMsgOnMasterKeyScreen(toastMsg: self.pulseMsgs.serverErrorOfPulse, toPulseListing: false)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      func startLoader() {
            DispatchQueue.main.async {
                self.masterKeyText.isUserInteractionEnabled=false
                self.goButton.isUserInteractionEnabled=false
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                self.masterKeyText.isUserInteractionEnabled=true
                self.goButton.isUserInteractionEnabled=true
                  ScreenLoader.shared.stopLoader()
            }
      }

}
