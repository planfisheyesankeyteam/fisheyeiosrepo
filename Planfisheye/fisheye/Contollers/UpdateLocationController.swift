//
//  UpdateLocationController.swift
//  fisheye
//
//  Created by Sankey Solutions on 25/09/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class GetLocationGlobally : NSObject, CLLocationManagerDelegate{
   
    static let shared: GetLocationGlobally = GetLocationGlobally()
    var locationManager: CLLocationManager!
    let validationController = ValidationController()
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var locationName: String?
    

    func initialize(){
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            UIApplication.shared.setMinimumBackgroundFetchInterval(20)
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            let status = CLLocationManager.authorizationStatus()
            
            if #available(iOS 9.0, *) {
                if status == .authorizedAlways{
                    self.locationManager!.allowsBackgroundLocationUpdates = true
                }
            }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        //get last entries of locations updated list
        guard let location = locations.last
            else { return }
        
        let geolocation = GeoLocation()
        let locationLatitude=location.coordinate.latitude
        let locationLongitude=location.coordinate.longitude
        geolocation.participantLatitude = locationLatitude
        geolocation.participantLongitude = locationLongitude
        geolocation.locationDateTime =  validationController.getTodaysDateInTimeStamp()
        
        latitude=locationLatitude
        longitude=locationLongitude
        let lat = Double(latitude)
        let lng = Double(locationLongitude)
        let lction = CLLocation(latitude: lat, longitude: lng)
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(lction) { (placemarks, error) in
            if error != nil {
                
            } else {
                self.processResponse(withPlacemarks: placemarks, error: error)
                geolocation.participantLocationName=self.locationName!
                DispatchQueue.main.async {
                    self.postGeoLocation(geolocation: geolocation)
                }
            }
        }
    }
    
    
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        if error != nil {
            
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                self.locationName = placemark.compactAddress
            } else {
                
            }
        }
    }
    
    
    func postGeoLocation( geolocation: GeoLocation) {
        let postLocation = PostGeoLocation(participantLocationReceived: geolocation)
        postLocation.addDidFinishBlockObserver { [unowned self] (operation, _) in            
        }
        AppDelegate.addProcedure(operation: postLocation)
    }
    
    
    private func locationManager(_ manager: CLLocationManager!, didFailWithError error: NSError!) {
        locationManager?.stopUpdatingLocation()
        if ((error) != nil) {
              
        }
    }
    
    
    
}
