func showGetUserName(index: Int) {

        let alertController = UIAlertController(title: "Enter Master Key", message: "", preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {
            _ -> Void in
        }))
        alertController.addAction(UIAlertAction(title: "Open", style: .default, handler: {
            alert -> Void in
            let fNameField = alertController.textFields![0] as UITextField
           // let lNameField = alertController.textFields![1] as UITextField

            if fNameField.text != ""{

                if(CommonMethods.getMasterkey() == fNameField.text) {

                self.logs[index].isencrypted = 0
                self.tableview.reloadData()
                } else {
                    let errorAlert = UIAlertController(title: "Error", message: "Incorrect master key", preferredStyle: .alert)
                    errorAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
                        _ -> Void in
                        self.present(alertController, animated: true, completion: nil)
                    }))
                    self.present(errorAlert, animated: true, completion: nil)
                }
               // self.newUser = User(fn: fNameField.text!, ln: lNameField.text!)
                //TODO: Save user data in persistent storage - a tutorial for another time
            } else {
                let errorAlert = UIAlertController(title: "Error", message: "Please enter master key", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {
                    _ -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }))
                self.present(errorAlert, animated: true, completion: nil)
            }
        }))

        alertController.addTextField(configurationHandler: { (textField) -> Void in
           // textField.placeholder = "First Name"
            textField.textAlignment = .center
            textField.keyboardType = .numberPad
        })

//        alertController.addTextField(configurationHandler: { (textField) -> Void in
//            textField.placeholder = "Last Name"
//            textField.textAlignment = .center
//        })

        self.present(alertController, animated: true, completion: nil)
    }
    func masterkeyViewClick(sender: UITapGestureRecognizer) {
        if(self.logs[(sender.view?.tag)!].isencrypted == 1) {
        showGetUserName(index: (sender.view?.tag)!)
      //  print("\(sender.view?.tag)")
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.tableview.dequeueReusableCell(withIdentifier: "PulseLogbookTableViewCell", for: indexPath)as! PulseLogbookTableViewCell
        cell.tag = tableView.tag
        let fepulse = logs[indexPath.row]

        let date = UTCToLocal(date: fepulse.createdTimeStamp!, isdate: true)
        let time = UTCToLocal(date: fepulse.createdTimeStamp!, isdate: false)

       // print("\(date)")

        cell.timeLabel.text = time
        cell.dateLabel.text = date
        cell.masterkeyView.tag = indexPath.row

        //pending request

        if(fepulse.pulsetype == 0 && fepulse.ispulsed == 0 && fepulse.isaccepeted == 0 ) {
            cell.pendingLabel.isHidden = false
//            DispatchQueue.main.async
//                {
//            UIView.animate(withDuration: 0.2, delay: 0.4, options: [UIViewAnimationOptions.curveEaseInOut ,  UIViewAnimationOptions.repeat], animations: {
//                
//                cell.pendingLabel.alpha = 0
//            }, completion: {(_ finished: Bool) -> Void in
//                cell.pendingLabel.alpha = 1.0
//            })
//            }
        } else {
            cell.pendingLabel.isHidden = true

        }
       // cell.pulsemapView.showsUserLocation = false

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(masterkeyViewClick(sender:)))

        cell.masterkeyView.addGestureRecognizer(tapGesture)
        if(fepulse.pulsetype == 0) {
            cell.mapviewHeightConstraint.constant = 0

            cell.selectionStyle = .gray
            //cell.viewMapLabel.isHidden = true

        } else {

            if((fepulse.pulsetype == 2 || fepulse.pulsetype == 1) && fepulse.ispulsed == 0) {
                cell.selectionStyle = .none

                //cell.viewMapLabel.isHidden = false

                cell.mapviewHeightConstraint.constant = 150.0

                DispatchQueue.main.async {

                        let annotation = MKPointAnnotation()
                        let centerCoordinate = CLLocationCoordinate2D(latitude: Double(fepulse.latitude!)!, longitude: Double(fepulse.longitude!)!)
                        annotation.coordinate = centerCoordinate
                       // annotation.colour = UIColor.red
                        let span = MKCoordinateSpanMake(0.1, 0.1)
                        let location = CLLocation(latitude: Double(fepulse.latitude!)!, longitude: Double(fepulse.longitude!)!)
                        self.fetchCountryAndCity(location: location) { country, city in
                            print("country:", country)
                            print("city:", city)

                            annotation.title = city
                            let region = MKCoordinateRegionMake(centerCoordinate, span)

                            cell.pulsemapView.addAnnotation(annotation)
                            cell.pulsemapView.region = region
                            cell.pulsemapView.selectAnnotation(annotation, animated: true)
                        }

//                        let yourAnnotationAtIndex = 0
//                        cell.pulsemapView.selectAnnotation(cell.pulsemapView.annotations[yourAnnotationAtIndex], animated: true)
                }
            } else {
                cell.selectionStyle = .gray

                cell.mapviewHeightConstraint.constant = 0

            }

        }

        if(fepulse.message != nil && (fepulse.message?.characters.count)!>0) {
            cell.masterkeyView.isHidden = false
            cell.masterkeyHeightConstraint.constant = 50

            if(fepulse.isencrypted == 1) {
                cell.mastermessage.text = "Locked Capsule"
            } else {
            cell.mastermessage.text = fepulse.message
            }
        } else {
            cell.masterkeyHeightConstraint.constant = 0
            cell.masterkeyView.isHidden = true

        }

        cell.titlelabel.text = fepulse.alertMessage

        //        if(String(values[indexPath.row]) == "103")
        //         {
        //        cell.mapviewHeightConstraint.constant = 0
        //         }else{
        //            cell.mapviewHeightConstraint.constant = 150.0
        //
        //        }
        return cell
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
