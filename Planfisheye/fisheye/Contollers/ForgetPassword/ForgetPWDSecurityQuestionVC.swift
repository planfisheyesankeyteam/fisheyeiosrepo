//
//  ForgetPWDSecurityQuestionVC.swift
//  fisheye
//
//  Created by Sankey Solution on 11/19/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class ForgetPWDSecurityQuestionVC: UIViewController {

    var securityAns1 = ""
    var securityAns2 = ""
    var emailId = ""
    var mobile = ""
    var ccode = ""
    var otp = ""
    var idtoken = ""
    var mobileNumber1 = ""
    var countryCod = ""
    var userName = ""
    var placeholder = ""
    var code: String!
    let appSharedPrefernce = AppSharedPreference()
    let validationController = ValidationController()
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    var forgotPwdAllText = ForgotPasswordTexts.shared
    var validation: Bool = false

    @IBOutlet weak var fishEyeLabel: UIImageView!
    @IBOutlet weak var forgetPWDMainViewHeading: UILabel!
    @IBOutlet weak var subViewHeading: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var securityView: UIView!
    @IBOutlet weak var backButtonTapped: UIButton!
    @IBOutlet weak var question1: UILabel!
    @IBOutlet weak var answer1: UICustomTextField!
    @IBOutlet weak var subSecurityHeadinUnderline: UIView!
    @IBOutlet weak var question2: UILabel!
    @IBOutlet weak var mainDivider: UIView!
    @IBOutlet weak var ans2Divider: UIView!
    @IBOutlet weak var ans1divider: UIView!
    @IBOutlet weak var answer2: UICustomTextField!
    @IBOutlet weak var answerTwoBlueUnderline: UIImageView!
    @IBOutlet weak var answerTwoBlackUnderline: UIImageView!
    @IBOutlet weak var answerOneBlueUnderline: UIImageView!
    @IBOutlet weak var answerOneBlackUnderline: UIImageView!
    @IBOutlet weak var fisheyeMotto: UILabel!
      @IBAction func backButtonTapped(_ sender: Any) {
        backButtonFunction()
    }
      @IBOutlet weak var finalSubmit: UIButton!
      @IBAction func finalSubmitTapped(_ sender: Any) {
        self.startLoader()
        verifyAnswer(answerone: answer1.text!, answertwo: answer2.text!)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
        self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
        self.forgotPwdAllText = ForgotPasswordTexts.shared
        self.securityView.layer.cornerRadius = 6
        self.securityView.layer.cornerRadius = 6
        self.securityView.layer.shadowColor = UIColor.black.cgColor
        self.securityView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.securityView.layer.shadowOpacity = 0.4
        self.securityView.layer.shadowRadius = 6.0
        self.securityView.layer.masksToBounds = false
//      self.view.backgroundColor = UIColor.rebrandedLightPurple()
        getSecurityQuestion()
        // for  input field validations
        self.answer1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.answer2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
      self.answer1.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
      self.answer2.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
      self.setLocalizationText()
      self.addObservers()

    }

    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ForgetPWDSecurityQuestionVC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        // Dispose of any resources that can be recreated.
    }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

        switch textField {

        case answer1:
            self.answerOneBlackUnderline.isHidden = true
            self.answerOneBlueUnderline.isHidden = false
            if((answer1.text?.characters.count)! > 0 ) {
                answer1.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
            }

        case answer2:
            self.answerTwoBlackUnderline.isHidden = true
            self.answerTwoBlueUnderline.isHidden = false
            if((answer2.text?.characters.count)! > 0 ) {
                answer2.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
            }

        default:
            break
        }
    }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case answer1:
                  self.answerOneBlackUnderline.isHidden = false
                  self.answerOneBlueUnderline.isHidden = true

            case answer2:
                  self.answerTwoBlackUnderline.isHidden = false
                  self.answerTwoBlueUnderline.isHidden = true

            default:
                  break
            }
      }

    func backButtonFunction() {

        self.dismiss(animated: true, completion: nil)
    }

    func resetFunction() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordSetNewPwdVC") as! ResetPasswordSetNewPwdVC
        nextVC.userName = self.userName
        nextVC.idtoken = self.idtoken
        self.present(nextVC, animated: true, completion: nil)
    }

    func getSecurityQuestion() {
        self.startLoader()
        //self.finalSubmit.isUserInteractionEnabled = false
        let headers =
            [

                "Content-Type": "application/json",
                "idtoken": self.idtoken,
                "action": "getsecurityquesanses"

        ] as [String: Any]

        let awsURL = AppConfig.getSecurityQuestionAnswerAPI

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.addValue("getsecurityquesanses", forHTTPHeaderField: "action")
        request.httpMethod = AppConfig.httpMethodGet
        request.httpBody =  "".data(using: String.Encoding.utf8)

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                 self.stopLoader()
                let statusCode = data["statusCode"] as? String ?? ""
                  self.stopLoader()
                if(statusCode == "200") {
                    if let user = data["user"] as? [String: Any] {
                        if let securityQuesAnsArray=user["securityQuesAnses"] as? [[String: Any]] {
                        let securityQuestion1 = securityQuesAnsArray[0]["securityQues"] as? String ?? ""
                        self.securityAns1 = securityQuesAnsArray[0]["securityAns"] as? String ?? ""
                        let securityQuestion2 = securityQuesAnsArray[1]["securityQues"] as? String ?? ""
                        self.securityAns2 = securityQuesAnsArray[1]["securityAns"] as? String ?? ""
                              DispatchQueue.main.async {
                                    self.question2.text = securityQuestion2
                                    self.question1.text = securityQuestion1
                              }
                        }

                        self.userName = user["name"] as? String ?? ""
                    }

                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot password", action: "Verify security question answers", label: "Security questions answers verified successfully", value: 0)
                } else {
                    self.view.makeToast(self.signupmsg.securiyquestionfailed, duration: self.signupmsg.toastDelayTime, position: .bottom)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.signupmsg.toastDelayMedium) {
                        self.view.hideToast()
                    }
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot password", action: "Verify security question answers", label: "Failed to give correct security question answers", value: 0)
                }

            } else {
                self.stopLoader()
                self.view.makeToast(self.signupmsg.securiyquestionfailed, duration: self.signupmsg.toastDelayTime, position: .bottom)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.signupmsg.toastDelayMedium) {
                    self.view.hideToast()
                }
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot password", action: "Verify security question answers", label: "Failed to give correct security question answers", value: 0)

            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }

    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
        self.present(nextVC, animated: true, completion: nil)
    }

    func startLoader() {

        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
    }

    func stopLoader() {
      DispatchQueue.main.async {
        ScreenLoader.shared.stopLoader()
      }
    }

    func verifyAnswer(answerone: String, answertwo: String) {
        let ans1 = answerone.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
        let ans2 = answertwo.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
        var isValidationSuccessful: Bool = true

        if(ans1.count > 0 ) {
            if(self.securityAns1 != ans1) {
                self.answer1.configOnError(withPlaceHolder: self.signupmsg.incorrectanswer)
                placeholder = answer1.placeholder!
                isValidationSuccessful = false
            } else {
                self.answer1.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                placeholder = answer1.placeholder!
            }
        } else {
            self.answer1.configOnError(withPlaceHolder: self.signupmsg.questionone)
            isValidationSuccessful = false
            placeholder = answer1.placeholder!
        }

        if(ans2.count > 0) {
            if(self.securityAns2 != ans2) {
                self.answer2.configOnError(withPlaceHolder: self.signupmsg.incorrectanswer)
                isValidationSuccessful = false
            } else {
                self.answer2.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
            }
        } else {
            self.answer2.configOnError(withPlaceHolder: self.signupmsg.questionone)
            isValidationSuccessful = false
        }

        validation = isValidationSuccessful
        if isValidationSuccessful {
            resetFunction()
        }

        self.stopLoader()
//        self.finalSubmit.isUserInteractionEnabled = true
    }

      // add all observers to this viewController here
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      // End

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.forgetPWDMainViewHeading.text = self.forgotPwdAllText.forgotPwdPrimaryHeader
                  self.subViewHeading.text = self.forgotPwdAllText.securityQuesAnsesHeader
                  self.fisheyeMotto.text = self.forgotPwdAllText.letsWorkTogether
                  self.answer1.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                  self.answer2.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                 
            }
      }
      /* END */
}
