//
//  ForgotPasswordTexts.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 27/07/2018.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

class ForgotPasswordTexts {

      static let shared: ForgotPasswordTexts = ForgotPasswordTexts()
      var forgotPwdPrimaryHeader: String = "Forgot Your Password?"
      var forgotPwdSecondaryHeader: String = "Enter Your E-mail & Mobile number"
      var youWillReceive: String = "You will recieve a Security Key on your E-mail and Phone number"
      var otpValidFor: String =  "Security Key valid for"
      var minutes: String = "minutes"
      var sendPasscode: String = "Send Security Key"
      var tryAgain: String = "Please try again"
      var enterSixDigitOTP: String = ""
      var securityQuesAnsesHeader: String = "Please answer these security questions"
      var resetPasswordHeader: String = "Reset Password"
      var enterNewPasswordHeader: String = "Enter New Password"
      var letsWorkTogether: String = "Let's work together"
      var securityKey: String = "Security Key"
      var invalidSecurityKey: String = "Invalid security key"
      var enterPasscodeSent: String = "Enter the six digit security key sent to your e-mail address and phone"
      var errorWhileRecoveringPwd: String = "Please contact us with your request to activate your account"
}
