//
//  ForgetPasswordScreen1VC.swift
//  fisheye
//
//  Created by Sankey Solution on 11/18/17.
//  Modified by Anagha Meshram on 01/02/2018
//  Removed old final button and replaced with new one with ImageView on it and commented the finalsubmit's usages throughtout the code
//  Copyright © 2017 Keerthi. All rights reserved.

import UIKit
import MRCountryPicker
import Toast_Swift
//import Toast_Swift

class ForgetPasswordScreen1VC: UIViewController, UITextFieldDelegate, MRCountryPickerDelegate {

      var emailId = ""
      var mobile = ""
      var ccode = ""
      var otp = ""
      var idtoken = ""
      var mobileNumber1 = ""
      var countryCod = ""
      var userName = ""
      var placeholderone = ""
      var placeholdertwo = ""
      var placeholderthree = ""
      var placeholderfour = ""
      var placeholderfive = ""
      var code: String!
      /*Defined Variables*/
      var remSeconds: Double = 900
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var forgotPwdAllText = ForgotPasswordTexts.shared

      var timer = Timer()
      let appSharedPrefernce = AppSharedPreference()
      let validationController = ValidationController()
    
    var minMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBelesserThan
    var maxMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBeGreaterThan

      @IBOutlet weak var forgotPwdMainView: UIView!
      @IBOutlet weak var countryPicker: MRCountryPicker!
      @IBOutlet weak var forgetPwdMainLabel: UILabel!
      @IBOutlet weak var afterOtpSentDivider: UIView!
      @IBOutlet weak var fishEyeLabel: UIImageView!

      @IBAction func backButtonTapped(_ sender: Any) {
            backFunction()
      }

      @IBOutlet weak var mobileNumberBlueUnderline: UIImageView!
      @IBOutlet weak var mobileNumberBlackunderline: UIImageView!
      @IBOutlet weak var emailBlackUnderline: UIImageView!
      @IBOutlet weak var emailBlueUnderline: UIImageView!
      @IBOutlet weak var subViewHeader: UILabel!
      @IBOutlet weak var subView: UIView!
      @IBOutlet weak var subViewHeaderUnderLine: UIView!
      @IBOutlet weak var emailTextField: UICustomTextField!
      @IBOutlet weak var timerImage: UIImageView!
      @IBOutlet weak var otpTimerView: UILabel!
      @IBOutlet weak var retryOtp: UIButton!
      @IBOutlet weak var mobileNumber: UICustomTextField!
      @IBOutlet weak var countryCode: UICustomTextField!
      @IBOutlet weak var emailTextFieldUnderLine: UIView!
      @IBOutlet weak var enterOtpTextField: UICustomTextField!
      @IBOutlet weak var enterYorMailUnderline: UIView!
      @IBOutlet weak var youWillRecieve: UILabel!
      @IBOutlet weak var enterPasscodeSent: UILabel!

      @IBAction func finalSubmitButtonTapped(_ sender: Any) {
            submitOtpFunctionCalled()
      }

      @IBOutlet weak var enterYourEmail: UILabel!
      @IBAction func retryButtonTapped(_ sender: Any) {
            self.retryOtp.isUserInteractionEnabled = false
            self.startLoader()
            resendOtpFunctionCalled()
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Send passcode clicked", label: "", value: 0)
      }
      @IBAction func sendPasscodeTapped(_ sender: Any) {
            self.sendPasscode.isUserInteractionEnabled = false
        verifyUser(emailId: emailTextField.text!, mobNumber: mobileNumber.text!)

      }
      @IBOutlet weak var countryCodeUnderline: UIView!
      @IBOutlet weak var sendPasscode: UIButton!
      @IBOutlet weak var otpView: UIView!

      func scheduleTimer() {
            DispatchQueue.main.async {
                  self.timerImage.isHidden = false
                  self.retryOtp.isUserInteractionEnabled = false
                  self.remSeconds = 900
                  self.timer.invalidate()
                  self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
            }
      }

    @objc func update() {
            self.remSeconds -= 1
            if self.remSeconds == 0 {
                  timer.invalidate()
                  self.timerImage.isHidden = true
                  otpTimerView.text = ""
                  self.retryOtp.isUserInteractionEnabled = true
                  return
            } else {
                  self.retryOtp.isUserInteractionEnabled = false
            }
            self.animate()
            let remSecondsStr = String(self.remSeconds.minuteSecond)
        otpTimerView.text = self.forgotPwdAllText.otpValidFor + " " + remSecondsStr + " " + self.forgotPwdAllText.minutes
      }

      func animate() {
            UIView.animate(withDuration: 0.5) { () -> Void in
                  self.rotateAnimation(button: self.timerImage)
                  //            self.timerImage.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
            }
      }

      /* Created by by vaishali */
      func rotateAnimation(button: UIImageView, duration: CFTimeInterval = 2.0) {
            DispatchQueue.main.async {
                  let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
                  rotateAnimation.fromValue = 0.0
                  rotateAnimation.toValue = CGFloat(.pi * 2.0)
                  rotateAnimation.duration = duration
                  rotateAnimation.repeatCount = Float.greatestFiniteMagnitude
                  button.layer.add(rotateAnimation, forKey: "syncRotation")
            }
      }
      /* END */

      func submitOtpFunctionCalled() {
            let enteredOtp = enterOtpTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.mobileNumber1 = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            if(mobileNumber1.first == "0") {
                  mobileNumber1 = String(mobileNumber1.dropFirst(1))
            }
            self.countryCod = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""

            if (enterOtpTextField.text?.characters.count) == 6 {
                  self.startLoader()
                  self.enterOtpTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.securityKey)
                  let otpEntered = enteredOtp

                  let parameters =
                        [
                              "idtoken": idtoken,
                              "countryCode": self.countryCod,
                              "phonenumber": mobileNumber1,
                              "otpValue": otpEntered
                  ] as [String: Any]

                  let awsURL = AppConfig.forgotVerifyOTPAPI

                  guard let URL = URL(string: awsURL) else { return }
                  let sessionConfig = URLSessionConfiguration.default
                  let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
                  var request = URLRequest(url: URL)
                  request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
                  request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
                  request.httpMethod = AppConfig.httpMethodPost
                  guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                        return
                  }
                  request.httpBody = httpBody

                  guard let signedRequest = URLRequestSigner().sign(request: request) else {
                        return
                  }

                  let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                        if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                              let data = json
                              let statusCode = data["statusCode"] as? String ?? ""
                              if(statusCode == "200" || statusCode=="0") {
                                    self.stopLoader()
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot password", action: "verify OTP to set new password", label: "OTP verification successful - Redirection to set new password", value: 0)
                                    self.resetPasswordPageCalled()
                              } else if(statusCode == "4") {
                                self.stopLoader()
                                DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.otpExpired, duration: self.profilemsg.toastDelayTime, position: .bottom)
                                }
                                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP expired", label: "Mobile Verification Failed", value: 0)
                              } else {
                                    self.stopLoader()
                                    self.enterOtpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidotp)
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot password", action: "verify OTP to set new password", label: "OTP verification failed", value: 0)
                              }

                        } else {
                              self.stopLoader()
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }

                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot password", action: "verify OTP to set new password", label: "OTP verification failed", value: 0)
                        }
                  })
                  task.resume()
                  session.finishTasksAndInvalidate()

            } else {
                  self.enterOtpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidotp)
            }
      }

      func resetPasswordPageCalled() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordSetNewPwdVC") as! ResetPasswordSetNewPwdVC
            nextVC.showDisabledOTPView = self as ShowDisabledOTPView
            nextVC.userName = self.userName
            nextVC.idtoken = self.idtoken
            DispatchQueue.main.async {
                  self.present(nextVC, animated: true, completion: nil)
            }
      }

      func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

            if textField == countryCode {

                  let picker = MICountryPicker()

                  picker.showCallingCodes = true

                  // or closure
                  picker.didSelectCountryWithCallingCodeClosure = { name, code, dailCode in
                        self.dismiss(animated: true, completion: nil)
                        self.countryCode.text = "\(dailCode)"
                        self.code = code
                        self.mobileNumber.becomeFirstResponder()
                  }

                  self.present(picker, animated: true, completion: nil)

                  return false
            }
            return true
      }

    func verifyUser(emailId: String, mobNumber: String) {
            var isValidationSuccessful: Bool = true
        if (emailId.characters.count) <= 0 {
                  self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremailid)
                  placeholderone = emailTextField.placeholder!
                  isValidationSuccessful = false
            }

            if (emailId.characters.count) > 0 {
                  let emailText: String = self.emailTextField.text!
                  if validationController.isValidEmail(testStr: emailText) {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                  } else {
                        self.emailTextField.configOnError(withPlaceHolder: self.profilemsg.invalidemail)
                            placeholderfour = emailTextField.placeholder!
                        isValidationSuccessful = false
                  }
            }

            if (mobNumber.count) <= 0 {
                  self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                      placeholdertwo = mobileNumber.placeholder!
                  isValidationSuccessful = false
            } else if((mobNumber.characters.count) < self.minMobNumDigits && (mobNumber.characters.count) > self.maxMobNumDigits) {
                  self.mobileNumber.configOnError(withPlaceHolder: self.profilemsg.invalidphone)
                     placeholderthree = mobileNumber.placeholder!

                  isValidationSuccessful = false
            }

            if (countryCode.text?.characters.count)! <= 0 {
                  self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.entrphonenumber)
                  self.countryCode.configOnError(withPlaceHolder: self.signupmsg.entercountrycode)
                  isValidationSuccessful = false
            } else {
                  self.countryCode.configOnErrorCleared(withPlaceHolder: self.signupmsg.code)
            }

            if isValidationSuccessful {
                  self.forgotPasswordAPI()
            }
      }

      func forgotPasswordAPI() {
            self.startLoader()
            self.emailId = emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.mobile = mobileNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if(mobile.first == "0") {
                  mobile = String(mobile.dropFirst(1))
            }
            self.ccode = countryCode.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            let parameters =
                  [
                        "action": "forgotpassword",
                        "email": self.emailId,
                        "phonenumber": self.mobile,
                        "countryCode": self.ccode
            ] as [String: Any]

            let awsURL = AppConfig.forgotPasswordAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json

                        self.stopLoader()
                        let statusCode = data["statusCode"] as? String ?? ""
                        if(statusCode == "0") {
                              if let user = data["user"] as? [String: Any] {
                                    self.idtoken = data["idtoken"] as? String ?? ""
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "idtoken", value: self.idtoken)
                                    self.mobileNumber1 = user["phonenumber"] as? String ?? ""
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.mobileNumber1)
                                    self.countryCod = user["countryCode"] as? String ?? ""
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "countrycode", value: self.countryCod)
                                    self.userName = user["name"] as? String ?? ""
                                    DispatchQueue.main.async {
                                          self.otpView.backgroundColor = UIColor.white
                                          self.otpView.isUserInteractionEnabled = true
                                          self.timerImage.isHidden = false
                                          self.otpTimerView.isHidden = false
                                          self.retryOtp.isHidden = false
                                          self.sendPasscode.isHidden = true
                                          self.sendPasscode.isUserInteractionEnabled = false

                                          self.emailTextField.isUserInteractionEnabled = false
                                          self.countryCode.isUserInteractionEnabled = false
                                          self.mobileNumber.isUserInteractionEnabled = false
                                          self.afterOtpSentDivider.isHidden = false
                                          self.scheduleTimer()
                                          self.animate()
                                          self.view.makeToast(self.profilemsg.otpsent, duration: self.signupmsg.toastDelayTime, position: .bottom)
                                    }

                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime) {
                                          self.view.hideToast()
                                    }
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Send passcode", label: "User found, OTP sent", value: 0)

                              }
                        } else if(statusCode == "1") {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.signupmsg.usernotfound, duration: self.signupmsg.toastDelayTime, position: .bottom)

                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Send passcode clicked", label: "User not found", value: 0)
                        } else if(statusCode == "2") {

                              DispatchQueue.main.async {
                                    self.view.makeToast( self.signupmsg.emailphonenumbernotmatch, duration: self.signupmsg.toastDelayTime, position: .bottom)

                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.sendPasscode.isUserInteractionEnabled = true
                                    var securityQuestionsExist = false
                                    if let user = data["user"] as? [String: Any] {
                                          securityQuestionsExist = user["securityQuesAnses_exist"] as? Bool ?? false
                                    }
                                    if securityQuestionsExist {
                                          DispatchQueue.main.async {
                                                self.view.hideToast()
                                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                let nextVC = storyBoard.instantiateViewController(withIdentifier: "ForgetPWDSecurityQuestionVC") as! ForgetPWDSecurityQuestionVC
                                                nextVC.idtoken = data["idtoken"] as? String ?? ""
                                                self.present(nextVC, animated: true, completion: nil)
                                          }
                                    } else {
                                          self.showToast(message: self.forgotPwdAllText.errorWhileRecoveringPwd)
                                    }
                              })

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Send passcode clicked", label: "Email or Phone Number do not match with the registered id - Redirection to enter security question answers", value: 0)
                        } else if(statusCode == "3") {

                              if let user = data["user"] as? [String: Any] {
                                    let signUpMethod = user["signupMethod"] as? String ?? ""
                                    DispatchQueue.main.async {
                                          self.view.makeToast( "You are registered on Fisheye with your \(signUpMethod) ID. Please try logging in again with  \(signUpMethod).", duration: self.signupmsg.toastDelayTime, position: .bottom)

                                    }
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                          self.view.hideToast()
                                    })

                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Send passcode clicked", label: "Social sign up users are not allowed to change password", value: 0)
                        }

                  } else {
                        self.sendPasscode.isUserInteractionEnabled = true
                        self.stopLoader()
                        DispatchQueue.main.async {
                              self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime) {
                              self.view.hideToast()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Send passcode clicked", label: "Failure Occurred", value: 0)
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }

      func resendOtpFunctionCalled() {
            self.startLoader()
            var phonenumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            if(phonenumber.first == "0") {
                  phonenumber = String(phonenumber.dropFirst(1))
            }
            let countrycode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""

            let parameters =
                  [

                        "action": "resendotp",
                        "idtoken": idtoken,
                        "email": self.emailId,
                        "phonenumber": phonenumber,
                        "countryCode": countrycode

            ] as [String: Any]

            let awsURL = AppConfig.forgotResendOTPAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        let statusCode = data["statusCode"] as? String ?? ""
                        if(statusCode == "200") {
                              self.stopLoader()
                              self.scheduleTimer()
                              self.animate()
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.otpsent, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Retry button clicked", label: "OTP sent successful", value: 0)
                        } else {
                              self.stopLoader()
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.signupmsg.smssendingfailed, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime) {
                                    self.view.hideToast()
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Retry button clicked", label: "Failed to send OTP", value: 0)
                        }
                  } else {
                        self.stopLoader()
                        DispatchQueue.main.async {
                              self.view.makeToast(self.signupmsg.smssendingfailed, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime) {
                              self.view.hideToast()
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Forgot Password", action: "Send passcode clicked", label: "Failed to sent OTP", value: 0)

                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }

      func backFunction() {
            let loginPreference = self.appSharedPrefernce.getAppSharedPreferences(key: "loginPreferenceUsed") as? String ?? ""
//            let introductaryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "dashboardIntroducataryPages") as? String ?? "1"
//            let syncIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "syncIntroductoryVariableIndex") as? String ?? "1"
//            let pulseIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "pulseIntroductoryVariableIndex") as? String ?? "1"
//            let meetMeIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIntroductoryVariableIndex") as? String ?? "1"

            let isFetchFromBackendCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isFetchFromBackendCompleted") as? String ?? "no"

            let isfronendSyncFullyCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isfronendSyncFullyCompleted") as? String ?? "no"
            let preferredLanguage = self.appSharedPrefernce.getAppSharedPreferences(key: "preferredLanguage") as? String ?? ""

            let fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ??  ""

            self.appSharedPrefernce.removeAllAppSharedPreferences()
            self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: preferredLanguage)

            self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: loginPreference)
//            self.appSharedPrefernce.setAppSharedPreferences(key: "dashboardIntroducataryPages", value: "1")
            self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeId", value: fisheyeId)

            self.appSharedPrefernce.setAppSharedPreferences(key: "isFetchFromBackendCompleted", value: isFetchFromBackendCompleted)
            self.appSharedPrefernce.setAppSharedPreferences(key: "isfronendSyncFullyCompleted", value: isfronendSyncFullyCompleted)

//            if(syncIntroductoryVariableIndex == "0") {
//                  self.appSharedPrefernce.setAppSharedPreferences(key: "syncIntroductoryVariableIndex", value: "0")
//            } else {
//                  self.appSharedPrefernce.setAppSharedPreferences(key: "syncIntroductoryVariableIndex", value: "1")
//            }
//
//            if(pulseIntroductoryVariableIndex == "0") {
//                  self.appSharedPrefernce.setAppSharedPreferences(key: "pulseIntroductoryVariableIndex", value: "0")
//            } else {
//                  self.appSharedPrefernce.setAppSharedPreferences(key: "pulseIntroductoryVariableIndex", value: "1")
//            }

//            if(meetMeIntroductoryVariableIndex == "0") {
//                  self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIntroductoryVariableIndex", value: "0")
//            } else {
//                  self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIntroductoryVariableIndex", value: "1")
//            }
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextVC, animated: true, completion: nil)

      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.stopLoader()
            }

      }

      // a picker item was selected
      func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
            self.countryCode.text = phoneCode
            countryPicker.isHidden = true
            self.subView.isHidden = false
      }

      @IBOutlet weak var phonePicker: UIButton!

      @IBAction func phonePickerTapped(_ sender: Any) {
            self.subView.isHidden = true
            self.countryPicker.isHidden = false
      }

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
        self.countryPicker.layer.cornerRadius = 6.0 
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
            self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
            self.forgotPwdAllText = ForgotPasswordTexts.shared
            countryPicker.isHidden = true
            self.retryOtp.isHidden = true
            self.otpTimerView.isHidden = true
            self.timerImage.isHidden = true
            self.otpView.isUserInteractionEnabled = false
            self.countryCode.isUserInteractionEnabled = false
            self.afterOtpSentDivider.isHidden = true
            self.forgetPwdMainLabel.text = "Forgot your password ?"
            self.otpView.layer.cornerRadius = 6
            self.subView.layer.cornerRadius = 6
            self.subView.layer.shadowColor = UIColor.black.cgColor
            self.subView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.subView.layer.shadowOpacity = 0.4
            self.subView.layer.shadowRadius = 6.0
            self.subView.layer.masksToBounds = false
            self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.mobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.emailTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.mobileNumber.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.enterOtpTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.enterOtpTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

            // Do any additional setup after loading the view.

            self.emailBlackUnderline.isHidden = false
            self.emailBlueUnderline.isHidden = true

            self.mobileNumberBlackunderline.isHidden = false
            self.mobileNumberBlueUnderline.isHidden = true

            countryPicker.countryPickerDelegate = self
            countryPicker.showPhoneNumbers = true

            // set country by its code
            countryPicker.setCountry("GB")

            // optionally set custom locale; defaults to system's locale
            countryPicker.setLocale("gb_GB")

            // set country by its name
            countryPicker.setCountryByName("London")
            self.retryOtp.isUserInteractionEnabled = false
            self.setLocalizationText()
            self.addObservers()
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.ForgetPasswordScreen1VC)
      }

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case emailTextField:
                  self.emailBlackUnderline.isHidden = true
                  self.emailBlueUnderline.isHidden = false
                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  } else {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                  }

            case mobileNumber:
                  self.mobileNumberBlackunderline.isHidden = true
                  self.mobileNumberBlueUnderline.isHidden = false
                  if (mobileNumber.text?.characters.count)! <= 0 {
                        self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  } else {
                        self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                  }

            case enterOtpTextField:
                  if((enterOtpTextField.text?.characters.count)! > 0 ) {
                        self.enterOtpTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.securityKey)
                        if((enterOtpTextField.text?.characters.count)! > 6) {
                              enterOtpTextField.text = String((enterOtpTextField.text?.dropLast())!)
                        }
                  } else {
                        self.enterOtpTextField.configOnError(withPlaceHolder: self.signupmsg.enterotp)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case emailTextField:
                  self.emailBlackUnderline.isHidden = false
                  self.emailBlueUnderline.isHidden = true
                  let emailText: String = textField.text!
                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  } else {
                        if validationController.isValidEmail(testStr: emailText) {
                              self.emailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                        } else {
                              self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.invalidemail)
                        }
                  }
                  self.sendPasscode.isUserInteractionEnabled = true

            case mobileNumber:
                  self.mobileNumberBlackunderline.isHidden = false
                  self.mobileNumberBlueUnderline.isHidden = true
                  self.sendPasscode.isUserInteractionEnabled = true
                  if (mobileNumber.text?.characters.count)! <= 0 {
                        self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  } else {
                        self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                    if (mobileNumber.text?.characters.count)! > self.minMobNumDigits && (mobileNumber.text?.characters.count)! < self.maxMobNumDigits {
                              self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                        } else {
                              self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.invalidphone)
                        }
                    self.showToast(message: self.signupmsg.phoneNumberShouldBeValid)
                  }

            case enterOtpTextField:
                  if((enterOtpTextField.text?.characters.count)! > 0 ) {
                        self.enterOtpTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.securityKey)
                        if((enterOtpTextField.text?.characters.count)! > 6) {
                              enterOtpTextField.text = String((enterOtpTextField.text?.dropLast())!)
                        }
                        if (enterOtpTextField.text?.characters.count)! < 6 {
                              self.enterOtpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidotp)
                        }
                  } else {
                        self.enterOtpTextField.configOnError(withPlaceHolder: self.signupmsg.enterotp)
                  }

            default:
                  break
            }
      }
      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
            self.present(nextVC, animated: true, completion: nil)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      func disabledOTPView() {
            self.otpView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.sendPasscode.isHidden = false
            self.sendPasscode.isUserInteractionEnabled = true
            self.retryOtp.isHidden = true
            self.retryOtp.isUserInteractionEnabled = false
            self.enterOtpTextField.text = ""
            self.enterOtpTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.securityKey)
            self.otpTimerView.isHidden = true
            self.timerImage.isHidden = true
            self.otpView.isUserInteractionEnabled = false
      }

      // Show toast on view
      //Author: Vaishali
      func showToast(message: String) {
            DispatchQueue.main.async {
                self.view.makeToast(message, duration: self.signupmsg.toastDelayTime, position: .bottom)
            }
      }
      // end of function showing toast on screen

      // add all observers to this viewController here
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      // End

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.forgetPwdMainLabel.text = self.forgotPwdAllText.forgotPwdPrimaryHeader
                  self.enterYourEmail.text = self.forgotPwdAllText.forgotPwdSecondaryHeader
                  self.youWillRecieve.text = self.forgotPwdAllText.youWillReceive
                  self.sendPasscode.setTitle(self.forgotPwdAllText.sendPasscode, for: .normal)
                  self.retryOtp.setTitle(self.forgotPwdAllText.tryAgain, for: .normal)
                  self.emailTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.email)
                  self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                  self.enterPasscodeSent.text = self.forgotPwdAllText.enterPasscodeSent
                  
                   self.countryCode.configOnErrorCleared(withPlaceHolder: self.signupmsg.code)
            }
      }
      /* END */

}

extension ForgetPasswordScreen1VC: ShowDisabledOTPView {
      func toDisableOTPView() {
            self.disabledOTPView()
      }
}
