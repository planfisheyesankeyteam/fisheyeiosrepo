//
//  AppSharedPreference.swift
//  fisheye
//
//  Created by Sankey Solution on 11/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

public class AppSharedPreference {

    let sharedPreference = UserDefaults.standard

    //Setting up Shared PreferencesAppSharedPreference
    public func setAppSharedPreferences(key: String, value: Any) {

        sharedPreference.set(value, forKey: key)

    }

    //Getting up Shared PreferencesAppSharedPreference
    public func getAppSharedPreferences(key: String) -> Any! {

        return sharedPreference.object(forKey: key)

    }

    //Getting up Shared PreferencesAppSharedPreference
    public func removeAppSharedPreferences(key: String) {

        sharedPreference.removeObject(forKey: key)
        sharedPreference.synchronize()

    }

    //Getting up Shared PreferencesAppSharedPreference
    public func removeAllAppSharedPreferences() {

        sharedPreference.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        sharedPreference.synchronize()

    }

}
