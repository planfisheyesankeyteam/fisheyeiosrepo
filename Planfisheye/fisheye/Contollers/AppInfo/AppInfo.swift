//
//  AppInfo.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 13/06/2018.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import UIKit

var MyObservationContext = 0
class AppInfo: UIViewController {

      @IBOutlet weak var versionLbl: UILabel!
      @IBOutlet weak var webView: UIWebView!
      @IBOutlet weak var webViewHeightConstraint: NSLayoutConstraint!
      @IBOutlet weak var scrollView: UIScrollView!
      @IBOutlet weak var termsAndConditionsHeader: UILabel!
      @IBOutlet weak var fisheyeMotto: UILabel!
      @IBOutlet weak var fisheyeStatement: UILabel!
      @IBOutlet weak var privacyPolicy: UILabel!
      @IBOutlet weak var termsOfUse: UILabel!
      @IBOutlet weak var cookiePolicy: UILabel!
      @IBOutlet weak var disclaimerPolicy: UILabel!

      var observing = false
      var webViewHeight = 0
      var version = ""
      let appService = AppService.shared
      let signupMsgs = SignupToastMsgHeadingSubheadingLabels.shared
      var isLoaderShown: Bool = false
        
      override func viewDidLoad() {
        super.viewDidLoad()
            self.stopLoader()
            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            webView.scrollView.isScrollEnabled = false
            webView.scrollView.bounces = false
            self.versionLbl.text = self.version
            self.addObservers()
            self.setLocalizationText()
    }

      override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.navigationBar.isHidden = true
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.AppInfo)
      }

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /*  Set all text labels according to language selected by FEUser --*/
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.termsAndConditionsHeader.text = self.appService.termsNConditions
                  self.fisheyeMotto.text = self.appService.letsWorkTogether
                  self.fisheyeStatement.text = self.signupMsgs.fisheyeStatement
                  self.privacyPolicy.text = self.signupMsgs.privacyPolicy
                  self.termsOfUse.text = self.signupMsgs.termsOfUse
                  self.cookiePolicy.text = self.signupMsgs.cookiePolicyFor
                  self.disclaimerPolicy.text = self.signupMsgs.disclaimerPolicy
            }
      }
      /* END */

      deinit {
            stopObservingHeight()
      }

      func startObservingHeight() {
            let options = NSKeyValueObservingOptions([.new])
            webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: options, context: &MyObservationContext)
            observing = true
      }

      func stopObservingHeight() {
            if observing {
                  webView.scrollView.removeObserver(self, forKeyPath: "contentSize", context: &MyObservationContext)
                  observing = false
            }
      }

      override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
            guard let keyPath = keyPath,
                  let context = context else {
                        super.observeValue(forKeyPath: nil, of: object, change: change, context: nil)
                        return
            }
            switch (keyPath, context) {
            case("contentSize", &MyObservationContext):
                  self.webViewHeightConstraint.constant = webView.scrollView.contentSize.height
                  self.scrollView.translatesAutoresizingMaskIntoConstraints = true
                  self.scrollView.contentSize = CGSize(width: 320, height: self.webViewHeightConstraint.constant + (53*6))

            default:
                  super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            }
      }

      @IBAction func backBtnTapped(_ sender: Any) {
            self.stopLoader()
            self.dismiss(animated: true, completion: nil)
      }

      @IBAction func termsNConditionsTapped(_ sender: Any) {
            if(self.webViewHeightConstraint.constant > 0) {
                  self.webViewHeightConstraint.constant = 0
                  self.scrollView.contentSize = CGSize(width: 320, height: (53*6))
            } else {
                  let URLget = URL(string: AppConfig.termsAndConditions)
                  let urlRequest1 = URLRequest(url: URLget!)
                  webView.loadRequest(urlRequest1)
                  webView.delegate = self as UIWebViewDelegate
                  self.webViewHeightConstraint.constant = 200
            }
      }

      @IBAction func privacyPolicyTapped(_ sender: Any) {
            if let url = URL(string: self.appService.privacyPolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      @IBAction func marketplaceTermsTapped(_ sender: Any) {
            if let url = URL(string: self.appService.marketplacePolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      @IBAction func cookiePolicyTapped(_ sender: Any) {
            if let url = URL(string: self.appService.cookiePolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      @IBAction func disclaimerPolicyTapped(_ sender: Any) {
            if let url = URL(string: self.appService.disclaimerPolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      @IBAction func policyUpdates(_ sender: Any) {
            if let url = URL(string: self.appService.policyUpdatesURL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      //Function to start and stop the loader
      func startLoader() {
        self.isLoaderShown = true
            DispatchQueue.main.async {
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }
      }

      func stopLoader() {
        DispatchQueue.main.async {
              ScreenLoader.shared.stopLoader()
        }
      }
      //End of Function to start and stop the loader

      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AppInfo: UIWebViewDelegate {
      func webViewDidFinishLoad(_ webView: UIWebView) {
            self.stopLoader()
            self.webViewHeightConstraint.constant = webView.scrollView.contentSize.height
            self.scrollView.translatesAutoresizingMaskIntoConstraints = true
            self.scrollView.contentSize = CGSize(width: 320, height: self.webViewHeightConstraint.constant + (53*6))
            if (!self.observing) {
                  self.startObservingHeight()
            }
      }

      func webViewDidStartLoad(_ webView: UIWebView) {
        if !self.isLoaderShown{
            self.startLoader()
        }
      }

      func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
            self.stopLoader()
      }
}
