//
//  AppReusableFunctions.swift
//  fisheye
//
//  Created by venkatesh murthy on 25/10/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

func saveFisheyeUserObject(data: [String: Any]) {
    var profileObject = Profile()
    if let user = data["user"] as? [String: Any] {
        profileObject.addresses = []
        if let addresses = user["addresses"] as? [[String: Any]] {
            for i  in 0..<addresses.count {
                var addressItem = AddressType()
                addressItem.locality = addresses[i]["locality"] as? String ?? ""
                addressItem.addressId = addresses[i]["addressId"] as? String ?? ""
                addressItem.street_address = addresses[i]["street_address"] as? String ?? ""
                addressItem.region =  addresses[i]["region"] as? String ?? ""
                addressItem.formatted =  addresses[i]["formatted"] as? String ?? ""
                addressItem.type = addresses[i]["type"] as? String ?? ""
                addressItem.country = addresses[i]["country"] as? String ?? ""
                addressItem.postal_code = addresses[i]["postal_code"] as? String ?? ""
                profileObject.addresses.append(addressItem)
            }
        }
            
            profileObject.name = user["name"] as? String ?? ""
            profileObject.phonenumber = user["phonenumber"] as? String ?? ""
            profileObject.id = user["id"] as? String ?? ""
            profileObject.countryCode = user["countryCode"] as? String ?? ""
            profileObject.signupMethod = user["signupMethod"] as? String ?? ""
            profileObject.email = user["email"] as? String ?? ""
            profileObject.privacyEnabled = user["privacyEnabled"] as? Bool
            profileObject.picture = user["picture"] as? String ?? ""
            profileObject.gender = user["gender"] as? String ?? ""
            profileObject.masterKey = user["masterKey"] as? String ?? ""
            
            let fisheyeName = user["name"] as? String ?? ""
            AppSharedPreference().setAppSharedPreferences(key: "fisheyeName", value: fisheyeName) as? String ?? ""
            var fisheyePhoto = ""
            if( user["picture"] != nil) {
                fisheyePhoto = user["picture"] as? String ?? ""
            }
            AppSharedPreference().setAppSharedPreferences(key: "fisheyePhoto", value: fisheyePhoto)
            AppService.shared.noOfUnreadMeetNotifications = user["userMeetMeNotifications"] as? Int ?? 0
            let genderSelection = user["gender"] as? String ?? ""
            AppSharedPreference().setAppSharedPreferences(key: "gender", value: genderSelection)
            AppSharedPreference().setAppSharedPreferences(key: "fisheyeUserObject", value: user)        
    }
}

