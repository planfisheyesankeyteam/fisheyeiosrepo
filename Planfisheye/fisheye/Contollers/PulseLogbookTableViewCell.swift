//
//  PulseLogbookTableViewCell.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 04/09/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import MapKit

class PulseLogbookTableViewCell: UITableViewCell {
    var onButtonTapped : (() -> Void)?

    @IBOutlet weak var masterkeyHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewMapLabel: UILabel!
    @IBOutlet weak var masterkeyView: UIView!
    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pulsemapView: MKMapView!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var mastermessage: UILabel!
    @IBOutlet weak var mapviewHeightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
