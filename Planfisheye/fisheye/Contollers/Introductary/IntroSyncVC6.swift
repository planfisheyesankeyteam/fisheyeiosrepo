//
//  IntroSyncVC6.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 15/05/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class IntroSyncVC6: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

      @IBAction func okButtonTapped(_ sender: Any) {
            self.dismiss(animated: true) {
                  NotificationCenter.default.post(name: LOAD_SYNC_BASE_VIEW, object: nil)
            }
      }
      /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
