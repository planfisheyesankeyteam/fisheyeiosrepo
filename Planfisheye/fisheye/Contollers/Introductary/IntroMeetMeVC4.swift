//
//  IntroMeetMeVC4.swift
//  fisheye
//
//  Created by SankeyMacPro on 05/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class IntroMeetMeVC4: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

      @IBAction func skipBtnTapped(_ sender: Any) {
            self.dismiss(animated: true) {
                  NotificationCenter.default.post(name: LOAD_MEET_BASE_VIEW, object: nil)

            }
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
