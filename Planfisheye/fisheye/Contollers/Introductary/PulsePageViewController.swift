//
//  PulsePageViewController.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 27/01/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class PulsePageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    lazy var viewControllerList: [UIViewController] = {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let viewControllerVC1 = sb.instantiateViewController(withIdentifier: "pulse-0")
        let viewControllerVC2 = sb.instantiateViewController(withIdentifier: "pulse-1")
        let viewControllerVC3 = sb.instantiateViewController(withIdentifier: "pulse-2")
        let viewControllerVC4 = sb.instantiateViewController(withIdentifier: "pulse-3")
        let viewControllerVC5 = sb.instantiateViewController(withIdentifier: "pulse-4")
        let viewControllerVC6 = sb.instantiateViewController(withIdentifier: "pulse-5")

        return[viewControllerVC1, viewControllerVC2, viewControllerVC3, viewControllerVC4, viewControllerVC5, viewControllerVC6]
    }()
    var pageControl = UIPageControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self

        if let firstViewCOntroller = viewControllerList.first {
            self.setViewControllers([firstViewCOntroller], direction: .forward, animated: true, completion: nil)
        }

        self.delegate = self
        configurePageControl()
    }

    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 100, width: UIScreen.main.bounds.width, height: 50))
        self.pageControl.numberOfPages = viewControllerList.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.purpleDots()
        self.pageControl.pageIndicatorTintColor = UIColor.white.withAlphaComponent(0.17)
        self.pageControl.currentPageIndicatorTintColor = UIColor.purpleDots()
      self.pageControl.layer.position.y = self.view.frame.height - 100
        self.view.addSubview(pageControl)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let vcIndex = viewControllerList.index(of: viewController)
            else {return nil}

        let previousIndex = vcIndex - 1

        guard previousIndex >= 0 else {
            return viewControllerList.last
        }
        guard viewControllerList.count > previousIndex else {
            return nil
        }
        return viewControllerList[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of: viewController)
            else {return nil}

        let nextIndex = vcIndex + 1

        guard viewControllerList.count > nextIndex else {
            return viewControllerList.first
        }
        guard viewControllerList.count > nextIndex else {
            return nil
        }
        return viewControllerList[nextIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = viewControllerList.index(of: pageContentViewController)!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
