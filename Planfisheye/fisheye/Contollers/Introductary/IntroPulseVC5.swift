//
//  IntroPulseVC5.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 19/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class IntroPulseVC5: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

      @IBAction func skipBtnTapped(_ sender: Any) {
            self.dismiss(animated: true) {
                  NotificationCenter.default.post(name: LOAD_PULSE_BASE_VIEW, object: nil)
            }
      }

      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
