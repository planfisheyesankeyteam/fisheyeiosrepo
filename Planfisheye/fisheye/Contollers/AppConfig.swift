//
//  AppConfig.swift
//  fisheye
//
//  Created by sankeyMacOs on 18/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
struct AppConfig {

    private static let prodStage = "prod"
    private static let devStage = "dev"
    private static let currentStage = prodStage
    //private static let currentStage = devStage
    
    private struct BaseURLsWithStage {
        static let User = "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/"+"\(currentStage)"
        static let Sync = "https://60sru49x84.execute-api.eu-west-2.amazonaws.com/"+"\(currentStage)"
        static let Lookout = "https://haz7wko856.execute-api.eu-west-2.amazonaws.com/"+"\(currentStage)"
        static let MeetMe = "https://sz3i35gurk.execute-api.eu-west-2.amazonaws.com/"+"\(currentStage)"
        static let Pulse = "https://toscmzkx6c.execute-api.eu-west-2.amazonaws.com/"+"\(currentStage)"
    }

    private static let SyncBaseURL = BaseURLsWithStage.Sync
    private static let LookoutBaseURL = BaseURLsWithStage.Lookout
    private static let MeetMeBaseURL = BaseURLsWithStage.MeetMe
    private static let PulseBaseURL = BaseURLsWithStage.Pulse
    private static let UserBaseURL = BaseURLsWithStage.User

    public static let userBaseURLForConfiguration = BaseURLsWithStage.User

/********************************************* User Module API's****************************************/

    //To save fcm token corresponding to user
    static var getTranslatedText: String {
        return "https://3ohobwv2z6.execute-api.eu-west-2.amazonaws.com/prod/updatelanguages"
    }

    //To save fcm token corresponding to user
    static var saveFCMTakenAPI: String {
        return UserBaseURL + "/savefcmtoken"
    }

    //To add master key while sign up
    static var addMasterKeyAPI: String {
        return UserBaseURL + "/users/user/addmasterkey"
    }

    //To add security question and answer while sign-up flow
    static var addSecurityQuestionAnswerAPI: String {
        return UserBaseURL + "/users/user/addsecurityquesanses"
    }

    //To resent OTP
    static var resendOTPAPI: String {
        return UserBaseURL + "/phonenumber/resendotp"
    }

    //To verify OTP
    static var verifyOTPAPI: String {
        return UserBaseURL + "/phonenumber/verifyotp"
    }

    //To send OTP
    static var sendOTPAPI: String {
        return UserBaseURL + "/phonenumber/sendotp"
    }

    //To forgot password send OTP
    static var forgotVerifyOTPAPI: String {
        return UserBaseURL + "/phonenumber/fverifyotp"
    }

    //Forgot password API
    static var forgotPasswordAPI: String {
        return UserBaseURL + "/password/forgotpassword"
    }

    //Forgot resent OTP
    static var forgotResendOTPAPI: String {
        return UserBaseURL + "/phonenumber/fresendotp"
    }

    //get security question answer
    static var getSecurityQuestionAnswerAPI: String {
        return UserBaseURL + "/users/user/getsecurityquesanses"
    }

    //add address API
    static var addAddressAPI: String {
        return UserBaseURL + "/address/addaddress"
    }

    //Login API
    static var loginAPI: String {
        return UserBaseURL + "/users/login"
    }

    //change masterkey without old masterkey
    static var changeMasterWithoutOldAPI: String {
        return UserBaseURL + "/users/user/changemasterkeywithoutoldmasterkey"
    }

    //change masterkey without old masterkey
    static var deleteUserAPI: String {
        return UserBaseURL + "/users/deleteuser"
    }

    //chnage password API
    static var changePasswordAPI: String {
        return UserBaseURL + "/password/changepassword"
    }

    //reset password API
    static var resetPasswordAPI: String {
        return UserBaseURL + "/password/resetpassword"
    }

    //To Get Profile Url
    static var getFisheyeUserProfile: String {
        return UserBaseURL + "/users/getuserdetails"
    }

    //To update Profile Url
    static var updateFisheyeBasicUserProfile: String {
        return UserBaseURL + "/users/updateuserdetails"
    }

    //To Get Terms And Conditions Url
    static var termsAndConditions: String {
        return "https://s3.eu-west-2.amazonaws.com/fisheye-content-storage/ProfileImages/TermsAndConditionsFisheye.html"
    }

    //To Add New Fisheye User Basic Url
    static var addBasicInfo: String {
        return UserBaseURL + "/users/adduserdetails"
    }

    //To update Terms and Condition flag
    static var updateTNC: String {
        return UserBaseURL + "/users/user/accepttermsnconditions"
    }

    //To modifyUserDetails
    static var modifyUserDetails: String {
        return UserBaseURL + "/users/updateuserdetailsforregistration"
    }

    //To reset MasterKey
    static var resetMasterKeyDetails: String {
        return UserBaseURL + "/users/user/updatemasterkey"
    }

    //To reset Update Address
    static var updateAddress: String {
        return UserBaseURL + "/address/updateaddress"
    }

    //To reset Profile Mobile and Email
    static var updateBasicDetails: String {
        return UserBaseURL + "/phonenumber/newnumbersendotp"
    }

    //To delete the Address details
    static var deleteAddressDetails: String {
        return UserBaseURL + "/users/user/deleteaddress"
    }

/*************************************** MeetMe Module API's *****************************************/

    static var contentType: String {
        return "Content-Type"
    }
    static var httpMethodPost: String {
        return "POST"
    }
    static var httpMethodGet: String {
        return "GET"
    }
    static var httpMethodPut: String {
        return "PUT"
    }
    static var httpMethodDelete: String {
        return "DELETE"
    }

    static var meetmeBaseUrl: String {
        return MeetMeBaseURL
    }

    static var meetmeCreateMeetUrl: String {
        return MeetMeBaseURL + "/meetme"
    }
    
    static var deleteNotificationToken: String {
        return MeetMeBaseURL + "/deletenotificationtoken"
    }

    static var meetMeNotificationsUrl: String {
        return MeetMeBaseURL + "/meetmenotifications"
    }

    static var getMeetmeConfigurationUrl: String {
        return MeetMeBaseURL + "/meetmeconfigurations"
    }

    static var postGeoLocationUrl: String {
        return MeetMeBaseURL + "/meetme/geolocation"
    }

    static func addParticipantUrl(meetMeId: String!) -> String {
        return MeetMeBaseURL + "/meetme/\(meetMeId!)/participants"
    }

    static func updateMeetUrl(meetMeId: String!) -> String {
        return MeetMeBaseURL + "/meetme/\(meetMeId!)"
    }

    static func acceptMeetInvite(meetmeId: String!, userId: String!) -> String {
           return MeetMeBaseURL + "/meetme/\(meetmeId!)/participants/\(userId!)/accept"
    }

    static func declineMeetInvite(meetmeId: String!, userId: String!) -> String {
        return MeetMeBaseURL + "/meetme/\(meetmeId!)/participants/\(userId!)/decline"
    }

    static func removeParticipant(selectedMeetmeId: String!) -> String {
        return  MeetMeBaseURL + "/meetme/\(selectedMeetmeId!)/participants/participant"
    }

    static func changeCapsuleLockUrl(selectedMeetmeId: String!) -> String {
        return  MeetMeBaseURL + "/meetme/\(selectedMeetmeId!)/participants/participant"
    }

    static func getGeoLocationsOfMeet(selectedMeetmeId: String!) -> String {
        return  MeetMeBaseURL + "/meetme/geolocation?meetmeid=\(selectedMeetmeId!)"
    }

    static func getMeetCapsule(selectedMeetmeId: String!) -> String {
        return  MeetMeBaseURL +  "/meetme/\(selectedMeetmeId!)/capsule"
    }

    static func createMeetMessage(selectedMeetmeId: String!) -> String {
        return  MeetMeBaseURL +  "/meetme/\(selectedMeetmeId!)/capsule"
    }

    static func capsuleCountUrl(selectedMeetmeId: String!) -> String {
        return  MeetMeBaseURL +  "/meetme/\(selectedMeetmeId!)/capsulecount"
    }

    static var getMeetMeTrackingOptions: String {
        return  MeetMeBaseURL + "/meetme/trackingoptions"
    }

    static func getAllMeetings(upcomingFlagApiCall: String!, searchKeyApiCall: String!) -> String {
        return  MeetMeBaseURL +  "/meetme?isupcoming=\(upcomingFlagApiCall!)&searchKey=\(searchKeyApiCall!)"
    }

    static func getMyMeetings(upcomingFlagApiCall: String!, searchKeyApiCall: String!) -> String {
        return  MeetMeBaseURL +   "/meetme/mymeetings?isupcoming=\(upcomingFlagApiCall!)&searchKey=\(searchKeyApiCall!)"
    }

    //To save fcm token corresponding to user
    static var deleteUsersPreviousLocation: String {
        return MeetMeBaseURL + "/meetme/geolocation"
    }

    static func AddUnsentCapsuleMessagesURL(meetmeId: String!) -> String {
        return MeetMeBaseURL + "/meetme/" + meetmeId + "/addunsetcapsule"
    }
    
    static func ReduceMeetNotificationCount() -> String {
        return MeetMeBaseURL + "/reducemeetnotificationcount"
    }

/************************************** Pulse Module API's ********************************************/

    //To get request type of pulses by or for logged in user
    static var getRequestTypePulsesAPI: String {
        return PulseBaseURL + "/pulse/getrequesttypepulselogbook"
    }

    //to get share type of pulses by or for logged in user
    static var getShareTypeOfPulsesAPI: String {
        return PulseBaseURL + "/pulse/getsharetypepulselogbool"
    }

    //To get past requests to or by logged in user
    static var getPastRequestedPulseAPI: String {
        return PulseBaseURL + "/pulse/getpastrequestpulselogbook"
    }

    //To get past shared pulse by or to the logged in user
    static var getPastSharedPulseAPI: String {
        return PulseBaseURL + "/pulse/getpastsharepulselogbook"
    }

    //To get pulse details
    static var getPulseDetailsAPI: String {
        return PulseBaseURL + "/pulse/pulsedetails"
    }

    // To request a pulse now
    static var requestAPulseNowAPI: String {
        return PulseBaseURL + "/pulse/now/request"
    }

    // To share a pulse now
    static var shareAPulseNowAPI: String {
        return PulseBaseURL + "/pulse/now/share"
    }

    // To request a pulse at scheduled time
    static var requestAPulseAtScheduledTimeAPI: String {
        return PulseBaseURL + "/pulse/schedule/request"
    }

    // To share a pulse at scheduled time
    static var shareAPulseAtScheduledTimeAPI: String {
        return PulseBaseURL + "/pulse/schedule/share"
    }

    // To decline a pulse request
    static var declineAPulseAPI: String {
        return PulseBaseURL + "/pulse/updatepulse/updatepulseisaccepted"
    }

    // To delete a pulse
    static var deletePulseAPI: String {
        return PulseBaseURL + "/pulse/deletepulse"
    }

    // To update a scheduled pulse (as no cannot be updated)
    static var updatePulseAPI: String {
        return PulseBaseURL + "/pulse/schedule/update"
    }

    // To accept a pulse now(accept by sharing location as response to location request)
    static var acceptAPulseWithLocationAPI: String {
        return PulseBaseURL + "/pulse/now/responsetorequest"
    }

    //To accept a pulse to share location at scheduled time
    static var acceptAPulseToShareLocationAtscheduledTimeAPI: String {
        return PulseBaseURL + "/pulse/schedule/respondtoscheduledrequest"
    }

    //To get user master key and match with entered master key
    //need to creat in pulse module seperately as we are following microservices architecture
    static var getMasterKeyAPI: String {
        return UserBaseURL + "/users/user/getmasterkey"
    }

/***************************************** Syn API's **********************************************/

    // To update contact FE Ids
    static var updateFEIdsAPI: String {
        return SyncBaseURL + "/contacts/updatefeid"
    }

    // To sync contact to backend
    static var syncContactsAPI: String {
        return SyncBaseURL + "/contacts"
    }

    // To get List of owner Contacts
    static var getAllOwnerContacts: String {
        return SyncBaseURL + "/contacts"
    }

      // To sync contact to backend
      static var syncContactsEncryptAPI: String {
            return SyncBaseURL + "/synccontactswithencrypt"
      }

      // To get List of owner Contacts
      static var getAllOwnerDecryptContacts: String {
            return SyncBaseURL + "/synccontactswithencrypt"
      }

    // To get Particular contact
    static var getContactObj: String {
        return SyncBaseURL + "/contacts/contact"
    }

      // To get Particular contact
      static var getUpdatedContacts: String {
            return SyncBaseURL + "/getupdatedcontacts"
      }

      // To get Particular contact
      static var getBockedByContactList: String {
            return SyncBaseURL + "/getblockedbycontacts"
      }

      // To get Particular contact
      static var blockUnblockOperation: String {
            return SyncBaseURL + "/blockunblockoperation"
      }

      // To change privacy of Particular contact
      static var changePrivacySetting: String {
            return SyncBaseURL + "/changeprivavacysetting"
      }

    /* -------------------------------  Sync Constants  -------------------------------- */

    //To Send maximum no. of contacts in Sync contacts API & update FE- Id API
    var sendMaximumNoOfContacts: Int {
        return 100
    }

/**************************************  Lookout Module API's *****************************************/

      // To get Particular contact
      static var giveLookoutUserFeedback: String {
            return  LookoutBaseURL + "/lookoutfeedback"
      }

/************************************* Screen Names for Google Analytics ******************************/

       // Sync Screen variables
      static var SyncViewController: String {
            return "Sync Module - Sync contacs listing page"
      }

       static var addContactPersonalInfoVC: String {
            return "Sync Module - Add contact Personal information page"
      }

       static var addContactPostalInfoVC: String {
          return  "Sync Module - Add contact Postal information page"
      }

       static var addContactProfesionalInfoVC: String {
          return  "Sync Module - Add contact Professional information page"
      }

       static var addContactGeneralInfoVC: String {
            return "Sync Module - Add contact General information page"
      }

       static var UpdateContactPersonalInfoVC: String {
          return  "Sync Module - Update contact Personal information page"
      }

       static var UpdateContactPostalInfoVC: String {
          return  "Sync Module - Update contact Postal information page"
      }

       static var UpdateContactProfessionalInfoVC: String {
          return  "Sync Module - Update contact Professional information page"
      }

       static var UpdateContactGeneralInfoVC: String {
          return  "Sync Module - Update contact General information page"
      }

       static var AdrressAddEditPopUpVC: String {
          return  "Sync Module - Sync contacts address add/edit pop-up"
      }

       static var SyncActionPopUpViewController: String {
         return   "Sync Module - multi purpose pop-up"
      }

      // Profile module
      static var ProfileBasicViewController: String {
            return   "Profile Module - Basic information page"
      }

      static var ProfileAddressViewController: String {
            return   "Profile Module - Address information page"
      }

      static var ProfileSecurityVC: String {
            return   "Profile Module - Security page"
      }

      static var ProfileAddressEditingPopUpVC: String {
            return   "Profile Module - Address add/edit pop-up"
      }

      static var ProfileVerificationVC: String {
            return   "Profile Module - Verify email / phoneNumber"
      }

      static var ProfileChangeMasterKeyVC: String {
            return   "Profile Module - Change master key pop-up"
      }

      static var MasterKeyOTPVerificationVC: String {
            return   "Profile Module - OTP verification page after master key mismatch during change master key "
      }

      static var ProfileChangePasswordVC: String {
            return   "Profile Module - Change password pop-up"
      }

      static var DeleteMyProfileVC: String {
            return   "Profile Module - Delete profile confirmation pop-up"
      }

      static var DeleteVerificationVC: String {
            return   "Profile Module - Verify OTP page during delete my profile"
      }

      static var ProfileDeleteAddressVC: String {
            return   "Profile Module - Delete address confirmation pop-up"
      }

      // Sign up module's
      static var SignUpMyProfileVC: String {
            return   "Sign-Up Module - SignUp with phoneNumber & Email & set password page"
      }

      static var TermsAndConditionsVC: String {
            return   "Sign-Up Module - Terms & Conditions page"
      }

      static var SignUPOTPVC: String {
            return   "Sign-Up Module - OTP verification page during sign-up"
      }

      static var SignUpMasterKeyVC: String {
            return   "Sign-Up Module - Enter master key page"
      }

      static var SignUpSecurityQuestionVC: String {
            return   "Sign-Up Module - Enter security question answers"
      }

      static var SignUpWithSocialLogin: String {
            return   "Sign-Up Module - Social signUp with phoneNumber & Email"
      }

      // Forgot password
      static var ForgetPasswordScreen1VC: String {
            return   "Forgot Password - Enter phoneNumber & Email page to receive OTP"
      }

      static var ForgetPWDSecurityQuestionVC: String {
            return   "Forgot Password - Answer security questions page when wrong information provided"
      }

      static var ResetPasswordSetNewPwdVC: String {
            return   "Forgot Password - Reset password page after successful verification"
      }

      static var RedirectionControllerVC: String {
            return   "Login - Redirection page from spash screen to dashboard / login page"
      }

      static var LoginViewController: String {
            return   "Login page"
      }

      static var BaseViewController: String {
            return   "Dashboard page"
      }

      static var AppInfo: String {
            return   "AppInfo"
      }

      static var MapViewController: String {
            return   "Location map"
      }

      // Pop-ups
      static var AllPopupDualActionVC: String {
            return   "Dual action pop-up"
      }

      static var BasicPopUpViewController: String {
            return   "Basic Pop-up"
      }

      static var ActionPopUpViewController: String {
            return   "Action pop-up"
      }

      // Meet-Me Module
      static var MeetMeLogBook: String {
            return   "Meet Me Module - Meet Me logbook page"
      }

      static var CreateMeetVC: String {
            return   "Meet Me Module - Create meet page"
      }

      static var EditMeetVC: String {
            return   "Meet Me Module - Edit meet page"
      }

      static var MeetMeDetailVC: String {
            return   "Meet Me Module - Meet details page"
      }

      static var SearchContactsVC: String {
            return   "Meet Me Module - Search contacts page to add in meet"
      }

      static var EditParticipantsVc: String {
            return   "Meet Me Module - Add / Remove participants from meet"
      }

      static var MeetMeCapsuleVC: String {
            return   "Meet Me Module - Meet me capsule page"
      }

      static var MeetMeTrackingOptionDialogueVC: String {
            return   "Meet Me Module - Meet me tracking options page"
      }

      static var PulseListingViewController: String {
            return   "Pulse Module - Pulse logbook page"
      }

      static var PulseDetailsViewController: String {
            return   "Pulse Module - Pulse details page"
      }

      static var ShareAndRequestPulseVCViewController: String {
            return   "Pulse Module - Share & Request pulse page"
      }

      static var capsuleInsertPopUpViewController: String {
            return   "Pulse Module - Insert capsule page"
      }

      static var MasterKeyPopUpViewController: String {
            return   "Pulse Module - Enter master key pop-up"
      }

}
