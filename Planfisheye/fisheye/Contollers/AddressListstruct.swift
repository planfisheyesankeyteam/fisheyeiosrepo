//
//  AddressListstruct.swift
//  fisheye
//
//  Created by Sankey Solutions on 02/12/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

struct AddressListstruct {
    
    var addressTranslatedText: String!
    var addressActualText: String!
}
