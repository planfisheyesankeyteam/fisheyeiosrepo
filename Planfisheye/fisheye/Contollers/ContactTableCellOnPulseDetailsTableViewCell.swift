//
//  ContactTableCellOnPulseDetailsTableViewCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 08/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class ContactTableCellOnPulseDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var recipientEmail: UILabel!
    @IBOutlet weak var recipientPhoneNumber: UILabel!
    @IBOutlet weak var recipientImage: UIImageView!
    @IBOutlet weak var recipientName: UILabel!
    @IBOutlet weak var viewContainingCell: UIView!
    @IBOutlet weak var seperationBtwPhoneNumberAndEmail: UIView!
    @IBOutlet weak var singleDetailDisplay: UILabel!
    @IBOutlet weak var stackViewDetails: UIView!
    @IBOutlet weak var isFisheyeIndicationView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
