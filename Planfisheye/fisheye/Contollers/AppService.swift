//
//  AppService.swift
//  fisheye
//
//  Created by Sankey Solution on 11/13/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

public class AppService {
    
    // Declaring regex and messages
    static let shared: AppService = AppService()
    
    var noOfUnreadMeetNotifications: Int = 0
    var trasportModeArray = [ModeofTransport(translatedText:"Walking",actualText:"Walking"), ModeofTransport(translatedText:"Driving",actualText:"Driving")]
      
    var localTransportModeArray = [ModeofTransport]();
      
      var localtrackingOptionArray = [TrackingOption(isTimeRequired:false,optionName:"Before the meet",optionTranslatedName:"Before the meet",trackingTime:[5,10,15]),
                                 TrackingOption(isTimeRequired:false,optionName:"After the meet",optionTranslatedName:"After the meet",trackingTime:[5,10,15]),
                                 TrackingOption(isTimeRequired:false,optionName:"At the start",optionTranslatedName:"At the start",trackingTime:[5,10,15]),
                                 TrackingOption(isTimeRequired:false,optionName:"Never",optionTranslatedName:"Never",trackingTime:[5,10,15])]
      
      var meetmeTrackingOptionList:  [MeetMeTrackingOptionsList] = []
    
    
    
    //  Address Types to use in Profile and Sync   //
    var addressTypes: [String] = ["Home", "Work"]
    
    var addressTypeArray = [AddressListstruct(addressTranslatedText:"Home",addressActualText:"Home"), AddressListstruct(addressTranslatedText:"Work",addressActualText:"Work")]
    
    var localAddressType = [AddressListstruct]()

//      contacts.append(Person(name: "Jack", surname: "Johnson", phone: 2, isCustomer: false))
  
     var to = "to"
    //AWS S3 Image URL
    var awsImageStorageUrl = "https://s3.eu-west-2.amazonaws.com/"
    var awsS3ImageStorageBucket = "fisheye-content-storage/"
    var awsS3ImageStorageProfileFolder = "ProfileImages/"
    //end
    
    var nameLengthRegex = "^[a-zA-Z]{3}$"
    var nameLengthRegexMessage = "Name should contain atleast 3 characters"
    
    var emailRegex = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    var emailRegexMessage = "Email address should be in valid format"
    
    var passwordRegex = "/^(?=.*[0-9])(?=.*?[A-Za-z])[a-zA-Z0-9]{6,}$/"
    var passwordRegexMessage = "Password must contain alpha numeric characters"
    
    var passwordLengthRegex = "^.{6,}$"
    var passwordLengthRegexMessage = "Password length should be mininum 6 characters"
    
    var confirmPasswordRegex = ""
    var confirmPasswordRegexMesasge = "Confirm password didn’t match"
    
    var mobileNumberRegex = "[0-9]{10}$"
    var mobileNumberRegexMessage = "Mobile number should contain numeric characters"
    
    var mobileNumberLengthRegex = "^[0-9]{10}$"
    var mobileNumberLengthRegexMessage = "Mobile number should be 10 digit only"
    
    var pincodeRegex = "^[0-9]*$"
    var pincodeRegexMessage = "Pincde should contain numeric characters"
    
    var pincodeLengthRegex = "^[0-9]{6}$"
    var pincodeRegexLengthMessage = "Pincode should be 6 digit only"
    
    var passwdPolicyRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$&*])(?=\\S+$).{6,15}$"
    var passwdPolicyRegexMessage = "Check password policy"
    var notSamePasswdMessage = "Password and confirm password should be same"
    
    var checkInternetConnectionMessage = "Please check your Internet Connection and try again later!"
    var emptyText = "Please type a message!"
    /****************** Meetme Logbook strings ******************/
    var acceptMeetSuccessString = "You have Accepted the Meetme Invitation successfully!"
    var acceptMeetFailureString = "Unable to Accept the MeetMe invitation! Please try again later"
    var acceptMeetLateString = "Sorry, you cannot accept the invite now as the meeting has already begun!"
    
    var acceptMeetParticipentIsNotPartOfMeet = "Sorry, you cannot accept the invite now as you are not part of this meeting!"
    
    var declinedMeetSuccessString = "You have Declined the Meetme Invitation successfully!"
    var declinedMeetFailureString = "Unable to Decline the MeetMe invitation! Please try again later"
    var declinedMeetLateString = "Sorry, you cannot decline the invite now as the meeting has already begun!"
    
    var cancelMeetSuccesString = "You have Cancelled the Meetme successfully!"
    var cancelMeetFailureString = "Unable to Delete the MeetMe! Please try again later"
    
    var createMeetSuccessString = "Meetme created successfully!"
    var createMeetFailureString = "Unable to Create Meetme! Please try again later!"
    
    var stopMeetSuccessString = "Meetme ended successfully!"
    var stopMeetFailureString = "Unable to end Meetme! Please try again later!"
    
    var updateMeetSuccessString = "Meetme updated successfully!"
    var updateMeetFailureString = "Unable to Update Meetme! Please try again later!"
    
    var removeParticipantSuccessString  = "Participant removed succesfully from Meet"
    var removeParticipantFailureString = "Unable to Remove the Participant from Meetme! Please try again later"
    
    var maxParticipantLimitReachedString = ""
    var deleteMeetSuccess = "Meet is succesfully cancelled!"
    var deleteMeetFaiure = "Unable to delete Meet!"
    
    var contactsAddedSuccess = "Participants added to Meetme successfully"
    var contactsAddedFailure = "Unable to add Participants to Meetme!Please try again later"
    
    var noCapsuleFoundMessage = "No Capsule found!"
    var capsuleLocked = "Capsule locked"
    var capsuleUnlocked = "Capsule unlocked"
    
    //Labels
    var meetDateLabel = "Today"
    var meetDateLabel2 = "Tomorrow"
    var earliestTrackingLabel = "Earliest track"
    
    var DeleteMeetme = "Delete Meetme"
    var DeleteMeetmeSubHeading = "Are you sure you want to cancel this meet?"
    
    var declineMeet = "Decline Meet"
    var declineMeeting = "Are you sure you want to decline invitation to this meet?"
    
    var meetMe = "Meet me"
    var meetmeLogbook = "Meetme Logbook"
    var myMeetingsLabel = "My Meetings"
    var allMeetingsLabel = "All Meetings"
    var createMeetButton = "Create Meet"
    var noMeetingsLabel = "There are no meetings scheduled for you.You can create your meeting here"
    var noMeetingsLabel2 = "No meetings found with the text"
    var noMeetingsLabel3 = "Error Loading Meetings! Please tap on the tab to try again!"
    
    var userNotPartOfTheMeeting = "Sorry!! You are not part of the meeting,so we cannot show you meeting capsules."
    
    // ------------------------------------------------------ MeetMeDetailVC --------------------------------------------- //
    
    var DeclinemeetHeading = "Decline Meetme"
    var DeclineSubHeading = "Are you sure you want to decline invitation to this meet?"
    
    var DeleteMeetHeading = "Delete Meetme"
    var DeleteMeetSubHeading = "Are you sure you want to cancel this meet?"
    
    var removeParticipant = "Remove Participant"
    var  removeParticipantSubHeading = "Are you sure you want to remove this participant from the Meetme?"
    
    var stopMeeting = "Stop Meeting"
    var stopMeetingSubHeading = "Are you sure you want to end this meet?"
    
    var participantsLabel = "Participants"
    var earliestTracking = "Earliest track"
    var unlockCapsuleLabel = "Unlock Capsule"
    var orgnizer = "Organizer"
    var accepted = "Accepted"
    var notYetResponded = "Not Yet Responded"
    var declined = "Declined"
    
    var dontHaveHostLocation = "We don't have host location as host tracking is not started,meeting is stooped or meet is expired"
    
    // ------------------------------------------------------  CreateMeetVC --------------------------------------------- //
    var participantNameLabel = "Unknown"
    
    var selectStratTime = "Select Start Time"
    var done = "Done"
    
    var selectEndTime = "Select End Time"
    
    var selectStartDate = "Select Start Date"
    
    var selectEndDate = "Select End Date"
    
    var invalidTitle = "Invalid title"
    
    var title =  "Title"
    
    var invalidLocation = "Invalid location"
    
    var earliestTrack = "Earliest track: "
    
    var trackingMin = "mins"
    ////earlistTime backend getter setter in front end at API consume
    //trackingMin
    var detailsIncomplete = "Details Incomplete"
    
    var createMeet = "Create Meet"
    var createMeetSubHeding = "Would you like to cancel this setup and go back or continue with this meeting request?"
    var location = "Location"
    
    var locateMap = "Tap to locate on map"
    
    var startAt = "Starts at"
    
    var endsAt = "Ends at"
    
    var addParticipant = "Add Participants"
    
    var partcanInvite = "Participants can invite others"
    
    var startTracking = "Start Tracking"
    
    var time = "Time"
    
    var createanewmeet = "Create a new Meet"
    
    var goBack2 = "Go Back"
    
    var quit = "Quit"
    
    var searchForPlaces = "Search for place"
    
    // ------------------------------------------------------  SearchContactsVC --------------------------------------------- //
    var noContactsSelectedLabel = "You haven't selected any contacts to add in your meet"
    var contactOnFisheye = "Contacts on Fisheye"
    var contactOnPhone = "Contacts from Phone"
    var save = "SAVE"
    
    var noContactsLabel = "No contacts retrieved from your phone. But you may add your contacts manually"
    
    var noContactsLabel2 = "No Contacts found with the text"
    
    // ------------------------------------------------------  MeetMeCapsuleVC --------------------------------------------- //
    var enterMessage = "Enter Message"
    
    // ------------------------------------------------------  MeetMeCapsuleVC --------------------------------------------- //
    var editmeetmee = "Edit MeetMe"
    
    // ------------------------------------------------------  MeetMeNotification --------------------------------------------- //
    var yesterday = "Yesterday"
    var meetMeNotification = "Meetme Notifications"
    var noNotification = "No Notifications"
    
    // ------------------------------------------------------  Terms & Conditions URL  --------------------------------------------- //
    
    var privacyPolicyURL = "https://app.termly.io/document/privacy-notice/682a5643-9315-42b3-9a3e-cbd7b918dbc8"
    var marketplacePolicyURL = "https://app.termly.io/document/terms-of-use-for-online-marketplace/87dc4dbe-aa4e-4edf-be3f-f53bd6f8094e"
    var cookiePolicyURL = "https://app.termly.io/document/cookie-policy/b4a80685-c739-436a-94ed-c0b1136d73fc"
    var disclaimerPolicyURL = "https://app.termly.io/document/disclaimer/d26f51c9-cb70-45c0-8e1e-66fadd14626d"
    var policyUpdatesURL = "https://s3.eu-west-2.amazonaws.com/fisheye-content-storage/ProfileImages/policyUpdates.html"
    
    // Contact Us link
    var contactUsLink: String = "https://www.fisheyehub.net/contact-us"
    
    // Common labels and messages
    var cancel: String = "Cancel"
    var continue2: String = "Continue"
    var ok: String = "Ok"
    var contactUsHeader: String = "Contact Us"
    var termsNConditions: String = "Terms and Conditions"
    var letsWorkTogether: String = "Let's work together"
    var version: String = "Version"
    
    
    var pressLongToSelectLocation: String = "Press long to select location"
    
    var walking: String = "Walking"
    var driving: String = "Driving"
    
    
    // Toast Timers //
    var firstTimeInterval: Double = 1.0
    var secondTimeInterval: Double = 2.0
    var thirdTimeInterval: Double = 3.0
    var meetmeSearchContactTimeInterval: Double = 2.0
    
    // Languagesmode
    var languages: [String] = ["English", "Spanish", "Marathi", "Hindi", "Bengali", "French", "Arabic", "German", "Italian", "Dutch", "Japenese", "Russia", "Korean"]
    var meetingCreatedSuccess: String = "Meeting created successfully!"
    var chooseYourTrackingOption: String = "Choose your tracking option"
    var mode: String = "Mode"
    var past: String = "PAST"
        //var noMeeting: String = "There are no meetings scheduled for you.You can create your meeting here "
    var profileImageBorderColor: String  = "ffffff"
    var carouselCardColor: String = "ffffff"
    var noOfCarouselCards: Int = 3
    var noOfTabsToShowInTopRibbon: Int = 3
      
  
      var undelivered: String = "Undelivered"
      var sent:String = "sent"
      var readBy:String = "read by some"
      var readByall:String = "read by all"
      
    
}
