//
//  PulseLogbookNoteViewCell.swift
//  fisheye
//
//  Created by Sylveon on 28/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class PulseLogbookNoteViewCell: UITableViewCell {

    @IBOutlet weak var pulseTitleLabelOnNote: UILabel!

    //@IBOutlet weak var pulseLabelOnNote: UILabel!

    @IBOutlet weak var hisPulseImageOnNote: UIImageView!

    @IBAction func dismissBtn(_ sender: Any) {
    }

    @IBAction func successBtn(_ sender: Any) {
    }

    //@IBOutlet weak var dismissBtn: UIButton!

    @IBOutlet weak var pulseTimeOnNote: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
