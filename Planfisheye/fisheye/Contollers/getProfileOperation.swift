//
//  getProfileOperation.swift
//  fisheye
//
//  Created by sankeyMacOs on 18/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

struct PreviousProfileObject {
    init() {
        
    }
    var phonenumber: String?
    var countryCode: String?
    var email: String?
    var privacyEnabled: Bool?
}

struct AddressType {
    init() {
        
    }
    var locality: String?
    var addressId: String?
    var street_address: String?
    var region: String?
    var formatted: String?
    var type: String?
    var country: String?
    var postal_code: String?
}

struct Profile {
    init() {
        
    }
    var name: String?
    var phonenumber: String?
    var id: String?
    var addresses: [AddressType] = [AddressType]()
    var countryCode: String?
    var signupMethod: String?
    var email: String?
    var privacyEnabled: Bool?
    var picture: String?
    var gender: String?
    var userMeetmeNotificationCount: Int?
    var userPulseNotificationCount: Int?
    var isMarketingMessagingEnabled: Bool?
    var masterKey: String?
}

class getProfileOperation: Procedure {
    
    let appSharedPrefernce = AppSharedPreference()
    var profileObject = Profile()
    var idtoken = ""
    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }
    
    override func execute() {
        let awsURL = AppConfig.getFisheyeUserProfile
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue("getuserdetails", forHTTPHeaderField: "action")
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodGet
        request.httpBody =  "".data(using: String.Encoding.utf8)
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                
                let statusCode = data["statusCode"] as? String ?? ""
                if(statusCode == "0") {
                    if let user = data["user"] as? [String: Any] {
                        self.profileObject.addresses = []
                        if let addresses = user["addresses"] as? [[String: Any]] {
                            for i  in 0 ..< addresses.count {
                                var addressItem = AddressType()
                                addressItem.locality = addresses[i]["locality"] as? String ?? ""
                                addressItem.addressId =  addresses[i]["addressId"] as? String ?? ""
                                addressItem.street_address = addresses[i]["street_address"] as? String ?? ""
                                addressItem.region =  addresses[i]["region"]  as? String ?? ""
                                addressItem.formatted =  addresses[i]["formatted"] as? String ?? ""
                                addressItem.type =  addresses[i]["type"] as? String ?? ""
                                addressItem.country = addresses[i]["country"] as? String ?? ""
                                addressItem.postal_code = addresses[i]["postal_code"] as? String ?? ""
                                self.profileObject.addresses.append(addressItem)
                            }
                        }
                        self.profileObject.name = user["name"] as? String ?? ""
                        self.profileObject.phonenumber = user["phonenumber"] as? String ?? ""
                        self.profileObject.id = user["id"] as? String ?? ""
                        self.profileObject.countryCode = user["countryCode"] as? String ?? ""
                        self.profileObject.signupMethod = user["signupMethod"] as? String ?? ""
                        self.profileObject.email = user["email"] as? String ?? ""
                        self.profileObject.privacyEnabled = user["privacyEnabled"] as? Bool
                        self.profileObject.isMarketingMessagingEnabled = user["isMarketingMessagingEnabled"] as? Bool ?? false
                        self.profileObject.picture = user["picture"] as? String ?? ""
                        self.profileObject.gender = user["gender"] as? String ?? ""
                        self.profileObject.userMeetmeNotificationCount = user["userMeetMeNotifications"] as? Int
                        self.profileObject.userPulseNotificationCount = user["userPulseNotifications"] as? Int
                        self.profileObject.masterKey = user["masterKey"] as? String ?? ""
                        
                        let fisheyeName = user["name"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeName", value: fisheyeName) as? String ?? ""
                        var fisheyePhoto = ""
                        if( user["picture"] != nil) {
                            fisheyePhoto = user["picture"] as? String ?? ""
                        }
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyePhoto", value: fisheyePhoto)
                        let genderSelection = user["gender"]  as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "gender", value: genderSelection)
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeUserObject", value: user)
                        
                    }
                }
                
                self.finish()
                
            } else {
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

class editBasicInformationOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var profileObject = Profile()
    let appConfig = AppConfig()
    var idtoken = ""
    var isImportantDetailsChanged: Bool = false
    
    init(profile: Profile) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        profileObject = profile
    }
    
    var awsURL = AppConfig.updateFisheyeBasicUserProfile
    
    override func execute() {
        let parameters =
            [
                "action": "updateuserdetails",
                "idtoken": idtoken,
                "user": [
                    "name": profileObject.name,
                    "email": profileObject.email,
                    "phonenumber": profileObject.phonenumber,
                    "picture": profileObject.picture,
                    "countryCode": profileObject.countryCode,
                    "privacyEnabled": profileObject.privacyEnabled,
                    "gender": profileObject.gender,
                    "isMarketingMessagingEnabled": profileObject.isMarketingMessagingEnabled
                ],
                "isImportantDetailsChanged": isImportantDetailsChanged
                
                ] as [String: Any]
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
         let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                
                let responseStatus = data["statusCode"] as? String ?? ""
                if responseStatus == "200"{
                    if let user = data["user"] as? [String: Any] {
                        self.profileObject .addresses = []
                        if let addresses = user["addresses"] as? [[String: Any]] {
                            for i  in 0 ..< addresses.count {
                                var addressItem = AddressType()
                                addressItem.locality = addresses[i]["locality"] as? String ?? ""
                                addressItem.addressId =  addresses[i]["addressId"] as? String ?? ""
                                addressItem.street_address = addresses[i]["street_address"] as? String ?? ""
                                addressItem.region =  addresses[i]["region"]  as? String ?? ""
                                addressItem.formatted =  addresses[i]["formatted"] as? String ?? ""
                                addressItem.type =  addresses[i]["type"] as? String ?? ""
                                addressItem.country = addresses[i]["country"] as? String ?? ""
                                addressItem.postal_code = addresses[i]["postal_code"] as? String ?? ""
                                self.profileObject.addresses.append(addressItem)
                            }
                        }
                        self.profileObject.name = user["name"] as? String ?? ""
                        self.profileObject.phonenumber = user["phonenumber"] as? String ?? ""
                        self.profileObject.id = user["id"] as? String ?? ""
                        self.profileObject.countryCode = user["countryCode"] as? String ?? ""
                        self.profileObject.signupMethod = user["signupMethod"] as? String ?? ""
                        self.profileObject.email = user["email"] as? String ?? ""
                        self.profileObject.privacyEnabled = user["privacyEnabled"] as? Bool
                        self.profileObject.picture = user["picture"] as? String ?? ""
                        self.profileObject.gender = user["gender"] as? String ?? ""
                        self.profileObject.userMeetmeNotificationCount = user["userMeetMeNotifications"] as? Int
                        self.profileObject.userPulseNotificationCount = user["userPulseNotifications"] as? Int
                        let fisheyeName = user["name"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeName", value: fisheyeName) as? String ?? ""
                        var fisheyePhoto = ""
                        if( user["picture"] != nil) {
                            fisheyePhoto = user["picture"] as? String ?? ""
                        }
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyePhoto", value: fisheyePhoto)
                        let genderSelection = user["gender"]  as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "gender", value: genderSelection)
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeUserObject", value: user)
                        
                    }
                }
                self.appSharedPrefernce.setAppSharedPreferences(key: "editBasicProfileStatusCode", value: responseStatus)
                if let user = data["user"] as? [String: Any] {
                    let variable = user["picture"] as? String ?? ""
                    self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeUserObject", value: user )
                    self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyePhoto", value: variable)
                }
                self.finish()
                
            } else {
                
                self.appSharedPrefernce.setAppSharedPreferences(key: "editBasicProfileStatusCode", value: "NetworkError")
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

// Creating structure to change Password
struct Password {
    init() {
        
    }
    var newPassword: String?
    var password: String?
}
// End of structure to change Password

//Operation to change Password

class changePasswordOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var passwordObject = Password()
    var idtoken = ""
    init(password: Password) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        passwordObject = password
    }
    
    var awsURL = AppConfig.changePasswordAPI
    
    override func execute() {
        let parameters =
            [
                "action": "changepassword",
                "idtoken": idtoken,
                "newPassword": passwordObject.newPassword,
                "password": passwordObject.password
                ] as [String: Any]
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                let responseStatus = data["statusCode"] as? String ?? ""
                self.appSharedPrefernce.setAppSharedPreferences(key: "editChangePassword", value: responseStatus)
                self.finish()
                
            } else {
                
                self.appSharedPrefernce.setAppSharedPreferences(key: "editChangePassword", value: "NetworkError")
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
//End of Operation to change Password

// Creating structure to change MasterKey
struct MasterKey {
    init() {
        
    }
    var masterkey: String?
    var newMasterKey: String?
}
// End of structure to change Password

//Operation to change MasterKey

class changeMasterKeyOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var masterKeyObject = MasterKey()
    var idtoken = ""
    init(masterKey: MasterKey) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        masterKeyObject = masterKey
    }
    
    var awsURL = AppConfig.resetMasterKeyDetails
    
    override func execute() {
        let parameters =
            [
                "action": "updatemasterkey",
                "idtoken": idtoken,
                "masterkey": masterKeyObject.masterkey,
                "newMasterKey": masterKeyObject.newMasterKey
                ] as [String: Any]
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                
                let data = json
                let statusCodeRecieved = data["statusCode"] as? String ?? ""
                self.appSharedPrefernce.setAppSharedPreferences(key: "editChangeMasterKey", value: statusCodeRecieved)
                self.finish()
                
            } else {
                self.appSharedPrefernce.setAppSharedPreferences(key: "editChangePassword", value: "NetworkError")
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
//End of Operation to change Password

//Operation to Add Address

class addAddressOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var addressObject = AddressType()
    var idtoken = ""
    init(address: AddressType) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        addressObject = address
    }
    
    var awsURL = AppConfig.addAddressAPI
    
    override func execute() {
        let parameters =
            [
                "action": "addaddress",
                "idtoken": self.idtoken,
                "address":
                    [
                        "formatted": addressObject.formatted,
                        "street_address": addressObject.street_address,
                        "locality": addressObject.locality,
                        "region": addressObject.region,
                        "postal_code": addressObject.postal_code,
                        "country": addressObject.country,
                        "type": addressObject.type
                ]
                ] as [String: Any]
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                
                let responseStatus = data["statusCode"] as? String ?? ""
                self.appSharedPrefernce.setAppSharedPreferences(key: "addAddressStatus", value: responseStatus)
                self.finish()
                
            } else {
                
                self.appSharedPrefernce.setAppSharedPreferences(key: "addAddressStatus", value: "NetworkError")
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
//End of Operation to Add Address

// Creating Operation to Update Address

class updateAddressOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var addressObject = AddressType()
    var idtoken = ""
    init(address: AddressType) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        addressObject = address
    }
    
    var awsURL = AppConfig.updateAddress
    
    override func execute() {
        let parameters =
            [
                "idtoken": self.idtoken,
                "address":
                    [
                        "addressId": addressObject.addressId,
                        "formatted": addressObject.formatted,
                        "street_address": addressObject.street_address,
                        "locality": addressObject.locality,
                        "region": addressObject.region,
                        "postal_code": addressObject.postal_code,
                        "country": addressObject.country,
                        "type": addressObject.type
                ]
                ] as [String: Any]
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                let statusCodeRecieved = data["statusCode"] as? String ?? ""
                self.appSharedPrefernce.setAppSharedPreferences(key: "updateAddressStatus", value: statusCodeRecieved)
                self.finish()
                
            } else {
                self.appSharedPrefernce.setAppSharedPreferences(key: "updateAddressStatus", value: "NetworkError")
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
//End of Operation to Update Address

// Creating structure to Delete Address
struct DeleteAddress {
    init() {
        
    }
    var addressId: String?
}
// End of structure to Delete Address

//Operation to Delete Address

class deleteAddressOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var deleteObject = DeleteAddress()
    var idtoken = ""
    init(deleteAddress: DeleteAddress) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        deleteObject = deleteAddress
    }
    
    var awsURL = AppConfig.deleteAddressDetails
    
    override func execute() {
        let parameters =
            [
                "action": "deleteAddress",
                "idtoken": idtoken,
                "addressId": deleteObject.addressId
                ] as [String: Any]
        
        let awsURL = AppConfig.deleteAddressDetails
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                let statusCodeRecieved = data["statusCode"] as? String ?? ""
                self.appSharedPrefernce.setAppSharedPreferences(key: "deleteAddress", value: statusCodeRecieved)
                self.finish()
                
            } else {
                self.appSharedPrefernce.setAppSharedPreferences(key: "deleteAddress", value: "NetworkError")
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
        
    }
}
//End of Operation to Delete Address

class GetTranslatedText: Procedure {
    
    let appSharedPrefernce = AppSharedPreference()
    let profilemsg = ProfileToastMsgHeadingSubheadingLabels()
    let signUpMsg = SignupToastMsgHeadingSubheadingLabels()
    let syncmsg = SyncToastMessages()
    let pulsemsg = PulseToastMsgHeadingSubheadingLabels()
    let lookoutmsg = LookoutToastMessages()
    let appService = AppService()
    let forgetmsg = ForgotPasswordTexts()
    var updateLanguage = ""
    //       @IBOutlet var radialView: RadialGradientView!
    
    init(language: String) {
        super.init()
        
        updateLanguage = language
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            UIApplication.shared.endIgnoringInteractionEvents()
            ScreenLoader.shared.stopLoader()
        }
        
    }
    
    override func execute() {
        
        var awsURL = AppConfig.getTranslatedText
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(updateLanguage, forHTTPHeaderField: "preferedLanguage")
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                if(data["statusCode"] as? String ?? "" == "0") {
                    if let responseObj = json as? [String: Any] {
                        if let allTranslatedText = responseObj["languageTranslationObj"] as? [String: Any] {
                            //   NotificationCenter.default.post(name: CHANGE_LANGUAGE, object: nil)
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.signIn = allTranslatedText["signIn"] as? String ?? ""
                            ForgotPasswordTexts.shared.letsWorkTogether = allTranslatedText["letsWorkTogether"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.letsWorkTogether = allTranslatedText["letsWorkTogether"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.newUserSignUp = allTranslatedText["newUserQus"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.meetMe = allTranslatedText["meetMe"] as? String ?? ""
                            AppService.shared.allMeetingsLabel = allTranslatedText["allMeetings"] as? String ?? ""
                            AppService.shared.myMeetingsLabel = allTranslatedText["myMeetings"] as? String ?? ""
                            AppService.shared.createMeet = allTranslatedText["createMeet"] as? String ?? ""
                            AppService.shared.createMeetButton = allTranslatedText["createMeet"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.credintialsinvalid = allTranslatedText["loginDetailsIncorrect"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.letsWorkTogetherLabel = allTranslatedText["letsWorkTogether"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.optionalCapsuleMsg = allTranslatedText["optional"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.startDate = allTranslatedText["startDate"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.letsWorkTogether = allTranslatedText["letsWorkTogether"] as? String ?? ""
                            AppService.shared.letsWorkTogether = allTranslatedText["letsWorkTogether"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.letsWorkTogether = allTranslatedText["letsWorkTogether"] as? String ?? ""
                            
                            AppService.shared.meetmeLogbook = allTranslatedText["meetmeLogbook"] as? String ?? ""
                            AppService.shared.noMeetingsLabel = allTranslatedText["thereAreNoMeeting"] as? String ?? ""
                            AppService.shared.noMeetingsLabel2 = allTranslatedText["noMeetingsFound"] as? String ?? ""
                            
                            AppService.shared.noMeetingsLabel3 = allTranslatedText["noMeetingsLabel3"] as? String ?? ""
                            
                            AppService.shared.noMeetingsLabel2 = allTranslatedText["noMeetingsFound"] as? String ?? ""
                              
                            AppService.shared.removeParticipant = allTranslatedText["removeParticipant"] as? String ?? ""
                              
                              AppService.shared.removeParticipantSubHeading = allTranslatedText["removeParticipantSubHeading"] as? String ?? ""
                              
                              AppService.shared.updateMeetSuccessString = allTranslatedText["meetmeUpdatedSuccessfully"] as? String ?? ""
                              
                            AppService.shared.earliestTracking = allTranslatedText["earliestTrack"] as? String ?? ""
                            AppService.shared.earliestTrackingLabel = allTranslatedText["earliestTrack"] as? String ?? ""
//                             let transportModes = allTranslatedText["transportMode"] as? [[String: Any]] ?? AppService.shared.trasportModeArray
                              
                              if let trackingOptionList = allTranslatedText["trackingOptions"] as? [[String: Any]] {
                                    
                                     MeetMeTrackingOptions.trackingOption = []
                                     AppService.shared.localtrackingOptionArray = []     

                                      for trackOption in trackingOptionList
                                      {
                                         
                                          let isTimeRequiredBackend = trackOption["isTimeRequired"] as? String ?? ""
                                          let isDeletedBacked = trackOption["isDeleted"] as? String ?? ""
                                          let optionTranslatedBacked = trackOption["optionTranslatedName"] as? String ?? ""
                                          let optionActualText = trackOption["optionName"] as? String ?? ""
                                          var trackingOptionStructure = TrackingOption()
                                          // trackingOptionStructure.isDeleted = trackOption["isDeleted"] as? Int
                                          trackingOptionStructure.isTimeRequired = trackOption["isTimeRequired"] as? Bool
                                          trackingOptionStructure.optionTranslatedName = trackOption["optionTranslatedName"] as? String ?? ""
                                          trackingOptionStructure.optionName = trackOption["optionName"] as? String ?? ""
                                          trackingOptionStructure.trackingTime = trackOption["trackingTime"] as? NSArray
                                          MeetMeTrackingOptions.trackingOption.append(trackingOptionStructure)
                                        
                                    AppService.shared.localtrackingOptionArray.append(trackingOptionStructure)
                                         
                                          
                                    }


                              }
    
                            if let addressList =  allTranslatedText["addressTypeList"] as? [[String: Any]]{
                              AppService.shared.addressTypes = []
                              AppService.shared.localAddressType = []
                                
                                for address in addressList{
                                    let addressTranslatedText = address["addressTranslatedText"] as? String ?? ""
                                    var addressListstruct = AddressListstruct()
                                    addressListstruct.addressActualText = address["addressActualText"] as? String ?? ""
                                   
                                    addressListstruct.addressTranslatedText = address["addressTranslatedText"] as? String ?? ""
                                    
                                    AppService.shared.localAddressType.append(addressListstruct)
                                    AppService.shared.addressTypes.append(addressTranslatedText)
                                    
                                    
                                }
                                
                            }
                            
                            if let genderList = allTranslatedText["genderArray"] as? [[String: Any]] {
                                
                                SignupToastMsgHeadingSubheadingLabels.shared.gender = []
                                SignupToastMsgHeadingSubheadingLabels.shared.localGenderArrayList = []
                                SignupToastMsgHeadingSubheadingLabels.shared.genderArrayList = []
                                
                                for genderArrayList in genderList{
                                    
                                    let genderTranslatedText = genderArrayList["genderTranslatedText"] as? String ?? ""
                                    
                                    var genderListStructure =  GenderListStructure()
                                    
                                    genderListStructure.genderActualText = genderArrayList["gender"] as? String ?? ""
                                    
                                    genderListStructure.genderTranslatedText = genderArrayList["genderTranslatedText"]  as? String ?? ""
                                    
                                    
                                    SignupToastMsgHeadingSubheadingLabels.shared.localGenderArrayList.append(genderListStructure)
                                    
                                    SignupToastMsgHeadingSubheadingLabels.shared.genderArrayList.append(genderTranslatedText)
                                   
                                }
                            }
                            
                              if let transportModes = allTranslatedText["transportMode"] as? [[String: Any]] {
                                    MeetMeTrackingOptions.trackingMode = []
                                    AppService.shared.localTransportModeArray = []
                                    AppService.shared.trasportModeArray = []
                                
                                    for transportMode in transportModes {
                                          let traslatedText = transportMode["translatedText"] as? String ?? ""
                                         
                                          var modeofTransport = ModeofTransport()
                                          modeofTransport.actualText = transportMode["mode"] as? String ?? ""
                                          modeofTransport.translatedText = transportMode["translatedText"] as? String ?? ""
                                          AppService.shared.localTransportModeArray.append(modeofTransport)
                                          AppService.shared.trasportModeArray.append(modeofTransport)
                                          MeetMeTrackingOptions.trackingMode.append(traslatedText)
                                          //MeetMeTrackingOptions.trackingMode.append(actualText)
                                    }
                              }
                            
                          
                            ForgotPasswordTexts.shared.enterPasscodeSent = allTranslatedText["enterTheSixDigit"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enteremail = allTranslatedText["enterEmail"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enteremailid = allTranslatedText["enterEmail"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.entermobilenumber = allTranslatedText["enterMobileNumber"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.email = allTranslatedText["email"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.code = allTranslatedText["countryCode"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.genderNonConforming = allTranslatedText["genderNonconforming"] as? String ?? ""
                              
                             SignupToastMsgHeadingSubheadingLabels.shared.pleaseUpdateSecurityQuestion = allTranslatedText["pleaseUpdateSecurityQuestion"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.genderNonConforming = allTranslatedText["genderNonconforming"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.passwordPolicy = allTranslatedText["passwordPolicy"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.registrationsuccess = allTranslatedText["registrationCompleted"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.securityQuestionThree = allTranslatedText["memorableCityName"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.securityQuestionTwo = allTranslatedText["yourFavoriteBook"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.passwordPolicy = allTranslatedText["passwordPolicy"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.masterkeyplaceholder = allTranslatedText["currentMasterKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.user = allTranslatedText["user"] as? String ?? ""
                            ForgotPasswordTexts.shared.enterNewPasswordHeader = allTranslatedText["enterNewPassword1"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.deleteProfileLabel = allTranslatedText["deleteAccount"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.chnagePasswordLabel = allTranslatedText["changePassword"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.newmasterkey = allTranslatedText["newMasterKey"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.entercurrentmasterkey = allTranslatedText["enterCurrentMasterKey"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.entercurrentpswd = allTranslatedText["enterCurrentPassword"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.entercurrentpswd = allTranslatedText["enterCurrentPassword"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.enterMasterKeyTitle = allTranslatedText["enterMasteKey"] as? String ?? ""
                            AppService.shared.createMeetSuccessString = allTranslatedText["meetmeCreatedSuccessfully"] as? String ?? ""
                            AppService.shared.stopMeetSuccessString = allTranslatedText["stopMeetSuccessString"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.deleteprofilesubheading = allTranslatedText["sureToDeleteProfile"] as? String ?? ""
                            
                            AppService.shared.unlockCapsuleLabel = allTranslatedText["unlockCapsule"] as? String ?? ""
                            AppService.shared.meetDateLabel2 = allTranslatedText["tomorrow"] as? String ?? ""
                            AppService.shared.meetDateLabel = allTranslatedText["today"] as? String ?? ""
                            AppService.shared.yesterday = allTranslatedText["yesterday"] as? String ?? ""
                            AppService.shared.title = allTranslatedText["title"] as? String ?? ""
                            
                            
                            AppService.shared.declinedMeetSuccessString = allTranslatedText["declinedMeetSuccessString"] as? String ?? ""
                              
                            AppService.shared.acceptMeetSuccessString = allTranslatedText["acceptMeetSuccessString"] as? String ?? ""
                              
                              AppService.shared.contactsAddedSuccess = allTranslatedText["contactsAddedSuccess"] as? String ?? ""
                              
                              
                              SyncToastMessages.shared.title = allTranslatedText["title"] as? String ?? ""
                              
                            
                            AppService.shared.location = allTranslatedText["location"] as? String ?? ""
                            AppService.shared.startAt = allTranslatedText["startsAt"] as? String ?? ""
                            AppService.shared.endsAt = allTranslatedText["endsAt"] as? String ?? ""
                            AppService.shared.addParticipant = allTranslatedText["addParticipants"] as? String ?? ""
                            AppService.shared.partcanInvite = allTranslatedText["participantsCanInvite"] as? String ?? ""
                            AppService.shared.startTracking = allTranslatedText["startTracking"] as? String ?? ""
                            AppService.shared.time = allTranslatedText["time"] as? String ?? ""
                            AppService.shared.chooseYourTrackingOption = allTranslatedText["chooseYourTrackingOption"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.personalInfoNotShared = allTranslatedText["personalInfoIsNot"] as? String ?? ""
                            AppService.shared.createMeetSubHeding = allTranslatedText["ifYouGoBack"] as? String ?? ""
                            AppService.shared.goBack2 = allTranslatedText["goBack1"] as? String ?? ""
                            AppService.shared.cancel = allTranslatedText["cancel"] as? String ?? ""
                            AppService.shared.goBack2 = allTranslatedText["continues"] as? String ?? ""
                            AppService.shared.continue2 = allTranslatedText["continues"] as? String ?? ""
                            // self.signUpMsg.meetMe = allTranslatedText["goBack2"] as? String ?? ""
                            SyncToastMessages.shared.cancel = allTranslatedText["cancel"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pickerCancel = allTranslatedText["cancel"] as? String ?? ""
                            AppService.shared.selectEndDate = allTranslatedText["selectEndDate"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseDeletionSubHeading = allTranslatedText["wishToCancelPulse"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pickerCancel = allTranslatedText["cancel"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.cancel = allTranslatedText["cancel"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.delete = allTranslatedText["delete"] as? String ?? ""
                            
                            
                            
                            
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.endDatePickerTitle = allTranslatedText["selectEndDate"] as? String ?? ""
                            AppService.shared.selectStratTime = allTranslatedText["selecStartTime"] as? String ?? ""
                            AppService.shared.selectEndTime = allTranslatedText["selectEndTime"] as? String ?? ""
                            AppService.shared.selectStartDate = allTranslatedText["selectStartDate"] as? String ?? ""
                            AppService.shared.pressLongToSelectLocation = allTranslatedText["pressLongToSelectLocation"] as? String ?? ""
                            AppService.shared.searchForPlaces = allTranslatedText["searchForPlaces"] as? String ?? ""
                            AppService.shared.createanewmeet = allTranslatedText["createNewMeet"] as? String ?? ""
//                            AppService.shared.meetMe = allTranslatedText["newUdoneserQus"] as? String ?? ""
                            AppService.shared.noContactsLabel2 = allTranslatedText["noContactsFoundWith"] as? String ?? ""
                            AppService.shared.noContactsSelectedLabel = allTranslatedText["youHaveNotSelected"] as? String ?? ""
                            AppService.shared.save = allTranslatedText["save"] as? String ?? ""
                            AppService.shared.invalidTitle = allTranslatedText["invalidTitle"] as? String ?? ""
                            AppService.shared.invalidLocation = allTranslatedText["invalidLocation"] as? String ?? ""
                            AppService.shared.meetMeNotification = allTranslatedText["meetmeNotifications"] as? String ?? ""
                            SyncToastMessages.shared.noContactsFoundFromSearch = allTranslatedText["noMatchingSync"] as? String ?? ""
                            AppService.shared.participantsLabel = allTranslatedText["participants"] as? String ?? ""
                            AppService.shared.enterMessage = allTranslatedText["enterMessage"] as? String ?? ""
//                            AppService.shared.meetMe = allTranslatedText["noCapsuleFound"] as? String ?? ""
                            AppService.shared.checkInternetConnectionMessage = allTranslatedText["pleaseCheckYourInternetTAL"] as? String ?? ""
                            AppService.shared.undelivered = allTranslatedText["undelivered"] as? String ?? ""
                            AppService.shared.sent = allTranslatedText["sent"] as? String ?? ""
                            AppService.shared.readBy = allTranslatedText["readBy"] as? String ?? ""
                            AppService.shared.readByall = allTranslatedText["readByall"] as? String ?? ""
                            AppService.shared.walking = allTranslatedText["walking"] as? String ?? ""
                            AppService.shared.driving = allTranslatedText["driving"] as? String ?? ""
                              
                            ProfileToastMsgHeadingSubheadingLabels.shared.networkfailureMsg = allTranslatedText["pleaseCheckNetworkConnection"] as? String ?? ""

                            AppService.shared.cancelMeetSuccesString = allTranslatedText["youHaveCancelledMeetme"] as? String ?? ""
                            AppService.shared.editmeetmee = allTranslatedText["editMeetme"] as? String ?? ""
                            // self.signUpMsg.meetMe = allTranslatedText["user"] as? String ?? ""
                            AppService.shared.meetMe = allTranslatedText["meetMe"] as? String ?? ""
                            AppService.shared.declineMeeting = allTranslatedText["sureToDeclineInvitation"] as? String ?? ""
                            AppService.shared.continue2 = allTranslatedText["continues"] as? String ?? ""
                            AppService.shared.DeclinemeetHeading = allTranslatedText["declineMeetme"] as? String ?? ""
                            AppService.shared.locateMap = allTranslatedText["tapToLocateOnMap"] as? String ?? ""
                            AppService.shared.DeleteMeetmeSubHeading = allTranslatedText["sureToCancelMeet"] as? String ?? ""
                        
                            
                            AppService.shared.ok = allTranslatedText["ok"] as? String ?? ""
                            AppService.shared.stopMeetingSubHeading = allTranslatedText["sureToEndmeet"] as? String ?? ""
                            AppService.shared.detailsIncomplete = allTranslatedText["detailsIncomplete"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.sync = allTranslatedText["sync"] as? String ?? ""
                            SyncToastMessages.shared.sync = allTranslatedText["sync"] as? String ?? ""
                            SyncToastMessages.shared.syncContacts = allTranslatedText["syncContacts"] as? String ?? ""
                              
                            SyncToastMessages.shared.name = allTranslatedText["name"] as? String ?? ""
                            AppService.shared.contactOnFisheye = allTranslatedText["contactsOnFisheye"] as? String ?? ""
                              
                              AppService.shared.contactOnFisheye = allTranslatedText["contactsOnFisheye"] as? String ?? ""
                              
                              AppService.shared.stopMeeting = allTranslatedText["stopMeeting"] as? String ?? ""
//                              AppService.shared.noMeeting = allTranslatedText["thereAreNoMeeting"] as? String ?? ""
                              SyncToastMessages.shared.fishEyeContacts = allTranslatedText["contactsOnFisheye"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.fishEyeContactLabel = allTranslatedText["contactsOnFisheye"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.noPhoneNoEmail = allTranslatedText["noPhoneNoEmail"] as? String ?? ""
                            
                            SyncToastMessages.shared.recentlyAddedContacts = allTranslatedText["recentlyAdded"] as? String ?? ""
                            SyncToastMessages.shared.call = allTranslatedText["call"] as? String ?? ""
                            SyncToastMessages.shared.whatsAppCall = allTranslatedText["whatsAppCall"] as? String ?? ""
                            SyncToastMessages.shared.facetimeCall = allTranslatedText["skypeCall"] as? String ?? ""
                            SyncToastMessages.shared.pulseRequest = allTranslatedText["pulseRequest"] as? String ?? ""
                            SyncToastMessages.shared.pulseShare = allTranslatedText["pulseShare"] as? String ?? ""
                            SyncToastMessages.shared.blockContact = allTranslatedText["blockContact"] as? String ?? ""
                            SyncToastMessages.shared.unblockContact = allTranslatedText["unblockContact"] as? String ?? ""
                            SyncToastMessages.shared.mergeContact = allTranslatedText["mergeContact"] as? String ?? ""
                            SyncToastMessages.shared.deleteContact = allTranslatedText["deleteContact"] as? String ?? ""
                            SyncToastMessages.shared.unblock = allTranslatedText["unblock"] as? String ?? ""
                            SyncToastMessages.shared.ok = allTranslatedText["oKCapital"] as? String ?? ""
                            AppService.shared.noContactsLabel = allTranslatedText["noContactsRetrieved"] as? String ?? ""
                              
                              AppService.shared.declineMeet = allTranslatedText["declineMeet"] as? String ?? ""
                             
                            SyncToastMessages.shared.noContactsFound = allTranslatedText["noContactsRetrieved"] as? String ?? ""
                            
                            SyncToastMessages.shared.areUSureToBlock = allTranslatedText["sureToBlockSync"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToBlockContact = allTranslatedText["sureToBlockSyncThisWill"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToUnblockContact = allTranslatedText["confirmToUnblockContact"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToUnblock = allTranslatedText["confirmToUnblockThisSyncContact"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToDeleteContact = allTranslatedText["confirmToDeleteSyncContact"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToDelete = allTranslatedText["confirmToDeleteThisSyncContact"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToMergeContact = allTranslatedText["confirmToMergeTwoContacts"] as? String ?? ""
                            SyncToastMessages.shared.with = allTranslatedText["with"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToSync = allTranslatedText["wishToSyncYourIphone"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToMeregeTheseContacts = allTranslatedText["wishToMergeTheseTwoContacts"] as? String ?? ""
                            SyncToastMessages.shared.syncInProgress = allTranslatedText["bearWhileSyncingContacts"] as? String ?? ""
                            SyncToastMessages.shared.syncCompleted = allTranslatedText["syncIsNowUpdated"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.name = allTranslatedText["name"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.name = allTranslatedText["name"] as? String ?? ""
                            SyncToastMessages.shared.name = allTranslatedText["name"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.email = allTranslatedText["email"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.profileEmail = allTranslatedText["email"] as? String ?? ""
                            SyncToastMessages.shared.email = allTranslatedText["email"] as? String ?? ""
                            
                            SyncToastMessages.shared.notAllowedToAddSelfPhoneAndEmailInSync = allTranslatedText["youMayNotAdd"] as? String ?? ""
                            SyncToastMessages.shared.contactAlreadyExists = allTranslatedText["contactAlreadyExistOnSync"] as? String ?? ""
                            SyncToastMessages.shared.contactAlreadyExistsByPhoneOrEmail = allTranslatedText["contactAlreadyExistOnSync"] as? String ?? ""
                            
                            SyncToastMessages.shared.enterName = allTranslatedText["enterName"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.entername = allTranslatedText["enterName"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidphone = allTranslatedText["invalidMobileNumber"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.chnagePasswordLabel = allTranslatedText["changePassword"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.invalidphone = allTranslatedText["invalidMobileNumber"] as? String ?? ""
                            SyncToastMessages.shared.invalidPhone = allTranslatedText["invalidMobileNumber"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidemail = allTranslatedText["invalidEmailAddress"] as? String ?? ""
                              
                              SignupToastMsgHeadingSubheadingLabels.shared.invalidemail = allTranslatedText["invalidEmailAddress"] as? String ?? ""
                            SyncToastMessages.shared.invalidEmail = allTranslatedText["invalidEmailAddress"] as? String ?? ""
                            
                            SyncToastMessages.shared.emptyAddressViewtext = allTranslatedText["youMayAddMultipleAddresses"] as? String ?? ""
                            SyncToastMessages.shared.toPlaceCall = allTranslatedText["ifYouWishToMakeThisCall"] as? String ?? ""
                            SyncToastMessages.shared.toPlaceFacetimeCall = allTranslatedText["ifYouWishToMakeThisSkypeCall"] as? String ?? ""
                            SyncToastMessages.shared.toPlaceWhatsAppCall = allTranslatedText["ifYouWishToMakeThisWhatsappCall"] as? String ?? ""
                            SyncToastMessages.shared.failedToBlockContact = allTranslatedText["unableToBlockBearWithUs"] as? String ?? ""
                            SyncToastMessages.shared.failedToSyncContacts = allTranslatedText["unableToSyncBearWithUs"] as? String ?? ""
                            SyncToastMessages.shared.deleteAddress = allTranslatedText["deleteAddress"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enteremail = allTranslatedText["enterEmail"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enteremail = allTranslatedText["enterEmail"] as? String ?? ""
                            SyncToastMessages.shared.enterEmail = allTranslatedText["enterEmail"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.addressLabel = allTranslatedText["address"] as? String ?? ""
                            SyncToastMessages.shared.address = allTranslatedText["address"] as? String ?? ""
                            
                            SyncToastMessages.shared.companyName = allTranslatedText["companyName"] as? String ?? ""
                            SyncToastMessages.shared.enterCompany = allTranslatedText["enterCompanyName"] as? String ?? ""
                            SyncToastMessages.shared.enterTitle = allTranslatedText["enterTitle"] as? String ?? ""
                            SyncToastMessages.shared.notes = allTranslatedText["notes"] as? String ?? ""
                            SyncToastMessages.shared.submit = allTranslatedText["submit"] as? String ?? ""
                            LookoutToastMessages.shared.submit = allTranslatedText["submit"] as? String ?? ""
                            SyncToastMessages.shared.invalidWebsite = allTranslatedText["invalidWebsite"] as? String ?? ""
                            SyncToastMessages.shared.enterWebsite = allTranslatedText["enterWebsite"] as? String ?? ""
                            SyncToastMessages.shared.enterNotes = allTranslatedText["enterNotes"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.addrestype = allTranslatedText["selectAddressType"] as? String ?? ""
                            SyncToastMessages.shared.selectAddressType = allTranslatedText["selectAddressType"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.pincode = allTranslatedText["postcode"] as? String ?? ""
                            SyncToastMessages.shared.pincode = allTranslatedText["postcode"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.state = allTranslatedText["state"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.deleteaddheading = allTranslatedText["deleteAddress"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.deleteaddsubheading = allTranslatedText["sureToDeleteAddress"] as? String ?? ""
                            
                            
                            SyncToastMessages.shared.state = allTranslatedText["state"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.country = allTranslatedText["country"] as? String ?? ""
                            
                            
                            SyncToastMessages.shared.country = allTranslatedText["country"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.city = allTranslatedText["city"] as? String ?? ""
                              
                            SyncToastMessages.shared.city = allTranslatedText["city"] as? String ?? ""
                            
                            SyncToastMessages.shared.errorWhileFetchingContact = allTranslatedText["higherTrafficUnableToSyncBear"] as? String ?? ""
                            SyncToastMessages.shared.errorWhileFetchingContact = allTranslatedText["higherTrafficUnableToSyncBear"] as? String ?? ""
                            
                            SyncToastMessages.shared.noInternetToUnblock = allTranslatedText["pleaseCheckNetworkConnection"] as? String ?? ""
                            SyncToastMessages.shared.noInternetToBlock = allTranslatedText["pleaseCheckNetworkConnection"] as? String ?? ""
                              
                            SyncToastMessages.shared.name = allTranslatedText["name"] as? String ?? ""
                            
                              
                            SyncToastMessages.shared.failedToDelete = allTranslatedText["higherTrafficUnableToDelete"] as? String ?? ""
                            SyncToastMessages.shared.failedToDelete = allTranslatedText["higherTrafficUnableToDelete"] as? String ?? ""
                            
                            SyncToastMessages.shared.phoneRequired = allTranslatedText["noPhoneNumberAvailable"] as? String ?? ""
                            SyncToastMessages.shared.phoneOrEmailRequired = allTranslatedText["noPhoneEmailIsAvailableForContact"] as? String ?? ""
                            SyncToastMessages.shared.confirmationOfPrivacyWillEnabledInProfile = allTranslatedText["confirmationOfPrivacyWillEnabledInProfile"] as? String ?? ""
                            SyncToastMessages.shared.confirmationOfPrivacyWillDisabledInProfile = allTranslatedText["confirmationOfPrivacyWillDisabledInProfile"] as? String ?? ""
                            SyncToastMessages.shared.addPhone = allTranslatedText["addPhone"] as? String ?? ""
                            SyncToastMessages.shared.addEmail = allTranslatedText["addEmail"] as? String ?? ""
                            
                            AppService.shared.capsuleLocked = allTranslatedText["capsuleLocked"] as? String ?? ""
                             AppService.shared.done = allTranslatedText["done"] as? String ?? ""
                            AppService.shared.capsuleUnlocked = allTranslatedText["capsuleUnlocked"] as? String ?? ""
                            ForgotPasswordTexts.shared.forgotPwdPrimaryHeader = allTranslatedText["forgotYourPassword"] as? String ?? ""
                            ForgotPasswordTexts.shared.forgotPwdSecondaryHeader = allTranslatedText["enterYourEmailMobile"] as? String ?? ""
                            ForgotPasswordTexts.shared.youWillReceive = allTranslatedText["youWillRecieveSecurityKey"] as? String ?? ""
                            ForgotPasswordTexts.shared.sendPasscode = allTranslatedText["sendSecurityKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.Phoneno = allTranslatedText["mobileNumber"] as? String ?? ""
                            SyncToastMessages.shared.phone = allTranslatedText["mobileNumber"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.otpValidFor = allTranslatedText["securityKeyValidFor"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.noInternet = allTranslatedText["checkYourNetworkConnectivity"] as? String ?? ""
                            AppService.shared.checkInternetConnectionMessage = allTranslatedText["checkInternetConnection"] as? String ?? ""
                            
                            AppService.shared.dontHaveHostLocation = allTranslatedText["dontHaveHostLocation"] as? String ?? ""
                           
                            
                            AppService.shared.acceptMeetParticipentIsNotPartOfMeet = allTranslatedText["acceptMeetParticipentIsNotPartOfMeet"] as? String ?? ""
                            
                             AppService.shared.userNotPartOfTheMeeting = allTranslatedText["userNotPartOfTheMeeting"] as? String ?? ""
                            
                            
                            AppService.shared.contactOnPhone = allTranslatedText["contactFromPhone"] as? String ?? ""
                               
                            SignupToastMsgHeadingSubheadingLabels.shared.emailphonenumbernotmatch = allTranslatedText["emailPhonDoNotMatchregistered"] as? String ?? ""
                            ForgotPasswordTexts.shared.resetPasswordHeader = allTranslatedText["resetPassword"] as? String ?? ""
                            ForgotPasswordTexts.shared.securityQuesAnsesHeader = allTranslatedText["answerSecurityQuestions"] as? String ?? ""
                            ForgotPasswordTexts.shared.tryAgain = allTranslatedText["pleaseTryAgain"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.minutes = allTranslatedText["minutes"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.minutes = allTranslatedText["minutes"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterotp = allTranslatedText["enterSecurityKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.enterotp = allTranslatedText["enterSecurityKey"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.resend = allTranslatedText["resend"] as? String ?? ""
                            
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidotp = allTranslatedText["invalidSecurityKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.securityKey = allTranslatedText["securityKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.incorrectanswer = allTranslatedText["incorrectAnswer"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.twoUsersFoundWhileSignUp = allTranslatedText["anotherUserWithThisEmailMobile"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.otpsent = allTranslatedText["securityKeySentSuccess"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.emailid = allTranslatedText["emailAddress"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.password = allTranslatedText["password"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.reenterpassword = allTranslatedText["reenterNewPassword"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterReEnterPassword = allTranslatedText["reenterNewPassword"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.retypepassword =  allTranslatedText["reenterNewPassword"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.incorrectPassword = allTranslatedText["incorrectPassword"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.paswordmismatch = allTranslatedText["incorrectPassword"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.inValidPassword = allTranslatedText["invalidPassword"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.renterpassword = allTranslatedText["passwordMismatch"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.errorpasswordchange = allTranslatedText["unableToCompletepPasswordChange"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.paswordchangesuccess = allTranslatedText["passwordChanged"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.otpValidFor = allTranslatedText["securityKeyValidFor"] as? String ?? ""
                            
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.otpTimerPart1 = allTranslatedText["securityKeyValidFor"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.noInternet = allTranslatedText["checkYourNetworkConnectivity"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.usernotfound = allTranslatedText["userIDProvidedNotFound"] as? String ?? ""
                            AppService.shared.checkInternetConnectionMessage = allTranslatedText["checkInternetConnection"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.masterkeyFailure = allTranslatedText["checkInternetConnection"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.networkfailureMsg = allTranslatedText["checkInternetConnection"] as? String ?? ""
                            AppService.shared.checkInternetConnectionMessage = allTranslatedText["checkInternetConnection"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.emailphonenumbernotmatch = allTranslatedText["emailPhonDoNotMatchregistered"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.phoneNumberShouldBeValid = allTranslatedText["phoneNumberShouldBeValid"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.smssendingfailed = allTranslatedText["failedToSendSecurityKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.securiyquestionfailed = allTranslatedText["securityQuestionsLoadingFailed"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.answerone = allTranslatedText["answer"] as? String ?? ""
                              
                            SignupToastMsgHeadingSubheadingLabels.shared.questionone = allTranslatedText["provideAnswer"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.invalidpassword = allTranslatedText["invalidPassword2"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.inValidPassword = allTranslatedText["invalidPassword2"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.fisheye = allTranslatedText["fisheyeMainLabel"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.fisheyeLabel = allTranslatedText["fisheyeMainLabel"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseLogbookTitle = allTranslatedText["pulseLogbook"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.myReuestsTapTitle = allTranslatedText["myRequests"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.sharesTapTitle = allTranslatedText["shares"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.capsuleMsgPlaceHolder = allTranslatedText["past"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pastButtonTitle = allTranslatedText["past"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.capsuleMsgPlaceHolder = allTranslatedText["enterYourMessage100Characters"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.capsuleLockedUnlockedLabel = allTranslatedText["capsuleIsOpen"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.enterYourCapsuleLabel = allTranslatedText["enterCapsule"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.addCapsuleLabel = allTranslatedText["addMessage"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.insertCapsuleLabel = allTranslatedText["addMessage"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.configOnErrorEnterMasterKey = allTranslatedText["useYourMasterKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.masterkey = allTranslatedText["masterKey"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.configOnErrorClearedMasterKey = allTranslatedText["masterKey"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.masterkeymismatch = allTranslatedText["incorrectMasterKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.keymismatch = allTranslatedText["incorrectMasterKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.reentermasterkey = allTranslatedText["reenter4DigitMasterKey"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.invalidmk = allTranslatedText["incorrectMasterKey"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.configOnErrorInvalidMasterKey = allTranslatedText["incorrectMasterKey"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.warningToEnterMasterKey = allTranslatedText["notProceedWithoutMasterKey"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.noContactChoosenLabel = allTranslatedText["notChoosenAnyContact"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.nowPulse = allTranslatedText["now"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.schedulePulse = allTranslatedText["scheduled"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pickerDone = allTranslatedText["done"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.pulsetime = allTranslatedText["hhmm"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.endDate = allTranslatedText["endDate"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.noContactFound = allTranslatedText["noContactFound"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.shareAndRequestPageTitle = allTranslatedText["pulse"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.pulse = allTranslatedText["pulse"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.addMasterKey = allTranslatedText["addMasterKey"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.requestTabOfShareAndRequestScrren = allTranslatedText["request"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.shareTabOfShareAndRequestScrren = allTranslatedText["share"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseRepeaterLabel = allTranslatedText["repeat"] as? String ?? ""
                            
                            SyncToastMessages.shared.addContact = allTranslatedText["addContact"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.addContactLabel = allTranslatedText["addContact"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.addContactHeading = allTranslatedText["addContact"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.sundayOneLetter = allTranslatedText["sun"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.mondayOneLetter = allTranslatedText["mon"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.tuesdayOneLetter = allTranslatedText["tue"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.wedOneLetter = allTranslatedText["wed"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.nowPulseRequest = allTranslatedText["locationRequestSent"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.nowPulseShare = allTranslatedText["requestedLocationShare"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.scheduledPulseRequest = allTranslatedText["scheduledRequestSent"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.scheduledPulseShare = allTranslatedText["requestedScheduledShare"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.nowPulseReqAccepted = allTranslatedText["locationRequestAccepted"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.schedPulseReqAccepted = allTranslatedText["scheduledLocationRequestAccepted"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.schedPulseReqAccepted = allTranslatedText["scheduledLocationRequestAccepted"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseUpdated = allTranslatedText["scheduledPulseUpdated"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.serverErrorOfPulse = allTranslatedText["networkErrorContactIfPersists"] as? String ?? ""
                            AppService.shared.accepted = allTranslatedText["accepted"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseAccepted = allTranslatedText["accepted"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.acceptedPulseRecipients = allTranslatedText["accepted"] as? String ?? ""
                            
                            AppService.shared.declined = allTranslatedText["declined"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseDeclined = allTranslatedText["declined"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.declinedPulseRecipients = allTranslatedText["declined"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.notRespondedRecipients = allTranslatedText["awaitingResponse"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.needToUnblockContToSendPulseReq = allTranslatedText["needToUnblockContToSendPulseReq"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.needToUnblockContToSendPulseShare = allTranslatedText["needToUnblockContToSendPulseShare"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.capLockNeedMasterKey = allTranslatedText["lockedCapsuleUseMasterKey"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.checkInternetConnection = allTranslatedText["noNetworkConnectionDetected"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.emptyCapWhileRequestingPulse = allTranslatedText["noMessagingCapsule"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.emptyCapWhileSharingPulse = allTranslatedText["noMessagingCapsule"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.labelBetweenSDandED = allTranslatedText["to"] as? String ?? ""
                            
                            AppService.shared.orgnizer = allTranslatedText["organizer"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.labelBetweenEDandPT = allTranslatedText["at"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseExpired = allTranslatedText["expired"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.addMasterKeyInProfile = allTranslatedText["addMasterKeyInProfile"] as? String ?? ""
                           
                            
                            SyncToastMessages.shared.addContact = allTranslatedText["addContact1"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.addContactLabel = allTranslatedText["addContact1"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.addContactHeading = allTranslatedText["addContact1"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.pulseDeletionHeading = allTranslatedText["deletePulse"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.tellDeclineMeansHeading = allTranslatedText["declinePulse"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.tellAcceptMeansHeading = allTranslatedText["acceptPulse"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.tellAcceptMeansSubHeading = allTranslatedText["sharingLocationRequestorWishToContinue"] as? String ?? ""
                            AppService.shared.quit = allTranslatedText["quit"] as? String ?? ""
                            // SyncToastMessages.shared.enterCompany = allTranslatedText["enterCompanyName"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.capLabel = allTranslatedText["capsule"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.username = allTranslatedText["enterUsername"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.userexistlogin = allTranslatedText["alreadyRegisteredOnFisheyeContinue"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.userexist = allTranslatedText["userIDAlreadyStartedRegistration"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.servererror = allTranslatedText["higherTrafficBearWithUs"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.userProfile = allTranslatedText["userProfile"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.selectGenderPlaceholder = allTranslatedText["selectGender"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.selectGender = allTranslatedText["selectGender"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.male = allTranslatedText["male"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.male = allTranslatedText["male"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.female = allTranslatedText["female"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.female = allTranslatedText["female"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.transFemale = allTranslatedText["transFemale"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.transFemale = allTranslatedText["transFemale"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.biGender = allTranslatedText["biGender"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.biGender = allTranslatedText["biGender"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.nonBinary = allTranslatedText["nonBinary"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.nonBinary = allTranslatedText["nonBinary"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.undisclosed = allTranslatedText["undisclosed"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.undisclosed = allTranslatedText["undisclosed"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.ratherNotSay = allTranslatedText["ratherNotSay"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.ratherNotSay = allTranslatedText["ratherNotSay"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.acceptanceOFTNCHeader = allTranslatedText["acceptanceOfTAndC"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.acceptanceOfTNCText = allTranslatedText["byAcceptingTandCIAgree"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.disclaimerPolicy = allTranslatedText["disclaimerPolicy"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.cookiePolicyFor = allTranslatedText["cookiePolicy"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.termsOfUse = allTranslatedText["termsOfUseForFEcosystem"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.privacyPolicy = allTranslatedText["privacyPolicy"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.agree = allTranslatedText["agree"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.disagree = allTranslatedText["disagree"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.emailotp = allTranslatedText["emailSecurityKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.enteremailOTP = allTranslatedText["yourEmailKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.entermobotp = allTranslatedText["yourSMSKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.mobileotp = allTranslatedText["smsSecurityKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidemailotp = allTranslatedText["invalidEmailKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidmobotp = allTranslatedText["invalidsmsSecurityKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.otpverificationfailed = allTranslatedText["securityVerificationFailed"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterSixDigitKey = allTranslatedText["pleaseEnter6DigitKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.resend = allTranslatedText["resend"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterMasterKeyHeader = allTranslatedText["enterMasterKey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterFourDigitMasterKey = allTranslatedText["enter4DigitMasterkey"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.masterKeyPurpose = allTranslatedText["usedToAuthenticatePrivateMessages"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.entermasterkey = allTranslatedText["provide4DigitMasterKey"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.invalidmk = allTranslatedText["incorrectMasterKey2"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.masterkeymismatch = allTranslatedText["incorrectMasterKey2"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.keymismatch = allTranslatedText["incorrectMasterKey2"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.configOnErrorInvalidMasterKey = allTranslatedText["incorrectMasterKey2"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.errormasterkey = allTranslatedText["unableToRegisterYourMasterKeys"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.masterkey = allTranslatedText["masterKey2"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.configOnErrorClearedMasterKey = allTranslatedText["masterKey2"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.securityQuestions = allTranslatedText["securityQuestions"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.securityQuestionOne = allTranslatedText["yourSchoolName"] as? String ?? ""
                            ForgotPasswordTexts.shared.errorWhileRecoveringPwd = allTranslatedText["unableToRecoverPassword"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.userexitphonenumber = allTranslatedText["contactAlreadyExistsOnFisheye"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.profileUpdate = allTranslatedText["yourProfileUpdated"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.privacydetaildisable = allTranslatedText["disabledPersonalInformationVisible"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.privacyinfoshared = allTranslatedText["personalInformationVisible"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.persInfoshared = allTranslatedText["personalInformationVisible"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.privacydetailenabled = allTranslatedText["buttonEnabledPersonalInfoBlocked"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.privacymessagenotshared = allTranslatedText["personalInformationSharingBlocked"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.entercode = allTranslatedText["enterCode"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.code = allTranslatedText["code"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.codePlaceholder = allTranslatedText["code"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.phone = allTranslatedText["phoneNo"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.phoneNumberPlaceholder = allTranslatedText["phoneNo"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.privacyenabled = allTranslatedText["privacyEnabled"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.privacydisabled = allTranslatedText["privacyDisabled"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.privacyEnaDisaText = allTranslatedText["enablingPrivacyPersonalInfoWillNotShown"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.promotionalCommText = allTranslatedText["optionToReceiveMessagesfromFE"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.promotionalCommNote = allTranslatedText["noteFisheyeOperatesOnZeroTolerance"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.promotionalCommTitle = allTranslatedText["promotionalCommunication"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.basicInformation = allTranslatedText["basicInformation"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.emptyAddressListlabel = allTranslatedText["youCanAddMultipleAddresses"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.addAddressLabel = allTranslatedText["addAddress"] as? String ?? ""
                            SyncToastMessages.shared.addAddress = allTranslatedText["addAddress"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.securityLabel = allTranslatedText["security"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.chnageMasterKey = allTranslatedText["changeMasterKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.currentpassword = allTranslatedText["currentPassword"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.newmasterkey = allTranslatedText["newMasterKey"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.enternewmk = allTranslatedText["enterNewMasterKey"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.masterKeyAdded = allTranslatedText["masterKeyAdded"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidcurrentpswd = allTranslatedText["incorrectCurrentPassword"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.newpassword = allTranslatedText["newPassword"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.enternewpwd = allTranslatedText["enterNewPassword"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterpassword = allTranslatedText["enterNewPassword"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidpwd = allTranslatedText["newPasswordMismatch"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.confirmpswd = allTranslatedText["confirmPassword"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.newandretypepswd = allTranslatedText["newPasswordsMismatch"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.deleteprofileheading = allTranslatedText["deleteProfile"] as? String ?? ""
                            SyncToastMessages.shared.delete = allTranslatedText["delete"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.deleteButtonTitle = allTranslatedText["delete"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.otpsendtoemailPhone = allTranslatedText["sending6digitSecurityKeysRegistered"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.verificationsuccess = allTranslatedText["verificationCompleted"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.profiledeletesuccess = allTranslatedText["profileDataDeletedFisheyeSystem"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.unabletodelete = allTranslatedText["unableToCompleteProfileDeletion"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.pleaseEnterPart1Label = allTranslatedText["enter6DigitCodeSent"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.verificationLabel = allTranslatedText["verification"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToDeleteAddress = allTranslatedText["wishToDeleteSyncContactsAddress"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.logout = allTranslatedText["logout"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.myProfile = allTranslatedText["myProfile"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.lookout = allTranslatedText["lookout"] as? String ?? ""
                            AppService.shared.contactUsHeader = allTranslatedText["contactUs"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.fisheyeStatement = allTranslatedText["fisheyeStatement"] as? String ?? ""
                            AppService.shared.termsNConditions = allTranslatedText["termsAndConditions"] as? String ?? ""
                            AppService.shared.version = allTranslatedText["version"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.forgotPassword = allTranslatedText["forgotPassword"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.loginusernmae = allTranslatedText["username"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.noEmailForSocialAccount = allTranslatedText["thereIsNoEmailAssociated"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterValidCredentials = allTranslatedText["enterValidCredentials"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.invalidplaceholder = allTranslatedText["incorrectEmailAddress"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.loginsuccess = allTranslatedText["loginSuccessful"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.registrationincomplete = allTranslatedText["looksLikeYouStarted"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.usernotfoundcred = allTranslatedText["loginDetailsIncorrectRequestToCheckDetails"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.pleaseloginwithsocial = allTranslatedText["useLoginDetails"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.skip = allTranslatedText["skip"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.deleteaddsubheading = allTranslatedText["sureToDeleteAddress"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.addressdelete = allTranslatedText["addressDeleted"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.addressaddedsuccess = allTranslatedText["addressUpdated"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.updatemasterkey =
                                allTranslatedText["masterKeyUpdated"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.masterkeymismatch = allTranslatedText["incorrectProvideCorrectMasterKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.enterpincode = allTranslatedText["enterPostcode"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.logoutConfirmation = allTranslatedText["sureToLogout"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.orLbl = allTranslatedText["or"] as? String ?? ""
                            AppService.shared.noNotification = allTranslatedText["noNotifications"] as? String ?? ""
                            LookoutToastMessages.shared.failedToAddFeedback = allTranslatedText["unableToAddFeedback"] as? String ?? ""
                            LookoutToastMessages.shared.successInAddingFeedback = allTranslatedText["responseSubmitted"] as? String ?? ""
                            LookoutToastMessages.shared.hi = allTranslatedText["hi"] as? String ?? ""
                            LookoutToastMessages.shared.registeredInterest = allTranslatedText["registerInterest"] as? String ?? ""
                            LookoutToastMessages.shared.thankYouLbl = allTranslatedText["feedbackReceived"] as? String ?? ""
                            LookoutToastMessages.shared.feedbackQuestionOne = allTranslatedText["likeToContinueFisheyeAfterInitialTrial"] as? String ?? ""
                            LookoutToastMessages.shared.feedbackQuestionTwo = allTranslatedText["willingToPayForFisheye"] as? String ?? ""
                            LookoutToastMessages.shared.feedbackQuestionThree = allTranslatedText["happyToMakeAnyFutureCommittment"] as? String ?? ""
                            LookoutToastMessages.shared.feedbackQuestionFour = allTranslatedText["willingToWaitForPremiumVersion"] as? String ?? ""
                            LookoutToastMessages.shared.thanksBeforeSubmitting = allTranslatedText["feedbackHelpUsBuildBetterFisheyeCommunity"] as? String ?? ""
                            LookoutToastMessages.shared.yes = allTranslatedText["yes"] as? String ?? ""
                            LookoutToastMessages.shared.no = allTranslatedText["no"] as? String ?? ""
                            LookoutToastMessages.shared.payForFEOptionOne = allTranslatedText["£10£12£15Year"] as? String ?? ""
                            LookoutToastMessages.shared.payForFEOptionTwo = allTranslatedText["£3£4£5Quarter"] as? String ?? ""
                            LookoutToastMessages.shared.threeMonths = allTranslatedText["months3"] as? String ?? ""
                            LookoutToastMessages.shared.sixMonths = allTranslatedText["months6"] as? String ?? ""
                            LookoutToastMessages.shared.feedbackQuestionFive = allTranslatedText["anySuggestOnFisheye"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.recipient = allTranslatedText["recipient"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.capLabel = allTranslatedText["capsule1"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.capsule = allTranslatedText["capsule1"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.wrongMasterKey = allTranslatedText["incorrectKey"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.invalidnewmasterkey = allTranslatedText["incorrectKey"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.Invalidcurrentmasterkey = allTranslatedText["incorrectKey"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.enterMasterKey = allTranslatedText["useYourKey"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.editCapsuleLabel = allTranslatedText["editMessage"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.editMessage = allTranslatedText["editMessage"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.createPulse = allTranslatedText["createPulse"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.retypepassword = allTranslatedText["reenterNewPassword1"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.reenterpassword = allTranslatedText["reenterNewPassword1"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterReEnterPassword = allTranslatedText["reenterNewPassword1"] as? String ?? ""
                            
                            SignupToastMsgHeadingSubheadingLabels.shared.updateSecurityAnswerToRecoverAccount = allTranslatedText["updateSecurityAnswerToRecoverAccount"] as? String ?? ""
                            
                            
                            AppService.shared.mode = allTranslatedText["mode"] as? String ?? ""
                            AppService.shared.notYetResponded = allTranslatedText["notYetResponded"] as? String ?? ""
                            SyncToastMessages.shared.invalidPinCode = allTranslatedText["invalidPincode"] as? String ?? ""
                            SyncToastMessages.shared.syncInProgress = allTranslatedText["pleaseBearUs"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.selectLanguage = allTranslatedText["selectLanguages"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.creatPulsetext = allTranslatedText["sharingLocation"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.networkFailureMsg = allTranslatedText["pleaseCheckNetworkConnection"] as? String ?? ""
                            AppService.shared.deleteMeetSuccess = allTranslatedText["meetSuccesfullyCancelled"] as? String ?? ""
                            AppService.shared.DeleteMeetme = allTranslatedText["deleteMeetme"] as? String ?? ""
                            AppService.shared.DeleteMeetHeading = allTranslatedText["deleteMeetme"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.enternewpwd = allTranslatedText["enterNewPassword1"] as? String ?? ""
                            SignupToastMsgHeadingSubheadingLabels.shared.enterpassword = allTranslatedText["enterNewPassword1"] as? String ?? ""
                            
                            PulseToastMsgHeadingSubheadingLabels.shared.thruOneLetter = allTranslatedText["thu"] as? String ?? ""
                            SyncToastMessages.shared .enterCompany = allTranslatedText["enterCompanyName"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.fridayOneLetter = allTranslatedText["fri"] as? String ?? ""
                            PulseToastMsgHeadingSubheadingLabels.shared.saturedayOneLetter = allTranslatedText["sat"] as? String ?? ""
                            //                                          NotificationCenter.default.post(name: CHANGE_LANGUAGE, object: nil)
                            
                            SyncToastMessages.shared.home = allTranslatedText["home"] as? String ?? ""
                            
                            SyncToastMessages.shared .work = allTranslatedText["work"] as? String ?? ""
                            // SyncToastMessages.shared .enterCompany = allTranslatedText["addContactPersonalInfo"] as? String ?? ""
                             
                            SyncToastMessages.shared.addContactPersonalInfo = allTranslatedText["addContactPersonalInfo"] as? String ?? ""
                            SyncToastMessages.shared.general = allTranslatedText["general"] as? String ?? ""
                            SyncToastMessages.shared.website = allTranslatedText["website"] as? String ?? ""
                            SyncToastMessages.shared.professional = allTranslatedText["professional"] as? String ?? ""
                            SyncToastMessages.shared.postal = allTranslatedText["postal"] as? String ?? ""
                            SyncToastMessages.shared.personal = allTranslatedText["personal"] as? String ?? ""
                            SyncToastMessages.shared.enterCountry = allTranslatedText["enterCountry"] as? String ?? ""
                            SyncToastMessages.shared.enterPincode = allTranslatedText["enterPinCode"] as? String ?? ""
                            SyncToastMessages.shared.enterState = allTranslatedText["enterState"] as? String ?? ""
                            SyncToastMessages.shared.enterCity = allTranslatedText["enterCity"] as? String ?? ""
                            SyncToastMessages.shared.enterAddressLine1 = allTranslatedText["enterAddressline1"] as? String ?? ""
                            SyncToastMessages.shared.addressType = allTranslatedText["addressType"] as? String ?? ""
                            SyncToastMessages.shared.addressLine1 = allTranslatedText["addressLine1"] as? String ?? ""
                            SyncToastMessages.shared.addressLine2 = allTranslatedText["addressLine2"] as? String ?? ""
                            SyncToastMessages.shared.phoneContacts = allTranslatedText["contactFromPhone"] as? String ?? ""
                            
                            
                            SyncToastMessages.shared.enterAddressType = allTranslatedText["enterAddressType"] as? String ?? ""
                            SyncToastMessages.shared.privacyChange = allTranslatedText["privacyChange"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToDisablePrivacyForContact = allTranslatedText["areUSureToDisablePrivacyForContact"] as? String ?? ""
                            SyncToastMessages.shared.areUSureToEnablePrivacyForContact = allTranslatedText["areUSureToEnablePrivacyForContact"] as? String ?? ""
                            SyncToastMessages.shared.showPrivacy = allTranslatedText["showPrivacy"] as? String ?? ""
                            SyncToastMessages.shared.keepPrivate = allTranslatedText["keepPrivate"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.home = allTranslatedText["home"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.work = allTranslatedText["work"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.addressonee = allTranslatedText["addressLine1"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.addressTwo = allTranslatedText["addressLine2"] as? String ?? ""
                            
                            ProfileToastMsgHeadingSubheadingLabels.shared.enternewpwd = allTranslatedText["enterNewPassword"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.enteraddresssone =  allTranslatedText["enterAddressline1"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.Entercity = allTranslatedText["enterCity"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.enterstate = allTranslatedText["enterState"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.entercountry = allTranslatedText["enterCountry"] as? String ?? ""
                            ProfileToastMsgHeadingSubheadingLabels.shared.enterpincode = allTranslatedText["enterPostcode"] as? String ?? ""
                        }
                    }
                } else {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Stability", action: "Get translated text", label: "Failed to get translation - Error", value: 0)
                }
                self.finish()
            } else {
                self.finish()
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Stability", action: "Get translated text", label: "Failed to get translation - Error", value: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

class DeleteUsersPerviousLocations: Procedure {
    
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    override init() {
        super.init()
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }
    
    override func execute() {
        
        let parameters =
            [
                "idtoken": self.idtoken
                
                ] as [String: Any]
        
        var awsURL = AppConfig.deleteUsersPreviousLocation
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpMethod = "PUT"
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                self.finish()
                
            } else {
                
                self.finish()
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Stability", action: "Delete user's previous locations", label: "Failed to delete user's pervious location - Error", value: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
