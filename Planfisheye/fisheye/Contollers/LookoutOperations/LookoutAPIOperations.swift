//
//  LookoutAPIOperations.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 17/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

// give user feedback about Lookout
class LookoutUserFeedback: Procedure {

      let appSharedPrefernce = AppSharedPreference()
      var idtoken = ""
      var feedbackObj = LookoutUserFeedbackObj()
      var statusCode: String = ""
      override init() {
            super.init()
            self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
      }

      override func execute() {

            let isWillingToContinueAfterTrial = self.feedbackObj.isWillingToContinueAfterTrial
            let amountWillingToPayForFisheye = self.feedbackObj.amountWillingToPayForFisheye
            let isHappyToMakeFutureCommitementForPremiumVersionOfFisheye = self.feedbackObj.isHappyToMakeFutureCommitementForPremiumVersionOfFisheye
            let premiumVersionWait = self.feedbackObj.premiumVersionWait
            let suggestionForFisheye = self.feedbackObj.suggestionForFisheye

            let parameters =
                  [
                        "action": "addLookoutFeedback",
                        "idtoken": self.idtoken,
                        "feedbackObj": [
                              "isWillingToContinueAfterTrial": isWillingToContinueAfterTrial,
                              "amountWillingToPayForFisheye": amountWillingToPayForFisheye,
                              "isHappyToMakeFutureCommitementForPremiumVersionOfFisheye": isHappyToMakeFutureCommitementForPremiumVersionOfFisheye,
                              "premiumVersionWait": premiumVersionWait,
                              "suggestionForFisheye": suggestionForFisheye
                        ]
                ] as [String: Any]

            let awsURL = AppConfig.giveLookoutUserFeedback
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"

            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                     DispatchQueue.global().async {
                                    if let data = json as? [String: Any] {
                                          self.statusCode = data["statusCode"] as? String ??  ""
                                        
                                          self.finish()
                                    }
                        }
                  } else {
                   
                        self.finish()
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }
}
