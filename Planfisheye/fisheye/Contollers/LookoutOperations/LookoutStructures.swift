//
//  LookoutStructures.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 17/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

struct LookoutUserFeedbackObj {
      init() {
      }
      var isWillingToContinueAfterTrial: String!
      var amountWillingToPayForFisheye: String!
      var isHappyToMakeFutureCommitementForPremiumVersionOfFisheye: String!
      var premiumVersionWait: String!
      var suggestionForFisheye: String!
}
