//
//  LookoutToastMessages.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 16/06/2018.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

class LookoutToastMessages {

      static let shared: LookoutToastMessages = LookoutToastMessages()

      // -------------------------------------------------------------    Toast Messages   --------------------------------------------------------------- //

      // Common Toast Messages //

      // to show syncing is in progress in the background.
      var failedToAddFeedback: String = "We are experiencing higher than expected traffic, and unable to add feedback. Please bear with us, and try again in a few minutes"
      var successInAddingFeedback: String = "Thank you. Your response has been submitted"
      var noInternet: String = "Please check your network connection and try again"
      var hi: String = "Hi"
      var registeredInterest: String = "Register Interest"
      var thankYouLbl: String = "Thank you, feedback received. We are working on Lookout release now."
      var feedbackQuestionOne: String = "Would you like to continue using Fisheye after the initial trial period?"
      var feedbackQuestionTwo: String = "What would you be willing to pay for Fisheye? not below: Since we do not sell data, we hope you will buy our products to help support this platform."
      var feedbackQuestionThree: String = "Are you happy to make any future committment for a premium version of Fisheye?"
      var feedbackQuestionFour: String = "How long are you willing to wait for a premium version of Fisheye?"
      var feedbackQuestionFive: String = "Is there any other ideas for apps you would like to suggest on Fisheye?"
      var thanksBeforeSubmitting: String = "Thanks! Your feedback help us build a better Fisheye community, closer to our user's requirements."
      var submit: String = "Submit"
      var yes: String  = "Yes"
      var no: String =  "No"
      var payForFEOptionOne: String = "£10 / £12/ £15 year"
      var payForFEOptionTwo: String = "£3 / £4/ £5 quarter"
      var threeMonths: String = "3 Months"
      var sixMonths: String = "6 Months"
}
