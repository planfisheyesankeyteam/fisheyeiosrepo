//
//  SyncNotificationCentre.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 11/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

// notification to load sync base view controller after skip button pressed fron sync introductory screen
let LOAD_SYNC_BASE_VIEW = Notification.Name("LoadSyncBaseVC")

// notification to refresh address list from addressAddEditPopUp
let REFRESH_ADDRESSES_NOTIFICATION = Notification.Name("RefreshAddreses")

// notification to refresh contact list after new contact is submitted
let REFRESH_CONTACTS_NOTIFICATION = Notification.Name("RefreshContacts")

// notification to show error toast msgs
let SHOW_TOAST_MESSAGE = Notification.Name("ShowErrorToastMessage")

// notification to show new page from sync module
let LOADE_NEW_PAGE = Notification.Name("LoadNewPage")
