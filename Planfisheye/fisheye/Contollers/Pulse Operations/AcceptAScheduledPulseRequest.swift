//
//  AcceptAScheduledPulseRequest.swift
//  fisheye
//
//  Created by Sankey Solutions on 30/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

class AcceptAScheduledPulseRequest: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var message = ""
    var statusCode: String = ""
    var isEncrypted: Bool = false
    var respondToPulseOf: String = ""
    var pulseToWhichUserResponding: String = ""
    var acceptingPulseStartDate: Double = 0.0
    var acceptingPulseEndDate: Double = 0.0
    var acceptingPulsePulseTime: Double = 0.0
    var acceptingPulseRepeatersArray = [String]()

 init(isEncrypted: Bool, message: String, respondToPulseOf: String, pulseToWhichUserResponding: String, acceptingPulseStartDate: Double, acceptingPulseEndDate: Double, acceptingPulsePulseTime: Double, acceptingPulseRepeatersArray: [String]) {
            super.init()
            self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.message = message
            self.isEncrypted = isEncrypted
            self.respondToPulseOf = respondToPulseOf
            self.pulseToWhichUserResponding = pulseToWhichUserResponding
            self.acceptingPulseStartDate = acceptingPulseStartDate
            self.acceptingPulseEndDate = acceptingPulseEndDate
            self.acceptingPulsePulseTime = acceptingPulsePulseTime
            self.acceptingPulseRepeatersArray = acceptingPulseRepeatersArray
        }

     override func execute() {

            let parameters =
                [
                    "action": "acceptpulseandsharelocatscheduledtime",
                    "idtoken": self.idtoken,
                    "pulseId": self.pulseToWhichUserResponding,
                    "respondToHisPulse": self.respondToPulseOf,
                    "pulse": [
                        "pulseStartDate": self.acceptingPulseStartDate,
                        "pulseEndDate": self.acceptingPulseEndDate,
                        "pulseTime": self.acceptingPulsePulseTime,
                        "pulseRepeatersForDays": self.acceptingPulseRepeatersArray,
                        "isEncrypted": self.isEncrypted,
                        "message": self.message
                    ]
                ] as [String: Any]

            let awsURL = AppConfig.acceptAPulseToShareLocationAtscheduledTimeAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.httpBody = "".data(using: String.Encoding.utf8)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.addValue(idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = "POST"
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                return
            }

            let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    if let resultObject = json as? [String: Any] {
                        self.statusCode = resultObject["statusCode"] as? String ?? ""
                    }
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Accept scheduled pulse", action: "Accept a scheduled pulse request", label: "Success while Accepting a scheudled pulse request with pulse id : "+self.pulseToWhichUserResponding, value: 0)
                    self.finish()
                } else {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Accept scheduled pulse", action: "Accept a scheduled pulse request", label: "Failure while Accepting a scheudled pulse request with pulse id : "+self.pulseToWhichUserResponding, value: 0)
                    self.finish()
                }
            })
        task.resume()
        session.finishTasksAndInvalidate()

    }
}
