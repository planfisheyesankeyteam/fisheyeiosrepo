//
//  ShareAndRequestOperations.swift
//  fisheye
//
//  Created by Sankey Solutions on 07/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

let LOAD_PULSE_BASE_VIEW = Notification.Name("LoadPulseBaseVC")

//// notification to refresh address list from addressAddEditPopUp
//let REFRESH_ADDRESSES_NOTIFICATION = Notification.Name("RefreshAddreses")

protocol DeletePulseProtocol {
    func toDeletePulse(toReload: Bool)
    func toDeclinePulse(toReload: Bool)
    func afetrAcceptingPulse(toReload: Bool)
    func toEnterCapsuleWhileAcceptingPulse(toReload: Bool)
    func toAlertToAddRequestInContactList(toReload: Bool)
    func unlockedCapsuleDisplay(toReload: Bool)
}

protocol InsertCapsuleProtocol {
    func acceptPulseAndShareCapsuleWithLocation(toReload: Bool)
    func toAlertToAddRequestInContactList(toReload: Bool)
    func dismissInsertCapsulePopUp(toReload: Bool)
    func dismisCapsulePopUpAndUpdateAccRec(toReload: Bool)

}

protocol PulseListingProtocol {
    func toDeletePulse(toReload: Bool)
    func toEnterCapsuleWhileAcceptingPulse(toReload: Bool)
    func afetrAcceptingPulse(toReload: Bool)
    func toDeclinePulse(toReload: Bool)
    func unlockedCapsuleDisplay(toReload: Bool)
    func toAlertToAddRequestInContactList(toReload: Bool)
}

protocol PulseInSyncProtocol {
    func toEnterCapsuleWhileSendingPulseRequestFromSync(toReload: Bool)
    func toEnterCapsuleWhileSendingPulseShareFromSync(toReload: Bool)
}

// contact Structure
struct PulseObject {
    init() {

    }
    var toObjArray: String!
    var isEncrypted: Bool!
    var toObjectArray: [RecipientObj] = [RecipientObj]()
    var message: String!
    var companyEmail: String!
    var title: String!
    var company: String!
    var website: String!
    var notes: String!
    var isBackendSync: Bool!
    var source: String!
    var contactFEId: String!
    var isdeleted: Bool!
    var ismerged: Bool!
    var picture: String!
    var mergeParentId: String!
    var stringSecondaryPhones: String!
    var stringSecondaryEmails: String!
    var addressString: String!
}

// contact Structure
struct RecipientObj {
    init() {

    }
    var fisheyeId: String!
    var name: String!
    var phoneNumber: String!
    var email: String!
    var isAccepted: Bool!
    var isRead: Bool!
    var isDeleted: Bool!
    var imageUrl: String!
}

// get logged in user contacts from backend
class PulseNowRequest: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var contactId = ""
    var contactObjs: [ContactObj] = []
    var singleContactObj = ContactObj()
    var addressList = [AddressType]()

    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {
    }

}
