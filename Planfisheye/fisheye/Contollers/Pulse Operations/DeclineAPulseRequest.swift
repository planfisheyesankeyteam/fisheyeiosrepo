//
//  DeclineAPulseRequest.swift
//  fisheye
//
//  Created by Sankey Solutions on 30/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

class DeclineAPulseRequest: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var pulseToBeDeclined: String = ""
    var statusCode: String = ""
    var isAccepted: Bool = false

    init(isAccepted: Bool, pulseToBeDeclined: String) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.pulseToBeDeclined = pulseToBeDeclined
        self.isAccepted = isAccepted
    }

    override func execute() {

        let parameters =
            [
                "action": "updateAcceptRejectStatus",
                "idtoken": self.idtoken,
                "pulseId": self.pulseToBeDeclined,
                "isAccepted": self.isAccepted
            ] as [String: Any]

        let awsURL = AppConfig.declineAPulseAPI

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                
                if let resultObject = json as? [String: Any] {
                    self.statusCode = resultObject["statusCode"] as? String ?? ""
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Decline Pulse", action: "Decline a pulse request", label: "Success while declining a pulse request of id : "+self.pulseToBeDeclined, value: 0)
                   self.finish()
                } else {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Decline Pulse", action: "Decline a pulse request", label: "Failure while declining a pulse request of id : "+self.pulseToBeDeclined, value: 0)
                    self.finish()
                }
            } else {
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Decline Pulse", action: "Decline a pulse request", label: "Failure while declining a pulse request of id : "+self.pulseToBeDeclined, value: 0)
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
