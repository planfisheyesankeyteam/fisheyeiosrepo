//
//  updatePulse.swift
//  fisheye
//
//  Created by Sankey Solutions on 04/04/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

class UpdatePulseData: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var message = ""
    var statusCode: String = ""
    var isEncrypted: Bool = false
    var recipientsIdArray = [String]()
    var pulseTime: Double = 0.0
    var pulseStartDate: Double = 0.0
    var pulseEndDate: Double = 0.0
    var toUpdatePulseId: String = ""
    var pulseType: String = ""
    var pulseDayRepeaters = [String]()

    init(isEncrypted: Bool, message: String, recipientsIdArray: [String], toUpdatePulseId: String, pulseTime: Double, pulseStartDate: Double, pulseEndDate: Double, pulseDayRepeaters: [String], pulseType: String) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.message = message
        self.isEncrypted = isEncrypted
        self.recipientsIdArray = recipientsIdArray
        self.pulseTime = pulseTime
        self.toUpdatePulseId = toUpdatePulseId
        self.pulseStartDate = pulseStartDate
        self.pulseEndDate = pulseEndDate
        self.pulseDayRepeaters = pulseDayRepeaters
        self.pulseType = pulseType
    }

    override func execute() {

        let parameters =
            [
                "idtoken": self.idtoken,
                "action": "pulsescheduledUpdate",
                "scheduledPulseData": [
                    "scheduledPulseId": self.toUpdatePulseId,
                    "toIdArray": self.recipientsIdArray,
                    "scheduledAtTime": self.pulseTime,
                    "scheduleFromDate": self.pulseStartDate,
                    "scheduleTillDate": self.pulseEndDate,
                    "repeatersForDays": self.pulseDayRepeaters,
                    "message": self.message,
                    "isEncrypted": self.isEncrypted,
                    "pulseType": self.pulseType
                ]
            ] as [String: Any]

        let awsURL = AppConfig.updatePulseAPI

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                if let resultObject = json as? [String: Any] {
                    self.statusCode = resultObject["statusCode"] as? String ?? ""
                }
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Edit", action: "Edit pulse data", label: "Pulse data is updated successfully", value: 0)
                self.finish()
            } else {
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Edit", action: "Edit pulse data", label: "Failure while updating pulse data", value: 0)
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }

}
