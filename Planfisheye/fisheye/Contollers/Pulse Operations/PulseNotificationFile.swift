//
//  PulseNotificationFile.swift
//  fisheye
//
//  Created by Sankey Solutions on 15/06/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

let DISMISS_LOADER_WHEN_LOCATION_SETTING_OPENED = Notification.Name("DismissLoaderWhenLocationSettingOpened")
let TOAST_WHEN_UNABLE_TOGET_LOCATION = Notification.Name("NotifyWhenLocationIsNotGot")
let ALERT_NETWORK_DETECTION = Notification.Name("NotifyWhenLocationIsNotGot")
let UPDATE_LOCATION = Notification.Name("UpdateLocation")

protocol DismissCapsulePopUpToShowSync {
    func dismissCapsule()
}
