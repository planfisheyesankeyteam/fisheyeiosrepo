////
////  File.swift
////  fisheye
////
////  Created by Sankey Solutions on 07/02/18.
////  Copyright © 2018 Keerthi. All rights reserved.
////
//
//import Foundation
//import ProcedureKit
//import Alamofire
//import SwiftyJSON
//
//// contact Structure
//struct PulseObj {
//    init(){
//
//    }
//    var pulseId: String!
//    var toIdArray: String!
//    var isEncrypted: String!
//    var secondaryPhoneNumbers:[String] = []
//    var secondaryEmails:[String] = []
//    var contactId: Int!
//    var addressList:[AddressType] = [AddressType]()
//    var email: String!
//    var privacyEnabled: Bool!
//    var companyEmail: String!
//    var title: String!
//    var company: String!
//    var website: String!
//    var notes: String!
//    var isBackendSync: Bool!
//    var source: String!
//    var contactFEId: String!
//    var isdeleted: Bool!
//    var ismerged: Bool!
//    var picture: String!
//    var mergeParentId: String!
//    var stringSecondaryPhones: String!
//    var stringSecondaryEmails: String!
//    var addressString: String!
//}
//
//// get logged in user contacts from backend
//class getLoggedInUserContactsOperation: Procedure {
//    let appSharedPrefernce = AppSharedPreference()
//    var idtoken = ""
//    var contactId = ""
//    var contactObjs: [ContactObj] = []
//    var singleContactObj = ContactObj()
//    var addressList = [AddressType]()
//
//    override init() {
//        super.init()
//        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
//    }
//    override func execute() {
//
//        let headers: HTTPHeaders =
//            [
//
//                "Content-Type":"application/json",
//                "action": "getcontacts",
//                "idtoken": idtoken,
//                "searchKey": ""
//
//        ]
//
//        let awsURL = AppConfig.getAllOwnerContacts
//        Alamofire.request(awsURL, method: .get, encoding: JSONEncoding.default, headers: headers)
//            .responseJSON
//            {
//                response in
//                switch response.result
//                {
//                case .success:
//                    let data = JSON(response.result.value!)
//                    if(data["statusCode"] == "200")
//                    {
//
//                        let countArray = Int((data["contacts"].count))
//
//                        if(countArray != 0) {
//                            DispatchQueue.global().async
//                                {
//                                    for i in 0..<Int((data["contacts"].count)){
//
//                                        self.singleContactObj.name = data["contacts"][i]["name"].stringValue
//                                        let getAddressList = data["contacts"][i]["addressList"]
//                                        for j in 0..<Int((getAddressList.count)){
//                                            var contactAddress =  AddressType()
//                                            contactAddress.addressId = getAddressList[j]["addressId"].stringValue
//                                            contactAddress.country = getAddressList[j]["country"].stringValue
//                                            contactAddress.locality = getAddressList[j]["locality"].stringValue
//                                            contactAddress.street_address = getAddressList[j]["street_address"].stringValue
//                                            contactAddress.postal_code = getAddressList[j]["postal_code"].stringValue
//                                            contactAddress.region = getAddressList[j]["region"].stringValue
//                                            contactAddress.type = getAddressList[j]["type"].stringValue
//                                            contactAddress.formatted = getAddressList[j]["formatted"].stringValue
//                                            self.addressList.append(contactAddress)
//                                        }
//                                        self.singleContactObj.addressList = self.addressList
//                                        let secondaryPhoneNumbers = data["contacts"][i]["secondaryPhoneNumbers"]
//                                        var phoneNumbers: [String] = []
//                                        for k  in 0..<Int((secondaryPhoneNumbers.count))
//                                        {
//                                            phoneNumbers.append(secondaryPhoneNumbers[k].stringValue)
//                                        }
//                                        let secondaryEmails = data["contacts"][i]["secondaryEmails"]
//                                        var emails: [String] = []
//                                        for k  in 0..<Int((secondaryEmails.count))
//                                        {
//                                            emails.append(secondaryEmails[k].stringValue)
//                                        }
//                                        self.singleContactObj.secondaryEmails = emails
//                                        self.singleContactObj.name = data["contacts"][i]["name"].stringValue
//                                        self.singleContactObj.phoneNumber = data["contacts"][i]["phoneNumber"].stringValue
//                                        self.singleContactObj.contactId = Int(data["contacts"][i]["contactId"].stringValue)
//                                        self.singleContactObj.email = data["contacts"][i]["email"].stringValue
//                                        self.singleContactObj.company = data["contacts"][i]["company"].stringValue
//                                        self.singleContactObj.companyEmail = data["contacts"][i]["companyEmail"].stringValue
//                                        self.singleContactObj.title = data["contacts"][i]["title"].stringValue
//                                        self.singleContactObj.website = data["contacts"][i]["website"].stringValue
//                                        self.singleContactObj.notes = data["contacts"][i]["notes"].stringValue
//                                        self.singleContactObj.isBackendSync = true
//                                        self.singleContactObj.notes = data["contacts"][i]["notes"].stringValue
//                                        self.singleContactObj.source = data["contacts"][i]["source"].stringValue
//                                        self.singleContactObj.contactFEId = data["contactFEId"][i]["contactFEId"].stringValue
//                                        self.singleContactObj.isdeleted = data["contacts"][i]["isdeleted"].boolValue
//                                        self.singleContactObj.ismerged = data["contacts"][i]["ismerged]"].boolValue
//                                        self.singleContactObj.privacyEnabled = data["contacts"]["privacyEnabled]"] .boolValue
//                                        self.singleContactObj.picture = data["contacts"]["picture"] .stringValue
//                                        self.singleContactObj.mergeParentId = data["contacts"]["mergeParentId"] .stringValue
//                                        self.singleContactObj.isBackendSync = true
//
//                                        self.contactObjs.append(self.singleContactObj)
//                                        DatabaseManagement.shared.insertContactsInSQLiteStorage(contactObj: self.singleContactObj)
//                                        if((i % 10) == 0){
//                                            NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
//                                        }
//                                    }
//                                    self.finish()
//                                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
//                            }
//
//                        }else{
//                            self.finish()
//                        }
//                    }else{
//                        self.finish()
//                    }
//                case .failure(let error):
//                    self.finish()
//                }
//        }
//    }
//
//}
