//
//  RequestOrSchedulePulseAtScheduledTime.swift
//  fisheye
//
//  Created by Sankey Solutions on 31/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

class RequestOrSharePulseAtScheduledTime: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var message = ""
    var statusCode: String = ""
    var isEncrypted: Bool = false
    var recipientsIdArray = [String]()
    var startDateTimeStamp: Double = 0.0
    var endDateTimeStamp: Double = 0.0
    var pulseTimeTimeStamp: Double = 0.0
    var repeatersArray = [String]()
    var pulseType: String = ""

    init(isEncrypted: Bool, message: String, recipientsIdArray: [String], startDateTimeStamp: Double, endDateTimeStamp: Double, pulseTimeTimeStamp: Double, repeatersArray: [String], pulseType: String) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.message = message
        self.isEncrypted = isEncrypted
        self.recipientsIdArray = recipientsIdArray
        self.startDateTimeStamp = startDateTimeStamp
        self.endDateTimeStamp = endDateTimeStamp
        self.pulseTimeTimeStamp = pulseTimeTimeStamp
        self.repeatersArray = repeatersArray
        self.pulseType = pulseType
    }

    override func execute() {

        let parameters =
            [
                "idtoken": self.idtoken,
                "scheduledPulseData": [
                    "toIdArray": self.recipientsIdArray,
                    "isEncrypted": self.isEncrypted,
                    "message": self.message,
                    "scheduledAtTime": self.pulseTimeTimeStamp,
                    "scheduleFromDate": self.startDateTimeStamp,
                    "scheduleTillDate": self.endDateTimeStamp,
                    "repeatersForDays": self.repeatersArray,
                    "isActive": 1,
                    "pulseType": self.pulseType
                ]
            ] as [String: Any]
        
       

        var awsURL = ""
        if(self.pulseType=="0") {
            awsURL = AppConfig.requestAPulseAtScheduledTimeAPI
            
        } else if(self.pulseType=="1") {
            awsURL = AppConfig.shareAPulseAtScheduledTimeAPI
            
        } else {

        }

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"

        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
               
                if let resultObject = json as? [String: Any] {
                    self.statusCode = resultObject["statusCode"] as? String ?? ""
                }

                if(self.pulseType=="0") {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse request at scheduled time", action: "Create a pulse request at a scheduled time", label: "Pulse request at scheduled time is created successfully", value: 0)
                } else if(self.pulseType=="1") {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse share at scheduled time", action: "Create a pulse share at a scheduled time", label: "Pulse share at scheduled time is created successfully", value: 0)
                }
                self.finish()
            } else {
                if(self.pulseType=="0") {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse request at scheduled time", action: "Create a pulse request at a scheduled time", label: "Failure while creating Pulse request at scheduled time", value: 0)
                } else if(self.pulseType=="1") {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse share at scheduled time", action: "Create a pulse share at a scheduled time", label: "Failure while creating a Pulse share at scheduled time", value: 0)
                }
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
