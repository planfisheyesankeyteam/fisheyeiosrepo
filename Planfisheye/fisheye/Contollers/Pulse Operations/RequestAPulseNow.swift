//
//  RequestAPulseNow.swift
//  fisheye
//
//  Created by Sankey Solutions on 30/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

class RequestAPulseNow: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var message = ""
    var statusCode: String = ""
    var isEncrypted: Bool = false
    var recipientsIdArray = [String]()

    init(isEncrypted: Bool, message: String, recipientsIdArray: [String]) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.message = message
        self.isEncrypted = isEncrypted
        self.recipientsIdArray = recipientsIdArray
    }

    override func execute() {

        let parameters =
            [
                "idtoken": self.idtoken,
                "action": "pulsenowrequest",
                "pulse": [
                    "responderIdArray": self.recipientsIdArray,
                    "isEncrypted": self.isEncrypted,
                    "message": self.message
                ]
            ] as [String: Any]
       
        let awsURL = AppConfig.requestAPulseNowAPI

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {

                if let resultObject = json as? [String: Any] {

                    self.statusCode = resultObject["statusCode"] as? String ?? ""

                }
                self.finish()
            } else {
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
