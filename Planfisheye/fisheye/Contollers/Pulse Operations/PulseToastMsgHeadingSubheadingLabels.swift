//
//  PulseToastMsgHeadingSubheadingLabels.swift
//  fisheye
//
//  Created by Sankey Solutions on 28/05/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

class PulseToastMsgHeadingSubheadingLabels {

    static let shared: PulseToastMsgHeadingSubheadingLabels = PulseToastMsgHeadingSubheadingLabels()
    
        //Toast delay time
        var toastDelayTimeForPulse: Double = 3.0

/************************************ Toast Massages *****************************************/

    //------------------------------ Common Toast massages--------------------------------//

        //Now pulse request send success
            var nowPulseRequest: String = "Your location request has been sent"

        //Now Pulse share send success
            var nowPulseShare: String = "You have requested a location share"

        //Scheduled Pulse request send success
            var scheduledPulseRequest: String = "A scheduled request has been sent"

        //Scheduled pulse share send success
            var scheduledPulseShare: String = "You have requested a scheduled location share"

        //Now Pulse request accepted success
            var nowPulseReqAccepted: String = "Your location request has been accepted"

        //Scheduled pulse request accept success
            var schedPulseReqAccepted: String = "Your scheduled location request has been accepted"

        //pulse Request is declined
            var pulseReqDeclined: String = "You have declined this request"

        //Scheduled pulse updated success
            var pulseUpdated: String = "Scheduled location request/share updated successfully"

        //Network failure
            var networkFailureMsg: String = "Please check your network connection and try again"

        //unable to fetch last locatin from backend.
            var unableToFetchLastLocation: String = "Unable to share your last location."

        //Server error message while getting pulse details
        var serverErrorOfPulse: String = "There seems to be a network error, please contact us if this persists"
    
        var addMasterKeyInProfile: String = "Please add master key from profile-security"

    //---------------------------- Master key screen specific taost Msg---------------------//

        //When backend masterkey doesnot match with entered master key
            var wrongMasterKey: String = "Incorrect Key"

        //when master key is not entered and clicked on go
            var enterMasterKey: String = "Use your key"
    
        //when contact dnt have phone number as well as email id
            var noPhoneNoEmail: String = "Don't have phone number and email id"

/************************************ Headings and Subheadings *******************************/

        //tell what accepting a pulse means
        var tellAcceptMeansHeading: String = "Accept Location Request"
        var tellAcceptMeansSubHeading: String = "Your are sharing your location with the requestor, do you wish to continue?"

        //tell what accepting a pulse means
        var tellDeclineMeansHeading: String = "Decline Location Request"
        var tellDeclineMeansSubHeading: String = "You wish to decline this Location request?"

        //add pulse requester Contact To Your Contact List To Respond to his pulse
        var addContactHeading: String = "Add Contact"
        var addContactSubHeading: String = "This requester does not seem to be a known. Intact. Please add the requester’s details to your Sync contacts. Once the sync is complete, you will be able to accept this pulse."

        //pulse deletion confirmation
        var pulseDeletionHeading: String = "Delete Location request/share"
        var pulseDeletionSubHeading: String = "Do you wish to cancel this location request/share?"

/******************************** Labels/Place holders/Validations ********************************/

    //----------------------------- Pulse Listing Screen ---------------------------//
        // Fisheye label at bottom
        var fisheyeLabel: String = "Fisheye Hub"

        //Lets work together sologan
        var letsWorkTogetherLabel: String = "Let's work together"

        //Pulse logbook screen title
        var pulseLogbookTitle: String = "Logbook"

        //My requests and requests to me tab on pulse
        var myReuestsTapTitle: String = "My requests"

        //shares tab on pulse logbook
        var sharesTapTitle: String = "Shares"

        //past button title
        var pastButtonTitle: String = "PAST"

        //label between SD and ED
        var labelBetweenSDandED: String = "to"

        //label between ED and PT
        var labelBetweenEDandPT: String = "at"

        //label used to indicate user has accepted the pulse
        var pulseAccepted: String = "Accepted"

        //label used to indicate user has declined the pulse
        var pulseDeclined: String = "Declined"

        //label to indicated pilse request is expired
        var pulseExpired: String = "Expired"

        var capLabel: String = "Capsule :"

        var capsule: String = "Capsule"

        var createPulse: String = "Create Location request/share"

        //label ti indicate capsule is locked,and user will need to enter his master key to unlock the capsule
        var capLockNeedMasterKey: String = "This is a Locked Capsule, please use your Master key"

        //to indicate pulse request creator has not entered message while creating
        var emptyCapWhileRequestingPulse: String = "You have no messaging capsule"

        //to indicate pulse share creator has not entered message while creating
        var emptyCapWhileSharingPulse: String = "You have no messaging capsule"

        //user has his pulses in pulse table but unable to that data from backend due to network connection
        //then instead og showing empty logbook view show,message on pulse logbook
        var checkInternetConnection: String = "No network connection detected"

        //no pulse found corresponding to search key indication
        var noPulseCorrSearchKey: String = "No records found for your search request i.e "

        //annotation title for shared location map
        var annotationTitle: String = "Location"

        var creatPulsetext: String = "You can start sharing your location or start requesting for location with people."
    
        var needToUnblockContToSendPulseReq = "You need to unblock this contact to request location."
    
        var needToUnblockContToSendPulseShare = "You need to unblock this contact to share location."

    //-------------------------------- Pulse Details ---------------------------------//

        // section title to indicate which recipient accepted the pulse
        var acceptedPulseRecipients: String = "Accepted"

        //section title to indicate which recipient declined the pulse
        var declinedPulseRecipients: String = "Declined"

        //section title to indicate which recipient not responded to the pulse
        var notRespondedRecipients: String = "Awaiting response"

        var sundayOneLetter: String = "Sun"
        var mondayOneLetter: String = "Mon"
        var tuesdayOneLetter: String = "Tue"
        var wedOneLetter: String = "Wed"
        var thruOneLetter: String = "Thu"
        var fridayOneLetter: String = "Fri"
        var saturedayOneLetter: String = "Sat"

        var recipient: String = "Recipient"

    //------------------------------- Share and request screen ------------------------//

        //share and request pulse screen title
        var shareAndRequestPageTitle = "Location"

        //request tab of pulse share screen
        var requestTabOfShareAndRequestScrren = "Request"

        //share tab of pulse share and request screen
        var shareTabOfShareAndRequestScrren = "Share"

        //label to show no contact is choosen
        var noContactChoosenLabel = "You have not choosen any contact to share or request a location."

        //Now to creat now type pulse
        var nowPulse: String = "Now"

        //schedule a pulse label button
        var schedulePulse: String = "Scheduled"

        //pulse time label
        var pulsetime: String = "hh:mm"

        //start date label
        var startDate: String = "Start Date"

        //end date label
        var endDate: String = "End Date"

        var pulseRepeaterLabel = "Repeat"

        //to list fisheye contact under fish-eye contact section
        var fishEyeContactLabel: String = "Contacts on Fisheye"

        //to list phone contact under phone contact section
        var phoneContactLabel: String = "Contacts from Phone"

        //When no user contacts found
        var noContactFound: String = "No contact found."

        //edit capsule while editing pulse
        var editCapsuleLabel: String = "Edit Message"

        //Insert capsule while creating pulse
        var insertCapsuleLabel: String = "Add Message"

        //add contact label
        var addContactLabel: String = "Add contact"

        //edit capsule
        var editMessage: String = "Edit Message"

    //---------------------------- Time and date selection selection ------------------------//

        //title of time picker
        var timePickerTitle: String = "Pulse Scheduler"

        //title of start date picker
        var startDatePickerTitle: String = "Select Start Date"

        //title of End date picker
        var endDatePickerTitle: String = "Select End Date"

        // date/time picker done button
        var pickerDone: String = "Done"

        // date/time picker done button
        var pickerCancel: String = "Cancel"

    //--------------------------------- capsule pop-up -----------------------------//

        //optional capsule message indication
        var optionalCapsuleMsg: String = "Optional : "

        //capsule message placeholder
        var capsuleMsgPlaceHolder: String = "Enter your message here. Space limited to 100 Characters only"

        //capsule is locked message
        var capsuleLockedUnlockedLabel: String = "Capsule is open"

        var enterYourCapsuleLabel: String = "Enter your message"

        var addCapsuleLabel: String = "Add Message"

    //-------------------------------- Master key pop-up ---------------------------//

        //Master key Pop-up title
        var enterMasterKeyTitle: String = "Enter your Master key"

        //to Show in master key text box when master key is not entered
        var configOnErrorEnterMasterKey: String = "Use your master key"

        //when error cleared to show valid master key
        var configOnErrorClearedMasterKey: String = "Master key"

        //when invalid master key is entered then to indicate user
        var configOnErrorInvalidMasterKey: String = "Incorrect master key"

        //warning to enter master
        var warningToEnterMasterKey: String = "You may not proceed without entering your master key"

}
