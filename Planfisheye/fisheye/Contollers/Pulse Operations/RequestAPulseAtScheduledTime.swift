//
//  RequestAPulseAtScheduledTime.swift
//  fisheye
//
//  Created by Sankey Solutions on 30/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit
import Alamofire
import SwiftyJSON

class RequestOrSharePulseAtScheduledTime: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var message = ""
    var statusCode: String = ""
    var isEncrypted: Bool = false
    var recipientsIdArray = [String]()
    var startDateTimeStamp: Double = 0.0
    var endDateTimeStamp: Double = 0.0
    var pulseTimeTimeStamp: Double = 0.0
    var repeatersArray = [String]()
    var pulseType: String = ""

    init(isEncrypted: Bool, message: String, recipientsIdArray: [String], startDateTimeStamp: Double, endDateTimeStamp: Double, pulseTimeTimeStamp: Double, repeatersArray: [String], pulseType: String) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.message = message
        self.isEncrypted = isEncrypted
        self.recipientsIdArray = recipientsIdArray
        self.startDateTimeStamp = startDateTimeStamp
        self.startDateTimeStamp = endDateTimeStamp
        self.pulseTimeTimeStamp = pulseTimeTimeStamp
        self.repeatersArray = repeatersArray
        self.pulseType = pulseType
    }

    override func execute() {
        let headers: HTTPHeaders =
            [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
        let parameters: Parameters =
            [
                "idtoken": self.idtoken,
                "scheduledPulseData": [
                    "toIdArray": self.recipientsIdArray,
                    "isEncrypted": self.isEncrypted,
                    "message": self.message,
                    "scheduledAtTime": self.pulseTimeTimeStamp,
                    "scheduleFromDate": self.startDateTimeStamp,
                    "scheduleTillDate": self.endDateTimeStamp,
                    "repeatersForDays": self.repeatersArray,
                    "isActive": 1,
                    "pulseType": self.pulseType
                ]
            ]

        var awsURL = ""
        if(self.pulseType=="0") {
             awsURL = AppConfig.requestAPulseAtScheduledTimeAPI
             print("scheduled pulse request URL", awsURL)
        } else if(self.pulseType=="1") {
            awsURL = AppConfig.shareAPulseAtScheduledTimeAPI
            print("scheduled pulse share URL", awsURL)
        } else {

        }

        Alamofire.request(awsURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in
                switch response.result {
                case .success:
                    let data = JSON(response.result.value!)
                    self.statusCode = data["statusCode"].stringValue
                    self.finish()

                case .failure(let error):
                    self.finish(withErrors: [error])
                }
        }
    }
}
