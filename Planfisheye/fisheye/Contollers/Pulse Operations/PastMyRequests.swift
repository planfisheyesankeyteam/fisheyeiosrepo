//
//  PastMyRequests.swift
//  fisheye
//
//  Created by Sankey Solutions on 27/07/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation
import ProcedureKit
import SwiftyJSON
import Alamofire

class PastMyRequests: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var statusCode: String = ""
    var searchKeyVal = ""
   // var logs = [String: Any] = []
    var logs = [PulseDetails]()
    //var logsArray =

    init(searchKey: String) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.searchKeyVal=searchKey
    }

    override func execute() {

         var headers: HTTPHeaders=[:]

        let awsURL = AppConfig.getPastRequestedPulseAPI

        print("the API for getPast my requests is ", awsURL)

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.addValue(self.searchKeyVal, forHTTPHeaderField: "searchKey")
        request.httpBody = "".data(using: String.Encoding.utf8)
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                print("my search key is ", self.searchKeyVal)
                print("result object from backend", json)
                if let resultObject = json as? [String: Any] {
                    print("Object form backend", json)
                    self.statusCode = resultObject["statusCode"] as? String ?? ""
                    print("status code : ", self.statusCode)
//                    self.logsArray = (resultObject["logs"] as? [PulseDetails])!
                    if let logsArray = resultObject["logs"] as? [[String: Any]] {
                       // print("here is past my request response object ",self.logsArray)
//                        self.logs = logsArray
                        for pulseDataObj in logsArray {
                        //    self.logs.append(pulseDataObj)
                        }//                        }print("assigned to other var from response object ",logsArray)
                    }
                }
                self.finish()
            } else {
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
