//
//  AcceptAPulseWithLocation.swift
//  fisheye
//
//  Created by Sankey Solutions on 30/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit
import CoreLocation
import MapKit

class AcceptNowPulseRequest: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var message = ""
    var statusCode: String = ""
    var isEncrypted: Bool = false
    var respondToPulseOf: String = ""
    var pulseToWhichUserResponding: String = ""
    var locationName: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var locManager = CLLocationManager()
    let geocoder = CLGeocoder()

    init(isEncrypted: Bool, message: String, respondToPulseOf: String, pulseToWhichUserResponding: String) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.message = message
        self.isEncrypted = isEncrypted
        self.respondToPulseOf = respondToPulseOf
        self.pulseToWhichUserResponding=pulseToWhichUserResponding
       
    }

    override func execute() {
        self.locManager.requestAlwaysAuthorization()
        self.locManager.requestWhenInUseAuthorization()
        self.locManager.startUpdatingLocation()
        self.latitude = (self.locManager.location?.coordinate.latitude)!
        self.longitude = (self.locManager.location?.coordinate.longitude)!

        let lction = CLLocation(latitude: self.latitude, longitude: self.longitude)
        var geocoder = CLGeocoder()
        // Geocode Location
      
        geocoder.reverseGeocodeLocation(lction) { (placemarks, error) in
            // Process Response
            self.processResponse(withPlacemarks: placemarks, error: error)
           
        }
    }

    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        if let error = error {
           
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                self.locationName = placemark.compactAddress!
                
            } else {
                var locationLabel = "No Matching Addresses Found"
            }
        }
        self.acceptANowPulseAfterGettingAtPara()
    }

    func acceptANowPulseAfterGettingAtPara() {

        let parameters =
            [
                "action": "acceptpulseandsharelocationwithcapsule",
                "idtoken": self.idtoken,
                "pulseId": self.pulseToWhichUserResponding,
                "respondToHisPulse": self.respondToPulseOf,
                "pulse": [
                    "latitude": self.latitude,
                    "longitude": self.longitude,
                    "locationName": self.locationName,
                    "isEncrypted": self.isEncrypted,
                    "message": self.message
                ]
            ] as [String: Any]

        let awsURL = AppConfig.acceptAPulseWithLocationAPI

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                if let resultObject = json as? [String: Any] {
                    self.statusCode = resultObject["statusCode"] as? String ?? ""
                }
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Accept Pulse", action: "Accept a pulse request which is for now", label: "Success while accepting a pulse now with pulse id : "+self.pulseToWhichUserResponding, value: 0)
                self.finish()
            } else {
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Accept Pulse", action: "Accept a pulse request which is for now", label: "Failure while accepting a pulse now with pulse id : "+self.pulseToWhichUserResponding, value: 0)
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
