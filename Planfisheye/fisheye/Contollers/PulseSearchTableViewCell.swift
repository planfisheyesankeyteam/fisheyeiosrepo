//
//  PulseSearchTableViewCell.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 04/09/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class PulseSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var fisheeyelogoimg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var nameLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
