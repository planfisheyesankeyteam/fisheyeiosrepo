//
//  SyncOperation.swift
//  fisheye
//
//  Created by sankeyMacOs vaishali on 22/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

var fetchingContactsFromBackendGlobalOperation = DispatchWorkItem {}

// contact Structure
struct ContactObj {
    init() {
        
    }
    var userId: String!
    var name: String!
    var phoneNumber: String!
    var secondaryPhoneNumbers: [String] = []
    var secondaryEmails: [String] = []
    var contactId: Int!
    var addressStringArray1: [String] = []
    var addressStringArray2: [String] = []
    var addressList: [AddressType] = [AddressType]()
    var email: String!
    var privacyEnabled: Bool!
    var privacySetForContactUser: Bool!
    var companyEmail: String!
    var title: String!
    var company: String!
    var website: String!
    var notes: String!
    var isBackendSync: Bool!
    var source: String!
    var contactFEId: String!
    var isdeleted: Bool!
    var ismerged: Bool!
    var picture: String!
    var mergeParentId: String!
    var stringSecondaryPhones: String!
    var stringSecondaryEmails: String!
    var addressString1: String!
    var addressString2: String!
    var isBlocked: Bool!
    var isBlockedByContact: Bool!
    var sharedData: Int!
    var createdAt: String!
    var updatedAt: String!
    
}

// get logged in user contacts from backend
class getLoggedInUserContactsOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var contactId = ""
    var contactsCount: Int = 0
    let dateFormatter = DateFormatter()
    var refreshIndex: Int = 1
    var syncText = SyncToastMessages()
    
    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        ContactsSync.shared.isSyncInProgress = true
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = NSTimeZone.local
    }
    
    override func execute() {
        self.appSharedPrefernce.setAppSharedPreferences(key: "isFetchFromBackendCompleted", value: "no")
        
        let awsURL = AppConfig.getAllOwnerDecryptContacts
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        
        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                if let contactsResponseObj = json as? [String: Any] {
                    if let contactList = contactsResponseObj["contacts"] as? [[String: Any]] {
                        self.contactsCount = contactList.count
                        DispatchQueue.global().async {
                            for i in 0..<self.contactsCount {
                                var singleContactObj = ContactObj()
                                let singleContact = contactList[i]
                                self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                                guard self.idtoken != "" else {
                                    return
                                }
                                
                                singleContactObj.name = singleContact["name"] as? String ??  ""
                                singleContactObj.contactId = Int(singleContact["contactId"] as? String ?? "")
                                let getAddressList = singleContact["addressList"] as? [[String: Any]] ?? []
                                if(getAddressList.count > 0) {
                                    let country0 = getAddressList[0]["country"] as? String ??  ""
                                    let locality0 = getAddressList[0]["locality"] as? String ??  ""
                                    let street_address0 = getAddressList[0]["street_address"] as? String ??  ""
                                    let postal_code0 = getAddressList[0]["postal_code"] as? String ??  ""
                                    let region0 = getAddressList[0]["region"] as? String ??  ""
                                    let type0 = getAddressList[0]["type"] as? String ??  ""
                                    let formatted0 = getAddressList[0]["formatted"] as? String ??  ""
                                    
                                    let addressString0 = type0  + "," + formatted0 + "," + street_address0 + ","
                                    singleContactObj.addressString1 = addressString0 + locality0 + "," + region0 + "," + country0 + "," + postal_code0
                                }
                                
                                if(getAddressList.count > 1) {
                                    let country1 = getAddressList[1]["country"] as? String ??  ""
                                    let locality1 = getAddressList[1]["locality"] as? String ??  ""
                                    let street_address1 = getAddressList[1]["street_address"] as? String ??  ""
                                    let postal_code1 = getAddressList[1]["postal_code"] as? String ??  ""
                                    let region1 = getAddressList[1]["region"] as? String ??  ""
                                    let type1 = getAddressList[1]["type"] as? String ??  ""
                                    let formatted1 = getAddressList[1]["formatted"] as? String ??  ""
                                    
                                    let addressString1 = type1  + "," + formatted1 + "," + street_address1 + ","
                                    singleContactObj.addressString2 = addressString1 + locality1 + "," + region1 + "," + country1 + "," + postal_code1
                                }
                                
                                var phoneNumbers: String = ""
                                if let secondaryPhoneNumbers = singleContact["secondaryPhoneNumbers"] as? [String] {
                                    for k in 0..<secondaryPhoneNumbers.count {
                                        let tempPhone = secondaryPhoneNumbers[k]
                                        if(k == 0) {
                                            phoneNumbers = tempPhone
                                        } else {
                                            phoneNumbers = phoneNumbers + "," + tempPhone
                                        }
                                    }
                                }
                                var emails: String = ""
                                if let secondaryEmails = singleContact["secondaryEmails"] as? [String] {
                                    for k in 0..<secondaryEmails.count {
                                        let tempEmail = secondaryEmails[k]
                                        if(k == 0) {
                                            emails = tempEmail
                                        } else {
                                            emails = emails + "," + tempEmail
                                        }
                                    }
                                }
                                
                                singleContactObj.stringSecondaryPhones = phoneNumbers
                                singleContactObj.stringSecondaryEmails = emails
                                singleContactObj.name = singleContact["name"] as? String ??  ""
                                singleContactObj.phoneNumber = singleContact["phoneNumber"] as? String ??  ""
                                
                                singleContactObj.userId = singleContact["userId"] as? String ??  ""
                                singleContactObj.email = singleContact["email"] as? String ??  ""
                                singleContactObj.company = singleContact["company"] as? String ??  ""
                                singleContactObj.companyEmail = singleContact["companyEmail"] as? String ??  ""
                                singleContactObj.title = singleContact["title"] as? String ??  ""
                                singleContactObj.website = singleContact["website"] as? String ??  ""
                                singleContactObj.notes = singleContact["notes"] as? String ??  ""
                                singleContactObj.isBackendSync = false
                                singleContactObj.notes = singleContact["notes"] as? String ??  ""
                                singleContactObj.source = singleContact["source"] as? String ??  ""
                                singleContactObj.contactFEId = singleContact["contactFEId"] as? String ??  ""
                                singleContactObj.isdeleted = singleContact["isdeleted"] as? Bool ??  false
                                singleContactObj.ismerged = singleContact["ismerged"]as? Bool ??  false
                                singleContactObj.mergeParentId = singleContact["mergeParentId"] as? String ??  ""
                                singleContactObj.privacyEnabled = singleContact["privacyEnabled"] as? Bool ??  true
                                singleContactObj.privacySetForContactUser = singleContact["privacySetForContactUser"] as? Bool ??  true
                                singleContactObj.isBlocked = singleContact["isBlocked"] as? Bool ??  false
                                singleContactObj.isBlockedByContact = singleContact["isBlockedByContact"] as? Bool ??  false
                                singleContactObj.picture = singleContact["picture"] as? String ??  ""
                                singleContactObj.sharedData = singleContact["sharedData"]as? Int ??  2
                                singleContactObj.createdAt = singleContact["createdAt"] as? String ??  ""
                                singleContactObj.updatedAt = singleContact["updatedAt"] as? String ??  ""
                                
                                DatabaseManagement.shared.insertNewContactInSQLiteStorage(contactObj: singleContactObj)
                                
                                if(i == contactList.count - 1) {
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "isFetchFromBackendCompleted", value: "yes")
                                    ContactsSync.shared.isSyncInProgress = false
                                }
                            }
                            self.getBlockedByContactsFromBackend()
                        }
                        self.finish()
                    } else {
                        self.finish()
                    }
                } else {
                    self.finish()
                }
            } else {
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
    func getBlockedByContactsFromBackend() {
        let getBlockedByContacts = getBlockedByContactsAPIOperation()
        getBlockedByContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
            let toastObj: [String: String] = ["toastMessage": SyncToastMessages().syncCompleted, "isSyncCompleted": "Yes"]
            NotificationCenter.default.post(name: SHOW_TOAST_MESSAGE, object: nil, userInfo: toastObj)
        }
        AppDelegate.addProcedure(operation: getBlockedByContacts)
    }
}

// Sync contacts API operation - BackendSYnc functionality
class SyncContactsAPIOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var responseContacts: [ContactObj] = []
    var responseRecentlyAddedContacts: [ContactObj] = []
    var contactObjs: [ContactObj] = []
    var addressList = [AddressType]()
    let date = Date()
    let dateFormatter = DateFormatter()
    
    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = NSTimeZone.local
    }
    override func execute() {
        var mainContactObj: [AnyObject] = []
        
        mainContactObj = getFormattedContacts(contacts: contactObjs)
        
        let parameters =
            [
                "action": "synccontacts",
                "idtoken": self.idtoken,
                "jsonObj":
                    [
                        "contacts": mainContactObj
                ]
                ] as [String: Any]
        
        

        let awsURL = AppConfig.syncContactsEncryptAPI
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {

                DispatchQueue.global().async {
                    if let contactsResponseObj = json as? [String: Any] {
                        if let contactList = contactsResponseObj["contacts"] as? [[String: Any]] {
                            for contact in contactList {
                                var singleContactObj = ContactObj()
                                self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                                guard self.idtoken != "" else {
                                    return
                                }
                                singleContactObj.name = contact["name"] as? String ??  ""
                                singleContactObj.contactId = Int(contact["contactId"] as? String ??  "") ?? 0
                                singleContactObj.isBackendSync = true
                                singleContactObj.phoneNumber = contact["phoneNumber"] as? String ??  ""
                                singleContactObj.email = contact["email"] as? String ??  ""
                                singleContactObj.contactFEId = contact["contactFEId"] as? String ??  ""
                                singleContactObj.privacyEnabled = contact["privacyEnabled"]as? Bool ??  true
                                singleContactObj.privacySetForContactUser = contact["privacySetForContactUser"] as? Bool ??  true
                                singleContactObj.picture = contact["picture"] as? String ??  ""
                                singleContactObj.sharedData = contact["sharedData"] as? Int ??  2
                                singleContactObj.isBlocked = contact["isBlocked"] as? Bool ??  false
                                singleContactObj.isBlockedByContact = contact["isBlockedByContact"] as? Bool ??  false
                                singleContactObj.createdAt = contact["createdAt"] as? String ??  ""
                                singleContactObj.updatedAt = contact["updatedAt"] as? String ??  ""
                                self.responseContacts.append(singleContactObj)
                                if(ContactsSync.shared.recentlyAddedContactsIdArray.contains(singleContactObj.contactId)) {
                                    self.responseRecentlyAddedContacts.append(singleContactObj)
                                }
                            }
                        }
                        // Track Event on Google Analytics //
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Sync contacts", label: "Sync contact successful", value: 0)
                        // Track Screen on Google Analytics  End //
                    } else {
                        // Track Event on Google Analytics //
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Sync contacts", label: "Sync contact successful", value: 0)
                        // Track Screen on Google Analytics  End //
                    }
                    
                    DatabaseManagement.shared.updateCotactInRecentlyAdded(syncResponse: self.responseRecentlyAddedContacts)
                    DatabaseManagement.shared.updateCotact(syncResponse: self.responseContacts)
                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                    self.finish()
                }
            } else {
                
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

// Sync contacts API operation - BackendSYnc functionality
class getUpdatedContactsAPIOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var responseContacts: [ContactObj] = []
    var responseRecentlyAddedContacts: [ContactObj] = []
    let date = Date()
    let dateFormatter = DateFormatter()
    
    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = NSTimeZone.local
    }
    override func execute() {
        let awsURL = AppConfig.getUpdatedContacts
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpBody = "".data(using: String.Encoding.utf8)
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                DispatchQueue.global().async {
                    if let contactsResponseObj = json as? [String: Any] {
                        if let contactList = contactsResponseObj["contacts"] as? [[String: Any]] {
                            for contact in contactList {
                                var singleContactObj = ContactObj()
                                self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                                guard self.idtoken != "" else {
                                    return
                                }
                                
                                singleContactObj.name = contact["name"] as? String ??  ""
                                singleContactObj.contactId = Int(contact["contactId"] as? String ??  "") ?? 0
                                singleContactObj.isBackendSync = true
                                singleContactObj.phoneNumber = contact["phoneNumber"] as? String ??  ""
                                singleContactObj.email = contact["email"] as? String ??  ""
                                singleContactObj.contactFEId = contact["contactFEId"] as? String ??  ""
                                singleContactObj.privacyEnabled = contact["privacyEnabled"]as? Bool ??  true
                                singleContactObj.privacySetForContactUser = contact["privacySetForContactUser"] as? Bool ??  true
                                singleContactObj.picture = contact["picture"] as? String ??  ""
                                singleContactObj.sharedData = contact["sharedData"] as? Int ??  2
                                singleContactObj.isBlocked = contact["isBlocked"] as? Bool ??  false
                                singleContactObj.isBlockedByContact = contact["isBlockedByContact"] as? Bool ??  false
                                self.responseContacts.append(singleContactObj)
                                if(ContactsSync.shared.recentlyAddedContactsIdArray.contains(singleContactObj.contactId)) {
                                    self.responseRecentlyAddedContacts.append(singleContactObj)
                                }
                            }
                        } else {
                            self.finish()
                        }
                    } else {
                        self.finish()
                    }
                    
                    DatabaseManagement.shared.updateCotactInRecentlyAdded(syncResponse: self.responseRecentlyAddedContacts)
                    DatabaseManagement.shared.updateContactWithBlockedDetails(syncResponse: self.responseContacts)
                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                    self.finish()
                }
                /* --------------- Track Event on Google Analytics --------------- */
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Get updated contacts", label: "Get updated contacts successfull", value: 0)
                /* --------------- Track Screen on Google Analytics  End --------------- */
            } else {
                //* --------------- Track Event on Google Analytics --------------- */
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Get updated contacts", label: "Failed to get updated contacts", value: 0)
                /* --------------- Track Screen on Google Analytics  End--------------- */
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

// Sync contacts API operation - BackendSYnc functionality
class blockUnblockOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var responseContacts: [ContactObj] = []
    var responseRecentlyAddedContacts: [ContactObj] = []
    let date = Date()
    let dateFormatter = DateFormatter()
    var contactObjs: [ContactObj] = []
    var isBlocked: Bool = false
    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = NSTimeZone.local
    }
    override func execute() {
        var mainContactObj: [AnyObject] = []
        mainContactObj = getFormattedContacts(contacts: contactObjs)
        let parameters =
            [
                "action": "blockUnblockOperation",
                "idtoken": self.idtoken,
                "jsonObj":
                    [
                        "contacts": mainContactObj
                ],
                "isBlocked": self.isBlocked
                ] as [String: Any]
        
        let awsURL = AppConfig.blockUnblockOperation
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        //            request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "PUT"
        
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        
        request.httpBody = httpBody
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
           
            
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                DispatchQueue.global().async {
                   
                    if let contactsResponseObj = json as? [String: Any] {
                        if let contactList = contactsResponseObj["contacts"] as? [[String: Any]] {
                            for contact in contactList {
                                var singleContactObj = ContactObj()
                                self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                                guard self.idtoken != "" else {
                                    return
                                }
                                
                                singleContactObj.name = contact["name"] as? String ??  ""
                                singleContactObj.contactId = Int(contact["contactId"] as? String ??  "") ?? 0
                                singleContactObj.isBackendSync = true
                                singleContactObj.phoneNumber = contact["phoneNumber"] as? String ??  ""
                                singleContactObj.email = contact["email"] as? String ??  ""
                                singleContactObj.contactFEId = contact["contactFEId"] as? String ??  ""
                                singleContactObj.privacyEnabled = contact["privacyEnabled"]as? Bool ??  true
                                singleContactObj.privacySetForContactUser = contact["privacySetForContactUser"] as? Bool ??  true
                                singleContactObj.picture = contact["picture"] as? String ??  ""
                                singleContactObj.sharedData = contact["sharedData"] as? Int ??  2
                                singleContactObj.isBlocked = contact["isBlocked"] as? Bool ??  false
                                singleContactObj.isBlockedByContact = contact["isBlockedByContact"] as? Bool ??  false
                                self.responseContacts.append(singleContactObj)
                                if(ContactsSync.shared.recentlyAddedContactsIdArray.contains(singleContactObj.contactId)) {
                                    self.responseRecentlyAddedContacts.append(singleContactObj)
                                }
                            }
                            self.finish()
                        } else {
                            self.finish()
                        }
                    } else {
                        self.finish()
                    }
                    
                    DatabaseManagement.shared.updateCotactInRecentlyAdded(syncResponse: self.responseRecentlyAddedContacts)
                    DatabaseManagement.shared.updateContactWithBlockedDetails(syncResponse: self.responseContacts)
                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                    self.finish()
                }
                /* --------------- Track Event on Google Analytics --------------- */
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Block unblock operation", label: "Block unblock operation successfull", value: 0)
                /* --------------- Track Screen on Google Analytics  End --------------- */
            } else {
                //* --------------- Track Event on Google Analytics --------------- */
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Block unblock operation", label: "Block unblock operation  failed", value: 0)
                /* --------------- Track Screen on Google Analytics  End--------------- */
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

// Sync contacts API operation - BackendSYnc functionality
class getBlockedByContactsAPIOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var phoneNumber = ""
    var countryCode = ""
    var email = ""
    var responseContacts: [ContactObj] = []
    var responseRecentlyAddedContacts: [ContactObj] = []
    let date = Date()
    let dateFormatter = DateFormatter()
    
    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.phoneNumber = appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
        self.countryCode = appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
        self.email = appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = NSTimeZone.local
    }
    override func execute() {
        let parameters =
            [
                "action": "getUpdatedContacts",
                "idtoken": self.idtoken,
                "phoneNumber": self.countryCode + self.phoneNumber ,
                "email": self.email
                ] as [String: Any]
        
        let awsURL = AppConfig.getBockedByContactList
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                DispatchQueue.global().async {
                    if let contactsResponseObj = json as? [String: Any] {
                        if let contactList = contactsResponseObj["contacts"] as? [[String: Any]] {
                            for contact in contactList {
                                var singleContactObj = ContactObj()
                                self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
                                guard self.idtoken != "" else {
                                    return
                                }
                                
                                singleContactObj.name = contact["name"] as? String ??  ""
                                singleContactObj.contactId = Int(contact["contactId"] as? String ??  "") ?? 0
                                singleContactObj.isBackendSync = true
                                singleContactObj.phoneNumber = contact["phoneNumber"] as? String ??  ""
                                singleContactObj.email = contact["email"] as? String ??  ""
                                singleContactObj.contactFEId = contact["contactFEId"] as? String ??  ""
                                singleContactObj.privacyEnabled = contact["privacyEnabled"]as? Bool ??  true
                                singleContactObj.privacySetForContactUser = contact["privacySetForContactUser"] as? Bool ??  true
                                singleContactObj.picture = contact["picture"] as? String ??  ""
                                singleContactObj.sharedData = contact["sharedData"] as? Int ??  2
                                singleContactObj.isBlocked = contact["isBlocked"] as? Bool ??  false
                                singleContactObj.isBlockedByContact = contact["isBlockedByContact"] as? Bool ??  false
                                self.responseContacts.append(singleContactObj)
                                if(ContactsSync.shared.recentlyAddedContactsIdArray.contains(singleContactObj.contactId)) {
                                    self.responseRecentlyAddedContacts.append(singleContactObj)
                                }
                            }
                            self.finish()
                        } else {
                            self.finish()
                        }
                    } else {
                        self.finish()
                    }
                    
                    DatabaseManagement.shared.updateCotactInRecentlyAdded(syncResponse: self.responseRecentlyAddedContacts)
                    DatabaseManagement.shared.updateContactWithBlockedDetails(syncResponse: self.responseContacts)
                    NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
                    self.finish()
                }
                
                // Track Event on Google Analytics //
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Get blocked by contacts", label: "Get blocked by contacts fetched successfull", value: 0)
                // Track Screen on Google Analytics  End //
                
            } else {
                // Track Event on Google Analytics //
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Get blocked by contacts", label: "Failed to get blocked by contacts", value: 0)
                // Track Screen on Google Analytics  End //
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

class RefreshContactList: Procedure {
    
    var appSharedPrefernce = AppSharedPreference()
    
    override init() {
        super.init()
    }
    override func execute() {
        ContactsSync.shared.recentlyAddedContacts = DatabaseManagement.shared.getRecentlyAddedContacts()
        ContactsSync.shared.recentlyAddedContacts = ContactsSync.shared.recentlyAddedContacts.reversed()
        
        ContactsSync.shared.FEContacts = DatabaseManagement.shared.getFisheyeContacts()
        ContactsSync.shared.nonFEContacts = DatabaseManagement.shared.getNonFisheyeContacts()
        
        ContactsSync.shared.recentlyAddedContactsIdArray = []
        if(ContactsSync.shared.recentlyAddedContacts != nil) {
            for contact in ContactsSync.shared.recentlyAddedContacts {
                ContactsSync.shared.recentlyAddedContactsIdArray.append(contact.contactId)
            }
        }
        
        self.finish()
    }
}

class ChangePrivacySetting: Procedure {
    
    var appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var privacyEnabled = true
    var contactFEId = ""
    var contactObjs: [ContactObj] = []
    var responseContacts: [ContactObj] = []
    var responseRecentlyAddedContacts: [ContactObj] = []
    
    override init() {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }
    override func execute() {
        var mainContactObj: [AnyObject] = []
        mainContactObj = getFormattedContacts(contacts: contactObjs)
        
        let parameters =
            [
                "action": "getUpdatedContacts",
                "idtoken": self.idtoken,
                "jsonObj": [
                    "contacts": mainContactObj
                ]
                ] as [String: Any]
        let awsURL = AppConfig.changePrivacySetting
       
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "PUT"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                DispatchQueue.global().async {
                    
                    if let responseJson = json as? [String: Any] {
                        if let statusCode = responseJson["statusCode"] as? String {
                            if statusCode == "0" {
                                if let contactList = responseJson["contacts"] as? [[String: Any]] {
                                    for contactObj in contactList {
                                        
                                        var tempContact = ContactObj()
                                        tempContact.name = contactObj["name"] as? String ??  ""
                                        tempContact.contactId = Int(contactObj["contactId"] as? String ??  "") ?? 0
                                        tempContact.isBackendSync = true
                                        tempContact.phoneNumber = contactObj["phoneNumber"] as? String ??  ""
                                        tempContact.email = contactObj["email"] as? String ??  ""
                                        tempContact.contactFEId = contactObj["contactFEId"] as? String ??  ""
                                        tempContact.privacyEnabled = contactObj["privacyEnabled"]as? Bool ??  true
                                        tempContact.privacySetForContactUser = contactObj["privacySetForContactUser"] as? Bool ??  true
                                        tempContact.picture = contactObj["picture"] as? String ??  ""
                                        tempContact.sharedData = contactObj["sharedData"] as? Int ??  2
                                        tempContact.isBlocked = contactObj["isBlocked"] as? Bool ??  false
                                        tempContact.isBlockedByContact = contactObj["isBlockedByContact"] as? Bool ??  false
                                        tempContact.createdAt = contactObj["createdAt"] as? String ??  ""
                                        tempContact.updatedAt = contactObj["updatedAt"] as? String ??  ""
                                        self.responseContacts.append(tempContact)
                                        if(ContactsSync.shared.recentlyAddedContactsIdArray.contains(tempContact.contactId)) {
                                            self.responseRecentlyAddedContacts.append(tempContact)
                                        }
                                        
                                    }
                                    DatabaseManagement.shared.updateCotactInRecentlyAdded(syncResponse: self.responseRecentlyAddedContacts)
                                    DatabaseManagement.shared.updateCotact(syncResponse: self.responseContacts)
                                }
                            }
                        }
                    }
                    self.finish()
                }
                
                // Track Event on Google Analytics //
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Change privacy setting", label: "Changed privacy setting", value: 0)
                // Track Screen on Google Analytics  End //
                
            } else {
                // Track Event on Google Analytics //
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Backend Sync", action: "Change privacy setting", label: "Failed to change privacy setting", value: 0)
                // Track Screen on Google Analytics  End //
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}
