//
//  CreateMeetmeViewController.swift
//  fisheye
//
//  Created by Keerthi on 12/10/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit

@objc protocol CreateMeetMeDelegate {
    @objc optional func didCreateMeeting()

}

class CreateMeetmeViewController: UIViewController {

    @IBOutlet weak var createMeetingLbl: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createMeetingBtn: UIButton!
    @IBOutlet weak var mainView: UIView!

     @IBOutlet weak var bottomView: UIView!

    fileprivate var addressArray: [FEAddress]!

    var titile: String!
    var titleDesc: String!

    var selectedPlaceMark: MKPlacemark?

    var updateAddressDelegate: UpdateAddress?
    weak var delegate: AddContactDelegate?

    var secArray: [String] = ["Title", "Description", "Address", "From Date", "To Date", ""]

    override func viewDidLoad() {

            super.viewDidLoad()

            //closeBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)

            self.createMeetingBtn.setTitleColor(UIColor.white, for: .normal)

            self.closeBtn.isUserInteractionEnabled = true

            self.closeBtn.addTarget(self, action: #selector(backclick(_:)), for: .touchUpInside)

            self.view.bringSubview(toFront: self.mainView)

            self.view.bringSubview(toFront: self.bottomView)

            self.createMeetingBtn.backgroundColor = UIColor.frenchBlue()

            self.createMeetingBtn.layer.cornerRadius = 0.09467391304*self.createMeetingBtn.bounds.width

            self.createMeetingBtn.setTitleColor(UIColor.white, for: .normal)

//                self.phoneNumArray = [""]
//
//                self.emailArray = [""]
//
                self.addressArray = [FEAddress()]

                self.title = ""

           // self.createMeetingBtn.addTarget(self, action: #selector(addContact), for: .touchUpInside)

            self.tableView.dataSource = self

            self.tableView.delegate = self

//            let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController
//            
//            vc.footerTitle.isHidden = true
//            
//            vc.footerSubTitle.isHidden = true
//            
//            // Do any additional setup after loading the view.

    }
    func applyViewShadow() {

        //        kApplicationDelegate.window?.bringSubview(toFront: self.view)

        self.view.layer.cornerRadius = 8

        // border
        self.view.layer.borderWidth = 0
        self.view.layer.borderColor = UIColor.black.cgColor

        self.mainView.layer.cornerRadius = 8

        // border
        self.mainView.layer.borderWidth = 0
        self.mainView.layer.borderColor = UIColor.black.cgColor

        self.mainView.layer.backgroundColor = UIColor.white.cgColor

        self.mainView.layer.shadowColor = UIColor.black.cgColor
        self.mainView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.mainView.layer.shadowOpacity = 0.7
        self.mainView.layer.shadowRadius = 4.0

        self.mainView.layer.masksToBounds = false

        self.bottomView.layer.masksToBounds = false
        self.bottomView.layer.cornerRadius = 8
        //self.bottomView.layer.forwardingTarget(for: #selector(addContact))

        //    self.view.layer.sublayers?.forEach { $0.removeFromSuperlayer() }

        //        let tap = UITapGestureRecognizer.init(target: self, action: #selector(addContact))
        //
        //
        //
        //        self.addContactLbl.isUserInteractionEnabled = true
        //
        //        self.addContactLbl.addGestureRecognizer(tap)

        self.createMeetingBtn.isUserInteractionEnabled = true

    }

    func backclick(_ sender: Any) {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController;              vc.hideBottomviews()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateMeetmeViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {

        case 6:
            return 80
        default:
            return 54
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return secArray.count

        //        return array.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()

        //  let phoneIndex = self.array.index(of: "Phone No.")
        //  let emailIndex = self.array.index(of: "Email")

        //  let addressIndex = self.array.index(of: "Address Line1(Street/Landmark)")

        //  let secEmailIndex = addressIndex!-emailIndex!-2

//        switch self.secArray[indexPath.section] {
//        case "Title":
//
//            let genCell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath) as! ContactGenricCell
//           // genCell.cellType = .nameField
//            genCell.delegate = self as! ContactGenricCellDelegate
//            genCell.custTextField.text = self.title
//            cell = genCell
//
//        case "Description":
//            let genCell = tableView.dequeueReusableCell(withIdentifier: "descCell", for: indexPath) as! ContactGenricCell
//            // genCell.cellType = .nameField
//            genCell.delegate = self as! ContactGenricCellDelegate
//            genCell.custTextField.text = self.titleDesc
//            cell = genCell
//
//
//
//        case "From Date":
//
//            let genCell = tableView.dequeueReusableCell(withIdentifier: "fromDateCell", for: indexPath) as! ContactGenricCell
//            // genCell.cellType = .nameField
//            genCell.delegate = self as! ContactGenricCellDelegate
//           // genCell.custTextField.text = self.fromDate
//            cell = genCell
//
//        case "Address":
//
//
//            let  addressCell = tableView.dequeueReusableCell(withIdentifier: "addLine1Cell", for: indexPath) as! AddressLine1Cell
//            addressCell.mapImageView.isUserInteractionEnabled = true
//
//            addressCell.searchMapLbl.isUserInteractionEnabled = true
//
//            addressCell.addressLine1.textContainerInset = .zero
//
//            addressCell.addressLine1.delegate = self
//
//            addressCell.addressLine1.textContainer.lineFragmentPadding = 0.0
//
//            let tap = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))
//
//            tap.numberOfTapsRequired = 1
//
//            let tap2 = UITapGestureRecognizer(target: self, action: #selector(navigateToMaps))
//
//            tap2.numberOfTapsRequired = 1
//
//            addressCell.mapImageView.addGestureRecognizer(tap2)
//
//            addressCell.searchMapLbl.addGestureRecognizer(tap)
//
//
//            if let streetAddress = self.addressArray[0].streetAddress{
//                addressCell.addressLine1.text = streetAddress
//            }
//
//            if let locality = self.addressArray[0].locality{
//                addressCell.addressLine1.text?.append(",\(locality)")
//            }
//
//
//
//            if let state = self.addressArray[0].region{
//                addressCell.addressLine2.text = state
//            }
//            if let country = self.addressArray[0].country{
//                addressCell.addressLine2.text?.append(",\(country)")
//            }
//
//            //addressCell.addressLine2.text = state + country //self.contact != nil? self.contact?.address?.streetAddress : addressCell.addressLine2.text
//            if let postalCode = self.addressArray[0].postalCode{
//                addressCell.zipCode.text = postalCode//self.contact != nil ? self.contact?.address?.postalCode : addressCell.zipCode.text
//            }
//
//            cell = addressCell
//
//
//        case "":
//            let genCell = tableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! ContactGenricCell
//            genCell.cellType = .companyField
//            genCell.delegate = self as! ContactGenricCellDelegate
//            genCell.custTextField.text = self.company//self.contact != nil ? self.contact!.company : genCell.custTextField.text
//            cell = genCell
//
//
//        default:
//            break
//        }
        return cell
    }

    func navigateToMaps() {

        let mapVC = MapViewController.instantiateFromStoryboardWithIdentifier(identifier: "MapViewController")

        //mapVC.addressSearchDelegate = self as AddressSearch

        let navController = UINavigationController(rootViewController: mapVC)

        navigationController?.present(navController, animated: true, completion: nil)

    }

    func addTapped(_ sender: Any) {

        //        address?.country = "India"
        //        address?.formatted = "jhgjg"
        //        address?.locality = "Banh"
        //        address?.postalCode = "5765765"
        //        address?.region = "hgjhgj"
        //        address?.streetAddress = "kjhkhihoi"
        //        address?.type = self.addressTypeTxtField.text

        if let placemark  = selectedPlaceMark {
            if let tit = placemark.title, let loc = placemark.locality, let subLoc = placemark.subLocality, let postalCode = placemark.postalCode, let state = placemark.administrativeArea, let country = placemark.country, let name = placemark.name, let thoroughfare = placemark.thoroughfare {

                let address = FEAddress()
                address?.formatted = "\(tit) \(name) \(loc) \(state) \(country) "
                address?.streetAddress = thoroughfare
                address?.locality = loc
                address?.region = state
                address?.postalCode = postalCode
                address?.country = country

                addressArray.append(address!)

                //   self.tableView.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
        }
        //            else{
        //
        //                self.view.endEditing(true)
        //
        //                if (addressLine1.text?.characters.count)! <= 0 {
        //
        //                    self.addressLine1.configOnError(withPlaceHolder: "Enter Addressline 1")
        //                }
        //
        //
        //                else if  (self.addressLine2.text?.characters.count)! <= 0{
        //
        //                    self.addressLine2.configOnError(withPlaceHolder: "Enter Addressline 2")
        //                }
        //
        ////                else if  (self.cityTextField.text?.characters.count)! <= 0{
        ////
        ////                    self.cityTextField.configOnError(withPlaceHolder: "Enter City")
        ////                }
        ////
        ////
        ////
        ////                else if  (self.stateTextField.text?.characters.count)! <= 0{
        ////
        ////                    self.stateTextField.configOnError(withPlaceHolder: "Enter State")
        ////                }
        //
        //
        //                else if  (self.postalCodeTextField.text?.characters.count)! <= 0{
        //
        //                    self.postalCodeTextField.configOnError(withPlaceHolder: "Enter Postal Code")
        //                }
        //
        ////                else if (self.countryTextField.text?.characters.count)! <= 0{
        ////                    self.countryTextField.configOnError(withPlaceHolder: "Enter Country")
        ////
        ////                }
        //
        //
        //                else{
        //                    self.address = FEAddress()
        //
        //                    self.address?.formatted = self.addressLine1.text
        //                    self.address?.streetAddress = self.addressLine2.text
        ////                    address?.locality = self.cityTextField.text
        ////                    address?.region = self.stateTextField.text
        //                    self.address?.postalCode = self.postalCodeTextField.text
        ////                    address?.country = self.countryTextField.text
        //
        //                }
        //
        //            }

    }

}

extension CreateMeetmeViewController: AddressSearch {

    func addrssObjectFrom(place: MKPlacemark?) {

        if let placemark = place {
            selectedPlaceMark = placemark

            let address = FEAddress()

            if let tit = placemark.title {
                address?.formatted = "\(tit)"
            }

            if let name = placemark.name {
                address?.streetAddress = "\(name)"
            }

            if let thoroughfare = placemark.thoroughfare {
                address?.streetAddress?.append(thoroughfare)
            }
            if let subLoc = placemark.subLocality {
                //self.addressLine2.text = "\(subLoc)"
            }
            if let loc = placemark.locality {
                address?.locality = "\(loc)"
            }
            if let postalCode = placemark.postalCode {
                address?.postalCode = "\(postalCode)"
            }
            if let state = placemark.administrativeArea {
                address?.region = "\(state)"
            }
            if let country = placemark.country {
                address?.country = "\(country)"
            }

            addressArray[0] = address!

            self.tableView.reloadSections(IndexSet(integer: 3), with: .none)

        }
    }

}
