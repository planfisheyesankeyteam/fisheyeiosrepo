//
//  SearchContactsTableViewCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 05/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class SearchContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var contactCellView: UIView!
    @IBOutlet weak var contactCellImageView: UIImageView!
    @IBOutlet weak var contactCellName: UILabel!
    @IBOutlet weak var contactCellPhoneNumber: UILabel!
    @IBOutlet weak var contactCellEmailId: UILabel!
    @IBOutlet weak var contactCellCheckboxImage: UIImageView!
    @IBOutlet weak var contactCellCheckboxButton: MyButton!
    @IBOutlet weak var isFisheyeStrip: UIView!
    @IBOutlet weak var seperator: UIView!
    @IBOutlet weak var singleDetailDisplay: UILabel!
    @IBOutlet weak var stackViewDetails: UIView!

    var isChecked = false
    var row: Int?
    var section: Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class MyButton: UIButton {

    var row: Int?
    var section: Int?

}
