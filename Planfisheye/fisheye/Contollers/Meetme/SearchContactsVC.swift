//
//  SearchContactsVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 05/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class SearchContactsVC: UIViewController, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

      //Layout variables
      @IBOutlet weak var contactsView: UIView!
      @IBOutlet weak var appBarView: UIView!
      @IBOutlet weak var contactsCollectionView: UICollectionView!
      @IBOutlet weak var searchView: UIView!
      @IBOutlet weak var contactsTableView: UITableView!

      @IBOutlet weak var noContactsSelectedView: UIView!
      //Appbar layout variables
      @IBOutlet weak var backButtonImageView: UIImageView!
      @IBOutlet weak var backButton: UIButton!
      @IBOutlet weak var addParticipantsLabel: UILabel!
      @IBOutlet weak var searchImageView: UIImageView!
      @IBOutlet weak var participantCountLabel: UILabel!
      @IBOutlet weak var searchButton: UIButton!

      @IBOutlet weak var searchBar: UISearchBar!
      @IBOutlet weak var saveButton: UIButton!

      @IBOutlet weak var noContactsView: UIView!
      @IBOutlet weak var noContactsLabel: UILabel!

      var fisheyeContacts: [ContactObj] = []
      var nonFisheyeContacts: [ContactObj] = []
      var allContactList: [ContactObj] = []
      var currentMeet: [ContactObj] = []
      var sectionData: [Int: [ContactObj]] = [:]
      var totalContacts = 0
      var selectedContacts = 0
      var isFromDetailVc = false
      var hideTableSection0 = false
      var hideTableSection1 = false
      var meetmeId: String!
      var sendSelectedContacts: SendSelectedContacts?
      var redirectProtocol: RedirectedToMeetmeLogbookVC?
      let appSharedPrefernce = AppSharedPreference()
      var appService = AppService.shared
      var mainContactObj: [AnyObject] = []
      var addParticipentAfterMeetCreated = false
      @IBOutlet weak var noContactsSelectedLabel: UILabel!

    @IBOutlet var addParticipants: UILabel!

      var idtoken = ""
      var sections = ["Contacts on Fisheye", "Contacts from Phone"]

      override func viewDidLoad() {
            self.stopLoader()
            self.appService = AppService.shared
            if currentMeet.count>0 {
                  self.noContactsSelectedView.isHidden = true
            } else {
                  self.noContactsSelectedView.isHidden = false
            }
            super.viewDidLoad()
            self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            searchBar.delegate = self
            searchBar.showsCancelButton = true
            contactsTableView.delegate = self
            self.getContacts()
            sectionData =  [0: self.fisheyeContacts, 1: self.fisheyeContacts]
            setLocalizationText()
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SearchContactsVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.noContactsSelectedLabel.text = self.appService.noContactsSelectedLabel
                  self.sections = [self.appService.contactOnFisheye, self.appService.contactOnPhone]
                  self.saveButton.setTitle(self.appService.save, for: .normal)
                  self.addParticipants.text = self.appService.addParticipant
            }
      }

      /******************* IBAction functions start here *******************/
      @IBAction func onAppBarSearchButtonTapped(_ sender: Any) {
            searchView.isHidden = false
            appBarView.isHidden = true
      }

      @IBAction func onBackButtonTapped(_ sender: Any) {
            self.view.removeFromSuperview()
      }

      @IBAction func onSaveButtonTapped(_ sender: Any) {
            if !isFromDetailVc {
                  GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add Participant", action: "Save button clicked", label: "User saves participants Successfully", value: 0)
                  sendSelectedContacts?.addContacts(addedContacts: currentMeet)
                  self.view.removeFromSuperview()
            } else {
                  if self.selectedContacts > 0 {
                        self.addParticipantsCallWithoutProcdure()
                  } else {
                        self.view.removeFromSuperview()
                  }

            }
      }
      /******************* IBAction functions end here *******************/

      /******************* Search related functions start here *******************/

      func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            self.searchBar.endEditing(true)
            let searchKeyText = searchBar.text
            fisheyeContacts = DatabaseManagement.shared.searchUnblockedFEContacts(searchText: searchKeyText)
            nonFisheyeContacts = DatabaseManagement.shared.searchUnblockedNonFEContacts(searchText: searchKeyText)
            getSearchedContacts(searchKey: searchKeyText! )
      }

      func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchView.isHidden = true
            appBarView.isHidden = false
            self.searchBar.endEditing(true)
            self.getContacts()
      }

      func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            if( searchBar.text == "") {
                  self.getContacts()
            }
//            let searchKeyText = searchText
//            fisheyeContacts = DatabaseManagement.shared.searchUnblockedFEContacts(searchText: searchKeyText)
//            nonFisheyeContacts = DatabaseManagement.shared.searchUnblockedNonFEContacts(searchText: searchKeyText)
//            getSearchedContacts(searchKey:searchKeyText )
      }
      /******************* Search related functions end here *******************/

      /********************* Collectionview for selected participants starts here *******************/
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

            return currentMeet.count
      }

      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchContactsCollectionViewCell", for: indexPath) as! SearchContactsCollectionViewCell
            let selectedContact = currentMeet[indexPath.row]
            if selectedContact.name == "" || selectedContact.name == " "  || selectedContact.name == nil {
                  cell.contactName.text = "Unknown"
            } else {
                  cell.contactName.text = selectedContact.name
            }

            cell.selectedContactImage?.sd_setImage(with: URL(string: selectedContact.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            cell.selectedContactImage.layer.cornerRadius =   cell.selectedContactImage.frame.size.height / 2
            cell.selectedContactImage.layer.masksToBounds = true
            cell.deselectContactButton.tag = indexPath.row
            let tapGestureDeselect = UITapGestureRecognizer(target: self, action: #selector(deselectContact(sender:)))
            cell.deselectContactButton.addGestureRecognizer(tapGestureDeselect)
            return cell
      }

      /********************* Collectionview for selected participants ends here *******************/

      func numberOfSections(in tableView: UITableView) -> Int {
            return self.sections.count
      }

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            switch (section) {
            case 0 :
                  if hideTableSection0 {
                        return 0
                  } else {
                        return (sectionData[section]?.count)!
                  }
            case 1:
                  if hideTableSection1 {
                        return 0
                  } else {
                        return (sectionData[section]?.count)!
                  }

            default:
                  return (sectionData[section]?.count)!
            }
      }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let tableCell = Bundle.main.loadNibNamed("SearchContactsTableViewCell", owner: self, options: nil)?.first as! SearchContactsTableViewCell

            let contactObj = sectionData[indexPath.section]![indexPath.row]
            if contactObj.name != " "{
                  tableCell.contactCellName.text = contactObj.name
            } else {
                  tableCell.contactCellName.text = "Unknown"
            }

            let privacyEnabled = contactObj.privacyEnabled ?? false
            if(contactObj.phoneNumber.isEmpty && !contactObj.email.isEmpty) {
                  tableCell.stackViewDetails.isHidden = true
                  tableCell.singleDetailDisplay.isHidden = false
                  tableCell.singleDetailDisplay.text = contactObj.email
            } else if(!contactObj.phoneNumber.isEmpty && contactObj.email.isEmpty) {
                  tableCell.stackViewDetails.isHidden = true
                  tableCell.singleDetailDisplay.isHidden = false
                  tableCell.singleDetailDisplay.text = contactObj.phoneNumber
            } else if(!contactObj.phoneNumber.isEmpty && !contactObj.email.isEmpty) {
                  tableCell.singleDetailDisplay.isHidden = true
                  tableCell.stackViewDetails.isHidden = false
                  if(privacyEnabled && contactObj.sharedData == 0) {
                        tableCell.singleDetailDisplay.text = contactObj.email
                        tableCell.seperator.isHidden = true
                        tableCell.stackViewDetails.isHidden = true
                  } else if(privacyEnabled && contactObj.sharedData == 1) {
                        tableCell.singleDetailDisplay.text = contactObj.phoneNumber
                        tableCell.seperator.isHidden = true
                        tableCell.stackViewDetails.isHidden = true
                  } else {
                        tableCell.singleDetailDisplay.isHidden = true
                        tableCell.stackViewDetails.isHidden = false
                        tableCell.contactCellPhoneNumber.text = contactObj.phoneNumber
                        tableCell.contactCellEmailId.text = contactObj.email
                  }
            } else {
                  tableCell.contactCellPhoneNumber.text = "n/a"
                  tableCell.contactCellEmailId.text = "n/a"
                  tableCell.seperator.isHidden = true
            }

            tableCell.contactCellImageView.layer.cornerRadius =   tableCell.contactCellImageView.frame.size.height / 2
            tableCell.contactCellImageView.layer.masksToBounds = true
            tableCell.contactCellImageView?.sd_setImage(with: URL(string: contactObj.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))

            let isSelected = currentMeet.contains(where: { $0.contactId == contactObj.contactId })
            if isSelected {
                  tableCell.isChecked = true
                  tableCell.contactCellCheckboxImage.image = UIImage(named: "successBtn")
            } else {
                  tableCell.isChecked = false
                  tableCell.contactCellCheckboxImage.image = UIImage(named: "unselectedRadioBtn")
            }

            if(indexPath.section==0) {
                  tableCell.isFisheyeStrip.isHidden = false
                  tableCell.isFisheyeStrip.backgroundColor = UIColor.purpleRing()
            } else {
                  tableCell.isFisheyeStrip.isHidden = true

            }

            let tapGestureFullTab = TapGestureRecognizer(target: self, action: #selector(addContact(sender:)))
            tapGestureFullTab.row = indexPath.row
            tapGestureFullTab.section = indexPath.section
            tableCell.contactCellCheckboxButton.row = indexPath.row
            tableCell.contactCellCheckboxButton.section = indexPath.section
            tableCell.contactCellCheckboxButton.addGestureRecognizer(tapGestureFullTab)

            let tapGestureSelect = TapGestureRecognizer(target: self, action: #selector(addContact(sender:)))
            tapGestureSelect.row = indexPath.row
            tapGestureSelect.section = indexPath.section
            tableCell.row = indexPath.row
            tableCell.section = indexPath.row
            tableCell.addGestureRecognizer(tapGestureSelect)

            return tableCell
      }

      func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            switch (section) {
            case 0 :
                  if hideTableSection0 {
                        return UIView(frame: CGRect.zero)
                  } else {
                        let view = UIView()
                        let title = UILabel()
                        title.font = UIFont(name: "Helvetica Neue", size: 10)
                        title.text = self.sections[section]
                        title.textColor = UIColor.black
                        title.font = UIFont.italicSystemFont(ofSize: 10)
                        let size = CGSize(width: 250, height: 1000)
                        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                    let estimatedFrame = NSString(string: title.text!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], context: nil)
                        view.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 25)
                        title.frame = CGRect(x: (view.frame.width/2)-(estimatedFrame.width/2), y: 4, width: 200, height: estimatedFrame.height )
                        let constarint = NSLayoutConstraint(item: title, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
                        view.addSubview(title)
                        view.addConstraint(constarint)
                        view.backgroundColor =  UIColor.contactsTypeBg()
                        return view
                  }
            case 1:
                  if hideTableSection1 {
                        return UIView(frame: CGRect.zero)
                  } else {
                        let view = UIView()
                        let title = UILabel()
                        title.font = UIFont(name: "Helvetica Neue", size: 10)
                        title.text = self.sections[section]
                        title.textColor = UIColor.black
                        title.font = UIFont.italicSystemFont(ofSize: 10)
                        let size = CGSize(width: 250, height: 1000)
                        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                    let estimatedFrame = NSString(string: title.text!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], context: nil)
                        view.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 25)
                        title.frame = CGRect(x: (view.frame.width/2)-(estimatedFrame.width/2), y: 4, width: 200, height: estimatedFrame.height )
                        let constarint = NSLayoutConstraint(item: title, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
                        view.addSubview(title)
                        view.addConstraint(constarint)
                        view.backgroundColor =  UIColor.contactsTypeBg()
                        return view
                  }

            default:
                  return nil
            }

      }

      func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            switch (section) {
            case 0 :
                  if hideTableSection0 {
                        return 0
                  } else {
                        return 25
                  }
            case 1:
                  if hideTableSection1 {
                        return 0
                  } else {
                        return 25
                  }

            default:
                  return 25
            }
      }

    @objc func addContact(sender: Any ) {
            let indexRow = (sender as! TapGestureRecognizer).row
            let indexSection =  (sender as! TapGestureRecognizer).section
            var selectedContactObj: ContactObj?
            switch(indexSection!) {
            case 0:
                  selectedContactObj = self.fisheyeContacts[indexRow!]
                  break
            case 1:
                  selectedContactObj = self.nonFisheyeContacts[indexRow!]
                  break

            default:
                  break
            }


            let indexPath = IndexPath(row: indexRow!, section: indexSection!)
            guard let cell = contactsTableView.cellForRow(at: indexPath ) as? SearchContactsTableViewCell
                  else { return }
            
            if cell.isChecked {

                  for i in 0..<currentMeet.count {
                        if let selectedCon = currentMeet[i]  as?  ContactObj {
                              if selectedContactObj?.contactId == selectedCon.contactId {
                                    currentMeet.remove(at: i)
                                    break
                              }
                        }
                  }
                  selectedContacts -= 1
                  self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
                  contactsCollectionView.reloadData()
                  cell.isChecked = false
                  cell.contactCellCheckboxImage.image = UIImage(named: "unselectedRadioBtn")
            } else {
                
                  if let  track = MeetMeTrackingOptions.meetmeParticipantLimit {
                        if selectedContacts < track {
                              selectedContacts += 1
                              let contactObj = [
                                    "contactId": selectedContactObj?.contactId,
                                    "fisheyeId": selectedContactObj?.contactFEId
                                    ] as [String: Any]
                              mainContactObj.append(contactObj as AnyObject)
                              currentMeet.append(selectedContactObj!)
                              contactsCollectionView.reloadData()
                              cell.isChecked = true
                              self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
                              cell.contactCellCheckboxImage.image = UIImage(named: "successBtn")
                        } else {
                             
                            self.view.makeToast("This version is available for up to \(track) participants only. The premium version will offer more participants to be added in a session.", duration: AppService.shared.meetmeSearchContactTimeInterval, position: .bottom)
                              //                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.meetmeSearchContactTimeInterval, execute: {
                              //                    self.view.hideToast()
                              //                })
                        }
                  }
            }
            if self.selectedContacts>0 {
                  self.noContactsSelectedView.isHidden = true
            } else {
                  self.noContactsSelectedView.isHidden = false
            }
      }

    @objc func deselectContact(sender: Any ) {
            let index = (sender as AnyObject).view!.tag
            self.currentMeet.remove(at: index)
            selectedContacts -= 1
            self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
            contactsCollectionView.reloadData()
            contactsTableView.reloadData()
            if self.selectedContacts>0 {
                  self.noContactsSelectedView.isHidden = true

            } else {
                  self.noContactsSelectedView.isHidden = false
            }

      }
      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }
}

extension SearchContactsVC {
      func getContacts() {

            /* get fisheye contacts */
            let fisheyeContacts: [ContactObj]
            fisheyeContacts = DatabaseManagement.shared.getUnblockedFEContacts()
            self.fisheyeContacts = []
            self.fisheyeContacts = fisheyeContacts
            self.fisheyeContacts.sort { $0.name < $1.name }
            /** End **/

            /* get non fisheye contacts */
            let nonFisheyeContacts: [ContactObj]
            nonFisheyeContacts = DatabaseManagement.shared.getUnblockedNonFEContacts()
            self.nonFisheyeContacts = []
            self.nonFisheyeContacts = nonFisheyeContacts
            self.nonFisheyeContacts.sort { $0.name < $1.name }
            /** End **/

            if self.fisheyeContacts.count == 0 {
                  hideTableSection0 = true
            } else {
                  hideTableSection0 = false
            }

            if self.nonFisheyeContacts.count == 0 {
                  hideTableSection1 = true
            } else {
                  hideTableSection1 = false
            }

            DispatchQueue.main.async {
                        if self.fisheyeContacts.count != 0 || self.nonFisheyeContacts.count != 0 {
                              self.sectionData =  [0: self.fisheyeContacts, 1: self.nonFisheyeContacts]
                              self.contactsTableView.reloadData()
                              self.contactsTableView.isHidden = false
                              self.noContactsView.isHidden = true
                        } else {
                              self.contactsTableView.isHidden = true
                              self.noContactsView.isHidden = false
                              self.noContactsLabel.text = self.appService.noContactsLabel
                        }

            }
      }

      func getSearchedContacts (searchKey: String ) {

            self.fisheyeContacts.sort { $0.name < $1.name }
            self.nonFisheyeContacts.sort { $0.name < $1.name }

            if self.fisheyeContacts.count == 0 {
                  hideTableSection0 = true
            } else {
                  hideTableSection0 = false
            }

            if self.nonFisheyeContacts.count == 0 {
                  hideTableSection1 = true
            } else {
                  hideTableSection1 = false
            }

            DispatchQueue.main.async {
                  if self.fisheyeContacts.count == 0 && self.nonFisheyeContacts.count == 0 {

                        self.contactsTableView.isHidden = true
                        self.noContactsView.isHidden = false
                        self.noContactsLabel.text = self.appService.noContactsLabel2 + " \"\(searchKey)\""
                  } else {
                        if self.currentMeet.count>0 {
                              self.noContactsSelectedView.isHidden = true
                        } else {
                              self.noContactsSelectedView.isHidden = false
                        }
                        self.contactsTableView.isHidden = false
                        self.noContactsView.isHidden = true
                        self.sectionData =  [0: self.fisheyeContacts, 1: self.nonFisheyeContacts]
                        self.contactsTableView.reloadData()
                  }

            }

      }
}

extension SearchContactsVC {
      func addParticipantsApiCall() {
            let addParticipantApi =
                  AddParticipants(meetmeId: meetmeId, fisheyeids: mainContactObj)

            addParticipantApi.addDidFinishBlockObserver { [unowned self] (operation, _) in

                  if operation.responseStatus == "200"{
                        self.redirectProtocol?.nextAction(method: "addParticipants")
                        self.view.removeFromSuperview()
                  } else {

                  }
            }
            AppDelegate.addProcedure(operation: addParticipantApi)
      }

      func addParticipantsCallWithoutProcdure() {

            let parameters =
                  [
                        "participants": mainContactObj
                        ] as [String: Any]
            let awsURL = AppConfig.addParticipantUrl(meetMeId: meetmeId)
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json

                        let responseStatus = data["statusCode"] as? String ?? ""
                        if responseStatus == "200"{

                              DispatchQueue.main.async {
                                    self.view.makeToast(self.appService.contactsAddedSuccess)
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add Participant", action: "Save button clicked", label: "User saves participants Successfully", value: 0)
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.meetmeSearchContactTimeInterval, execute: {
                                    self.view.hideToast()
                                    self.redirectProtocol?.nextAction(method: "addParticipants")
                                    self.view.removeFromSuperview()
                              })

                        } else {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.appService.contactsAddedFailure)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.meetmeSearchContactTimeInterval, execute: {
                                    self.view.hideToast()

                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add Participant", action: "Save button clicked", label: "User's attempt to save participants Failed", value: 0)
                        }

                  } else {

                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }

}
