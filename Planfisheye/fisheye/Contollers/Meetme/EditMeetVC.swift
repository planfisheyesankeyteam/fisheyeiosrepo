//
//  EditMeetVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 27/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import MapKit
import Toast_Swift
import DatePickerDialog
import DropDown

class EditMeetVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

      // Outlet Variables
      @IBOutlet weak var editMeetCompleteView: UIView!
      @IBOutlet weak var editMeetTitleLabel: UILabel!

      @IBOutlet weak var appbarView: UIView!
      @IBOutlet weak var editMeetScrollView: UIScrollView!
      @IBOutlet weak var editMeetMainView: UIView!
      @IBOutlet weak var backButton: UIButton!

      @IBOutlet weak var titleImageView: UIImageView!
      @IBOutlet weak var titleCustomTextField: UICustomTextField!
      @IBOutlet weak var titleUnderlineImageView: UIImageView!

      @IBOutlet weak var meetLocationCustomTextField: UICustomTextField!
      @IBOutlet weak var locationUnderlineImageView: UIImageView!
      @IBOutlet weak var locationImageView: UIImageView!
      @IBOutlet weak var mapImageView: UIImageView!
      @IBOutlet weak var mapButton: UIButton!

      @IBOutlet weak var tapToLocateLabel: UILabel!
      @IBOutlet weak var meetStartsAtLabel: UILabel!
      @IBOutlet weak var meetStartDateTimingView: UIView!
      @IBOutlet weak var meetEndDateTimmingView: UIView!
      @IBOutlet weak var meetStartTimeView: UIView!
      @IBOutlet weak var meetStartTimeUnderlineImageView: UIImageView!
      @IBOutlet weak var meetStartTimeLabel: UILabel!
      @IBOutlet weak var meetStartTimeImageView: UIImageView!
      @IBOutlet weak var meetStartTimeButton: UIButton!

      @IBOutlet weak var meetStartDateView: UIView!
      @IBOutlet weak var meetStartDateUnderlineImage: UIImageView!
      @IBOutlet weak var meetStartDateImageView: UIImageView!
      @IBOutlet weak var meetStartDateButton: UIButton!
      @IBOutlet weak var meetStartDateLabel: UILabel!

      @IBOutlet weak var meetEndTimeView: UIView!
      @IBOutlet weak var meetEndTimeLabel: UILabel!
      @IBOutlet weak var meetEndTimeButton: UIButton!
      @IBOutlet weak var meetEndTimeImageView: UIImageView!
      @IBOutlet weak var meetEndTimeUnderlineImageView: UIImageView!
      @IBOutlet weak var meetEndDateView: UIView!
      @IBOutlet weak var meetEndsAtLabel: UILabel!
      @IBOutlet weak var meetEndDateButton: UIButton!
      @IBOutlet weak var meetEndDateImageView: UIImageView!
      @IBOutlet weak var meetEndDateLabel: UILabel!
      @IBOutlet weak var meetEndDateUnderlineImageView: UIImageView!

      @IBOutlet weak var discloseTrailCustomTextField: UITextField!
      @IBOutlet weak var startTrackingLabel: UILabel!
      @IBOutlet weak var startTrackingTimeLabel: UILabel!
      @IBOutlet weak var discloseTrailImageView: UIImageView!
      @IBOutlet weak var discloseTrailUnderline: UIImageView!
      @IBOutlet weak var discloseTrailDropdownImageView: UIImageView!
      @IBOutlet weak var discloseTrailButton: UIButton!
      @IBOutlet weak var discloseTrailPicker: UIPickerView!

      @IBOutlet weak var discloseTrailTimeCustomTextField: UITextField!
      @IBOutlet weak var discloseTrailTimeView: UIView!
      @IBOutlet weak var discloseTrailUnderlineImageView: UIImageView!
      @IBOutlet weak var discloseTraiTimelDropdownImageVeiw: UIImageView!
      @IBOutlet weak var discloseTrailTimeButton: UIButton!
      @IBOutlet weak var discloseTrailTimePickerView: UIPickerView!

      @IBOutlet weak var selectModeOfTransport: UILabel!
      

      @IBOutlet weak var mode: UILabel!
      @IBOutlet weak var modeTableView: UITableView!

      @IBOutlet weak var selectModeOfTransportbtn: UIButton!

      @IBOutlet weak var saveMeetButton: UIButton!

      var meetLatitude = 0.0
      var meetLongitude = 0.0
      var selectedText:String!

      var modeOfTransportArray = ["Walking", "Driving"]
      var englishmodeArray: [String] = []
      var selectmodeArray: Int = 1

      var contactsTobeAdded: [ContactObj] = []
      var mainContactObj: [AnyObject] = []
      var discloseTrailData: [String] = []
      var discloseTrailTimeData: [String] = []
      var selectedPlaceMark: MKPlacemark?
      var discloseTrailSelectedOption = "Before the meet"
      var selectedTrailOptionIndex = 0
      var selectedTrailTimeIndex = 0
      var discloseTrailEnabled = true
       var trackingOptionSelectedText:String!
      var  selectedTimeText = " 5"

      var validationController = ValidationController()
      var meetContacts = SearchContactsVC()
      let appSharedPrefernce = AppSharedPreference()
      let networkStatus = NetworkStatus()
      var appService = AppService.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      let trackingOptionsDropdown: DropDown = DropDown()
      let trackingTimeOptionsDropdown: DropDown = DropDown()
      let modeofTransportDropdown = DropDown()

      var meetmeDetailObj: MeetMeLogbookVariables?
      var reloadMeetDetail: ReloadProtocol?
      var participantFisheyeIds = [String]()
      var minutesForTrail = "5"
      var titleValid = true
      var locationValid = true
      var timeStartValid = true
      var timeEndValid = true
      var dateStartValid = true
      var dateEndValid = true

      var trackStatusValid = true
      var participantsValid = true
      var idtoken = ""
      var startDateTime = ""
      var endDateTime = ""
      var startDateTimeUTC: Date?
      var endDateTimeUTC: Date?

      var placeHolderOne = ""
      var placeHolderTwo = ""
      var validStartDate = ""
      var inValidStartDate = ""
      var validStartTime  = ""
      var invalidStartTime = ""
      var validEndDate = ""
      var inValidEndDate = ""
      var validEndTime = ""
      var inValidEndTime = ""

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            initializeAllComponents()
            setData()
            discloseTrailPicker.delegate = self
            discloseTrailPicker.dataSource = self
            discloseTrailTimePickerView.delegate = self
            discloseTrailTimePickerView.dataSource = self
            self.englishmodeArray = self.modeOfTransportArray
            setLocalizationText()
             NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
            initializeMode()
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.EditMeetVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
      }

      func initializeAllComponents() {
           
                           self.discloseTrailCustomTextField.text = MeetMeTrackingOptions.trackingOption[0].optionTranslatedName
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.discloseTrailPicker.backgroundColor = UIColor.duckEggBlue()
            self.discloseTrailTimePickerView.backgroundColor = UIColor.duckEggBlue()
            
            self.discloseTrailCustomTextField.text = discloseTrailSelectedOption
            //  self.editMeetScrollView.delegate = self
            self.discloseTrailTimeCustomTextField.text = "5"+" "+self.signupmsg.minutes
            discloseTrailCustomTextField.isUserInteractionEnabled = false
            discloseTrailTimeCustomTextField.isUserInteractionEnabled = false
            meetLocationCustomTextField.isUserInteractionEnabled = false
            self.titleCustomTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.meetLocationCustomTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.titleCustomTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.meetLocationCustomTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            discloseTrailData = MeetMeTrackingOptions.trackingOption.map { $0.optionTranslatedName }
            if MeetMeTrackingOptions.trackingOption.count > 0 {
                  MeetMeTrackingOptions.trackingOption[0].trackingTime.forEach {vars in
                        self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                  }
            }
            trackingOptionsDropdown.anchorView = discloseTrailUnderline
            trackingOptionsDropdown.dataSource = discloseTrailData

            self.trackingTimeOptionsDropdown.anchorView = self.discloseTrailUnderlineImageView
            self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
            self.trackingTimeOptionsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                  self.discloseTrailTimeCustomTextField.text = self.discloseTrailTimeData[index]
                //  self.selectedTimeText = item
                  
                 
                  self.selectedTrailTimeIndex = index
                  self.minutesForTrail =  String(MeetMeTrackingOptions.trackingOption[self.getSelectedOption()].trackingTime[index] as! Int)
                  self.selectedTimeText = String(MeetMeTrackingOptions.trackingOption[self.getSelectedOption()].trackingTime[index] as! Int)
            }

            trackingOptionsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                 
                  self.discloseTrailCustomTextField.text = self.discloseTrailData[index]
                  
                  
                   let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionTranslatedName == item ) } )
                  if objArray != nil && objArray.count > 0
                  {
                        self.discloseTrailSelectedOption = self.discloseTrailData[0]
                        self.trackingOptionSelectedText = objArray[0].optionName
                       
                  }else{
                        self.discloseTrailSelectedOption = self.discloseTrailData[0]
                        self.trackingOptionSelectedText = self.appService.localtrackingOptionArray[0].optionTranslatedName
                  }
                  
                  if  MeetMeTrackingOptions.trackingOption[index].isTimeRequired! {
                        self.discloseTrailTimeView.isHidden = false
                  } else {
                        self.discloseTrailTimeView.isHidden = true
                  }
                  switch index {
                  case 0:
                        self.discloseTrailSelectedOption =  self.trackingOptionSelectedText
                        self.discloseTrailEnabled = true
                        self.discloseTrailTimeData = []
                        self.selectedTrailOptionIndex = 0
                        MeetMeTrackingOptions.trackingOption[0].trackingTime.forEach {vars in
                              self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                        }
                        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                        break
                  case 1:
                        self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                        self.discloseTrailEnabled = true
                        self.discloseTrailTimeData = []
                        self.selectedTrailOptionIndex = 1
                        MeetMeTrackingOptions.trackingOption[1].trackingTime.forEach {vars in
                              self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                        }
                        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                        break
                  case 2:
                        self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                        self.minutesForTrail =  "0"
                        self.discloseTrailEnabled = true
                        self.selectedTrailOptionIndex = 2
                        self.discloseTrailTimeData = []
                        MeetMeTrackingOptions.trackingOption[2].trackingTime.forEach {vars in
                              self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                        }
                        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                        break
                  case 3:
                        self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                        self.minutesForTrail =  "0"
                        self.discloseTrailEnabled = false
                        self.selectedTrailOptionIndex = 3
                        self.discloseTrailTimeData = []
                        MeetMeTrackingOptions.trackingOption[3].trackingTime.forEach {vars in
                              self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                        }
                        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                        break
                  default:
                        self.discloseTrailSelectedOption = self.discloseTrailData[index]
                        self.discloseTrailEnabled = true
                        self.selectedTrailOptionIndex = index
                        self.discloseTrailTimeData = []
                        MeetMeTrackingOptions.trackingOption[index].trackingTime.forEach {vars in
                              self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                        }
                        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                        break
                  }
            }
      }

      func initializeMode() {
            self.modeOfTransportArray = MeetMeTrackingOptions.trackingMode
            self.modeTableView.isHidden = true
            self.modeofTransportDropdown.anchorView = self.modeTableView
            self.modeofTransportDropdown.dataSource = self.modeOfTransportArray
            self.modeofTransportDropdown.width = 200
            self.modeofTransportDropdown.selectionAction = {
                [unowned self] (index: Int, item: String) in
                self.selectModeOfTransport.text = item
                self.selectmodeArray = index
                  let objArray = self.appService.localTransportModeArray.filter( { return ($0.translatedText == item ) } )
                   if objArray != nil && objArray.count > 0
                   {  self.selectedText = objArray[0].actualText}
                   else{
                         self.selectedText = objArray[0].actualText
                  }
                 
            }
            
      }

      func getSelectedOption() -> Int {
            guard let isSelected = MeetMeTrackingOptions.trackingOption.index(where: { $0.optionName == self.discloseTrailCustomTextField.text})
                  else {
                        return 0
            }
            return isSelected
      }
    
      func setData() {

            self.titleCustomTextField.text = self.meetmeDetailObj?.meetmeTitle
            self.meetLocationCustomTextField.text = self.meetmeDetailObj?.meetmeLocation
            self.meetStartDateLabel.text = validationController.convert_date_from_UTC_time(givenUTCdate: (self.meetmeDetailObj?.startDateTime)!)
            self.meetStartTimeLabel.text = validationController.convert_time_from_UTC_time(givenUTCtime: (self.meetmeDetailObj?.startDateTime)!)
            self.meetEndDateLabel.text = validationController.convert_date_from_UTC_time(givenUTCdate: (self.meetmeDetailObj?.endDateTime)!)
            self.meetEndTimeLabel.text = validationController.convert_time_from_UTC_time(givenUTCtime: (self.meetmeDetailObj?.endDateTime)!)
            if let lat = meetmeDetailObj?.latitude, let convertedLat = Double(lat) {
                  self.meetLatitude = convertedLat
            }
            if let long = meetmeDetailObj?.longitude, let convertedLong = Double(long) {
                  self.meetLongitude = convertedLong
            }
            let fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as! String
            for participant in (self.meetmeDetailObj?.participantList)! {
                  
                  if(fisheyeId == participant.fisheyeId) {
                      
                        let objArray = self.appService.localTransportModeArray.filter( {
                              return ($0.actualText == participant.trackingMode)
                        } )
                         if objArray != nil && objArray.count > 0
                         { self.selectModeOfTransport.text = objArray[0].translatedText}
                         else { self.selectModeOfTransport.text = appService.localTransportModeArray[0].translatedText}
                        
                        self.selectedText = participant.trackingMode
                        if  MeetMeTrackingOptions.trackingOption.count>0 {
                              if let trackingbj = MeetMeTrackingOptions.trackingOption.index(where: {$0.optionName == participant.trackingStatus}) {
                                  var fetchedTrackObj = MeetMeTrackingOptions.trackingOption[trackingbj]
                                let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionName == participant.trackingStatus ) } )
                                if participant.istrackingEnabled == false{
                                    self.discloseTrailCustomTextField.text = appService.localtrackingOptionArray[appService.localtrackingOptionArray.count - 1].optionTranslatedName
                                    self.discloseTrailSelectedOption = appService.localtrackingOptionArray[appService.localtrackingOptionArray.count - 1].optionName
                                    fetchedTrackObj = appService.localtrackingOptionArray[appService.localtrackingOptionArray.count - 1]
                                    self.discloseTrailEnabled = false
                                }else {
                                    self.discloseTrailEnabled = true
                                    if objArray != nil && objArray.count > 0
                                    {
                                        self.discloseTrailCustomTextField.text = objArray[0].optionTranslatedName
                                        self.discloseTrailSelectedOption = participant.trackingStatus ?? self.discloseTrailSelectedOption
                                        
                                    }else{
                                        
                                        self.discloseTrailCustomTextField.text = appService.localtrackingOptionArray[0].optionTranslatedName
                                        self.discloseTrailSelectedOption = appService.localtrackingOptionArray[0].optionName
                                        
                                    }
                                }
                                
                                    if fetchedTrackObj.isTimeRequired! {
                                          
                                          self.discloseTrailTimeView.isHidden = false
                                          
                                        //  self.discloseTrailCustomTextField.text = participant.trackingStatus
                                          
                                          self.discloseTrailTimeCustomTextField.text = participant.trackingTime! + " "+self.signupmsg.minutes
                                          let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.trackingTime .contains(participant.trackingTime ) ) } )
                                          if objArray != nil && objArray.count > 0
                                          {
                                                self.discloseTrailTimeCustomTextField.text = participant.trackingTime! + " "+self.signupmsg.minutes
                                               self.selectedTimeText   = participant.trackingTime!
                                          }else{
                                                  self.discloseTrailTimeCustomTextField.text = participant.trackingTime! + " "+self.signupmsg.minutes
                                                self.selectedTimeText   = participant.trackingTime!
                                          }
                                    } else {
                                          self.discloseTrailTimeView.isHidden = true
//                                          self.discloseTrailCustomTextField.text = objArray[0].optionTranslatedName
                                            selectedTimeText =  "0"

                                          
                                    }
                              } else {
                                    self.discloseTrailTimeView.isHidden = true
                                    
                                    
                              }
                        }
                        break
                  }
            }
      }

      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return contactsTobeAdded.count
      }

      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            navigateToContactsScreen()
      }

      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateMeetParticipantCollectionViewCell", for: indexPath) as! CreateMeetParticipantCollectionViewCell
            let contactObj = contactsTobeAdded[indexPath.row]
            cell.participantNameLabel.text = validationController.getFirstChar(givenString: contactObj.name!)
            cell.participantImageView?.sd_setImage(with: URL(string: contactObj.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            cell.participantImageView.layer.cornerRadius =   cell.participantImageView.frame.size.height / 2
            cell.participantImageView.layer.masksToBounds = true
            return cell
      }

      func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
      }

      func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if pickerView == discloseTrailPicker {
                  return self.discloseTrailData.count
            } else {
                  return discloseTrailTimeData.count
            }
      }

      func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if pickerView == discloseTrailPicker {
                  return discloseTrailData[row]
            } else {
                  return discloseTrailTimeData[row]
            }
      }

      func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if pickerView == discloseTrailPicker {
                  editMeetScrollView.isScrollEnabled = true
                  discloseTrailPicker.isHidden = true
                  discloseTrailCustomTextField.text = discloseTrailData[row]
                  if  discloseTrailCustomTextField.text != "Never" &&  discloseTrailCustomTextField.text != "At the start"{
                        discloseTrailTimeView.isHidden = false
                  } else {
                        discloseTrailTimeView.isHidden = true
                  }
                  switch discloseTrailCustomTextField.text {
                  case "Before the Meet"?:
                        discloseTrailSelectedOption =  "Before"
                        discloseTrailEnabled = true
                        discloseTrailTimeView.isHidden = false
                        break
                  case "After the Meet"?:
                        discloseTrailSelectedOption = "After"
                        discloseTrailEnabled = true
                        discloseTrailTimeView.isHidden = false
                        break
                  case "At the start"?:
                        discloseTrailSelectedOption = "At the start"
                        minutesForTrail =  "0"
                        selectedTimeText = "0"
                        discloseTrailEnabled = true
                        discloseTrailTimeView.isHidden = true
                        break
                  case "Never"?:
                        discloseTrailSelectedOption = "Never"
                        minutesForTrail =  "0"
                        selectedTimeText = "0"
                        discloseTrailEnabled = false
                        discloseTrailTimeView.isHidden = true
                        break
                  default:
                        break
                  }
            } else {
                  editMeetScrollView.isScrollEnabled = true
                  discloseTrailTimeCustomTextField.text = discloseTrailTimeData[row]
                  discloseTrailTimePickerView.isHidden = true
                  switch discloseTrailTimeCustomTextField.text {
                  case "5 minutes"?:
                        minutesForTrail =  "5"
                        selectedTimeText = "5"
                        break
                  case "10 minutes"?:
                        minutesForTrail = "10"
                        selectedTimeText = "10"
                        break
                  case "15 minutes"?:
                        minutesForTrail = "15"
                        selectedTimeText = "15"
                        break
                  default:
                        break
                  }
            }
      }

      @IBAction func modeOfTransportButtonTappedbtn(_ sender: Any) {
            if  modeofTransportDropdown.isHidden {
                  self.modeofTransportDropdown.show()
            } else {
                  self.modeofTransportDropdown.hide()
            }
      }

      @IBAction func onMapButtonTapped(_ sender: Any) {
            let mapVC = MapViewController.instantiateFromStoryboardWithIdentifier(identifier: "MapViewController")
            mapVC.addressSearchDelegate = self as? AddressSearchNew
            if  self.meetLatitude != 0.0 &&  self.meetLongitude != 0.0 {
                  mapVC.originalLong = self.meetLongitude
                  mapVC.originalLat = self.meetLatitude
            }
            let navController = UINavigationController(rootViewController: mapVC)
            self.present(navController, animated: true, completion: nil)
      }
    
      //  Start tracking time
      @IBAction func onTimeButtonTapped(_ sender: Any) {
            DatePickerDialog().show(self.appService.selectStratTime, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time) {
                  (date) -> Void in
                  if let dt = date {
                        let formatterForEndtime = DateFormatter()
                        formatterForEndtime.dateFormat = "HH:mm"
                        self.meetStartTimeLabel.text = formatterForEndtime.string(from: dt)
                  }
            }
      }

      //  End tracking time
      @IBAction func onEndTimeButtonTapped(_ sender: Any) {
            DatePickerDialog().show(self.appService.selectEndTime, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .time) {
                  (date) -> Void in
                  if let dt = date {
                        let formatterForEndtime = DateFormatter()
                        formatterForEndtime.dateFormat = "HH:mm"
                        self.meetEndTimeLabel.text = formatterForEndtime.string(from: dt)
                  }
            }
      }

      //  Start tracking date
      @IBAction func onDateDropdownTapped(_ sender: Any) {
            DatePickerDialog().show(self.appService.selectStartDate, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date(), datePickerMode: .date) {
                  (date) -> Void in
                  if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd/MM/YYYY"
                        self.meetStartDateLabel.text = formatter.string(from: dt)
                  }
            }
      }

      //  End tracking date
      @IBAction func onEndDateDropdownTapped(_ sender: Any) {
            DatePickerDialog().show(self.appService.selectEndDate, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: Date(), datePickerMode: .date) {
                  (date) -> Void in
                  if let dt = date {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd/MM/YYYY"
                        self.meetEndDateLabel.text = formatter.string(from: dt)
                  }
            }
      }

      // For Start Tracking Picker view
      @IBAction func onDiscloseTrailTapped(_ sender: Any) {
            if trackingOptionsDropdown.isHidden {
                  trackingOptionsDropdown.show()
            } else {
                  trackingOptionsDropdown.hide()
            }
      }

      @IBAction func onSaveMeetButtonTapped(_ sender: Any) {
            onSaveButtonTapped(meetingTitle: titleCustomTextField.text!, startdate: meetStartDateLabel.text!, Enddate: meetEndDateLabel.text!, startTime: meetStartTimeLabel.text!, EndTime: meetEndTimeLabel.text!)
      }

      func onSaveButtonTapped(meetingTitle: String, startdate: String, Enddate: String, startTime: String, EndTime: String) {
            //For title validation
            if titleCustomTextField.text?.count == 0 {
                  self.titleCustomTextField.configOnError(withPlaceHolder: self.appService.invalidTitle)
                  placeHolderOne = titleCustomTextField.placeholder!
                  titleValid = false
            } else {
                  titleValid = true
                  self.titleCustomTextField.configOnErrorCleared(withPlaceHolder: "Title")
                  placeHolderTwo = titleCustomTextField.placeholder!
            }
            //For location validation
            if meetLocationCustomTextField.text?.count == 0 {
                  self.meetLocationCustomTextField.configOnError(withPlaceHolder: self.appService.invalidLocation)
                  locationValid = false
            } else {
                  locationValid = true
            }
            //For start Time validation
            if !validationController.isValidMeetTime(givenString: startTime) {
                  self.meetStartTimeLabel.textColor = UIColor.red

                  invalidStartTime = "invalid start time"
                  timeStartValid = false
            } else {
                  validStartTime  = "valid start time"
                  timeStartValid = true
            }
            //For end Time validation
            if !validationController.isValidMeetTime(givenString: EndTime) {
                  self.meetEndTimeLabel.textColor = UIColor.red
                  inValidEndTime = "invalid end time"
                  timeEndValid = false

            } else {
                  validEndTime = "valid end time"
                  timeEndValid = true
            }
            //For start date validation
            if !validationController.isValidMeetDate(givenString: startdate) {
                  self.meetStartDateLabel.textColor = UIColor.red

                  inValidStartDate = "invalid start date"
                  dateStartValid = false
            } else {
                  validStartDate = "valid start date"
                  dateStartValid = true
            }
            //For end date validation
            if !validationController.isValidMeetDate(givenString: Enddate) {
                  self.meetEndDateLabel.textColor = UIColor.red
                  validEndDate = "valid end date"
                  dateEndValid = false
            } else {
                  inValidEndDate = "invalid end date"
                  dateEndValid = true
            }
            let tempStartDateTime = "\(meetStartTimeLabel.text!)" + " " + "\(meetStartDateLabel.text!)"
            let tempEndDateTime = "\(meetEndTimeLabel.text!)" + " " + "\(meetEndDateLabel.text!)"

            startDateTime = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempStartDateTime) )
            endDateTime = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempEndDateTime) )

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
            dateFormatter.timeZone = NSTimeZone.local
            endDateTimeUTC = dateFormatter.date(from: endDateTime)!
            startDateTimeUTC = dateFormatter.date(from: startDateTime)!
            if !(startDateTimeUTC! < endDateTimeUTC!) {
                  self.dateEndValid = false
                  self.timeEndValid = false
                  self.meetEndDateLabel.textColor = UIColor.red
                  self.meetEndTimeLabel.textColor = UIColor.red
            } else {
                  self.dateEndValid = true
                  self.timeEndValid = true
                  self.meetEndDateLabel.textColor = UIColor.black
                  self.meetEndTimeLabel.textColor = UIColor.black
            }

            let startDateTimeChecker = validationController.getTimeStampFromStringDate(stringDate: validationController.getTodaysDateTimeValidation())*1000
            let startDateTimeTemp = validationController.getTimeStampFromStringDate(stringDate: startDateTime)*1000

            if startDateTimeTemp > startDateTimeChecker {
                  self.meetStartTimeLabel.textColor = UIColor.black
                  self.meetStartDateLabel.textColor = UIColor.black
                  timeStartValid = true
                  dateStartValid = true
            } else {
                  self.meetStartTimeLabel.textColor = UIColor.red
                  self.meetStartDateLabel.textColor = UIColor.red
                  timeStartValid = false
                  dateStartValid = false
            }

            if titleValid && locationValid && timeStartValid && timeEndValid && dateStartValid && dateEndValid {
                  guard networkStatus.isNetworkReachable()
                        else {
                              self.view.makeToast(appService.checkInternetConnectionMessage)
                        return
                  }
                
                let status = CLLocationManager.authorizationStatus()
                
                switch status {
                case .authorized, .authorizedWhenInUse, .authorizedAlways :
                    updateMeetApiCall()
                case .denied,.notDetermined:
                    self.stopLoader()
                    let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                    })
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                        alert.dismiss(animated: true, completion: nil)
                    })
                    present(alert, animated: true)
                    break
                default :
                    self.stopLoader()
                    break
                }
            } else {
            }
      }

      @IBAction func onDiscloseTrialTimeTapped(_ sender: Any) {
            if trackingTimeOptionsDropdown.isHidden {
                  trackingTimeOptionsDropdown.show()
            } else {
                  trackingTimeOptionsDropdown.hide()
            }
      }

      @IBAction func onAddParticipantButtonTapped(_ sender: Any) {
            navigateToContactsScreen()
      }

      @IBAction func onBackButtonClicked(_ sender: Any) {
            self.view.removeFromSuperview()

      }

      // Function to fill up the Pop up details
      func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, status: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.appSharedPrefernce.setAppSharedPreferences(key: "closestatus", value: status)
      }
      // End of Function to fill up the Pop up details

      //Function to call PopUp
      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
            nextVC.redirectProtocol = self as? RedirectToPrivacyControl
            self.present(nextVC, animated: true, completion: nil)
      }

      func dualActionPopUp(heading: String, subheading: String, action: String, method: String, module: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)

      }

      func dualActionPopUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "AllPopupDualActionVC") as! AllPopupDualActionVC
            self.present(nextVC, animated: true, completion: nil)
      }

      // End of Function to call PopUp

      func navigateToContactsScreen() {
            let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
            self.meetContacts = storyboard.instantiateViewController(withIdentifier: "SearchContactsVC") as! SearchContactsVC
            self.meetContacts.view.frame = self.editMeetCompleteView.bounds
        self.addChild(self.meetContacts)
        self.meetContacts.didMove(toParent: self)
            self.meetContacts.view.tag = 0
            self.meetContacts.view.frame.origin.y = -self.view.frame.size.height
            self.meetContacts.view.frame.origin.y = self.editMeetCompleteView.bounds.origin.y
            self.meetContacts.view.frame.origin.x = -self.view.frame.size.width
            self.meetContacts.view.frame.origin.x = self.editMeetCompleteView.bounds.origin.x
            self.editMeetCompleteView.addSubview(self.meetContacts.view)
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
        ScreenLoader.shared.stopLoader()
      }

    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.titleCustomTextField.placeholder = self.appService.title
                  self.meetLocationCustomTextField.placeholder =  self.appService.location
                  self.tapToLocateLabel.text = self.appService.locateMap
                  self.meetStartsAtLabel.text =  self.appService.startAt
                  self.meetEndsAtLabel.text =  self.appService.endsAt

                  self.startTrackingLabel.text =  self.appService.startTracking
                  self.startTrackingTimeLabel.text =  self.appService.time
                  self.editMeetTitleLabel.text = self.appService.editmeetmee
                  self.mode.text = self.appService.mode
            }
      }

      // function to validate textfields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  titleCustomTextField:
                  if (titleCustomTextField.text!.characters.count > 19) {
                        titleCustomTextField.deleteBackward()
                  }
                  self.titleUnderlineImageView.image = UIImage(named: "BlueUnderLine")

                  if titleCustomTextField.text?.count == 0 {
                        self.titleCustomTextField.configOnError(withPlaceHolder: self.appService.invalidTitle)
                        titleValid = false
                  } else {
                        titleValid = true
                        self.titleCustomTextField.configOnErrorCleared(withPlaceHolder: self.appService.title)
                  }

            case meetLocationCustomTextField:

                  self.locationUnderlineImageView.image = UIImage(named: "BlueUnderLine")

                  if meetLocationCustomTextField.text?.count == 0 {
                        self.meetLocationCustomTextField.configOnError(withPlaceHolder: self.appService.invalidLocation)
                        locationValid = false
                  } else {
                        locationValid = true

                  }
            default:
                  break
            }
      }

      //End of Function to Handle Hide show of the underline
    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {
            case  titleCustomTextField:
                  self.titleUnderlineImageView.image = UIImage(named: "BlackUnderLine")

            case meetLocationCustomTextField:
                  self.locationUnderlineImageView.image = UIImage(named: "BlackUnderLine")
                  self.meetLocationCustomTextField.configOnErrorCleared(withPlaceHolder: self.appService.location)
            default:
                  break
            }
      }
}

extension EditMeetVC {
      func updateMeetApiCall() {
            var trackTime = 0
            if MeetMeTrackingOptions.trackingOption[self.selectedTrailOptionIndex].trackingTime.count>0 {
                  trackTime = MeetMeTrackingOptions.trackingOption[self.selectedTrailOptionIndex].trackingTime[self.selectedTrailTimeIndex] as! Int
            } else {
                  trackTime = 0
            }
            let meetmeId = (self.meetmeDetailObj?.meetmeId!)!
            let parameters =
                  [
                        "istrackingEnabled": self.discloseTrailEnabled,
                        "trackingStatus": self.discloseTrailSelectedOption,
                        "trackingTime": selectedTimeText ,
                        "trackingMode": selectedText,
                        "meetme": [
                              "meetmeId": meetmeId,
                              "title": titleCustomTextField.text!,
                              "titleSearchKey": titleCustomTextField.text!.lowercased(),
                              "locationName": meetLocationCustomTextField.text!,
                              "locationNameSearchKey": meetLocationCustomTextField.text!.lowercased(),
                              "latitude": String(self.meetLatitude),
                              "longitude": String(self.meetLongitude),
                              "startDateTime": validationController.getTimeStampFromStringDate(stringDate: startDateTime)*1000,
                              "endDateTime": validationController.getTimeStampFromStringDate(stringDate: endDateTime)*1000
                        ]
                    ] as [String: Any]
            let awsURL = AppConfig.updateMeetUrl(meetMeId: meetmeId)
            guard let URL = URL(string: awsURL) else {

                  return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }
            self.startLoader()
            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, response: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        let response = data["statusCode"] as? String ?? ""
                        if response == "200"{
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Edit MeetMe", action: "Save MeetMe Button clicked", label: "User updates MeetMe successfully", value: 0)
                              DispatchQueue.main.async {
                            self.view.makeToast(self.appService.updateMeetSuccessString)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                                    self.view.hideToast()
                                    self.reloadMeetDetail?.reloadData(toReload: true)
                                    self.view.removeFromSuperview()
                              })
                        } else {
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Edit MeetMe", action: "Save MeetMe Button clicked", label: "User's attempt to update MeetMe is unsuccessful", value: 0)
                              DispatchQueue.main.async {
                                self.view.makeToast(self.appService.updateMeetFailureString)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                                    self.view.hideToast()
                              })
                        }
                  } else {
                        DispatchQueue.main.async {
                              self.view.makeToast(self.appService.updateMeetFailureString)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                              self.view.hideToast()
                        })
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Edit MeetMe", action: "Save MeetMe Button clicked", label: "User's attempt to update MeetMe is unsuccessful", value: 0)
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }
}

extension EditMeetVC: AddressSearchNew {

      func addrssObjectFrom(place: MKPlacemark?) {

            if let placemark = place {
                  meetLocationCustomTextField.text = ""
                  selectedPlaceMark = placemark
                  if let name = placemark.name {
                        meetLocationCustomTextField.text?.append(name)
                  }

                  if let loc = placemark.locality {
                        meetLocationCustomTextField.text?.append(", \(loc)")
                  }
                  if let state = placemark.administrativeArea {
                        meetLocationCustomTextField.text?.append(", \(state)")
                  }
                  if let country = placemark.country {
                        meetLocationCustomTextField.text?.append(", \(country)")
                  }
                  if let postalCode = placemark.postalCode {
                        meetLocationCustomTextField.text?.append(" - \(postalCode)")
                  }
                  if let latitude = placemark.location?.coordinate.latitude {
                        self.meetLatitude = latitude
                  }

                  if let longitude = placemark.location?.coordinate.longitude {
                        self.meetLongitude = longitude
                  }
                  self.meetLocationCustomTextField.configOnErrorCleared(withPlaceHolder: self.appService.location)
                  let newPosition = meetLocationCustomTextField.beginningOfDocument
                  meetLocationCustomTextField.selectedTextRange = meetLocationCustomTextField.textRange(from: newPosition, to: newPosition)
            } else {
                  selectedPlaceMark = nil
            }
      }
}
