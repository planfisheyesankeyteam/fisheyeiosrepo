//
//  MeetMeBaseViewController.swift
//  fisheye
//
//  Created by Sankey Solutions on 09/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class MeetMeBaseViewController: UIViewController {

    @IBOutlet weak var meetmeBaseControllerView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var baseContainerView: UIView!
    @IBOutlet weak var addButtonImageView: UIImageView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var dashboardIconImageView: UIImageView!
    @IBOutlet weak var dashboardIconButton: UIButton!

    var selectedContacts: [ContactObj] = []
    var createMeetVc = CreateMeetVC()
    var isFromNotification: Bool = false
    var notificationMeetMeId = ""
    var isFromCapsuleChatNotification: Bool = false
    var isCapsuleLocked: Bool = false
    var meetTitle: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.meetmeBaseControllerView.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        roundCorners(myview: mainContainerView)
        loadlogbook()
        NotificationCenter.default.addObserver(forName: REFRESH_MEET_NOTIFICATION, object: nil, queue: nil) { (_) in
            DispatchQueue.main.async {
                self.addButton.isHidden = false
                self.addButtonImageView.isHidden = false
            }
        }
        NotificationCenter.default.addObserver(forName: REFRESH_MEET_CONTACT, object: nil, queue: nil) { (notification) in

            if let contacts = notification.userInfo?["contacts"] as? [ContactObj] {
               self.selectedContacts = contacts
            }

            self.loadCreateView(contactsToSend: self.selectedContacts)
        }
        self.hideCreateMeetBtn()
    }

    func hideCreateMeetBtn(){
        NotificationCenter.default.addObserver(forName: HIDE_CREATE_MEET_BTN, object: nil, queue: nil) { (_) in
           self.addButton.isHidden = true
           self.addButtonImageView.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onAddButtonTapped(_ sender: Any) {
        let nilContacts: [ContactObj] = []
        loadCreateView(contactsToSend: nilContacts)
    }

    @IBAction func onDashboardButtonPressed(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
        self.present(nextVC, animated: true, completion: nil)
    }

    func roundCorners(myview: UIView) {
            myview.layer.shadowColor = UIColor.black.cgColor
            myview.layer.shadowOffset = CGSize(width: 3, height: 3)
            myview.layer.shadowOpacity = 0.7
            myview.layer.shadowRadius = 10.0
            self.baseContainerView.layer.masksToBounds = true
            self.baseContainerView.layer.cornerRadius = 8
    }

    func loadlogbook() {
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MeetMeLogBook") as! MeetMeLogBook
        vc.isFromNotification = self.isFromNotification
        vc.notificationMeetMeId = self.notificationMeetMeId
        vc.isFromCapsuleChatNotification = self.isFromCapsuleChatNotification
        vc.isCapsuleLocked = self.isCapsuleLocked
        vc.meetTitle = self.meetTitle
        vc.view.frame = self.baseContainerView.bounds
        self.addChild(vc)

        vc.view.frame.origin.y = -self.view.frame.size.height
        self.baseContainerView.addSubview(vc.view)
        vc.view.frame.origin.y = self.baseContainerView.bounds.origin.y

         addButtonImageView.isHidden = false
         self.isFromNotification = false
         self.notificationMeetMeId = ""
    }

    func loadCreateView(contactsToSend: [ContactObj]) {
        NotificationCenter.default.post(name: STOP_LOCATION_UPDATE, object: nil)
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        createMeetVc = storyboard.instantiateViewController(withIdentifier: "CreateMeetVC") as! CreateMeetVC
        createMeetVc.view.frame = self.baseContainerView.bounds
        self.addChild(createMeetVc)
        createMeetVc.didMove(toParent: self)
        createMeetVc.view.frame.origin.y = -self.view.frame.size.height
        self.baseContainerView.addSubview(createMeetVc.view)
        createMeetVc.view.frame.origin.y = self.baseContainerView.bounds.origin.y

        createMeetVc.contactsTobeAdded = contactsToSend
        addButton.isHidden = true
        addButtonImageView.isHidden = true

    }

}
