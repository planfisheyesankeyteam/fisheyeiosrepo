//
//  MeetMeLogBook.swift
//  fisheye
//
//  Created by Sankey Solution on 12/7/17.
//  Copyright © 2017 Rushikesh. All rights reserved.
//
import UIKit
import Foundation
import MapKit
import Toast_Swift

class MeetMeLogBook: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MKMapViewDelegate {

    //Outlet Variables

    @IBOutlet var OuterSafeView: UIView!
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var meetMeLogbookTableView: UITableView!
    @IBOutlet weak var meetMeLogbookView: UIView!
    @IBOutlet weak var meetMeLogbookSearchBarView: UIView!
    @IBOutlet weak var meetMeLogbookSearchbar: UISearchBar!
    @IBOutlet weak var meetMeLogbookAppBarView: UIView!
    @IBOutlet weak var appBarBackButtonImageView: UIImageView!
    @IBOutlet weak var appBarBackButton: UIButton!
    @IBOutlet weak var appBarMeetLogbookLabel: UILabel!
    @IBOutlet weak var appBarMeetmeTitleLabel: UILabel!
    @IBOutlet weak var appBarSearchImageView: UIImageView!
    @IBOutlet weak var appBarSearchButton: UIButton!

    @IBOutlet weak var createMeetButton: UIButton!
    @IBOutlet weak var noMeetingsImageView: UIImageView!
    @IBOutlet weak var noMeetingsLabel: UILabel!
    @IBOutlet weak var logbookIcon: UIImageView!
    @IBOutlet weak var logbookButton: UIButton!

    @IBOutlet weak var allMeetingsView: UIView!
    @IBOutlet weak var allMeetingsLabel: UILabel!
    @IBOutlet weak var allMeetingsButton: UIButton!
    @IBOutlet weak var myMeetingsView: UIView!
    @IBOutlet weak var myMeetingsLabel: UILabel!
    @IBOutlet weak var myMeetingsButton: UIButton!

    @IBOutlet weak var pastLabel: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var activeNotificationView: UIView!
    @IBOutlet weak var activeNotificationLabel: UILabel!
    @IBOutlet weak var inactiveNotificationIcon: UIImageView!
    @IBOutlet weak var noMeetingView: UIView!

    let locationManager = CLLocationManager()
    let meetMeLogbooktableView = MeetMeLogBookTableViewCell()

    //String Variables
    var idtoken = ""
    var fisheyeId = ""
    var expandedRows = Set<Int>()
    var count = 0
    var timeInterval: Double = 40
    var selectedMeetMeId = ""
    var selectedMeetMeIndex = 0

    //Boolean variables
    var hasSelectedAllMeetings = true
    var isUpcoming = false
    var isExpanded = false

    //Class Variables
    var meetList: [MeetMeLogbookVariables] = []
    var meetObjToBeDeleted = MeetMeLogbookVariables()
    var createMeetVC: CreateMeetVC!
    let appSharedPrefernce = AppSharedPreference()
    let networkStatus = NetworkStatus()
    var appService = AppService.shared
    var signmsg = SignupToastMsgHeadingSubheadingLabels.shared
    var pulseMsg = PulseToastMsgHeadingSubheadingLabels.shared
    let validationController = ValidationController()
    var timer: Timer?
    var tabDeselectedColor = UIColor()
    var tabSelectedColor = UIColor()
    var notificationMeetMeId = ""
    var isFromNotification: Bool = false
    var meetMeBaseViewCController = MeetMeBaseViewController()
    var isFromCapsuleChatNotification: Bool = false
    var isCapsuleLocked: Bool = false
    var meetTitle = ""
    var selectedMeetIdToSeeCapsule: String = ""
    var selectedMeetIndexToSeeCapsule: Int = -1
    var isEventFlag = false
    var isEventFlagDetail = false
    var isHostDetail = false
    var isHostUser = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.appService = AppService.shared
        self.initialize()
        self.setLocalizationText()
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
        GetLocationGlobally.shared.initialize()
        self.getMeetLogbook(upcomingFlagApiCall: "false", searchKeyApiCall: "")
        self.getNotificationCount()
        if self.isFromNotification && self.notificationMeetMeId != ""{
            let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
            let meetMeBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "MeetMeDetailVC") as! MeetMeDetailVC
            meetMeBaseViewcontroller.selectedMeetmeId = notificationMeetMeId

            meetMeBaseViewcontroller.isUpcoming = self.isUpcoming
            meetMeBaseViewcontroller.hasOpenedContacts = false
            meetMeBaseViewcontroller.view.frame = self.view.bounds
            self.addChild(meetMeBaseViewcontroller)
            self.view.addSubview(meetMeBaseViewcontroller.view)

            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {                  meetMeBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
        }else if self.isFromCapsuleChatNotification && self.notificationMeetMeId != ""{
            
            if self.isCapsuleLocked{
                let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
                let nextVC = storyBoard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController") as! MasterKeyPopUpViewController
                nextVC.isFromCapsuleChatNotification = true
                nextVC.notificationMeetMeId = self.notificationMeetMeId
                nextVC.isCapsuleLocked = self.isCapsuleLocked
                nextVC.reloadProtocol = self as RedirectedToMeetmeLogbookVC
                DispatchQueue.main.async{
                    self.present(nextVC, animated: true, completion: nil)
                }
            }else{
                DispatchQueue.main.async{
                    let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
                    let meetmeCapsuleVC = storyboard.instantiateViewController(withIdentifier: "MeetMeCapsuleVC") as! MeetMeCapsuleVC
                    meetmeCapsuleVC.meetmeTitle = self.meetTitle
                    meetmeCapsuleVC.selectedMeetmeId  = self.notificationMeetMeId
                    meetmeCapsuleVC.isFromDetails = false
                    meetmeCapsuleVC.view.frame = self.view.bounds
                    self.addChild(meetmeCapsuleVC)
                    self.view.addSubview(meetmeCapsuleVC.view)
                    UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        meetmeCapsuleVC.view.frame.origin.y = self.view.bounds.origin.y
                    }, completion: nil)
                }
            }
        }
      self.isFromNotification = false
        self.activeNotificationLabel.text = String(self.appService.noOfUnreadMeetNotifications)

      NotificationCenter.default.addObserver(self, selector: #selector(refreshMeetNotificationCount), name: REFRESH_MEET_NOTIFICATION_COUNT, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /******************* Function to be called while initializing view starts here *******************/
    func initialize() {
      // self.meetMeLogbookTableView.register(UINib(nibName: "MeetMeLogBookTableViewCell", bundle: nil), forCellReuseIdentifier: "MeetMeLogBookTableViewCell")
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
        meetMeLogbookSearchbar.delegate = self
        meetMeLogbookSearchbar.showsCancelButton = true
     
        meetMeLogbookTableView.delegate = self
        self.meetMeLogbookTableView.rowHeight = UITableView.automaticDimension
        createMeetButton.layer.cornerRadius = 4
        tabSelectedColor =  UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        tabDeselectedColor = UIColor.rebrandedPurple()
        timeInterval = MeetMeTrackingOptions.mapResetTimeInterval ?? 30.00

        self.meetMeLogBookShadowView()
        self.allMeetingsView.backgroundColor = tabSelectedColor
        self.allMeetingsLabel.textColor = tabDeselectedColor
        self.myMeetingsView.backgroundColor = tabDeselectedColor
        self.myMeetingsLabel.textColor = tabSelectedColor
        pastLabel.textColor = UIColor.rebrandedPurple()
        self.logbookIcon.image = UIImage(named: "ActiveLogbookIcon")
        //For Reloading Logbook from any page
        NotificationCenter.default.addObserver(forName: REFRESH_MEET_NOTIFICATION, object: nil, queue: nil) { (_) in
            self.meetList = []
            DispatchQueue.main.async {
                self.meetMeLogbookTableView.reloadData()
            }

            if self.isUpcoming && self.hasSelectedAllMeetings {
                self.getMeetLogbook(upcomingFlagApiCall: "true", searchKeyApiCall: "")
            } else if !self.isUpcoming && self.hasSelectedAllMeetings {
                self.getMeetLogbook(upcomingFlagApiCall: "false", searchKeyApiCall: "")
            } else if self.isUpcoming && !self.hasSelectedAllMeetings {
                self.getMyMeetings(upcomingFlagApiCall: "true", searchKeyApiCall: "")
            } else {
                self.getMyMeetings(upcomingFlagApiCall: "false", searchKeyApiCall: "")
            }
            self.getNotificationCount()
        }
        //For stopping location updates
        NotificationCenter.default.addObserver(forName: STOP_LOCATION_UPDATE, object: nil, queue: nil) { (_) in
            self.stopTimer()
        }

        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)

    }

    func meetMeLogBookShadowView() {
        self.meetMeLogbookView.layer.shadowColor = UIColor.black.cgColor
        self.meetMeLogbookView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.meetMeLogbookView.layer.shadowOpacity = 0.4
        self.meetMeLogbookView.layer.shadowRadius = 6.0
        self.meetMeLogbookView.layer.masksToBounds = false
    }
    /******************* Function to be called while initializing view ends here *******************/

    /******************* Function for mapview starts here *******************/

    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        // this is where visible maprect should be set
        mapView.showAnnotations(mapView.annotations, animated: false)
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is CustomAnnotation) {
            return nil
        }

        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }

        if let customAnno = annotation as? CustomAnnotation {
            if customAnno.annotationType == "Destination"{
                let pinImage = UIImage(named: "shakingHandsCopy")
                annotationView!.image = pinImage
                annotationView?.frame.size = CGSize(width: 33.0, height: 33.0)
            } else if customAnno.annotationType == "Participant" {
                if customAnno.participantName != nil {
                    let pinImage = UIImage(named: "participantLocation")
                    annotationView!.image = pinImage
                    annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                } else {
                    let pinImage = UIImage(named: "Image")
                    annotationView!.image = pinImage
                    annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                }
            } else if customAnno.annotationType == "ParticipantLoctionStart" {
                if customAnno.participantName != nil {
                    let pinImage = UIImage(named: "startPoint")
                    annotationView!.image = pinImage
                    annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                } else {
                    let pinImage = UIImage(named: "Image")
                    annotationView!.image = pinImage
                    annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                }
            } else {
                let pinImage = UIImage(named: "Image")
                annotationView!.image = pinImage
                annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
            }
        }

        return annotationView
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            if let temp = overlay.title {
                if temp == "isForDirections"{
                    renderer.strokeColor = UIColor.blue
                } else {
                    renderer.strokeColor = UIColor.acceptGreen()
                }
            }
            renderer.lineDashPattern = [5, 5]
            renderer.lineWidth = 2
            return renderer
        }

        return MKOverlayRenderer()
    }

    /******************* Function for mapview ends here *******************/

    /******************* Search related functions start here *******************/
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchKeyText = searchBar.text
        self.meetList = []
        self.noMeetingView.isHidden = true

        if isUpcoming && hasSelectedAllMeetings {
            self.getMeetLogbook(upcomingFlagApiCall: "true", searchKeyApiCall: searchKeyText!)
        } else if !isUpcoming && hasSelectedAllMeetings {
            self.getMeetLogbook(upcomingFlagApiCall: "false", searchKeyApiCall: searchKeyText!)
        } else if isUpcoming && !hasSelectedAllMeetings {
            self.getMyMeetings(upcomingFlagApiCall: "true", searchKeyApiCall: searchKeyText!)
        } else {
            self.getMyMeetings(upcomingFlagApiCall: "false", searchKeyApiCall: searchKeyText!)
        }
        self.meetMeLogbookSearchbar.endEditing(true)
        
         self.meetMeLogbookSearchbar.resignFirstResponder()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        
        self.meetMeLogbookAppBarView.isHidden = false
        self.meetMeLogbookSearchbar.endEditing(true)
        self.meetMeLogbookSearchBarView.isHidden = true
        self.meetList = []
        meetMeLogbookTableView.reloadData()
        if isUpcoming && hasSelectedAllMeetings {
            self.getMeetLogbook(upcomingFlagApiCall: "true", searchKeyApiCall: "")
        } else if !isUpcoming && hasSelectedAllMeetings {
            self.getMeetLogbook(upcomingFlagApiCall: "false", searchKeyApiCall: "")
        } else if isUpcoming && !hasSelectedAllMeetings {
            self.getMyMeetings(upcomingFlagApiCall: "true", searchKeyApiCall: "")
        } else {
            self.getMyMeetings(upcomingFlagApiCall: "false", searchKeyApiCall: "")
        }
        self.meetMeLogbookSearchbar.resignFirstResponder()
    }
    /******************* Search related functions end here *******************/

    /********************* Collectionview for participants in a single meet starts here *******************/

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let meetme =  self.meetList[collectionView.tag]
        let participants = meetme.participantListForCollection
        if participants.count>4 {
            return 5
        } else {
            let returnVariable = participants.count + 1
            return returnVariable
        }

    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let size = CGSize(width: 28, height: collectionView.bounds.size.height)
        return size
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParticipantsLogbookCollectionViewCell", for: indexPath) as! ParticipantsLogbookCollectionViewCell

        let meetme =  self.meetList[collectionView.tag]
        if meetme.participantListForCollection.count<=4 {
            if indexPath.row != meetme.participantListForCollection.count {
                let participant = meetme.participantListForCollection[indexPath.row]
                cell.participantNameLabel.text = participant.participantName
                cell.participantImageView?.sd_setImage(with: URL(string: participant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                cell.participantImageView.layer.cornerRadius = cell.participantImageView.frame.size.width / 2
                cell.participantImageView.clipsToBounds = true
                cell.participantImageView.layer.borderWidth = 1.5

                if participant.isAccepted! {
                    cell.participantImageView.layer.borderColor = UIColor.green.cgColor
                    cell.participantStatusImageView.isHidden = false
                } else if participant.isDeclined! {
                    cell.participantImageView.layer.borderColor = UIColor.red.cgColor
                    cell.participantStatusImageView.image =  UIImage(named: "DeclinedParticipant")
                    cell.participantStatusImageView.isHidden = false
                } else {
                    cell.participantImageView.layer.borderColor = UIColor.lightGray.cgColor
                    cell.participantStatusImageView.isHidden = true
                }
                cell.participantNameLabel.isHidden = false
            } else {
                cell.participantImageView.image = UIImage(named: "threeDotsOption")

                cell.participantNameLabel.isHidden = true
                cell.participantImageView.layer.borderColor = UIColor.clear.cgColor
                cell.participantStatusImageView.isHidden = true
            }
        } else {
            if indexPath.row != 4 {
                let participant = meetme.participantListForCollection[indexPath.row]
                cell.participantNameLabel.text = participant.participantName
                cell.participantImageView?.sd_setImage(with: URL(string: participant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                cell.participantImageView.layer.cornerRadius = cell.participantImageView.frame.size.width / 2
                cell.participantImageView.clipsToBounds = true
                cell.participantImageView.layer.borderWidth = 1.5

                if participant.isAccepted! {
                    cell.participantImageView.layer.borderColor = UIColor.green.cgColor
                } else if participant.isDeclined! {
                    cell.participantImageView.layer.borderColor = UIColor.red.cgColor
                    cell.participantStatusImageView.image =  UIImage(named: "DeclinedParticipant")
                } else {
                    cell.participantImageView.layer.borderColor = UIColor.lightGray.cgColor
                    cell.participantStatusImageView.isHidden = true
                }
                cell.participantNameLabel.isHidden = false
            } else {
                cell.participantImageView.image = UIImage(named: "threeDotsOption")
                cell.participantImageView.layer.borderColor = UIColor.clear.cgColor
                cell.participantNameLabel.isHidden = true
                cell.participantStatusImageView.isHidden = true
            }
        }

        return cell
    }
    /**********************  Collectionview for participants in a single meet ends here ************************/

    /********************* All Table view functions start here ********************/
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == meetMeLogbookTableView {
            return meetList.count
        } else {
            return 0
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let defaultCell = UITableViewCell()

        // Main logbook tableview starts here
        if tableView == meetMeLogbookTableView {

            let cell = Bundle.main.loadNibNamed("MeetMeLogBookTableViewCell", owner: self, options: nil)?.first as! MeetMeLogBookTableViewCell

            cell.meetMeLogBookMapView.delegate = self
            cell.participantCollectionView?.register(ParticipantsLogbookCollectionViewCell.self, forCellWithReuseIdentifier: "ParticipantsLogbookCollectionViewCell")
            cell.participantCollectionView?.register(UINib(nibName: "ParticipantsLogbookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ParticipantsLogbookCollectionViewCell")
            
            let meetTableObj =  self.meetList[indexPath.row]

            cell.meetTitleLabel.text = meetTableObj.meetmeTitle
            cell.meetAddressLabel.text = meetTableObj.meetmeLocation
            cell.unlockCapsuleLabel.text=self.appService.unlockCapsuleLabel
            cell.meetTimeLabel.text = validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.startDateTime!)
            let givenDate = validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!)
            let todaysDate = validationController.getTodaysDate()
            let differenceInDate = validationController.calculateDaysBetweenTwoDates(start: todaysDate, end: givenDate)
            switch (differenceInDate) {
            case 0:
                cell.meetDateLabel.text = self.appService.meetDateLabel
                break
            case 1:
                cell.meetDateLabel.text = self.appService.meetDateLabel2
                break
            default:
                cell.meetDateLabel.text = givenDate
                break
            }

            //For extended view
            if validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!) == validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.endDateTime!) {
                cell.extendedViewDateTime.text =
                "\(validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!) ) - \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.startDateTime!) ) \(self.pulseMsg.labelBetweenSDandED) \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.endDateTime!) )"
            } else {
                cell.extendedViewDateTime.text =
                "\(validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!) ) - \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.startDateTime!) ) \(self.pulseMsg.labelBetweenSDandED) \(validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.endDateTime!) ) - \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.endDateTime!) )"
            }
            //For destination on map
            let annotation = CustomAnnotation()
            let lat = (meetTableObj.latitude! as NSString).doubleValue
            let long = (meetTableObj.longitude! as NSString).doubleValue
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.title = "Destination"
            annotation.annotationType = "Destination"
            cell.meetMeLogBookMapView.addAnnotation(annotation)
            cell.meetMeLogBookMapView.showAnnotations(cell.meetMeLogBookMapView.annotations, animated: true)
            //cell.meetMeLogBookMapView.fitAllMarkers(shouldIncludeCurrentLocation: true)

            //Tap gestures for accept ,decline and details

            cell.acceptButton.tag = indexPath.row
            cell.declineButton.tag = indexPath.row
            cell.mainNotificationView.tag = indexPath.row
            cell.meetMeLogBookMapView.tag = indexPath.row
            cell.participantCollectionView.tag = indexPath.row
            cell.unlockCapsuleButton.tag = indexPath.row
            let  gestureForMainNotification1 =  setTapGesturesForMeetmeLogbookCell(componentName: "closeCell")
            let gestureForMainNotification2 =  setTapGesturesForMeetmeLogbookCell(componentName: "openMeetmeDetail")
            gestureForMainNotification1.require(toFail: gestureForMainNotification2)
            cell.mainNotificationView.addGestureRecognizer(gestureForMainNotification1)
            cell.mainNotificationView.addGestureRecognizer(gestureForMainNotification2)

            let  gestureForMapView1 =  setTapGesturesForMeetmeLogbookCell(componentName: "closeCell")
            let gestureForMapView2 =  setTapGesturesForMeetmeLogbookCell(componentName: "openMeetmeDetail")
            gestureForMapView1.require(toFail: gestureForMapView2)
            cell.meetMeLogBookMapView.addGestureRecognizer(gestureForMapView1)
            cell.meetMeLogBookMapView.addGestureRecognizer(gestureForMapView2)

            let gestureForAcceptMeet =  setTapGesturesForMeetmeLogbookCell(componentName: "acceptMeet")
            let gestureForDeclineMeet =  setTapGesturesForMeetmeLogbookCell(componentName: "declineMeet")
            let gestureForCollectionView =  setTapGesturesForMeetmeLogbookCell(componentName: "openMeetmeDetailWithContacts")
            cell.participantCollectionView.addGestureRecognizer(gestureForCollectionView)

            // For accept, decline and delete status
            for l in 0..<meetTableObj.participantList.count {

                let participantObj = meetTableObj.participantList[l]
                if participantObj.isHost! {
                    cell.hostImageView?.sd_setImage(with: URL(string: participantObj.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                    cell.hostImageView.layer.cornerRadius = cell.hostImageView.frame.size.width / 2
                    cell.hostImageView.clipsToBounds = true
                }
                if participantObj.fisheyeId! == self.fisheyeId {

                    let gestureForChangeLock = changeLockstatusGesture(meetindex: indexPath.row, participantIndex: l, forLockStatusChange: true)
                    cell.capsuleLockButton.addGestureRecognizer(gestureForChangeLock)

                    let gestureForUnlockCapsule = changeLockstatusGesture(meetindex: indexPath.row, participantIndex: l, forLockStatusChange: false)
                    //fOR CAPSULE
                    if participantObj.isCapsuleLocked! {
                        cell.capsuleLockImageView.image = UIImage(named: "masterKeyLock")
                        cell.unlockCapsuleButton.addGestureRecognizer(gestureForUnlockCapsule)
                    } else {
                        cell.capsuleLockImageView.image = UIImage(named: "masterKeyUnlocked")
                        cell.unlockCapsuleButton.tag = indexPath.row
                        cell.unlockCapsuleButton.addTarget(self, action: #selector(openCapsule(sender:)), for: .allTouchEvents)

                    }

                    //For Capsule count
                    if let capsuleCount = participantObj.messageCount {
                        cell.capsuleNotificationNumber.text = String(capsuleCount)
                    }

                    //For tracking status
                    cell.earliestTrackingLabel.text = self.appService.earliestTrackingLabel + " :"
                    if participantObj.trackingStatus != nil && participantObj.istrackingEnabled! {
                        cell.earliestTrackingLabel.isHidden = false
                        cell.trackingStatus.text = participantObj.trackingStatus
                        let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionName == participantObj.trackingStatus ) } )
                        
                        if objArray != nil && objArray.count > 0{
                              
                              cell.trackingStatus.text = objArray[0].optionTranslatedName
                             
                        }else{
                              cell.trackingStatus.text = participantObj.trackingStatus
                        }

                        
                  
                        cell.trackingTimeLabel.text = participantObj.trackingTime! + " " + self.signmsg.minutes
                        
                        
                    } else {
                        cell.trackingStatus.text = ""
                        cell.trackingTimeLabel.text = ""
                    }
                    if isUpcoming {
                        //For meet interaction of delete,accept,decline
                        if participantObj.isHost! {
                            cell.isHoster = true
                            cell.declineImage.isHidden = true
                            cell.declineButton.isHidden = true
                            cell.acceptImage.isHidden = true
                            cell.meetDeleteIcon.isHidden = true

                            let gestureForDelete =  setTapGesturesForMeetmeLogbookCell(componentName: "deleteMeet")
                            cell.acceptButton.addGestureRecognizer(gestureForDelete)
                        } else if participantObj.isAccepted! && !participantObj.isDeclined! {
                            cell.inviteStatusView.backgroundColor = UIColor.acceptGreen()

                            cell.meetDeleteIcon.isHidden = true
                            cell.acceptImage.image = UIImage(named: "AcceptedFilled")
                            cell.declineImage.image = UIImage(named: "DeclinedHollow")
                            cell.acceptButton.addGestureRecognizer(gestureForAcceptMeet)
                            cell.declineButton.addGestureRecognizer(gestureForDeclineMeet)

                        } else if !participantObj.isAccepted! && participantObj.isDeclined! {

                            cell.inviteStatusView.backgroundColor = UIColor.rejectRed()

                            cell.meetDeleteIcon.isHidden = true
                            cell.declineImage.image = UIImage(named: "DeclinedFilled")
                            cell.acceptImage.image = UIImage(named: "AcceptedHollow")
                            cell.acceptButton.addGestureRecognizer(gestureForAcceptMeet)
                            cell.declineButton.addGestureRecognizer(gestureForDeclineMeet)

                        } else {

                            cell.inviteStatusView.backgroundColor = UIColor.lightPurple()
                            cell.meetDeleteIcon.isHidden = true
                            cell.declineImage.image = UIImage(named: "DeclinedHollow")
                            cell.acceptImage.image = UIImage(named: "AcceptedHollow")
                            cell.acceptButton.addGestureRecognizer(gestureForAcceptMeet)
                            cell.declineButton.addGestureRecognizer(gestureForDeclineMeet)
                        }
                    } else {
                        cell.declineImage.image = UIImage(named: "Image-3")
                        cell.acceptImage.image = UIImage(named: "Image-3")
                        cell.meetDeleteIcon.image = UIImage(named: "Image-3")
                        cell.acceptButton.isUserInteractionEnabled = false
                        cell.declineButton.isUserInteractionEnabled = false

                        if participantObj.isAccepted! && !participantObj.isHost! {
                            cell.inviteStatusView.backgroundColor = UIColor.acceptGreen()
                        } else if participantObj.isDeclined! && !participantObj.isHost! {
                            cell.inviteStatusView.backgroundColor = UIColor.rejectRed()
                        } else if !participantObj.isHost! {
                            cell.inviteStatusView.backgroundColor = UIColor.lightPurple()
                        }
                    }

                }
            }

            cell.isExpanded = self.expandedRows.contains(indexPath.row)

            cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            cell.participantCollectionView.tag = indexPath.row
            return cell
        }
        // Main logbook tableview ends here
        return defaultCell
    }
    
    @objc func refreshMeetNotificationCount(){
        DispatchQueue.main.async{
            self.activeNotificationLabel.text = String(self.appService.noOfUnreadMeetNotifications)
        }
    }
    
    /************** All Table view functions end here ****************/

    /*************** All button tap functionalities start here**************/
    @IBAction func onAppBarSearchButtonTapped(_ sender: Any) {
        self.meetMeLogbookAppBarView.isHidden = true
        self.meetMeLogbookSearchBarView.isHidden = false
        self.meetMeLogbookSearchbar.setShowsCancelButton(true, animated: true)
        self.meetMeLogbookSearchbar.becomeFirstResponder()
    }

    @IBAction func onAppBarBackButtonTapped(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
        self.present(nextVC, animated: true, completion: nil)
    }

    @IBAction func onLogbookButtonTapped(_ sender: Any) {
        isUpcoming = !isUpcoming
        if isUpcoming && hasSelectedAllMeetings {
            pastLabel.textColor = UIColor.unSelectedTextColor()
            self.logbookIcon.image = UIImage(named: "LogbookIcon-1")
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            stopTimer()
            appBarMeetLogbookLabel.isHidden = true
            appBarMeetmeTitleLabel.isHidden = false
            self.getMeetLogbook(upcomingFlagApiCall: "true", searchKeyApiCall: "")
        } else if !isUpcoming && hasSelectedAllMeetings {
            pastLabel.textColor = UIColor.rebrandedPurple()
            logbookIcon.image = UIImage(named: "ActiveLogbookIcon")
            appBarMeetLogbookLabel.isHidden = false
            appBarMeetmeTitleLabel.isHidden = true
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            stopTimer()
            self.getMeetLogbook(upcomingFlagApiCall: "false", searchKeyApiCall: "")
        } else if isUpcoming && !hasSelectedAllMeetings {
            self.logbookIcon.image = UIImage(named: "LogbookIcon-1")
             pastLabel.textColor = UIColor.unSelectedTextColor()
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            stopTimer()
            appBarMeetLogbookLabel.isHidden = true
            appBarMeetmeTitleLabel.isHidden = false
            self.getMyMeetings(upcomingFlagApiCall: "true", searchKeyApiCall: "")
        } else {

            logbookIcon.image = UIImage(named: "ActiveLogbookIcon")
             pastLabel.textColor = UIColor.rebrandedPurple()
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            stopTimer()

            appBarMeetLogbookLabel.isHidden = false
            appBarMeetmeTitleLabel.isHidden = true
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            stopTimer()
            self.getMyMeetings(upcomingFlagApiCall: "false", searchKeyApiCall: "")
        }

    }

    @IBAction func onNotificationButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        let meetmeNotificationVC = storyboard.instantiateViewController(withIdentifier: "MeetMeNotificationVC") as! MeetMeNotificationVC
        meetmeNotificationVC.view.frame = self.view.bounds
        self.addChild(meetmeNotificationVC)
        self.view.addSubview(meetmeNotificationVC.view)

        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            meetmeNotificationVC.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }

    @IBAction func onCreateMeetButtonHandler(_ sender: Any) {
        stopTimer()
        openCreateMeetVC()
    }

    @IBAction func onAllmeetingsButtonTapped(_ sender: Any) {

        self.allMeetingsView.backgroundColor = tabSelectedColor
        self.allMeetingsLabel.textColor = tabDeselectedColor
        self.myMeetingsView.backgroundColor = tabDeselectedColor
        self.myMeetingsLabel.textColor = tabSelectedColor
        hasSelectedAllMeetings = true

        if isUpcoming {
            stopTimer()
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            self.getMeetLogbook(upcomingFlagApiCall: "true", searchKeyApiCall: "")
        } else {
            stopTimer()
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            self.getMeetLogbook(upcomingFlagApiCall: "false", searchKeyApiCall: "")
        }
    }

    @IBAction func onMyMeetingsButtonTapped(_ sender: Any) {
        hasSelectedAllMeetings = false
        self.myMeetingsView.backgroundColor = tabSelectedColor
        self.myMeetingsLabel.textColor = tabDeselectedColor
        self.allMeetingsView.backgroundColor = tabDeselectedColor
        self.allMeetingsLabel.textColor = tabSelectedColor
        if isUpcoming {
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            stopTimer()
            self.getMyMeetings(upcomingFlagApiCall: "true", searchKeyApiCall: "")
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Create MeetMe", action: "Add participant Button clicked", label: "User is redirected to Add Participant Page", value: 0)
        } else {
            stopTimer()
            self.meetList = []
            self.meetMeLogbookTableView.reloadData()
            self.getMyMeetings(upcomingFlagApiCall: "false", searchKeyApiCall: "")
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        var userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
        if(self.meetList[indexPath.row].hostId == userId){
            return true
        }
        return false
    }
    
    
    private func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
           var userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
            if(self.meetList[indexPath.row].hostId == userId){
                self.deleteMeetOnLeft(index : indexPath.row)
                self.meetObjToBeDeleted = self.meetList[indexPath.row]
                UIView.animate(withDuration: TimeInterval(2), animations: {
                    self.view.layoutIfNeeded()
                })
            }
        }
//            var userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
////           self.meetObjToBeDeleted=self.meetList[indexPath.row]
//            if(self.meetList[indexPath.row].hostId == userId){
//                if editingStyle == .delete {
//                    //            self.confirmPulseDeletion()
//                    self.meetObjToBeDeleted = self.meetList[indexPath.row]
//                    self.meetList=self.meetList.filter {$0.meetmeId !=  self.meetObjToBeDeleted.meetmeId}
//                    self.meetMeLogbookTableView.reloadData()
//                    //            if(self.logsArray.count == 0){
//                    //                self.showThisWhenNothingForLogbook()
//                    //            }
//                    UIView.animate(withDuration: TimeInterval(2), animations: {
//                        self.view.layoutIfNeeded()
//                    })
//                }
//            }
//        }
    }

    /*************** All button tap functionalities end here**************/

}

extension MeetMeLogBook {

    /**************Function for Accept, Decline and Delete meet start here ****************/

    @objc func deleteMeet(sender: Any) {
      
        let index = (sender as AnyObject).view!.tag
        self.dualActionPopUp(heading: self.appService.DeleteMeetme, subheading: self.appService.DeleteMeetmeSubHeading, action: "deleteMeetSuccess", method: "deleteMeetme", module: "NA", meetMeIndex: index )
        self.dualActionPopUpVCController()

    }
    
    func deleteMeetOnLeft(index : Int) {

        self.dualActionPopUp(heading: self.appService.DeleteMeetme, subheading: self.appService.DeleteMeetmeSubHeading, action: "deleteMeetSuccess", method: "deleteMeetme", module: "NA", meetMeIndex: index )
        self.dualActionPopUpVCController()

    }

    @objc func declineMeetMe(sender: Any) {
        let index = (sender as AnyObject).view!.tag
        self.dualActionPopUp(heading: self.appService.declineMeet, subheading: self.appService.declineMeeting, action: "declineMeetSuccess", method: "declineMeetme", module: "NA", meetMeIndex: index )
        self.dualActionPopUpVCController()

    }

    /****************Function for Accept, Decline and Delete meet end here ******************/

    /**************Function for opening and closing extended view , detail view and create meet vc start here ****************/

    @objc func openMeetmeDetailVC(sender: Any) {
        stopTimer()
        if self.meetList.count>(sender as AnyObject).view!.tag {
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        let meetMeBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "MeetMeDetailVC") as! MeetMeDetailVC
        meetMeBaseViewcontroller.selectedMeetmeId = self.meetList[(sender as AnyObject).view!.tag].meetmeId!
        meetMeBaseViewcontroller.isUpcoming = self.isUpcoming
        meetMeBaseViewcontroller.hasOpenedContacts = false
        meetMeBaseViewcontroller.view.frame = self.view.bounds
            self.addChild(meetMeBaseViewcontroller)
        self.view.addSubview(meetMeBaseViewcontroller.view)

        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            meetMeBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
        }
    }

    @objc func openVerifyMasterKeyVCForLockChange(sender: Any) {
        stopTimer()
        let gesture = sender as! TapGestureRecognizer
        self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIndex", value: gesture.meetIndex!)
        self.appSharedPrefernce.setAppSharedPreferences(key: "participantIndex", value: gesture.participantIndex!)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController") as! MasterKeyPopUpViewController
        nextVC.reloadProtocol = self as RedirectedToMeetmeLogbookVC
        nextVC.fromMeetMeForCapsuleLock = true

        self.present(nextVC, animated: true, completion: nil)
    }

    @objc func openVerifyMasterKeyVCForCapsule(sender: Any) {
        stopTimer()
        let gesture = sender as! TapGestureRecognizer
        self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIndex", value: gesture.meetIndex!)
        self.appSharedPrefernce.setAppSharedPreferences(key: "participantIndex", value: gesture.participantIndex!)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController") as! MasterKeyPopUpViewController
        nextVC.reloadProtocol = self as RedirectedToMeetmeLogbookVC
        nextVC.fromMeetMeForCapsuleOpening = true
        self.present(nextVC, animated: true, completion: nil)
    }

    @objc func openMeetmeContactDetailVC(sender: Any) {
        stopTimer()
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        let meetMeBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "MeetMeDetailVC") as! MeetMeDetailVC
        meetMeBaseViewcontroller.selectedMeetmeId = self.meetList[(sender as AnyObject).view!.tag].meetmeId!
        meetMeBaseViewcontroller.hasOpenedContacts = true
        meetMeBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(meetMeBaseViewcontroller)
        meetMeBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(meetMeBaseViewcontroller.view)

        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            meetMeBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }

    func openCreateMeetVC() {
        
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        let meetMeBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "CreateMeetVC") as! CreateMeetVC
        meetMeBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(meetMeBaseViewcontroller)
        meetMeBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(meetMeBaseViewcontroller.view)
        NotificationCenter.default.post(name: HIDE_CREATE_MEET_BTN, object: self)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            meetMeBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)

    }

    @objc func openCapsule(sender: Any) {
        stopTimer()
        let fetchedMeeting = self.meetList[(sender as! UIButton).tag]
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        let meetmeCapsuleVC = storyboard.instantiateViewController(withIdentifier: "MeetMeCapsuleVC") as! MeetMeCapsuleVC
        meetmeCapsuleVC.meetmeTitle = fetchedMeeting.meetmeTitle
        meetmeCapsuleVC.selectedMeetmeId  = fetchedMeeting.meetmeId!
        meetmeCapsuleVC.isFromDetails = false
        meetmeCapsuleVC.makeUnreadCapsuleCountZero = self as MakeUnreadCapsuleCountZero
        for meetParticipant in fetchedMeeting.participantList {
            if meetParticipant.isHost {
                meetmeCapsuleVC.hostImage.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"), options: [.continueInBackground, .progressiveLoad])
            }
        }
        meetmeCapsuleVC.view.frame = self.view.bounds
        self.addChild(meetmeCapsuleVC)
        self.view.addSubview(meetmeCapsuleVC.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            meetmeCapsuleVC.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
        self.selectedMeetIdToSeeCapsule = fetchedMeeting.meetmeId!
        self.selectedMeetIndexToSeeCapsule = (sender as! UIButton).tag
    }

    @objc func closeNotification(sender: Any) {
        let index = (sender as AnyObject).view!.tag
        let indexPath = IndexPath(row: index, section: 0)
        guard let cell = meetMeLogbookTableView.cellForRow(at: indexPath ) as? MeetMeLogBookTableViewCell
            else { return }

        switch cell.isExpanded {

        case true:
            self.expandedRows.remove(((indexPath as AnyObject).row))
            stopTimer()
            break

        case false:
            selectedMeetMeId = meetList[index].meetmeId!
            selectedMeetMeIndex = index
            self.fetchMeetingDetail()
            self.expandedRows.insert(((indexPath as AnyObject).row))
            break

        }

        cell.isExpanded = !cell.isExpanded
        self.meetMeLogbookTableView.beginUpdates()
        self.meetMeLogbookTableView.endUpdates()
    }

    @objc func acceptMeetOptions(sender: Any) {
        let index = (sender as AnyObject).view!.tag
        self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIndex", value: index)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Meetme", bundle: nil)
        let acceptVC = storyBoard.instantiateViewController(withIdentifier: "MeetMeTrackingOptionDialogueVC") as! MeetMeTrackingOptionDialogueVC
        acceptVC.modalPresentationStyle = .overCurrentContext
        acceptVC.acceptedDelegate = self as AcceptMeet
        present(acceptVC, animated: true, completion: nil)

    }
    /************Function for opening and closing extended view end here **************/

}

/**********************Start of Extension for API Calls*********************/

extension MeetMeLogBook {

    func acceptMeetMeRequest( meetId: String, indexNo: Int, trackingStatus: String, trackingTime: String, transport: String, hostId: String, hostName: String) {
        guard networkStatus.isNetworkReachable()
            else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.appService.checkInternetConnectionMessage)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.appService.firstTimeInterval, execute: {
                    self.view.hideToast()
                })
                return
        }
        self.startLoader()

        let acceptMeetme = AcceptMeetInvite(meetmeId: meetId, trackingStatus: trackingStatus, trackingTime: trackingTime, trackingMode: transport)
        
        acceptMeetme.addDidFinishBlockObserver { [unowned self] (operation, _) in
            let responseStatus = operation.responseStatus
            if responseStatus == "200"{
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.view.makeToast(self.appService.acceptMeetSuccessString)
                    self.expandedRows = []
                    let indexLogbook = IndexPath(row: indexNo, section: 0)

                    let participants = self.meetList[indexNo].participantList

                    if let selectedParticipant = participants.first(where: {$0.fisheyeId == self.fisheyeId}) {

                        selectedParticipant.isAccepted = true
                        selectedParticipant.isDeclined = false
                        self.meetMeLogbookTableView.reloadRows(at: [indexLogbook], with: .top)
                    }
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User accepts MeetMe Successfully", value: 0)
                }
                DatabaseManagement.shared.createContactIfDoesntExists(hostId: hostId, hostName: hostName)
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                    self.view.hideToast()
                })

            } else if responseStatus == "405"{
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.view.makeToast(self.appService.acceptMeetLateString)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                    self.view.hideToast()
                })

                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User's accept MeetMe attempt Failed", value: 0)
            }else if responseStatus == "406"{
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.view.makeToast(self.appService.acceptMeetParticipentIsNotPartOfMeet)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                    self.view.hideToast()
                })
                
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User's accept MeetMe attempt Failed", value: 0)
            } else {
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.view.makeToast(self.appService.acceptMeetFailureString)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                    self.view.hideToast()
                })

                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User's accept MeetMe attempt Failed", value: 0)
            }
        }
        AppDelegate.addProcedure(operation: acceptMeetme)
    }

    func declineMeetMeRequest( meetId: String, indexNo: Int) {
        guard networkStatus.isNetworkReachable()
            else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.appService.checkInternetConnectionMessage)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                    self.view.hideToast()
                })
                return
        }
        self.startLoader()
        let declineMeetme = DeclineMeetInvite(meetmeId: meetId)
        declineMeetme.addDidFinishBlockObserver { [unowned self] (operation, _) in
            let responseStatus = operation.responseStatus
            if responseStatus == "200"{
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.view.makeToast(self.appService.declinedMeetSuccessString)
                    let indexLogbook = IndexPath(row: indexNo, section: 0)
                    self.expandedRows = []
                    let participants = self.meetList[indexNo].participantList
                    if let selectedParticipant = participants.first(where: {$0.fisheyeId == self.fisheyeId}) {
                        selectedParticipant.isAccepted = false
                        selectedParticipant.isDeclined = true
                        self.meetMeLogbookTableView.reloadRows(at: [indexLogbook], with: .top)
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                    self.view.hideToast()
                })
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Decline Meetme Confirmation Popup", action: "Confirmation Popup Ok button clicked", label: "User declines MeetMe Successfully", value: 0)
            } else if responseStatus == "405"{
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.view.makeToast(self.appService.declinedMeetLateString)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                    self.view.hideToast()
                })

                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User's accept MeetMe attempt Failed", value: 0)

            } else {
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.view.makeToast(self.appService.declinedMeetFailureString)
                }

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                    self.view.hideToast()
                })

                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Decline Meetme Confirmation Popup", action: "Confirmation Popup Ok button clicked", label: "User's decline MeetMe attempt Failed", value: 0)
            }
        }
        AppDelegate.addProcedure(operation: declineMeetme)
    }

    func deleteMeetme( deletedMeetme: MeetMeLogbookVariables) {
        guard networkStatus.isNetworkReachable()
            else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.appService.checkInternetConnectionMessage)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                    self.view.hideToast()
                })
                return
        }
        self.startLoader()
        var meetmeToDelete = deletedMeetme

        meetmeToDelete.isDeleted = true
        let deleteMeetme = UpdateMeetme(updatedMeetme: meetmeToDelete)
        deleteMeetme.addDidFinishBlockObserver { [unowned self] (operation, _) in
            self.stopLoader()
            let responseStatus = operation.responseStatus
            if responseStatus == "200"{
                DispatchQueue.main.async {
                    self.view.makeToast(self.appService.deleteMeetSuccess)
                    if self.hasSelectedAllMeetings && self.isUpcoming {
                        self.stopLoader()
                        self.meetList = []
                        self.meetMeLogbookTableView.reloadData()
                        self.getMeetLogbook(upcomingFlagApiCall: "true", searchKeyApiCall: "")
                    } else if !self.hasSelectedAllMeetings && self.isUpcoming {
                        self.meetList = []
                        self.meetMeLogbookTableView.reloadData()
                        self.getMyMeetings(upcomingFlagApiCall: "true", searchKeyApiCall: "")
                    } else if self.hasSelectedAllMeetings && !self.isUpcoming {
                        self.meetList = []
                        self.meetMeLogbookTableView.reloadData()
                        self.getMeetLogbook(upcomingFlagApiCall: "false", searchKeyApiCall: "")
                    } else {
                        self.meetList = []
                        self.meetMeLogbookTableView.reloadData()
                        self.getMyMeetings(upcomingFlagApiCall: "false", searchKeyApiCall: "")
                    }
                }

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                    self.view.hideToast()
                }
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Delete Meetme Confirmation Popup", action: "Confirmation Popup Ok button clicked", label: "User deletes MeetMe Successfully", value: 0)
            } else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.appService.deleteMeetFaiure)
                }

                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval) {
                    self.view.hideToast()
                }

            }
        }
        AppDelegate.addProcedure(operation: deleteMeetme)

    }

    func fetchMeetLocations(meetmeId: String, index: Int) {
        let getMeetLocation = GetLocationOfMeetme(meetIdForLocation: meetmeId)

        getMeetLocation.addDidFinishBlockObserver { [unowned self] (operation, _) in
            let response = operation.responseStatus
            if response == "200"{

                var destinationLatitude: Double  = 0.0
                var destinationLongitude: Double = 0.0

                var meetParticipantLocations = operation.meetParticipantLocations
                let tempIndexPath = IndexPath(row: self.selectedMeetMeIndex, section: 0)
                guard self.meetList.count > tempIndexPath.row
                    else{
                          return
                    }
                let tempMeetme = self.meetList[tempIndexPath.row]
                DispatchQueue.main.async {
                    guard let cell = self.meetMeLogbookTableView.cellForRow(at: tempIndexPath ) as? MeetMeLogBookTableViewCell
                        else { return }

                    var meetAnnotations: [CustomAnnotation] = []
                    DispatchQueue.main.async {
                        let allAnnotations = cell.meetMeLogBookMapView.annotations
                        cell.meetMeLogBookMapView.removeAnnotations(allAnnotations)

                        let allOverlays = cell.meetMeLogBookMapView.overlays
                        cell.meetMeLogBookMapView.removeOverlays(allOverlays)

                        let destinationAnnotation = CustomAnnotation()

                        let lat = (tempMeetme.latitude! as NSString).doubleValue
                        destinationLatitude = lat
                        let long = (tempMeetme.longitude! as NSString).doubleValue
                        destinationLongitude = long

                        destinationAnnotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)

                        destinationAnnotation.title = "Destination"
                        destinationAnnotation.annotationType = "Destination"

                        cell.meetMeLogBookMapView.showAnnotations([destinationAnnotation], animated: true)

//                        for annotation in allAnnotations{
//                            if let customAnnotation = annotation as? CustomAnnotation{
//                                if customAnnotation.annotationType == "Destination"{
//                                    cell.meetMeLogBookMapView.removeAnnotation(customAnnotation)
//
//                                    let destinationAnnotation = CustomAnnotation()
//
//                                    let lat = (tempMeetme.latitude! as NSString).doubleValue
//                                    destinationLatitude = lat
//                                    let long = (tempMeetme.longitude! as NSString).doubleValue
//                                    destinationLongitude = long
//
//                                    destinationAnnotation.coordinate = CLLocationCoordinate2D(latitude: lat , longitude:long)
//
//                                    destinationAnnotation.title = "Destination"
//                                    destinationAnnotation.annotationType = "Destination"
//
//                                    cell.meetMeLogBookMapView.showAnnotations([destinationAnnotation], animated: true)
//                                }
//                            }
//                        }
                    }

                    DispatchQueue.global().async {
            
                        for l in 0..<meetParticipantLocations.count {

                            let userLocations = meetParticipantLocations[l].geoLocations
                            var userAnnotations: [CustomAnnotation] = []
                            for k in 0..<userLocations.count {
                                
                                let geolocation = userLocations[k]
                                let annotation = CustomAnnotation()

                                let lat = geolocation.participantLatitude

                                let long = geolocation.participantLongitude

//                                annotation.annotationType = "Participant"
                                if k == 0 {
                                    annotation.annotationType = "Participant"
                                    annotation.participantName =  meetParticipantLocations[l].participantName
                                    annotation.title = meetParticipantLocations[l].participantName
                                }

                                if k == userLocations.count - 1 {
                                   
                                    annotation.annotationType = "ParticipantLoctionStart"
                                    annotation.participantName = meetParticipantLocations[l].participantName
                                    annotation.title = meetParticipantLocations[l].participantName
                                }

                                //added by pritam
                                if k == 0 {
                                    let directionRequest = MKDirections.Request()
                                    let soruceLatitude = geolocation.participantLatitude
                                    let sourceLongitude = geolocation.participantLongitude

                                    let sourceCoordinates = CLLocationCoordinate2D(latitude: soruceLatitude!, longitude: sourceLongitude!)
                                    let destCoordinates  = CLLocationCoordinate2D(latitude: destinationLatitude, longitude: destinationLongitude)

                                    let sourcePlacemark = MKPlacemark(coordinate: sourceCoordinates )
                                    let destPlacemark = MKPlacemark(coordinate: destCoordinates)

                                    let sorceItem = MKMapItem(placemark: sourcePlacemark)
                                    let destItem = MKMapItem(placemark: destPlacemark)

                                    directionRequest.source = sorceItem
                                    directionRequest.destination = destItem
                                    if meetParticipantLocations[l].trackingMode == "Walking"{
                                        directionRequest.transportType = .walking
                                    } else if meetParticipantLocations[l].trackingMode == "Driving"{
                                        directionRequest.transportType = .automobile
                                    }

                                    let directions = MKDirections(request: directionRequest)
                                    directions.calculate(completionHandler: {
                                        response, error in

                                        guard let response = response else {
                                            if let error = error {
                                                print("error......",error)
                                            }
                                            return
                                        }
                                        let route = response.routes[0]
                                          route.polyline.title = "isForDirections"
                                        cell.meetMeLogBookMapView.addOverlay(route.polyline, level: .aboveRoads)
                                        let rekt =  route.polyline.boundingMapRect
                                        cell.meetMeLogBookMapView.setRegion(MKCoordinateRegion(rekt), animated: true)

                                        cell.meetMeLogBookMapView.setRegion(MKCoordinateRegion(rekt), animated: true)

                                    })
                                }

                                annotation.coordinate = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                                userAnnotations.append(annotation)
                                meetAnnotations.append(annotation)
                            }

                            DispatchQueue.main.async {
                                cell.meetMeLogBookMapView.addAnnotations(userAnnotations)

                                var locations = userAnnotations.map { $0.coordinate }

                                let polyline = MKPolyline(coordinates: &locations, count: locations.count)
                                polyline.title = "isNotForDirections"
                                cell.meetMeLogBookMapView.addOverlay(polyline)
                            }
                        }
                    }
                }

            }

        }
        AppDelegate.addProcedure(operation: getMeetLocation)
    }

    func getNotificationCount( ) {
        let getNotificationCountApi = getProfileOperation()
        getNotificationCountApi.addDidFinishBlockObserver { [unowned self] (operation, _) in
            DispatchQueue.main.async {
                if let profileReputationpoints = operation.profileObject.userMeetmeNotificationCount {
                    self.activeNotificationLabel.text = String(profileReputationpoints)
                }
            }
        }
        AppDelegate.addProcedure(operation: getNotificationCountApi)
    }
    
    
    func reduceNotificationCountAndMarkSeen(countToReduce: Int, meetIdArray : [String]){
        
        let reduceNotificationCount = ReduceUserMeetNotificationCount(countToReduce: countToReduce, meetIdArray : meetIdArray)
        reduceNotificationCount.addDidFinishBlockObserver { [unowned self] (operation, _) in
            DispatchQueue.main.async {
                
            }
        }
        AppDelegate.addProcedure(operation: reduceNotificationCount)
    }

}
/**********************End of Extension for API Calls**********************/

/**********************Function for delete callback start here*******************/
extension MeetMeLogBook: RedirectedToMeetmeLogbookVC, AcceptMeet {

    func nextAction(method: String) {
        switch method {
        case "declineMeetme":
            let meetMeIndex = (self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIndex") as? Int)!
            let meetMeToDelete = self.meetList[meetMeIndex].meetmeId
            let indexPath = IndexPath(row: meetMeIndex, section: 0)
            guard let cell = meetMeLogbookTableView.cellForRow(at: indexPath ) as? MeetMeLogBookTableViewCell
                else { return }
            switch cell.isExpanded {
            case true:
                self.expandedRows.remove(((indexPath as AnyObject).row)!)
                break

            case false:
                self.expandedRows.insert(((indexPath as AnyObject).row)!)
                break
            }
            self.appSharedPrefernce.removeAppSharedPreferences(key: "meetMeIndex")
            declineMeetMeRequest(meetId: meetMeToDelete!, indexNo: meetMeIndex)
            break

        case "deleteMeetme":
            self.stopTimer()
            let meetMeIndex = (self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIndex") as? Int)!
            let meetMeToDelete = self.meetList[meetMeIndex]
            let indexPath = IndexPath(row: meetMeIndex, section: 0)
            guard let cell = meetMeLogbookTableView.cellForRow(at: indexPath ) as? MeetMeLogBookTableViewCell
                else { return }
            switch cell.isExpanded {
            case true:
                self.expandedRows.remove(((indexPath as AnyObject).row)!)
                break

            case false:
                self.expandedRows.insert(((indexPath as AnyObject).row)!)
                break
            }
            self.appSharedPrefernce.removeAppSharedPreferences(key: "meetMeIndex")
            deleteMeetme(deletedMeetme: meetMeToDelete)
            break

        case "openMeetmeCapsule":
            let meetMeIndex = (self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIndex") as? Int)!
            let sender  = UIButton()
            sender.tag = meetMeIndex
            self.appSharedPrefernce.removeAppSharedPreferences(key: "meetMeIndex")
            self.openCapsule(sender: sender)
            break

        case "changeLockStatus":
            let meetMeIndex = (self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIndex") as? Int)!
            let participantIndex =  (self.appSharedPrefernce.getAppSharedPreferences(key: "participantIndex") as? Int)!
            let isCapsuleLocked = meetList[meetMeIndex].participantList[participantIndex].isCapsuleLocked
            self.changeCapsuleLock(selectedMeetmeIndex: meetMeIndex, isCapsuleLockedRecieved: isCapsuleLocked!, participantIndex: participantIndex)
            self.appSharedPrefernce.removeAppSharedPreferences(key: "meetMeIndex")
            self.appSharedPrefernce.removeAppSharedPreferences(key: "participantIndex")
            break
            
        case "navigateToChatScreen":
            DispatchQueue.main.async{
                let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
                let meetmeCapsuleVC = storyboard.instantiateViewController(withIdentifier: "MeetMeCapsuleVC") as! MeetMeCapsuleVC
                meetmeCapsuleVC.meetmeTitle = self.meetTitle
                meetmeCapsuleVC.selectedMeetmeId  = self.notificationMeetMeId
                meetmeCapsuleVC.makeUnreadCapsuleCountZero = self as MakeUnreadCapsuleCountZero
                meetmeCapsuleVC.isFromDetails = false
                meetmeCapsuleVC.view.frame = self.view.bounds
                self.addChild(meetmeCapsuleVC)
                self.view.addSubview(meetmeCapsuleVC.view)
                UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    meetmeCapsuleVC.view.frame.origin.y = self.view.bounds.origin.y
                }, completion: nil)
            }
            break
        default:
            break
        }
    }

    func meetAccepted(discloseTrail: String, discloseTrailTime: String, transportMode: String) {
      
        let meetMeIndex = (self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIndex") as? Int)!
        let meetMeToAccept = self.meetList[meetMeIndex].meetmeId
        let hostId = self.meetList[meetMeIndex].hostId
//        let hostParticipant = ParticipantVariables()
        let hostParticipant = self.meetList[meetMeIndex].participantList.first{$0.isHost == true}
        acceptMeetMeRequest(meetId: meetMeToAccept!, indexNo: meetMeIndex, trackingStatus: discloseTrail, trackingTime: discloseTrailTime, transport: transportMode, hostId: hostId as? String ?? "", hostName: hostParticipant?.participantName as? String ?? "")

    }

    /**********************Function for delete callback end here**********************/

    /*****************Functions for pop up with two options start here ****************/
    func dualActionPopUp(heading: String, subheading: String, action: String, method: String, module: String, meetMeIndex: Int) {
        self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
        self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
        self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
        self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIndex", value: meetMeIndex)
    }

    func dualActionPopUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "AllPopupDualActionVC") as! AllPopupDualActionVC
        nextVC.reloadLogbook = self as RedirectedToMeetmeLogbookVC
        self.present(nextVC, animated: true, completion: nil)
    }
    /*****************Functions for pop up with two options end here ****************/

    /************************** Timer functions start here **************************/
    func scheduleTimer() {
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
    }

    @objc func updateLocation() {

        if self.meetList.count>selectedMeetMeIndex {
            guard let selectedMeetme  = meetList[selectedMeetMeIndex] as? MeetMeLogbookVariables else { return}
            let todaysDate = validationController.getTodaysDateInTimeStamp()
            if todaysDate>selectedMeetme.trackingTime! && selectedMeetme.endDateTime!>todaysDate {
                fetchMeetLocations(meetmeId: selectedMeetMeId, index: selectedMeetMeIndex)
            }
            getMessageCount(selectedMeetmeId: selectedMeetMeIndex)
        }
    }

    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }

    /************************** Timer functions end here **************************/

    /************************** Loader functions start here **************************/
    func startLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }

    }

    func stopLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.stopLoader()
        }

    }

    /************************** Loader functions end here **************************/

    /************************** To fit all annotations functions start here **************************/
    func zoomAnnotations(on mapView: MKMapView, toFrame annotationsFrame: CGRect, animated: Bool) {
        if mapView.annotations.count < 2 {
            return
        }
        // Step 1: make an MKMapRect that contains all the annotations
        let annotations = mapView.annotations
        let firstAnnotation: MKAnnotation = annotations[0]
        var minPoint: MKMapPoint = MKMapPoint(firstAnnotation.coordinate)
        var maxPoint: MKMapPoint = minPoint
        for annotation: MKAnnotation in annotations {
            let point: MKMapPoint = MKMapPoint(annotation.coordinate)
            if point.x < minPoint.x {
                minPoint.x = point.x
            }
            if point.y < minPoint.y {
                minPoint.y = point.y
            }
            if point.x > maxPoint.x {
                maxPoint.x = point.x
            }
            if point.y > maxPoint.y {
                maxPoint.y = point.y
            }
        }
        let mapRect: MKMapRect = MKMapRect(x: minPoint.x, y: minPoint.y, width: maxPoint.x - minPoint.x, height: maxPoint.y - minPoint.y)
        // Step 2: Calculate the edge padding
        let edgePadding: UIEdgeInsets = UIEdgeInsets(top: annotationsFrame.minY, left: annotationsFrame.minX, bottom: mapView.bounds.maxY - annotationsFrame.maxY, right: mapView.bounds.maxX - annotationsFrame.maxX)
        // Step 3: Set the map rect
        mapView.setVisibleMapRect(mapRect, edgePadding: edgePadding, animated: animated)
    }
    /************************* To fit all annotations functions end here *************************/

    /******************** To disable button actions while loading  starts here ******************/

    func disableAllButtons() {
        DispatchQueue.main.async {
            self.logbookButton.isUserInteractionEnabled = false
            self.myMeetingsButton.isUserInteractionEnabled = false
            self.allMeetingsButton.isUserInteractionEnabled = false
            self.meetMeLogbookTableView.isUserInteractionEnabled = false
        }
    }

    func enableAllButtons() {
        DispatchQueue.main.async {
            self.logbookButton.isUserInteractionEnabled = true
            self.myMeetingsButton.isUserInteractionEnabled = true
            self.allMeetingsButton.isUserInteractionEnabled = true
        }
    }

    /******************** To disable button actions while loading  starts here ******************/

    /******************** To set all the inernationalization texts ******************************/
    @objc func setLocalizationText() {
      DispatchQueue.main.async {
            self.appBarMeetmeTitleLabel.text = self.appService.meetMe
            self.appBarMeetLogbookLabel.text = self.appService.meetmeLogbook
            self.myMeetingsLabel.text = self.appService.myMeetingsLabel
            self.allMeetingsLabel.text = self.appService.allMeetingsLabel
            self.createMeetButton.setTitle(self.appService.createMeetButton, for: [])
            self.noMeetingsLabel.text = self.appService.noMeetingsLabel
            self.pastLabel.text =  self.pulseMsg.pastButtonTitle
//            self.setSearchButtonText(text:  AppService.shared.cancel, searchBar: self.meetMeLogbookSearchbar)
            self.setUpSearchBar()
            
      }
    }
    /******************** To set all the inernationalization texts ******************************/
      func setUpSearchBar(){
//            if let searchTextField = self.meetMeLogbookSearchbar.value(forKey: "_searchField") as? UITextField, let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
//                  // Create a template copy of the original button image
//                  let templateImage =  clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
//                  // Set the template image copy as the button image
//                  clearButton.setImage(templateImage, for: .normal)
//                  // Finally, set the image color
//                  clearButton.tintColor = UIColor.red
//
//
//            }
            
//            self.meetMeLogbookSearchbar.setBackgroundImage(UIImage(named: "addCopy3-1"), for: .bookmark, state: .normal)

            
      }
     
//      func setUpSearchBar() {
//
//            let barButtonAppearanceInSearchBar: UIBarButtonItem?
//            barButtonAppearanceInSearchBar = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])
//            barButtonAppearanceInSearchBar?.image = UIImage(named: "addCopy3-1.png")?.withRenderingMode(.alwaysTemplate)
//            barButtonAppearanceInSearchBar?.tintColor = UIColor.white
//            barButtonAppearanceInSearchBar?.title = nil
//      }

      
//      func setSearchButtonText(text:String,searchBar:UISearchBar) {
//
//            for subview in searchBar.subviews {
//                  for innerSubViews in subview.subviews {
//                        if let cancelButton = innerSubViews as? UIButton {
//                              var bonds: CGRect
//                              bonds = cancelButton.frame
//                              bonds.size.width = 84
//                              cancelButton.bounds = bonds
//
//                              cancelButton.setTitleColor(UIColor.white, for: .normal)
//                              cancelButton.setTitle(text, for: .normal)
//                        }
//                  }
//            }
//
//      }

    func setDetail(fetchedMeeting: MeetMeLogbookVariables) {

        let indexLogbook = IndexPath(row: selectedMeetMeIndex, section: 0)

        guard let cell = self.meetMeLogbookTableView.cellForRow(at: indexLogbook) as? MeetMeLogBookTableViewCell else {return}

        cell.meetMeLogBookMapView.delegate = self
        cell.participantCollectionView?.register(ParticipantsLogbookCollectionViewCell.self, forCellWithReuseIdentifier: "ParticipantsLogbookCollectionViewCell")
        cell.participantCollectionView?.register(UINib(nibName: "ParticipantsLogbookCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ParticipantsLogbookCollectionViewCell")
        let meetTableObj =  fetchedMeeting

        cell.meetTitleLabel.text = meetTableObj.meetmeTitle
        cell.meetAddressLabel.text = meetTableObj.meetmeLocation
        cell.meetTimeLabel.text = validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.startDateTime!)
        let givenDate = validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!)
        let todaysDate = validationController.getTodaysDate()
        let differenceInDate = validationController.calculateDaysBetweenTwoDates(start: todaysDate, end: givenDate)
        switch (differenceInDate) {
        case 0:
            cell.meetDateLabel.text = appService.meetDateLabel
            break
        case 1:
            cell.meetDateLabel.text = appService.meetDateLabel2
            break
        default:
            cell.meetDateLabel.text = givenDate
            break
        }

        //For extended view
        if validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!) == validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.endDateTime!) {
            cell.extendedViewDateTime.text =
            "\(validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!) ) - \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.startDateTime!) ) \(self.pulseMsg.labelBetweenSDandED) \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.endDateTime!) )"
        } else {
            cell.extendedViewDateTime.text =
            "\(validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.startDateTime!) ) - \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.startDateTime!) ) \(self.pulseMsg.labelBetweenSDandED) \(validationController.convert_date_from_UTC_time(givenUTCdate: meetTableObj.endDateTime!) ) - \(validationController.convert_time_from_UTC_time(givenUTCtime: meetTableObj.endDateTime!) )"
        }
        //For destination on map
        let annotation = CustomAnnotation()
        let lat = (meetTableObj.latitude! as NSString).doubleValue
        let long = (meetTableObj.longitude! as NSString).doubleValue
        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        annotation.title = "Destination"
        annotation.annotationType = "Destination"
        cell.meetMeLogBookMapView.addAnnotation(annotation)

        //Tap gestures for accept ,decline and details

        cell.acceptButton.tag = indexLogbook.row
        cell.declineButton.tag = indexLogbook.row
        cell.mainNotificationView.tag = indexLogbook.row
        cell.meetMeLogBookMapView.tag = indexLogbook.row
        cell.participantCollectionView.tag = indexLogbook.row
        cell.unlockCapsuleButton.tag = indexLogbook.row
        let  gestureForMainNotification1 =  setTapGesturesForMeetmeLogbookCell(componentName: "closeCell")
        let gestureForMainNotification2 =  setTapGesturesForMeetmeLogbookCell(componentName: "openMeetmeDetail")
        gestureForMainNotification1.require(toFail: gestureForMainNotification2)
        cell.mainNotificationView.addGestureRecognizer(gestureForMainNotification1)
        cell.mainNotificationView.addGestureRecognizer(gestureForMainNotification2)

        let  gestureForMapView1 =  setTapGesturesForMeetmeLogbookCell(componentName: "closeCell")
        let gestureForMapView2 =  setTapGesturesForMeetmeLogbookCell(componentName: "openMeetmeDetail")
        gestureForMapView1.require(toFail: gestureForMapView2)
        cell.meetMeLogBookMapView.addGestureRecognizer(gestureForMapView1)
        cell.meetMeLogBookMapView.addGestureRecognizer(gestureForMapView2)

        let gestureForAcceptMeet =  setTapGesturesForMeetmeLogbookCell(componentName: "acceptMeet")
        let gestureForDeclineMeet =  setTapGesturesForMeetmeLogbookCell(componentName: "declineMeet")
        let gestureForCollectionView =  setTapGesturesForMeetmeLogbookCell(componentName: "openMeetmeDetailWithContacts")
        cell.participantCollectionView.addGestureRecognizer(gestureForCollectionView)

        // For accept, decline and delete status
        for l in 0..<meetTableObj.participantList.count {

            let participantObj = meetTableObj.participantList[l]
            if participantObj.isHost! {
                cell.hostImageView?.sd_setImage(with: URL(string: participantObj.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                cell.hostImageView.layer.cornerRadius = cell.hostImageView.frame.size.width / 2
                cell.hostImageView.clipsToBounds = true
            }
            if participantObj.fisheyeId! == self.fisheyeId {

                let gestureForChangeLock = changeLockstatusGesture(meetindex: indexLogbook.row, participantIndex: l, forLockStatusChange: true)
                cell.capsuleLockButton.addGestureRecognizer(gestureForChangeLock)

                let gestureForUnlockCapsule = changeLockstatusGesture(meetindex: indexLogbook.row, participantIndex: l, forLockStatusChange: false)
                //fOR CAPSULE
                if participantObj.isCapsuleLocked! {
                    cell.capsuleLockImageView.image = UIImage(named: "masterKeyLock")
                    cell.unlockCapsuleButton.addGestureRecognizer(gestureForUnlockCapsule)
                } else {
                    cell.capsuleLockImageView.image = UIImage(named: "masterKeyUnlocked")
                    cell.unlockCapsuleButton.tag = indexLogbook.row
                    cell.unlockCapsuleButton.addTarget(self, action: #selector(openCapsule(sender:)), for: .allTouchEvents)
                }

                //For Capsule count
                if let capsuleCount = participantObj.messageCount {
                    cell.capsuleNotificationNumber.text = String(capsuleCount)
                }

                //For tracking status
                if participantObj.trackingStatus != nil && participantObj.istrackingEnabled! {
                    cell.earliestTrackingLabel.isHidden = false
                  
                  let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionName == participantObj.trackingStatus ) } )
                  
                  if objArray != nil && objArray.count > 0{
                        cell.trackingStatus.text = objArray[0].optionTranslatedName
                        
                  }else {
                        cell.trackingStatus.text = participantObj.trackingStatus
                        
                  }
                  
                  cell.trackingTimeLabel.text = participantObj.trackingTime! + " " + self.signmsg.minutes
                  
                } else {
                    cell.earliestTrackingLabel.isHidden = true
                    cell.trackingStatus.text = ""
                    cell.trackingTimeLabel.text = ""
                }
                if isUpcoming {
                    //For meet interaction of delete,accept,decline
                    if participantObj.isHost! {
                        cell.isHoster = true
                        cell.declineImage.isHidden = true
                        cell.declineButton.isHidden = true
                        cell.acceptImage.isHidden = true
                        if cell.isExpanded {
                            cell.meetDeleteIcon.isHidden = false
                        } else {
                            cell.meetDeleteIcon.isHidden = true
                        }
                        let gestureForDelete =  setTapGesturesForMeetmeLogbookCell(componentName: "deleteMeet")
                        cell.acceptButton.addGestureRecognizer(gestureForDelete)
                    } else if participantObj.isAccepted! && !participantObj.isDeclined! {
                        cell.inviteStatusView.backgroundColor = UIColor.acceptGreen()

                        cell.meetDeleteIcon.isHidden = true
                        cell.acceptImage.image = UIImage(named: "AcceptedFilled")
                        cell.declineImage.image = UIImage(named: "DeclinedHollow")
                        cell.acceptButton.addGestureRecognizer(gestureForAcceptMeet)
                        cell.declineButton.addGestureRecognizer(gestureForDeclineMeet)

                    } else if !participantObj.isAccepted! && participantObj.isDeclined! {

                        cell.inviteStatusView.backgroundColor = UIColor.rejectRed()

                        cell.meetDeleteIcon.isHidden = true
                        cell.declineImage.image = UIImage(named: "DeclinedFilled")
                        cell.acceptImage.image = UIImage(named: "AcceptedHollow")
                        cell.acceptButton.addGestureRecognizer(gestureForAcceptMeet)
                        cell.declineButton.addGestureRecognizer(gestureForDeclineMeet)

                    } else {

                        cell.inviteStatusView.backgroundColor = UIColor.lightPurple()
                        cell.meetDeleteIcon.isHidden = true
                        cell.declineImage.image = UIImage(named: "DeclinedHollow")
                        cell.acceptImage.image = UIImage(named: "AcceptedHollow")
                        cell.acceptButton.addGestureRecognizer(gestureForAcceptMeet)
                        cell.declineButton.addGestureRecognizer(gestureForDeclineMeet)
                    }
                } else {
                    cell.declineImage.image = UIImage(named: "Image-3")
                    cell.acceptImage.image = UIImage(named: "Image-3")
                    cell.meetDeleteIcon.image = UIImage(named: "Image-3")
                    cell.acceptButton.isUserInteractionEnabled = false
                    cell.declineButton.isUserInteractionEnabled = false

                    if participantObj.isAccepted! && !participantObj.isHost! {
                        cell.inviteStatusView.backgroundColor = UIColor.acceptGreen()
                    } else if participantObj.isDeclined! && !participantObj.isHost! {
                        cell.inviteStatusView.backgroundColor = UIColor.rejectRed()
                    } else if !participantObj.isHost! {
                        cell.inviteStatusView.backgroundColor = UIColor.lightPurple()
                    }
                }

            }
        }

        cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexLogbook.row)
        cell.participantCollectionView.reloadData()
        cell.participantCollectionView.tag = indexLogbook.row
      
        if(isEventFlagDetail){
//            cell.capsuleView.isHidden = true
//            cell.capsuleImageView.isHidden = true
//            cell.capsuleNotificationCounLabelt.isHidden = true
//            cell.capsuleButton.isHidden = true
            cell.capsuleLockButton.isHidden = true
            cell.capsuleLockImageView.isHidden = true
            cell.unlockCapsuleLabel.isHidden = true
        }
        
    }

    /************************** To set tapGestures function start here **************************/

    func setTapGesturesForMeetmeLogbookCell(componentName: String) -> TapGestureRecognizer {
        switch(componentName) {
        case "acceptMeet":
            var tapGestureAccept = TapGestureRecognizer()
            tapGestureAccept = TapGestureRecognizer(target: self, action: #selector(self.acceptMeetOptions(sender:)))
            return tapGestureAccept

        case "declineMeet":
            var tapGestureDecline = TapGestureRecognizer()
            tapGestureDecline = TapGestureRecognizer(target: self, action: #selector(declineMeetMe(sender:)))
            return tapGestureDecline
            
        case  "deleteMeet":
            var tapGestureDelete = TapGestureRecognizer()
            tapGestureDelete = TapGestureRecognizer(target: self, action: #selector(deleteMeet(sender:)))
            return tapGestureDelete

        case  "openMeetmeDetail":
            var tapGestureRespond = TapGestureRecognizer()
            tapGestureRespond = TapGestureRecognizer(target: self, action: #selector(self.openMeetmeDetailVC(sender:)))
            tapGestureRespond.numberOfTapsRequired = 2
            return tapGestureRespond

        case  "closeCell":
            var tapGestureCloseButton = TapGestureRecognizer()
            tapGestureCloseButton = TapGestureRecognizer(target: self, action: #selector(closeNotification(sender:)))
            tapGestureCloseButton.numberOfTapsRequired = 1
            return tapGestureCloseButton

        case "openMeetmeDetailWithContacts":
            var tapGestureContacts = TapGestureRecognizer()
            tapGestureContacts = TapGestureRecognizer(target: self, action: #selector(openMeetmeContactDetailVC(sender:)))
            return tapGestureContacts

        default:
            let tapGesture = TapGestureRecognizer()
            return tapGesture

        }
    }

    func changeLockstatusGesture(meetindex: Int, participantIndex: Int, forLockStatusChange: Bool) -> TapGestureRecognizer {

        var tapGestureCapsule = TapGestureRecognizer()

        if forLockStatusChange {
            tapGestureCapsule = TapGestureRecognizer(target: self, action: #selector(openVerifyMasterKeyVCForLockChange(sender:)))
        } else {
            tapGestureCapsule = TapGestureRecognizer(target: self, action: #selector(openVerifyMasterKeyVCForCapsule(sender:)))
        }
        tapGestureCapsule.meetIndex = meetindex
        tapGestureCapsule.participantIndex = participantIndex
        return tapGestureCapsule
    }

    /************************** To set tapGestures function end here  **************************/

    /************************** API CALLS WITHOUT PROCEDURE KIT***************************/
    func getMeetLogbook(upcomingFlagApiCall: String, searchKeyApiCall: String) {
        guard networkStatus.isNetworkReachable()
            else {

                DispatchQueue.main.async {

                    self.stopLoader()
                    self.noMeetingsLabel.text = self.appService.checkInternetConnectionMessage
                    self.meetMeLogbookTableView.isHidden = true
                    self.noMeetingView.isHidden = false
                    self.noMeetingsLabel.isHidden = false
                    self.createMeetButton.isHidden = true
                    self.enableAllButtons()
                }

                return
        }
        self.expandedRows.removeAll()
        self.disableAllButtons()
        self.startLoader()

        let awsURL = AppConfig.getAllMeetings(upcomingFlagApiCall: upcomingFlagApiCall, searchKeyApiCall: searchKeyApiCall.lowercased())
        guard let tempUrl = awsURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else { return }
        guard let URL = URL(string: tempUrl) else { return }

        let sessionConfig = URLSessionConfiguration.default

        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return }
        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                DispatchQueue.main.async {
                    self.enableAllButtons()
                    self.stopLoader()
                }
                if let meetingResponseObj = json as? [String: Any] {
                    let statusCode = meetingResponseObj["statusCode"]  as? String ??  ""
                    if statusCode == "200"{
                        if let meetingObjList = meetingResponseObj["meetings"] as? [[String: Any]] {
                            var meetIdArray: [String] = []
                            for meetingApiObj in meetingObjList {
                                var meetingObj =  MeetMeLogbookVariables()
                                meetingObj.meetmeId = meetingApiObj["meetmeId"]  as? String ??  ""
                                meetingObj.hostId = meetingApiObj["hostId"]  as? String ??  ""
                                meetingObj.meetmeLocation = meetingApiObj["locationName"] as? String ??  ""
                                meetingObj.meetmeTitle = meetingApiObj["title"] as? String ??  ""
                                meetingObj.latitude = meetingApiObj["latitude"] as? String ??  ""
                                meetingObj.longitude = meetingApiObj["longitude"] as? String ??  ""
                                meetingObj.startDateTime = meetingApiObj["startDateTime"] as? Double
                                meetingObj.endDateTime = meetingApiObj["endDateTime"] as? Double
                                meetingObj.trackingTime = meetingApiObj["trackingTime"] as? Double
                                meetingObj.createdDate = meetingApiObj["createdDate"] as? Double
                                meetingObj.acceptedList = []
                                meetingObj.declinedList = []
                                meetingObj.participantList = []
                                meetingObj.participantListForCollection = []
                                

                                if ((meetingApiObj["isEventCreated"]) != nil) {
                                    
                                    var tempEventFlag = meetingApiObj["isEventCreated"] as? Int ?? 0
                                    if (tempEventFlag == 1){
                                        meetingObj.isEventFlag = true
                                    } else {
                                         meetingObj.isEventFlag = false
                                    }
                                 } else {
                                    meetingObj.isEventFlag = false
                                }
                                
                                if let participantList = meetingApiObj["participants"] as? [[String: Any]] {
                                    for j in 0..<participantList.count {
                                        let participantObj =  ParticipantVariables()
                                        participantObj.isHost =  participantList[j]["ishost"]  as? Bool
                                        participantObj.participantId =  participantList[j]["participantId"]  as? String ??  ""
                                        participantObj.fisheyeId =  participantList[j]["fisheyeId"]  as? String ??  ""
                                        participantObj.participantEmail =  participantList[j]["participantEmailId"]  as? String ??  ""
                                        participantObj.participantName =  participantList[j]["participantName"] as? String ??  ""
                                        participantObj.participantPhoneNumber=participantList[j]["participantPhoneNumber"] as? String ??  ""
                                        participantObj.participantPicture = participantList[j]["participantPicture"] as? String ??  ""
                                        participantObj.isAccepted =  participantList[j]["isaccepted"]  as? Bool
                                        participantObj.isDeclined =  participantList[j]["isdecline"] as? Bool
                                        participantObj.istrackingEnabled =  participantList[j]["istrackingEnabled"] as? Bool
                                        participantObj.trackingTime = participantList[j]["trackingTime"] as? String ??  ""
                                        participantObj.trackingStatus = participantList[j]["trackingStatus"] as? String ??  ""
                                        participantObj.isCapsuleLocked = participantList[j]["isCapsuleLocked"] as? Bool
                                        
                                        participantObj.isSeen = participantList[j]["isSeen"] as? Bool
                                        if participantObj.fisheyeId == self.fisheyeId{
                                            if participantObj.isSeen != true {
                                                meetIdArray.append(meetingObj.meetmeId as? String ?? "")
                                            }
                                        }
                                        
                                        participantObj.messageCount = participantList[j]["messageCount"] as? Int
                                        meetingObj.participantList.append(participantObj)
                                        if !participantObj.isHost! {
                                            meetingObj.participantListForCollection.append(participantObj)
                                        }
                                        if participantObj.isAccepted! {
                                            meetingObj.acceptedList.append(participantObj)
                                        } else if participantObj.isDeclined! {
                                            meetingObj.declinedList.append(participantObj)
                                        }
                                    }
                                }
                                self.meetList.append(meetingObj)
                            }
                            if meetIdArray.count > 0 {
                                self.reduceNotificationCountAndMarkSeen(countToReduce: meetIdArray.count, meetIdArray : meetIdArray)
                            }
                        }
                        if self.meetList.count != 0 {
                            DispatchQueue.main.async {
                                self.meetMeLogbookTableView.isHidden = false
                                self.noMeetingView.isHidden = true
                            }
                            DispatchQueue.main.async {
                                self.meetMeLogbookTableView.reloadData()
                                self.meetMeLogbookTableView.isUserInteractionEnabled = true
                            }
                        } else {
                            if searchKeyApiCall == ""{
                                DispatchQueue.main.async {
                                    self.meetMeLogbookTableView.isHidden = true
                                    self.noMeetingsLabel.isHidden = false
                                    self.noMeetingView.isHidden = false
                                    self.createMeetButton.isHidden = false
                                    self.noMeetingsImageView.isHidden = false
                                    self.noMeetingsLabel.text =  self.appService.noMeetingsLabel
                                }

                            } else {
                                DispatchQueue.main.async {
                                    self.noMeetingsLabel.text = self.appService.noMeetingsLabel2 + " \"\(searchKeyApiCall)\""
                                    self.noMeetingsLabel.isHidden = false
                                    self.meetMeLogbookTableView.isHidden = true
                                    self.noMeetingView.isHidden = false
                                    self.createMeetButton.isHidden = true
                                    self.noMeetingsImageView.isHidden = true
                                }
                            }
                        }
                        if upcomingFlagApiCall == "true"{
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "All Meetings", action: "All Meetings with Upcoming button clicked", label: "All Upcoming meetings loaded successfully", value: 0)
                        } else {
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "All Meetings", action: "All Meetings with Logbook button clicked", label: "All Meetings Logbook loaded successfully", value: 0)
                        }
                    } else {
                        DispatchQueue.main.async {
                            self.noMeetingsLabel.text = self.appService.noMeetingsLabel3
                            self.noMeetingsLabel.isHidden = false
                            self.meetMeLogbookTableView.isHidden = true
                            self.noMeetingView.isHidden = false
                            self.createMeetButton.isHidden = true
                            self.noMeetingsImageView.isHidden = true
                        }
                        if upcomingFlagApiCall == "true"{
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "All Meetings", action: "All Meetings with Upcoming button clicked", label: "All Upcoming meetings loading failed", value: 0)
                        } else {
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "All Meetings", action: "All Meetings with Logbook button clicked", label: "All Meetings Logbook loading failed", value: 0)
                        }
                    }
                }
            } else {
                if upcomingFlagApiCall == "true"{
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "All Meetings", action: "All Meetings with Upcoming button clicked", label: "All Upcoming meetings loading failed", value: 0)
                } else {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "All Meetings", action: "All Meetings with Logbook button clicked", label: "All Meetings Logbook loading failed", value: 0)
                }
                DispatchQueue.main.async {
                    self.noMeetingsLabel.text = self.appService.noMeetingsLabel3
                    self.noMeetingsLabel.isHidden = false
                    self.meetMeLogbookTableView.isHidden = true
                    self.noMeetingView.isHidden = false
                    self.createMeetButton.isHidden = true
                    self.noMeetingsImageView.isHidden = true
                    self.stopLoader()
                    self.enableAllButtons()
                }
            }

        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
    
    func getMyMeetings(upcomingFlagApiCall: String, searchKeyApiCall: String) {
        guard networkStatus.isNetworkReachable()
            else {

                DispatchQueue.main.async {
                    self.enableAllButtons()
                    self.stopLoader()
                    self.noMeetingsLabel.text = self.appService.checkInternetConnectionMessage
                    self.meetMeLogbookTableView.isHidden = true
                    self.noMeetingView.isHidden = false
                    self.noMeetingsLabel.isHidden = false
                    self.createMeetButton.isHidden = true
                }

                return
        }
        self.expandedRows.removeAll()
        self.disableAllButtons()
        self.startLoader()

        let awsURL = AppConfig.getMyMeetings(upcomingFlagApiCall: upcomingFlagApiCall, searchKeyApiCall: searchKeyApiCall.lowercased())
        guard let tempUrl = awsURL.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else { return }

        guard let URL = URL(string: tempUrl) else { return }
        let sessionConfig = URLSessionConfiguration.default

        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.httpMethod = AppConfig.httpMethodGet
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return }
        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                DispatchQueue.main.async {
                    self.stopLoader()
                    self.enableAllButtons()
                }
                if let meetingResponseObj = json as? [String: Any] {

                    let statusCode = meetingResponseObj["statusCode"]  as? String ??  ""

                    if statusCode == "200"{
                        if let meetingObjList = meetingResponseObj["meetings"] as? [[String: Any]] {
                            for meetingApiObj in meetingObjList {
                                var meetingObj =  MeetMeLogbookVariables()
                                meetingObj.meetmeId = meetingApiObj["meetmeId"]  as? String ??  ""
                                meetingObj.meetmeLocation = meetingApiObj["locationName"] as? String ??  ""
                                meetingObj.meetmeTitle = meetingApiObj["title"] as? String ??  ""
                                meetingObj.latitude = meetingApiObj["latitude"] as? String ??  ""
                                meetingObj.longitude = meetingApiObj["longitude"] as? String ??  ""
                                meetingObj.startDateTime = meetingApiObj["startDateTime"] as? Double
                                meetingObj.endDateTime = meetingApiObj["endDateTime"] as? Double
                                meetingObj.trackingTime = meetingApiObj["trackingTime"] as? Double
                                meetingObj.acceptedList = []
                                meetingObj.declinedList = []
                                meetingObj.participantList = []
                                meetingObj.participantListForCollection = []
                                if let participantList = meetingApiObj["participants"] as? [[String: Any]] {
                                    for j in 0..<participantList.count {
                                        let participantObj =  ParticipantVariables()
                                        participantObj.isHost =  participantList[j]["ishost"]  as? Bool
                                        participantObj.participantId =  participantList[j]["participantId"]  as? String ??  ""
                                        participantObj.fisheyeId =  participantList[j]["fisheyeId"]  as? String ??  ""
                                        participantObj.participantEmail =  participantList[j]["participantEmailId"]  as? String ??  ""
                                        participantObj.participantName =  participantList[j]["participantName"] as? String ??  ""
                                        participantObj.participantPhoneNumber=participantList[j]["participantPhoneNumber"] as? String ??  ""
                                        participantObj.participantPicture = participantList[j]["participantPicture"] as? String ??  ""
                                        participantObj.isAccepted =  participantList[j]["isaccepted"]  as? Bool
                                        participantObj.isDeclined =  participantList[j]["isdecline"] as? Bool
                                        participantObj.istrackingEnabled =  participantList[j]["istrackingEnabled"] as? Bool
                                        participantObj.trackingTime = participantList[j]["trackingTime"] as? String ??  ""
                                        participantObj.trackingStatus = participantList[j]["trackingStatus"] as? String ??  ""
                                        participantObj.isCapsuleLocked = participantList[j]["isCapsuleLocked"] as? Bool
                                        participantObj.messageCount = participantList[j]["messageCount"] as? Int
                                        meetingObj.participantList.append(participantObj)

                                        if !participantObj.isHost! {
                                            meetingObj.participantListForCollection.append(participantObj)
                                        }

                                        if participantObj.isAccepted! {
                                            meetingObj.acceptedList.append(participantObj)
                                        } else if participantObj.isDeclined! {
                                            meetingObj.declinedList.append(participantObj)
                                        }
                                    }
                                }
                                self.meetList.append(meetingObj)
                            }
                        }
                        if upcomingFlagApiCall == "true"{
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Meetings", action: "My Meetings with Logbook button clicked", label: "My Meetings Upcoming meetings loaded successfully", value: 0)
                        } else {
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Meetings", action: "My Meetings with Logbook button clicked", label: "My Meetings Logbook loaded successfully", value: 0)
                        }
                    } else {

                    }
                    if self.meetList.count != 0 {

                        DispatchQueue.main.async {
                            self.meetMeLogbookTableView.isHidden = false
                            self.noMeetingView.isHidden = true
                            self.meetMeLogbookTableView.reloadData()
                            self.meetMeLogbookTableView.isUserInteractionEnabled = true
                        }
                    } else {
                        if searchKeyApiCall == ""{
                            DispatchQueue.main.async {
                                self.meetMeLogbookTableView.isHidden = true
                                self.noMeetingView.isHidden = false
                                self.createMeetButton.isHidden = false
                                self.noMeetingsImageView.isHidden = false
                                self.noMeetingsLabel.isHidden = false
                               
                                self.noMeetingsLabel.text = self.appService.noMeetingsLabel
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.noMeetingsLabel.text = self.appService.noMeetingsLabel2+" \"\(searchKeyApiCall)\""
                                self.noMeetingsLabel.isHidden = false
                                self.meetMeLogbookTableView.isHidden = true
                                self.noMeetingView.isHidden = false
                                self.createMeetButton.isHidden = true
                                self.noMeetingsImageView.isHidden = true
                            }
                        }

                    }
                } else {
                    if upcomingFlagApiCall == "true"{
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Meetings", action: "My Meetings with Logbook button clicked", label: "My Upcoming Meetings loading failed", value: 0)
                    } else {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Meetings", action: "My Meetings with Logbook button clicked", label: "My Meetings Logbook loading failed", value: 0)
                    }
                    DispatchQueue.main.async {
                        self.noMeetingsLabel.text = self.appService.noMeetingsLabel3
                        self.noMeetingsLabel.isHidden = false
                        self.meetMeLogbookTableView.isHidden = true
                        self.noMeetingView.isHidden = false
                        self.createMeetButton.isHidden = true
                        self.noMeetingsImageView.isHidden = true
                    }
                }

            } else {
                if upcomingFlagApiCall == "true"{
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Meetings", action: "My Meetings with Logbook button clicked", label: "My Upcoming Meetings loading failed", value: 0)
                } else {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "My Meetings", action: "My Meetings with Logbook button clicked", label: "My Meetings Logbook loading failed", value: 0)
                }
                DispatchQueue.main.async {
                    self.noMeetingsLabel.text = self.appService.noMeetingsLabel3
                    self.noMeetingsLabel.isHidden = false
                    self.meetMeLogbookTableView.isHidden = true
                    self.noMeetingView.isHidden = false
                    self.createMeetButton.isHidden = true
                    self.noMeetingsImageView.isHidden = true
                    
                    self.stopLoader()
                    self.enableAllButtons()
                }
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }

    func changeCapsuleLock(selectedMeetmeIndex: Int, isCapsuleLockedRecieved: Bool, participantIndex: Int) {

        let selectedMeetMeIdForCapsuleLock = meetList[selectedMeetmeIndex].meetmeId
        let isCapsuleLocked = !isCapsuleLockedRecieved

        let parameters =
            [
                "isCapsuleLocked": isCapsuleLocked
        ] as [String: Any]

        let awsURL =  AppConfig.changeCapsuleLockUrl(selectedMeetmeId: selectedMeetMeIdForCapsuleLock!)
        guard let URL = URL(string: awsURL) else {
            return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPut
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json

                let responseStatus = data["statusCode"] as? String ?? ""
                if responseStatus == "200"{
                    let meetme = self.meetList[selectedMeetmeIndex]
                    meetme.participantList[participantIndex].isCapsuleLocked = isCapsuleLocked
                    if isCapsuleLocked {
                        DispatchQueue.main.async {
                            let indexLogbook = IndexPath(row: selectedMeetmeIndex, section: 0)
                            guard let cell = self.meetMeLogbookTableView.cellForRow(at: indexLogbook) as? MeetMeLogBookTableViewCell else {return}
                            DispatchQueue.main.async {
                                self.view.makeToast(self.appService.capsuleLocked)
                            }
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.appService.firstTimeInterval, execute: {
                                self.view.hideToast()
                            })
                            cell.capsuleLockImageView.image = UIImage(named: "CapsuleLocked")
                        }

                    } else {
                        DispatchQueue.main.async {
                            let indexLogbook = IndexPath(row: selectedMeetmeIndex, section: 0)
                            guard let cell = self.meetMeLogbookTableView.cellForRow(at: indexLogbook) as? MeetMeLogBookTableViewCell else {return}
                            cell.capsuleLockImageView.image = UIImage(named: "CapsuleUnlocked")
                            DispatchQueue.main.async {
                                self.view.makeToast(self.appService.capsuleUnlocked)
                            }
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.appService.firstTimeInterval, execute: {
                                self.view.hideToast()
                            })
                        }
                    }
                }

            } else {

            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }

    func getMessageCount(selectedMeetmeId: Int) {
        let meetmeId = meetList[selectedMeetmeId].meetmeId
        let awsURL = AppConfig.capsuleCountUrl(selectedMeetmeId: meetmeId!)
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodGet
        request.httpBody =  "".data(using: String.Encoding.utf8)

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, response: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json

                let response = data["statusCode"] as? String ?? ""
                if response == "200"{
                    let count = data["messageCount"] as? Int
                    DispatchQueue.main.async {

                            let indexLogbook = IndexPath(row: selectedMeetmeId, section: 0)
                            DispatchQueue.main.async {
                                guard let cell = self.meetMeLogbookTableView.cellForRow(at: indexLogbook) as? MeetMeLogBookTableViewCell else { return}
                                cell.capsuleNotificationNumber.text = String(count!)
                            }

                    }
                }
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }

    func fetchMeetingDetail() {
        var fetchedMeeting = MeetMeLogbookVariables()
        guard networkStatus.isNetworkReachable()
            else {
                DispatchQueue.main.async {
                    self.view.makeToast(self.appService.checkInternetConnectionMessage)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.appService.firstTimeInterval, execute: {
                    self.view.hideToast()
                })
                return
        }

        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

        let awsURL = AppConfig.updateMeetUrl(meetMeId: selectedMeetMeId)

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default

        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return }
        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                if let meetingResponseObj = json as? [String: Any] {
                    let responseCode = meetingResponseObj["statusCode"] as? String ?? ""
                    if responseCode == "200"{
                        if let meetingsList = meetingResponseObj["meetme"] as? [String: Any] {
                            fetchedMeeting.meetmeId = meetingsList["meetmeId"] as? String ??  ""
                            fetchedMeeting.meetmeLocation = meetingsList["locationName"] as? String ??  ""
                            fetchedMeeting.meetmeTitle = meetingsList["title"] as? String ??  ""
                            fetchedMeeting.latitude = meetingsList["latitude"] as? String ??  ""
                            fetchedMeeting.longitude = meetingsList["longitude"] as? String ??  ""
                            fetchedMeeting.startDateTime = meetingsList["startDateTime"] as? Double
                            fetchedMeeting.endDateTime = meetingsList["endDateTime"] as? Double
                            fetchedMeeting.trackingTime = meetingsList["trackingTime"] as? Double
                            fetchedMeeting.acceptedList = []
                            fetchedMeeting.declinedList = []
                            fetchedMeeting.participantList = []
                            fetchedMeeting.participantListForCollection = []
                            fetchedMeeting.notYetRespondedList = []
                            
                            // Hitesh
                            if ((meetingsList["isEventCreated"]) != nil) {
                                let tempEventFlag = meetingsList["isEventCreated"] as? Int ?? 0
                                if (tempEventFlag == 1){
                                    fetchedMeeting.isEventFlag = true
                                    self.isEventFlagDetail = true
                                } else {
                                    fetchedMeeting.isEventFlag = false
                                }
                            } else {
                                fetchedMeeting.isEventFlag = false
                            }

                            if let participantList = meetingsList["participants"] as? [[String: Any]] {
                                let participantObj1 =  ParticipantVariables()
                                let participantObj2 =  ParticipantVariables()
                                for k in 0..<participantList.count {
                                    
                                    if(participantList[k]["ishost"] as? Bool==true){
                                        participantObj1.isHost =  participantList[k]["ishost"] as? Bool
                                        participantObj1.participantId =  participantList[k]["participantId"] as? String ??  ""
                                        participantObj1.fisheyeId =  participantList[k]["fisheyeId"]as? String ??  ""
                                        participantObj1.participantEmail =  participantList[k]["participantEmailId"] as? String ??  ""
                                        participantObj1.participantName =  participantList[k]["participantName"] as? String ??  ""
                                        participantObj1.participantPhoneNumber = participantList[k]["participantPhoneNumber"] as? String ??  ""
                                        participantObj1.participantPicture = participantList[k]["participantPicture"] as? String ??  ""
                                        participantObj1.isAccepted =  participantList[k]["isaccepted"]as? Bool
                                        participantObj1.isDeclined =  participantList[k]["isdecline"]as? Bool
                                        participantObj1.istrackingEnabled =  participantList[k]["istrackingEnabled"]  as? Bool
                                        participantObj1.trackingTime = participantList[k]["trackingTime"]  as? String ??  ""
                                        participantObj1.trackingStatus = participantList[k]["trackingStatus"] as? String ??  ""
                                        participantObj1.isCapsuleLocked = participantList[k]["isCapsuleLocked"] as? Bool
                                        participantObj1.messageCount = participantList[k]["messageCount"] as? Int
                                    }
                                    if((participantList[k]["fisheyeId"]as? String ??  "")==(self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId")as? String ??  "")){
                                        participantObj2.isHost =  participantList[k]["ishost"] as? Bool
                                        participantObj2.participantId =  participantList[k]["participantId"] as? String ??  ""
                                        participantObj2.fisheyeId =  participantList[k]["fisheyeId"]as? String ??  ""
                                        participantObj2.participantEmail =  participantList[k]["participantEmailId"] as? String ??  ""
                                        participantObj2.participantName =  participantList[k]["participantName"] as? String ??  ""
                                        participantObj2.participantPhoneNumber = participantList[k]["participantPhoneNumber"] as? String ??  ""
                                        participantObj2.participantPicture = participantList[k]["participantPicture"] as? String ??  ""
                                        participantObj2.isAccepted =  participantList[k]["isaccepted"]as? Bool
                                        participantObj2.isDeclined =  participantList[k]["isdecline"]as? Bool
                                        participantObj2.istrackingEnabled =  participantList[k]["istrackingEnabled"]  as? Bool
                                        participantObj2.trackingTime = participantList[k]["trackingTime"]  as? String ??  ""
                                        participantObj2.trackingStatus = participantList[k]["trackingStatus"] as? String ??  ""
                                        participantObj2.isCapsuleLocked = participantList[k]["isCapsuleLocked"] as? Bool
                                        participantObj2.messageCount = participantList[k]["messageCount"] as? Int
                                    }
                                }
                                for j in 0..<participantList.count {
                                    let participantObj =  ParticipantVariables()
                                    participantObj.isHost =  participantList[j]["ishost"] as? Bool
                                    participantObj.participantId =  participantList[j]["participantId"] as? String ??  ""
                                    participantObj.fisheyeId =  participantList[j]["fisheyeId"]as? String ??  ""
                                    participantObj.participantEmail =  participantList[j]["participantEmailId"] as? String ??  ""
                                    participantObj.participantName =  participantList[j]["participantName"] as? String ??  ""
                                    participantObj.participantPhoneNumber = participantList[j]["participantPhoneNumber"] as? String ??  ""
                                    participantObj.participantPicture = participantList[j]["participantPicture"] as? String ??  ""
                                    participantObj.isAccepted =  participantList[j]["isaccepted"]as? Bool
                                    participantObj.isDeclined =  participantList[j]["isdecline"]as? Bool
                                    participantObj.istrackingEnabled =  participantList[j]["istrackingEnabled"]  as? Bool
                                    participantObj.trackingTime = participantList[j]["trackingTime"]  as? String ??  ""
                                    participantObj.trackingStatus = participantList[j]["trackingStatus"] as? String ??  ""
                                    participantObj.isCapsuleLocked = participantList[j]["isCapsuleLocked"] as? Bool
                                    participantObj.messageCount = participantList[j]["messageCount"] as? Int
                                    
                                    if (self.isEventFlagDetail) {
                                        if( participantObj1.fisheyeId  == participantObj2.fisheyeId ) {
                                            fetchedMeeting.participantList.append(participantObj)
                                            if !participantObj.isHost! {
                                                fetchedMeeting.participantListForCollection.append(participantObj)
                                            }
                                            if participantObj.isAccepted! {
                                                fetchedMeeting.acceptedList.append(participantObj)
                                            } else if participantObj.isDeclined! {
                                                fetchedMeeting.declinedList.append(participantObj)
                                            } else if !participantObj.isAccepted! && !participantObj.isDeclined! && !participantObj.isHost! {
                                                fetchedMeeting.notYetRespondedList.append(participantObj)
                                            }
                                        } else{
                                            if (fetchedMeeting.participantListForCollection.count < 1){
                                                fetchedMeeting.participantListForCollection.append(participantObj1)
                                                fetchedMeeting.participantList.append(participantObj1)
                                                fetchedMeeting.hostList.append(participantObj1)
                                                
                                                
                                            }
                                            
                                        } 
                                        
                                    } else if (fetchedMeeting.isEventFlag == false) {
                                        fetchedMeeting.participantList.append(participantObj)
                                        
                                        if !participantObj.isHost! {
                                            fetchedMeeting.participantListForCollection.append(participantObj)
                                        }
                                        if participantObj.isHost! {
                                            fetchedMeeting.hostList.append(participantObj)
                                        }
                                        if participantObj.isAccepted! {
                                            fetchedMeeting.acceptedList.append(participantObj)
                                        } else if participantObj.isDeclined! {
                                            fetchedMeeting.declinedList.append(participantObj)
                                        } else if !participantObj.isAccepted! && !participantObj.isDeclined! && !participantObj.isHost! {
                                            fetchedMeeting.notYetRespondedList.append(participantObj)
                                        }
                                    }
                                    
                                }
                            }
                        }
                        DispatchQueue.main.async {

                            let count = self.meetList.count

                            if  count > self.selectedMeetMeIndex {
                                guard let boo = self.meetList[self.selectedMeetMeIndex] as?  MeetMeLogbookVariables else {
                                    return
                                }
                                self.meetList[self.selectedMeetMeIndex] = fetchedMeeting
                            }

                            self.setDetail(fetchedMeeting: fetchedMeeting)

                            self.updateLocation()

                            self.scheduleTimer()
                        }
                    } else {

                    }
                }
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }

}

extension MeetMeLogBook: MakeUnreadCapsuleCountZero {
    func makeCountZero(isToMakeCountZero: Bool) {
        if self.meetList.count > self.selectedMeetIndexToSeeCapsule{
            if self.meetList[self.selectedMeetIndexToSeeCapsule].meetmeId == self.selectedMeetIdToSeeCapsule{
                if isToMakeCountZero{
                    let meet = self.meetList[self.selectedMeetIndexToSeeCapsule]
                    for k in 0..<meet.participantList.count{
                        if meet.participantList[k].fisheyeId == self.fisheyeId{
                            meet.participantList[k].messageCount = 0
                        }
                    }
                    let indexPath = IndexPath(row: self.selectedMeetIndexToSeeCapsule, section: 0)
                    guard let cell = meetMeLogbookTableView.cellForRow(at: indexPath ) as? MeetMeLogBookTableViewCell
                        else { return }
                    cell.capsuleNotificationNumber.text = String(0)
                }
            }
        }
    }
}


