//
//  MeetMeNotificationTableCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 02/04/18.
//  Copyright © 2018 Rushikesh. All rights reserved.
//

import UIKit

class MeetMeNotificationTableCell: UITableViewCell {

    @IBOutlet weak var completeNotificationView: UIView!
    @IBOutlet weak var meetingInviteStatusView: UIView!
    @IBOutlet weak var hostImageView: UIImageView!
    @IBOutlet weak var meetTitleLabel: UILabel!
    @IBOutlet weak var meetAddressLabel: UILabel!
    @IBOutlet weak var notificationDate: UILabel!
    @IBOutlet weak var notificationTime: UILabel!
    @IBOutlet weak var notificationTypeImageView: UIImageView!

    @IBOutlet weak var capsuleLbl: UILabel!
    @IBOutlet weak var capsuleCountView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
