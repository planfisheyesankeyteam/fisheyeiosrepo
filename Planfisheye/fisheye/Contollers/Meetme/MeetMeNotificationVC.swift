//
//  MeetMeNotificationVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 02/04/18.
//  Copyright © 2018 Rushikesh. All rights reserved.
//

import UIKit

class MeetMeNotificationVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

      @IBOutlet weak var meetmeNotificationCompleteView: UIView!
      @IBOutlet weak var appBarView: UIView!
      @IBOutlet weak var backButtonImageView: UIImageView!
      @IBOutlet weak var notificationTitleLabel: UILabel!
      @IBOutlet weak var backButton: UIButton!
      @IBOutlet weak var notificationTableView: UITableView!
      @IBOutlet weak var noNotificationsLabel: UILabel!
      @IBOutlet weak var noNotificationsView: UIView!

      var meetMeNotifications: [MeetmMeNotificationVariables] = []
      var validationController = ValidationController()
      let appSharedPrefernce = AppSharedPreference()
      var appService = AppService.shared
      var idtoken = ""

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return meetMeNotifications.count
      }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = Bundle.main.loadNibNamed("MeetMeNotificationTableCell", owner: self, options: nil)?.first as! MeetMeNotificationTableCell
            let notificationObj = meetMeNotifications[indexPath.row]
            cell.meetTitleLabel.text = notificationObj.meetmeTitle
            cell.meetAddressLabel.text = notificationObj.meetmeAddress
            cell.hostImageView?.sd_setImage(with: URL(string: notificationObj.imageString ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            cell.hostImageView.layer.cornerRadius = cell.hostImageView.frame.size.width / 2
            cell.hostImageView.clipsToBounds = true

            cell.notificationTime.text = validationController.convert_time_from_UTC_time(givenUTCtime: notificationObj.meetmeNotificationDateTime!)
            if notificationObj.notificationType == "INVITE"{
                  cell.notificationTypeImageView.isHidden = true
            } else {
                  cell.notificationTypeImageView.isHidden = false
            }
            let givenDate = validationController.convert_date_from_UTC_time(givenUTCdate: notificationObj.meetmeNotificationDateTime!)
            let todaysDate = validationController.getTodaysDate()
            let differenceInDate = validationController.calculateDaysBetweenTwoDates(start: todaysDate, end: givenDate)
            switch (differenceInDate) {
            case 0:
                  cell.notificationDate.text = self.appService.meetDateLabel
                  break
            case -1:
                  cell.notificationDate.text = self.appService.yesterday
                  break
            default:
                  cell.notificationDate.text = givenDate
                  break
            }
        
            cell.capsuleCountView.isHidden = true
            cell.capsuleCountView.layer.cornerRadius = cell.capsuleCountView.frame.width / 2
            let capsuleCount = notificationObj.unreadCapsuleCount as? Int ?? 0
            if capsuleCount > 0 {
                cell.capsuleCountView.isHidden = false
                cell.capsuleLbl.text = String(capsuleCount)
            }
            let tapGesture = TapGestureRecognizer(target: self, action: #selector(openMeetmeDetailVC(sender:)))
            tapGesture.row = indexPath.row
            tapGesture.section = indexPath.section
            cell.addGestureRecognizer(tapGesture)

            return cell
      }

      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            notificationTableView.delegate = self
            self.geetMeetmeNotifications()
            seenMeetNotifications()
            setLocalizationText()
            // Do any additional setup after loading the view.
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

    @objc func openMeetmeDetailVC(sender: Any) {
            let gesture = sender as! TapGestureRecognizer
            let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
            let meetMeBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "MeetMeDetailVC") as! MeetMeDetailVC
            meetMeBaseViewcontroller.selectedMeetmeId = self.meetMeNotifications[gesture.row!].meetmeId!
            meetMeBaseViewcontroller.hasOpenedContacts = false
            meetMeBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(meetMeBaseViewcontroller)
            self.view.addSubview(meetMeBaseViewcontroller.view)

            UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: {
                  meetMeBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
      }

      @IBAction func onBackButtonTapped(_ sender: Any) {
            self.view.removeFromSuperview()
      }

      func startLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }

      }

      func stopLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.stopLoader()
            }

      }

      func  setLocalizationText() {

            notificationTitleLabel.text =  self.appService.meetMeNotification
            noNotificationsLabel.text = self.appService.noNotification
      }
}

extension MeetMeNotificationVC {
      func geetMeetmeNotifications() {
            DispatchQueue.main.async {
                self.startLoader()
            }

            let awsURL = AppConfig.meetMeNotificationsUrl
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default

            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            let idtoken  = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            request.httpMethod = AppConfig.httpMethodGet
            request.httpBody = "".data(using: String.Encoding.utf8)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(idtoken, forHTTPHeaderField: "idtoken")
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        DispatchQueue.main.async {
                            self.stopLoader()
                        }
                    
                        let responseCode = json["statusCode"] as? String ??  ""
                        if responseCode == "200"{
                              if let notificationList = json["meetMeNotifications"] as? [[String: Any]] {
                                    for i in 0..<notificationList.count {
                                          if notificationList.count>0 {
                                                let notificationObj =  MeetmMeNotificationVariables()
                                                notificationObj.meetmeId = notificationList[i]["meetmeId"]  as? String ??  ""
                                                notificationObj.meetmeAddress = notificationList[i]["meetLocation"] as? String ??  ""
                                                notificationObj.meetmeTitle = notificationList[i]["meetTitle"] as? String ??  ""
                                                notificationObj.meetmeNotificationDateTime = notificationList[i]["notificationDateandTime"] as? Double
                                                notificationObj.notificationType = notificationList[i]["notificationType"] as? String ??  ""
                                                notificationObj.imageString = notificationList[i]["image"] as? String ??  ""
                                                notificationObj.unreadCapsuleCount = notificationList[i]["unreadCapsuleCount"] as? Int ??  0
                                            
                                            if self.meetMeNotifications.filter({$0.meetmeId == notificationObj.meetmeId && $0.notificationType == "CAPSULE"}).count > 0{
                                                self.meetMeNotifications.filter({$0.meetmeId == notificationObj.meetmeId && $0.notificationType == "CAPSULE"}).first?.unreadCapsuleCount = (self.meetMeNotifications.filter({$0.meetmeId == notificationObj.meetmeId && $0.notificationType == "CAPSULE"}).first?.unreadCapsuleCount as? Int ?? 0) + 1
                                            }else{
                                                self.meetMeNotifications.append(notificationObj)                                          }
                                            
                                          }
                                    }
                                
                                    DispatchQueue.main.async {
                                          if self.meetMeNotifications.count>0 {
                                                self.notificationTableView.isHidden = false
                                                self.notificationTableView.reloadData()
                                                self.noNotificationsView.isHidden = true

                                          } else {
                                                
                                                self.noNotificationsView.isHidden = false
                                                self.notificationTableView.isHidden = true
                                          }
                                    }
                              }
                        }

                  } else {
                        DispatchQueue.main.async {
                            self.stopLoader()
                        }
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }

      func seenMeetNotifications() {
            let awsURL = AppConfig.meetMeNotificationsUrl
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            request.httpBody = "".data(using: String.Encoding.utf8)

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    if let data = jsonOpt {
                        let count = data["userMeetMeNotifications"] as? Int ?? 0
                        AppService.shared.noOfUnreadMeetNotifications = count
                        NotificationCenter.default.post(name: REFRESH_MEET_NOTIFICATION_COUNT, object: nil)
                    }
                  } else {

                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }
}
