//
//  BaseForParticipantView.swift
//
//
//  Created by Keerthi Chinivar on 09/01/17.
//  Copyright © 2017 TA. All rights reserved.
//

import UIKit

@objc protocol ParticipantViewDelegate {
    func cellTappedAt(index: Int)
}

class Participant {

    var image: UIImage?
    let title: String?

    init(imageStr: String, title: String ) {
    self.image = UIImage.init(named: imageStr)
    self.title = title
    }

}

class BaseForParticipantView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    lazy var baseCollectionView: UICollectionView  = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: self.bounds, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.isScrollEnabled = true
        return cv
    }()

    weak var participantViewDelegate: ParticipantViewDelegate?

    var selectedIndex: Int = 0

    var inputData: [Participant]!

    let kParticipantCell = "menuBarCollectionViewCell"

    init(frame: CGRect, dataSource: [Participant] ) {
        super.init(frame: frame)
        self.inputData = dataSource
        //self.baseCollectionView.register(UINib.init(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: kCollectionViewCell)
        self.baseCollectionView.register(UINib.init(nibName: "MenuBarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: kParticipantCell)
        self.baseCollectionView.backgroundColor = UIColor.clear
        //baseCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 44, 0)
        //baseCollectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 40, 0)
        self.addSubview(baseCollectionView)

        //        let selectedIndexPath = IndexPath(item: selectedIndex, section: 0)

        //        baseCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width/4, height: frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return inputData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kParticipantCell, for: indexPath) as! MenuBarCollectionViewCell

        cell.imageView.image = self.inputData[indexPath.row].image

        cell.imageView.layer.cornerRadius = 5.0

        cell.titleLabel.text  =  self.inputData[indexPath.row].title

        cell.titleLabel.textColor = UIColor.black
        //        if indexPath.row == 0 {
        //            cell.isHighlighted = true
        //        }

        cell.backgroundColor = UIColor.clear

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        guard let method = participantViewDelegate?.cellTappedAt(index: indexPath.item) else {
            return
        }
        method
    }

    // MARK: UICollectionViewDelegate

    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */

}
