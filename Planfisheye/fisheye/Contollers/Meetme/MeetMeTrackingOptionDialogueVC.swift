//
//  MeetMeTrackingOptionDialogueVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 09/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation

class MeetMeTrackingOptionDialogueVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var dialogueView: UIView!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var pickerViewTracker: UIPickerView!
    @IBOutlet weak var pickerViewTime: UIPickerView!
    @IBOutlet weak var lastView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var trackingOptionUnderlineImageView: UIImageView!
    @IBOutlet weak var timeOptionsUnderlineImageView: UIImageView!

    @IBOutlet var StartTrackingTime: UILabel!
    @IBOutlet var chooseTrackingOption: UILabel!
    //    @IBOutlet weak var selectModeOFTransport: UICustomTextField!

    @IBOutlet weak var selectModeOFTransport: UILabel!

    @IBOutlet weak var modeTableView: UITableView!

    @IBOutlet var cancel: UILabel!
    @IBOutlet var mode: UILabel!
    @IBOutlet weak var SelectModeOfTrasnportbtn: UIButton!

    @IBOutlet var ok: UILabel!
    @IBOutlet var time: UILabel!
    var modeOfTransportArray = ["Walking", "Driving"]
    var englishmodeArray: [String] = []
    var selectmodeArray: Int = 1
   let modeofTransportDropdown = DropDown()

    var acceptedDelegate: AcceptMeet?
    let trackingOptionsDropdown: DropDown = DropDown()
    let trackingTimeOptionsDropdown: DropDown = DropDown()
    var appService = AppService.shared
    var discloseTrailData: [String] = []
    var discloseTrailTimeData: [String] = []
    var discloseMode: [String] = []
    var trackingTimeToSend = "5"
    var discloseTrailSelectedOption = "Before the meet"
    var minutesForTrail = "5"
    var trackingOptionSelectedText:String!
    var discloseTrailEnabled = true
    var selectedTrailOptionIndex = 0
    var selectedTrailTimeIndex = 0
    var selectedText:String!
    var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        setLocalizationText()
        self.englishmodeArray = self.modeOfTransportArray
        initializeUIComponents()
        initializeMode()

    }

    func setLocalizationText() {
      DispatchQueue.main.async {
            self.chooseTrackingOption.text = self.appService.chooseYourTrackingOption
            self.StartTrackingTime.text = self.appService.startTracking
            self.mode.text = self.appService.mode
            self.time.text = self.appService.time
            self.cancel.text = self.appService.cancel
            self.ok.text = self.appService.ok
      }
    }

    @IBAction func modeOfTransportButtonTappedbtn(_ sender: Any) {
        if  modeofTransportDropdown.isHidden {
            self.modeofTransportDropdown.show()
        } else {
            self.modeofTransportDropdown.hide()
        }
    }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.MeetMeTrackingOptionDialogueVC)
      }

    func initializeUIComponents() {

        dialogueView.layer.cornerRadius = 10
        self.timeLabel.text =  "5"+" "+self.signupmsg.minutes
        self.trackLabel.text = MeetMeTrackingOptions.trackingOption[0].optionTranslatedName
      
      if appService.localTransportModeArray[0].actualText != nil
      {
            self.selectedText = appService.localTransportModeArray[0].actualText
      }else{
            self.selectedText = appService.trasportModeArray[0].actualText
      }
        pickerViewTracker.delegate = self
        pickerViewTime.delegate = self
        pickerViewTracker.backgroundColor = UIColor.lightPurple()
        pickerViewTime.backgroundColor = UIColor.lightPurple()
      self.selectModeOFTransport.text = MeetMeTrackingOptions.trackingMode[0]
      self.discloseTrailData = MeetMeTrackingOptions.trackingOption.map { $0.optionTranslatedName }
        trackingOptionsDropdown.anchorView = trackingOptionUnderlineImageView
        trackingOptionsDropdown.dataSource = discloseTrailData
        self.trackingTimeOptionsDropdown.anchorView = self.timeOptionsUnderlineImageView
      if MeetMeTrackingOptions.trackingOption.count > 0 {
            MeetMeTrackingOptions.trackingOption[0].trackingTime.forEach {vars in
                self.discloseTrailTimeData.append(String(vars as! Int) + " " + self.signupmsg.minutes)
            }
        }
        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
        trackingOptionsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.trackLabel.text = self.discloseTrailData[index]
            
            let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionTranslatedName == item ) } )
            
            if objArray != nil && objArray.count > 0
            {
                    self.discloseTrailSelectedOption = objArray[0].optionName
                     self.trackingOptionSelectedText = objArray[0].optionName
            }else{
                  self.discloseTrailSelectedOption = self.discloseTrailData[0]
                  self.trackingOptionSelectedText = objArray[0].optionName
            }
            
            if  MeetMeTrackingOptions.trackingOption[index].isTimeRequired! {

           self.middleView.isHidden = false
            } else {
                 self.middleView.isHidden = true

            }
            switch index {
            case 0:

                self.discloseTrailTimeData = []
                MeetMeTrackingOptions.trackingOption[0].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + " " + self.signupmsg.minutes)
                }
                 self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                
                self.selectedTrailOptionIndex = 0
                self.trackingTimeToSend = "5"
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            case 1:
                self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                
                self.discloseTrailEnabled = true
                self.discloseTrailTimeData = []
                self.selectedTrailOptionIndex = 1
                 self.trackingTimeToSend = "5"
                MeetMeTrackingOptions.trackingOption[1].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + " " + self.signupmsg.minutes)
                }
                
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            case 2:
                self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                
                self.minutesForTrail =  "0"
                self.discloseTrailEnabled = true

                self.selectedTrailOptionIndex = 2
                self.discloseTrailTimeData = []
                MeetMeTrackingOptions.trackingOption[2].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + " " + self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            case 3:
                self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                
                self.minutesForTrail =  "0"
                self.discloseTrailEnabled = false
                self.selectedTrailOptionIndex = 3
                self.discloseTrailTimeData = []
                
                MeetMeTrackingOptions.trackingOption[3].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + " " + self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break

            default:
                self.discloseTrailSelectedOption = self.discloseTrailData[index]
                
                self.discloseTrailEnabled = true
                self.selectedTrailOptionIndex = index
                self.discloseTrailTimeData = []

                 self.trackingTimeToSend = "5"
                MeetMeTrackingOptions.trackingOption[index].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + " " + self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            }
        }

        trackingTimeOptionsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
             self.timeLabel.text = self.discloseTrailTimeData[index]
            

            self.trackingTimeToSend = String(MeetMeTrackingOptions.trackingOption[self.selectedTrailOptionIndex].trackingTime[index] as! Int)
        }

    }
    func initializeMode() {
        self.modeOfTransportArray = MeetMeTrackingOptions.trackingMode
    
        self.modeTableView.isHidden = true

        self.modeofTransportDropdown.anchorView = self.modeTableView
        self.modeofTransportDropdown.dataSource = self.modeOfTransportArray
        self.modeofTransportDropdown.width = 200
        self.modeofTransportDropdown.selectionAction = {
                [unowned self] (index: Int, item: String) in
                self.selectModeOFTransport.text = item
                self.selectmodeArray = index
          
            let objArray = self.appService.localTransportModeArray.filter( { return ($0.translatedText == item ) } )
            
            if objArray != nil && objArray.count > 0
            {
                  self.selectModeOFTransport.text = objArray[0].translatedText
                  self.selectedText = objArray[0].actualText
            } else{
                  self.selectModeOFTransport.text = objArray[0].translatedText
                  self.selectedText = objArray[0].actualText
            }
      }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectModeOFTransport.text = self.modeOfTransportArray[indexPath.row]
        //            self.genderTextField.font = UIFont(name: "Montserrat-Regular", size: 16)
        self.modeTableView.isHidden = true
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case pickerViewTracker :
            return discloseTrailData[row]

        case pickerViewTime :
            return discloseTrailTimeData[row]

        default:
            return "none"
        }
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
                switch pickerView {
        case pickerViewTracker :
            return discloseTrailData.count

        case pickerViewTime :
            return discloseTrailTimeData.count

        default:
            return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if(pickerView == pickerViewTracker) {
            trackLabel.text = discloseTrailData[row]
           
            pickerViewTracker.isHidden = true

            middleView.isHidden = false
        } else if(pickerView == pickerViewTime) {
            timeLabel.text = discloseTrailTimeData[row]
            
            pickerViewTime.isHidden = true
            switch(row) {
            case 0:
                trackingTimeToSend = "5"
                break
            case 1:
                trackingTimeToSend = "10"
                break
            case 2:
                trackingTimeToSend = "15"
                break
            default:
                break
            }
        }

    }

    @IBAction func dropDownButtonTappedForTracking(_ sender: Any) {

        if trackingOptionsDropdown.isHidden {
            trackingOptionsDropdown.show()
        } else {
            trackingOptionsDropdown.hide()
        }
    }

    @IBAction func dropDownButtonTappedForTime(_ sender: Any) {
        if trackingTimeOptionsDropdown.isHidden {
            trackingTimeOptionsDropdown.show()
        } else {
            trackingTimeOptionsDropdown.hide()
        }

    }
    @IBAction func onOKButtonTapped(_ sender: Any) {
        let status = CLLocationManager.authorizationStatus()
        switch status {
            
        case .authorized, .authorizedWhenInUse, .authorizedAlways :
            print("Calling Meet Accepted Function")
            acceptedDelegate?.meetAccepted(discloseTrail: discloseTrailSelectedOption, discloseTrailTime: trackingTimeToSend, transportMode: selectedText)
            
            self.dismiss(animated: true, completion: nil)
        case .denied,.notDetermined:
            let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                alert.dismiss(animated: true, completion: nil)
            })
            present(alert, animated: true)
            break
        default :
//            self.stopLoader()
            break
        }
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func crossButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
