//
//  MeetMeParticipantsCollectionViewCell.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 06/02/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class MeetMeParticipantsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var participantsImage: UIImageView!
    @IBOutlet weak var participantsName: UILabel!
    @IBOutlet weak var acceptedSign: UIImageView!
    @IBOutlet weak var rejectedSign: UIImageView!
    @IBOutlet weak var participantButton: UIButton!
}
