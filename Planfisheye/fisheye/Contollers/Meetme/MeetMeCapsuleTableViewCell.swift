//
//  MeetMeCapsuleTableViewCell.swift
//  fisheye
//
//  Created by SankeyProUser on 08/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class MeetMeCapsuleTableViewCell: UITableViewCell {

    @IBOutlet weak var participantChatBubbleImgView: UIImageView!
    @IBOutlet weak var participantChatBubble: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   /* {
    self.participantChatBubbleImgView.layer.cornerRadius = 10
    
    //Designing the Chat Bubbles for participant
    let maskPathParticipant = UIBezierPath(roundedRect: participantChatBubble.bounds,
                                           byRoundingCorners: [.bottomLeft, .bottomRight, .topRight],
                                           cornerRadii: CGSize(width: 10.0, height: 10.0))
    
    let shapeParticipant = CAShapeLayer()
    shapeParticipant.path = maskPathParticipant.cgPath
    participantChatBubble.layer.mask = shapeParticipant
    }*/

}
