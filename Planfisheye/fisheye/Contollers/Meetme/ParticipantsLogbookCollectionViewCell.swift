//
//  ParticipantsLogbookCollectionViewCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 25/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class ParticipantsLogbookCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var participantCollectionCompleteView: UIView!
    @IBOutlet weak var participantImageView: UIImageView!
    @IBOutlet weak var participantNameLabel: UILabel!
    @IBOutlet weak var participantStatusImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

}
