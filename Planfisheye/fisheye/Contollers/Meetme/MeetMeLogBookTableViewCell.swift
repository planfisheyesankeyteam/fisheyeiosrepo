//
//  MeetMeLogBookTableViewCell.swift
//  fisheye
//
//  Created by Sankey Solution on 12/7/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit

class MeetMeLogBookTableViewCell: UITableViewCell {

    //Main notification outlet variables

    @IBOutlet weak var completeNotificationView: UIView!
    @IBOutlet weak var mainNotificationView: UIView!
    @IBOutlet weak var hostImageView: UIImageView!
    @IBOutlet weak var meetTitleLabel: UILabel!
    @IBOutlet weak var meetAddressLabel: UILabel!
    @IBOutlet weak var acceptImage: UIImageView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineImage: UIImageView!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var meetTimeLabel: UILabel!
    @IBOutlet weak var meetDateLabel: UILabel!
    @IBOutlet weak var meetDeleteIcon: UIImageView!

    //Extended notification outlet variables
    @IBOutlet weak var extendedNotificationView: UIView!
    @IBOutlet weak var meetMeLogBookMapView: MKMapView!
    @IBOutlet weak var notificationCapsuleView: UIView!

    @IBOutlet weak var capsuleLockImageView: UIImageView!
    @IBOutlet weak var capsuleLockButton: UIButton!
    @IBOutlet weak var extendedViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var completeNotificationViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var participantCollectionView: UICollectionView!
    @IBOutlet weak var extendedViewDateTime: UILabel!

    @IBOutlet weak var unlockCapsuleLabel: UILabel!
    @IBOutlet weak var earliestTrackingLabel: UILabel!
    @IBOutlet weak var trackingTimeLabel: UILabel!
    @IBOutlet weak var trackingStatus: UILabel!
    @IBOutlet weak var unlockCapsuleImage: UIImageView!
    @IBOutlet weak var capsuleNotificationNumber: UILabel!
    @IBOutlet weak var unlockCapsuleButton: UIButton!
    @IBOutlet weak var inviteStatusView: UIView!
    @IBOutlet weak var zoomInButton: UIButton!
    @IBOutlet weak var zoomOutButton: UIButton!

    var isHoster: Bool = false
    var isExpanded: Bool = false {
        didSet {
           
            if !isExpanded {
                self.completeNotificationViewHeightConstraint.constant = 63.0
                self.extendedViewHeightConstraint.constant = 0.0
                self.extendedNotificationView.isHidden = true
                //                  self.meetDeleteIcon.isHidden = true
                //                self.acceptImage.isHidden = true
                //                self.declineImage.isHidden = true
                //                self.declineButton.isHidden = true
                self.acceptButton.isHidden = false
                self.meetTimeLabel.isHidden = true
                self.mainNotificationView.backgroundColor = UIColor.white
                if !isHoster {
                    self.acceptImage.isHidden = false
                    self.declineImage.isHidden = false
                    self.declineButton.isHidden = false
                } else {
                    self.acceptImage.isHidden = true
                    self.declineImage.isHidden = true
                    self.declineButton.isHidden = true
                    self.meetDeleteIcon.isHidden = false
                }
                inviteStatusView.isHidden = false
            } else {

                self.completeNotificationViewHeightConstraint.constant = 352.0
                self.extendedViewHeightConstraint.constant = 289.0
                self.extendedNotificationView.isHidden = false
                self.acceptButton.isHidden = false
                self.meetTimeLabel.isHidden = true
                inviteStatusView.isHidden = true
                self.mainNotificationView.backgroundColor = UIColor.newBlue()
                if !isHoster {
                    self.acceptImage.isHidden = false
                    self.declineImage.isHidden = false
                    self.declineButton.isHidden = false
                } else {
                    self.acceptImage.isHidden = true
                    self.declineImage.isHidden = true
                    self.declineButton.isHidden = true
                    self.meetDeleteIcon.isHidden = false
                }

            }
          
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int) {

        participantCollectionView.delegate = dataSourceDelegate
        participantCollectionView.dataSource = dataSourceDelegate
        participantCollectionView.tag = row
        participantCollectionView.reloadData()
    }

    @IBAction func zoomOutButtonTapped(_ sender: Any) {
        var region: MKCoordinateRegion? = meetMeLogBookMapView?.region
        let latitude = min((region?.span.latitudeDelta)! * 2.0, 180.0)
        let longitude = min((region?.span.longitudeDelta)! * 2.0, 180.0)
        region?.span.latitudeDelta = latitude
        region?.span.longitudeDelta = longitude
        meetMeLogBookMapView?.setRegion(region!, animated: true)
    }

    @IBAction func zoomInButtonTapped(_ sender: Any) {
        var region: MKCoordinateRegion? = meetMeLogBookMapView?.region
        region?.span.latitudeDelta /= (2.0 as  CLLocationDegrees)
        region?.span.longitudeDelta /= (2.0 as  CLLocationDegrees)
        meetMeLogBookMapView?.setRegion(region!, animated: true)
    }

}
