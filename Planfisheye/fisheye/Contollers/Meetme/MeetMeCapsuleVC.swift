//
//  MeetMeCapsuleVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 06/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift
import IQKeyboardManagerSwift

class MeetMeCapsuleVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var capsuleChatCollectionView: UICollectionView!

    @IBOutlet weak var noCapsuleView: UIView!
    @IBOutlet weak var noCaspuleImageView: UIImageView!
    @IBOutlet weak var noCapsuleLabel: UILabel!
    
    static let shared: MeetMeCapsuleVC = MeetMeCapsuleVC()
    let networkStatus = NetworkStatus()
    var appService = AppService.shared
    let appSharedPrefernce = AppSharedPreference()
    let validationController = ValidationController()
//    let connectivity  =  Connectivity()
    let now = Date()
    let formatter = DateFormatter()
    let messageSync = MessageSync()
    let message = MeetMeMessage()
    let reachability = Reachability()!
    private let cellId = "cellId"
    var messages: [MeetMeMessage] = []
    var selectedMeetmeId = ""
    var meetmeTitle: String?
    var meetmeParticipantCount : Int?
    var capsule: MeetMeCapsule?
    var timer: Timer?
    var bottomConstraint: NSLayoutConstraint?
    var bottomConstraint2: NSLayoutConstraint?
    var fisheyeId = ""
    var newMessage = ""
    var idtoken = ""
    var obj: [String: Any]?
    var profileImageUrl = ""
    var profilename = ""
    var profileId = ""
    var capsuleStatuses: [String] = [AppService.shared.undelivered,AppService.shared.sent, AppService.shared.readBy,AppService.shared.readByall]
      
    var capsuleStatusColors: [String] = ["red", "blue", "yellow","green"]
    var makeUnreadCapsuleCountZero: MakeUnreadCapsuleCountZero?
    var isCapsuleFetched: Bool = false
    var makeUnreadCapsuleCountZeroOnDetails: MakeUnreadCapsuleCountZeroOnMeetDetails?
    var isFromDetails: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.appService = AppService.shared
        fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
        idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.fetchMeetmeCapsule()
        capsuleChatCollectionView?.backgroundColor = UIColor.white
        capsuleChatCollectionView?.register(ChatLogMessageCell.self, forCellWithReuseIdentifier: cellId)
        capsuleChatCollectionView?.alwaysBounceVertical = true
        view.addSubview(messageInputContainerView)
        view.addConstraintsWithFormat("H:|[v0]|", views: messageInputContainerView)
        view.addConstraintsWithFormat("V:[v0(48)]", views: messageInputContainerView)
        IQKeyboardManager.sharedManager().enable = false

        setupInputComponents()

        self.inputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        scheduleTimer()

        self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
        
        if let tempObj = self.obj {
            self.profileImageUrl = tempObj["picture"] as? String ?? ""
            self.profilename = tempObj["name"] as? String ?? ""
            self.profileId = tempObj["id"] as? String ?? ""
        }
        self.addObservers()
    }

    func addObservers(){
        NotificationCenter.default.addObserver(forName: SEND_UNSEND_MESSAGES, object: nil, queue: nil) { (_) in
            self.hideView()
        }
    }
    
    func hideView(){
        self.noCapsuleView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.MeetMeCapsuleVC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.sharedManager().enable = true
    }
    
    let messageInputContainerView: UIView = {
        let view = UIView()
         view.backgroundColor = UIColor.white
        return view
    }()

    let inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = AppService.shared.enterMessage
        textField.font = UIFont(name: "Montserrat-Regular", size: 14)
        return textField
    }()

    let appBarView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        return uiView
    }()
    
    let messageStatusNotationView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        return uiView
    }()
    
    let readBySomeoneStatusView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        return uiView
    }()
    
    let deliveredStatusView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        return uiView
    }()
    
    let sentStatusView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        return uiView
    }()
    
    let unsentStatusView: UIView = {
        let uiView = UIView()
        uiView.backgroundColor = UIColor.white
        return uiView
    }()

    let separationView: UIView = {
        let separationView = UIView()
        separationView.backgroundColor = UIColor.lightGray
        return separationView
    }()

    let backButton: UIButton = {
        let backButton = UIButton()
        backButton.setImage(UIImage(named: "BackArrow"), for: UIControl.State.normal)
        backButton.addTarget(self, action: #selector(onBackButtonPressed), for: .touchUpInside)
        return backButton
    }()

    let titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 21))
        titleLabel.addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 150))
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont(name: "Montserrat-Medium", size: 16)
        titleLabel.textColor = UIColor.selectedTextColor()
        return titleLabel
    }()
    
    
    let statusLabel2: UILabel = {
        let statusLabel2 = UILabel()
        statusLabel2.translatesAutoresizingMaskIntoConstraints = false
        statusLabel2.addConstraint(NSLayoutConstraint(item: statusLabel2, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 21))
        statusLabel2.addConstraint(NSLayoutConstraint(item: statusLabel2, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
        statusLabel2.textAlignment = .left
        statusLabel2.font = UIFont(name: "Montserrat-Medium", size: 11)
        statusLabel2.textColor = UIColor.gray
        return statusLabel2
    }()
    
    let statusLabel3: UILabel = {
        let statusLabel3 = UILabel()
        statusLabel3.translatesAutoresizingMaskIntoConstraints = false
        statusLabel3.addConstraint(NSLayoutConstraint(item: statusLabel3, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 21))
        statusLabel3.addConstraint(NSLayoutConstraint(item: statusLabel3, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
        statusLabel3.textAlignment = .left
        statusLabel3.font = UIFont(name: "Montserrat-Medium", size: 11)
        statusLabel3.textColor = UIColor.gray
        return statusLabel3
    }()
    
    let statusLabel4: UILabel = {
        let statusLabel4 = UILabel()
        statusLabel4.translatesAutoresizingMaskIntoConstraints = false
        statusLabel4.addConstraint(NSLayoutConstraint(item: statusLabel4, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 21))
        statusLabel4.addConstraint(NSLayoutConstraint(item: statusLabel4, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
        statusLabel4.textAlignment = .left
        statusLabel4.font = UIFont(name: "Montserrat-Medium", size: 11)
        statusLabel4.textColor = UIColor.gray
        return statusLabel4
    }()
    
    let statusLabel5: UILabel = {
        let statusLabel5 = UILabel()
        statusLabel5.translatesAutoresizingMaskIntoConstraints = false
        statusLabel5.addConstraint(NSLayoutConstraint(item: statusLabel5, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 21))
        statusLabel5.addConstraint(NSLayoutConstraint(item: statusLabel5, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
        statusLabel5.textAlignment = .left
        statusLabel5.font = UIFont(name: "Montserrat-Medium", size: 11)
        statusLabel5.textColor = UIColor.gray
        return statusLabel5
    }()
    

    let hostImage: UIImageView = {
        let hostImage = UIImageView()
        hostImage.translatesAutoresizingMaskIntoConstraints = false
        hostImage.addConstraint(NSLayoutConstraint(item: hostImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 26))
        hostImage.addConstraint(NSLayoutConstraint(item: hostImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 26))

        return hostImage
    }()

    lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("", for: [])
        button.setImage(UIImage(named: "sendButtonCapsule"), for: [])
        self.newMessage = self.inputTextField.text!
        button.addTarget(self, action: #selector(createMeetMeMessage), for: .touchUpInside)
        return button
    }()

    @objc func textFieldDidChange(_ textField: UICustomTextField) {
        if textField == inputTextField {
            self.newMessage = textField.text!
        }
    }

    /*************** CollectionView functions start here ***************/
    
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatLogMessageCell

        let messageObj = messages[indexPath.row]
 
        if let messageText = messageObj.messageBody, let profileImageName = messageObj.picture {
            cell.messageTextView.text = messageObj.messageBody
            cell.participantName.text = messageObj.name
            let messageDateTime = "\(validationController.convert_time_from_UTC_time(givenUTCtime: (messageObj.messageTime)!)) - \(validationController.convert_date_from_UTC_time(givenUTCdate: (messageObj.messageTime)!))"
            let size = CGSize(width: 250, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)
            let estimatedFrameName = NSString(string: (messageObj.name)!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)
            let estimatedFrameTime = NSString(string: (messageDateTime)).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 8)], context: nil)
            if fisheyeId != messageObj.fisheyeId {
                cell.participantName.isHidden = false
                cell.timeLabel.text = messageDateTime
                cell.profileImageView.sd_setImage(with: URL(string: profileImageName ), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"), options: [.continueInBackground, .progressiveLoad])
                cell.messageTextView.frame = CGRect(x: 48 + 8, y: estimatedFrameName.height+4, width: estimatedFrame.width  + 16, height: estimatedFrame.height + 20)
                if estimatedFrameName.width>estimatedFrame.width && estimatedFrameName.width>estimatedFrameTime.width {
                    cell.textBubbleView.frame = CGRect(x: 48, y: 0, width: estimatedFrameName.width + 20 + 8, height: estimatedFrame.height + estimatedFrameTime.height + estimatedFrameName.height + 10 + 20)
                } else if estimatedFrame.width > estimatedFrameName.width && estimatedFrame.width > estimatedFrameTime.width {
                    cell.textBubbleView.frame = CGRect(x: 48, y: 0, width: estimatedFrame.width + 16 + 8, height: estimatedFrame.height + estimatedFrameTime.height + estimatedFrameName.height + 10 + 20)
                } else {
                    cell.textBubbleView.frame = CGRect(x: 48, y: 0, width: estimatedFrameTime.width + 16 + 8, height: estimatedFrame.height + estimatedFrameTime.height + estimatedFrameName.height + 10 + 20)
                }
                cell.participantName.frame = CGRect(x: 60, y: 0, width: estimatedFrameName.width + 8, height: estimatedFrameName.height + 10)
                cell.timeLabel.frame = CGRect(x: (cell.textBubbleView.frame.width+40)-(estimatedFrameTime.width), y: cell.textBubbleView.frame.height - estimatedFrameTime.height - 10, width: estimatedFrameTime.width+5, height: estimatedFrameTime.height + 5)
                cell.statusImageView.frame = CGRect(x: (cell.textBubbleView.frame.width+103)-(estimatedFrameTime.width), y: cell.textBubbleView.frame.height - estimatedFrameTime.height-5, width: 8,  height: 8)
                cell.statusImageView.layer.cornerRadius = 4
                cell.statusImageView.isHidden=true
                cell.profileImageView.isHidden = false
                cell.textBubbleView.backgroundColor = UIColor.capsuleBlue()

            }else {
                cell.statusImageView.isHidden = false
                if(messageObj.isSent == true){
                    if((messageObj.isReadBy.contains(messageObj.fisheyeId!) && messageObj.isReadBy.count <= 1) || messageObj.isReadBy == [] || messageObj.isReadBy == nil || messageObj.isReadBy.count == 0){
                        cell.statusImageView.backgroundColor = UIColor.blue
                    }else if((messageObj.isReadBy.contains(messageObj.fisheyeId!)) && (messageObj.isReadBy.count > 1)){
                        if((messageObj.isReadBy.count == self.meetmeParticipantCount)){
                            cell.statusImageView.backgroundColor = UIColor.green
                        }else if((self.meetmeParticipantCount!) > (messageObj.isReadBy.count)){
                            cell.statusImageView.backgroundColor = UIColor.yellow
                        }
                    }
                }else{
                    cell.statusImageView.backgroundColor = UIColor.red
                }
                cell.timeLabel.text = messageDateTime
                cell.messageTextView.frame =  CGRect(x: view.frame.width - estimatedFrame.width - 16 - 16 - 8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 20)
                if estimatedFrame.width > estimatedFrameTime.width {
                    cell.textBubbleView.frame = CGRect(x: view.frame.width - estimatedFrame.width - 50, y: 0, width: estimatedFrame.width  + 16 + 8 + 10, height: estimatedFrame.height + estimatedFrameTime.height + 20 + 6)
                } else {
                    cell.messageTextView.frame =  CGRect(x: view.frame.width - estimatedFrameTime.width - 16 - 16 - 8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height + 20)
                    cell.textBubbleView.frame = CGRect(x: view.frame.width - estimatedFrameTime.width - 50, y: 0, width: estimatedFrameTime.width  + 16 + 8 + 10, height: estimatedFrame.height + estimatedFrameTime.height + 20 + 6)
                }
                cell.textBubbleView.backgroundColor = UIColor(white: 0.95, alpha: 1)
                cell.timeLabel.frame = CGRect(x: view.frame.width - estimatedFrameTime.width  - 16 - 8, y: cell.textBubbleView.frame.height - estimatedFrameTime.height - 12, width: estimatedFrameTime.width+5, height: estimatedFrameTime.height+5)
                cell.statusImageView.frame = CGRect(x: view.frame.width - estimatedFrameTime.width + 40, y: cell.textBubbleView.frame.height - estimatedFrameTime.height - 7, width: 8, height: 8)
                cell.statusImageView.layer.cornerRadius = 4
                cell.profileImageView.isHidden = true
                cell.participantName.isHidden = true
            }
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let messageObj = messages[indexPath.row]
        if let messageText = messages[indexPath.item].messageBody {
            let messageDateTime = "\(validationController.convert_time_from_UTC_time(givenUTCtime: (messageObj.messageTime)!)) - \(validationController.convert_date_from_UTC_time(givenUTCdate: (messageObj.messageTime)!))"
            let size = CGSize(width: 250, height: 1000)

            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: messageText).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)
            let estimatedFrameName = NSString(string: (messageObj.name)!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)
            let estimatedFrameTime = NSString(string: (messageDateTime)).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 8)], context: nil)
            if fisheyeId != messageObj.fisheyeId {
                return CGSize(width: view.frame.width, height: estimatedFrame.height + estimatedFrameTime.height + estimatedFrameName.height + 20 + 10)
            } else {
                return CGSize(width: view.frame.width, height: estimatedFrame.height  + estimatedFrameTime.height + 10 + 20)
            }
        }

        return CGSize(width: self.capsuleChatCollectionView.frame.width, height: 100)
    }

     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextField.endEditing(true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 58, left: 0, bottom: 63, right: 0)
    }

    /*************** CollectionView functions end here ***************/
    @objc func handleKeyboardNotification(notification: NSNotification) {

        if let userInfo = notification.userInfo {

            let keyboardFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue

            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification

            if UIDevice.modelName.isEqualToString(find: "iPhone X") {

                bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height - 30 : 0
                bottomConstraint2?.constant = isKeyboardShowing ? -keyboardFrame!.height  : 0
            } else {

                bottomConstraint?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
                bottomConstraint2?.constant = isKeyboardShowing ? -keyboardFrame!.height : 0
            }

            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {

                self.view.layoutIfNeeded()

            }, completion: { (_) in

                if isKeyboardShowing  && self.messages.count>0 {
                    let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
                    self.capsuleChatCollectionView?.scrollToItem(at: indexPath as IndexPath, at: .centeredVertically, animated: true)
                }

            })
        }
    }

    /************************** Timer functions start here **************************/
    func scheduleTimer() {
        stopTimer()
        timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
    }

    @objc func updateLocation() {
        self.fetchMeetmeCapsule()
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }

    /************************** Timer functions end here **************************/
    private func setupInputComponents() {
        let topBorderView = UIView()
        topBorderView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)

        messageInputContainerView.addSubview(inputTextField)
        messageInputContainerView.addSubview(sendButton)
        messageInputContainerView.addSubview(topBorderView)
        messageInputContainerView.addConstraintsWithFormat("H:|-8-[v0][v1(60)]|", views: inputTextField, sendButton)
        messageInputContainerView.addConstraintsWithFormat("V:|[v0]|", views: inputTextField)
        messageInputContainerView.addConstraintsWithFormat("V:|[v0]|", views: sendButton)
        messageInputContainerView.addConstraintsWithFormat("H:|[v0]|", views: topBorderView)
        messageInputContainerView.addConstraintsWithFormat("V:|[v0(0.5)]", views: topBorderView)

        titleLabel.text = self.meetmeTitle

        
        appBarView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 55)
        backButton.frame = CGRect(x: 8, y: (appBarView.frame.height/2)-20, width: 40, height: 40)

        messageInputContainerView.frame = CGRect(x: 0, y: appBarView.frame.height, width: view.frame.width, height: 48)

        bottomConstraint = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        bottomConstraint2 = NSLayoutConstraint(item: self.capsuleChatCollectionView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        
        appBarView.addSubview(titleLabel)
        
        self.CapsuleStatusView()
        
        appBarView.addSubview(hostImage)
        appBarView.addSubview(backButton)
        appBarView.layer.zPosition = .greatestFiniteMagnitude
        view.addConstraint(bottomConstraint!)
        view.addConstraint(bottomConstraint2!)
        view.addSubview(appBarView)        
        let titleConstraint1 = NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: appBarView, attribute: .centerX, multiplier: 1, constant: 15)
        let titleConstraint2 = NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: appBarView, attribute: .centerY, multiplier: 1, constant: 0)
        
        let hostImageConstraint1 = NSLayoutConstraint(item: hostImage, attribute: .left, relatedBy: .equal, toItem: backButton, attribute: .right, multiplier: 1, constant: 50)
        let hostImageConstraint2 = NSLayoutConstraint(item: hostImage, attribute: .centerY, relatedBy: .equal, toItem: appBarView, attribute: .centerY, multiplier: 1, constant: 0)

        view.addConstraintsWithFormat("H:|[v0]|", views: messageInputContainerView)
        view.addConstraintsWithFormat("V:[v0(48)]", views: messageInputContainerView)
        appBarView.addConstraints([titleConstraint1, titleConstraint2])
        
        appBarView.addConstraint(hostImageConstraint1)
        appBarView.addConstraint(hostImageConstraint2)

        self.view.bringSubviewToFront(messageInputContainerView)

    }

    @objc func onBackButtonPressed() {
//        seenMessages()
        stopTimer()
        if self.isFromDetails{
            makeUnreadCapsuleCountZeroOnDetails?.makeCountZeroOnDetails(isToMakeCountZero: self.isCapsuleFetched)
        }else{
            makeUnreadCapsuleCountZero?.makeCountZero(isToMakeCountZero: self.isCapsuleFetched)
        }
        self.view.removeFromSuperview()
    }
    
    func addUnsentCapToDBOnNetworkDetection() {
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                return
        }
       
        let feedbackOperation = AddUnsentCapsuleMessages(userId: "12345678888888", meetmeId: MessageSync.shared.meetMeId, meetmeUnsentMsgsArray: MessageSync.shared.offlineMessages)
        feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
             if(operation.statusCode == "200") {
                MessageSync.shared.offlineMessages=[]
                MessageSync.shared.meetMeId=""
                let meetPartCount = operation.meetmeCapsule.meetParticipantCount
                self.meetmeParticipantCount = meetPartCount
                self.messages = operation.meetmeCapsule.messages
               
                DispatchQueue.main.async {
                    if (self.messages.count > 0 || MessageSync.shared.offlineMessages.count > 0) {
                        NotificationCenter.default.post(name: SEND_UNSEND_MESSAGES, object: nil)
                        self.capsuleChatCollectionView?.isHidden = false
                        let index = self.messages.count - 1
                        let indexPath = IndexPath(row: index, section: 0)
                        self.capsuleChatCollectionView?.reloadData()
                        self.capsuleChatCollectionView?.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
                    } else {
                        self.capsuleChatCollectionView?.isHidden = true
                        self.noCapsuleView.isHidden = false
                        self.noCaspuleImageView.isHidden = false
                        self.noCapsuleLabel.text = self.appService.noCapsuleFoundMessage
                    }
                }
            } else {
               
            }
        }
        AppDelegate.addProcedure(operation: feedbackOperation)
    }
}

class ChatLogMessageCell: UICollectionViewCell {

    let messageTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont(name: "Montserrat-Regular", size: 11) //UIFont.systemFont(ofSize: 12)
        textView.text = "Sample message"
        textView.backgroundColor = UIColor.clear
        textView.isEditable = false
        return textView
    }()

    let participantName: UILabel = {
        let textView = UILabel()
        textView.font = UIFont(name: "Montserrat-Medium", size: 11) // UIFont.boldSystemFont(ofSize: 12)
        textView.text = "Rushikesh"
        textView.backgroundColor = UIColor.clear
        return textView
    }()

    let textBubbleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.95, alpha: 1)
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()

    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let statusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
//        imageView.layer.cornerRadius = 5
//        imageView.image = UIImage(named: "AcceptedParticipant.png")
        return imageView
    }()

    let timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-Italic", size: 7) //UIFont.italicSystemFont(ofSize: 8)
        label.text = "06:31 - 12/01/2017"
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.gray
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        addSubview(textBubbleView)
        addSubview(messageTextView)
        addSubview(profileImageView)
        addSubview(participantName)
        addSubview(timeLabel)
        addSubview(statusImageView)
        addConstraintsWithFormat("H:|-8-[v0(30)]", views: profileImageView)
        addConstraintsWithFormat("V:[v0(30)]|", views: profileImageView)
    }

}

extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
extension MeetMeCapsuleVC {
    func fetchMeetmeCapsule() {
        guard networkStatus.isNetworkReachable()
            else {
                DispatchQueue.main.async {
                    if(self.messages.count > 0 || MessageSync.shared.offlineMessages.count > 0){
                    }else{
                        self.capsuleChatCollectionView?.isHidden = false
                        self.noCapsuleView.isHidden = false
                        self.noCaspuleImageView.isHidden = false
                        self.noCapsuleLabel.text = self.appService.checkInternetConnectionMessage
                    }
                 }
                return
        }

        let meetmeCapsuleCall = GetMeetMeCapsule(meetMeId: self.selectedMeetmeId)
        meetmeCapsuleCall.addDidFinishBlockObserver { [unowned self] (operation, _) in

            let responseStatus = operation.responseStatus
            if responseStatus == "200"{
                self.capsule = operation.meetmeCapsule
                self.meetmeParticipantCount = operation.meetmeCapsule.meetParticipantCount
                let meetPartCount=operation.meetmeCapsule.meetParticipantCount
                self.meetmeParticipantCount=meetPartCount
                self.messages = operation.meetmeCapsule.messages
                DispatchQueue.main.async {
                    if self.messages.count > 0 {
                        self.noCapsuleView.isHidden = true
                        self.noCaspuleImageView.isHidden = true
                        self.capsuleChatCollectionView?.isHidden = false
                        let index = self.messages.count - 1
                        let indexPath = IndexPath(row: index, section: 0)
                        self.capsuleChatCollectionView?.reloadData()
                        self.view.layoutIfNeeded()
                        self.capsuleChatCollectionView?.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
                    } else {
                        self.capsuleChatCollectionView?.isHidden = true
                        self.noCapsuleView.isHidden = false
                        self.noCaspuleImageView.isHidden = false
                        self.noCapsuleLabel.text = self.appService.noCapsuleFoundMessage
                    }
                    self.hostImage.sd_setImage(with: URL(string: (self.capsule?.hostParticipant.participantPicture)! ), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"), options: [.continueInBackground, .progressiveLoad])
                        self.hostImage.layer.cornerRadius = self.hostImage.frame.size.width / 2
                        self.hostImage.layer.masksToBounds = true
                }
                self.seenMessages()
                self.isCapsuleFetched = true
            }else{
                self.isCapsuleFetched = false
            }
        }
        AppDelegate.addProcedure(operation: meetmeCapsuleCall)
    }

    @objc func createMeetMeMessage() {
        if self.inputTextField.text == ""{
            return
        }
        self.inputTextField.text = nil
        if ((self.newMessage.count==0) || (self.newMessage.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count==0)){
            self.view.makeToast(self.appService.emptyText)
        } else {
            guard networkStatus.isNetworkReachable()
            else {
                DispatchQueue.main.async {
                       self.noCapsuleView.isHidden = true
                        self.noCaspuleImageView.isHidden = true
                        self.noCapsuleLabel.isHidden=true
                        self.capsuleChatCollectionView?.isHidden = false

                        self.offlineMessage(meetMeId: self.selectedMeetmeId, meetmeMessage: self.newMessage)
                    }

                    return
            }

            let meetmeCapsuleCall = CreateMessage(meetMeId: self.selectedMeetmeId, meetmeMessage: self.newMessage, meetTitle: self.meetmeTitle!)
            meetmeCapsuleCall.addDidFinishBlockObserver { [unowned self] (operation, _) in

                let responseStatus = operation.responseStatus
                if responseStatus == "200"{
                    self.capsule = operation.meetmeCapsule
                    self.messages = operation.meetmeCapsule.messages
                 
                    DispatchQueue.main.async {
                        if self.messages.count > 0 || MessageSync.shared.offlineMessages.count > 0{
                            self.noCapsuleView.isHidden = true
                            self.capsuleChatCollectionView?.isHidden = false
                            let index = self.messages.count - 1
                            let indexPath = IndexPath(row: index, section: 0)
                            self.capsuleChatCollectionView?.reloadData()
                            self.capsuleChatCollectionView?.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
                            self.addUnsentCapToDBOnNetworkDetection()
                        } else {
                            self.capsuleChatCollectionView?.isHidden = true
                            self.noCapsuleView.isHidden = false
                        }
                    }
                }
            }
            AppDelegate.addProcedure(operation: meetmeCapsuleCall)
        }

    }

    func offlineMessage(meetMeId: String, meetmeMessage: String) {
        formatter.dateFormat = "HH:mm a"
        let message2 = MeetMeMessage()
        message2.messageBody = meetmeMessage
        message2.messageTime = validationController.getTodaysDateInTimeStamp()
        message2.picture = self.profileImageUrl
        message2.name = self.profilename
        message2.fisheyeId = self.profileId
        message2.isSent = false
        messages.append(message2)
        let currentMeetArray = MessageSync.shared.offlineMessages.filter( { return ($0.meetmeId == meetMeId) } )
        if currentMeetArray.count > 0 {
            currentMeetArray[0].offlineMessages.append(message2)
        }else{
            let newMeetCapsule = OfflineMeetMeCapsule()
            newMeetCapsule.meetmeId = meetMeId
            newMeetCapsule.meetmeTitle = self.meetmeTitle
            newMeetCapsule.offlineMessages.append(message2)
            MessageSync.shared.offlineMessages.append(newMeetCapsule)
        }
//        MessageSync.shared.offlineMessages.append(message2)
        MessageSync.shared.meetMeId = meetMeId
        self.view.layoutIfNeeded()
        let index = self.messages.count - 1
        let indexPath = IndexPath(row: index, section: 0)
        self.capsuleChatCollectionView?.reloadData()
        self.capsuleChatCollectionView?.isHidden = false
        self.capsuleChatCollectionView?.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
    }

    func seenMessages() {

        guard self.networkStatus.isNetworkReachable()
            else {
                self.view.makeToast(self.appService.checkInternetConnectionMessage)
                return
        }

        let awsURL = AppConfig.capsuleCountUrl(selectedMeetmeId: selectedMeetmeId )
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        request.httpBody =  "".data(using: String.Encoding.utf8)

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, response: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                let meetNotificationCount = data["userMeetMeNotifications"] as? Int ?? 0
                AppService.shared.noOfUnreadMeetNotifications = meetNotificationCount
                NotificationCenter.default.post(name: REFRESH_MEET_NOTIFICATION_COUNT, object: nil)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }

    


}

extension UIView {
    var allSubviews: [UIView] {
        return self.subviews.flatMap { [$0] + $0.allSubviews }
    }
}


extension MeetMeCapsuleVC  {
    
    func CapsuleStatusView(){
        
        messageStatusNotationView.frame = CGRect(x: 0, y: appBarView.frame.height, width: view.frame.width, height: 30)
        view.addSubview(messageStatusNotationView)
        separationView.frame = CGRect(x: 0, y: messageStatusNotationView.frame.origin.y + messageStatusNotationView.frame.height - 3, width: appBarView.frame.width, height: 1)
        self.view.addSubview(separationView)

        var widthOfPreviousStatusViews = 0
        for j in 0..<capsuleStatuses.count{

        let statusLabel: UILabel = {
            let statusLabel1 = UILabel()
            statusLabel1.text = "undelivered"
            statusLabel1.translatesAutoresizingMaskIntoConstraints = false
            statusLabel1.font = UIFont(name: "Montserrat-Medium", size: 11)
            statusLabel1.textAlignment = .left
            statusLabel1.textColor = UIColor.gray
            return statusLabel1
        }()
        
        let colorView: UIView = {
            let uiView = UIView()
            return uiView
        }()
        
        let statusView: UIView = {
            let uiView = UIView()
            uiView.backgroundColor = UIColor.white
            return uiView
        }()
            
            let status = capsuleStatuses[j]
            
            statusLabel.text = status
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let size = CGSize(width: 250, height: 1000)
            let estimatedFrameStatusLabel = NSString(string: (statusLabel.text ?? "")).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont(name: "Montserrat-Medium", size: 11)], context: nil)
            messageStatusNotationView.addSubview(statusView)
            
            colorView.frame = CGRect(x: 10, y: 0, width: 11, height: 11)
//            var capsuleStatusColors: [String] = ["red", "blue", "yellow" ,"green"]

            let color = self.capsuleStatusColors[j]
            switch color {
                case "red":
                    colorView.backgroundColor = UIColor.red
                    break
                
                case "blue":
                    colorView.backgroundColor = UIColor.blue
                    break
                
                case "yellow":
                    colorView.backgroundColor = UIColor.yellow
                    break
                
                case "orange":
                    colorView.backgroundColor = UIColor.orange
                    break
                
                case "green":
                    colorView.backgroundColor = UIColor.green
                    break
                
                default:
                    colorView.backgroundColor = UIColor.blue
                    break
            }
            colorView.layer.cornerRadius = colorView.frame.height / 2
            statusView.addSubview(colorView)
            let statusLabelLeftConstraint = NSLayoutConstraint(item: statusLabel, attribute: .left, relatedBy: .equal, toItem: colorView, attribute: .right, multiplier: 1, constant: 5)
            statusView.frame = CGRect(x: CGFloat(widthOfPreviousStatusViews), y: 5 , width: colorView.frame.width + estimatedFrameStatusLabel.width + 20, height: estimatedFrameStatusLabel.height)
            statusView.addSubview(statusLabel)
            statusView.addConstraint(statusLabelLeftConstraint)
            widthOfPreviousStatusViews = widthOfPreviousStatusViews + Int(statusView.frame.width)
        }
    }
}

