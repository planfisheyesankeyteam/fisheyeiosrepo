//
//  SearchContactsCollectionViewCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 05/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class SearchContactsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var selectedContactImage: UIImageView!

    @IBOutlet weak var deselectContactImageView: UIImageView!

    @IBOutlet weak var deselectContactButton: UIButton!

    @IBOutlet weak var contactName: UILabel!

}
