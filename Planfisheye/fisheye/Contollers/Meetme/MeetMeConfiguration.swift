//
//  MeetMeConfiguration.swift
//  fisheye
//
//  Created by Sankey Solutions on 17/04/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

struct MeetMeConfiguration {
    var numberOfParticipants = 0
    var trackingOptions = [MeetMeTrackingOptions]()
}
