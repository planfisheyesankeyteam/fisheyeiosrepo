//
//  MeetMeCapsuleCollectionCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 20/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class MeetMeCapsuleCollectionCell: UICollectionViewCell {

    @IBOutlet weak var completeCellView: UIView!
    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var participantName: UILabel!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var participantImageContainerView: UIView!
    @IBOutlet weak var participantImageView: UIImageView!

}
