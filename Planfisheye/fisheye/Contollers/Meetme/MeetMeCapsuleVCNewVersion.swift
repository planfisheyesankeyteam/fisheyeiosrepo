//
//  MeetMeCapsuleVCNewVersion.swift
//  fisheye
//
//  Created by Sankey Solutions on 20/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class MeetMeCapsuleVCNewVersion: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var messages: [MeetMeMessage]?
    var capsule: MeetMeCapsule?
    let validationController = ValidationController()

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (messages?.count)!
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "MeetMeCapsuleCollectionCell", for: indexPath) as! MeetMeCapsuleCollectionCell

        let messageObj = messages![indexPath.row]
        cell.participantName.text = messageObj.name
        cell.messageText.text = messageObj.messageBody
        cell.dateLabel.text = validationController.convert_date_from_UTC_time(givenUTCdate: messageObj.messageTime!)
        cell.messageTime.text = validationController.convert_time_from_UTC_time(givenUTCtime: messageObj.messageTime!)
        cell.participantImageView.sd_setImage(with: URL(string: messageObj.picture! ), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrameMessage = NSString(string: messageObj.messageBody!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)
        let estimatedFrameDate = NSString(string: cell.dateLabel.text!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)
        let estimatedFrameName = NSString(string: messageObj.name!).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)
        let estimatedFrameTime = NSString(string: cell.messageTime.text! ).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12)], context: nil)

        let padding = cell.messageText.textContainer.lineFragmentPadding
        cell.messageText.textContainerInset = UIEdgeInsets(top: 0, left: -padding, bottom: 0, right: -padding)
        let messageTextConstraintHeight =  NSLayoutConstraint(item: cell.messageText, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (estimatedFrameMessage.height + 20))
        let messageTextConstraintWidth =  NSLayoutConstraint(item: cell.messageText, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (estimatedFrameMessage.width + 16 + 8 + 10))

        let dateLabelHeightConstarint = NSLayoutConstraint(item: cell.messageText, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (estimatedFrameDate.height))

        let timeLabelHeightConstraint = NSLayoutConstraint(item: cell.messageText, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (estimatedFrameTime.height+10))

        let messageViewConstraintHeight = NSLayoutConstraint(item: cell.messageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (estimatedFrameMessage.height+20+estimatedFrameName.height+estimatedFrameTime.height+20))
        let messageViewConstraintWidth = NSLayoutConstraint(item: cell.messageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (estimatedFrameMessage.width+30))

        let completeConstraint = NSLayoutConstraint(item: cell.completeCellView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: (estimatedFrameMessage.height+estimatedFrameDate.height+estimatedFrameName.height+estimatedFrameTime.height+30))
        cell.addConstraints([completeConstraint, messageTextConstraintHeight, messageTextConstraintWidth, messageViewConstraintHeight, messageViewConstraintWidth, dateLabelHeightConstarint, timeLabelHeightConstraint])
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
