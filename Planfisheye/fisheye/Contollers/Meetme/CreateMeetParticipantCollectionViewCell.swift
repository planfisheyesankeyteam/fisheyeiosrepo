//
//  CreateMeetParticipantCollectionViewCell.swift
//  fisheye
//
//  Created by Sankey Solutions on 02/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class CreateMeetParticipantCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var participantImageView: UIImageView!

    @IBOutlet weak var participantNameLabel: UILabel!

}
