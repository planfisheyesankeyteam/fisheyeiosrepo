//
//  EditParticipantsVc.swift
//  fisheye
//
//  Created by Sankey Solutions on 28/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class EditParticipantsVc: UIViewController, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var contactsView: UIView!
    @IBOutlet weak var appBarView: UIView!
    @IBOutlet weak var contactsCollectionView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var contactsTableView: UITableView!

    //Appbar layout variables
    @IBOutlet weak var backButtonImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addParticipantsLabel: UILabel!
    @IBOutlet weak var searchImageView: UIImageView!
    @IBOutlet weak var participantCountLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var saveButton: UIButton!

    var fisheyeContacts: [ContactObj] = []
    var nonFisheyeContacts: [ContactObj] = []
    var allContactList: [ContactObj] = []
    var currentMeet: [ParticipantVariables] = []
    var totalContacts = 0
    var selectedContacts = 0
    var sendSelectedContacts: SendSelectedContacts?
    let appSharedPrefernce = AppSharedPreference()
    override func viewDidLoad() {
        super.viewDidLoad()

        searchBar.delegate = self
        searchBar.showsCancelButton = true
        contactsTableView.delegate = self
        self.getContacts()
    }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.EditParticipantsVc)
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onAppBarSearchButtonTapped(_ sender: Any) {
        searchView.isHidden = false
        appBarView.isHidden = true
    }

    @IBAction func onBackButtonTapped(_ sender: Any) {
        self.view.removeFromSuperview()
    }

    @IBAction func onSaveButtonTapped(_ sender: Any) {

        self.view.removeFromSuperview()

    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchView.isHidden = true
        appBarView.isHidden = false
        self.getContacts()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        let searchKeyText = searchText
        fisheyeContacts = DatabaseManagement.shared.searchFEContacts(searchText: searchKeyText)
        nonFisheyeContacts = DatabaseManagement.shared.searchNonFEContacts(searchText: searchKeyText)
        getSearchedContacts()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return currentMeet.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchContactsCollectionViewCell", for: indexPath) as! SearchContactsCollectionViewCell
        let selectedContact = currentMeet[indexPath.row]
        cell.contactName.text = selectedContact.participantName
        cell.selectedContactImage?.sd_setImage(with: URL(string: selectedContact.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"), options: [.continueInBackground, .progressiveDownload])
        cell.selectedContactImage.layer.cornerRadius =   cell.selectedContactImage.frame.size.height / 2
        cell.selectedContactImage.layer.masksToBounds = true
        cell.deselectContactButton.tag = indexPath.row
        let tapGestureDeselect = UITapGestureRecognizer(target: self, action: #selector(deselectContact(sender:)))
        cell.deselectContactButton.addGestureRecognizer(tapGestureDeselect)
        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return allContactList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = Bundle.main.loadNibNamed("SearchContactsTableViewCell", owner: self, options: nil)?.first as! SearchContactsTableViewCell

        let contactObj = allContactList[indexPath.row]
        tableCell.contactCellName.text = contactObj.name
        tableCell.contactCellEmailId.text = contactObj.email
        tableCell.contactCellPhoneNumber.text = contactObj.phoneNumber
        tableCell.contactCellImageView.layer.cornerRadius =   tableCell.contactCellImageView.frame.size.height / 2
        tableCell.contactCellImageView.layer.masksToBounds = true
        tableCell.contactCellImageView?.sd_setImage(with: URL(string: contactObj.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"), options: [.continueInBackground, .progressiveDownload])

        let isSelected = currentMeet.contains(where: { $0.contactId == String(contactObj.contactId)})
        if isSelected {
            tableCell.isChecked = true
            tableCell.contactCellCheckboxImage.image = UIImage(named: "successBtn")
        } else {
            tableCell.isChecked = false
            tableCell.contactCellCheckboxImage.image = UIImage(named: "unselectedRadioBtn")
        }

        let tapGestureFullTab = UITapGestureRecognizer(target: self, action: #selector(addContact(sender:)))
        tableCell.contactCellCheckboxButton.tag = indexPath.row
        tableCell.contactCellCheckboxButton.addGestureRecognizer(tapGestureFullTab)

        let tapGestureSelect = UITapGestureRecognizer(target: self, action: #selector(addContact(sender:)))
        tableCell.tag = indexPath.row
        tableCell.addGestureRecognizer(tapGestureSelect)

        return tableCell
    }

    func addContact(sender: Any ) {
        let index = (sender as AnyObject).view!.tag
        let selectedContactObj = allContactList[index]

        let indexPath = IndexPath(row: index, section: 0)
        guard let cell = contactsTableView.cellForRow(at: indexPath ) as? SearchContactsTableViewCell
            else { return }

        if cell.isChecked {
            for i in 0..<currentMeet.count {
                if let selectedCon = currentMeet[i]  as?  ParticipantVariables {
                    if String(selectedContactObj.contactId) == selectedCon.contactId {
                        currentMeet.remove(at: i)
                        break
                    }
                }
            }
            selectedContacts -= 1
            self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
            contactsCollectionView.reloadData()
            cell.isChecked = false
            cell.contactCellCheckboxImage.image = UIImage(named: "unselectedRadioBtn")
        } else {
            selectedContacts += 1
            let newParticipant = ParticipantVariables()
            newParticipant.contactId = String(selectedContactObj.contactId)
            newParticipant.fisheyeId = selectedContactObj.contactFEId
            currentMeet.append(newParticipant)
            contactsCollectionView.reloadData()
            cell.isChecked = true
            self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
            cell.contactCellCheckboxImage.image = UIImage(named: "successBtn")
        }
    }

    func deselectContact(sender: Any ) {
        let index = (sender as AnyObject).view!.tag

        self.currentMeet.remove(at: index)
        selectedContacts -= 1
        self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
        contactsCollectionView.reloadData()
        contactsTableView.reloadData()

    }

}

extension EditParticipantsVc {
    func getContacts() {

        /* get fisheye contacts */
        let fisheyeContacts: [ContactObj]
        fisheyeContacts = DatabaseManagement.shared.getFisheyeContacts()
        self.fisheyeContacts = []
        self.fisheyeContacts = fisheyeContacts
        self.fisheyeContacts.sort { $0.name < $1.name }
        /** End **/

        /* get non fisheye contacts */
        let nonFisheyeContacts: [ContactObj]
        nonFisheyeContacts = DatabaseManagement.shared.getNonFisheyeContacts()
        self.nonFisheyeContacts = []
        self.nonFisheyeContacts = nonFisheyeContacts
        self.nonFisheyeContacts.sort { $0.name < $1.name }
        /** End **/

        /* all contact list */
        self.allContactList  = []
        for i in 0..<fisheyeContacts.count {  // append nonfisheye contacts to fisheye contact list
            self.allContactList.append(self.fisheyeContacts[i])
        }
        for i in 0..<nonFisheyeContacts.count {  // append nonfisheye contacts to fisheye contact list
            self.allContactList.append(self.nonFisheyeContacts[i])
        }
        totalContacts = allContactList.count
        self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
        /*** End **/

        //For no contacts fetched from db
        if( self.allContactList.count == 0) {

        } else {

        }

        DispatchQueue.main.async {
                self.contactsTableView.reloadData()

        }
    }

    func getSearchedContacts () {

        self.fisheyeContacts.sort { $0.name < $1.name }
        self.nonFisheyeContacts.sort { $0.name < $1.name }

        /* all contact list */
        self.allContactList  = []
        for i in 0..<self.fisheyeContacts.count {  // append nonfisheye contacts to fisheye contact list
            self.allContactList.append(self.fisheyeContacts[i])
        }
        for i in 0..<self.nonFisheyeContacts.count {  // append nonfisheye contacts to fisheye contact list
            self.allContactList.append(self.nonFisheyeContacts[i])
        }
        /*** End **/
        totalContacts = allContactList.count
        self.participantCountLabel.text = "(\(self.selectedContacts)/\(self.totalContacts))"
        DispatchQueue.main.async {
            self.contactsTableView.reloadData()
        }
        //For no Contact
        if(self.allContactList.count == 0) {

        } else {

        }
    }
}
