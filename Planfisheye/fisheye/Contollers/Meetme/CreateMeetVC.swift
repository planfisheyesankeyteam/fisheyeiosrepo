//
//  CreateMeetVC.swift
//  fisheye
//  Modified by Anagha Meshram on 13/02/2018
//  Created by Sankey Solutions on 01/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import MapKit
import Toast_Swift
import DatePickerDialog
import DropDown

class CreateMeetVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    // Outlet Variables
    @IBOutlet weak var createMeetCompleteView: UIView!

    @IBOutlet weak var createNewMeetingLabel: UILabel!
    @IBOutlet weak var appbarView: UIView!
    @IBOutlet weak var createMeetScrollView: UIScrollView!
    @IBOutlet weak var createMeetMainView: UIView!

    @IBOutlet weak var backButton: UIButton!

    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleCustomTextField: UICustomTextField!
    @IBOutlet weak var titleUnderlineImageView: UIImageView!
    @IBOutlet weak var meetLocationCustomTextField: UICustomTextField!
    @IBOutlet weak var locationUnderlineImageView: UIImageView!
    @IBOutlet weak var locationImageView: UIImageView!

    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var mapButton: UIButton!

    @IBOutlet weak var tapToLocateLabel: UILabel!
      
    @IBOutlet weak var mode: UILabel!
    @IBOutlet weak var meetStartDateTimingView: UIView!
    @IBOutlet weak var meetStartTimeView: UIView!
    @IBOutlet weak var meetStartTimeUnderlineImageView: UIImageView!
    @IBOutlet weak var meetStartsAtLabel: UILabel!
    @IBOutlet weak var meetStartTimeLabel: UILabel!
    @IBOutlet weak var meetStartTimeImageView: UIImageView!
    @IBOutlet weak var meetStartTimeButton: UIButton!
    @IBOutlet weak var meetStartDateView: UIView!
    @IBOutlet weak var meetStartDateUnderlineImage: UIImageView!
    @IBOutlet weak var meetStartDateImageView: UIImageView!
    @IBOutlet weak var meetStartDateButton: UIButton!

    @IBOutlet weak var meetStartDateLabel: UILabel!

    @IBOutlet weak var meetDateEndTimmingView: UIView!
    @IBOutlet weak var meetEndTimeView: UIView!
    @IBOutlet weak var meetEndTimeLabel: UILabel!
    @IBOutlet weak var meetEndTimeButton: UIButton!
    @IBOutlet weak var meetEndTimeImageView: UIImageView!
    @IBOutlet weak var meetEndTimeUnderlineImageView: UIImageView!
    @IBOutlet weak var meetEndDateView: UIView!
    @IBOutlet weak var meetEndsAtLabel: UILabel!
    @IBOutlet weak var meetEndDateButton: UIButton!
    @IBOutlet weak var meetEndDateImageView: UIImageView!
    @IBOutlet weak var meetEndDateLabel: UILabel!
    @IBOutlet weak var meetEndDateUnderlineImageView: UIImageView!

    //@IBOutlet weak var contactInfoLabel: UILabel!
    @IBOutlet weak var participantInviteLabel: UILabel!
    @IBOutlet weak var addParticipantLabel: UILabel!
    @IBOutlet weak var addParticipantImageView: UIImageView!
    @IBOutlet weak var addParticipantButtonImageView: UIImageView!
    @IBOutlet weak var addParticipantButton: UIButton!
    @IBOutlet weak var startTrackingLabel: UILabel!
    @IBOutlet weak var meetParticipantsCollectionView: UICollectionView!
    @IBOutlet weak var startTrackingTimeLabel: UILabel!
    @IBOutlet weak var discloseTrailCustomTextField: UITextField!
    @IBOutlet weak var discloseTrailImageView: UIImageView!
    @IBOutlet weak var discloseTrailUnderline: UIImageView!
    @IBOutlet weak var discloseTrailDropdownImageView: UIImageView!
    @IBOutlet weak var discloseTrailButton: UIButton!
    @IBOutlet weak var discloseTrailPicker: UIPickerView!
    @IBOutlet weak var discloseTrailTimeCustomTextField: UITextField!
    @IBOutlet weak var discloseTrailTimeView: UIView!
    @IBOutlet weak var discloseTrailUnderlineImageView: UIImageView!
    @IBOutlet weak var discloseTraiTimelDropdownImageVeiw: UIImageView!
    @IBOutlet weak var discloseTrailTimeButton: UIButton!
    @IBOutlet weak var discloseTrailTimePickerView: UIPickerView!
    @IBOutlet weak var saveMeetButton: UIButton!


    @IBOutlet weak var selectModeOfTransport: UILabel!
    @IBOutlet weak var modeTableView: UITableView!
    @IBOutlet weak var selectModeOfTransportbtn: UIButton!

    var meetLatitude = 0.0
    var meetLongitude = 0.0
    var modeOfTransportArray = ["Walking", "Driving"]
    var englishmodeArray: [String] = []
    var selectmodeArray: Int = 1
    var contactsTobeAdded: [ContactObj] = []
    var mainContactObj: [AnyObject] = []
    var discloseTrailData: [String] = []
    var discloseTrailTimeData: [String] = []
    var selectedPlaceMark: MKPlacemark?
    var discloseTrailSelectedOption = "Before the meet"
    var discloseSelectMode =  AppService.shared.walking
    var discloseTrailEnabled = true
      var SelectedText:String!
      var trackingOptionSelectedText:String!
    var validationController = ValidationController()
    var meetContacts = SearchContactsVC()
    let appSharedPrefernce = AppSharedPreference()
    let networkStatus = NetworkStatus()
      var appService = AppService.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    let trackingOptionsDropdown: DropDown = DropDown()
    let trackingTimeOptionsDropdown: DropDown = DropDown()
    let modeofTransportDropdown = DropDown()

    var participantFisheyeIds = [String]()
    var minutesForTrail = "5"
    var titleValid = true
    var locationValid = true
    var timeStartValid = true
    var timeEndValid = true
    var dateStartValid = true
    var dateEndValid = true

    var trackStatusValid = true
    var participantsValid = true
    var idtoken = ""
    var startDateTime = ""
    var endDateTime = ""
    var startDateTimeUTC: Date?
    var endDateTimeUTC: Date?
    var selectedTrailOptionIndex = 0
    var selectedTrailTimeIndex = 0

    var placeHolderOne = ""
    var placeHolderTwo = ""
    var placeHolderThree = ""
    var placeHolderFour = ""
    var placeHolderFive = ""
    var validStartTime = ""
    var InvalidStartTime = ""
    var validEndTime = ""
    var InvalidEndTime = ""
    var validStartDate = ""
    var InvalidStartDate = ""
    var validEndDate = ""
    var InvalidEndDate = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        self.appService = AppService.shared
        self.englishmodeArray = self.modeOfTransportArray
        initializeAllComponents()
        setLocalizationText()
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
        initializeMode()
    }

    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.CreateMeetVC)
    }

    @IBAction func modeOfTransportButtonTappedbtn(_ sender: Any) {
        if  modeofTransportDropdown.isHidden {
            self.modeofTransportDropdown.show()
        } else {
            self.modeofTransportDropdown.hide()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    func initializeAllComponents() {
        self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.discloseTrailPicker.backgroundColor = UIColor.duckEggBlue()
        self.discloseTrailTimePickerView.backgroundColor = UIColor.duckEggBlue()
        self.discloseTrailCustomTextField.text = self.appService.localtrackingOptionArray[0].optionTranslatedName
        

        self.selectModeOfTransport.text = appService.trasportModeArray[0].translatedText

        if appService.localTransportModeArray[0].actualText != nil
        {
             self.SelectedText = appService.localTransportModeArray[0].actualText
        }else{
             self.SelectedText = appService.trasportModeArray[0].actualText
        }
      
        self.createMeetScrollView.delegate = self
        ScreenLoader.shared.stopLoader()
        self.discloseTrailTimeCustomTextField.text = "5"+" "+self.signupmsg.minutes
        discloseTrailCustomTextField.isUserInteractionEnabled = false
        discloseTrailTimeCustomTextField.isUserInteractionEnabled = false
        meetLocationCustomTextField.isUserInteractionEnabled = false
        self.titleCustomTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.meetLocationCustomTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.titleCustomTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
        self.meetLocationCustomTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

      discloseTrailData = MeetMeTrackingOptions.trackingOption.map { $0.optionTranslatedName }
      

        trackingOptionsDropdown.anchorView = discloseTrailButton
        trackingOptionsDropdown.dataSource = discloseTrailData

        self.trackingTimeOptionsDropdown.anchorView = self.discloseTrailTimeButton
        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
      if MeetMeTrackingOptions.trackingOption.count > 0 {
            MeetMeTrackingOptions.trackingOption[0].trackingTime.forEach {vars in
                self.discloseTrailTimeData.append(String(vars as! Int) +  signupmsg.minutes)
            }
        }

        self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
        trackingOptionsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.discloseTrailCustomTextField.text = self.discloseTrailData[index]
            
            let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionTranslatedName == item ) } )
            
            if objArray != nil && objArray.count > 0
            {
                  self.discloseTrailSelectedOption = self.discloseTrailData[0]
                  self.trackingOptionSelectedText = objArray[0].optionName
                 
            }else{
//                  self.discloseTrailSelectedOption = self.discloseTrailData[0]
//                  self.trackingOptionSelectedText = objArray[0].optionName
            }
            
            
            
            if  MeetMeTrackingOptions.trackingOption[index].isTimeRequired! {

                self.discloseTrailTimeView.isHidden = false
            } else {

                self.discloseTrailTimeView.isHidden = true
            }
            switch index {
            case 0:
                self.discloseTrailSelectedOption =  self.trackingOptionSelectedText
            
                self.discloseTrailEnabled = true

                self.discloseTrailTimeData = []
                self.selectedTrailOptionIndex = 0
                MeetMeTrackingOptions.trackingOption[0].trackingTime.forEach {vars in
                  self.discloseTrailTimeData.append(String(vars as! Int) + "  "+self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            case 1:
                self.discloseTrailSelectedOption = self.trackingOptionSelectedText
               
                self.discloseTrailEnabled = true
                self.discloseTrailTimeData = []
                self.selectedTrailOptionIndex = 1
                MeetMeTrackingOptions.trackingOption[1].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            case 2:
                self.discloseTrailSelectedOption = self.trackingOptionSelectedText
                
                self.minutesForTrail =  "0"
                self.discloseTrailEnabled = true
                self.selectedTrailOptionIndex = 2
                self.discloseTrailTimeData = []
                MeetMeTrackingOptions.trackingOption[2].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + " "+self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            case 3:
                self.discloseTrailSelectedOption = self.trackingOptionSelectedText
              
                self.minutesForTrail =  "0"
                self.discloseTrailEnabled = false
                self.selectedTrailOptionIndex = 3
                self.discloseTrailTimeData = []

                MeetMeTrackingOptions.trackingOption[3].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + "  "+self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break

            default:
                self.discloseTrailSelectedOption = self.discloseTrailData[index]
                self.discloseTrailEnabled = true
                self.selectedTrailOptionIndex = index
                self.discloseTrailTimeData = []
                MeetMeTrackingOptions.trackingOption[index].trackingTime.forEach {vars in
                    self.discloseTrailTimeData.append(String(vars as! Int) + "  "+self.signupmsg.minutes)
                }
                self.trackingTimeOptionsDropdown.dataSource = self.discloseTrailTimeData
                break
            }
        }

        self.trackingTimeOptionsDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.discloseTrailTimeCustomTextField.text = self.discloseTrailTimeData[index]
            self.selectedTrailTimeIndex = index
            self.minutesForTrail =  String(MeetMeTrackingOptions.trackingOption[self.getSelectedOption()].trackingTime[index] as! Int)
        }
    }

    func getSelectedOption() -> Int {
      guard let isSelected = MeetMeTrackingOptions.trackingOption.index(where: { $0.optionName == self.discloseTrailCustomTextField.text})
            else {
                return 0
        }
        return isSelected
    }
    /************************** Loader functions start here **************************/
    func startLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }

    }

    func stopLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.stopLoader()
        }

    }

    /************************** Loader functions end here **************************/

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contactsTobeAdded.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        navigateToContactsScreen()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateMeetParticipantCollectionViewCell", for: indexPath) as! CreateMeetParticipantCollectionViewCell

        let contactObj = contactsTobeAdded[indexPath.row]
        if contactObj.name != "" && contactObj.name != " "{
            cell.participantNameLabel.text = validationController.getFirstChar(givenString: contactObj.name!)
        } else {
            cell.participantNameLabel.text = "Unknown"
        }

        cell.participantImageView?.sd_setImage(with: URL(string: contactObj.picture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        cell.participantImageView.layer.cornerRadius =   cell.participantImageView.frame.size.height / 2
        cell.participantImageView.layer.masksToBounds = true
        return cell
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
        if scrollView.contentOffset.x<0 {
            scrollView.contentOffset.x = 0
        }
    }

    @IBAction func onMapButtonTapped(_ sender: Any) {

        let mapVC = MapViewController.instantiateFromStoryboardWithIdentifier(identifier: "MapViewController")
        mapVC.addressSearchDelegate = self as AddressSearchNew
        if  self.meetLatitude != 0.0 &&  self.meetLongitude != 0.0 {
            mapVC.originalLong = self.meetLongitude
            mapVC.originalLat = self.meetLatitude
        }

        let navController = UINavigationController(rootViewController: mapVC)
        self.present(navController, animated: true, completion: nil)

    }
    //  Start tracking time
    @IBAction func onTimeButtonTapped(_ sender: Any) {
        DatePickerDialog().show(self.appService.selectStratTime, doneButtonTitle: self.appService.done, cancelButtonTitle: self.appService.cancel, datePickerMode: .time) {
            (date) -> Void in
            if let dt = date {
                let formatterForEndtime = DateFormatter()
                formatterForEndtime.dateFormat = "HH:mm"
                self.meetStartTimeLabel.text = formatterForEndtime.string(from: dt)
                self.meetStartTimeLabel.textColor = UIColor.black
            }
        }
    }

    //  End tracking time
    @IBAction func onEndTimeButtonTapped(_ sender: Any) {
        DatePickerDialog().show(self.appService.selectEndTime, doneButtonTitle: self.appService.done, cancelButtonTitle: self.appService.cancel, datePickerMode: .time) {
            (date) -> Void in
            if let dt = date {
                let formatterForEndtime = DateFormatter()
                formatterForEndtime.dateFormat = "HH:mm"
                self.meetEndTimeLabel.text = formatterForEndtime.string(from: dt)
                self.meetEndTimeLabel.textColor = UIColor.black
            }
        }
    }

    //  Start tracking date
    @IBAction func onDateDropdownTapped(_ sender: Any) {
        DatePickerDialog().show(self.appService.selectStartDate, doneButtonTitle: self.appService.done, cancelButtonTitle: self.appService.cancel, minimumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/YYYY"
                self.meetStartDateLabel.text = formatter.string(from: dt)
                self.meetStartDateLabel.textColor = UIColor.black
            }
        }
    }

    //  End tracking date

    @IBAction func onEndDateDropdownTapped(_ sender: Any) {
        DatePickerDialog().show(self.appService.selectEndDate, doneButtonTitle: self.appService.done, cancelButtonTitle: self.appService.cancel, minimumDate: Date(), datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/YYYY"
                self.meetEndDateLabel.text = formatter.string(from: dt)
                self.meetEndDateLabel.textColor = UIColor.black
            }
        }
    }

    // For Start Tracking Picker view
    @IBAction func onDiscloseTrailTapped(_ sender: Any) {

        if  trackingOptionsDropdown.isHidden {
            self.trackingOptionsDropdown.show()
        } else {
            self.trackingOptionsDropdown.hide()
        }

    }

    @IBAction func onSaveMeetButtonTapped(_ sender: Any) {
        onMeetMeButtonTapped(meetingTitle: titleCustomTextField.text!, startdate: meetStartDateLabel.text!, Enddate: meetEndDateLabel.text!, startTime: meetStartTimeLabel.text!, EndTime: meetEndTimeLabel.text!)
    }

    //edited by pritam
    func onMeetMeButtonTapped(meetingTitle: String, startdate: String, Enddate: String, startTime: String, EndTime: String) {
        //For title validation
        if meetingTitle.count == 0 {
            self.titleCustomTextField.configOnError(withPlaceHolder: self.appService.invalidTitle)
                placeHolderOne = titleCustomTextField.placeholder!
            titleValid = false
        } else {
            titleValid = true
            self.titleCustomTextField.configOnErrorCleared(withPlaceHolder: self.appService.title)
                placeHolderTwo = titleCustomTextField.placeholder!

        }

        //For location validation
        if meetLocationCustomTextField.text?.count == 0 {
            self.meetLocationCustomTextField.configOnError(withPlaceHolder: self.appService.invalidLocation)
            locationValid = false
        } else {
            locationValid = true
        }

        //For start Time validation
        if startTime == "HH:mm" || !validationController.isValidMeetTime(givenString: startTime) {
            self.meetStartTimeLabel.textColor = UIColor.red
            timeStartValid = false
            InvalidStartTime = "Invalid startTime"
        } else {
            self.meetStartTimeLabel.textColor = UIColor.black
            timeStartValid = true
            validStartTime = "valid startTime"
        }

        //For end Time validation
        if EndTime == "HH:mm" || !validationController.isValidMeetTime(givenString: EndTime) {
            self.meetEndTimeLabel.textColor = UIColor.red
            timeEndValid = false
            InvalidEndTime = "Invalid endTime"
        } else {
            self.meetEndTimeLabel.textColor = UIColor.black
            timeEndValid = true
            validEndTime = "valid endTime"
        }

        //For start date validation
        if  startdate == "dd/MM/yyyy" || !validationController.isValidMeetDate(givenString: startdate) {
            self.meetStartDateLabel.textColor = UIColor.red
            InvalidStartDate = "Invalid start date"
            dateStartValid = false

        } else {
            dateStartValid = true
            validStartDate = "valid start date"
        }

        //For end date validation
        if Enddate == "dd/MM/yyyy" || !validationController.isValidMeetDate(givenString: Enddate) {
            self.meetEndDateLabel.textColor = UIColor.red
            dateEndValid = false
            InvalidEndDate = "Invalid EndDate"
        } else {
            dateEndValid = true
            validEndDate = "valid EndDate"
        }

        let tempStartDateTime = "\(meetStartTimeLabel.text!)" + " " + "\(meetStartDateLabel.text!)"
        let tempEndDateTime = "\(meetEndTimeLabel.text!)" + " " + "\(meetEndDateLabel.text!)"

        if meetStartTimeLabel.text! != "HH:mm" &&  meetEndTimeLabel.text! != "HH:mm" &&  meetStartDateLabel.text! != "dd/MM/yyyy" && meetEndDateLabel.text! != "dd/MM/yyyy"{

            startDateTime = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempStartDateTime) )
            endDateTime = validationController.get_Date_time_from_UTC_timeSwift(givenDateTime: validationController.getTimeStampFromStringDate(stringDate: tempEndDateTime) )
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
            dateFormatter.timeZone = NSTimeZone.local
            endDateTimeUTC = dateFormatter.date(from: endDateTime)!
            startDateTimeUTC = dateFormatter.date(from: startDateTime)!
            if !(startDateTimeUTC! < endDateTimeUTC!) {
                self.dateEndValid = false
                self.timeEndValid = false
                self.meetEndDateLabel.textColor = UIColor.red
                self.meetEndTimeLabel.textColor = UIColor.red
            } else {
                self.dateEndValid = true
                self.timeEndValid = true
                self.meetEndDateLabel.textColor = UIColor.black
                self.meetEndTimeLabel.textColor = UIColor.black
            }
            let startDateTimeChecker = validationController.getTimeStampFromStringDate(stringDate: validationController.getTodaysDateTimeValidation())*1000
            let startDateTimeTemp = validationController.getTimeStampFromStringDate(stringDate: startDateTime)*1000

            if startDateTimeTemp > startDateTimeChecker {

                self.meetStartTimeLabel.textColor = UIColor.black
                self.meetStartDateLabel.textColor = UIColor.black
                timeStartValid = true
                dateStartValid = true
            } else {

                self.meetStartTimeLabel.textColor = UIColor.red
                self.meetStartDateLabel.textColor = UIColor.red
                timeStartValid = false
                  dateStartValid = false
            }
      }
      
      //Contact validation
      if contactsTobeAdded.count == 0 {
            addParticipantLabel.textColor = UIColor.red
            participantsValid = false
      } else {
            addParticipantLabel.textColor = UIColor.frenchBlue70()
            participantsValid = true
        }

        if titleValid && locationValid && timeStartValid && timeEndValid && dateStartValid && dateEndValid && participantsValid {
            guard networkStatus.isNetworkReachable()
                else {

                    self.view.makeToast(appService.checkInternetConnectionMessage, duration: AppService.shared.secondTimeInterval, position: .bottom)
                    return
            }
            let status = CLLocationManager.authorizationStatus()
           
            
            switch status {
            case .authorized, .authorizedWhenInUse, .authorizedAlways :
                self.createMeetCall()
            case .denied,.notDetermined:
                
                self.stopLoader()
                let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                    alert.dismiss(animated: true, completion: nil)
                })
                present(alert, animated: true)
                break
            default :
                self.stopLoader()
                break
            }

        } else {
            self.view.makeToast(appService.detailsIncomplete, duration: AppService.shared.secondTimeInterval, position: .bottom)
        }

      }

    @IBAction func onDiscloseTrialTimeTapped(_ sender: Any) {

        if  trackingTimeOptionsDropdown.isHidden {
            self.trackingTimeOptionsDropdown.show()
        } else {
            self.trackingTimeOptionsDropdown.hide()
        }
    }

    @IBAction func onAddParticipantButtonTapped(_ sender: Any) {
        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Create MeetMe", action: "Add participant Button clicked", label: "User is redirected to Add Participant Page", value: 0)
        navigateToContactsScreen()
    }

    @IBAction func onBackButtonClicked(_ sender: Any) {

        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Create MeetMe", action: "Back Button clicked", label: "User asked for confirmation", value: 0)
        //self.dismiss(animated: true, completion: nil)
        self.dualActionPopUp(heading: self.appService.createMeet, subheading: self.appService.createMeetSubHeding, action: "prevoiusPageConfirmed", method: "previousPageConfirmation", module: "NA")
        self.dualActionPopUpVCController()

    }

    // Function to fill up the Pop up details
    func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, status: String) {
        self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
        self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
        self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
        self.appSharedPrefernce.setAppSharedPreferences(key: "closestatus", value: status)
    }
    // End of Function to fill up the Pop up details

    //Function to call PopUp
    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
        nextVC.redirectProtocol = self as  RedirectToPrivacyControl
        self.present(nextVC, animated: true, completion: nil)
    }

    func dualActionPopUp(heading: String, subheading: String, action: String, method: String, module: String) {
        self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
        self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
        self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)

    }

    func dualActionPopUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "AllPopupDualActionVC") as! AllPopupDualActionVC
        nextVC.redirectProtocol = self as  RedirectToPrivacyControl
        self.present(nextVC, animated: true, completion: nil)
    }

    @objc func setLocalizationText() {
      DispatchQueue.main.async {
            self.titleCustomTextField.placeholder = self.appService.title
            self.meetLocationCustomTextField.placeholder =  self.appService.location
            self.tapToLocateLabel.text = self.appService.locateMap
            self.meetStartsAtLabel.text =  self.appService.startAt
            self.meetEndsAtLabel.text =  self.appService.endsAt
            self.addParticipantLabel.text =  self.appService.addParticipant
            self.participantInviteLabel.text =  self.appService.partcanInvite
            self.startTrackingLabel.text =  self.appService.startTracking
            self.startTrackingTimeLabel.text =  self.appService.time
            self.createNewMeetingLabel.text = self.appService.createanewmeet
            self.mode.text = self.appService.mode
      }
    }

     func initializeMode() {
     
      
        self.modeOfTransportArray = MeetMeTrackingOptions.trackingMode
        self.modeTableView.isHidden = true
        self.modeofTransportDropdown.anchorView = self.modeTableView
        self.modeofTransportDropdown.dataSource = self.modeOfTransportArray
        self.modeofTransportDropdown.width = 200
        
        self.modeofTransportDropdown.selectionAction = {
                [unowned self] (index: Int, item: String) in
                self.selectModeOfTransport.text = item
                self.selectmodeArray = index
                
            let objArray = self.appService.localTransportModeArray.filter( { return ($0.translatedText == item ) } )
            
         
             if objArray != nil && objArray.count > 0
             {
                  self.selectModeOfTransport.text = objArray[0].translatedText
                  self.SelectedText = objArray[0].actualText
             } else{
                  self.selectModeOfTransport.text = objArray[0].translatedText
                  self.SelectedText = objArray[0].actualText
            }
            
        }

    }

    // End of Function to call PopUp

    func navigateToContactsScreen() {

        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        self.meetContacts = storyboard.instantiateViewController(withIdentifier: "SearchContactsVC") as! SearchContactsVC
        self.meetContacts.sendSelectedContacts = self as SendSelectedContacts
        self.meetContacts.currentMeet = contactsTobeAdded
        self.meetContacts.selectedContacts = contactsTobeAdded.count
        self.meetContacts.view.frame = self.createMeetCompleteView.bounds
        self.addChild(self.meetContacts)
        self.meetContacts.didMove(toParent: self)
        self.meetContacts.view.tag = 0
        self.meetContacts.view.frame.origin.y = -self.view.frame.size.height
        self.meetContacts.view.frame.origin.y = self.createMeetCompleteView.bounds.origin.y
        self.meetContacts.view.frame.origin.x = -self.view.frame.size.width
        self.meetContacts.view.frame.origin.x = self.createMeetCompleteView.bounds.origin.x
        self.createMeetCompleteView.addSubview(self.meetContacts.view)

    }

    // function to validate textfields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {

        switch textField {

        case  titleCustomTextField:

//            if (titleCustomTextField.text!.characters.count > 19) {
//                titleCustomTextField.deleteBackward()
//            }
            self.titleUnderlineImageView.image = UIImage(named: "BlueUnderLine")

            if titleCustomTextField.text?.count == 0 {
                self.titleCustomTextField.configOnError(withPlaceHolder: self.appService.invalidTitle)
                titleValid = false
            } else {
                titleValid = true
                self.titleCustomTextField.configOnErrorCleared(withPlaceHolder: self.appService.title)
            }

        case meetLocationCustomTextField:

            self.locationUnderlineImageView.image = UIImage(named: "BlueUnderLine")

            if meetLocationCustomTextField.text?.count == 0 {
                self.meetLocationCustomTextField.configOnError(withPlaceHolder: self.appService.invalidLocation)
                locationValid = false
            } else {
                locationValid = true

            }
        default:
            break
        }
    }

    //End of Function to Handle Hide show of the underline
    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
        switch textField {

        case  titleCustomTextField:

            self.titleUnderlineImageView.image = UIImage(named: "BlackUnderLine")

        case meetLocationCustomTextField:
            self.locationUnderlineImageView.image = UIImage(named: "BlackUnderLine")
            self.meetLocationCustomTextField.configOnErrorCleared(withPlaceHolder: self.appService.location)

        default:
            break
        }
    }

    func textField(_ textField: UICustomTextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 10 // Bool
    }

}

extension CreateMeetVC: AddressSearchNew, SendSelectedContacts, RedirectToPrivacyControl {
    func redirect(toRedirect: Bool) {
        NotificationCenter.default.post(name: REFRESH_MEET_NOTIFICATION, object: nil)
        self.view.removeFromSuperview()
    }

    func addContacts(addedContacts: [ContactObj]) {

        self.contactsTobeAdded = addedContacts
        self.meetParticipantsCollectionView.reloadData()
        if contactsTobeAdded.count>0 {
            addParticipantLabel.textColor = UIColor.frenchBlue70()
            participantsValid = true
        }
        mainContactObj = []
        for addedContact in addedContacts {
            let contactObj = [
                "contactId": addedContact.contactId,
                "fisheyeId": addedContact.contactFEId
                ] as [String: Any]
            mainContactObj.append(contactObj as AnyObject)
        }
        self.createMeetCompleteView.willRemoveSubview(self.meetContacts.view)
    }

    func addrssObjectFrom(place: MKPlacemark?) {
        if let placemark = place {
            meetLocationCustomTextField.text = ""
            selectedPlaceMark = placemark
            if let name = placemark.name {
                meetLocationCustomTextField.text?.append(name)
            }

            if let loc = placemark.locality {
                meetLocationCustomTextField.text?.append(", \(loc)")
            }
            if let state = placemark.administrativeArea {
                meetLocationCustomTextField.text?.append(", \(state)")
            }
            if let country = placemark.country {
                meetLocationCustomTextField.text?.append(", \(country)")
            }
            if let postalCode = placemark.postalCode {
                meetLocationCustomTextField.text?.append(" - \(postalCode)")
            }
            if let latitude = placemark.location?.coordinate.latitude {
                self.meetLatitude = latitude
            }

            if let longitude = placemark.location?.coordinate.longitude {
                self.meetLongitude = longitude
            }
            self.meetLocationCustomTextField.configOnErrorCleared(withPlaceHolder: self.appService.location)
            let newPosition = meetLocationCustomTextField.beginningOfDocument
            meetLocationCustomTextField.selectedTextRange = meetLocationCustomTextField.textRange(from: newPosition, to: newPosition)
        } else {
            selectedPlaceMark = nil
        }
    }
}

extension CreateMeetVC {

    func createMeetCall() {
        self.startLoader()
        self.saveMeetButton.isUserInteractionEnabled = false
        var trackTime = 0
      if MeetMeTrackingOptions.trackingOption[self.selectedTrailOptionIndex].trackingTime.count>0 {
            trackTime = MeetMeTrackingOptions.trackingOption[self.selectedTrailOptionIndex].trackingTime[self.selectedTrailTimeIndex] as! Int
        } else {
            trackTime = 0
        }
        let parameters =
            [
                "istrackingEnabled": self.discloseTrailEnabled,
                "trackingStatus": self.discloseTrailSelectedOption,
                "trackingTime": trackTime,
                "trackingMode": SelectedText,
                "isSeen": true,
                "meetme": [
                    "title": titleCustomTextField.text!,
                    "titleSearchKey": titleCustomTextField.text!.lowercased(),
                    "locationName": meetLocationCustomTextField.text!,
                    "locationNameSearchKey": meetLocationCustomTextField.text!.lowercased(),
                    "latitude": String(self.meetLatitude),
                    "longitude": String(self.meetLongitude),
                    "startDateTime": validationController.getTimeStampFromStringDate(stringDate: startDateTime)*1000,
                    "endDateTime": validationController.getTimeStampFromStringDate(stringDate: endDateTime)*1000
                ],
                "participants": self.mainContactObj
                ] as [String: Any]
        
        let awsURL = AppConfig.meetmeCreateMeetUrl
        
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, response: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                self.stopLoader()
              // self.saveMeetButton.isUserInteractionEnabled = true
                let response = data["statusCode"] as? String ?? ""
                if response == "200"{
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Create MeetMe", action: "Save MeetMe Button clicked", label: "User creates MeetMe Succesfully", value: 0)
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.createMeetSuccessString)
                    }

                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                        self.view.hideToast()
                        self.saveMeetButton.isUserInteractionEnabled = true
                        NotificationCenter.default.post(name: REFRESH_MEET_NOTIFICATION, object: nil)
                        self.view.removeFromSuperview()
                    })

                } else {
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Create MeetMe", action: "Save MeetMe Button clicked", label: "User's create MeetMe attempts Failed", value: 0)
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.createMeetFailureString)
                         self.saveMeetButton.isUserInteractionEnabled = true
                    }

                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                        self.view.hideToast()

                    })
                }

            } else {

                DispatchQueue.main.async {
                    self.stopLoader()
                    self.saveMeetButton.isUserInteractionEnabled = true
                    self.view.makeToast(self.appService.createMeetFailureString)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                    self.view.hideToast()
                })
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Create MeetMe", action: "Save MeetMe Button clicked", label: "User's create MeetMe attempts Failed", value: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }

}
