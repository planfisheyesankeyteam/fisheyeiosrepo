//
//  MeetMeDetailVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 05/02/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Foundation
import Toast_Swift
import MapKit

class MeetMeDetailVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, MKMapViewDelegate {
    
    // Outlet Variables
    @IBOutlet weak var meetMeDetailCompleteView: UIView!
    @IBOutlet weak var meetmeDetailLoadingView: UIView!
    @IBOutlet weak var participantsLabel: UILabel!
    
    //Tab View
    @IBOutlet weak var meetmeDetailTabView: UIView!
    @IBOutlet weak var hostImageView: UIImageView!
    @IBOutlet weak var AddressMeetMe: UILabel!
    @IBOutlet weak var DateTimeMeetMe: UILabel!
    @IBOutlet weak var titleOfMeet: UILabel!
    
   
    @IBOutlet weak var acceptMeetmeButton: UIButton!
    @IBOutlet weak var declineMeetmeButton: UIButton!
    @IBOutlet weak var stopButtonImageView: UIImageView!
    @IBOutlet weak var acceptMeetmeImageView: UIImageView!
    @IBOutlet weak var declineMeetmeImageView: UIImageView!
    @IBOutlet weak var deleteButtonImageView: UIImageView!
    @IBOutlet weak var editButtonImageView: UIImageView!
    
    @IBOutlet weak var stopMeetMeButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backButtonImageView: UIImageView!
    @IBOutlet weak var trackingEvent: UILabel!
    @IBOutlet weak var trackingTime: UILabel!
    @IBOutlet weak var mapViewContainer: UIView!
    
    // Participant View
    @IBOutlet weak var participantCollectionView: UICollectionView!
    @IBOutlet weak var addParticipantButton: UIButton!
    @IBOutlet weak var addParticipantButtonImageView: UIImageView!
    @IBOutlet weak var participantTableViewContainer: UIView!
    
   
    //Content
    @IBOutlet weak var meetMeDetailMapView: MKMapView!
    @IBOutlet weak var participantTableView: UITableView!
    @IBOutlet weak var zoomInButton: UIButton!
    @IBOutlet weak var zoomOutButton: UIButton!
    @IBOutlet weak var participantTableViewCloseButton: UIButton!
    
    //Capsule Content
    @IBOutlet weak var capsuleView: UIView!
    @IBOutlet weak var capsuleImageView: UIImageView!
    @IBOutlet weak var capsuleNotificationCounLabelt: UILabel!
    @IBOutlet weak var capsuleButton: UIButton!
    @IBOutlet weak var capsuleLockButton: UIButton!
    @IBOutlet weak var capsuleLockImageView: UIImageView!
    @IBOutlet weak var earliestTracking: UILabel!
    @IBOutlet weak var unlockCapsuleLabel: UILabel!
    @IBOutlet weak var networkErrorLabel: UILabel!
    
    // Class Variables
    let appSharedPrefernce = AppSharedPreference()
    let validationController = ValidationController()
    let items = ParticipantVariables()
    let networkStatus = NetworkStatus()
    var appService = AppService.shared
    var pulseMsg = PulseToastMsgHeadingSubheadingLabels.shared
    var signmsg = SignupToastMsgHeadingSubheadingLabels.shared
    var fetchedMeeting = MeetMeLogbookVariables()
    var editMeetVc = EditMeetVC()
    var meetContacts = SearchContactsVC()
    var meetmeCapsuleVC = MeetMeCapsuleVC()
    var capsuleObj = MeetMeCapsule()
    var timer: Timer?
    var hostIdb = ""
    var loggedInUserIsPartOfMeeting = false
    
    //String Variables
    var sections = ["Organizer", "Accepted", "Not Yet Responded", "Declined"]
    var timeInterval: Double = 30
    var sectionData: [Int: [ParticipantVariables]] = [:]
    var fisheyeId = ""
    var idtoken = ""
    var selectedMeetmeId: String!
    
    //Boolean variables
    var hasOpenedContacts = false
    var hideTableSection0 = false
    var hideTableSection1 = false
    var hideTableSection2 = false
    var isHost = false
    var tableViewHidden = true
    var isCapsuleLocked = false
    var isUpcoming = false
    var isTrackingEnabled = false
    var reloadLogbook = false
    
    var responseStatus = ""
    var hostId = ""
    var hostLat = 0.0
    var hostLong = 0.0
    var trackingStatusText = ""
    var trackingTimeText = ""
    var meetingObj = MeetMeLogbookVariables()
    var acceptedList =  [ParticipantVariables()]
    var declinedList =  [ParticipantVariables()]
    var notYetRespondedList = [ParticipantVariables()]
    var participantList =  [ParticipantVariables()]
    var participantListForEvent =  [ParticipantVariables()]
    var participantListForCollection =  [ParticipantVariables()]
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    var isEventFlag = false
    var isEventFlagDetail = false
    var isHostDetail = false
    var isHostUser = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        self.appService = AppService.shared
        participantTableView.delegate = self
        meetMeDetailMapView.delegate = self
        self.fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
        timeInterval = MeetMeTrackingOptions.mapResetTimeInterval ?? 30.00
        if !hasOpenedContacts {
            self.participantTableViewContainer?.alpha = 0.0
            tableViewHidden = true
            mapViewContainer.isHidden = false
        } else {
            participantTableViewContainer.isHidden = false
            tableViewHidden = false
            mapViewContainer.isHidden = true
        }
        fetchedMeeting.acceptedList = []
        fetchedMeeting.declinedList = []
        fetchedMeeting.notYetRespondedList = []
        fetchedMeeting.participantListForCollection = []
        fetchedMeeting.hostList = []
        sectionData =   [0: self.fetchedMeeting.hostList, 1: self.fetchedMeeting.acceptedList, 2: self.fetchedMeeting.notYetRespondedList, 3: self.fetchedMeeting.declinedList]
        fetchMyMeeting()
        setLocalizationText()
        getMessageCount()
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.MeetMeDetailVC)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func hostImageButton(_ sender: Any) {
        if(self.fetchedMeeting.hostList.count > 0 && self.hostLat != 0.0 && self.hostLong != 0.0){
            let destinationAnnotation = CustomAnnotation()
            let latHost = self.hostLat
            let longHost = self.hostLong
            var loc = CLLocationCoordinate2D(latitude:latHost,longitude:longHost)
            self.hostIdb = self.fetchedMeeting.hostList[0].fisheyeId ?? ""
            var region = MKCoordinateRegion()
            region = MKCoordinateRegion(center: loc, latitudinalMeters: 100, longitudinalMeters: 100)
            meetMeDetailMapView?.setRegion(region, animated: true)
        }else{
            DispatchQueue.main.async {
                self.view.makeToast(self.appService.dontHaveHostLocation)
            }
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                self.view.hideToast()
            })
        }
    }
    @IBAction func meetTitleButton(_ sender: Any) {
        _ = CustomAnnotation()
        
        let lat = (self.fetchedMeeting.latitude! as NSString).doubleValue
        let long = (self.fetchedMeeting.longitude! as NSString).doubleValue
        let loc = CLLocationCoordinate2D(latitude:lat,longitude:long)
        
        var region = MKCoordinateRegion()
        region = MKCoordinateRegion(center: loc, latitudinalMeters: 100, longitudinalMeters: 100)
        
        meetMeDetailMapView?.setRegion(region, animated: true)
        
    }
    @IBAction func onBackButtonPressed(_ sender: Any) {
        stopTimer()
        if reloadLogbook {
            NotificationCenter.default.post(name: REFRESH_MEET_NOTIFICATION, object: nil)
        }
        self.view.removeFromSuperview()
    }
    
    @IBAction func declineOrEditButtonClicked(_ sender: Any) {
        if editButtonImageView.isHidden {
            self.declineMeetMe()
        } else {
            stopTimer()
            let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
            editMeetVc = storyboard.instantiateViewController(withIdentifier: "EditMeetVC") as! EditMeetVC
            editMeetVc.meetmeDetailObj = fetchedMeeting
            editMeetVc.reloadMeetDetail = self as ReloadProtocol
            editMeetVc.view.frame = self.meetMeDetailCompleteView.bounds
            self.addChild(editMeetVc)
            editMeetVc.view.frame.origin.y = -self.view.frame.size.height
            self.meetMeDetailCompleteView.addSubview(editMeetVc.view)
            editMeetVc.view.frame.origin.y = self.meetMeDetailCompleteView.bounds.origin.y
        }
        
    }
    
    /************************** Loader functions start here **************************/
    func startLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
        
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.stopLoader()
        }
        
    }
    
    /************************** Loader functions end here **************************/
    func declineMeetMe() {
        self.dualActionPopUp(heading: self.appService.DeclinemeetHeading, subheading: self.appService.DeclineSubHeading, action: "declineMeetSuccess", method: "declineMeetme", module: "NA" )
        self.dualActionPopUpVCController()
        
    }
    
    func deleteMeet() {
        self.dualActionPopUp(heading: self.appService.DeleteMeetHeading, subheading: self.appService.DeleteMeetmeSubHeading, action: "deleteMeetSuccess", method: "deleteMeetme", module: "NA" )
        self.dualActionPopUpVCController()
        
    }
    
    @objc func removeParticipantFromTableView(_sender: Any) {
        let removeButton = _sender as! MyButton
        let sectionToDelete = removeButton.section
        let rowToDelete = removeButton.row
        self.appSharedPrefernce.setAppSharedPreferences(key: "sectionToDelete", value: sectionToDelete!)
        self.appSharedPrefernce.setAppSharedPreferences(key: "rowToDelete", value: rowToDelete!)
        self.dualActionPopUp(heading: self.appService.removeParticipant, subheading: self.appService.removeParticipantSubHeading, action: "removeParticipantSuccess", method: "removeParticipant", module: "NA" )
        self.dualActionPopUpVCController()
    }
    
    /*****************Functions for pop up with two options start here ****************/
    func dualActionPopUp(heading: String, subheading: String, action: String, method: String, module: String) {
        self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
        self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
        self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
    }
    
    func dualActionPopUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "AllPopupDualActionVC") as! AllPopupDualActionVC
        nextVC.reloadLogbook = self as RedirectedToMeetmeLogbookVC
        self.present(nextVC, animated: true, completion: nil)
    }
    /*****************Functions for pop up with two options end here ****************/
    
    @IBAction func zoomInButtonTapped(_ sender: Any) {
        var region: MKCoordinateRegion? = meetMeDetailMapView?.region
        region?.span.latitudeDelta /= (2.0 as  CLLocationDegrees)
        region?.span.longitudeDelta /= (2.0 as  CLLocationDegrees)
        meetMeDetailMapView?.setRegion(region!, animated: true)
    }
    
    @IBAction func zoomOutButtonTapped(_ sender: Any) {
        var region: MKCoordinateRegion? = meetMeDetailMapView?.region
        let latitude = min((region?.span.latitudeDelta)! * 2.0, 180.0)
        let longitude = min((region?.span.longitudeDelta)! * 2.0, 180.0)
        region?.span.latitudeDelta = latitude
        region?.span.longitudeDelta = longitude
        meetMeDetailMapView?.setRegion(region!, animated: true)
    }
    
    @IBAction func onAcceptOrDeleteButtonClicked(_ sender: Any) {
        if(deleteButtonImageView.isHidden) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Meetme", bundle: nil)
            let acceptVC = storyBoard.instantiateViewController(withIdentifier: "MeetMeTrackingOptionDialogueVC") as! MeetMeTrackingOptionDialogueVC
            acceptVC.modalPresentationStyle = .overCurrentContext
            acceptVC.acceptedDelegate = self as AcceptMeet
            self.present(acceptVC, animated: true, completion: nil)
        } else {
            stopTimer()
            deleteMeet()
        }
        
    }
    
    @IBAction func onStopMeetMeButtonTapped(_ sender: Any) {
        self.dualActionPopUp(heading: self.appService.stopMeeting, subheading: self.appService.stopMeetingSubHeading, action: "stopMeetSucces", method: "stopMeetme", module: "NA" )
        self.dualActionPopUpVCController()
        
    }
    
    @IBAction func onAddParticipantButtonClicked(_ sender: Any) {
        stopTimer()
        openContactsVc()
    }
    
    @IBAction func onTableViewCloseButtonTapped(_ sender: Any) {
        HideShowTableView()
    }
    
    @IBAction func onCapsuleLockButtonTapped(_ sender: Any) {
        self.stopTimer()
        openVerifyMasterKeyVC(forCapsuleStatusChange: true)
    }
    
    @IBAction func onCapsuleButtonTapped(_ sender: Any) {
        self.stopTimer()
        if self.isCapsuleLocked {
            self.openVerifyMasterKeyVC(forCapsuleStatusChange: false)
        } else {
            if(self.loggedInUserIsPartOfMeeting==true){
                self.openCapsule()
            }else{
                self.view.makeToast(self.appService.userNotPartOfTheMeeting)
            }
        }
    }
    
    /*********************** Collection View functions starts here **************************/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let participants = fetchedMeeting.participantListForCollection
        if participants.count>4 {
            return 5
        } else {
            let returnVariable = participants.count + 1
            return returnVariable
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let participants = fetchedMeeting.participantListForCollection
        if participants.count>4 {
            if indexPath.row == 5 {
                HideShowTableView()
            }
        } else {
            if indexPath.row == participants.count {
                HideShowTableView()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MeetMeParticipantsCollectionViewCell", for: indexPath) as! MeetMeParticipantsCollectionViewCell
        
        if fetchedMeeting.participantListForCollection.count<4 {
            if indexPath.row != fetchedMeeting.participantListForCollection.count {
                let participant = fetchedMeeting.participantListForCollection[indexPath.row]
                if (participant.participantName == nil || (participant.participantName?.isEmpty)! || participant.participantName == " " || participant.participantName == "")
                {
                    
                    cell.participantsName.text = "Unknown"
                }else{
                    cell.participantsName.text = participant.participantName
                }
                
                cell.participantsName.isHidden = false
                cell.participantsImage?.sd_setImage(with: URL(string: participant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                cell.participantsImage.layer.cornerRadius = cell.participantsImage.frame.size.width / 2
                cell.participantsImage.clipsToBounds = true
                cell.participantsImage.layer.borderWidth = 1.5
                
                if participant.isAccepted! {
                    cell.participantsImage.layer.borderColor = UIColor.green.cgColor
                    cell.acceptedSign.isHidden = false
                    cell.rejectedSign.isHidden = true
                } else if participant.isDeclined! {
                    cell.participantsImage.layer.borderColor = UIColor.red.cgColor
                    cell.acceptedSign.isHidden = true
                    cell.rejectedSign.isHidden = false
                } else {
                    cell.participantsImage.layer.borderColor = UIColor.clear.cgColor
                    cell.acceptedSign.isHidden = true
                    cell.rejectedSign.isHidden = true
                }
            } else {
                cell.participantButton.addTarget(self, action: #selector(hideTableViewWithGesture(sender:)), for: UIControl.Event.touchUpInside)
                cell.participantsImage.image = UIImage(named: "threeDotsOption")
                cell.participantsImage.layer.borderColor =  UIColor.clear.cgColor
                cell.participantsName.isHidden = true
                cell.acceptedSign.isHidden = true
                cell.rejectedSign.isHidden = true
            }
        } else {
            if indexPath.row != 4 {
                let participant = fetchedMeeting.participantListForCollection[indexPath.row]
                cell.participantsName.text = participant.participantName
                cell.participantsName.isHidden = false
                cell.participantsImage?.sd_setImage(with: URL(string: participant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                cell.participantsImage.layer.cornerRadius = cell.participantsImage.frame.size.width / 2
                cell.participantsImage.clipsToBounds = true
                cell.participantsImage.layer.borderWidth = 1.5
                
                if participant.isAccepted! {
                    cell.participantsImage.layer.borderColor = UIColor.green.cgColor
                    cell.acceptedSign.isHidden = false
                    cell.rejectedSign.isHidden = true
                } else if participant.isDeclined! {
                    cell.participantsImage.layer.borderColor = UIColor.red.cgColor
                    cell.acceptedSign.isHidden = true
                    cell.rejectedSign.isHidden = false
                } else {
                    cell.participantsImage.layer.borderColor =  UIColor.clear.cgColor
                    cell.acceptedSign.isHidden = true
                    cell.rejectedSign.isHidden = true
                }
            } else {
                cell.participantButton.addTarget(self, action: #selector(hideTableViewWithGesture(sender:)), for: UIControl.Event.touchUpInside)
                cell.participantsImage.image = UIImage(named: "threeDotsOption")
                cell.participantsImage.layer.borderColor =  UIColor.clear.cgColor
                cell.participantsName.isHidden = true
                cell.acceptedSign.isHidden = true
                cell.rejectedSign.isHidden = true
            }
        }
        return cell
    }
    
    @objc func hideTableViewWithGesture(sender: Any) {
        HideShowTableView()
    }
    
    /*********************** Collection View functions end here **************************/
    
    /************************ Table View functions starts here ***************************/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch (section) {
        case 0:
            return (sectionData[section]?.count)!
            
        case 1 :
            if hideTableSection0 {
                return 0
            } else {
                return (sectionData[section]?.count)!
            }
        case 2:
            if hideTableSection1 {
                return 0
            } else {
                return (sectionData[section]?.count)!
            }
        case 3 :
            if hideTableSection2 {
                return 0
            } else {
                return (sectionData[section]?.count)!
            }
        default:
            return (sectionData[section]?.count)!
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch (section) {
        case 0 :
            
            let view = UIView()
            let title = UILabel()
            title.font = UIFont(name: "Helvetica Neue", size: 10)
            title.text = self.sections[section]
            title.textColor = UIColor.purple
            title.font = UIFont.italicSystemFont(ofSize: 10)
            title.frame = CGRect(x: 19, y: 5, width: 250, height: 20 )
            view.addSubview(title)
            view.backgroundColor = UIColor.lightGrey()
            return view
            
        case 1 :
            if hideTableSection0 {
                return UIView(frame: CGRect.zero)
            } else {
                let view = UIView()
                let title = UILabel()
                title.font = UIFont(name: "Helvetica Neue", size: 10)
                title.text = self.sections[section]
                title.textColor = UIColor.acceptGreen()
                title.font = UIFont.italicSystemFont(ofSize: 10)
                title.frame = CGRect(x: 19, y: 5, width: 250, height: 20 )
                view.addSubview(title)
                view.backgroundColor = UIColor.lightGrey()
                return view
            }
        case 2:
            if hideTableSection1 {
                return UIView(frame: CGRect.zero)
            } else {
                let view = UIView()
                let title = UILabel()
                title.font = UIFont(name: "Helvetica Neue", size: 10)
                title.text = self.sections[section]
                title.textColor = UIColor.drkGrey()
                title.font = UIFont.italicSystemFont(ofSize: 10)
                title.frame = CGRect(x: 19, y: 5, width: 250, height: 20 )
                view.addSubview(title)
                view.backgroundColor = UIColor.lightGrey()
                return view
            }
        case 3 :
            if hideTableSection2 {
                return UIView(frame: CGRect.zero)
            } else {
                let view = UIView()
                let title = UILabel()
                title.font = UIFont(name: "Helvetica Neue", size: 10)
                title.text = self.sections[section]
                title.textColor = UIColor.red
                title.font = UIFont.italicSystemFont(ofSize: 10)
                title.frame = CGRect(x: 19, y: 5, width: 250, height: 20 )
                view.addSubview(title)
                view.backgroundColor = UIColor.lightGrey()
                return view
            }
        default:
            return nil
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (section) {
        case 0 :
            
            return 25
            
        case 1 :
            if hideTableSection0 {
                return 0
            } else {
                return 25
            }
        case 2:
            if hideTableSection1 {
                return 0
            } else {
                return 25
            }
        case 3 :
            if hideTableSection2 {
                return 0
            } else {
                return 25
            }
        default:
            return 0
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowcell = Bundle.main.loadNibNamed("SearchContactsTableViewCell", owner: self, options: nil)?.first as! SearchContactsTableViewCell
        let participantObj = sectionData[indexPath.section]![indexPath.row]
        rowcell.contactCellImageView?.sd_setImage(with: URL(string: participantObj.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        rowcell.contactCellImageView.layer.cornerRadius = rowcell.contactCellImageView.frame.size.width / 2
        rowcell.contactCellImageView.clipsToBounds = true
        rowcell.contactCellImageView.layer.borderWidth = 1.5
        rowcell.backgroundColor = UIColor.lightGrey()
        rowcell.contactCellView.backgroundColor = UIColor.lightGrey()
        let todaysDate = validationController.getTodaysDateInTimeStamp()
        
        if self.isHost && todaysDate<fetchedMeeting.startDateTime! {
            rowcell.contactCellCheckboxImage.image =  UIImage(named: "DeclinedFilled")
            rowcell.contactCellCheckboxImage.isHidden = false
            rowcell.contactCellCheckboxButton.isHidden = false
        } else {
            
            rowcell.contactCellCheckboxImage.isHidden = true
            rowcell.contactCellCheckboxButton.isHidden = true
        }
        
        if participantObj.isHost {
            rowcell.isFisheyeStrip.backgroundColor = UIColor.purpleRing()
            rowcell.contactCellCheckboxImage.isHidden = true
            rowcell.contactCellCheckboxButton.isHidden = true
        }
        
        rowcell.contactCellCheckboxButton.row = indexPath.row
        rowcell.contactCellCheckboxButton.section = indexPath.section
        rowcell.contactCellCheckboxButton.addTarget(self, action: #selector(removeParticipantFromTableView(_sender:)), for: UIControl.Event.touchUpInside)
        
        rowcell.contactCellPhoneNumber.text = participantObj.participantPhoneNumber
        rowcell.contactCellEmailId.text = participantObj.participantEmail
        
        if (participantObj.participantName == nil || (participantObj.participantName?.isEmpty)! || participantObj.participantName == " " || participantObj.participantName == "")
        {
            
            rowcell.contactCellName.text = "Unknown"
        }else{
            rowcell.contactCellName.text = participantObj.participantName
        }
        
        
        if (participantObj.fisheyeId != nil && !(participantObj.fisheyeId?.isEmpty)!)
        {
            rowcell.isFisheyeStrip.backgroundColor = UIColor.purpleRing()
        }else{
            rowcell.isFisheyeStrip.isHidden = true
        }
        
        
        if (!self.isEventFlagDetail){
        
        if((participantObj.participantPhoneNumber?.isEmpty)! && !(participantObj.participantEmail?.isEmpty)!) {
            rowcell.stackViewDetails.isHidden = true
            rowcell.singleDetailDisplay.isHidden = false
            rowcell.singleDetailDisplay.text = participantObj.participantEmail
            
        } else if(!(participantObj.participantPhoneNumber?.isEmpty)! && (participantObj.participantEmail?.isEmpty)!) {
            
            rowcell.stackViewDetails.isHidden = true
            rowcell.singleDetailDisplay.isHidden = false
            rowcell.singleDetailDisplay.text = participantObj.participantPhoneNumber
        } else if(!(participantObj.participantPhoneNumber?.isEmpty)! && !(participantObj.participantEmail?.isEmpty)!) {
            rowcell.singleDetailDisplay.isHidden = true
            rowcell.stackViewDetails.isHidden = false
            
        } else {
            rowcell.contactCellName.text = "Unknown"
            rowcell.contactCellPhoneNumber.text = "n/a"
            rowcell.contactCellEmailId.text = "n/a"
            rowcell.seperator.isHidden = true
        }
        }else{
            if((participantObj.participantName?.contains("USER"))!){
                
//                rowcell.contactCellName.text = "Unknown"
                rowcell.contactCellPhoneNumber.text = "n/a"
                rowcell.contactCellEmailId.text = "n/a"
                rowcell.seperator.isHidden = true
            }
        }
        switch( indexPath.section ) {
        case 0 :
            rowcell.contactCellImageView.layer.borderColor = UIColor.purple.cgColor
        case 1:
            rowcell.contactCellImageView.layer.borderColor = UIColor.green.cgColor
        case 2:
            rowcell.contactCellImageView.layer.borderColor = UIColor.lightGray.cgColor
        case 3:
            rowcell.contactCellImageView.layer.borderColor = UIColor.red.cgColor
        default:
            rowcell.contactCellImageView.layer.borderColor = UIColor.lightGray.cgColor
        }
        return rowcell
        
    }
    
    /*********************** Table View functions end here **************************/
    
    func fetchMyMeeting() {
        DispatchQueue.main.async {
            self.networkErrorLabel.isHidden = true
            // self.view.makeToast(self.appService.checkInternetConnectionMessage)
        }
        startLoader()
        DispatchQueue.main.async {
            self.meetmeDetailLoadingView.isHidden = false
            self.meetMeDetailCompleteView.isHidden = true
        }
        guard networkStatus.isNetworkReachable()
            else {
                stopLoader()
                
                DispatchQueue.main.async {
                    self.networkErrorLabel.isHidden = false
                    // self.view.makeToast(self.appService.checkInternetConnectionMessage)
                }
                return
        }
        
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        
        let awsURL = AppConfig.updateMeetUrl(meetMeId: selectedMeetmeId!)
        
        let graphqlUrl = (awsURL)
        guard let URL = URL(string: graphqlUrl) else { return }
        let sessionConfig = URLSessionConfiguration.default
        
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return }
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                self.stopLoader()
                
                if let meetingResponseObj = json as? [String: Any] {
                    let responseCode = meetingResponseObj["statusCode"] as? String ?? ""
                    var meetIdArray: [String] = []
                    if responseCode == "200"{
                        if let meetingsList = meetingResponseObj["meetme"] as? [String: Any] {
                            self.fetchedMeeting.meetmeId = meetingsList["meetmeId"] as? String ??  ""
                            self.fetchedMeeting.meetmeLocation = meetingsList["locationName"] as? String ??  ""
                            self.fetchedMeeting.meetmeTitle = meetingsList["title"] as? String ??  ""
                            self.fetchedMeeting.latitude = meetingsList["latitude"] as? String ??  ""
                            self.fetchedMeeting.longitude = meetingsList["longitude"] as? String ??  ""
                            self.fetchedMeeting.startDateTime = meetingsList["startDateTime"] as? Double
                            self.fetchedMeeting.endDateTime = meetingsList["endDateTime"] as? Double
                            self.fetchedMeeting.createdDate = meetingsList["createdDate"] as? Double
                            self.fetchedMeeting.trackingTime = meetingsList["trackingTime"] as? Double
                            self.fetchedMeeting.acceptedList = []
                            self.fetchedMeeting.declinedList = []
                            self.fetchedMeeting.participantList = []
                            self.fetchedMeeting.participantListForEvent = []
                            self.fetchedMeeting.participantListForCollection = []
                            self.fetchedMeeting.notYetRespondedList = []
                            self.fetchedMeeting.hostList = []
                            
                            // Hitesh
                            if ((meetingsList["isEventCreated"]) != nil) {
                                let tempEventFlag = meetingsList["isEventCreated"] as? Int ?? 0
                                if (tempEventFlag == 1){
                                    self.fetchedMeeting.isEventFlag = true
                                    self.isEventFlagDetail = true
                                } else {
                                    self.fetchedMeeting.isEventFlag = false
                                }
                            } else {
                                self.fetchedMeeting.isEventFlag = false
                            }
                            
                            
                            if let participantList = meetingsList["participants"] as? [[String: Any]] {
                                let participantObj1 =  ParticipantVariables()
                                let participantObj2 =  ParticipantVariables()
                                for k in 0..<participantList.count {
                                   
                                    if(participantList[k]["ishost"] as? Bool==true){
                                        participantObj1.participantId =  participantList[k]["participantId"] as? String ??  ""
                                        participantObj1.fisheyeId =  participantList[k]["fisheyeId"]as? String ??  ""
                                        participantObj1.isHost =  participantList[k]["ishost"] as? Bool
                                        participantObj1.participantEmail =  participantList[k]["participantEmailId"] as? String ??  ""
                                        participantObj1.participantName =  participantList[k]["participantName"] as? String ??  ""
                                        participantObj1.participantPhoneNumber = participantList[k]["participantPhoneNumber"] as? String ??  ""
                                        participantObj1.participantPicture = participantList[k]["participantPicture"] as? String ??  ""
                                        participantObj1.isAccepted =  participantList[k]["isaccepted"]as? Bool
                                        participantObj1.isDeclined =  participantList[k]["isdecline"]as? Bool
                                        participantObj1.istrackingEnabled =  participantList[k]["istrackingEnabled"]  as? Bool
                                        participantObj1.trackingTime = participantList[k]["trackingTime"]  as? String ??  ""
                                        participantObj1.trackingStatus = participantList[k]["trackingStatus"] as? String ??  ""
                                        participantObj1.contactId = participantList[k]["contactId"] as? String ??  ""
                                        participantObj1.trackingMode = participantList[k]["trackingMode"] as? String ??  ""
                                        participantObj1.isCapsuleLocked = participantList[k]["isCapsuleLocked"] as? Bool
                                        participantObj1.isSeen = participantList[k]["isSeen"] as? Bool
                                        if participantObj1.fisheyeId == self.fisheyeId{
                                            if participantObj1.isSeen != true {
                                                meetIdArray.append(self.fetchedMeeting.meetmeId as? String ?? "")
                                            }
                                        }
                                    }
                                    if((participantList[k]["fisheyeId"]as? String ??  "")==(self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId")as? String ??  "")){
                                        participantObj2.participantId =  participantList[k]["participantId"] as? String ??  ""
                                        participantObj2.fisheyeId =  participantList[k]["fisheyeId"]as? String ??  ""
                                        participantObj2.isHost =  participantList[k]["ishost"] as? Bool
                                        participantObj2.participantEmail =  participantList[k]["participantEmailId"] as? String ??  ""
                                        participantObj2.participantName =  participantList[k]["participantName"] as? String ??  ""
                                        participantObj2.participantPhoneNumber = participantList[k]["participantPhoneNumber"] as? String ??  ""
                                        participantObj2.participantPicture = participantList[k]["participantPicture"] as? String ??  ""
                                        participantObj2.isAccepted =  participantList[k]["isaccepted"]as? Bool
                                        participantObj2.isDeclined =  participantList[k]["isdecline"]as? Bool
                                        participantObj2.istrackingEnabled =  participantList[k]["istrackingEnabled"]  as? Bool
                                        participantObj2.trackingTime = participantList[k]["trackingTime"]  as? String ??  ""
                                        participantObj2.trackingStatus = participantList[k]["trackingStatus"] as? String ??  ""
                                        participantObj2.contactId = participantList[k]["contactId"] as? String ??  ""
                                        participantObj2.trackingMode = participantList[k]["trackingMode"] as? String ??  ""
                                        participantObj2.isCapsuleLocked = participantList[k]["isCapsuleLocked"] as? Bool
                                        participantObj2.isSeen = participantList[k]["isSeen"] as? Bool
                                        if participantObj2.fisheyeId == self.fisheyeId{
                                            if participantObj2.isSeen != true {
                                                meetIdArray.append(self.fetchedMeeting.meetmeId as? String ?? "")
                                            }
                                        }
                                    }
                                   
                                }
                                for j in 0..<participantList.count {
                                   
                                    let participantObj =  ParticipantVariables()
                                    participantObj.isHost =  participantList[j]["ishost"] as? Bool
                                    participantObj.participantId =  participantList[j]["participantId"] as? String ??  ""
                                    participantObj.fisheyeId =  participantList[j]["fisheyeId"]as? String ??  ""
                                    if( participantObj.fisheyeId  == self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? "" ){
                                        self.loggedInUserIsPartOfMeeting=true
                                    }
                                    participantObj.participantEmail =  participantList[j]["participantEmailId"] as? String ??  ""
                                    participantObj.participantName =  participantList[j]["participantName"] as? String ??  ""
                                    participantObj.participantPhoneNumber = participantList[j]["participantPhoneNumber"] as? String ??  ""
                                    participantObj.participantPicture = participantList[j]["participantPicture"] as? String ??  ""
                                    participantObj.isAccepted =  participantList[j]["isaccepted"]as? Bool
                                    participantObj.isDeclined =  participantList[j]["isdecline"]as? Bool
                                    participantObj.istrackingEnabled =  participantList[j]["istrackingEnabled"]  as? Bool
                                    participantObj.trackingTime = participantList[j]["trackingTime"]  as? String ??  ""
                                    participantObj.trackingStatus = participantList[j]["trackingStatus"] as? String ??  ""
                                    participantObj.contactId = participantList[j]["contactId"] as? String ??  ""
                                    participantObj.trackingMode = participantList[j]["trackingMode"] as? String ??  ""
                                    participantObj.isCapsuleLocked = participantList[j]["isCapsuleLocked"] as? Bool
                                    participantObj.isSeen = participantList[j]["isSeen"] as? Bool
                                    if participantObj.fisheyeId == self.fisheyeId{
                                        if participantObj.isSeen != true {
                                            meetIdArray.append(self.fetchedMeeting.meetmeId as? String ?? "")
                                        }
                                    }
          
                                    
                                    if (self.isEventFlagDetail) {
                                    if( participantObj1.fisheyeId  == participantObj2.fisheyeId ) {
                                        
                                        self.fetchedMeeting.participantList.append(participantObj)
                                        
                                        if !participantObj.isHost! {
                                            self.fetchedMeeting.participantListForCollection.append(participantObj)
                                        }
                                        if participantObj.isHost! {
                                            self.fetchedMeeting.hostList.append(participantObj)
                                        }
                                        if participantObj.isAccepted! {
                                            self.fetchedMeeting.acceptedList.append(participantObj)
                                        } else if participantObj.isDeclined! {
                                            self.fetchedMeeting.declinedList.append(participantObj)
                                        } else if !participantObj.isAccepted! && !participantObj.isDeclined! && !participantObj.isHost! {
                                            self.fetchedMeeting.notYetRespondedList.append(participantObj)
                                        }
                                        
                                        
                                        
                                        
                                    }else{
                                        if (self.fetchedMeeting.participantListForCollection.count < 1){
                                           
                                                self.fetchedMeeting.participantListForCollection.append(participantObj1)
                                                self.fetchedMeeting.participantList.append(participantObj1)
                                                self.fetchedMeeting.hostList.append(participantObj1)
                                                if participantObj2.isAccepted! {
                                                    self.fetchedMeeting.acceptedList.append(participantObj2)
                                                } else if participantObj2.isDeclined! {
                                                    self.fetchedMeeting.declinedList.append(participantObj2)
                                                } else if !participantObj2.isAccepted! && !participantObj2.isDeclined! && !participantObj2.isHost! {
                                                    self.fetchedMeeting.notYetRespondedList.append(participantObj2)
                                                }
                                        }
                                       
                                        
                                    }
                                        if(participantObj2.fisheyeId==participantObj.fisheyeId){
                                            self.fetchedMeeting.participantListForEvent.append(participantObj)
                                        }
                                        
                                }else if (self.fetchedMeeting.isEventFlag == false) {
                                        self.fetchedMeeting.participantList.append(participantObj)
                                        
                                        if !participantObj.isHost! {
                                            self.fetchedMeeting.participantListForCollection.append(participantObj)
                                        }
                                        if participantObj.isHost! {
                                            self.fetchedMeeting.hostList.append(participantObj)
                                        }
                                        if participantObj.isAccepted! {
                                            self.fetchedMeeting.acceptedList.append(participantObj)
                                        } else if participantObj.isDeclined! {
                                            self.fetchedMeeting.declinedList.append(participantObj)
                                        } else if !participantObj.isAccepted! && !participantObj.isDeclined! && !participantObj.isHost! {
                                            self.fetchedMeeting.notYetRespondedList.append(participantObj)
                                        }
                                    }
                                }
                                
                            }
                                //}
                                if meetIdArray.count > 0 {
                                    self.reduceNotificationCountAndMarkSeen(countToReduce: meetIdArray.count, meetIdArray : meetIdArray)
                                }
                            } else {
                                self.stopLoader()
                            }
                            DispatchQueue.main.async {
                                if (self.isEventFlagDetail) {
                                    if(self.isHostDetail) {
                                        self.editButtonImageView.isHidden = true
                                        self.declineMeetmeButton.isHidden = true
                                        self.setEventDetailForOrganizer()
                                        self.updateLocation()
                                        self.scheduleTimer()
                                    }
                                    else{
                                        self.editButtonImageView.isHidden = true
                                        self.declineMeetmeButton.isHidden = true
                                        self.setEventDetail()
                                        self.updateLocation()
                                        self.scheduleTimer()
                                    }
                                }
                                else {
                                    self.setDetail()
                                    self.updateLocation()
                                    self.scheduleTimer()
                                }
                                
                               
                            }
                        }
                    }
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
            
        }
        
        func setEventDetailForOrganizer(){
            self.editButtonImageView.isHidden = true
            self.declineMeetmeButton.isHidden = true
            let startTime = validationController.convert_time_from_UTC_time(givenUTCtime: fetchedMeeting.startDateTime!)
            let startDate = validationController.convert_date_from_UTC_time(givenUTCdate: fetchedMeeting.startDateTime!)
            let endTime = validationController.convert_time_from_UTC_time(givenUTCtime: fetchedMeeting.endDateTime!)
            let endDate = validationController.convert_date_from_UTC_time(givenUTCdate: fetchedMeeting.endDateTime!)
            self.titleOfMeet.text = fetchedMeeting.meetmeTitle
            self.AddressMeetMe.text = fetchedMeeting.meetmeLocation
            self.DateTimeMeetMe.text = "\(startDate) - \(startTime) \(self.pulseMsg.labelBetweenSDandED) \(endDate) - \(endTime)"
            if fetchedMeeting.acceptedList.count==0 && fetchedMeeting.declinedList.count == 0 {
                hideTableSection0 = true
                hideTableSection2 = true
            } else if fetchedMeeting.acceptedList.count==0 && fetchedMeeting.notYetRespondedList.count==0 {
                hideTableSection0 = true
                hideTableSection1 = true
            } else if fetchedMeeting.notYetRespondedList.count==0 && fetchedMeeting.declinedList.count == 0 {
                hideTableSection1 = true
                hideTableSection2 = true
            }
            sectionData =   [0: self.fetchedMeeting.hostList, 1: self.fetchedMeeting.acceptedList, 2: self.fetchedMeeting.notYetRespondedList, 3: self.fetchedMeeting.declinedList]
            
            for meetParticipant in fetchedMeeting.participantList {
                
                if meetParticipant.isHost {
                    hostImageView?.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                    hostImageView.layer.cornerRadius = hostImageView.frame.size.width / 2
                    hostImageView.clipsToBounds = true
                    self.hostId = meetParticipant.fisheyeId!
                }
                
                if self.fisheyeId == meetParticipant.fisheyeId {
                    self.isHost = meetParticipant.isHost
                    //self.isCapsuleLocked = meetParticipant.isCapsuleLocked!
                    
                    //For capsule Lock
                    if meetParticipant.isCapsuleLocked! {
                        capsuleLockImageView.image = UIImage(named: "CapsuleLocked")
                    } else {
                        capsuleLockImageView.image = UIImage(named: "CapsuleUnlocked")
                    }
                    
                    
                    //For tracking status
                    if meetParticipant.istrackingEnabled == true && meetParticipant.trackingTime != nil && meetParticipant.trackingTime != "" {
                        self.trackingStatusText = meetParticipant.trackingStatus!
                        isTrackingEnabled = true
                        self.trackingTimeText = meetParticipant.trackingTime!
                        
                        
                        let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionName == meetParticipant.trackingStatus ) } )
                        
                        if objArray != nil && objArray.count > 0
                        {
                            //trackingEvent.text = meetParticipant.trackingStatus
                            trackingEvent.text = objArray[0].optionTranslatedName
                        }else{
                            trackingEvent.text = appService.localtrackingOptionArray[0].optionTranslatedName
                        }
                        
                        
                        var trackingMinutes = self.signmsg.minutes
                        trackingTime.text = " \(meetParticipant.trackingTime ?? "-") " + "\(trackingMinutes)"
                        earliestTracking.text = self.appService.earliestTrackingLabel + " :"
                        
                        
                    }else {
                        trackingTime.text = ""
                        trackingEvent.text = ""
                        earliestTracking.text = ""
                    }
                    
                    //For Upcoming and past details
                    let todaysDate = validationController.getTodaysDateInTimeStamp()
                    if todaysDate>fetchedMeeting.startDateTime! {
                        
                        acceptMeetmeImageView.isHidden = true
                        stopButtonImageView.isHidden = true
                        declineMeetmeImageView.isHidden = true
                        editButtonImageView.isHidden = true
                        deleteButtonImageView.isHidden = true
                        acceptMeetmeButton.isHidden = true
                        declineMeetmeButton.isHidden = true
                        addParticipantButton.isHidden = true
                        addParticipantButtonImageView.isHidden = true
                    } else if todaysDate>fetchedMeeting.startDateTime! && todaysDate < fetchedMeeting.endDateTime! && meetParticipant.isHost {
                        
                        changeDetailsForStopMeetMe()
                        
                    } else {
                        //For accept,decline host status
                        if meetParticipant.isHost {
                            isHost = true
                            
                            hostImageView?.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                            hostImageView.layer.cornerRadius = hostImageView.frame.size.width / 2
                            hostImageView.clipsToBounds = true
                            acceptMeetmeImageView.isHidden = true
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            editButtonImageView.isHidden = true
                            deleteButtonImageView.isHidden = true
                            
                        } else if meetParticipant.isAccepted! {
                            
                            acceptMeetmeImageView.isHidden = false
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = false
                            editButtonImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            deleteButtonImageView.isHidden = true
                            acceptMeetmeImageView.image = UIImage(named: "AcceptedFilled")
                            declineMeetmeImageView.image = UIImage(named: "DeclinedHollow")
                        } else if meetParticipant.isDeclined! {
                            
                            acceptMeetmeImageView.isHidden = false
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = false
                            editButtonImageView.isHidden = true
                            deleteButtonImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            declineMeetmeImageView.image = UIImage(named: "DeclinedFilled")
                            acceptMeetmeImageView.image = UIImage(named: "AcceptedHollow")
                        }
                    }
                }
            }
            let annotation = CustomAnnotation()
            let lat = (fetchedMeeting.latitude! as NSString).doubleValue
            let long = (fetchedMeeting.longitude! as NSString).doubleValue
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.title = "Destination"
            annotation.annotationType = "Destination"
            meetMeDetailMapView.addAnnotation(annotation)
            meetMeDetailMapView.showAnnotations([annotation], animated: true)
            meetMeDetailMapView.showsScale = true
            if fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 &&  self.fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = false
                hideTableSection2 = false
            } else if fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = false
                hideTableSection2 = true
            } else if self.fetchedMeeting.notYetRespondedList.count != 0 &&  fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = true
                hideTableSection1 = false
                hideTableSection2 = false
            } else if fetchedMeeting.acceptedList.count != 0 && fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = true
                hideTableSection2 = false
            } else if  fetchedMeeting.acceptedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = true
                hideTableSection2 = true
            } else if  fetchedMeeting.notYetRespondedList.count != 0 {
                
                hideTableSection0 = true
                hideTableSection1 = false
                hideTableSection2 = true
            } else if fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = true
                hideTableSection1 = true
                hideTableSection2 = false
            }
            
            
            self.participantTableView.reloadData()
            self.participantCollectionView.reloadData()
            self.meetMeDetailCompleteView.isHidden = false
            self.meetmeDetailLoadingView.isHidden = true
            
            self.capsuleView.isHidden = true
            self.capsuleImageView.isHidden = true
            self.capsuleNotificationCounLabelt.isHidden = true
            self.capsuleButton.isHidden = true
            self.capsuleLockButton.isHidden = true
            self.capsuleLockImageView.isHidden = true
            self.unlockCapsuleLabel.isHidden = true
            self.editButtonImageView.isHidden = true
            self.declineMeetmeButton.isHidden = true
            self.deleteButtonImageView.isHidden = true
        }
        
        func setEventDetail() {
            let startTime = validationController.convert_time_from_UTC_time(givenUTCtime: fetchedMeeting.startDateTime!)
            let startDate = validationController.convert_date_from_UTC_time(givenUTCdate: fetchedMeeting.startDateTime!)
            let endTime = validationController.convert_time_from_UTC_time(givenUTCtime: fetchedMeeting.endDateTime!)
            let endDate = validationController.convert_date_from_UTC_time(givenUTCdate: fetchedMeeting.endDateTime!)
            self.titleOfMeet.text = fetchedMeeting.meetmeTitle
            self.AddressMeetMe.text = fetchedMeeting.meetmeLocation
            self.DateTimeMeetMe.text = "\(startDate) - \(startTime) \(self.pulseMsg.labelBetweenSDandED) \(endDate) - \(endTime)"
            if fetchedMeeting.acceptedList.count==0 && fetchedMeeting.declinedList.count == 0 {
                hideTableSection0 = true
                hideTableSection2 = true
            } else if fetchedMeeting.acceptedList.count==0 && fetchedMeeting.notYetRespondedList.count==0 {
                hideTableSection0 = true
                hideTableSection1 = true
            } else if fetchedMeeting.notYetRespondedList.count==0 && fetchedMeeting.declinedList.count == 0 {
                hideTableSection1 = true
                hideTableSection2 = true
            }
            sectionData =   [0: self.fetchedMeeting.hostList, 1: self.fetchedMeeting.acceptedList, 2: self.fetchedMeeting.notYetRespondedList, 3: self.fetchedMeeting.declinedList]
            
            for meetParticipant in self.fetchedMeeting.participantListForEvent {
                if meetParticipant.isHost {
                    hostImageView?.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                    hostImageView.layer.cornerRadius = hostImageView.frame.size.width / 2
                    hostImageView.clipsToBounds = true
                    self.hostId = meetParticipant.fisheyeId!
                }
                
                if self.fisheyeId == meetParticipant.fisheyeId {
                    self.isHost = meetParticipant.isHost
                    //self.isCapsuleLocked = meetParticipant.isCapsuleLocked!
                    
                    //For capsule Lock
                    if meetParticipant.isCapsuleLocked! {
                        capsuleLockImageView.image = UIImage(named: "CapsuleLocked")
                    } else {
                        capsuleLockImageView.image = UIImage(named: "CapsuleUnlocked")
                    }
                    
                     //For tracking status
                    if meetParticipant.istrackingEnabled == true && meetParticipant.trackingTime != nil && meetParticipant.trackingTime != "" {
                        self.trackingStatusText = meetParticipant.trackingStatus!
                        isTrackingEnabled = true
                        self.trackingTimeText = meetParticipant.trackingTime!
                        
                        
                        let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionName == meetParticipant.trackingStatus ) } )
                        
                        if objArray != nil && objArray.count > 0
                        {
                            //trackingEvent.text = meetParticipant.trackingStatus
                            trackingEvent.text = objArray[0].optionTranslatedName
                        }else{
                            trackingEvent.text = appService.localtrackingOptionArray[0].optionTranslatedName
                        }
                        
                        
                        var trackingMinutes = self.signmsg.minutes
                        trackingTime.text = " \(meetParticipant.trackingTime ?? "-") " + "\(trackingMinutes)"
                        earliestTracking.text = self.appService.earliestTrackingLabel + " :"
                        
                        
                    }else {
                        trackingTime.text = ""
                        trackingEvent.text = ""
                        earliestTracking.text = ""
                    }
                    if meetParticipant.isHost {
                        isHost = true
                        hostImageView?.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                        hostImageView.layer.cornerRadius = hostImageView.frame.size.width / 2
                        hostImageView.clipsToBounds = true
                        acceptMeetmeImageView.isHidden = true
                        stopButtonImageView.isHidden = true
                        declineMeetmeImageView.isHidden = true
                        stopMeetMeButton.isHidden = true
                        editButtonImageView.isHidden = true
                        deleteButtonImageView.isHidden = false
                        
                    } else if meetParticipant.isAccepted! {
                        acceptMeetmeImageView.isHidden = false
                        stopButtonImageView.isHidden = true
                        declineMeetmeImageView.isHidden = false
                        editButtonImageView.isHidden = true
                        stopMeetMeButton.isHidden = true
                        deleteButtonImageView.isHidden = true
                        acceptMeetmeImageView.image = UIImage(named: "AcceptedFilled")
                        declineMeetmeImageView.image = UIImage(named: "DeclinedHollow")
                    } else if meetParticipant.isDeclined! {
                        acceptMeetmeImageView.isHidden = false
                        stopButtonImageView.isHidden = true
                        declineMeetmeImageView.isHidden = false
                        editButtonImageView.isHidden = true
                        deleteButtonImageView.isHidden = true
                        stopMeetMeButton.isHidden = true
                        declineMeetmeImageView.image = UIImage(named: "DeclinedFilled")
                        acceptMeetmeImageView.image = UIImage(named: "AcceptedHollow")
                    }
                    
                    //For Upcoming and past details
                    let todaysDate = validationController.getTodaysDateInTimeStamp()
                    if todaysDate>fetchedMeeting.startDateTime! {
                        
                        acceptMeetmeImageView.isHidden = true
                        stopButtonImageView.isHidden = true
                        declineMeetmeImageView.isHidden = true
                        editButtonImageView.isHidden = true
                        deleteButtonImageView.isHidden = true
                        acceptMeetmeButton.isHidden = true
                        declineMeetmeButton.isHidden = true
                        addParticipantButton.isHidden = true
                        addParticipantButtonImageView.isHidden = true
                    } else if todaysDate>fetchedMeeting.startDateTime! && todaysDate < fetchedMeeting.endDateTime! && meetParticipant.isHost {
                        
                        changeDetailsForStopMeetMe()
                        
                    } else {
                        //For accept,decline host status
                        if meetParticipant.isHost {
                            isHost = true
                            hostImageView?.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                            hostImageView.layer.cornerRadius = hostImageView.frame.size.width / 2
                            hostImageView.clipsToBounds = true
                            acceptMeetmeImageView.isHidden = true
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            editButtonImageView.isHidden = false
                            deleteButtonImageView.isHidden = false
                            
                        } else if meetParticipant.isAccepted! {
                            acceptMeetmeImageView.isHidden = false
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = false
                            editButtonImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            deleteButtonImageView.isHidden = true
                            acceptMeetmeImageView.image = UIImage(named: "AcceptedFilled")
                            declineMeetmeImageView.image = UIImage(named: "DeclinedHollow")
                        } else if meetParticipant.isDeclined! {
                            acceptMeetmeImageView.isHidden = false
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = false
                            editButtonImageView.isHidden = true
                            deleteButtonImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            declineMeetmeImageView.image = UIImage(named: "DeclinedFilled")
                            acceptMeetmeImageView.image = UIImage(named: "AcceptedHollow")
                        }
                    }
                }
            }
            let annotation = CustomAnnotation()
            let lat = (fetchedMeeting.latitude! as NSString).doubleValue
            let long = (fetchedMeeting.longitude! as NSString).doubleValue
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.title = "Destination"
            annotation.annotationType = "Destination"
            meetMeDetailMapView.addAnnotation(annotation)
            meetMeDetailMapView.showAnnotations([annotation], animated: true)
            meetMeDetailMapView.showsScale = true
//            if fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 &&  self.fetchedMeeting.declinedList.count != 0 {
//
//                hideTableSection0 = false
//                hideTableSection1 = false
//                hideTableSection2 = false
//            } else if fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 {
//
//                hideTableSection0 = false
//                hideTableSection1 = false
//                hideTableSection2 = true
//            } else if self.fetchedMeeting.notYetRespondedList.count != 0 &&  fetchedMeeting.declinedList.count != 0 {
//
//                hideTableSection0 = true
//                hideTableSection1 = false
//                hideTableSection2 = false
//            } else if fetchedMeeting.acceptedList.count != 0 && fetchedMeeting.declinedList.count != 0 {
//
//                hideTableSection0 = false
//                hideTableSection1 = true
//                hideTableSection2 = false
//            } else if  fetchedMeeting.acceptedList.count != 0 {
//
//                hideTableSection0 = false
//                hideTableSection1 = true
//                hideTableSection2 = true
//            } else if  fetchedMeeting.notYetRespondedList.count != 0 {
//
//                hideTableSection0 = true
//                hideTableSection1 = false
//                hideTableSection2 = true
//            } else if fetchedMeeting.declinedList.count != 0 {
//
//                hideTableSection0 = true
//                hideTableSection1 = true
//                hideTableSection2 = false
//            }
//
            self.participantsLabel.text = "Organizer"
            self.participantTableView.reloadData()
            self.participantCollectionView.reloadData()
            self.meetMeDetailCompleteView.isHidden = false
            self.meetmeDetailLoadingView.isHidden = true
            //self.participantCollectionView.isHidden = true
            self.addParticipantButton.isHidden = true
            self.addParticipantButtonImageView.isHidden = true
            //self.participantTableViewContainer.isHidden = true
            
            self.capsuleView.isHidden = true
            self.capsuleImageView.isHidden = true
            self.capsuleNotificationCounLabelt.isHidden = true
            self.capsuleButton.isHidden = true
            self.capsuleLockButton.isHidden = true
            self.capsuleLockImageView.isHidden = true
            self.unlockCapsuleLabel.isHidden = true
            self.editButtonImageView.isHidden = true
            self.declineMeetmeButton.isHidden = true
            self.deleteButtonImageView.isHidden = true
            
        }
        
        func setDetail() {
            let startTime = validationController.convert_time_from_UTC_time(givenUTCtime: fetchedMeeting.startDateTime!)
            let startDate = validationController.convert_date_from_UTC_time(givenUTCdate: fetchedMeeting.startDateTime!)
            let endTime = validationController.convert_time_from_UTC_time(givenUTCtime: fetchedMeeting.endDateTime!)
            let endDate = validationController.convert_date_from_UTC_time(givenUTCdate: fetchedMeeting.endDateTime!)
            self.titleOfMeet.text = fetchedMeeting.meetmeTitle
            self.AddressMeetMe.text = fetchedMeeting.meetmeLocation
            self.DateTimeMeetMe.text = "\(startDate) - \(startTime) \(self.pulseMsg.labelBetweenSDandED) \(endDate) - \(endTime)"
            if fetchedMeeting.acceptedList.count==0 && fetchedMeeting.declinedList.count == 0 {
                hideTableSection0 = true
                hideTableSection2 = true
            } else if fetchedMeeting.acceptedList.count==0 && fetchedMeeting.notYetRespondedList.count==0 {
                hideTableSection0 = true
                hideTableSection1 = true
            } else if fetchedMeeting.notYetRespondedList.count==0 && fetchedMeeting.declinedList.count == 0 {
                hideTableSection1 = true
                hideTableSection2 = true
            }
            sectionData =   [0: self.fetchedMeeting.hostList, 1: self.fetchedMeeting.acceptedList, 2: self.fetchedMeeting.notYetRespondedList, 3: self.fetchedMeeting.declinedList]
            for meetParticipant in fetchedMeeting.participantList {
                
                if meetParticipant.isHost {
                    hostImageView?.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                    hostImageView.layer.cornerRadius = hostImageView.frame.size.width / 2
                    hostImageView.clipsToBounds = true
                    self.hostId = meetParticipant.fisheyeId!
                }
                
                if self.fisheyeId == meetParticipant.fisheyeId {
                    self.isHost = meetParticipant.isHost
                    self.isCapsuleLocked = meetParticipant.isCapsuleLocked!
                    
                    //For capsule Lock
                    if meetParticipant.isCapsuleLocked! {
                        capsuleLockImageView.image = UIImage(named: "CapsuleLocked")
                    } else {
                        capsuleLockImageView.image = UIImage(named: "CapsuleUnlocked")
                    }
                    
                    
                    //For tracking status
                    if meetParticipant.istrackingEnabled == true && meetParticipant.trackingTime != nil && meetParticipant.trackingTime != "" {
                        self.trackingStatusText = meetParticipant.trackingStatus!
                        isTrackingEnabled = true
                        self.trackingTimeText = meetParticipant.trackingTime!
                        
                        
                        let objArray = self.appService.localtrackingOptionArray.filter( { return ($0.optionName == meetParticipant.trackingStatus ) } )
                        
                        if objArray != nil && objArray.count > 0
                        {
                            //trackingEvent.text = meetParticipant.trackingStatus
                            trackingEvent.text = objArray[0].optionTranslatedName
                        }else{
                            trackingEvent.text = appService.localtrackingOptionArray[0].optionTranslatedName
                        }
                        
                        
                        var trackingMinutes = self.signmsg.minutes
                        trackingTime.text = " \(meetParticipant.trackingTime ?? "-") " + "\(trackingMinutes)"
                        earliestTracking.text = self.appService.earliestTrackingLabel + " :"
                        
                        
                    }else {
                        trackingTime.text = ""
                        trackingEvent.text = ""
                        earliestTracking.text = ""
                    }
                    
                    //For Upcoming and past details
                    let todaysDate = validationController.getTodaysDateInTimeStamp()
                    if todaysDate>fetchedMeeting.startDateTime! {
                        
                        acceptMeetmeImageView.isHidden = true
                        stopButtonImageView.isHidden = true
                        declineMeetmeImageView.isHidden = true
                        editButtonImageView.isHidden = true
                        deleteButtonImageView.isHidden = true
                        acceptMeetmeButton.isHidden = true
                        declineMeetmeButton.isHidden = true
                        addParticipantButton.isHidden = true
                        addParticipantButtonImageView.isHidden = true
                    } else if todaysDate>fetchedMeeting.startDateTime! && todaysDate < fetchedMeeting.endDateTime! && meetParticipant.isHost {
                        
                        changeDetailsForStopMeetMe()
                        
                    } else {
                        //For accept,decline host status
                        if meetParticipant.isHost {
                            isHost = true
                            hostImageView?.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
                            hostImageView.layer.cornerRadius = hostImageView.frame.size.width / 2
                            hostImageView.clipsToBounds = true
                            acceptMeetmeImageView.isHidden = true
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            editButtonImageView.isHidden = false
                            deleteButtonImageView.isHidden = false
                            
                        } else if meetParticipant.isAccepted! {
                            acceptMeetmeImageView.isHidden = false
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = false
                            editButtonImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            deleteButtonImageView.isHidden = true
                            acceptMeetmeImageView.image = UIImage(named: "AcceptedFilled")
                            declineMeetmeImageView.image = UIImage(named: "DeclinedHollow")
                        } else if meetParticipant.isDeclined! {
                            acceptMeetmeImageView.isHidden = false
                            stopButtonImageView.isHidden = true
                            declineMeetmeImageView.isHidden = false
                            editButtonImageView.isHidden = true
                            deleteButtonImageView.isHidden = true
                            stopMeetMeButton.isHidden = true
                            declineMeetmeImageView.image = UIImage(named: "DeclinedFilled")
                            acceptMeetmeImageView.image = UIImage(named: "AcceptedHollow")
                        }
                    }
                }
            }
            let annotation = CustomAnnotation()
            let lat = (fetchedMeeting.latitude! as NSString).doubleValue
            let long = (fetchedMeeting.longitude! as NSString).doubleValue
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.title = "Destination"
            annotation.annotationType = "Destination"
            meetMeDetailMapView.addAnnotation(annotation)
            meetMeDetailMapView.showAnnotations([annotation], animated: true)
            meetMeDetailMapView.showsScale = true
            if fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 &&  self.fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = false
                hideTableSection2 = false
            } else if fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = false
                hideTableSection2 = true
            } else if self.fetchedMeeting.notYetRespondedList.count != 0 &&  fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = true
                hideTableSection1 = false
                hideTableSection2 = false
            } else if fetchedMeeting.acceptedList.count != 0 && fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = true
                hideTableSection2 = false
            } else if  fetchedMeeting.acceptedList.count != 0 {
                
                hideTableSection0 = false
                hideTableSection1 = true
                hideTableSection2 = true
            } else if  fetchedMeeting.notYetRespondedList.count != 0 {
                
                hideTableSection0 = true
                hideTableSection1 = false
                hideTableSection2 = true
            } else if fetchedMeeting.declinedList.count != 0 {
                
                hideTableSection0 = true
                hideTableSection1 = true
                hideTableSection2 = false
            }
            
            self.participantTableView.reloadData()
            self.participantCollectionView.reloadData()
            self.meetMeDetailCompleteView.isHidden = false
            self.meetmeDetailLoadingView.isHidden = true
        }
        
        func removeParticipantApiCall(sectionToDelete: Int, rowToDelete: Int) {
            
            var participantToDelete = ParticipantVariables()
            guard networkStatus.isNetworkReachable()
                else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.checkInternetConnectionMessage)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    return
            }
            switch(sectionToDelete) {
                
            case 1:
                participantToDelete = fetchedMeeting.acceptedList[rowToDelete]
                break
            case 2:
                participantToDelete = fetchedMeeting.notYetRespondedList[rowToDelete]
                break
            case 3:
                participantToDelete = fetchedMeeting.declinedList[rowToDelete]
                break
            default:
                break
            }
            let deleteParticipant = DeleteParticipant(selectedParticipant: participantToDelete, meetmeId: fetchedMeeting.meetmeId!)
            deleteParticipant.addDidFinishBlockObserver { [unowned self] (operation, _) in
                let responseStatus = operation.responseStatus
                if responseStatus == "200"{
                    
                    switch(sectionToDelete) {
                    case 1:
                        self.fetchedMeeting.acceptedList.remove(at: rowToDelete)
                        break
                    case 2:
                        self.fetchedMeeting.notYetRespondedList.remove(at: rowToDelete)
                        break
                    case 3:
                        self.fetchedMeeting.declinedList.remove(at: rowToDelete)
                        break
                    default:
                        break
                    }
                    
                    
                    
                    self.sectionData =  [0: self.fetchedMeeting.hostList, 1: self.fetchedMeeting.acceptedList, 2: self.fetchedMeeting.notYetRespondedList, 3: self.fetchedMeeting.declinedList]
                    
                    self.participantTableView.reloadData()
                    if let index = self.fetchedMeeting.participantListForCollection.index(where: { (result) -> Bool in
                        result.participantId == participantToDelete.participantId
                    }) {
                        self.fetchedMeeting.participantListForCollection.remove(at: index)
                        
                        self.participantCollectionView.reloadData()
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.acceptMeetFailureString)
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                }
            }
            AppDelegate.addProcedure(operation: deleteParticipant)
        }
        
        func HideShowTableView() {
            tableViewHidden = !tableViewHidden
            UIView.animate(withDuration: 0.7, delay: 0.0, options: UIView.AnimationOptions.transitionFlipFromTop, animations: { () -> Void in
                if self.tableViewHidden {
                    self.participantTableViewContainer?.alpha = 0.0
                    self.mapViewContainer.isHidden = false
                } else {
                    self.participantTableViewContainer?.alpha = 1.0
                }
            }, completion: { (_: Bool) -> Void in
                
            })
        }
        
        
        func reduceNotificationCountAndMarkSeen(countToReduce: Int, meetIdArray : [String]){
            
            let reduceNotificationCount = ReduceUserMeetNotificationCount(countToReduce: countToReduce, meetIdArray : meetIdArray)
            reduceNotificationCount.addDidFinishBlockObserver { [unowned self] (operation, _) in
                DispatchQueue.main.async {
                    
                }
            }
            AppDelegate.addProcedure(operation: reduceNotificationCount)
        }
        
    }
    
    extension MeetMeDetailVC: ReloadProtocol {
        func reloadData(toReload: Bool) {
            if toReload {
                reloadLogbook = true
            }
            self.fetchMyMeeting()
        }
        
        func acceptMeetMeRequest( meetId: String, trackingStatus: String, trackingTime: String, trackingMode: String, hostId: String) {
            guard networkStatus.isNetworkReachable()
                else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.checkInternetConnectionMessage)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    return
            }
            
            let acceptMeetme = AcceptMeetInvite(meetmeId: meetId, trackingStatus: trackingStatus, trackingTime: trackingTime, trackingMode: trackingMode)
            acceptMeetme.addDidFinishBlockObserver { [unowned self] (operation, _) in
                let responseStatus = operation.responseStatus
                if responseStatus == "200"{
                    self.reloadLogbook = true
                    self.fetchMyMeeting()
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.acceptMeetSuccessString)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                        self.view.hideToast()
                    })
                } else if responseStatus == "405"{
                    DispatchQueue.main.async {
                        self.stopLoader()
                        self.view.makeToast(self.appService.acceptMeetLateString)
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User's accept MeetMe attempt Failed", value: 0)
                }else if responseStatus == "406"{
                    DispatchQueue.main.async {
                        self.stopLoader()
                        self.view.makeToast(self.appService.acceptMeetParticipentIsNotPartOfMeet)
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User's accept MeetMe attempt Failed", value: 0)
                } else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.acceptMeetFailureString)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.thirdTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Tracking Options Popup", action: "Tracking options Ok Button clicked", label: "User's accept MeetMe attempt Failed", value: 0)
                }
            }
            AppDelegate.addProcedure(operation: acceptMeetme)
        }
        
    }
    
    extension MeetMeDetailVC: AcceptMeet, RedirectedToMeetmeLogbookVC {
        
        func nextAction(method: String) {
            switch method {
            case "declineMeetme":
                declineMeetMeRequest(meetId: self.fetchedMeeting.meetmeId!)
                break
                
            case "deleteMeetme":
                deleteMeetme( deletedMeetme: self.fetchedMeeting)
                break
                
            case "addParticipants":
                fetchMyMeeting()
                break
            case "stopMeetme":
                stopMeetMe()
                break
            case "removeParticipant":
                let recievedRow = self.appSharedPrefernce.getAppSharedPreferences(key: "rowToDelete") as! Int
                let recievedSection = self.appSharedPrefernce.getAppSharedPreferences(key: "sectionToDelete") as! Int
                self.appSharedPrefernce.removeAppSharedPreferences(key: "sectionToDelete")
                self.appSharedPrefernce.removeAppSharedPreferences(key: "rowToDelete")
                removeParticipantWithourProcedure(sectionToDelete: recievedSection, rowToDelete: recievedRow)
            case "changeLockStatus":
                changeCapsuleLock()
                break
                
            case "openMeetmeCapsule":
                if(self.loggedInUserIsPartOfMeeting==true){
                    self.openCapsule()
                }else{
                    self.view.makeToast(self.appService.userNotPartOfTheMeeting)
                }
                break
                
            default:
                fetchMyMeeting()
                break
            }
        }
        
        func meetAccepted(discloseTrail: String, discloseTrailTime: String, transportMode: String) {
            let meetMeToAccept = fetchedMeeting.meetmeId
            let hostId = fetchedMeeting.hostId
            acceptMeetMeRequest(meetId: meetMeToAccept!, trackingStatus: discloseTrail, trackingTime: discloseTrailTime, trackingMode: transportMode, hostId: hostId ?? "")
        }
        
        func declineMeetMeRequest( meetId: String) {
            guard networkStatus.isNetworkReachable()
                else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.checkInternetConnectionMessage)
                    }
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    return
            }
            let declineMeetme = DeclineMeetInvite(meetmeId: meetId)
            declineMeetme.addDidFinishBlockObserver { [unowned self] (operation, _) in
                let responseStatus = operation.responseStatus
                if responseStatus == "200"{
                    self.reloadLogbook = true
                    self.fetchMyMeeting()
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.declinedMeetSuccessString)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.secondTimeInterval, execute: {
                        self.view.hideToast()
                    })
                } else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.declinedMeetFailureString)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                }
            }
            AppDelegate.addProcedure(operation: declineMeetme)
        }
        
        func deleteMeetme( deletedMeetme: MeetMeLogbookVariables) {
            guard networkStatus.isNetworkReachable()
                else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.checkInternetConnectionMessage)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    return
            }
            
            var meetmeToDelete = deletedMeetme
            meetmeToDelete.isDeleted = true
            let deleteMeetme = UpdateMeetme(updatedMeetme: meetmeToDelete)
            deleteMeetme.addDidFinishBlockObserver { [unowned self] (operation, _) in
                
                let responseStatus = operation.responseStatus
                if responseStatus == "200"{
                    NotificationCenter.default.post(name: REFRESH_MEET_NOTIFICATION, object: nil)
                    DispatchQueue.main.async {
                        self.view.removeFromSuperview()
                        self.dismiss(animated: true, completion: nil)
                        //                     let storyBoard: UIStoryboard = UIStoryboard(name: "Meetme", bundle: nil)
                        //                    let nextVC = storyBoard.instantiateViewController(withIdentifier: "MeetMeLogBook") as! MeetMeLogBook
                        //                    self.present(nextVC, animated: true, completion: nil)
                    }
                    
                }
            }
            AppDelegate.addProcedure(operation: deleteMeetme)
        }
        
        func fetchMeetmeCapsule() {
            guard networkStatus.isNetworkReachable()
                else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.checkInternetConnectionMessage)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    return
            }
            
            let meetmeCapsuleCall = GetMeetMeCapsule(meetMeId: self.selectedMeetmeId)
            meetmeCapsuleCall.addDidFinishBlockObserver { [unowned self] (operation, _) in
                
                let responseStatus = operation.responseStatus
                if responseStatus == "200"{
                    self.capsuleObj = operation.meetmeCapsule
                }
            }
            AppDelegate.addProcedure(operation: meetmeCapsuleCall)
        }
        
        func openVerifyMasterKeyVC(forCapsuleStatusChange: Bool!) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController") as! MasterKeyPopUpViewController
            nextVC.reloadProtocol = self as RedirectedToMeetmeLogbookVC
            if forCapsuleStatusChange {
                nextVC.fromMeetMeForCapsuleLock = true
            } else {
                nextVC.fromMeetMeForCapsuleOpening = true
            }
            self.present(nextVC, animated: true, completion: nil)
        }
        
        func changeCapsuleLock() {
            isCapsuleLocked = !isCapsuleLocked
            
            let parameters =
                [
                    "isCapsuleLocked": self.isCapsuleLocked
            ]
            
            let awsURL =  AppConfig.changeCapsuleLockUrl(selectedMeetmeId: selectedMeetmeId!)
            
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPut
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                return
            }
            
            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, response: URLResponse?, _: Error?) -> Void in
                if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    let data = json
                    
                    let response = data["statusCode"] as? String ?? ""
                    if response == "200"{
                        if self.isCapsuleLocked {
                            DispatchQueue.main.async {
                                self.capsuleLockImageView.image = UIImage(named: "CapsuleLocked")
                                DispatchQueue.main.async {
                                    self.view.makeToast(self.appService.capsuleLocked)
                                }
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                                    self.view.hideToast()
                                })
                            }
                            
                        } else {
                            DispatchQueue.main.async {
                                self.capsuleLockImageView.image = UIImage(named: "CapsuleUnlocked")
                                DispatchQueue.main.async {
                                    self.view.makeToast(self.appService.capsuleUnlocked)
                                }
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                                    self.view.hideToast()
                                })
                            }
                            
                        }
                    } else {
                        
                    }
                    
                } else {
                    
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
        }
        
        func fetchMeetLocations(meetmeId: String) {
            let getMeetLocation = GetLocationOfMeetme(meetIdForLocation: meetmeId)
            
            getMeetLocation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                let response = operation.responseStatus
                if response == "200"{
                    var sourceLatitude: Double  = 0.0
                    var sourceLongitude: Double = 0.0
                    var destinationLatitude: Double  = 0.0
                    var destinationLongitude: Double = 0.0
                    
                    var meetParticipantLocations = operation.meetParticipantLocations
                    
                    DispatchQueue.main.async {
                        let allAnnotations = self.meetMeDetailMapView.annotations
                        self.meetMeDetailMapView.removeAnnotations(allAnnotations)
                        
                        let allOverlays = self.meetMeDetailMapView.overlays
                        self.meetMeDetailMapView.removeOverlays(allOverlays)
                    }
                    
                    var meetAnnotations: [CustomAnnotation] = []
                    let destinationAnnotation = CustomAnnotation()
                    
                    destinationAnnotation.annotationType = "Destination"
                    let lat = (self.fetchedMeeting.latitude! as NSString).doubleValue
                    destinationLatitude = lat
                    
                    let long = (self.fetchedMeeting.longitude! as NSString).doubleValue
                    destinationLongitude = long
                    
                    destinationAnnotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                    
                    destinationAnnotation.title = "Destination"
                    meetAnnotations.append(destinationAnnotation)
                    for l in 0..<meetParticipantLocations.count {
                        
                        let userLocations = meetParticipantLocations[l].geoLocations
                        var userAnnotations: [CustomAnnotation] = []
                        
                        for k in 0..<userLocations.count {
                            
                            
                            let geolocation = userLocations[k]
                            let annotation = CustomAnnotation()
                            annotation.annotationType = "Participant"
                            let lat = geolocation.participantLatitude
                            let long = geolocation.participantLongitude
                            annotation.coordinate = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                            
                            if k == 0 {
                                if(meetParticipantLocations[l].fisheyeId==self.hostId){
                                    self.hostLat = geolocation.participantLatitude
                                    self.hostLong = geolocation.participantLongitude
                                }
                                
                                
                                annotation.annotationType = "Participant"
                                annotation.participantName = meetParticipantLocations[l].participantName
                                annotation.title = meetParticipantLocations[l].participantName
                            }
                            
                            if k == userLocations.count - 1 {
                                annotation.annotationType = "ParticipantLoctionStart"
                                annotation.participantName = meetParticipantLocations[l].participantName
                                annotation.title = meetParticipantLocations[l].participantName
                            }
                            userAnnotations.append(annotation)
                            meetAnnotations.append(annotation)
                            
                            if k == 0 {
                                DispatchQueue.global().async {
                                    
                                    sourceLatitude = userLocations[k].participantLatitude
                                    sourceLongitude = userLocations[k].participantLongitude
                                    
                                    let sourceCoordinates = CLLocationCoordinate2D(latitude: sourceLatitude, longitude: sourceLongitude)
                                    let destCoordinates = CLLocationCoordinate2D(latitude: destinationLatitude, longitude: destinationLongitude)
                                    
                                    let sourcePlacemark = MKPlacemark(coordinate: sourceCoordinates)
                                    let destPlacemark = MKPlacemark(coordinate: destCoordinates)
                                    
                                    let sorceItem = MKMapItem(placemark: sourcePlacemark)
                                    let destItem = MKMapItem(placemark: destPlacemark)
                                    
                                    let directionRequest = MKDirections.Request()
                                    directionRequest.source = sorceItem
                                    directionRequest.destination = destItem
                                    directionRequest.transportType = .automobile
                                    
                                    // if self.fetchedMeeting.participantList.
                                    if meetParticipantLocations[l].trackingMode == "Walking"{
                                        directionRequest.transportType = .walking
                                    } else if meetParticipantLocations[l].trackingMode == "Driving"{
                                        directionRequest.transportType = .automobile
                                    }
                                    
                                    let directions = MKDirections(request: directionRequest)
                                    directions.calculate(completionHandler: {
                                        response, error in
                                        
                                        guard let response = response else {
                                            if let error = error {
                                                
                                            }
                                            return
                                        }
                                        let route = response.routes[0]
                                        DispatchQueue.main.async {
                                            route.polyline.title = "isForDirections"
                                            self.meetMeDetailMapView.addOverlay(route.polyline, level: .aboveRoads)
                                            
                                            let rekt =  route.polyline.boundingMapRect
                                            self.meetMeDetailMapView.setRegion(MKCoordinateRegion(rekt), animated: true)
                                        }
                                        
                                    })
                                }
                            }
                            
                        }
                        DispatchQueue.main.async {
                            self.meetMeDetailMapView.addAnnotations(userAnnotations)
                        }
                        var locations = userAnnotations.map { $0.coordinate }
                        
                        let polyline = MKPolyline(coordinates: &locations, count: locations.count)
                        //                    polyline.setValue("false", forKey: "isForDirections")
                        polyline.title = "isNotForDirections"
                        //                    polyline.title = meetParticipantLocations[l].participantName
                        DispatchQueue.main.async {
                            self.meetMeDetailMapView.addOverlay(polyline)
                        }
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                        self.meetMeDetailMapView.showAnnotations(meetAnnotations, animated: true)
                    }
                }
                
            }
            AppDelegate.addProcedure(operation: getMeetLocation)
        }
        
        func openContactsVc() {
            let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
            self.meetContacts = storyboard.instantiateViewController(withIdentifier: "SearchContactsVC") as! SearchContactsVC
            self.meetContacts.meetmeId = self.selectedMeetmeId
            //        for participentObject in self.fetchedMeeting.participantListForCollection{
            //            var contactObj = ContactObj()
            //            contactObj.contactFEId = participentObject.fisheyeId
            //            contactObj.contactId = Int(participentObject.contactId!)
            //            contactObj.name = participentObject.participantName
            //            self.meetContacts.currentMeet.append(contactObj)
            //        }
            //        self.meetContacts.addParticipentAfterMeetCreated = true
            //        self.meetContacts.selectedContacts = self.fetchedMeeting.participantListForCollection.count
            self.meetContacts.isFromDetailVc = true
            self.meetContacts.redirectProtocol = self as RedirectedToMeetmeLogbookVC
            self.meetContacts.view.frame = self.meetMeDetailCompleteView.bounds
            self.addChild(self.meetContacts)
            self.meetContacts.didMove(toParent: self)
            self.meetContacts.view.tag = 0
            self.meetContacts.view.frame.origin.y = -self.view.frame.size.height
            self.meetContacts.view.frame.origin.y = self.meetMeDetailCompleteView.bounds.origin.y
            self.meetContacts.view.frame.origin.x = -self.view.frame.size.width
            self.meetContacts.view.frame.origin.x = self.meetMeDetailCompleteView.bounds.origin.x
            self.meetMeDetailCompleteView.addSubview(self.meetContacts.view)
        }
        
        func openCapsule() {
            
            let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
            self.meetmeCapsuleVC = storyboard.instantiateViewController(withIdentifier: "MeetMeCapsuleVC") as! MeetMeCapsuleVC
            self.meetmeCapsuleVC.meetmeTitle = fetchedMeeting.meetmeTitle
            self.meetmeCapsuleVC.selectedMeetmeId  = self.selectedMeetmeId
            meetmeCapsuleVC.isFromDetails = true
            self.meetmeCapsuleVC.makeUnreadCapsuleCountZeroOnDetails = self as MakeUnreadCapsuleCountZeroOnMeetDetails
            for meetParticipant in fetchedMeeting.participantList {
                if meetParticipant.isHost {
                    self.meetmeCapsuleVC.hostImage.sd_setImage(with: URL(string: meetParticipant.participantPicture ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"), options: [.continueInBackground, .progressiveLoad])
                }
            }
            self.meetmeCapsuleVC.view.frame = self.meetMeDetailCompleteView.bounds
            self.addChild(self.meetmeCapsuleVC)
            
            //self.meetmeCapsuleVC.didMove(toParentViewController: self)
            self.meetmeCapsuleVC.view.frame.origin.y = -self.view.frame.size.height
            self.meetmeCapsuleVC.view.frame.origin.y = self.meetMeDetailCompleteView.bounds.origin.y
            self.meetmeCapsuleVC.view.frame.origin.x = -self.view.frame.size.width
            self.meetmeCapsuleVC.view.frame.origin.x = self.meetMeDetailCompleteView.bounds.origin.x
            self.meetMeDetailCompleteView.addSubview(self.meetmeCapsuleVC.view)
        }
        
        /************************** Timer functions start here **************************/
        func scheduleTimer() {
            stopTimer()
            timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(self.updateLocation), userInfo: nil, repeats: true)
        }
        
        @objc func updateLocation() {
            let todaysDate = validationController.getTodaysDateInTimeStamp()
            
            if todaysDate>fetchedMeeting.trackingTime!  && fetchedMeeting.endDateTime!>todaysDate {
                fetchMeetLocations(meetmeId: selectedMeetmeId)
            } else if todaysDate>fetchedMeeting.endDateTime! {
                //            self.meetMeDetailMapView.add(route.polyline, level: .aboveRoads)
                //            self.meetMeDetailMapView.remove(route.polyline)
                //            self.meetMeDetailMapView.remove(meetMeDetailMapView.overlays as!)
            } else {
                
            }
            if todaysDate>fetchedMeeting.startDateTime!  && fetchedMeeting.endDateTime!>todaysDate && self.isHost {
                ("current date time is during meeting and logged in user is host")
                changeDetailsForStopMeetMe()
            }
            
        }
        
        func stopTimer() {
            if timer != nil {
                timer?.invalidate()
                timer = nil
            }
        }
        
        /************************** Timer functions end here **************************/
        
        /************************** For stop meetme starts here *********************************/
        
        func changeDetailsForStopMeetMe() {
            DispatchQueue.main.async {
                self.stopButtonImageView.isHidden = false
                self.stopMeetMeButton.isHidden = false
                self.acceptMeetmeButton.isHidden = true
                self.acceptMeetmeImageView.isHidden = true
                self.declineMeetmeImageView.isHidden = true
                self.declineMeetmeButton.isHidden = true
                self.editButtonImageView.isHidden = true
                self.deleteButtonImageView.isHidden = true
            }
            
        }
        
        /************************** For stop meetme ends here *********************************/
        
        /************************** To fit all annotations functions start here **************************/
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            if !(annotation is CustomAnnotation) {
                return nil
            }
            
            let annotationIdentifier = "AnnotationIdentifier"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                annotationView!.canShowCallout = true
            } else {
                annotationView!.annotation = annotation
            }
            
            if let customAnno = annotation as? CustomAnnotation {
                if customAnno.annotationType == "Destination"{
                    let pinImage = UIImage(named: "shakingHandsCopy")
                    annotationView!.image = pinImage
                    annotationView?.frame.size = CGSize(width: 33.0, height: 33.0)
                } else if customAnno.annotationType == "Participant" {
                    if customAnno.participantName != nil {
                        let pinImage = UIImage(named: "participantLocation")
                        annotationView!.image = pinImage
                        annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                    } else {
                        let pinImage = UIImage(named: "Image")
                        annotationView!.image = pinImage
                        annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                    }
                    
                } else if customAnno.annotationType == "ParticipantLoctionStart" {
                    if customAnno.participantName != nil {
                        let pinImage = UIImage(named: "startPoint")
                        annotationView!.image = pinImage
                        annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                    } else {
                        let pinImage = UIImage(named: "Image")
                        annotationView!.image = pinImage
                        annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                    }
                    
                } else {
                    let pinImage = UIImage(named: "Image")
                    annotationView!.image = pinImage
                    annotationView?.frame.size = CGSize(width: 15.0, height: 21.0)
                }
            }
            return annotationView
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            if overlay is MKCircle {
                let renderer = MKCircleRenderer(overlay: overlay)
                renderer.fillColor = UIColor.black.withAlphaComponent(0.5)
                renderer.strokeColor = UIColor.blue
                renderer.lineWidth = 1
                return renderer
                
            }
            if overlay is MKPolyline {
                let renderer = MKPolylineRenderer(overlay: overlay)
                if let temp = overlay.title {
                    if temp == "isForDirections"{
                        renderer.strokeColor = UIColor.blue
                    } else {
                        renderer.strokeColor = UIColor.acceptGreen()
                    }
                }
                renderer.lineDashPattern = [5, 5]
                renderer.lineWidth = 2
                
                return renderer
            }
            
            //        if overlay is CustomPolyline {
            //            let renderer = MKPolylineRenderer(overlay: overlay)
            //            renderer.strokeColor = UIColor.blue
            //            renderer.lineWidth = 2
            //
            //            return renderer
            //        }
            
            //        if overlay is MKPolyline &&
            //        {
            //            let renderer =  MKPolygonRenderer(overlay: overlay)
            //            renderer.strokeColor = UIColor.blue
            //            renderer.lineWidth = 5.0
            //
            //            return renderer
            //        }
            
            return MKOverlayRenderer()
        }
        
        func zoomAnnotations(on mapView: MKMapView, toFrame annotationsFrame: CGRect, animated: Bool) {
            if self.meetMeDetailMapView.annotations.count < 2 {
                return
            }
            // Step 1: make an MKMapRect that contains all the annotations
            let annotations = self.meetMeDetailMapView.annotations
            let firstAnnotation: MKAnnotation = annotations[0]
            var minPoint: MKMapPoint = MKMapPoint(firstAnnotation.coordinate)
            var maxPoint: MKMapPoint = minPoint
            for annotation: MKAnnotation in annotations {
                let point: MKMapPoint = MKMapPoint(annotation.coordinate)
                if point.x < minPoint.x {
                    minPoint.x = point.x
                }
                if point.y < minPoint.y {
                    minPoint.y = point.y
                }
                if point.x > maxPoint.x {
                    maxPoint.x = point.x
                }
                if point.y > maxPoint.y {
                    maxPoint.y = point.y
                }
            }
            let mapRect: MKMapRect = MKMapRect(x: minPoint.x, y: minPoint.y, width: maxPoint.x - minPoint.x, height: maxPoint.y - minPoint.y)
            // Step 2: Calculate the edge padding
            let edgePadding: UIEdgeInsets = UIEdgeInsets(top: annotationsFrame.minY, left: annotationsFrame.minX, bottom: mapView.bounds.maxY - annotationsFrame.maxY, right: mapView.bounds.maxX - annotationsFrame.maxX)
            // Step 3: Set the map rect
            mapView.setVisibleMapRect(mapRect, edgePadding: edgePadding, animated: animated)
        }
        /************************** To fit all annotations functions end here **************************/
        
        @objc func setLocalizationText() {
            DispatchQueue.main.async {
                self.participantsLabel.text = self.appService.participantsLabel
                self.earliestTracking.text = self.appService.earliestTracking + " :"
                self.unlockCapsuleLabel.text = self.appService.unlockCapsuleLabel
                self.sections = [self.appService.orgnizer, self.appService.accepted, self.appService.notYetResponded, self.appService.declined]
            }
        }
        
    }
    extension MeetMeDetailVC {
        func removeParticipantWithourProcedure(sectionToDelete: Int, rowToDelete: Int) {
            var participantToDelete = ParticipantVariables()
            guard networkStatus.isNetworkReachable()
                else {
                    self.view.makeToast(appService.checkInternetConnectionMessage)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    return
            }
            switch(sectionToDelete) {
            case 1:
                participantToDelete = fetchedMeeting.acceptedList[rowToDelete]
                break
            case 2:
                participantToDelete = fetchedMeeting.notYetRespondedList[rowToDelete]
                break
            case 3:
                participantToDelete = fetchedMeeting.declinedList[rowToDelete]
                break
            default:
                break
            }
            
            let parameters =
                [
                    "participant": [
                        "participantId": participantToDelete.participantId!
                    ]
                    ] as [String: Any]
            let awsURL = AppConfig.removeParticipant(selectedMeetmeId: self.selectedMeetmeId!)
            
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodDelete
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                return
            }
            
            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    let data = json
                    
                    let responseStatus = data["statusCode"] as? String ?? ""
                    if responseStatus == "200"{
                        DispatchQueue.main.async {
                            self.view.makeToast(self.appService.removeParticipantSuccessString)
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                            self.view.hideToast()
                        })
                        switch(sectionToDelete) {
                        case 1:
                            self.fetchedMeeting.acceptedList.remove(at: rowToDelete)
                            
                            break
                        case 2:
                            self.fetchedMeeting.notYetRespondedList.remove(at: rowToDelete)
                            
                            break
                        case 3:
                            self.fetchedMeeting.declinedList.remove(at: rowToDelete)
                            
                            break
                        default:
                            break
                        }
                        if self.fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 &&  self.fetchedMeeting.declinedList.count != 0 {
                            
                            self.hideTableSection0 = false
                            self.hideTableSection1 = false
                            self.hideTableSection2 = false
                        } else if self.fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.notYetRespondedList.count != 0 {
                            
                            self.hideTableSection0 = false
                            self.hideTableSection1 = false
                            self.hideTableSection2 = true
                        } else if self.fetchedMeeting.notYetRespondedList.count != 0 &&  self.fetchedMeeting.declinedList.count != 0 {
                            
                            self.hideTableSection0 = true
                            self.hideTableSection1 = false
                            self.hideTableSection2 = false
                        } else if self.fetchedMeeting.acceptedList.count != 0 && self.fetchedMeeting.declinedList.count != 0 {
                            
                            self.hideTableSection0 = false
                            self.hideTableSection1 = true
                            self.hideTableSection2 = false
                        } else if  self.fetchedMeeting.acceptedList.count != 0 {
                            
                            self.hideTableSection0 = false
                            self.hideTableSection1 = true
                            self.hideTableSection2 = true
                        } else if  self.fetchedMeeting.notYetRespondedList.count != 0 {
                            
                            self.hideTableSection0 = true
                            self.hideTableSection1 = false
                            self.hideTableSection2 = true
                        } else if self.fetchedMeeting.declinedList.count != 0 {
                            
                            self.hideTableSection0 = true
                            self.hideTableSection1 = true
                            self.hideTableSection2 = false
                        }else{
                            self.hideTableSection0 = true
                            self.hideTableSection1 = true
                            self.hideTableSection2 = true
                        }
                        self.sectionData =   [0: self.fetchedMeeting.hostList, 1: self.fetchedMeeting.acceptedList, 2: self.fetchedMeeting.notYetRespondedList, 3: self.fetchedMeeting.declinedList]
                        DispatchQueue.main.async {
                            self.participantTableView.reloadData()
                            if let index = self.fetchedMeeting.participantListForCollection.index(where: { (result) -> Bool in
                                result.participantId == participantToDelete.participantId
                            }) {
                                self.fetchedMeeting.participantListForCollection.remove(at: index)
                                self.participantCollectionView.reloadData()
                            }
                        }
                        
                        self.reloadLogbook = true
                    } else {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.appService.removeParticipantFailureString)
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                            self.view.hideToast()
                        })
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        self.view.makeToast(self.appService.removeParticipantFailureString)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                        self.view.hideToast()
                    })
                    
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
            
        }
        
        func getMessageCount() {
            
            let awsURL = AppConfig.capsuleCountUrl(selectedMeetmeId: self.selectedMeetmeId!)
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodGet
            request.httpBody = "".data(using: String.Encoding.utf8)
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                return
            }
            
            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    let data = json
                    let responseStatus = data["statusCode"] as? String ?? ""
                    if responseStatus == "200"{
                        if let count = data["messageCount"] as? Int {
                            DispatchQueue.main.async {
                                self.capsuleNotificationCounLabelt.text = String(count)
                            }
                        }
                    }
                    
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
        }
        
        func stopMeetMe() {
            self.startLoader()
            self.stopMeetMeButton.isUserInteractionEnabled = false
            
            let parameters =
                [
                    "istrackingEnabled": self.isTrackingEnabled,
                    "trackingStatus": self.trackingStatusText,
                    "trackingTime": self.trackingTimeText,
                    "isStopMeet": true,
                    "meetme": [
                        "meetmeId": self.fetchedMeeting.meetmeId!,
                        "title": self.fetchedMeeting.meetmeTitle!,
                        "locationName": self.fetchedMeeting.meetmeLocation!,
                        "latitude": self.fetchedMeeting.latitude!,
                        "longitude": self.fetchedMeeting.longitude!,
                        "startDateTime": self.fetchedMeeting.startDateTime!,
                        "endDateTime": validationController.getTodaysDateInTimeStamp().roundToDecimal(0),
                        "createdDate" : self.fetchedMeeting.createdDate!
                    ]
                    ] as [String: Any]
            
            let awsURL = AppConfig.updateMeetUrl(meetMeId: self.selectedMeetmeId!)
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            request.httpBody = httpBody
            
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                return
            }
            
            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, response: URLResponse?, _: Error?) -> Void in
                if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    let data = json
                    
                    let response = data["statusCode"] as? String ?? ""
                    
                    if response == "200"{
                        DispatchQueue.main.async {
                            self.view.makeToast(self.appService.stopMeetSuccessString)
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                            self.view.hideToast()
                        })
                        self.stopLoader()
                        if let meetingsObjt = data["meetme"] as? [String: Any] {
                            self.fetchedMeeting.meetmeLocation = meetingsObjt["locationName"] as? String ??  ""
                            self.fetchedMeeting.meetmeTitle = meetingsObjt["title"] as? String ??  ""
                            self.fetchedMeeting.latitude = meetingsObjt["latitude"] as? String ??  ""
                            self.fetchedMeeting.longitude = meetingsObjt["longitude"] as? String ??  ""
                            self.fetchedMeeting.startDateTime = meetingsObjt["startDateTime"] as? Double
                            self.fetchedMeeting.endDateTime = meetingsObjt["endDateTime"] as? Double
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                                self.setDetail()
                            })
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "MeetMe Detail Page", action: "Stop MeetMe Button clicked", label: "User ends MeetMe Succesfully", value: 0)
                    } else {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "MeetMe Detail Page", action: "Stop MeetMe Button clicked", label: "User's attempt to end Meet fails", value: 0)
                        self.stopLoader()
                    }
                    
                } else {
                    self.stopLoader()
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "MeetMe Detail Page", action: "Stop MeetMe Button clicked", label: "User's attempt to end Meet fails", value: 0)
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
        }
        
    }
    
    
    extension MeetMeDetailVC: MakeUnreadCapsuleCountZeroOnMeetDetails {
        func makeCountZeroOnDetails(isToMakeCountZero: Bool) {
            self.capsuleNotificationCounLabelt.text = String(0)
        }
}
