//
//  MeetMeLogBookTableViewCell.swift
//  fisheye
//
//  Created by Sankey Solution on 12/7/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit

class MeetMeLogBookTableViewCell: UITableViewCell {

    @IBOutlet weak var mainNotificationView: UIView!
    @IBOutlet weak var hostImageView: UIImageView!
    @IBOutlet weak var meetTitleLabel: UILabel!
    @IBOutlet weak var meetAddressLabel: UILabel!
    @IBOutlet weak var acceptImage: UIImageView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var declineImage: UIImageView!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var meetTimeLabel: UILabel!
    @IBOutlet weak var meetDateLabel: UILabel!
    @IBOutlet weak var notificationDetailView: UIView!
    @IBOutlet weak var attendant1ImageView: UIImageView!
    @IBOutlet weak var attendant2ImageView: UIImageView!
    @IBOutlet weak var attendant4ImageView: UIImageView!
    @IBOutlet weak var declined1ImageView: UIImageView!
    @IBOutlet weak var declined2ImageView: UIImageView!
    @IBOutlet weak var attendant3ImageView: UIImageView!
    @IBOutlet weak var attendant1Label: UILabel!
    @IBOutlet weak var attendant2Label: UILabel!
    @IBOutlet weak var attendant3Label: UILabel!
    @IBOutlet weak var attendant4Label: UILabel!
    @IBOutlet weak var acceptedListImageView: UIImageView!
    @IBOutlet weak var declinedListImageView: UIImageView!
    @IBOutlet weak var declined1Label: UILabel!
    @IBOutlet weak var declined2Label: UILabel!
    @IBOutlet weak var attendantElipsizeLabel: UILabel!
    @IBOutlet weak var meetMeLogBookMapView: MKMapView!

    @IBOutlet weak var notificationCapsuleView: UIView!
    @IBOutlet weak var capsuleLockImageView: UIImageView!
    @IBOutlet weak var capsuleTitleLabel: UILabel!
    @IBOutlet weak var capsuleStatusLabel: UILabel!

    //    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
