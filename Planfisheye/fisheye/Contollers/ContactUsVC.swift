//
//  ContactUsVC.swift
//  fisheye
//
//  Created by sankeyMacOs on 18/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsVC: UIViewController, UIWebViewDelegate {

      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var webView: UIWebView!
      @IBOutlet weak var contactUsHeader: UILabel!

      var appService = AppService()

    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.appService = AppService.shared
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        let URLget = URL(string: appService.contactUsLink)
        let urlRequest1 = URLRequest(url: URLget!)
        self.webView.loadRequest(urlRequest1)
        
        self.webView.delegate = self
        self.addObservers()
        self.setLocalizationText()
    }

      func addObservers() {
                  NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }

    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.contactUsHeader.text = self.appService.contactUsHeader
            }
      }

      func webViewDidFinishLoad(_ webView: UIWebView) {
            if webView.isLoading {
                  return
            }
        ScreenLoader.shared.stopLoader()
      }

      @IBAction func backButtonTapped(_ sender: Any) {
            ScreenLoader.shared.stopLoader()
            self.dismiss(animated: true, completion: nil)
      }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
