//
//  GeoLocation.swift
//  fisheye
//
//  Created by Sankey Solutions on 24/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

class GeoLocation {
    init() {

    }

    /** Address of the location */
    var participantLocationName: String!
    /** Latitiude of the location. */
    var participantLatitude: Double!
    /** Longitude of the location. */
    var participantLongitude: Double!
   /** Time stamp at which the location was stored*/
    var locationDateTime: Double!
}
class UserLocations {
    init() {

    }
    /** Name of the Participant.  */
    var participantName: String!

    var fisheyeId: String!

    var geoLocations = [ GeoLocation() ]

      var trackingMode: String!

}
