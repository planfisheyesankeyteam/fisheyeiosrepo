//
//  CustomPointAnnotation.swift
//  fisheye
//
//  Created by Sankey Solutions on 08/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    var pinCustomImageName: String!
}
