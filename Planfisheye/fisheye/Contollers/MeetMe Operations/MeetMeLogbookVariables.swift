//  MeetMeLogbookVariables.swift
//  fisheye
//
//  Created by Sankey Solutions on 22/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import ProcedureKit
import Foundation

struct MeetMeLogbookVariables {
    init() {

    }

    /** Meet Id generate from server side when used try to add new address. */
    var meetmeId: String?
    
    var hostId: String?
    /** Full mailing address, formatted for display or use on a mailing label. */
    var meetmeTitle: String?
    /** Address of the location */
    var meetmeLocation: String?
    /** Latitiude of the location. */
    var latitude: String?
    /** Longitude of the location. */
    var longitude: String?
    /** Meet start date time*/
    var startDateTime: Double?
    /** Meet end date time*/
    var endDateTime: Double?
    /* Deleted Meet me flag*/
    var isDeleted: Bool?
    /* Tracking start time*/
    var trackingTime: Double?
    //var participants
    var createdDate: Double?
    //To verify particular Object type is a meeting or event
    var isEventFlag: Bool?
    
    var acceptedList =  [ParticipantVariables()]

    var declinedList =  [ParticipantVariables()]

    var notYetRespondedList = [ParticipantVariables()]

    var participantList =  [ParticipantVariables()]

    var participantListForCollection =  [ParticipantVariables()]
    var participantListForEvent =  [ParticipantVariables()]

    var hostList = [ParticipantVariables()]
}

class AcceptMeetInvite: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var responseStatus = ""
    var meetmeId = ""
    var trackingStatus = ""
    var trackingTime = ""
    var trackingMode = ""
    var trackingEnabled = false
    var userId = ""

    init(meetmeId: String, trackingStatus: String, trackingTime: String, trackingMode: String) {
        super.init()
        self.meetmeId = meetmeId
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.userId = appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
        self.trackingStatus = trackingStatus
        self.trackingTime = trackingTime
        self.trackingMode = trackingMode
        if trackingStatus != "Never"{
            trackingEnabled = true
        } else {
            trackingEnabled = false
        }
    }

    override func execute() {
        let parameters =
            [
                "istrackingEnabled": trackingEnabled,
                "trackingStatus": trackingStatus,
                "trackingTime": trackingTime,
                "trackingMode": trackingMode
                ]as [String: Any]

        let awsURL =  AppConfig.acceptMeetInvite(meetmeId: meetmeId, userId: userId)
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPut
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                self.responseStatus = data["statusCode"] as? String ?? ""
                self.finish()
            } else {
                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }
}

class UpdateMeetme: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var updatedMeetmeObj = MeetMeLogbookVariables()
    var responseStatus = ""

    init(updatedMeetme: MeetMeLogbookVariables) {
        super.init()
        self.updatedMeetmeObj = updatedMeetme

        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {

        let parameters = [
                "meetme": [
                    "meetmeId": updatedMeetmeObj.meetmeId!,
                    "title": updatedMeetmeObj.meetmeTitle!,
                    "titleSearchKey": updatedMeetmeObj.meetmeTitle!.lowercased(),
                    "locationName": updatedMeetmeObj.meetmeLocation!,
                    "locationNameSearchKey": updatedMeetmeObj.meetmeLocation!.lowercased(),
                    "latitude": updatedMeetmeObj.latitude!,
                    "longitude": updatedMeetmeObj.longitude!,
                    "startDateTime": updatedMeetmeObj.startDateTime!,
                    "endDateTime": updatedMeetmeObj.endDateTime!,
                    "isDeleted": updatedMeetmeObj.isDeleted!
                ]
                ]as [String: Any]

        let awsURL = AppConfig.updateMeetUrl(meetMeId: updatedMeetmeObj.meetmeId!)

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPut
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                self.responseStatus = data["statusCode"] as? String ?? ""
                self.finish()

            } else {

                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }
}

class DeclineMeetInvite: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var meetmeId = ""
    var userId = ""
    var responseStatus = ""
    init(meetmeId: String) {
        super.init()
        self.meetmeId = meetmeId
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.userId = appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
    }

    override func execute() {

        let awsURL =  AppConfig.declineMeetInvite(meetmeId: meetmeId, userId: userId)
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPut

        request.httpBody =  "".data(using: String.Encoding.utf8)

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json

                self.responseStatus = data["statusCode"] as? String ?? ""
                self.finish()

            } else {

                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }
}

class SaveFCMToken: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var responseStatus = ""
    var fcmToken = ""

    init(fcmTokenRecieved: String) {
        super.init()
        self.fcmToken = fcmTokenRecieved
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {

        let parameters = [
                "fcmToken": self.fcmToken
        ] as [String:Any]

        let awsURL = AppConfig.saveFCMTakenAPI

        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                self.responseStatus = data["statusCode"] as? String ?? ""
                self.finish()

            } else {
                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

class PostGeoLocation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var meetmeId = ""
    var responseStatus = ""
    var participantLocation = GeoLocation()
    let validationController = ValidationController()
    init(participantLocationReceived: GeoLocation) {
        super.init()
        self.participantLocation = participantLocationReceived
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {

        let parameters =
            [
                "geoLocation": [
                    "latitude": self.participantLocation.participantLatitude,
                    "longitude": self.participantLocation.participantLongitude,
                    "locationName": self.participantLocation.participantLocationName,
                    "locationDateTime": validationController.getTodaysDateTime()
                ]
        ] as [String: Any]
        let awsURL = AppConfig.postGeoLocationUrl

        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json

                self.responseStatus = data["statusCode"] as? String ?? ""
                self.finish()

            } else {

                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

class GetLocationOfMeetme: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var responseStatus = ""
    var idtoken = ""
    var meetmeId = ""
    var meetParticipantLocations: [UserLocations] = []

    init(meetIdForLocation: String) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.meetmeId = meetIdForLocation
    }

    override func execute() {
        let awsURL = AppConfig.getGeoLocationsOfMeet(selectedMeetmeId: meetmeId)

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodGet

        request.httpBody =  "".data(using: String.Encoding.utf8)

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {

                self.responseStatus = json["statusCode"] as? String ?? ""
                if self.responseStatus == "200"{
                    if let userLocations = json["userLocations"] as? [[String: Any]] {
                        for j in 0..<userLocations.count {
                            let meetParticipantUserLocations = UserLocations()

                            if let geoLocations = userLocations[j]["geoLocations"] as? [[String: Any]] {

                            meetParticipantUserLocations.participantName = userLocations[j]["name"] as? String ??  ""
                                meetParticipantUserLocations.participantName = userLocations[j]["name"] as? String ??  ""
                             meetParticipantUserLocations.fisheyeId = userLocations[j]["fisheyeId"] as? String ?? ""
                                meetParticipantUserLocations.trackingMode = userLocations[j]["trackingMode"] as? String ??  ""
                            meetParticipantUserLocations.geoLocations = []

                            for i in 0..<geoLocations.count {
                                if geoLocations.count>0 {
                                    let meetParticipantLocationObj = GeoLocation()

                                    meetParticipantLocationObj.participantLatitude = ((geoLocations[i]["latitude"] as? String ??  "") as NSString).doubleValue

                                    meetParticipantLocationObj.participantLongitude = ((geoLocations[i]["longitude"] as? String ??  "") as NSString).doubleValue
                                    meetParticipantUserLocations.geoLocations.append(meetParticipantLocationObj)
                                }
                            }
                            self.meetParticipantLocations.append(meetParticipantUserLocations)
                        }
                        }
                    }
                }
                self.finish()

            } else {

                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }
}

class AddParticipants: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var meetmeId = ""
    var responseStatus = ""
    var participantFisheyeIds: [AnyObject] = []

    init( meetmeId: String, fisheyeids: [AnyObject]) {
        super.init()
        self.meetmeId = meetmeId
        self.participantFisheyeIds = fisheyeids
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {

        let parameters =
            [
                "participants": participantFisheyeIds
        ] as [String: Any]
        let awsURL = "https://sz3i35gurk.execute-api.eu-west-2.amazonaws.com/dev/meetme/\(meetmeId)/participants"

        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }


        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json

                self.responseStatus = data["statusCode"] as? String ?? ""
                self.finish()

            } else {

                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }
}

class DeleteParticipant: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var meetmeId = ""
    var responseStatus = ""
    var participantToDelete = ParticipantVariables()

    init( selectedParticipant: ParticipantVariables, meetmeId: String) {
        super.init()
        self.participantToDelete = selectedParticipant
        self.meetmeId = meetmeId
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {

        let parameters =
            [
                "participant": [
                    "participantId": participantToDelete.participantId!
                ]
        ] as [String: Any]
        let awsURL = AppConfig.removeParticipant(selectedMeetmeId: meetmeId)
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json

                self.responseStatus = data["statusCode"] as? String ?? ""
                self.finish()

            } else {

                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }
}

class GetMeetMeCapsule: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var responseStatus = ""
    var meetmeCapsule = MeetMeCapsule()
    var idtoken = ""
    var meetMeId = ""
    var fisheyeId = ""
    var unreadMessageCount = 0

    init(meetMeId: String) {
        super.init()
        self.meetMeId = meetMeId
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.fisheyeId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
    }

    override func execute() {

        let awsURL = AppConfig.getMeetCapsule(selectedMeetmeId: meetMeId)

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodGet
        request.httpBody =  "".data(using: String.Encoding.utf8)
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                self.responseStatus = json["statusCode"] as? String ?? ""
                if self.responseStatus == "200"{
                    if  let meetmeCapsule = json["meetMeCapsule"] as? [String: Any] {
                        self.meetmeCapsule.meetmeId = meetmeCapsule["meetmeId"] as? String ??  ""
                        self.meetmeCapsule.meetParticipantCount = meetmeCapsule["meetParticipantCount"] as? Int ??  0
                        if let hostParticipantData = meetmeCapsule["hostParticipant"]  as? [String: Any] {
                            self.meetmeCapsule.hostParticipant.fisheyeId = hostParticipantData["fisheyeId"] as? String ??  ""
                            self.meetmeCapsule.hostParticipant.participantPicture = hostParticipantData["participantPicture"] as? String ??  ""
                            self.meetmeCapsule.hostParticipant.participantName = hostParticipantData["participantName"] as? String ??  ""
                        }
                         self.meetmeCapsule.messages = []
                        if  let messages = meetmeCapsule["meetmeMessages"] as? [[String: Any]] {
                            for j in 0..<messages.count {
                                let meetmeMessageObj =  MeetMeMessage()
                                meetmeMessageObj.fisheyeId =  messages[j]["fisheyeId"] as? String ??  ""
                                meetmeMessageObj.messageId =  messages[j]["messageId"] as? String ??  ""
                                meetmeMessageObj.messageBody =  messages[j]["messageBody"] as? String ??  ""
                                meetmeMessageObj.isSent =  messages[j]["isSent"] as? Bool ??  false
                                meetmeMessageObj.name  =  messages[j]["name"] as? String ??  ""
                                meetmeMessageObj.picture =  messages[j]["picture"] as? String ??  ""
                                meetmeMessageObj.messageTime = messages[j]["messageTime"] as? Double
                                
                                if let readByArray=messages[j] ["isReadBy"] as? [String] {
                                    for readBy in readByArray {
                                       meetmeMessageObj.isReadBy.append(readBy)
                                    }
                                }
                                self.meetmeCapsule.messages.append(meetmeMessageObj)
                            }
                        }
                    }
                } else {
                     self.responseStatus = "500"
                }
                self.finish()
            } else {

                self.responseStatus = "500"
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
//    func reduceNotificationCountAndMarkSeen(countToReduce: Int, meetIdArray : [String]){
//
//        let reduceNotificationCount = ReduceUserMeetNotificationCount(countToReduce: countToReduce, meetIdArray : meetIdArray)
//        reduceNotificationCount.addDidFinishBlockObserver { [unowned self] (operation, _) in
//            DispatchQueue.main.async {
//                let meetUnreadCapsuleCountData: [String: Any] = ["unreadCapsuleCountToReduceBy": countToReduce, "meetmeIdArray": meetIdArray]
//
//                NotificationCenter.default.post(name: REFRESH_MEET_UNREAD_CAPSULE_COUNT, object: nil, userInfo: meetUnreadCapsuleCountData)
//            }
//        }
//        AppDelegate.addProcedure(operation: reduceNotificationCount)
//    }
    
}

class CreateMessage: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var responseStatus = ""
    var meetmeMessage =  ""
    var meetmeCapsule = MeetMeCapsule()
    var idtoken = ""
    var meetMeId = ""
    var meetTitle = ""
    var offlineMessage = ""

    init(meetMeId: String, meetmeMessage: String, meetTitle: String) {
        super.init()
        self.meetMeId = meetMeId
        self.meetmeMessage = meetmeMessage
        self.meetTitle = meetTitle
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {
        let parameters =
            [
                "meetMeMessage": [
                    "messageBody": meetmeMessage
                ],
                "meetme": [
                    "meetmeId": meetMeId,
                    "title": meetTitle
                ]
        ] as [String: Any]
        
        let awsURL = AppConfig.createMeetMessage(selectedMeetmeId: meetMeId)
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                self.responseStatus = data["statusCode"] as? String ?? ""
                if self.responseStatus == "200"{
                    if let meetmeCapsule = data["meetMeCapsule"] as? [String: Any] {
                        self.meetmeCapsule.meetmeId = meetmeCapsule["meetmeId"] as? String ?? ""
                        if let messages = meetmeCapsule["meetmeMessages"] as? [[String: Any]] {
                            self.meetmeCapsule.messages = []
                            for j in 0..<messages.count {
                                let meetmeMessageObj =  MeetMeMessage()
                                meetmeMessageObj.fisheyeId =  messages[j]["fisheyeId"] as? String ?? ""
                                meetmeMessageObj.messageId =  messages[j]["messageId"] as? String ?? ""
                                meetmeMessageObj.messageBody =  messages[j]["messageBody"] as? String ?? ""
                                meetmeMessageObj.isSent =  messages[j]["isSent"] as? Bool ??  false
                                meetmeMessageObj.name  =  messages[j]["name"] as? String ?? ""
                                meetmeMessageObj.picture =  messages[j]["picture"] as? String ?? ""
                                meetmeMessageObj.messageTime = messages[j]["messageTime"] as? Double
                                
                                if let readByArray=messages[j] ["isReadBy"] as? [String] {
                                    for readBy in readByArray {
                                        meetmeMessageObj.isReadBy.append(readBy)
                                    }
                                }
                                
                                self.meetmeCapsule.messages.append(meetmeMessageObj)
                            }
                        }
                    }
                }
                 self.finish()
            } else {
              self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
}

class GetMeetmeNotificationCount: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var responseStatus = ""
    var notificationCount = 0
    var idtoken = ""

    override init() {
        super.init()

        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
    }

    override func execute() {

        let awsURL = AppConfig.meetMeNotificationsUrl
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodGet
        request.httpBody = "".data(using: String.Encoding.utf8)

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json

                if let notificationList = data["meetMeNotifications"] as? [[String: Any]] {
                    self.notificationCount = notificationList.count
                } else {
                    self.notificationCount = 0
                }
                self.finish()

            } else {

                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()

    }

}



class ReduceUserMeetNotificationCount: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var responseStatus = ""
    var idtoken = ""
    var meetNotificationCountToReduce = 0
    var meetIdArray: [String] = []
    
    init(countToReduce: Int, meetIdArray : [String]) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.meetNotificationCountToReduce = countToReduce
        self.meetIdArray = meetIdArray
    }
    
    override func execute() {
        
        
        let parameters =
            [
                "idtoken": self.idtoken,
                "meetNotificationCountToReduce" : self.meetNotificationCountToReduce,
                "meetIdArray" : self.meetIdArray
                ] as [String: Any]
        
        let awsURL = AppConfig.ReduceMeetNotificationCount()
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                var count = data["userMeetMeNotifications"] as? Int ?? 0
                AppService.shared.noOfUnreadMeetNotifications = count
                NotificationCenter.default.post(name: REFRESH_MEET_NOTIFICATION_COUNT, object: nil)
                self.finish()
            } else {
                self.finish()
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
        
    }
    
}
