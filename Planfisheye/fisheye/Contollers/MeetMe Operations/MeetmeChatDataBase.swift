//
////
////  MeetmeChatDataBase.swift
////  fisheye
////
////  Created by venkatesh murthy on 03/09/18.
////  Copyright © 2018 Sankey Solutions. All rights reserved.
////
//
//import Foundation
//import SQLite
//import SwiftyJSON
//
//class DatabaseManagementofMessage{
//    
//    /* variable declaration */
//    var totalRecordsInLocalStorage: Int = 0
//    var recordIndex:Int = 0
//    let appSharedPrefernce = AppSharedPreference()
//    static let shared:DatabaseManagementofMessage = DatabaseManagementofMessage()
//    private let db: Connection?// DB connections
//    private let offlineChats = Table("offlineChats")
//    
//    /* declaring columns for the offlineChats table */
//    let meetMeId = Expression<String?>("meetMeId")
//    let message = Expression<String?>("message")
//    
//    private init()
//    {
//        /* declaring path for the database */
//        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
//        
//        /* creating database */
//        do {
//            db = try Connection("\(path)/FisheyeDatabase.sqlite3")
//        }
//        catch{
//            db = nil

//        }
//        
//    }
//    
//    /* checking ownerContacts table is already exists or not */
//    func isSQLiteTableAreadyExists() -> Bool{
//        do{
//            return try db!.scalar(
//                "SELECT EXISTS (SELECT * FROM sqlite_master WHERE type = 'table' AND name = ?)",
//                "offlineChats"
//                ) as! Int64 > 0
//        } catch{
//            return false
//        }3//    }
//    
//    func createOfflineChatsTable()
//    {
//        do{
//            try db!.run(offlineChats.create(ifNotExists: true){ (table) in
//                table.column(self.meetMeId)
//                table.column(self.message)
//            })
//        }
//        catch{
//        }
//    }
//    
//    func checKOfflineChatsTableEmpty() -> Int64{
//        do{
//            let totalRecordsInTable: Int64 = try db!.scalar("SELECT count() FROM offlineChats") as! Int64
//            return totalRecordsInTable
//        }
//        catch{
//        }
//    }
//    
//    /* reading records from the ownerContacts table of SQLite database */
////    func readDatabase() -> [messageObj]{
////        var messageToReturn:[messageObj]
////    }
//}
