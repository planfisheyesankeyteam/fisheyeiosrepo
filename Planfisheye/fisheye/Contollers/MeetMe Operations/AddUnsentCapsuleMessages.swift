//
//  AddUnsentCapsuleMessages.swift
//  fisheye
//
//  Created by Sankey Solutions on 06/09/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation
import ProcedureKit

class AddUnsentCapsuleMessages: Procedure {

    let appSharedPrefernce = AppSharedPreference()
    var idtoken = ""
    var meetmeId = ""
    var statusCode: String = ""
    var meetmeUnsentMsgsArray = [OfflineMeetMeCapsule]()
    var userId = ""
    var meetmeCapsule = MeetMeCapsule()
    var meetTitle = ""

    init(userId: String, meetmeId: String, meetmeUnsentMsgsArray: [OfflineMeetMeCapsule]) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        self.meetmeId = meetmeId
        self.userId = userId
        self.meetmeUnsentMsgsArray = meetmeUnsentMsgsArray
    }

    override func execute() {
        var meetCapsuleArray: [[String: Any]] = []

        for meetCapsule in meetmeUnsentMsgsArray {
            var mainMeessagesListOfSingleMeet: [AnyObject] = []
            let offlineMessageListForSingleMeet = meetCapsule.offlineMessages
            for message in offlineMessageListForSingleMeet {
                var meetmeMessage = [:] as [String: Any]
                meetmeMessage  =
                    [
                        "messageId": message.messageId,
                        "messageBody": message.messageBody,
                        "messageTime": message.messageTime,
                        "fisheyeId": message.fisheyeId
                    ] as [String: Any]
                mainMeessagesListOfSingleMeet.append(meetmeMessage as AnyObject)
            }
            var meetCapsuleObject = [:] as [String: Any]
            meetCapsuleObject = [
                "meetmeId": meetCapsule.meetmeId,
                "title": meetCapsule.meetmeTitle,
                "meetmeMessageList": mainMeessagesListOfSingleMeet
            ] as [String: Any]
            meetCapsuleArray.append(meetCapsuleObject)
        }

        if(meetCapsuleArray.count>0){
        
            let parameters =
                [
                    "idtoken": self.idtoken,
                    "action": "addunsentcapsule",
                    "meetmeCapsuleList": meetCapsuleArray

                ] as [String: Any]
           
            let awsURL = AppConfig.AddUnsentCapsuleMessagesURL(meetmeId: self.meetmeId)

            guard let URL = URL(string: awsURL) else {
                return

            }
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
    //        request.httpBody = "".data(using: String.Encoding.utf8)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.addValue(idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = "POST"
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                return
            }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                if let resultObject = json as? [String: Any] {
                    self.statusCode = resultObject["statusCode"] as? String ?? ""
                    if  let meetmeCapsule = json["meetMeCapsule"] as? [String: Any] {
                        self.meetmeCapsule.meetmeId = meetmeCapsule["meetmeId"] as? String ??  ""
                        self.meetmeCapsule.meetParticipantCount = meetmeCapsule["meetParticipantCount"] as? Int ??  0
                        if let hostParticipantData = meetmeCapsule["hostParticipant"]  as? [String: Any] {
                            self.meetmeCapsule.hostParticipant.fisheyeId = hostParticipantData["fisheyeId"] as? String ??  ""
                            self.meetmeCapsule.hostParticipant.participantPicture = hostParticipantData["participantPicture"] as? String ??  ""
                            self.meetmeCapsule.hostParticipant.participantName = hostParticipantData["participantName"] as? String ??  ""
                        }
                        self.meetmeCapsule.messages = []
                        if  let messages = meetmeCapsule["meetmeMessages"] as? [[String: Any]] {
                            for j in 0..<messages.count {
                                let meetmeMessageObj =  MeetMeMessage()
                                meetmeMessageObj.fisheyeId =  messages[j]["fisheyeId"] as? String ??  ""
                                meetmeMessageObj.messageId =  messages[j]["messageId"] as? String ??  ""
                                meetmeMessageObj.messageBody =  messages[j]["messageBody"] as? String ??  ""
                                meetmeMessageObj.isSent =  messages[j]["isSent"] as? Bool ??  false
                                meetmeMessageObj.name  =  messages[j]["name"] as? String ??  ""
                                meetmeMessageObj.picture =  messages[j]["picture"] as? String ??  ""
                                meetmeMessageObj.messageTime = messages[j]["messageTime"] as? Double
                                if let readByArray=messages[j] ["isReadBy"] as? [String] {
                                    for readBy in readByArray {
                                        meetmeMessageObj.isReadBy.append(readBy)
                                    }
                                    self.meetmeCapsule.messages.append(meetmeMessageObj)
                                }
                            }
                        }
                    }
                    self.finish()
                } else {
                    self.finish()
                }
            }
            })
            task.resume()
            session.finishTasksAndInvalidate()
        }else{
        }
    }
}
