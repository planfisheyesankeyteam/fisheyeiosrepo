//
//  MeetMeLogbook.swift
//  fisheye
//
//  Created by Sankey Solutions on 22/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
class MeetMeLogbookObject {
    init() {

    }

    /** Address Id generate from server side when used try to add new address. */
    var meetmeId: String?
    /** Full mailing address, formatted for display or use on a mailing label. */
    var meetmeTitle: String?
    /** Full street address component, which MAY include house number, street name, Post Office Box, and multi-line extended street address information */
    var meetmeLocation: String?
    /** City or locality component. */
    var latitude: String?
    /** State, province, prefecture, or region component. */
    var longitude: String?

}
