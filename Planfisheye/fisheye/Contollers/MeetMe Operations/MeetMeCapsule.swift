//
//  MeetMeCapsule.swift
//  fisheye
//
//  Created by Sankey Solutions on 14/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
class MeetMeCapsule {
    init() {

    }
    var meetmeId: String?
    var meetmeTitle: String?
    var capsuleCreatedDate: String?
    var messages =  [MeetMeMessage()]
    var hostParticipant = ParticipantVariables()
    var meetParticipantCount: Int?
}

class MeetMeMessage {
    init() {

    }
    var messageId: String?
    var fisheyeId: String?
    var messageBody: String?
    var name: String?
    var picture: String?
    var messageTime: Double?
    var isReadBy: [String] = []
    var isSent: Bool?
}

class OfflineMeetMeCapsule {
    init() {
        
    }
    var meetmeId: String?
    var meetmeTitle: String?
    var offlineMessages: [MeetMeMessage] = []
}
