//
//  MeetMeContact.swift
//  fisheye
//
//  Created by Sankey Solutions on 05/01/18.
//  Copyright © 2018 Fisheye. All rights reserved.
//

import Foundation
class MeetMeContact {
    init() {

    }
    var contactId: String?
    var fisheyeId: String?
    var contactName: String?
    var phoneNumber: String?
    var email: String?
    var picture: String?
}
