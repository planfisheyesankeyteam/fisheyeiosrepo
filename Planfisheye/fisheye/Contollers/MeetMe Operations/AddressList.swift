//
//  Address.swift
//  fisheye
//
//  Created by Sankey Solutions on 01/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
class AddressList {
    init() {

    }

    /** Address Id generate from server side when used try to add new address. */
    var addressId: String?
    /** Full mailing address, formatted for display or use on a mailing label. */
    var formatted: String?
    /** Full street address component, which MAY include house number, street name, Post Office Box, and multi-line extended street address information */
    var street_address: String?
    /** City or locality component. */
    var locality: String?
    /** State, province, prefecture, or region component. */
    var region: String?
    /** Zip code or postal code component. */
    var postal_code: String?
    /** Country name component. */
    var country: String?
    /** Type may be Home/Office/Business etc */
    var type: String?
}
