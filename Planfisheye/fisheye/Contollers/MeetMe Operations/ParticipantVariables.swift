//
//  MeetMeVariables.swift
//  fisheye
//
//  Created by Sankey Solutions on 22/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
class ParticipantVariables {
    init() {

    }
    var participantName: String?

    var participantPhoneNumber: String?

    var participantEmail: String?

    var participantPicture: String?

    var isAccepted: Bool?

    var isDeclined: Bool?

    var isHost: Bool!

    var contactId: String?

    var istrackingEnabled: Bool?

    var trackingStatus: String?

    var trackingTime: String?

    var participantId: String?

    var fisheyeId: String?

    var isCapsuleLocked: Bool?

    var messageCount: Int?

    var trackingMode: String?
    
    var isSeen: Bool?
}
