//
//  MeetmMeNotificationVariables.swift
//  fisheye
//
//  Created by Sankey Solutions on 02/04/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

class MeetmMeNotificationVariables {
    init() {

    }
    var meetmeId: String?
    var meetmeTitle: String?
    var meetmeAddress: String?
    var meetmeNotificationDateTime: Double?
    var notificationType: String?
    var imageString: String?
    var unreadCapsuleCount: Int?
}
