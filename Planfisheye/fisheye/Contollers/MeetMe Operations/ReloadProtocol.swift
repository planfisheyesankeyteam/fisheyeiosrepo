//
//  ReloadProtocol.swift
//  fisheye
//
//  Created by Sankey Solution on 12/6/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation

protocol ReloadProtocol {
    func reloadData(toReload: Bool)
}

protocol AddressDeleteProtocol {
    func deleteAddress(toDelete: Bool)
}

protocol privacySaveDataProtocol {
    func saveData(toReload: Bool)
}

protocol ModifySignUpDataProtocol {
    func modifyData(toReload: Bool)
}

protocol DeleteUserProtocol {
    func deleteData(toReload: Bool)
}

protocol RedirectToPrivacyControl {
    func redirect(toRedirect: Bool)
}

protocol RedirectToPSecurity {
    func security(toRedirect: Bool)
}

protocol ReloadPulseLogbook {
    func reloadPulseListing(toReload: Bool)
}

protocol SendSelectedContacts {
    func addContacts(addedContacts: [ContactObj])
}

protocol AcceptMeet {
    func meetAccepted(discloseTrail: String, discloseTrailTime: String, transportMode: String)
}

protocol RejectDismissBtn {
    func dismissBtn()
}

protocol RedirectToDashboard {
    func redirectDashboard(toRedirect: Bool)
}

protocol RedirectedToMeetmeLogbookVC {
    func nextAction(method: String)
}
protocol RedirectProtocol {
    func nextAction(method: String)
}

protocol MakeUnreadCapsuleCountZero {
    func makeCountZero(isToMakeCountZero: Bool)
}

protocol MakeUnreadCapsuleCountZeroOnMeetDetails {
    func makeCountZeroOnDetails(isToMakeCountZero: Bool)
}

let REFRESH_MEET_NOTIFICATION = Notification.Name("RefreshMeet")
let REFRESH_MEET_CONTACT = Notification.Name("BackContact")
let LOAD_MEET_BASE_VIEW = Notification.Name("LoadMeetMeBaseVC")
let REFRESH_PUSH_NOTIFICATIONS = Notification.Name("RefreshNotifications")
let STOP_LOCATION_UPDATE = Notification.Name("StopLocationUpdate")
let CHANGE_LANGUAGE = Notification.Name("ChangeLanguage")
let HIDE_CREATE_MEET_BTN = Notification.Name("HideCreateMeetBtn")
let REFRESH_MEET_NOTIFICATION_COUNT = Notification.Name("RefreshMeetNotificationCount")
let REFRESH_MEET_UNREAD_CAPSULE_COUNT = Notification.Name("RefreshMeetUnreadCapsuleCount")


