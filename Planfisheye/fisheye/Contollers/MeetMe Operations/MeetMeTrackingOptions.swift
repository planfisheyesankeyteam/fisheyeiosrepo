//
//  MeetMeTrackingOptions.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 22/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

struct MeetMeTrackingOptions {

      /* ------------------------------------------------ vaiable declaration -------------------------------------------- */

       static let shared: MeetMeTrackingOptions = MeetMeTrackingOptions()
      //static var meetTrackingOptions: [MeetMeTrackingOptionsList] = []
      static var trackingOption: [TrackingOption] = []
      static var meetmeParticipantLimit: Int?
      static var mapResetTimeInterval: Double?
      static var meetingTrackableTime: Double?
      static var trackingMode: [String] = ["Walking", "Driving"]
}
