//
//  capsuleInsertPopUpViewController.swift
//  fisheye
//
//  Created by Anuradha on 12/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import CoreLocation
import Toast_Swift

class capsuleInsertPopUpViewController: UIViewController, UITextViewDelegate, CLLocationManagerDelegate {

      /*************************************** outlet variables *************************************/

    @IBOutlet weak var openMasterkeyButton: UILabel!
    @IBOutlet weak var capsuleIsLockedLabel: UILabel!
      @IBOutlet weak var capsuleMessage: UITextView!
      @IBOutlet weak var viewContainsMasterKeyLock: UIView!
      @IBOutlet weak var charLimitLabel: UILabel!
      @IBOutlet weak var enterYourCapsuleLabel: UILabel!
      @IBOutlet weak var underLineToUpperView: UIView!
      @IBOutlet weak var insertCapsuleLabel: UILabel!
      @IBOutlet weak var upperViewOfInsertLabel: UIView!
      @IBOutlet weak var insertCapsulePopUp: UIView!
      @IBOutlet weak var baseViewForInsertCapsulePopUp: UIView!
//      @IBOutlet weak var masterKeyBtn: UIButton!
      @IBOutlet weak var optionalLabel: UILabel!
      
      @IBOutlet weak var submitButton: UIButton!
      @IBOutlet weak var backArrowIconOnInsertCapsule: UIImageView!
      @IBOutlet weak var backButtonOfInsertCapsule: UIButton!
      @IBOutlet weak var closeButtonOnInsertCapsule: UIButton!
      @IBOutlet weak var closeIconOnInsertCapsule: UIImageView!

    @IBAction func MasterKeyButton(_ sender: Any) {
        self.validateAndOpenMasterKeyOrSendCapsule(ifCapsuleOpenMasterKeyOrsendCapsule: "openMasterKey")
    }
    
    
      @IBAction func closeButtonOfInsertcapsuleIsClicked(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      @IBAction func nextBtnOfInsertCap(_ sender: Any) {
            self.validateAndOpenMasterKeyOrSendCapsule(ifCapsuleOpenMasterKeyOrsendCapsule: "sendCapsule")
      }

      @IBAction func backBtnOnInsertCapsulePopUp(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.optionalLabel.text = self.pulseMsgs.optionalCapsuleMsg
                  self.capsuleIsLockedLabel.text = self.pulseMsgs.capsuleLockedUnlockedLabel
                  self.enterYourCapsuleLabel.text = self.pulseMsgs.enterYourCapsuleLabel
                  self.capsuleMessage.text = self.pulseMsgs.capsuleMsgPlaceHolder
                  self.insertCapsuleLabel.text = self.pulseMsgs.capsule
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /************************************** globle variables ***********************************/

      var contactIdArray=[String]()
      var pulseType: String?
      var idtoken: String?
      var appService = AppService()
      let appSharedPrefernce = AppSharedPreference()
      var pulseMsgs = PulseToastMsgHeadingSubheadingLabels()
      var scheduledOrNow: String?
      var pulseTime: Double = 0.0
      var pulseStartDate: Double = 0.0
      var pulseEndDate: Double = 0.0
      var pulseRepeatersArray=[String]()
      var fromAcceptPulse: Bool = false
      var acceptThisPulse: String!
      var acceptThisPulseObj=PulseDetails()
      var respondToId: String!
      let appDelegete = UIApplication.shared.delegate as! AppDelegate
      var deletePulseProtocol: DeletePulseProtocol?
      var pulseListingProtocol: PulseListingProtocol?
      var pulseInSyncProtocol: PulseInSyncProtocol?
      var fromListingWhileAccepting: Bool = false
      var fromDetailsWhileAccepting: Bool = false
      var respondToPulseIdIsScheduled: Bool = false
      var shareAndRequestPulseVCViewController = ShareAndRequestPulseVCViewController()
      var reachedToEditPulseFromSR: Bool = false
      var pulseToBeUpdated: String = ""
      var capsuleToBeEdited: String = ""
      let maximunCharLimit: Int = 100
      var youCanEnterThisMuchMore: Int = 0
      var sendAPulseToFromSync: String = ""
      var pulseTypeFromSync: Int = 0
      var isPulseNeedToSendFromSync: Bool = false

      /************************************* Predefined function ***********************************/

      override func viewDidLoad() {

            super.viewDidLoad()
            self.stopLoader()
            self.appService = AppService.shared
            self.pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
            self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

            self.setLocalizationText()

            self.addObservers()

            if(self.fromAcceptPulse || self.isPulseNeedToSendFromSync) {//from listing/details to while accepting pulse
                  self.backArrowIconOnInsertCapsule.isHidden=true
                  self.backButtonOfInsertCapsule.isHidden=true
                  self.closeIconOnInsertCapsule.isHidden=false
                  self.closeButtonOnInsertCapsule.isHidden=false
                  self.optionalLabel.text=self.pulseMsgs.optionalCapsuleMsg
            } else {//while sharing/requesting pulse
                  self.closeIconOnInsertCapsule.isHidden=true
                  self.closeButtonOnInsertCapsule.isHidden=true
            }

            self.capsuleMessage.textColor = UIColor.capsuleTextViewPlaceHolderColor()

        self.viewContainsMasterKeyLock.layer.cornerRadius=self.viewContainsMasterKeyLock.frame.size.width/2
            let borderColor = UIColor(red: 112.0/255.0, green: 112.0/255.0, blue: 112.0/255.0, alpha: 1.0)
            self.viewContainsMasterKeyLock.layer.borderColor = borderColor.cgColor
            self.viewContainsMasterKeyLock.layer.borderWidth = 1.0
            self.insertCapsulePopUp.layer.cornerRadius=5

//            let masterKeyTabView = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
//            self.viewContainsMasterKeyLock.addGestureRecognizer(masterKeyTabView)
            capsuleMessage.delegate = self

            NotificationCenter.default.addObserver(forName: DISMISS_LOADER_WHEN_LOCATION_SETTING_OPENED, object: nil, queue: nil) { (_) in
                  self.stopLoader()
            }

            if(self.reachedToEditPulseFromSR) {
                  if(self.capsuleToBeEdited != "") {
                        self.capsuleMessage.textColor=UIColor.black
                        self.capsuleMessage.text=self.capsuleToBeEdited
                  } else {
                  }
            }
        
            if(self.isPulseNeedToSendFromSync){
                self.contactIdArray.append(self.sendAPulseToFromSync)
            }
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.capsuleInsertPopUpViewController)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      /*********************************** Text View Functions ****************************************/

      func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            let maxCharacter: Int = 100
            return (capsuleMessage.text?.utf16.count ?? 0) + text.utf16.count - range.length <= maxCharacter
      }

      func textViewDidBeginEditing(_ textView: UITextView) {
            self.enterYourCapsuleLabel.textColor=UIColor.black
            if(self.capsuleMessage.text == self.pulseMsgs.capsuleMsgPlaceHolder) {
                  self.capsuleMessage.text=""
                  self.capsuleMessage.textColor = UIColor.black
            }
      }

      func textViewDidEndEditing(_ textView: UITextView) {
            if(self.capsuleMessage.text.characters.count == 0) {
                  self.capsuleMessage.textColor = UIColor.capsuleTextViewPlaceHolderColor()
                  self.capsuleMessage.text = self.pulseMsgs.capsuleMsgPlaceHolder
            }
      }

      func textViewDidChange(_ textView: UITextView) {
            if(self.capsuleMessage.text != self.pulseMsgs.capsuleMsgPlaceHolder || self.capsuleMessage.text != "") {
                  self.enterYourCapsuleLabel.textColor=UIColor.black
            }
        let youCanEnterThisMuchMore = maximunCharLimit - (capsuleMessage.text?.count)!
            self.charLimitLabel.text = "\(youCanEnterThisMuchMore)"
      }

      /********************************* Pop-up/toast msg related functions ******************************/

      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
            nextVC.insertCapsuleProtocol = self as InsertCapsuleProtocol
            self.present(nextVC, animated: true, completion: nil)
      }

      //a common alert pop-up
      func dualButtonpopUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "AllPopupDualActionVC") as! AllPopupDualActionVC
            //  nextVC.quitButtonLabel.text="decline"
            //nextVC.deletePulseProtocol = self as DeletePulseProtocol
            self.present(nextVC, animated: true, completion: nil)
      }

      func ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: Bool, toastMsg: String) {
            DispatchQueue.main.async {
                  self.view.makeToast(toastMsg)
               
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse, execute: {
                  self.view.hideToast()
                  if(isActionAcceptPulse) {
                  } else {
                    if(self.isPulseNeedToSendFromSync){
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.openPulseListingPage()
                    }
                  }
            })
      }

      func addContactToYourContactListToRespond() {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: self.pulseMsgs.addContactHeading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: self.pulseMsgs.addContactSubHeading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: "warning")
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: "addReqContToRespToPulseReq")
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: "N/A")
            self.dualButtonpopUpVCController()
      }

      /********************************** new screen related function *************************************/

      func  openMAsterKeyPopUp() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController")  as! MasterKeyPopUpViewController
            nextVC.insertCapsuleProtocol = self as InsertCapsuleProtocol
            nextVC.capsuleMessage=self.capsuleMessage.text
        
            if(self.pulseEndDate==0.0) {
                nextVC.pulseRepeatersArray=[]
            } else {
                nextVC.pulseRepeatersArray=self.pulseRepeatersArray
            }
            nextVC.contactIdArray=self.contactIdArray
            nextVC.pulseType=self.pulseType
            nextVC.scheduledOrNow=self.scheduledOrNow
            nextVC.pulseTime=self.pulseTime
            nextVC.pulseStartDate=self.pulseStartDate
            nextVC.pulseEndDate=self.pulseEndDate

            nextVC.fromAcceptPulse=self.fromAcceptPulse
            nextVC.respondToId=self.respondToId
            nextVC.acceptThisPulse=self.acceptThisPulse
            nextVC.fromListingWhileAccepting=self.fromListingWhileAccepting
            nextVC.fromDetailsWhileAccepting=self.fromDetailsWhileAccepting
            nextVC.acceptThisPulseObj=self.acceptThisPulseObj
            nextVC.respondToPulseIdIsScheduled=self.respondToPulseIdIsScheduled
            nextVC.pulseToBeUpdated=self.pulseToBeUpdated
            nextVC.reachedToEditPulseFromSR=self.reachedToEditPulseFromSR
        
            nextVC.sendAPulseToFromSync = self.sendAPulseToFromSync
            nextVC.pulseTypeFromSync = self.pulseTypeFromSync
            nextVC.isPulseNeedToSendFromSync = self.isPulseNeedToSendFromSync
            nextVC.dismissCapsulePopUp = self as DismissCapsulePopUpToShowSync
            self.present(nextVC, animated: true, completion: nil)
      }

      func openPulseListingPage() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController")  as! BaseViewController
            nextVC.isToOpenPulse = true
            self.present(nextVC, animated: false, completion: nil)
      }

      /************************************ Hepling Functions ********************************************/

    func validateAndOpenMasterKeyOrSendCapsule(ifCapsuleOpenMasterKeyOrsendCapsule: String) {
            if(ifCapsuleOpenMasterKeyOrsendCapsule == "openMasterKey") {
                if(self.capsuleMessage.text == "" || self.capsuleMessage.text == self.pulseMsgs.capsuleMsgPlaceHolder || self.capsuleMessage.text.trimmingCharacters(in: .whitespaces) == "") {
                    self.enterYourCapsuleLabel.textColor=UIColor.red
                } else {
                    let obj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
                    if let tempObj = obj {
                        let masterKey = tempObj["masterKey"]  as? String ?? ""
                        if masterKey == "" {
                            self.view.makeToast(self.pulseMsgs.addMasterKeyInProfile, duration: PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse, position: .bottom)
                        }else{
                            self.openMAsterKeyPopUp()
                        }
                    }
                }
            }else {
                if( self.capsuleMessage.text == self.pulseMsgs.capsuleMsgPlaceHolder ){
                    self.capsuleMessage.text = ""
                }
                self.actionAccIsCapulseInserted()
            }
    }

    func startLoader() {
      DispatchQueue.main.async {
        self.capsuleMessage.isUserInteractionEnabled =  false
        self.submitButton.isUserInteractionEnabled = false
        self.openMasterkeyButton.isUserInteractionEnabled = false
//        self.viewContainsMasterKeyLock.isUserInteractionEnabled = false
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }
    }

    func stopLoader() {
       DispatchQueue.main.async {
        self.capsuleMessage.isUserInteractionEnabled =  true
        self.submitButton.isUserInteractionEnabled = true
        self.openMasterkeyButton.isUserInteractionEnabled = true
//        self.viewContainsMasterKeyLock.isUserInteractionEnabled = true
//        self.view.isUserInteractionEnabled=true
        ScreenLoader.shared.stopLoader()
      }
    }

      func actionAccIsCapulseInserted() {
            //while editing pulse from pulse details to share and request and then to Insert capsule
            if(self.reachedToEditPulseFromSR) {
                  self.updateScheduledPulse(pulseId: self.pulseToBeUpdated)
                  //while accepting pulse from pulse details/pulse listing
            } else if(self.fromAcceptPulse) {
                  if(self.respondToPulseIdIsScheduled) {
                        self.acceptPulseAndShareLocAtScheduledTime()
                  } else {
                    self.acceptPulseAndShareCapsuleWithLocation()
                  }
            } else if(self.isPulseNeedToSendFromSync){
                if(self.pulseTypeFromSync == 0) {
                   self.requestPulseNow()
                } else {
                    self.sharePulseNow()
                }
            }else{
                  if(self.pulseType=="0") {
                        if(self.scheduledOrNow=="Now" ) {
                              self.requestPulseNow()
                        } else if (self.scheduledOrNow=="Scheduled") {
                              self.requestPulseAtScheduled()
                        }
                  } else if(self.pulseType=="1") {
                        if(self.scheduledOrNow=="Now") {
                              self.sharePulseNow()
                        } else if(self.scheduledOrNow=="Scheduled") {
                              self.sharePulseAtScheduled()
                        }
                  }
            }
      }

      /**************************************** API call Share/Request ***************************************/

      func requestPulseNow() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                    
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.networkFailureMsg)
                        return
            }
        
            self.startLoader()
        
            var message = self.capsuleMessage.text as String
            if self.capsuleMessage.text == self.pulseMsgs.capsuleMsgPlaceHolder{
                message = ""
            }
            let feedbackOperation = RequestAPulseNow(isEncrypted: false, message: message, recipientsIdArray: self.contactIdArray)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  if(operation.statusCode == "200") {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: false, toastMsg: self.pulseMsgs.nowPulseRequest)
                  } else {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.serverErrorOfPulse)
                  }
                
                self.stopLoader()
                
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }

      func sharePulseNow() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.networkFailureMsg)
                        return
            }
        
            self.startLoader()

            let status = CLLocationManager.authorizationStatus()
        

            switch status {
            case .authorized, .authorizedWhenInUse, .authorizedAlways :
                var message = self.capsuleMessage.text as String
                if self.capsuleMessage.text == self.pulseMsgs.capsuleMsgPlaceHolder{
                    message = ""
                }
                  let feedbackOperation = ShareAPulseNow(isEncrypted: false, message: message, recipientsIdArray: self.contactIdArray)
                  feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                    
                        self.stopLoader()
                        if(operation.statusCode == "200") {
                                self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: false, toastMsg: self.pulseMsgs.nowPulseShare)
                        } else {
                              self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.serverErrorOfPulse)
                        }
                  }
                  AppDelegate.addProcedure(operation: feedbackOperation)
            case .denied:
                  self.stopLoader()
                  
                  let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
                  alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                  })
                  alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                        alert.dismiss(animated: true, completion: nil)
                  })
                  present(alert, animated: true)
                  break
                  
               case .notDetermined:
                    self.stopLoader()
                  GetLocationGlobally.shared.initialize()
                  break
                  
            default :
                  self.stopLoader()
                  break
            }
      }

    func acceptPulseAndShareCapsuleWithLocation() {
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.networkFailureMsg)
                return
        }
        self.startLoader()

        let status = CLLocationManager.authorizationStatus()
        

        switch status {
        case .authorized, .authorizedWhenInUse, .authorizedAlways :
            let feedbackOperation = AcceptNowPulseRequest(isEncrypted: false, message: self.capsuleMessage.text, respondToPulseOf: self.respondToId, pulseToWhichUserResponding: self.acceptThisPulse)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                DispatchQueue.main.async {
                    self.stopLoader()
                }
                if(operation.statusCode == "200") {
                    self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.nowPulseReqAccepted)
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Respond to requested pulse", action: "Accept pulse", label: "Accepted to share current location", value: 0)
                    if(self.fromDetailsWhileAccepting) {
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
                            self.dismiss(animated: true, completion: {
                                self.deletePulseProtocol?.afetrAcceptingPulse(toReload: true)
                            })
                        })
                    } else if(self.fromListingWhileAccepting) {
                        self.dismiss(animated: true, completion: {
                            self.pulseListingProtocol?.afetrAcceptingPulse(toReload: true)
                        })
                    }
                } else if(operation.statusCode == "409") {
                    if(self.fromDetailsWhileAccepting) {
                        self.dismiss(animated: false, completion: {
                            self.deletePulseProtocol?.toAlertToAddRequestInContactList(toReload: true)
                        })
                    } else if(self.fromListingWhileAccepting) {
                        self.dismiss(animated: false, completion: {
                            self.pulseListingProtocol?.toAlertToAddRequestInContactList(toReload: true)
                        })
                    }
                } else {
                    self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.serverErrorOfPulse)
                }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
        case .denied, .notDetermined:
            self.stopLoader()
            let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                alert.dismiss(animated: true, completion: nil)
            })
            present(alert, animated: true)
            break
        default :
            self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.unableToFetchLastLocation)
            self.stopLoader()
            break
        }
    }

      func requestPulseAtScheduled() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.networkFailureMsg)
                        return
            }
            self.startLoader()
        

            let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: false, message: self.capsuleMessage.text, recipientsIdArray: self.contactIdArray, startDateTimeStamp: self.pulseStartDate, endDateTimeStamp: self.pulseEndDate, pulseTimeTimeStamp: self.pulseTime, repeatersArray: self.pulseRepeatersArray, pulseType: "0")
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                
                    self.stopLoader()
               
                if(operation.statusCode == "200") {
                    self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: false, toastMsg: self.pulseMsgs.scheduledPulseRequest)
                } else {
                    self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.serverErrorOfPulse)
                }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)

      }

      func sharePulseAtScheduled() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.networkFailureMsg)
                        return
            }
            self.startLoader()

            let feedbackOperation = RequestOrSharePulseAtScheduledTime(isEncrypted: false, message: self.capsuleMessage.text, recipientsIdArray: self.contactIdArray, startDateTimeStamp: self.pulseStartDate, endDateTimeStamp: self.pulseEndDate, pulseTimeTimeStamp: self.pulseTime, repeatersArray: self.pulseRepeatersArray, pulseType: "1")
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                
                self.stopLoader()
                
                if(operation.statusCode == "200") {
                    self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: false, toastMsg: self.pulseMsgs.scheduledPulseShare)
                } else {
                    self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.serverErrorOfPulse)
                }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)

      }

      func updateScheduledPulse(pulseId: String) {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.networkFailureMsg)
                        return
            }
       
            self.startLoader()
            let feedbackOperation = UpdatePulseData(isEncrypted: false, message: self.capsuleMessage.text, recipientsIdArray: self.contactIdArray, toUpdatePulseId: self.pulseToBeUpdated, pulseTime: self.pulseTime, pulseStartDate: self.pulseStartDate, pulseEndDate: self.pulseEndDate, pulseDayRepeaters: self.pulseRepeatersArray, pulseType: self.pulseType!)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                
                self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: false, toastMsg: self.pulseMsgs.pulseUpdated)
                  } else {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.serverErrorOfPulse)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
        }

//
//    func acceptPulseAndShareLocAtScheduledTime(){
//      guard NetworkStatus.sharedManager.isNetworkReachable()
//            else {
//                  self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse:true,toastMsg:self.pulseMsgs.networkFailureMsg)
//                  return
//      }
//        self.startLoader()
//
//
//        let feedbackOperation = AcceptAScheduledPulseRequest(isEncrypted:false, message:self.capsuleMessage.text, respondToPulseOf:self.respondToId, pulseToWhichUserResponding:self.acceptThisPulse, acceptingPulseStartDate:self.acceptThisPulseObj.pulseStartDate, acceptingPulseEndDate:self.acceptThisPulseObj.pulseEndDate, acceptingPulsePulseTime:self.acceptThisPulseObj.pulseTime, acceptingPulseRepeatersArray:self.acceptThisPulseObj.pulseRepeatersForDays)
//
//        feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, errors) in
//            DispatchQueue.main.async{
//                self.stopLoader()
//            }
//            self.startLoader()
//
//
//            let status = CLLocationManager.authorizationStatus()

//
//            switch status {
//            case .authorized, .authorizedWhenInUse, .authorizedAlways :
//                  let feedbackOperation = AcceptNowPulseRequest(isEncrypted:false,message:self.capsuleMessage.text,respondToPulseOf:self.respondToId,pulseToWhichUserResponding:self.acceptThisPulse)
//                  feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, errors) in
//                        DispatchQueue.main.async{
//                              self.stopLoader()
//                        }
//                        if(operation.statusCode == "200"){
//                              self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse:true,toastMsg:self.pulseMsgs.nowPulseReqAccepted)
//                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Respond to requested pulse", action: "Accept pulse", label: "Accepted to share current location", value: 0)
//                              if(self.fromDetailsWhileAccepting){
//                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+AppService.shared.firstTimeInterval, execute: {
//                                          self.dismiss(animated: true, completion: {
//                                                self.deletePulseProtocol?.afetrAcceptingPulse(toReload:true)
//                                          })
//                                    })
//                              }else if(self.fromListingWhileAccepting){
//                                    self.dismiss(animated: true, completion: {
//                                          self.pulseListingProtocol?.afetrAcceptingPulse(toReload:true)
//                                    })
//                              }
//                        }else if(operation.statusCode == "409"){
//                              if(self.fromDetailsWhileAccepting){
//                                    self.dismiss(animated: false, completion: {
//                                          self.deletePulseProtocol?.toAlertToAddRequestInContactList(toReload: true)
//                                    })
//                              }else if(self.fromListingWhileAccepting){
//                                    self.dismiss(animated: false, completion: {
//                                          self.pulseListingProtocol?.toAlertToAddRequestInContactList(toReload: true)
//                                    })
//                              }
//                        }else{
//                              self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse:true,toastMsg:self.pulseMsgs.serverErrorOfPulse)
//                        }
//                  }
//                  AppDelegate.addProcedure(operation: feedbackOperation)
//            case .denied, .notDetermined:
//                  self.stopLoader()
//                  let alert = UIAlertController(title: nil, message: "GPS access is restricted. In order to share location, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
//                  alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
//                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
//                  })
//                  alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
//                        alert.dismiss(animated: true, completion: nil)
//                  })
//                  self.present(alert, animated: true)
//                  break
//            default :
//                  self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse:true,toastMsg:self.pulseMsgs.unableToFetchLastLocation)
//                  self.stopLoader()
//                  break
//            }
//      }
//}

      func acceptPulseAndShareLocAtScheduledTime() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                    
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.networkFailureMsg)
                        return
            }
            self.startLoader()

            let feedbackOperation = AcceptAScheduledPulseRequest(isEncrypted: false, message: self.capsuleMessage.text, respondToPulseOf: self.respondToId, pulseToWhichUserResponding: self.acceptThisPulse, acceptingPulseStartDate: self.acceptThisPulseObj.pulseStartDate, acceptingPulseEndDate: self.acceptThisPulseObj.pulseEndDate, acceptingPulsePulseTime: self.acceptThisPulseObj.pulseTime, acceptingPulseRepeatersArray: self.acceptThisPulseObj.pulseRepeatersForDays)

            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                self.stopLoader()
                
                  if(operation.statusCode == "200") {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.schedPulseReqAccepted)
                        if(self.fromDetailsWhileAccepting) {
                              self.dismiss(animated: true, completion: {
                                    self.deletePulseProtocol?.afetrAcceptingPulse(toReload: true)
                              })
                        } else if(self.fromListingWhileAccepting) {
                              self.dismiss(animated: true, completion: {
                                    self.pulseListingProtocol?.afetrAcceptingPulse(toReload: true)
                              })
                        }
                  } else if(operation.statusCode == "409") {
                        if(self.fromDetailsWhileAccepting) {
                              self.dismiss(animated: false, completion: {
                                    self.deletePulseProtocol?.toAlertToAddRequestInContactList(toReload: true)
                              })
                        } else if(self.fromListingWhileAccepting) {
                            self.dismiss(animated: true, completion: {
                                self.pulseListingProtocol?.toAlertToAddRequestInContactList(toReload: true)
                            })
                        }
                  } else {
                        self.ShowToastMsgAndTakeAppropriateAction(isActionAcceptPulse: true, toastMsg: self.pulseMsgs.serverErrorOfPulse)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }
//

}//class ends here.

extension capsuleInsertPopUpViewController: InsertCapsuleProtocol {

      func acceptPulseAndShareCapsuleWithLocation(toReload: Bool) {
            self.acceptPulseAndShareCapsuleWithLocation()
      }

      func toAlertToAddRequestInContactList(toReload: Bool) {
            self.addContactToYourContactListToRespond()
      }

      func dismissInsertCapsulePopUp(toReload: Bool) {
            self.dismiss(animated: true, completion: nil)
      }

      func dismisCapsulePopUpAndUpdateAccRec(toReload: Bool) {
            self.dismiss(animated: true, completion: {
                  if(self.fromDetailsWhileAccepting) {
                        self.deletePulseProtocol?.afetrAcceptingPulse(toReload: true)
                  } else if(self.fromListingWhileAccepting) {
                        self.pulseListingProtocol?.afetrAcceptingPulse(toReload: true)
                  }
            })
      }
}

extension capsuleInsertPopUpViewController: DismissCapsulePopUpToShowSync{
    func dismissCapsule() {
        self.dismiss(animated: true, completion: nil)
    }
}
