//
//  SQLiteDatabaseManagement.swift
//  fisheye
//
//  Created by Sankey Solutions  vaishali on 04/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation
import SQLite

class DatabaseManagement {

      /* variable declaration */
      var totalRecordsInLocalStorage: Int = 0
      var recordIndex: Int = 0
      let appSharedPrefernce = AppSharedPreference()
      static let shared: DatabaseManagement = DatabaseManagement()
      private let db: Connection?  // database connections
      private let ownerContacts = Table("OwnerContacts") // contacts table
      private let recentlyAdded = Table("RecentlyAdded")

      /* declaring columns for the OwnerContacts table */
      let userId = Expression<String?>("userId")
      let name = Expression<String?>("name")
      let phoneNumber = Expression<String?>("phoneNumber")
      let secondaryPhoneNumbers = Expression<String?>("secondaryPhoneNumbers")
      let secondaryEmails = Expression<String?>("secondaryEmails")
      let contactId = Expression<Int>("contactId")
      let privacyEnabled = Expression<String?>("privacyEnabled")
      let privacySetForContactUser = Expression<String?>("privacySetForContactUser")
      let addressString1 = Expression<String?>("addressString1")
      let addressString2 = Expression<String?>("addressString2")
      let email = Expression<String?>("email")
      let companyEmail = Expression<String?>("companyEmail")
      let title = Expression<String?>("title")
      let notes = Expression<String?>("notes")
      let company = Expression<String?>("company")
      let website = Expression<String?>("website")
      let isBackendSync = Expression<String?>("isBackendSync")
      let source = Expression<String?>("Source")
      let contactFEId = Expression<String?>("contactFEId")
      let isdeleted = Expression<String?>("isdeleted")
      let ismerged = Expression<String?>("ismerged")
      let picture = Expression<String?>("picture")
      let mergeParentId = Expression<String?>("mergeParentId")
      let isBlocked = Expression<String?>("isBlocked")
      let isBlockedByContact = Expression<String?>("isBlockedByContact")
      let sharedData = Expression<Int?>("sharedData")
      let createdAt = Expression<String?>("createdAt")
      let updatedAt = Expression<String?>("updatedAt")

      private init() {
            /* declaring path for the database */
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!

            /* creating database */
            do {
                  db = try Connection("\(path)/FisheyeDatabase.sqlite3")
            } catch {
                  db = nil
            }

            //adding createdAt column in ownercontacts table
            do {
                  try db?.run(ownerContacts.addColumn(self.createdAt))
            } catch {
                
            }
            do {
                  try db?.run(ownerContacts.addColumn(self.updatedAt))
            } catch {
                
            }

            do {
                  try db?.run(recentlyAdded.addColumn(self.createdAt))
            } catch {
                
            }
            do {
                  try db?.run(recentlyAdded.addColumn(self.updatedAt))
            } catch {
                
            }

            do {
                  try db?.run(ownerContacts.addColumn(self.privacySetForContactUser, defaultValue: "1"))
            } catch {
                
            }

            do {
                  try db?.run(recentlyAdded.addColumn(self.privacySetForContactUser, defaultValue: "1"))
            } catch {
                
            }

//            do{
//                  let toDelete = ownerContacts.filter(self.isdeleted == "1")
//                  try db!.run(toDelete.delete())
//            }catch{
//
//            }

            //         droping ownerContacts table
            //        do{
            //            try db!.run(ownerContacts.drop(ifExists: true))
            //        }
            //        catch{
            //
            //        }

            //  droping ownerContacts table
            //        do{
            //            try db!.run(recentlyAdded.drop(ifExists: true))
            //        }
            //        catch{
            //
            //        }
      }

      /* checking ownerContacts table is already exists or not */
      func isSQLiteTableAreadyExists() -> Bool {
            do {
                  return try db!.scalar(
                        "SELECT EXISTS (SELECT * FROM sqlite_master WHERE type = 'table' AND name = ?)",
                        "OwnerContacts"
                        ) as! Int64 > 0
            } catch {
                
                  return false
            }
      }
      /* End */

      /* creating table(OwnerContacts) for owner contacts in SQLite Database */
      func createOwenerContactsTable() {

            do {
                  try db!.run(ownerContacts.create(ifNotExists: true) { (table) in
                        table.column(self.userId)
                        table.column(self.phoneNumber)
                        table.column(self.name)
                        table.column(self.secondaryPhoneNumbers)
                        table.column(self.secondaryEmails)
                        table.column(self.contactId, primaryKey: true)
                        table.column(self.addressString1)
                        table.column(self.addressString2)
                        table.column(self.email)
                        table.column(self.privacyEnabled)
                        table.column(self.privacySetForContactUser)
                        table.column(self.title)
                        table.column(self.company)
                        table.column(self.companyEmail)
                        table.column(self.website)
                        table.column(self.notes)
                        table.column(self.isBackendSync)
                        table.column(self.source)
                        table.column(self.contactFEId)
                        table.column(self.isdeleted)
                        table.column(self.ismerged)
                        table.column(self.picture)
                        table.column(self.mergeParentId)
                        table.column(self.isBlocked)
                        table.column(self.isBlockedByContact)
                        table.column(self.sharedData)
                        table.column(self.createdAt)
                        table.column(self.updatedAt)
                  })
            } catch {
                
            }
      }
      /* End */

      /* creating table(RecentlyAdded) for owner contacts that is added in FESYNC in SQLite Database */
      func createRecentlyAddedTable() {

            do {
                  try db!.run(recentlyAdded.create(ifNotExists: true) { (table) in
                        table.column(self.userId)
                        table.column(self.phoneNumber)
                        table.column(self.name)
                        table.column(self.secondaryPhoneNumbers)
                        table.column(self.secondaryEmails)
                        table.column(self.contactId, primaryKey: true)
                        table.column(self.addressString1)
                        table.column(self.addressString2)
                        table.column(self.email)
                        table.column(self.privacyEnabled)
                        table.column(self.privacySetForContactUser)
                        table.column(self.title)
                        table.column(self.company)
                        table.column(self.companyEmail)
                        table.column(self.website)
                        table.column(self.notes)
                        table.column(self.isBackendSync)
                        table.column(self.source)
                        table.column(self.contactFEId)
                        table.column(self.isdeleted)
                        table.column(self.ismerged)
                        table.column(self.picture)
                        table.column(self.mergeParentId)
                        table.column(self.isBlocked)
                        table.column(self.isBlockedByContact)
                        table.column(self.sharedData)
                        table.column(self.createdAt)
                        table.column(self.updatedAt)
                  })
            } catch {
                
            }
      }
      /* End */

      /* checking ownerContacts table of SQLite database is empty or not */
      func checkIsOwnerContactsTableEmpty() -> Int64 {
            do {
                  let totalRecordsInTable: Int64 = try db!.scalar("SELECT count() FROM ownerContacts") as! Int64
                  return totalRecordsInTable
            } catch {
                  return 0
            }
      }
      /* End */

      /* reading records from the ownerContacts table of SQLite database */
      func readDatabase() -> [ContactObj] {
            var contactsToReturn: [ContactObj] = []
            do {
                  //            let contactsFetched = try self.db?.prepare(self.ownerContacts.filter((self.isdeleted == "0") && (self.ismerged == "0")))

                  let contactsFetched = try self.db?.prepare(self.ownerContacts)
                  contactsToReturn =  self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
            } catch {
            }
            return contactsToReturn
      }
      /* End */

      /* reading records from the ownerContacts table of SQLite database */
      func readRecentlyAddedDatabase() -> [ContactObj] {
            var contactsToReturn: [ContactObj] = []
            do {
                  let contactsFetched = try self.db?.prepare(self.recentlyAdded)
                  contactsToReturn =  self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
            } catch {
                
            }
            return contactsToReturn
      }
      /* End */

      /* inserting contacts in to SQLite ownerContacts table */
      func insertContactsInSQLiteStorage(contactObj: ContactObj) {
            let str1: Int = contactObj.contactId
            let str2: Int = self.recordIndex
            let contactId = str1 + str2
            var privacySetForContactUser: String = ""
            self.recordIndex = self.recordIndex + 1
            if(contactObj.privacySetForContactUser == false) {
                  privacySetForContactUser = "0"
            } else {
                  privacySetForContactUser = "1"
            }

            let insertContact = self.ownerContacts.insert(self.userId <- contactObj.userId, self.contactId <- contactId, self.name <- contactObj.name, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.companyEmail <- contactObj.companyEmail, self.title <- contactObj.title, self.company <- contactObj.company, self.website <- contactObj.website, self.notes <- contactObj.notes, self.isBackendSync <- "0", self.source <- contactObj.source, self.contactFEId <- contactObj.contactFEId, self.isdeleted <- "0", self.ismerged <- "0", self.privacyEnabled <- "0", self.privacySetForContactUser <- privacySetForContactUser, self.mergeParentId <- contactObj.mergeParentId, self.picture <- contactObj.picture, self.secondaryPhoneNumbers <- contactObj.stringSecondaryPhones, self.secondaryEmails <- contactObj.stringSecondaryEmails, self.addressString1 <- contactObj.addressString1, self.addressString2 <- contactObj.addressString2, self.isBlocked <- "0", self.isBlockedByContact <- "0", self.sharedData <- contactObj.sharedData, self.createdAt <- contactObj.createdAt, self.updatedAt <- contactObj.updatedAt )
            do {
                  try self.db!.run(insertContact)
            } catch {
                
            }
      }
      /* End */

      /* inserting contacts in to SQLite ownerContacts table */
      func insertNewContactInSQLiteStorage(contactObj: ContactObj) -> Bool {
            var isBackendSync: String = ""
            var isdeleted: String = ""
            var ismerged: String = ""
            var privacyEnabled: String = ""
            var privacySetForContactUser: String = ""
            var isBlockedByContact: String = ""
            var isBlocked: String = ""
            var isOperationSuccessfull: Bool = true
            if(contactObj.isBackendSync == false) {
                  isBackendSync = "0"
            } else {
                  isBackendSync = "1"
            }
            if(contactObj.isBlocked == false) {
                  isBlocked = "0"
            } else {
                  isBlocked = "1"
            }
            if(contactObj.isdeleted == false ) {
                  isdeleted = "0"
            } else {
                  isdeleted = "1"
            }

            if(!contactObj.ismerged) {
                  ismerged = "0"
            } else {
                  ismerged = "1"
            }

            if(contactObj.privacyEnabled == false) {
                  privacyEnabled = "0"
            } else {
                  privacyEnabled = "1"
            }

            if(contactObj.privacySetForContactUser == false) {
                  privacySetForContactUser = "0"
            } else {
                  privacySetForContactUser = "1"
            }

            if(contactObj.isBlockedByContact == false) {
                  isBlockedByContact = "0"
            } else {
                  isBlockedByContact = "1"
            }
            let insertContact = self.ownerContacts.insert(self.userId <- contactObj.userId, self.contactId <- contactObj.contactId, self.name <- contactObj.name, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.companyEmail <- contactObj.companyEmail, self.title <- contactObj.title, self.company <- contactObj.company, self.website <- contactObj.website, self.notes <- contactObj.notes, self.isBackendSync <- isBackendSync, self.source <- contactObj.source, self.contactFEId <- contactObj.contactFEId, self.isdeleted <- isdeleted, self.ismerged <- ismerged, self.privacyEnabled <- privacyEnabled, self.mergeParentId <- contactObj.mergeParentId, self.picture <- contactObj.picture, self.secondaryPhoneNumbers <- contactObj.stringSecondaryPhones, self.secondaryEmails <- contactObj.stringSecondaryEmails, self.isBlocked <- isBlocked, self.addressString1 <- contactObj.addressString1, self.addressString2 <- contactObj.addressString2, self.isBlockedByContact <- isBlockedByContact, self.sharedData <- contactObj.sharedData, self.createdAt <- contactObj.createdAt, self.updatedAt <- contactObj.updatedAt, self.privacySetForContactUser <- privacySetForContactUser)
            
            do {
                  try self.db!.run(insertContact)
            } catch {
                  isOperationSuccessfull = false
            }

            return isOperationSuccessfull
      }
      /* End */

      /* inserting contacts in to SQLite ownerContacts table */
      func insertNewContactInRecentlyAdded(contactObj: ContactObj) -> Bool {
            var isOperationSuccessfull: Bool = true
            let insertContact = self.recentlyAdded.insert(self.userId <- contactObj.userId, self.contactId <- contactObj.contactId, self.name <- contactObj.name, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.companyEmail <- contactObj.companyEmail, self.title <- contactObj.title, self.company <- contactObj.company, self.website <- contactObj.website, self.notes <- contactObj.notes, self.isBackendSync <- "0", self.source <- contactObj.source, self.contactFEId <- contactObj.contactFEId, self.isdeleted <- "0", self.ismerged <- "0", self.privacyEnabled <- "0", self.mergeParentId <- contactObj.mergeParentId, self.picture <- contactObj.picture, self.secondaryPhoneNumbers <- contactObj.stringSecondaryPhones, self.secondaryEmails <- contactObj.stringSecondaryEmails, self.isBlocked <- "0", self.isBlockedByContact <- "0", self.sharedData <- contactObj.sharedData)
            do {
                  try self.db!.run(insertContact)
                  removeMoreThanTenRows()
            } catch {
                  isOperationSuccessfull = false
            }

            return isOperationSuccessfull
      }
      /* End */

      func removeMoreThanTenRows() {
            let contactsOfRecentlyAdded = readRecentlyAddedDatabase()
            if(contactsOfRecentlyAdded.count > 10) {
                  do {

                        let toDelete = recentlyAdded.filter(contactId == contactsOfRecentlyAdded[0].contactId)
                        try db!.run(toDelete.delete())
                  } catch {
                    
                  }
            }
      }

      func deleteRecentlyAddedSinglecontact(contact: ContactObj) {
            do {
                  let toDelete = self.recentlyAdded.filter(contactId == contact.contactId)
                  try db!.run(toDelete.delete())
            } catch {
            }
      }

      /* fetching already existing contacts from ownerContacts table of SQLite database using phone no & email id
       to update it with the contact that is fetched from the iphone*/

      func getAlreadyExistsContactsFromAllContacts(tempPhoneNumber: String, tempEmail: String) -> [ContactObj] {
            var contactsFetched: AnySequence<Row>?
            var existingContacts: [ContactObj] = []
            do {

                  if(!tempPhoneNumber.isEmpty && tempEmail.isEmpty) {
                        contactsFetched = try self.db?.prepare(self.ownerContacts.filter((self.phoneNumber == tempPhoneNumber)))
                  } else if(tempPhoneNumber.isEmpty && !tempEmail.isEmpty) {
                        contactsFetched = try self.db?.prepare(self.ownerContacts.filter(( self.email == tempEmail)))
                  } else if(!tempPhoneNumber.isEmpty && !tempEmail.isEmpty) {
                        contactsFetched = try self.db?.prepare(self.ownerContacts.filter(((self.email == tempEmail) || (self.phoneNumber == tempPhoneNumber))))
                  }

                  if(contactsFetched != nil) {
                        existingContacts = self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
                  }
            } catch {
            }
            return existingContacts
      }
      /* End */

      func getAlreadyExistsContactsFromInUseContacts(tempPhoneNumber: String, tempEmail: String) -> [ContactObj] {
            var contactsFetched: AnySequence<Row>?
            var existingContacts: [ContactObj] = []
            do {

                  if(!tempPhoneNumber.isEmpty && tempEmail.isEmpty) {
                        contactsFetched = try self.db?.prepare(self.ownerContacts.filter((self.phoneNumber == tempPhoneNumber) && (self.isdeleted == "0") && (self.ismerged == "0")))
                  } else if(tempPhoneNumber.isEmpty && !tempEmail.isEmpty) {
                        contactsFetched = try self.db?.prepare(self.ownerContacts.filter(( self.email == tempEmail) && (self.isdeleted == "0") && (self.ismerged == "0")))
                  } else if(!tempPhoneNumber.isEmpty && !tempEmail.isEmpty) {
                        contactsFetched = try self.db?.prepare(self.ownerContacts.filter(((self.email == tempEmail) || (self.phoneNumber == tempPhoneNumber)) && (self.isdeleted == "0") && (self.ismerged == "0")))
                  }

                  if(contactsFetched != nil) {
                        existingContacts = self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
                  }
            } catch {
            }
            return existingContacts
      }
      /* End */

      /* fetching non sync contacts - contacts which has isBackendSync = 0
       from ownerCOntacts table of SQLite table to update in the Dynamodb */
      func getNonSyncedContacts() -> [ContactObj] {

            var nonSyncedContacts: [ContactObj] = []
            do {

                  let contactsFetched = try self.db?.prepare(self.ownerContacts.filter (self.isBackendSync == "0"))
                  nonSyncedContacts = self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
            } catch {
            }
            return nonSyncedContacts
      }
      /* End */
    
    func createContactIfDoesntExists(hostId: String, hostName: String){
        let condition1 = (self.isdeleted == "0") && (self.ismerged == "0")
        let condition2 = (self.contactFEId != "") && (self.isBlockedByContact == "0") && (self.contactFEId == hostId)
        do {
            let contactsFetched = try self.db?.prepare(self.ownerContacts.filter(condition1 && condition2))
            let contacts = self.mapRecordsToContactStructureForListing(contactObjs: contactsFetched!)
            if contacts.count == 0 {
                var contactObj = ContactObj()
                var privacyEnabledForContacts: Bool = true
                let profileObj = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeUserObject") as?  [String: Any]
                if let tempObj = profileObj {
                    privacyEnabledForContacts = tempObj["privacyEnabled"] as? Bool ?? true
                }

                contactObj.userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as! String
                contactObj.name = hostName
                contactObj.phoneNumber = ""
                contactObj.stringSecondaryPhones = ""
                contactObj.stringSecondaryEmails = ""
                contactObj.contactId = getTime()
                contactObj.addressString1 = ""
                contactObj.addressString2 = ""
                contactObj.email = ""
                contactObj.isdeleted = false
                contactObj.ismerged = false
                contactObj.companyEmail = ""
                contactObj.title = ""
                contactObj.company = ""
                contactObj.source = "addedfromMeetInvitation"
                contactObj.website = ""
                contactObj.notes = ""
                contactObj.picture = ""
                contactObj.privacyEnabled = true
                contactObj.privacySetForContactUser = privacyEnabledForContacts
                contactObj.contactFEId = hostId
                contactObj.isBackendSync = false
                contactObj.mergeParentId = ""
                contactObj.isBlocked = false
                contactObj.isBlockedByContact = false
                contactObj.sharedData = 3
                contactObj.createdAt = getCurrentTImeStamp()
                contactObj.updatedAt = getCurrentTImeStamp()
                
                insertNewContactInSQLiteStorage(contactObj: contactObj)
                /* Backend Sync */
                if(NetworkStatus.sharedManager.isNetworkReachable()) {
                    self.appSharedPrefernce.setAppSharedPreferences(key: "isBackendSyncInProgress", value: "yes")
                    ContactsSync.shared.getNonSyncedContacts()
                    ContactsSync.shared.backendSync()
                }
            }
//            contactsToReturn = self.mapRecordsToContactStructureForListing(contactObjs: contactsFetched!)
        } catch {
        }
    }

      /* update contactFEId field & isBackend sync field of contact in ownerContacts table of SQLite database
       after backendSync & updateFEID operation completed */
      func updateCotact(syncResponse: [ContactObj]) {

            var isBackendSync: String = ""
            var privacyEnabled: String = ""
            var privacySetForContactUser: String = ""

            for contactObj in syncResponse {
                  if(contactObj.isBackendSync == false) {
                        isBackendSync = "0"
                  } else {
                        isBackendSync = "1"
                  }

                  if(contactObj.privacyEnabled == false) {
                        privacyEnabled = "0"
                  } else {
                        privacyEnabled = "1"
                  }

                  if(contactObj.privacySetForContactUser == false) {
                        privacySetForContactUser = "0"
                  } else {
                        privacySetForContactUser = "1"
                  }

                  let contactId: Int = Int(contactObj.contactId)
                  do {
                        let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                        try self.db!.run(contactToUpdate.update(self.name <- contactObj.name, self.contactFEId <- contactObj.contactFEId, self.isBackendSync <- isBackendSync, self.picture <- contactObj.picture, self.privacyEnabled <- privacyEnabled, self.sharedData <- contactObj.sharedData, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.privacySetForContactUser <- privacySetForContactUser))
                  } catch {
                  }
            }
      }
      /* End */

      /* update contactFEId field & isBackend sync field of contact in recentlyAdded table of SQLite database
       after backendSync & updateFEID operation completed */
      func updateCotactInRecentlyAdded(syncResponse: [ContactObj]) {

            var isBackendSync: String = ""
            var privacyEnabled: String = ""
            var privacySetForContactUser: String = ""

            var isBlocked: String = ""
             var isBlockedByContact: String = ""

            for contactObj in syncResponse {
                  if(contactObj.isBackendSync == false) {
                        isBackendSync = "0"
                  } else {
                        isBackendSync = "1"
                  }

                  if(contactObj.privacyEnabled == false) {
                        privacyEnabled = "0"
                  } else {
                        privacyEnabled = "1"
                  }

                  if(contactObj.privacySetForContactUser == false) {
                        privacySetForContactUser = "0"
                  } else {
                        privacySetForContactUser = "1"
                  }

                  if(contactObj.isBlocked == false) {
                        isBlocked = "0"
                  } else {
                        isBlocked = "1"
                  }

                  if(contactObj.isBlockedByContact == false) {
                        isBlockedByContact = "0"
                  } else {
                        isBlockedByContact = "1"
                  }

                  let contactId: Int = Int(contactObj.contactId)
                  do {
                        let contactToUpdate = self.recentlyAdded.filter(self.contactId == contactId)
                        try self.db!.run(contactToUpdate.update(self.name <- contactObj.name, self.contactFEId <- contactObj.contactFEId, self.isBackendSync <- isBackendSync, self.picture <- contactObj.picture, self.privacyEnabled <- privacyEnabled, self.sharedData <- contactObj.sharedData, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.isBlocked <- isBlocked, self.isBlockedByContact <- isBlockedByContact, self.privacySetForContactUser <- privacySetForContactUser))
                  } catch {
                    
                  }
            }
      }
      /* End */

      /* update contactFEId field & isBackend sync field of contact in ownerContacts table of SQLite database
       after backendSync & updateFEID operation completed */
      func updateContactWithBlockedDetails(syncResponse: [ContactObj]) {

            var privacyEnabled: String = ""
            var privacySetForContactUser: String = ""
            var isBlocked: String = ""
            var isBlockedByContact: String = ""

            for contactObj in syncResponse {

                  if(contactObj.privacyEnabled == false) {
                        privacyEnabled = "0"
                  } else {
                        privacyEnabled = "1"
                  }

                  if(contactObj.privacySetForContactUser == false) {
                        privacySetForContactUser = "0"
                  } else {
                        privacySetForContactUser = "1"
                  }

                  if(contactObj.isBlocked == false) {
                        isBlocked = "0"
                  } else {
                        isBlocked = "1"
                  }

                  if(contactObj.isBlockedByContact == false) {
                        isBlockedByContact = "0"
                  } else {
                        isBlockedByContact = "1"
                  }

                  let contactId: Int = Int(contactObj.contactId)
                  do {
                        let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                        try self.db!.run(contactToUpdate.update(self.name <- contactObj.name, self.contactFEId <- contactObj.contactFEId, self.isBackendSync <- "1", self.picture <- contactObj.picture, self.privacyEnabled <- privacyEnabled, self.sharedData <- contactObj.sharedData, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.isBlocked <- isBlocked, self.isBlockedByContact <- isBlockedByContact, self.privacySetForContactUser <- privacySetForContactUser))

                  } catch {
                  }
            }
      }
      /* End */

      /* Fetch blank contactFEId value contacts from ownerContacts table of SQLite Database to update contactFEId */
      func getBlankContactFEIdContacts() -> [ContactObj] {
            var blankContactFEIdContcts: [ContactObj] = []
            do {
                  let contactsFetched = try self.db?.prepare(self.ownerContacts.filter((self.isdeleted == "0") && (self.ismerged == "0") && (self.contactFEId == "")))
                  blankContactFEIdContcts = self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
            } catch {
            }
            return blankContactFEIdContcts
      }
      /* End */

      /* Get only fisheye contacts from ownerContacts table of SQLite database*/
      func getFisheyeContacts() -> [ContactObj] {

            let condition1 = (self.isdeleted == "0") && (self.ismerged == "0")
            let condition2 = (self.contactFEId != "") && (self.isBlockedByContact == "0")
            var contactsToReturn: [ContactObj] = []

            // Merge contacts those have same contactFEId
            do {
                  let duplicatedContacts = try self.db?.prepare(self.ownerContacts.filter(condition1 && condition2).group(Expression<Int?>("contactFEId"), having: self.contactFEId.count > 1))
                  for duplicatedContact in duplicatedContacts! {
                        var i = 0
                        let contactsByContactFEId = try self.db?.prepare(self.ownerContacts.filter(condition1 && condition2 && self.contactFEId == duplicatedContact[self.contactFEId]).order(self.name))
                        let contactsMappedBySameContactFEId = self.mapRecordsToContactStructure(contactObjs: contactsByContactFEId!)
                        if((i + 1) < contactsMappedBySameContactFEId.count) {
                              self.mergeContact(contactWillExist: contactsMappedBySameContactFEId[i], withContactObj: contactsMappedBySameContactFEId[i+1])
                        }
                        i = i + 1
                  }
            } catch {
            }
            // End Merging

            // Fetch fisheye conacts
            do {
                  let contactsFetched = try self.db?.prepare(self.ownerContacts.filter(condition1 && condition2).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructureForListing(contactObjs: contactsFetched!)
            } catch {
            }
            return contactsToReturn
      }
      /* End */

      /* Get only fisheye contacts from ownerContacts table of SQLite database*/
      func getWholeFisheyeContacts() -> [ContactObj] {

            let condition1 = (self.isdeleted == "0") && (self.ismerged == "0")
            let condition2 = (self.contactFEId != "") && (self.isBlockedByContact == "0")
            var contactsToReturn: [ContactObj] = []

            // Fetch fisheye conacts
            do {
                  let contactsFetched = try self.db?.prepare(self.ownerContacts.filter(condition1 && condition2).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
            } catch {
            }
            return contactsToReturn
      }
      /* End */

    /* Created by Keerthi removed by vaishali  */
    func mergeContact(contactWillExist: ContactObj?, withContactObj: ContactObj?) {
        var withContactObj = withContactObj
        var mergedContact = ContactsSync.shared.mergeContacts(contactExisting: contactWillExist!, contactObjWillMerge: withContactObj!)
        if mergedContact.phoneNumber != nil  && mergedContact.phoneNumber == ""{
            mergedContact.sharedData = 0
        } else if mergedContact.email != nil  && mergedContact.email == ""{
            mergedContact.sharedData = 1
        } else {
            mergedContact.sharedData = 2
        }
        if(withContactObj?.addressString1 != nil && withContactObj?.addressString1 != "") {
            mergedContact.addressString1 = withContactObj?.addressString1
        }

        if(withContactObj?.addressString2 != nil && withContactObj?.addressString2 != "") {
            mergedContact.addressString2 = withContactObj?.addressString2
        }

        if(withContactObj?.company != nil && withContactObj?.company != "") {
            mergedContact.company = withContactObj?.company
        }

        if(withContactObj?.companyEmail != nil && withContactObj?.companyEmail != "") {
            mergedContact.companyEmail = withContactObj?.companyEmail
        }

        if(withContactObj?.title != nil && withContactObj?.title != "") {
            mergedContact.title = withContactObj?.title
        }

        if(withContactObj?.website != nil && withContactObj?.website != "") {
            mergedContact.website = withContactObj?.website
        }

        if(withContactObj?.notes != nil && withContactObj?.notes != "") {
            mergedContact.notes = withContactObj?.notes
        }

        mergedContact.isBackendSync = false
        mergedContact.updatedAt = getCurrentTImeStamp()
        DatabaseManagement.shared.updatePersonalInfo(contactObj: mergedContact)
        DatabaseManagement.shared.updateAddress(contactObj: mergedContact)
        DatabaseManagement.shared.updateProfessionalDetails(contactObj: mergedContact)
        DatabaseManagement.shared.updateGeneralInfo(contactObj: mergedContact)

        withContactObj?.mergeParentId = String(mergedContact.contactId)
        withContactObj?.updatedAt = getCurrentTImeStamp()
        DatabaseManagement.shared.mergeContacts(contactObj: withContactObj!)
        DatabaseManagement.shared.deleteRecentlyAddedSinglecontact(contact: withContactObj!)
        if(NetworkStatus.sharedManager.isNetworkReachable()) {
            ContactsSync.shared.getNonSyncedContacts()
            ContactsSync.shared.backendSync()
        }

        /* Notification to refresh contact list on SyncViewController*/
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: REFRESH_CONTACTS_NOTIFICATION, object: nil)
        }

    }
    /* END */

      /* get only non fishseye contacts from ownerContacts of SQLite database */
      func getNonFisheyeContacts() -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            let condition1 = (self.isdeleted == "0") && (self.ismerged == "0")
            let condition2 = (self.contactFEId == "") && (self.isBlockedByContact == "0")
            do {
                  let filterCondition = self.ownerContacts.filter( condition1 && condition2)
                  let contactsFetched = try self.db?.prepare(filterCondition.order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructureForListing(contactObjs: contactsFetched!)

            } catch {
            }
            return contactsToReturn
      }
      /* End */

      /* search FE contacts by name, email id & phone no */
      func searchFEContacts(searchText: String?) -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            var searchterm: String = ""
            if let searchcondition  = searchText {
                  searchterm = searchcondition
            }

            searchterm = "%" + searchterm + "%"
            let searchQueryForName = self.name.like(searchterm)
            let searchQueryForPhone = self.phoneNumber.like(searchterm)
            let searchQueryForEmail = self.email.like(searchterm)
            let searchQuery = (searchQueryForName || searchQueryForPhone || searchQueryForEmail)
            let condition1 = (self.contactFEId != "") && (self.isdeleted == "0")  && (self.isdeleted == "0")
            let condition2 = (self.ismerged == "0") && (self.isBlockedByContact == "0")
            do {
                  let searchContacts = try db!.prepare(self.ownerContacts.filter( searchQuery && condition1 && condition2).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: searchContacts)

            } catch {
            }

            return contactsToReturn
      }
      /* End */

      /* search non contacts by name, email id & phone no */
      func searchNonFEContacts(searchText: String?) -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            var searchterm: String = ""
            if let searchcondition  = searchText {
                  searchterm = searchcondition
            }

            searchterm = "%" + searchterm + "%"
            let searchQueryForName = self.name.like(searchterm)
            let searchQueryForPhone = self.phoneNumber.like(searchterm)
            let searchQueryForEmail = self.email.like(searchterm)
            let searchQuery = (searchQueryForName || searchQueryForPhone || searchQueryForEmail)
            let condition1 = (self.contactFEId == "") && (self.isdeleted == "0")
            let condition2 = (self.ismerged == "0") && (self.isBlockedByContact == "0")
            do {
                  let searchContacts = try db!.prepare(self.ownerContacts.filter( searchQuery && condition1 && condition2).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: searchContacts)
            } catch {
                  self.appSharedPrefernce.setAppSharedPreferences(key: "messageToShowViaToast", value: error)
            }

            return contactsToReturn
      }
      /* End */

      /* search non contacts by name, email id & phone no */
      func searchRecentlyAddedContacts(searchText: String?) -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            var searchterm: String = ""
            if let searchcondition  = searchText {
                  searchterm = searchcondition
            }

            searchterm = "%" + searchterm + "%"
            let searchQueryForName = self.name.like(searchterm)
            let searchQueryForPhone = self.phoneNumber.like(searchterm)
            let searchQueryForEmail = self.email.like(searchterm)
            let searchQuery = (searchQueryForName || searchQueryForPhone || searchQueryForEmail)
            let condition1 = (self.isdeleted == "0") && (self.ismerged == "0") && (self.isBlockedByContact == "0")
            do {
                  let searchContacts = try db!.prepare(self.recentlyAdded.filter( searchQuery && condition1).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: searchContacts)
            } catch {
                  self.appSharedPrefernce.setAppSharedPreferences(key: "messageToShowViaToast", value: error)
            }

            return contactsToReturn
      }

      /* delete all records from ownerContacts table of SQLite table - use when FE user will  log out */
      func deleteAllContactsFromSQLite() {
            do {
                  try db!.run(ownerContacts.delete())
            } catch {
                
            }
      }
      /* End */

      /* delete all records from recentlyAdded table of SQLite table - use when FE user will  log out */
      func deleteAllRecentlyAddedContactsFromSQLite() {
            do {
                  try db!.run(recentlyAdded.delete())
            } catch {
            }
      }
      /* End */

      /* updating already existing contacts while frontendSync operation */
      func updateAlreadyExistingContact(contacToUpdate: ContactObj) {
            let contactObj = ownerContacts.filter(self.contactId == contacToUpdate.contactId)
            do {
                  try db!.run(contactObj.update( self.name <- contacToUpdate.name,
                                                 self.phoneNumber <- contacToUpdate.phoneNumber,
                                                 self.email <- contacToUpdate.email,
                                                 self.secondaryPhoneNumbers <- contacToUpdate.stringSecondaryPhones, self.secondaryEmails <- contacToUpdate.stringSecondaryEmails, self.isBackendSync <- "0", self.sharedData <-  contacToUpdate.sharedData, self.updatedAt <- contacToUpdate.updatedAt, self.picture <-  contacToUpdate.picture
                  ))
            } catch {
            }
      }
      /* End */

      /* Description :- delete selected contact from SQLite Database */
      func deleteSelectedContact(contactId: Int) -> Bool {
            var isDeleted = false
            let contactObj = ownerContacts.filter(self.contactId == contactId)
            do {
                  var currentTimeStamp = getCurrentTImeStamp()
                  if currentTimeStamp == nil {
                        currentTimeStamp = "error"
                  }
                  try db!.run(contactObj.update( self.isdeleted <- "1", self.isBackendSync <- "0", self.updatedAt <- currentTimeStamp))
                  isDeleted = true
            } catch {
            }
            return isDeleted
      }
      /* End */

      func updatePersonalInfo(contactObj: ContactObj) -> Bool {
            var isOperationSuccessfull: Bool = true
            let contactId: Int = Int(contactObj.contactId)
            var isBackendSync: String = ""
            var privacySetForContact: String = ""

            if contactObj.privacySetForContactUser {
                  privacySetForContact = "1"
            } else {
                  privacySetForContact = "0"
            }

            if(contactObj.isBackendSync) {
                  isBackendSync = "1"
            } else {
                  isBackendSync = "0"
            }
        
            do {
                  let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                  try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.name <- contactObj.name, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.secondaryPhoneNumbers <- contactObj.stringSecondaryPhones, self.secondaryEmails <- contactObj.stringSecondaryEmails, self.sharedData <- contactObj.sharedData, self.createdAt <- contactObj.createdAt, self.updatedAt <- contactObj.updatedAt, self.privacySetForContactUser <- privacySetForContact))
            } catch {
                  isOperationSuccessfull = false
            }
            return isOperationSuccessfull
      }

      func updatePersonalInfoInRecentlyAdded(contactObj: ContactObj) -> Bool {
            var isOperationSuccessfull: Bool = true
            let contactId: Int = Int(contactObj.contactId)
            var isBackendSync: String = ""

            if(contactObj.isBackendSync == false) {
                  isBackendSync = "0"
            } else {
                  isBackendSync = "1"
            }

            do {
                  let contactToUpdate = self.recentlyAdded.filter(self.contactId == contactId)
                  try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.name <- contactObj.name, self.phoneNumber <- contactObj.phoneNumber, self.email <- contactObj.email, self.secondaryPhoneNumbers <- contactObj.stringSecondaryPhones, self.secondaryEmails <- contactObj.stringSecondaryEmails, self.sharedData <- contactObj.sharedData, self.createdAt <- contactObj.createdAt, self.updatedAt <- contactObj.updatedAt))
            } catch {
                  isOperationSuccessfull = false
            }
            return isOperationSuccessfull
      }

      func updateProfessionalDetails(contactObj: ContactObj) -> Bool {
            var isOperationSuccessfull: Bool = true
            let contactId: Int = Int(contactObj.contactId)
            var isBackendSync: String = ""

            if(contactObj.isBackendSync == false) {
                  isBackendSync = "0"
            } else {
                  isBackendSync = "1"
            }

            do {
                  if contactObj.updatedAt != nil {
                        let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.company <- contactObj.company, self.companyEmail <- contactObj.companyEmail, self.title <- contactObj.title, self.updatedAt <- contactObj.updatedAt))
                  } else {
                        let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.company <- contactObj.company, self.companyEmail <- contactObj.companyEmail, self.title <- contactObj.title))
                  }

            } catch {
                  isOperationSuccessfull = false
            }

            return isOperationSuccessfull
      }

      func updateGeneralInfo(contactObj: ContactObj) -> Bool {
            var isOperationSuccessfull: Bool = true
            let contactId: Int = Int(contactObj.contactId)
            var isBackendSync: String = ""

            if(contactObj.isBackendSync == false) {
                  isBackendSync = "0"
            } else {
                  isBackendSync = "1"
            }

            do {
                  let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                  if contactObj.updatedAt != nil {
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.website <- contactObj.website, self.notes <- contactObj.notes, self.updatedAt <- contactObj.updatedAt))
                  } else {
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.website <- contactObj.website, self.notes <- contactObj.notes))
                  }
            } catch {
                  isOperationSuccessfull = false
            }

            return isOperationSuccessfull
      }

      func updateAddress(contactObj: ContactObj) -> Bool {
            var isOperationSuccessfull: Bool = true
            let contactId: Int = Int(contactObj.contactId)
            var isBackendSync: String = ""

            if(contactObj.isBackendSync == false) {
                  isBackendSync = "0"
            } else {
                  isBackendSync = "1"
            }

            do {
                  let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                  if(contactObj.addressString1 != nil) {
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.addressString1 <- contactObj.addressString1, self.updatedAt <- contactObj.updatedAt))
                  } else {
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- isBackendSync, self.addressString2 <- contactObj.addressString2, self.updatedAt <- contactObj.updatedAt))
                  }
            } catch {
                  isOperationSuccessfull = false
            }
            return isOperationSuccessfull
      }

      func deleteFirstAddress(contactObj: ContactObj) -> Bool {
            var isOperationSuccessfull: Bool = true
            let contactId: Int = Int(contactObj.contactId)
            do {
                  let contactToUpdate = self.ownerContacts.filter(self.contactId == contactId)
                  let noOfAddressToDelete: Int = appSharedPrefernce.getAppSharedPreferences(key: "addressToDelete") as! Int
                  if(noOfAddressToDelete == 0) {
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- "0", self.addressString1 <- contactToUpdate[self.addressString2], self.addressString2 <- ""))
                  } else if(noOfAddressToDelete == 1) {
                        try self.db!.run(contactToUpdate.update(self.isBackendSync <- "0", self.addressString2 <- ""))
                  }
            } catch {
                  isOperationSuccessfull = false
            }
            return isOperationSuccessfull
      }

      func getContact(contactId: Int) -> ContactObj {
            var contactToOpen = [ContactObj()]

            do {
                  let contactFetched = try db!.prepare(self.ownerContacts.filter(self.contactId == contactId))
                  contactToOpen = mapRecordsToContactStructure(contactObjs: contactFetched)
            } catch {
            }

            if(contactToOpen.count > 0) {
                  return contactToOpen.first!
            } else {
                  let contactToReturn = ContactObj()
                  return contactToReturn
            }
      }

      func blockOrUnblockContact(contactObj: ContactObj) {
            var isBlocked = ""

            if(contactObj.isBlocked == false) {
                  isBlocked = "0"
            } else {
                  isBlocked = "1"
            }

            do {
                  let contactToUpdate = self.ownerContacts.filter(self.contactId == contactObj.contactId)

                  try self.db!.run(contactToUpdate.update(self.isBackendSync <- "0"))

                  try self.db!.run(contactToUpdate.update(self.isBlocked <- isBlocked, self.updatedAt <- contactObj.updatedAt))

            } catch {
            }
      }

      func mergeContacts(contactObj: ContactObj) -> Bool {

            var isOperationSuccessfull: Bool = true
            do {
                  let contactToUpdate = self.ownerContacts.filter(self.contactId == contactObj.contactId)
                  try self.db!.run(contactToUpdate.update(self.isBackendSync <- "0", self.mergeParentId <- contactObj.mergeParentId, self.ismerged <- "1", self.updatedAt <- getCurrentTImeStamp()))
            } catch {
                  isOperationSuccessfull = false
            }

            return isOperationSuccessfull
      }
    
    func getCountOfFEContactsWhoesPrivacyIsSameAsInProfile(privacyEnabled: Bool) -> Int{
        
        var privacySetForContactUser = "1"
        if privacyEnabled{
            privacySetForContactUser = "1"
        }else{
            privacySetForContactUser = "0"
        }

        var countOfFEContactsWhoesPrivacyIsSameAsInProfile: Int = 0
        let condition1 = (self.isdeleted == "0") && (self.ismerged == "0")
        let condition2 = (self.contactFEId != "") && (self.isBlockedByContact == "0")
        
        //        let contactsFetched = try self.db?.prepare(self.ownerContacts.filter(condition1 && condition2).order(self.name))
        do{
            countOfFEContactsWhoesPrivacyIsSameAsInProfile = try self.db?.scalar(self.ownerContacts.filter(condition1 && condition2 && self.privacySetForContactUser == privacySetForContactUser).count) ?? 0
        }catch{
        }
        
        return countOfFEContactsWhoesPrivacyIsSameAsInProfile
    }

      func getRecentlyAddedContacts() -> [ContactObj] {
            var contactsToReturn: [ContactObj] = []
            do {

                  let contactsFetched = try self.db?.prepare(self.recentlyAdded)
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
            } catch {
            }
            return contactsToReturn
      }

      /* Get only fisheye contacts from ownerContacts table of SQLite database which are  neither blocked by logged in user nor contact user has blocked to logged in user  */
      func getUnblockedFEContacts() -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            do {
//                  let condition1 = (self.isdeleted == "0")
                
                let condition1 = (self.isdeleted == "0") && (self.ismerged == "0") && (self.isBlocked == "0")
                let condition2 = (self.contactFEId != "") && (self.isBlockedByContact == "0")
//
                  let contactsFetched = try self.db?.prepare(self.ownerContacts.filter(condition1 && condition2).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: contactsFetched!)
            } catch {
            }
            return contactsToReturn
      }
      /* End */

      /* get only non fishseye contacts from ownerContacts of SQLite database which are  neither blocked by logged in user nor contact user has blocked to logged in user */
      func getUnblockedNonFEContacts() -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            let condition1 = (self.isdeleted == "0") && (self.ismerged == "0") && (self.isBlocked == "0")
            let condition2 = (self.contactFEId == "") && (self.isBlockedByContact == "0")
            do {
                  let filterCondition = self.ownerContacts.filter( condition1 && condition2)
                  let contactsFetched = try self.db?.prepare(filterCondition.order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructureForListing(contactObjs: contactsFetched!)

            } catch {
            }
            return contactsToReturn
      }
      /* End */

      /* search FE contacts by name, email id & phone no which are  neither blocked by logged in user nor contact user has blocked to logged in user*/
      func searchUnblockedFEContacts(searchText: String?) -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            var searchterm: String = ""
            if let searchcondition  = searchText {
                  searchterm = searchcondition
            }

            searchterm = "%" + searchterm + "%"
            let searchQueryForName = self.name.like(searchterm)
            let searchQueryForPhone = self.phoneNumber.like(searchterm)
            let searchQueryForEmail = self.email.like(searchterm)
            let searchQuery = (searchQueryForName || searchQueryForPhone || searchQueryForEmail)
            let condition1 = (self.contactFEId != "") && (self.isdeleted == "0")  && (self.isBlocked == "0")
            let condition2 = (self.ismerged == "0") && (self.isBlockedByContact == "0")
            do {
                  let searchContacts = try db!.prepare(self.ownerContacts.filter( searchQuery && condition1 && condition2).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: searchContacts)

            } catch {
            }

            return contactsToReturn
      }
      /* End */

      /* search non contacts by name, email id & phone no which are  neither blocked by logged in user nor contact user has blocked to logged in user*/
      func searchUnblockedNonFEContacts(searchText: String?) -> [ContactObj] {

            var contactsToReturn: [ContactObj] = []
            var searchterm: String = ""
            if let searchcondition  = searchText {
                  searchterm = searchcondition
            }

            searchterm = "%" + searchterm + "%"
            let searchQueryForName = self.name.like(searchterm)
            let searchQueryForPhone = self.phoneNumber.like(searchterm)
            let searchQueryForEmail = self.email.like(searchterm)
            let searchQuery = (searchQueryForName || searchQueryForPhone || searchQueryForEmail)
            let condition1 = (self.contactFEId == "") && (self.isdeleted == "0") && (self.isBlocked == "0")
            let condition2 = (self.ismerged == "0") && (self.isBlockedByContact == "0")
            do {
                  let searchContacts = try db!.prepare(self.ownerContacts.filter( searchQuery && condition1 && condition2).order(self.name))
                  contactsToReturn = self.mapRecordsToContactStructure(contactObjs: searchContacts)
            } catch {
                  self.appSharedPrefernce.setAppSharedPreferences(key: "messageToShowViaToast", value: error)
            }

            return contactsToReturn
      }
      /* End */

      /* Description :- Mapping records that are fetched from SQlite's ownerContacts table to structure ContactObj */
      func mapRecordsToContactStructure(contactObjs: AnySequence<Row>) -> [ContactObj] {

            var responseContacts: [ContactObj] = []
            for contactObj in contactObjs {
                  var singleContactObj = ContactObj()
                  let idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

                  if(idtoken == "") {
                        break
                  }

                  if let name = contactObj[self.name] {
                        singleContactObj.name = name
                  }
                  if let phoneNumber = contactObj[self.phoneNumber] {
                        singleContactObj.phoneNumber = phoneNumber
                  }

                  singleContactObj.contactId = contactObj[self.contactId]

                  if let email =  contactObj[self.email] {
                        singleContactObj.email = email
                  }

                  if let company = contactObj[self.company] {
                        singleContactObj.company = company
                  }

                  if let companyEmail = contactObj[self.companyEmail] {
                        singleContactObj.companyEmail = companyEmail
                  }

                  if let title  = contactObj[self.title] {
                        singleContactObj.title = title
                  }

                  if let website = contactObj[self.website] {
                        singleContactObj.website = website
                  }

                  if let notes = contactObj[self.notes] {
                        singleContactObj.notes = notes
                  }

                  if let source = contactObj[self.source] {
                        singleContactObj.source = source
                  }

                  if let contactFEId = contactObj[self.contactFEId] {
                        singleContactObj.contactFEId = contactFEId
                  }

                  if let mergeParentId = contactObj[self.mergeParentId] {
                        singleContactObj.mergeParentId = mergeParentId
                  }
                  if(singleContactObj.mergeParentId == nil) {
                        singleContactObj.mergeParentId = ""
                  }

                  if let picture = contactObj[self.picture] {
                        singleContactObj.picture = picture
                  }

                  if(contactObj[self.isBackendSync] == "0") {
                        singleContactObj.isBackendSync = false
                  } else {
                        singleContactObj.isBackendSync = true
                  }

                  singleContactObj.userId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId")  as! String

                  if let picture = contactObj[self.picture] {
                        singleContactObj.picture = picture
                  }

                  if(contactObj[self.isdeleted] == "0") {
                        singleContactObj.isdeleted = false
                  } else {
                        singleContactObj.isdeleted = true
                  }

                  if(contactObj[self.ismerged] == "0") {
                        singleContactObj.ismerged = false
                  } else {
                        singleContactObj.ismerged = true
                  }

                  if(contactObj[self.privacyEnabled] == "0") {
                        singleContactObj.privacyEnabled = false
                  } else {
                        singleContactObj.privacyEnabled = true
                  }

                  if(contactObj[self.privacySetForContactUser] == "0") {
                        singleContactObj.privacySetForContactUser = false
                  } else {
                        singleContactObj.privacySetForContactUser = true
                  }

                  if(contactObj[self.isBlocked] == "0") {
                        singleContactObj.isBlocked = false
                  } else {
                        singleContactObj.isBlocked = true
                  }

                  if(contactObj[self.isBlockedByContact] == "0") {
                        singleContactObj.isBlockedByContact = false
                  } else {
                        singleContactObj.isBlockedByContact = true
                  }

                  if let sharedData = contactObj[self.sharedData] {
                        singleContactObj.sharedData = sharedData
                  }

                  if let createdAt = contactObj[self.createdAt] {
                        singleContactObj.createdAt = createdAt
                  } else {
                        singleContactObj.createdAt = getCurrentTImeStamp()
                  }

                  if let updatedAt = contactObj[self.updatedAt] {
                        singleContactObj.updatedAt = updatedAt
                  } else {
                        singleContactObj.updatedAt = getCurrentTImeStamp()
                  }

                  if((contactObj[self.secondaryPhoneNumbers]) != nil && (contactObj[self.secondaryPhoneNumbers] != "")) {
                        singleContactObj.secondaryPhoneNumbers = (contactObj[self.secondaryPhoneNumbers]?.components(separatedBy: ","))!
                  }

                  if((contactObj[self.secondaryEmails]) != nil && (contactObj[self.secondaryEmails] != "")) {
                        singleContactObj.secondaryEmails = (contactObj[self.secondaryEmails]?.components(separatedBy: ","))!
                  }

                  if(contactObj[self.addressString1] != nil && contactObj[self.addressString1] != "") {
                        singleContactObj.addressStringArray1 = (contactObj[self.addressString1]?.components(separatedBy: ","))!
                  }

                  if(contactObj[self.addressString2] != nil  && contactObj[self.addressString2] != "") {
                        singleContactObj.addressStringArray2 = (contactObj[self.addressString2]?.components(separatedBy: ","))!
                  }

                  responseContacts.append(singleContactObj)
            }
            return responseContacts
      }
      /* End */

      /* Description :- Mapping records that are fetched from SQlite's ownerContacts table to structure ContactObj */
      func mapRecordsToContactStructureForListing(contactObjs: AnySequence<Row>) -> [ContactObj] {

            var responseContacts: [ContactObj] = []
            //        DispatchQueue.global().async{
            for contactObj in contactObjs {
                  var singleContactObj = ContactObj()
                  let idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

                  if(idtoken == "") {
                        break
                  }

                  if let name = contactObj[self.name] {
                        singleContactObj.name = name
                  }
                  if let phoneNumber = contactObj[self.phoneNumber] {
                        singleContactObj.phoneNumber = phoneNumber
                  }

                  singleContactObj.contactId = contactObj[self.contactId]

                  if let email =  contactObj[self.email] {
                        singleContactObj.email = email
                  }

                  if let contactFEId = contactObj[self.contactFEId] {
                        singleContactObj.contactFEId = contactFEId
                  }

                  if(contactObj[self.isBlocked] == "0") {
                        singleContactObj.isBlocked = false
                  } else {
                        singleContactObj.isBlocked = true
                  }

                  if let picture = contactObj[self.picture] {
                        singleContactObj.picture = picture
                  }

                  if let sharedData = contactObj[self.sharedData] {
                        singleContactObj.sharedData = sharedData
                  }

                  if(contactObj[self.privacyEnabled] == "0") {
                        singleContactObj.privacyEnabled = false
                  } else {
                        singleContactObj.privacyEnabled = true
                  }

                  if(contactObj[self.privacySetForContactUser] == "0") {
                        singleContactObj.privacySetForContactUser = false
                  } else {
                        singleContactObj.privacySetForContactUser = true
                  }

                  responseContacts.append(singleContactObj)
            }

            return responseContacts
      }

}
