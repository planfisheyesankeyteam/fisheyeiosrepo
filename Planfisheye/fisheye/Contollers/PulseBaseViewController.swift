//
//  PulseBaseViewController.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 03/09/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class PulseBaseViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var pulsedummyview: UIView!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!

    var isshare: Bool = false

    @IBAction func requestBtnClick(_ sender: Any) {
        isshare = false
        requestBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")

        shareBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
    }
    @IBOutlet weak var pulseShareView: UIView!

    var selectedShareOption: Bool = false

    @IBOutlet weak var searchpersonTextfield: DesignableUITextField!
    @IBAction func shareBtnClick(_ sender: UIButton) {
        isshare = true

        shareBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")

        requestBtn.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")

    }

    @IBAction func searchpersonTextClick(_ sender: UIButton) {

    }
    @IBAction func backclick(_ sender: Any) {

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

        vc.unHideBottomviews()
        vc.didTapOnLogo()

    }

//    @IBAction func pulseRequestClick(_ sender: Any) {
//        
//        selectedShareOption = false
//        pulseshare.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
//        pulserequest.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")
//        
//        
// 
//
//    }

    @IBAction func switchValueDidChange(sender: DGRunkeeperSwitch!) {
        print("valueChanged: \(sender.selectedIndex)")
    }
//    @IBAction func pulseShareClick(_ sender: Any) {
//        
//        selectedShareOption = true
//         pulserequest.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
//        pulseshare.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")
//    }
    @IBOutlet weak var pulserequest: UIButton!

    @IBOutlet weak var pulseoutsidestackview: UIView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var pulseshare: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        applyViewShadow()
        searchpersonTextfield.delegate = self

        searchpersonTextfield.addTarget(self, action: #selector(respondsToTf), for: .touchDown)

        // Do any additional setup after loading the view.
    }

    func respondsToTf() {
        let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
        let profileBaseviewController = storyboard.instantiateViewController(withIdentifier: "PulseUserSearchViewController") as! PulseUserSearchViewController
        profileBaseviewController.isshare = self.isshare
        profileBaseviewController.view.frame = self.view.bounds
        self.addChildViewController(profileBaseviewController)
        profileBaseviewController.didMove(toParentViewController: self)

        //  profileBaseviewController.delegate = self

        profileBaseviewController.view.frame.origin.y = -self.view.frame.size.height

        self.view.addSubview(profileBaseviewController.view)

        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: { _ in
            profileBaseviewController.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
        //
     //   print("you can pop-up the data picker here")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func applyViewShadow() {

        let vc = kApplicationDelegate.nVC.viewControllers[0] as! BaseViewController

        vc.hideBottomviews()

        self.mainview.layer.cornerRadius = 8

        // border
        self.mainview.layer.borderWidth = 0
        self.mainview.layer.borderColor = UIColor.black.cgColor
        //        self.mainview.clipsToBounds = true

        // shadow
        self.mainview.layer.shadowColor = UIColor.black.cgColor
        self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.mainview.layer.shadowOpacity = 0.7
        self.mainview.layer.shadowRadius = 10.0

        self.pulsedummyview.layer.cornerRadius = 17.0
        // shadow
      //  self.mainview.layer.shadowColor = UIColor.black.cgColor
        //self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
        //self.mainview.layer.shadowOpacity = 0.7
        //self.mainview.layer.shadowRadius = 10.0

       // self.pulseoutsidestackview.layer.cornerRadius = 50.0

        // border
        self.pulseoutsidestackview.layer.borderWidth = 0
        self.pulseoutsidestackview.layer.borderColor = UIColor.black.cgColor
        //        self.mainview.clipsToBounds = true

        // shadow
        self.pulseoutsidestackview.layer.shadowColor = UIColor.black.cgColor
        self.pulseoutsidestackview.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.pulseoutsidestackview.layer.shadowOpacity = 0.5
        self.pulseoutsidestackview.layer.shadowRadius = 17.0

       // pulseoutsidestackview.layer.cornerRadius = 13.0

      //  pulserequest.layer.cornerRadius = 20.0

        let runkeeperSwitch = DGRunkeeperSwitch(titles: ["Feed", "Leaderboard"])
        runkeeperSwitch.backgroundColor = UIColor(red: 229.0/255.0, green: 163.0/255.0, blue: 48.0/255.0, alpha: 1.0)
        runkeeperSwitch.selectedBackgroundColor = .white
        runkeeperSwitch.titleColor = .white
        runkeeperSwitch.selectedTitleColor = UIColor(red: 255.0/255.0, green: 196.0/255.0, blue: 92.0/255.0, alpha: 1.0)
        runkeeperSwitch.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 13.0)
        runkeeperSwitch.frame = CGRect(x: 30.0, y: 40.0, width: 200.0, height: 30.0)
//        runkeeperSwitch.addTarget(self, action: #selector(PulseBaseViewController.switchValueDidChange(sender:)), for: .valueChanged)
        //navigationItem.titleView = runkeeperSwitch
       // self.pulseoutsidestackview.addSubview(runkeeperSwitch)

        let runkeeperSwitch2 = DGRunkeeperSwitch()
        runkeeperSwitch2.titles = ["Share", "Request"]
        runkeeperSwitch2.backgroundColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")

            //UIColor(red: 239.0/255.0, green: 95.0/255.0, blue: 49.0/255.0, alpha: 1.0)
        runkeeperSwitch2.selectedBackgroundColor = .white
        runkeeperSwitch2.titleColor = .white
        runkeeperSwitch2.selectedTitleColor = UIColor.hexStringToUIColor(hex: "#263D93")

            //UIColor(red: 239.0/255.0, green: 95.0/255.0, blue: 49.0/255.0, alpha: 1.0)
                runkeeperSwitch2.addTarget(self, action: #selector(PulseBaseViewController.switchValueDidChange(sender:)), for: .valueChanged)
        runkeeperSwitch2.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 13.0)
        runkeeperSwitch2.frame = CGRect(x: 50.0, y: 20.0, width: pulseoutsidestackview.bounds.width - 80.0, height: 45.0)
        runkeeperSwitch2.setSelectedIndex(1, animated: true)
        runkeeperSwitch2.autoresizingMask = [.flexibleWidth]
      //  self.pulseoutsidestackview.addSubview(runkeeperSwitch2)
//
//        let path = UIBezierPath(roundedRect: pulseshare.bounds, byRoundingCorners: [.bottomLeft,.topLeft], cornerRadii: CGSize(width: 13, height: 13))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        pulseshare.layer.mask = mask
//////
//////        
//        let requestpath = UIBezierPath(roundedRect: pulserequest.bounds, byRoundingCorners: [.topRight,.bottomRight], cornerRadii: CGSize(width: 15, height: 15))
//        let requestmask = CAShapeLayer()
//        requestmask.path = requestpath.cgPath
//        pulserequest.layer.mask = requestmask
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
