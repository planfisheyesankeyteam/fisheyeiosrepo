//
//  AMZNAuthorizeUserDelegate.swift
//  fisheye
//
//  Created by Sankey Solution on 11/29/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
import LoginWithAmazon

class AMZNAuthorizeUserDelegate: NSObject, AIAuthenticationDelegate {

    func requestDidSucceed(_ apiResult: APIResult) {
        //let delegate = AMZNGetProfileDelegate(parentController: self)
        AIMobileLib.getProfile(self)
    }

    func requestDidFail(_ errorResponse: APIError) {
        let message: String = errorResponse.error.message
        // Your code when the authorization fails.
        UIAlertView(title: "", message: "User authorization failed with message: \(errorResponse.error.message)", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "").show()
    }

}
