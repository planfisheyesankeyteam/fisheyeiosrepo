//  LoginViewController.swift
//  Planfisheye
//  Created by Keerthi Chinivar on 22/06/17.
//Modified by Hitesh Patil Finalize date: 10/12/2017
//Copyright © 2017 fisheye. All rights reserved.

import UIKit

import GoogleSignIn
import FacebookLogin
import FacebookCore
import Toast_Swift
import Crashlytics
import DropDown
// Added the SDK to implement the Amazon Login
import LoginWithAmazon
// End of import to implement the SDK for Amazon Login
var dat = 0

class LoginViewController : UIViewController, AIAuthenticationDelegate, GIDSignInUIDelegate, UIGestureRecognizerDelegate, GIDSignInDelegate {
    
    var iconClick: Bool! = true
    var showOTPScreen: Bool = false
    @IBOutlet weak var internationalization: UITextField!
    var showSendOTPScreen: Bool = false
    var phoneNumber: String!
    var fisheyeId = ""
    var language = ""
    var countryCode: String!
    let languages = ["English", "Spanish", "Marathi", "Hindi", "Bengali", "French", "Arabic", "German", "Italian", "Dutch", "Japenese", "Russia", "Korean"]
    // Declaring appservice variable
    let appService = AppService.shared
    let appSharedPrefernce = AppSharedPreference()
    let validationController = ValidationController()
    let internationalizationDropdown = DropDown()
    var emailiddd = ""
    var loginid = ""
    var loginPwd = ""
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    var gradientLayer: CAGradientLayer!
    var testStatuscode = ""
    
    
    @IBOutlet weak var showPasswordBtn: UIButton!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var itsAboutYouLabel: UILabel!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var forgotPassBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var fisheyeLbl: UILabel!
    //    @IBOutlet weak var fisheyeLbl: UILabel!
    @IBOutlet weak var fisheyeLoginPreferenceSelection: UIImageView!
    @IBOutlet weak var facebookLoginPreferenceSelection: UIImageView!
    @IBOutlet weak var googleLoginPreferenceSelection: UIImageView!
    @IBOutlet weak var amazonLoginPreferenceSelection: UIImageView!
    @IBOutlet weak var internationalizeButton: UIButton!
    @IBOutlet weak var fisheyeMotto: UILabel!
    
    //Functionality added by Hitesh Patil
    // Added the Amazon Login Button
    
    @IBOutlet weak var amazonLoginButton: UIButton!
    
    // End of declaration of Amazon Button
    @IBOutlet weak var userNameTextField: UICustomTextField!
    @IBOutlet weak var passwordTextField: UICustomTextField!
    @IBOutlet weak var visiblePassImage: UIImageView!
    //    @IBOutlet var radialView: RadialGradientView!
    
    @IBOutlet weak var userFieldUnderLine: RadialGradientViewForUnderLines!
    
    @IBOutlet weak var passFieldUnderLine: RadialGradientViewForUnderLines!
    //    @IBOutlet weak var userFieldUnderLine: RadialGradientLayerForUnderLines!
    //    @IBOutlet weak var passFieldUnderLine: RadialGradientLayerForUnderLines!
    @IBOutlet weak var googleButton: GIDSignInButton!
    var idtoken = ""
    var mobileNumber = ""
    var countryCod = ""
    let lwa = LoginWithAmazonProxy.sharedInstance
    
    var amazonLoginName: String = ""
    var amazonLoginEmail: String = ""
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        ScreenLoader.shared.stopLoader()
    }
    
    @IBAction func showPasswordBtnClick(_ sender: Any) {
        if(iconClick == true) {
            self.passwordTextField.isSecureTextEntry = false
            iconClick = false
        } else {
            self.passwordTextField.isSecureTextEntry = true
            iconClick = true
        }
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
        
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            self.dismiss(animated: true, completion: nil)
            if GIDSignIn.sharedInstance().hasAuthInKeychain() {
                let gmailId = signIn.currentUser.profile.email ?? ""
                let name = signIn.currentUser.profile.name ?? ""
                GIDSignIn.sharedInstance().signOut()
                let pwd = "#FE#GOOGLE#"
                self.loginid = gmailId
                self.loginPwd = "#FE#GOOGLE#"
                self.appSharedPrefernce.setAppSharedPreferences(key: "loginMethod", value: "google")
                self.logincheckcontrolcalled(method: "google", loginid: gmailId, pwd: pwd)
                self.appSharedPrefernce.setAppSharedPreferences(key: "socialemail", value: gmailId)
                self.appSharedPrefernce.setAppSharedPreferences(key: "socialname", value: name)
            }
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is GIDSignInButton {
            return true
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func dismissOnTap() {
        self.view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.view.bounds
        
        gradientLayer.colors = [UIColor.contactsTypeBg(), UIColor.white]
        
        self.view.layer.addSublayer(gradientLayer)
    }
    
    @IBAction func amazonLoginButtonTapped(_ sender: Any) {
        
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                self.showToast(message: self.profilemsg.networkfailureMsg)
                return
        }
        LoginWithAmazonProxy.sharedInstance.login(delegate: self)
    }
    
    func requestDidSucceed(_ apiResult: APIResult) {
        if (apiResult.api == API.authorizeUser) {
            AIMobileLib.getAccessToken(forScopes: Settings.Credentials.SCOPES, withOverrideParams: nil, delegate: self)
        } else if(apiResult.api == API.getProfile) {
            let name = ((apiResult.result as? [AnyHashable: Any])?["name"]) as? String
            let email = ((apiResult.result as? [AnyHashable: Any])?["email"]) as? String
            self.amazonLoginEmail = email!
            self.amazonLoginName = name!
            self.appSharedPrefernce.setAppSharedPreferences(key: "socialemail", value: self.amazonLoginEmail)
            self.appSharedPrefernce.setAppSharedPreferences(key: "socialname", value: self.amazonLoginName)
            self.loginid = self.amazonLoginEmail
            self.loginPwd = "#FE#AMAZON#"
            self.appSharedPrefernce.setAppSharedPreferences(key: "loginMethod", value: "amazon")
            let pwd = "#FE#AMAZON#"
            self.logincheckcontrolcalled(method: "amazon", loginid: self.amazonLoginEmail, pwd: pwd)
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "login", action: "Amazon login", label: "Amazon verified successfully", value: 0)
        } else {
            let someInts: [Int] = [10]
            for index in someInts {
                let dat = AIMobileLib.getProfile(self)
            }
            
            //            let result = (apiResult.result as? [AnyHashable: Any])
            //            let name = ((apiResult.result as? [AnyHashable: Any])?["name"]) as? String
            //            let email = ((apiResult.result as? [AnyHashable: Any])?["email"]) as? String
            //            let user_id = ((apiResult.result as? [AnyHashable: Any])?["user_id"]) as? String
        }
    }
    
    func requestDidFail(_ errorResponse: APIError) {
      
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                self.showToast(message: self.profilemsg.networkfailureMsg)
                return
        }
        
        self.loginid = userNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        self.loginPwd = passwordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        self.appSharedPrefernce.setAppSharedPreferences(key: "loginMethod", value: "fisheye")
        if validationCheck() {
            logincheckcontrolcalled(method: "fisheye", loginid: loginid, pwd: self.loginPwd)
        }
    }
    
    @IBAction func fbLoginTapped(_ sender: UIButton) {
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                self.showToast(message: self.profilemsg.networkfailureMsg)
                return
        }
        let loginManager = LoginManager()
        
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) {
            result in
            
            switch result {
            case .failed(let error):
                print(error.localizedDescription)
            case .cancelled:
                print("cancelled")
            case .success(let _, _, let _):
                
                self.getFbInfo()
            }
        }
    }
    
    func getFbInfo() {
        struct MyProfileRequest: GraphRequestProtocol {
            struct Response: GraphResponseProtocol {
                
                var name: String?
                var id: String?
                var gender: String?
                var email: String?
                var profilePictureUrl: String?
                
                init(rawResponse: Any?) {
                    // Decode JSON from rawResponse into other properties here.
                    guard let response = rawResponse as? Dictionary<String, Any> else {
                        return
                    }
                    
                    if let name = response["name"] as? String {
                        self.name = name
                    }
                    
                    if let id = response["id"] as? String {
                        self.id = id
                    }
                    
                    if let email = response["email"] as? String {
                        self.email = email
                    }
                }
            }
            
            var graphPath = "/me"
            var parameters: [String: Any]? = ["fields": "id, email, name"]
            var accessToken = AccessToken.current
            var httpMethod: GraphRequestHTTPMethod = .GET
            var apiVersion: GraphAPIVersion = .defaultVersion
        }
        
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                if(response.email == nil) {
                    self.view.makeToast(self.signupmsg.noEmailForSocialAccount, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
                    return
                }
                self.appSharedPrefernce.setAppSharedPreferences(key: "socialemail", value: response.email!)
                self.appSharedPrefernce.setAppSharedPreferences(key: "socialname", value: response.name!)
                self.logincheckcontrolcalled(method: "facebook", loginid: response.email!, pwd: "#FE#FACEBOOK#")
                self.loginid = response.email!
                self.loginPwd = "#FE#FACEBOOK#"
                self.appSharedPrefernce.setAppSharedPreferences(key: "loginMethod", value: "facebook")
                self.appSharedPrefernce.setAppSharedPreferences(key: "loginMethod", value: "facebook")
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "login", action: "Facebook login", label: "Facebook verification successful - Get profile information from facebook successful", value: 0)
            case .failed(let _):
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "login", action: "Facebook login", label: "Failed to get profile information from facebook", value: 0)
            }
        }
        connection.start()
    }
    
    func getPreviousLocalization() {
        let preferredLanguage = self.appSharedPrefernce.getAppSharedPreferences(key: "preferredLanguage") as? String ?? "English"
        
        self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: preferredLanguage)
        self.internationalization.text = preferredLanguage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        //GIDSignIn.sharedInstance().clientID = "361866842537-2e6pjfmvlqe9973419c02mdqsm1rc910.apps.googleusercontent.com"
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        googleButton.style = GIDSignInButtonStyle.iconOnly
        self.fisheyeLbl.text="Fisheye Hub"
        self.getPreviousLocalization()
        self.changeLanguage()
        
       
        
        self.bgImageView.alpha = 0
        self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: "fisheye")
        
        _ = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        //        self.radialView.isUserInteractionEnabled = true
        //        self.radialView.addGestureRecognizer(viewTapGesture)
        
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showPassword))
        //        self.visiblePassImage.isUserInteractionEnabled = true
        //        self.visiblePassImage.addGestureRecognizer(tapGesture)
        
        self.loginBtn.layer.cornerRadius = 0.07304347826*self.loginBtn.bounds.width
        self.loginBtn.setTitleColor(UIColor.white, for: .normal)
        
        //        self.signUpBtn.setTitleColor(UIColor.lightURple(), for: .normal)
        
        //        self.forgotPassBtn.setTitleColor(UIColor.lightURple(), for: .normal)
        
        self.orLabel.textColor = UIColor.purpleRing()
        
        self.userNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        let loginPreference = self.appSharedPrefernce.getAppSharedPreferences(key: "loginPreferenceUsed") as? String ?? ""
        
        if loginPreference == "" || loginPreference == nil {
            self.amazonLoginPreferenceSelection.isHidden = true
            self.facebookLoginPreferenceSelection.isHidden = true
            self.googleLoginPreferenceSelection.isHidden = true
            self.fisheyeLoginPreferenceSelection.isHidden = true
        } else if loginPreference == "google" {
            self.amazonLoginPreferenceSelection.isHidden = true
            self.facebookLoginPreferenceSelection.isHidden = true
            self.googleLoginPreferenceSelection.isHidden = false
            self.fisheyeLoginPreferenceSelection.isHidden = true
        } else if loginPreference == "facebook" {
            self.amazonLoginPreferenceSelection.isHidden = true
            self.facebookLoginPreferenceSelection.isHidden = false
            self.googleLoginPreferenceSelection.isHidden = true
            self.fisheyeLoginPreferenceSelection.isHidden = true
        } else if loginPreference == "amazon" {
            self.amazonLoginPreferenceSelection.isHidden = false
            self.facebookLoginPreferenceSelection.isHidden = true
            self.googleLoginPreferenceSelection.isHidden = true
            self.fisheyeLoginPreferenceSelection.isHidden = true
        } else if loginPreference == "fisheye" {
            self.amazonLoginPreferenceSelection.isHidden = true
            self.facebookLoginPreferenceSelection.isHidden = true
            self.googleLoginPreferenceSelection.isHidden = true
            self.fisheyeLoginPreferenceSelection.isHidden = false
        }
        
        self.userNameTextField.placeholderColor = UIColor.rebrandedLightPurple()
        self.passwordTextField.placeholderColor = UIColor.rebrandedLightPurple()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        
        GIDSignIn.sharedInstance().delegate = self
        dismissOnTap()
        
        self.setLocalizationText()
        self.addObservers()
        
    }
    
    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */
    
    /*  Set all text labels according to language selected by FEUser --*/
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.internationalizationDropdown.dataSource = AppService.shared.languages
            self.signInLabel.text = self.signupmsg.signIn
            self.fisheyeLbl.text = "Fisheye Hub"
            self.userNameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.loginusernmae)
            self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
            self.fisheyeMotto.text = self.appService.letsWorkTogether
            self.orLabel.text = self.signupmsg.orLbl
            self.signUpBtn.setTitle(self.signupmsg.newUserSignUp, for: .normal)
            self.forgotPassBtn.setTitle(self.signupmsg.forgotPassword, for: .normal)
        }
    }
    /* END */
    
    override func viewWillAppear(_ animated: Bool) {
        ScreenLoader.shared.stopLoader()
        kApplicationDelegate.setStatusBarBackgroundColor(color: UIColor.clear)
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.LoginViewController)
    }
    
    @objc func textFieldDidChange(_ textField: UICustomTextField) {
        switch textField {
            
        case userNameTextField:
            
            if (userNameTextField.text?.characters.count)! > 0 {
                self.userNameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.loginusernmae)
            }
            
        case passwordTextField:
            
            if (passwordTextField.text?.characters.count)! > 0 {
                self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
            }
            
        default:
            break
        }
    }
    
    func changeLanguage()
    {
        internationalizationDropdown.anchorView = internationalizeButton
        internationalizationDropdown.dataSource = AppService.shared.languages
        internationalizationDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.getTranslatedText(updatelanguage: item)
            self.internationalization.text = item
            self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: item)
            self.internationalizationDropdown.hide()
            
        }
    }
    
    func startLoader() {
        DispatchQueue.main.async {
            self.userNameTextField.isUserInteractionEnabled=false
            self.passwordTextField.isUserInteractionEnabled=false
            self.loginBtn.isUserInteractionEnabled=false
            //            self.radialView.isUserInteractionEnabled = false
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            self.userNameTextField.isUserInteractionEnabled=true
            self.passwordTextField.isUserInteractionEnabled=true
            self.loginBtn.isUserInteractionEnabled=true
            //            self.radialView.isUserInteractionEnabled = true
            ScreenLoader.shared.stopLoader()
        }
        
    }
    
    func rotateView(targetView: UIView, duration: Double = 1.0) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))
        }) { finished in
            self.rotateView(targetView: targetView, duration: duration)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func decodePayload(tokenstr: String) -> NSDictionary {
        //Value that has to be returned
        var dictonary: NSDictionary?
        //splitting JWT to extract payload
        let arr: [String] = tokenstr.components(separatedBy: ".")
        //base64 encoded string i want to decode
        var base64Str = arr[1] as String
        if base64Str.characters.count % 4 != 0 {
            let padlen = 4 - base64Str.characters.count % 4
            base64Str += String(repeating: "=", count: padlen)
        }
        
        if let data = Data(base64Encoded: base64Str, options: []),
            let googleDataString = String(data: data, encoding: String.Encoding.utf8) {
            if let data = googleDataString.data(using: String.Encoding.utf8) {
                do {
                    dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] as! NSDictionary
                } catch let error as NSError {
                   
                }
            }
        }
        return dictonary!
    }
    
    func loadBaseViewController(data: [String: Any]) {
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime) {
            //            let introductaryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "dashboardIntroducataryPages") as? String ?? "0"
            
            //            if (introductaryVariableIndex == "0") {
            //                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //                let nextVC = storyBoard.instantiateViewController(withIdentifier: "DashBoardPageViewController") as! DashBoardPageViewController
            //                self.present(nextVC, animated: true, completion: nil)
            //            } else {
            let baseVC  = BaseViewController.instantiateFromStoryboardWithIdentifier(identifier: "BaseViewController")
            kApplicationDelegate.nVC = UINavigationController(rootViewController: baseVC)
            kApplicationDelegate.nVC.navigationBar.isHidden  = true
            kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC
            //            }
            
//        }
      saveFisheyeUserObject(data: data)
      self.appSharedPrefernce.setAppSharedPreferences(key: "loginId", value: self.loginid)

    }
    
    
    
    func loadSignUpWithSocialLogin() {
        self.appSharedPrefernce.setAppSharedPreferences(key: "loginPwd", value: self.loginPwd)
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpWithSocialLogin") as! SignUpWithSocialLogin
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func loadSignUPOTPVC() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func goToTermsNConditions() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func loadSecurityControllerVC(masterKey: String, signUpMethod: String) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpSecurityQuestionVC") as! SignUpSecurityQuestionVC
        nextVC.masterKey = masterKey
        nextVC.reEnterMasterKey = masterKey
        nextVC.signUpMethod = signUpMethod
        self.present(nextVC, animated: true, completion: nil)
        
    }
    
    func loadSecurityControllerVCByHidingSignInDetails() {
        DispatchQueue.main.async{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpSecurityQuestionVC") as! SignUpSecurityQuestionVC
            nextVC.isCameAfterRegistration = true
            nextVC.isFromLogin = true
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func redirectToDashboard(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
        self.present(nextVC, animated: true, completion: nil)
    }
    func loadSignUpMasterKey(signUpMethod: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpMasterKeyVC") as! SignUpMasterKeyVC
        nextVC.signUpMethod = signUpMethod
        self.present(nextVC, animated: true, completion: nil)
    }
    
    //    func showPassword() {
    //
    //    }
    
    @objc func endEditing() {
        self.view.endEditing(true)
    }
    
    func validationCheck() -> Bool {
        var returnValue = false
        let isEmailAddressValid = emailValidation()
        if(isEmailAddressValid) {
            let isPasswordValid = validationController.isValidPassword(testStr: passwordTextField.text!)
            if(isPasswordValid) {
                returnValue = true
                
            } else {
                passwordTextField.configOnError(withPlaceHolder: self.signupmsg.invalidpassword)
                returnValue = false
            }
        } else {
            userNameTextField.configOnError(withPlaceHolder: self.signupmsg.invalidplaceholder)
            let isPasswordValid = validationController.isValidPassword(testStr: passwordTextField.text!)
            if(!isPasswordValid) {
                passwordTextField.configOnError(withPlaceHolder: self.signupmsg.invalidpassword)
            }
            returnValue = false
        }
        return returnValue
    }
    
    func emailValidation() -> Bool {
        var returnValue = true
        let loginid = userNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if !validationController.isValidEmail(testStr: loginid) {
            returnValue = false
        }
        return  returnValue
    }
    
    func logUser() {
        Crashlytics.sharedInstance().setUserEmail("ext1dev@planfisheye.com")
        Crashlytics.sharedInstance().setUserName("Crash Tester")
        Crashlytics.setUserName("CfabricTester")
    }
    
    func passwordValidation() -> Bool {
        var returnValue = true
        let password = passwordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let passwordRegex = appService.passwordRegex
        
        do {
            let regex = try NSRegularExpression(pattern: passwordRegex)
            let nsString = password as NSString
            let results = regex.matches(in: password, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                returnValue = false
            }
        } catch let _ as NSError {
            returnValue = false
        }
        return  returnValue
    }
    
    func logincheckcontrolcalled(method: String, loginid: String, pwd: String) {
        DispatchQueue.main.async{
             self.startLoader()
        }
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                self.showToast(message: self.profilemsg.networkfailureMsg)
                return
        }
       
        let meth = method
        
        let parameters =
            [
                "email": loginid,
                "password": pwd
                ] as [String: Any]
        
        let awsURL = AppConfig.loginAPI
    
        guard let URL = URL(string: awsURL) else {
            
            return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
//                self.stopLoader()
                self.idtoken = data["idtoken"] as? String ?? ""
                self.testStatuscode = data["statusCode"] as? String ?? ""
                
                if(data["statusCode"] as? String ?? "" == "0") {
                    
                    if let user = data["user"] as? [String: Any] {
                        let signUpMethodology = user["signupMethod"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "signUpBasicInfo", value: data["user"] as? [String: Any])
                        self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: signUpMethodology)
                        self.appSharedPrefernce.setAppSharedPreferences(key: "idtoken", value: self.idtoken)
                        self.mobileNumber = user["phonenumber"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.mobileNumber)
                        self.emailiddd = user["email"] as? String ?? ""
                        self.countryCod = user["countryCode"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "email", value: self.emailiddd)
                        self.appSharedPrefernce.setAppSharedPreferences(key: "countrycode", value: self.countryCod)
                        self.fisheyeId = user["id"] as? String ?? ""
                        self.saveFcmToken()
                        let previousFEId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "loginPwd", value: self.loginPwd)
                        if(previousFEId != self.fisheyeId) {
                            DatabaseManagement.shared.deleteAllContactsFromSQLite()
                            DatabaseManagement.shared.deleteAllRecentlyAddedContactsFromSQLite()
                            self.appSharedPrefernce.removeAppSharedPreferences(key: "isFetchFromBackendCompleted")
                            self.appSharedPrefernce.removeAppSharedPreferences(key: "isfronendSyncFullyCompleted")
                        }
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeId", value: self.fisheyeId)
                        let fisheyeName = user["name"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeName", value: fisheyeName) as? String ?? ""
                        var fisheyePhoto = ""
                        if( user["picture"] != nil) {
                            fisheyePhoto = user["picture"] as? String ?? ""
                        }
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyePhoto", value: fisheyePhoto)
                        let genderSelection = user["gender"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "gender", value: genderSelection)
                        
                        Crashlytics.sharedInstance().setUserEmail(self.emailiddd)
                        Crashlytics.sharedInstance().setUserName(fisheyeName)
                        Crashlytics.sharedInstance().setUserIdentifier(self.fisheyeId)
                        let phoneNumberVerified1 = user["phonenumber_verified"] as? Bool
                        let masterKeyExists1 = user["masterKey_exist"] as? Bool
                        let securityQuestionsExist1=user["securityQuesAnses_exist"] as? Bool
                        let termsNCondition1=user["isTermsNConditionsAccepted"] as? Bool
                        
                        if (securityQuestionsExist1 == true && termsNCondition1 == false){
                            self.termNCRedirect()
                            self.stopLoader()
                        } else if(phoneNumberVerified1 == true && termsNCondition1 == true && securityQuestionsExist1 == false){
                            self.showToast(message: self.signupmsg.updateSecurityAnswerToRecoverAccount)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                self.loadSecurityControllerVCByHidingSignInDetails()
                                self.stopLoader()
                            })
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Compulsory to submit security question answers-Redirected to Sequrity ansewers page", value: 0)
                        } else if(phoneNumberVerified1 == true && termsNCondition1 == true) {
                            //                            if(termsNCondition1)! {
                            self.showToast(message: self.signupmsg.loginsuccess)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.appService.secondTimeInterval, execute: {
                                    self.loadBaseViewController(data: data)
                                    self.stopLoader()
                              })
                              
                            self.startContactFrontEndSync()
                            self.appSharedPrefernce.setAppSharedPreferences(key: "loginId", value: loginid)
                            self.appSharedPrefernce.setAppSharedPreferences(key: "loginPwd", value: pwd)

                            if(NetworkStatus.sharedManager.isNetworkReachable()) {
                                self.appSharedPrefernce.setAppSharedPreferences(key: "isBackendSyncInProgress", value: "yes")
                            }
                            
                            LookoutSharedInstances.shared.isLookoutInterestRegistered = user["isLookoutFeedbackRegistered"] as? Bool
                            
                            
                            //                            self.deleteUserLocations()
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login successful", value: 0)
                        }
                        else if(masterKeyExists1)! {
                            //Code to load security questions page
                            self.showToast(message: self.signupmsg.registrationincomplete)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                
                                self.loadSecurityControllerVC(masterKey: user["masterKey"]as? String ?? "", signUpMethod: signUpMethodology)
                                self.stopLoader()
                            })
                            
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "SignUp incomplete -  master key exists - Redirection to enter Security questions screen", value: 0)
                        } else if(phoneNumberVerified1 == true) {
                            //Code to load master key page
                            self.showToast(message: self.signupmsg.registrationincomplete)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                if((signUpMethodology == "google") || (signUpMethodology == "facebook") || (signUpMethodology == "amazon"))
                                {
                                     self.goToTermsNConditions()
                                }
                                else{
                                    self.loadSecurityControllerVC(masterKey: user["masterKey"]as? String ?? "", signUpMethod: signUpMethodology)
                                }
                                
                                self.stopLoader()
                            })
                            
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "SignUp incomplete -  master key exists - Redirection to enter Security questions screen", value: 0)
                        } else {
                            self.showToast(message: self.signupmsg.registrationincomplete)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                if signUpMethodology != "fisheye"{
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: signUpMethodology)
                                    self.loadSignUpWithSocialLogin()
                                } else {
                                    self.sendOtpToRegisteredNumber()
                                }
                                self.stopLoader()
                            })
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "SignUp incomplete - Terms & Conditions accepted - Send OTP to registered number", value: 0)
                        }
                        self.getTrackingOptions()
                    }
                } else if(data["statusCode"] as? String ?? ""  == "1") {
                    self.stopLoader()
                    self.showToast(message: self.signupmsg.credintialsinvalid)
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Invalid credentials", value: 0)
                } else if(data["statusCode"] as? String ?? "" == "2") {
                    self.stopLoader()
                    
                    if(meth == "google" || meth == "facebook" || meth == "amazon" ) {
                        if(meth == "google" ) {
                            self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: "google")
                        } else if(meth == "facebook") {
                            self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: "facebook")
                        } else if(meth == "amazon" ) {
                            self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: "amazon")
                        }
                        self.loadSignUpWithSocialLogin()
                        
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "SignUp incomplete - Redirected to social sign up page", value: 0)
                    } else {
                        self.showToast(message: self.signupmsg.usernotfoundcred)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login failed - Invalid credentials", value: 0)
                    }
                } else if (data["statusCode"] as? String ?? "" == "4") {
                    self.stopLoader()
                    let tempUser = data["user"] as? [String: Any]
                    if(tempUser != nil) {
                       
                        if(meth == "google" || meth == "facebook" || meth == "amazon" ) {
                            if(meth == "google" ) {
                                self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: "google")
                            } else if(meth == "facebook") {
                                self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: "facebook")
                            } else if(meth == "amazon" ) {
                                self.appSharedPrefernce.setAppSharedPreferences(key: "signUpMethod", value: "amazon")
                            }
                        }
                        self.loadSignUpWithSocialLogin()
                    } else {
                        self.showToast(message: self.signupmsg.pleaseloginwithsocial)
                    }
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login failed - Invalid credentials", value: 0)
                } else {
                    self.stopLoader()
                    self.showToast(message: self.signupmsg.enterValidCredentials)
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login failed - Invalid credentials", value: 0)
                }
//                DispatchQueue.main.async{
//                    self.stopLoader()
//                }
                
            } else {
//                self.stopLoader()
                self.showToast(message: self.profilemsg.networkfailureMsg)
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login failed", value: 0)
                DispatchQueue.main.async{
                    self.stopLoader()
                }
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
        
    }
    func saveFcmToken( ) {
        let fcmToken =  self.appSharedPrefernce.getAppSharedPreferences(key: "fcmToken")as? String ?? ""
        let saveFcmTokenApiCall = SaveFCMToken(fcmTokenRecieved: fcmToken)
        saveFcmTokenApiCall.addDidFinishBlockObserver { [unowned self] (operation, _) in
            let responseStatus = operation.responseStatus
            if responseStatus == "200"{
               
            }
        }
        AppDelegate.addProcedure(operation: saveFcmTokenApiCall)
    }
    
    func sendOtpToRegisteredNumber() {
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                self.showToast(message: self.profilemsg.networkfailureMsg)
                return
        }
        let parameters =
            [
                
                "action": "sendotp",
                "idtoken": idtoken,
                "phonenumber": mobileNumber,
                "countryCode": countryCod,
                "email": emailiddd
                ] as [String: Any]
        
        let awsURL = AppConfig.sendOTPAPI
        
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                if(data["statusCode"] as? String ?? "" == "200") {
                    self.loadSignUPOTPVC()
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Phone number not verified - OTP sent successfully", value: 0)
                } else {
                    self.showToast(message: self.signupmsg.smssendingfailed)
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Phone number not verified - Failed to sent OTP", value: 0)
                }
                
            } else {
                self.showToast(message: self.signupmsg.smssendingfailed)
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login failed", value: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
        
    }
    
    
    func getTrackingOptions() {
        let graphqlUrl = AppConfig.getMeetmeConfigurationUrl
        guard let URL = URL(string: graphqlUrl) else { return }
        let sessionConfig = URLSessionConfiguration.default
        
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        self.idtoken  = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        request.httpMethod = AppConfig.httpMethodGet
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpBody = "".data(using: String.Encoding.utf8)
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                if let meetmeConfigObj = json["meetmeConfig"] as? [String: Any] {
                    if let configContentObj = meetmeConfigObj["configContent"] as? [String: Any] {
                        
//                        if let meetmeTrackingOptions = configContentObj["trackingOptions"] as? [[String: Any]] {
//                            MeetMeTrackingOptions.meetTrackingOptions = []
//                            for trackOption in meetmeTrackingOptions {
//                                let trackObj = MeetMeTrackingOptionsList()
//                                trackObj.trackingOption = trackOption["optionName"]  as? String ??  ""
//                                trackObj.trackingTime = trackOption["trackingTime"]  as? NSArray
//                                trackObj.isTimeRequired = trackOption["isTimeRequired"] as? Bool
//                                MeetMeTrackingOptions.meetTrackingOptions.append(trackObj)
//                            }
//                        }
                        MeetMeTrackingOptions.meetmeParticipantLimit = configContentObj["numberOfParticipants"] as? Int
                        MeetMeTrackingOptions.mapResetTimeInterval = configContentObj["mapResetTimeInterval"] as? Double
                        MeetMeTrackingOptions.meetingTrackableTime = configContentObj["meetingTrackableTime"] as? Double
                        
                        AppService.shared.firstTimeInterval = configContentObj["firstTimeInterval"] as? Double ?? 1.0
                        AppService.shared.noOfCarouselCards = configContentObj["noOfCarouselCards"] as? Int ?? 4
                        AppService.shared.noOfTabsToShowInTopRibbon = configContentObj["noOfTabsToShowInTopRibbon"] as? Int ?? 3
                        AppService.shared.secondTimeInterval = configContentObj["secondTimeInterval"] as? Double ?? 2.0
                        AppService.shared.meetmeSearchContactTimeInterval = configContentObj["meetmeSearchContactTimeInterval"] as? Double ?? 1.5
                        ProfileToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix = configContentObj["profileToastDelatimebysix"] as? Double ?? 6.0
                        ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBelesserThan = configContentObj["minMobNumLength"] as? Int ?? 3
                        ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBeGreaterThan = configContentObj["maxMobNumLength"] as? Int ?? 14
                        
                        ProfileToastMsgHeadingSubheadingLabels.shared.emailRegX = configContentObj["emailAjax"] as? String ??  ""
                        ProfileToastMsgHeadingSubheadingLabels.shared.passwordRegX = configContentObj["passwordAjax"] as? String ??  ""
                        
                        ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime = configContentObj["profileToastDelayTime"] as? Double ?? 4.0
                        SignupToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix = configContentObj["signupToastdelatimebysix"] as? Double ?? 6.0
                        SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium = configContentObj["signupToastDelayMedium"] as? Double ?? 5.0
                        SignupToastMsgHeadingSubheadingLabels.shared.toastDelayTime = configContentObj["signupToastDelayTime"] as? Double ?? 4.0
                        SyncToastMessages.shared.syncToastMaxTimeInterval = configContentObj["syncToastMaxTimeInterval"] as? Double ?? 300.0
                        SyncToastMessages.shared.syncToastThirdTimeInterval = configContentObj["syncToastThirdTimeInterval"] as? Double ?? 180.0
                        PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse = configContentObj["toastDelayTimeForPulse"] as? Double ?? 3.0
                        
                        AppService.shared.profileImageBorderColor = configContentObj["profileImageBorderColor"] as? String ?? ""
                        AppService.shared.carouselCardColor = configContentObj["carouselCardColor"] as? String ?? ""
                        
                        if let addressTypes = configContentObj["addressTypes"] as? [String] {
                            AppService.shared.addressTypes = []
                            for addressType in addressTypes {
                                AppService.shared.addressTypes.append(addressType)
                            }
                        }
                        
                        
                        
                        if let languagesArray = configContentObj["languages"] as? [String] {
                            AppService.shared.languages = []
                            for language in languagesArray {
                                AppService.shared.languages.append(language)
                            }
                        }
                    }
                }
            } else {
                
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        let nextResponder =  self.view?.viewWithTag(nextTag)
        
        if let responder = nextResponder {
            responder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpMyProfileVC") as! SignUpMyProfileVC
        self.present(nextVC, animated: true, completion: nil)
        
    }
    
    func termNCRedirect() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
        DispatchQueue.main.async {
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func forgotPassTapped(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "ForgetPasswordScreen1VC") as! ForgetPasswordScreen1VC
        self.present(nextVC, animated: true, completion: nil)
        
    }
    
    @IBAction func onInternationalizeButtonTapped(_ sender: Any) {
        if  internationalizationDropdown.isHidden {
            self.internationalizationDropdown.show()
        } else {
            self.internationalizationDropdown.hide()
        }
    }
    
    // Show toast on view
    //Author: Vaishali
    func showToast(message: String) {
        DispatchQueue.main.async {
            self.view.makeToast(message, duration: ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime, position: .bottom)
        }
    }
    // end of function showing toast on screen
    
    func startContactFrontEndSync() {
        let frontendSyncGlobalOperation = DispatchWorkItem {
            DatabaseManagement.shared.createRecentlyAddedTable()
            
            /* Check SQLite table is created or not? */
            let isSQliteTableExists: Bool = DatabaseManagement.shared.isSQLiteTableAreadyExists()
            if(!isSQliteTableExists) {                                                                                                                                                                               // SQLite table does't exists
                DatabaseManagement.shared.createOwenerContactsTable()                                                                                      // create table
                self.getFEUserContactsFromBackend()
            } else {                                                                                                                                                                                                                // SQLite table  exists
                let totalRecordsInTable: Int64 = DatabaseManagement.shared.checkIsOwnerContactsTableEmpty()
                if(totalRecordsInTable == 0) {                                                                                                                                                               // SQLite table is empty
                    self.getFEUserContactsFromBackend()
                } else {                                                                                                                                                                                                        // SQLite table is not empty
                    self.getBlockedByContactsFromBackend()
                    let isFetchFromBackendCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isFetchFromBackendCompleted") as? String ?? "yes"
                    let isfronendSyncFullyCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isfronendSyncFullyCompleted") as? String ?? "yes"
                    if(isFetchFromBackendCompleted == "no") {
                        self.getFEUserContactsFromBackend()
                    } else if(isfronendSyncFullyCompleted == "no") {
                        ContactsSync.shared.frontEndSync()
                    } else {
                        if(NetworkStatus.sharedManager.isNetworkReachable()) {
                            ContactsSync.shared.getNonSyncedContacts()
                            if(!ContactsSync.shared.globalNonSyncedContcts.isEmpty ) {
                                ContactsSync.shared.backendSync()
                            }
                        }
                    }
                }
            }
        }
        DispatchQueue.global().async(execute: frontendSyncGlobalOperation)
    }
    
    func getFEUserContactsFromBackend() {
        self.appSharedPrefernce.setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "yes")
        let getContactsFromBackend = getLoggedInUserContactsOperation()
        getContactsFromBackend.addDidFinishBlockObserver { [unowned self] (operation, _) in
            DispatchQueue.main.async {
                ContactsSync.shared.isSyncInProgress = false
                let contactsCount = operation.contactsCount
               
                if(contactsCount > 0) {
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: false)
                    AppSharedPreference().setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "no")
                } else {
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: true)
                }
            }
        }
        AppDelegate.addProcedure(operation: getContactsFromBackend)
    }
    
    func getBlockedByContactsFromBackend() {
        let getBlockedByContacts = getBlockedByContactsAPIOperation()
        getBlockedByContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getBlockedByContacts)
    }
    
    // Description - Get translated text from backend
    // Author - Vaishali
    func getTranslatedText(updatelanguage: String) {
        self.startLoader()
        let getTranslation = GetTranslatedText(language: updatelanguage)
        getTranslation.addDidFinishBlockObserver { [unowned self] (_, _) in
            self.stopLoader()
            NotificationCenter.default.post(name: CHANGE_LANGUAGE, object: nil)
        }
        AppDelegate.addProcedure(operation: getTranslation)
    }
    // End
    
    // Description - Get translated text from backend
    // Author - Vaishali
    func deleteUserLocations() {
        
        let getTranslation = DeleteUsersPerviousLocations()
        getTranslation.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getTranslation)
    }
    // End
}

//class RadialGradientLayer: CALayer {
//
//    var center: CGPoint {
//
//        return CGPoint(x: bounds.width/2, y: bounds.height/2)
//
//    }
//
//    var radius: CGFloat {
//
//        return (bounds.width + bounds.height)/2
//
//    }
//
//    var colors: [UIColor] = [UIColor.contactsTypeBg(), UIColor.white] {
//
//        didSet {
//
//            setNeedsDisplay()
//
//        }
//
//    }
//
//    var cgColors: [CGColor] {
//        return colors.map({ (color) -> CGColor in
//            return color.cgColor
//        })
//    }
//
//    override init() {
//        super.init()
//        needsDisplayOnBoundsChange = true
//    }
//
//    required init(coder aDecoder: NSCoder) {
//        super.init()
//    }
//
//    override func draw(in ctx: CGContext) {
//        ctx.saveGState()
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//        let locations: [CGFloat] = [0.0, 0.5]
//        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
//            return
//        }
//        ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
//    }
//
//}

class RadialGradientLayerForUnderLines: CALayer {
    
    var center: CGPoint {
        return CGPoint(x: bounds.width/2, y: bounds.height/2)
        
    }
    
    var radius: CGFloat {
        return (bounds.width + bounds.height)/2
    }
    
    var colors: [UIColor] = [UIColor.purpleRing(), UIColor.contactsTypeBg()] {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var cgColors: [CGColor] {
        return colors.map({ (color) -> CGColor in
            return color.cgColor
        })
    }
    
    override init() {
        super.init()
        needsDisplayOnBoundsChange = true
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
    }
    
    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations: [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
            return
        }
        ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
    }
    
}

//class RadialGradientView: UIView {
//
//      private let gradientLayer = RadialGradientLayer()
//
//    public var colors: [UIColor] {
//        get {
//            return gradientLayer.colors
//        }
//        set {
//            gradientLayer.colors = newValue
//        }
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        if gradientLayer.superlayer == nil {
//            layer.insertSublayer(gradientLayer, at: 0)
//        }
//        gradientLayer.frame = bounds
//    }
//
//}

class RadialGradientViewForUnderLines: UIView {
    
    private let gradientLayerForUnderLine = RadialGradientLayerForUnderLines()
    
    public var colors: [UIColor] {
        get {
            return gradientLayerForUnderLine.colors
        }
        set {
            gradientLayerForUnderLine.colors = newValue
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if gradientLayerForUnderLine.superlayer == nil {
            layer.insertSublayer(gradientLayerForUnderLine, at: 0)
        }
        gradientLayerForUnderLine.frame = bounds
    }
    
}
