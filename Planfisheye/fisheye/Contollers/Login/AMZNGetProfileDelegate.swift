//
//  AMZNGetProfileDelegate.swift
//  fisheye
//
//  Created by Sankey Solution on 11/29/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
import LoginWithAmazon

class AMZNGetProfileDelegate: NSObject, AIAuthenticationDelegate {

    func requestDidSucceed(_ apiResult: APIResult) {

        let name = (apiResult.result as? [AnyHashable: Any])?["name"] as? String
        let email = (apiResult.result as? [AnyHashable: Any])?["email"] as? String
        let user_id = (apiResult.result as? [AnyHashable: Any])?["user_id"] as? String
        let postal_code = (apiResult.result as? [AnyHashable: Any])?["postal_code"] as? String

    }
    func requestDidFail(_ errorResponse: APIError) {

        if errorResponse.error.code == kAIApplicationNotAuthorized {

            //LoginViewController.showLogInPage()
        } else {

            UIAlertView(title: "", message: "Error occurred with message: \(errorResponse.error.message)", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "").show()
        }
    }

}
