//
//  RedirectionControllerVC.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 07/03/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class RedirectionControllerVC: UIViewController {
    
    var appSharedPrefernce =  AppSharedPreference()
    var idtoken: String  = ""
    var mobileNumber = ""
    var countryCod = ""
    var emailiddd = ""
    var fisheyeId = ""
    var screenName = ""
    var preferredLanguage = ""
    var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    var appService = AppService.shared
    var testStatuscode = ""
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
    
    @IBOutlet weak var fisheyeLabel: UILabel!
    
    @IBOutlet var letsWordTogetherLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        self.appService = AppService.shared
        self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
        self.fisheyeLabel.text = "Fisheye Hub"
        self.setLocalizationText()
        self.addObservers()
        self.getPreviousLocalization()
        self.getTranslatedText(updatelanguage: self.preferredLanguage)
        //        self.getTrackingOptions()
    }
    
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.letsWordTogetherLabel.text=self.signupmsg.letsWorkTogether
        }
    }
    /* END */
    
    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    
    func getPreviousLocalization() {
        self.preferredLanguage = self.appSharedPrefernce.getAppSharedPreferences(key: "preferredLanguage") as? String ?? "English"
        self.appSharedPrefernce.setAppSharedPreferences(key: "preferredLanguage", value: self.preferredLanguage)
//        Localize.update(language: self.preferredLanguage)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.logInUserIfLocalStorageExists()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.RedirectionControllerVC)
    }
    
    func navigateToLoginpage(){
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func logInUserIfLocalStorageExists() {
        let appLoginId = self.appSharedPrefernce.getAppSharedPreferences(key: "loginId") as? String ?? ""
        let appLoginPwd = self.appSharedPrefernce.getAppSharedPreferences(key: "loginPwd") as? String ?? ""
        
        if (appLoginId == "" && appLoginPwd == "" ) {
            self.navigateToLoginpage()
        } else if (appLoginId == nil && appLoginPwd == nil) {
            self.navigateToLoginpage()
        } else if (appLoginId.characters.count != 0 && appLoginPwd.characters.count != 0) {
            self.logincheckcontrolcalled( loginid: appLoginId , pwd: appLoginPwd as! String)
        } else {
            self.navigateToLoginpage()
        }
    }
    
    func logincheckcontrolcalled(loginid: String, pwd: String!) {
        
        let parameters =
            [
                "email": loginid,
                "password": pwd
                ] as [String: Any]
        
        let awsURL = AppConfig.loginAPI
        
        self.startLoader()
        
        guard let URL = URL(string: awsURL) else {
            
            return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        let task: URLSessionDataTask = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                
                self.stopLoader()
                self.idtoken = data["idtoken"] as? String ?? ""
                self.testStatuscode = data["statusCode"] as? String ?? ""
            
                if(data["statusCode"] as? String ?? "" == "0") {
                    
                    if let user = data["user"] as? [String: Any] {
                        let signUpMethodology = user["signupMethod"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "signUpBasicInfo", value: data["user"] as? [String: Any])
                        self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: signUpMethodology)
                        self.appSharedPrefernce.setAppSharedPreferences(key: "idtoken", value: self.idtoken)
                       
                        self.mobileNumber = user["phonenumber"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.mobileNumber)
                        self.emailiddd = user["email"] as? String ?? ""
                        self.countryCod = user["countryCode"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "email", value: self.emailiddd)
                        self.appSharedPrefernce.setAppSharedPreferences(key: "countrycode", value: self.countryCod)
                        self.fisheyeId = user["id"] as? String ?? ""
                        self.saveFcmToken()
                        let previousFEId = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeId") as? String ?? ""
                        if(previousFEId != self.fisheyeId) {
                              DatabaseManagement.shared.deleteAllContactsFromSQLite()
                              DatabaseManagement.shared.deleteAllRecentlyAddedContactsFromSQLite()
                              self.appSharedPrefernce.removeAppSharedPreferences(key: "isFetchFromBackendCompleted")
                              self.appSharedPrefernce.removeAppSharedPreferences(key: "isfronendSyncFullyCompleted")
                        }
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeId", value: self.fisheyeId)
                        let fisheyeName = user["name"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeName", value: fisheyeName) as? String ?? ""
                        var fisheyePhoto = ""
                        if( user["picture"] != nil) {
                            fisheyePhoto = user["picture"] as? String ?? ""
                        }
                        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyePhoto", value: fisheyePhoto)
                        let genderSelection = user["gender"] as? String ?? ""
                        self.appSharedPrefernce.setAppSharedPreferences(key: "gender", value: genderSelection)
                        
                        let phoneNumberVerified1 = user["phonenumber_verified"] as? Bool
                        let securityQuestionsExist1=user["securityQuesAnses_exist"] as? Bool
                        let termsNCondition1=user["isTermsNConditionsAccepted"] as? Bool
                        
                        if (securityQuestionsExist1 == true && termsNCondition1 == false){
                            self.navigateToLoginpage()
                        }else if(phoneNumberVerified1 == true && termsNCondition1 == true && securityQuestionsExist1 == false){
                            self.showToast(message: self.signupmsg.pleaseUpdateSecurityQuestion)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                self.loadSecurityControllerVCByHidingSignInDetails()
                            })
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Compulsory to submit security question answers-Redirected to Sequrity ansewers page", value: 0)
//                            self.navigateToLoginpage()
                        } else if(phoneNumberVerified1 == true && termsNCondition1 == true) {
                            //                            if(termsNCondition1)! {
                            self.showToast(message: self.signupmsg.loginsuccess)
                            self.loadBaseViewController(data: data)
                            self.startContactFrontEndSync()
                            if(NetworkStatus.sharedManager.isNetworkReachable()) {
                                self.appSharedPrefernce.setAppSharedPreferences(key: "isBackendSyncInProgress", value: "yes")
                            }
                            
                            LookoutSharedInstances.shared.isLookoutInterestRegistered = user["isLookoutFeedbackRegistered"] as? Bool
                            self.appSharedPrefernce.setAppSharedPreferences(key: "loginId", value: loginid)
                            self.appSharedPrefernce.setAppSharedPreferences(key: "loginPwd", value: pwd)
                            
                            //                            self.deleteUserLocations()
                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login successful", value: 0)
                        } else {
                            self.navigateToLoginpage()
                        }
                        self.getTrackingOptions()
                    }
                }else {
                    self.navigateToLoginpage()
                }
            } else {
                self.stopLoader()
                self.navigateToLoginpage()
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye from spalsh screen", label: "Login failed", value: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
        
    }
    
    func loadSecurityControllerVCByHidingSignInDetails() {
        DispatchQueue.main.async{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpSecurityQuestionVC") as! SignUpSecurityQuestionVC
            nextVC.isCameAfterRegistration = true
            nextVC.isFromLogin = false
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func redirectToDashboard(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func loadSignUpWithSocialLogin() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpWithSocialLogin") as! SignUpWithSocialLogin
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func loadSecurityControllerVC(masterKey: String, signUpMethod: String) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpSecurityQuestionVC") as! SignUpSecurityQuestionVC
        nextVC.masterKey = masterKey
        nextVC.reEnterMasterKey = masterKey
        nextVC.signUpMethod = signUpMethod
        self.present(nextVC, animated: true, completion: nil)
        
    }
    
    func navigateToLoginController() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func showToast(message: String) {
        DispatchQueue.main.async {
            self.view.makeToast(message, duration: AppService.shared.secondTimeInterval, position: .bottom)
        }
    }
    
    func termNCRedirect() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func sendOtpToRegisteredNumber() {
        let parameters =
            [
                
                "action": "sendotp",
                "idtoken": idtoken,
                "phonenumber": mobileNumber,
                "countryCode": countryCod,
                "email": emailiddd
                ] as [String: Any]
        
        let awsURL = AppConfig.sendOTPAPI
        guard let URL = URL(string: awsURL) else {return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = AppConfig.httpMethodPost
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                let data = json
                
                let smsresponse = data["statusCode"] as? String ?? ""
                if(smsresponse == "200") {
                    self.loadSignUPOTPVC()
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "SignUp incomplete - Phone number not verified - OTP sent successfully", value: 0)
                } else {
                    self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: "SMS Failure")
                    self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: "Sms Sending failed.")
                    self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: "failure")
                    self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: "dismiss")
                    self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: "")
                    self.popUpVCController()
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "SignUp incomplete - Phone number not verified - Failed to send OTP", value: 0)
                }
                
            } else {
                self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: "Network Failure")
                self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: "Check your Network connections.")
                self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: "failure")
                self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: "dismiss")
                self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: "")
                self.popUpVCController()
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "SignUp incomplete - Phone number not verified - Failed to send OTP", value: 0)
                
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }
    
    func saveFcmToken( ) {
        let fcmToken =  self.appSharedPrefernce.getAppSharedPreferences(key: "fcmToken")as? String ?? ""
        let saveFcmTokenApiCall = SaveFCMToken(fcmTokenRecieved: fcmToken)
        saveFcmTokenApiCall.addDidFinishBlockObserver { [unowned self] (operation, _) in
            let responseStatus = operation.responseStatus
            if responseStatus == "200"{
                
            }
        }
        AppDelegate.addProcedure(operation: saveFcmTokenApiCall)
    }
    
    func loadSignUPOTPVC() {
        DispatchQueue.main.async {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadBaseViewController(data: [String: Any]) {
        DispatchQueue.main.async {
            //            let introductaryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "dashboardIntroducataryPages") as? String ?? "0"
            //
            //            let syncIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "syncIntroductoryVariableIndex") as? String ?? "0"
            //            let pulseIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "pulseIntroductoryVariableIndex") as? String ?? "0"
            //
            //            let meetMeIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIntroductoryVariableIndex") as? String ?? "0"
            //            if (introductaryVariableIndex == "0") {
            //                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //                let nextVC = storyBoard.instantiateViewController(withIdentifier: "DashBoardPageViewController") as! DashBoardPageViewController
            //                self.present(nextVC, animated: true, completion: nil)
            //            } else {
            let baseVC  = BaseViewController.instantiateFromStoryboardWithIdentifier(identifier: "BaseViewController")
            kApplicationDelegate.nVC = UINavigationController(rootViewController: baseVC)
            kApplicationDelegate.nVC.navigationBar.isHidden  = true
            kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC
            //            }
            saveFisheyeUserObject(data: data)

        }
    }
    
    func startContactFrontEndSync() {
        let frontendSyncGlobalOperation = DispatchWorkItem {
            DatabaseManagement.shared.createRecentlyAddedTable()
            
            /* Check SQLite table is created or not? */
            let isSQliteTableExists: Bool = DatabaseManagement.shared.isSQLiteTableAreadyExists()
            if(!isSQliteTableExists) {                                                                                                                                                                               // SQLite table does't exists
                DatabaseManagement.shared.createOwenerContactsTable()                                                                                      // create table
                self.getFEUserContactsFromBackend()
            } else {                                                                                                                                                                                                                // SQLite table  exists
                let totalRecordsInTable: Int64 = DatabaseManagement.shared.checkIsOwnerContactsTableEmpty()
                if(totalRecordsInTable == 0) {                                                                                                                                                               // SQLite table is empty
                    self.getFEUserContactsFromBackend()
                } else {                                                                                                                                                                                                        // SQLite table is not empty
                    
                    self.getBlockedByContactsFromBackend()
                    let isFetchFromBackendCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isFetchFromBackendCompleted") as? String ?? "yes"
                    let isfronendSyncFullyCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isfronendSyncFullyCompleted") as? String ?? "yes"
                    if(isFetchFromBackendCompleted == "no") {
                        self.getFEUserContactsFromBackend()
                    } else if(isfronendSyncFullyCompleted == "no") {
                        ContactsSync.shared.frontEndSync()
                    } else {
                        if(NetworkStatus.sharedManager.isNetworkReachable()) {
                            ContactsSync.shared.getNonSyncedContacts()
                            if(!ContactsSync.shared.globalNonSyncedContcts.isEmpty ) {
                                ContactsSync.shared.backendSync()
                            }
                        }
                    }
                }
            }
        }
        DispatchQueue.global().async(execute: frontendSyncGlobalOperation)
    }
    
    func getFEUserContactsFromBackend() {
        self.appSharedPrefernce.setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "yes")
        let getContactsFromBackend = getLoggedInUserContactsOperation()
        getContactsFromBackend.addDidFinishBlockObserver { [unowned self] (operation, _) in
            DispatchQueue.main.async {
                ContactsSync.shared.isSyncInProgress = false
                let contactsCount = operation.contactsCount
                if(contactsCount > 0) {
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: false)
                    AppSharedPreference().setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "no")
                } else {
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: true)
                }
            }
        }
        AppDelegate.addProcedure(operation: getContactsFromBackend)
    }
    
    func getBlockedByContactsFromBackend() {
        let getBlockedByContacts = getBlockedByContactsAPIOperation()
        getBlockedByContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getBlockedByContacts)
    }
    
    //Function to start and end Loader
    func startLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.stopLoader()
        }
    }
    
    func rotateView(targetView: UIView, duration: Double = 1.0) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))
        }) { finished in
            self.rotateView(targetView: targetView, duration: duration)
        }
    }
    
    func getTrackingOptions() {
        let graphqlUrl = AppConfig.getMeetmeConfigurationUrl
        guard let URL = URL(string: graphqlUrl) else { return }
        let sessionConfig = URLSessionConfiguration.default
        
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        self.idtoken  = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        request.httpMethod = AppConfig.httpMethodGet
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
        request.httpBody = "".data(using: String.Encoding.utf8)
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                if let meetmeConfigObj = json["meetmeConfig"] as? [String: Any] {
                    if let configContentObj = meetmeConfigObj["configContent"] as? [String: Any] {
                       
//                        if let meetmeTrackingOptions = configContentObj["trackingOptions"] as? [[String: Any]] {
//                            MeetMeTrackingOptions.meetTrackingOptions = []
//                            for trackOption in meetmeTrackingOptions {
//                                let trackObj = MeetMeTrackingOptionsList()
//                                trackObj.trackingOption = trackOption["optionName"]  as? String ??  ""
//                                trackObj.trackingTime = trackOption["trackingTime"]  as? NSArray
//                                trackObj.isTimeRequired = trackOption["isTimeRequired"] as? Bool
//                                MeetMeTrackingOptions.meetTrackingOptions.append(trackObj)
//                            }
//                        }
                        MeetMeTrackingOptions.meetmeParticipantLimit = configContentObj["numberOfParticipants"] as? Int
                        MeetMeTrackingOptions.mapResetTimeInterval = configContentObj["mapResetTimeInterval"] as? Double
                        MeetMeTrackingOptions.meetingTrackableTime = configContentObj["meetingTrackableTime"] as? Double
                        
                        AppService.shared.firstTimeInterval = configContentObj["firstTimeInterval"] as? Double ?? 1.0
                        AppService.shared.noOfCarouselCards = configContentObj["noOfCarouselCards"] as? Int ?? 4
                        
                        AppService.shared.noOfTabsToShowInTopRibbon = configContentObj["noOfTabsToShowInTopRibbon"] as? Int ?? 3
                        
                        AppService.shared.secondTimeInterval = configContentObj["secondTimeInterval"] as? Double ?? 2.0
                        AppService.shared.meetmeSearchContactTimeInterval = configContentObj["meetmeSearchContactTimeInterval"] as? Double ?? 1.5
                        ProfileToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix = configContentObj["profileToastDelatimebysix"] as? Double ?? 6.0
                        ProfileToastMsgHeadingSubheadingLabels.shared.toastDelayTime = configContentObj["profileToastDelayTime"] as? Double ?? 4.0
                        ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBelesserThan = configContentObj["minMobNumLength"] as? Int ?? 3
                        ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBeGreaterThan = configContentObj["maxMobNumLength"] as? Int ?? 14
                        
                        ProfileToastMsgHeadingSubheadingLabels.shared.emailRegX = configContentObj["emailAjax"] as? String ??  ""
                        ProfileToastMsgHeadingSubheadingLabels.shared.passwordRegX = configContentObj["passwordAjax"] as? String ??  ""
                        
                        AppService.shared.profileImageBorderColor = configContentObj["profileImageBorderColor"] as? String ?? ""
                        AppService.shared.carouselCardColor = configContentObj["carouselCardColor"] as? String ?? ""
                        
                        SignupToastMsgHeadingSubheadingLabels.shared.toastdelatimebysix = configContentObj["signupToastdelatimebysix"] as? Double ?? 6.0
                        SignupToastMsgHeadingSubheadingLabels.shared.toastDelayMedium = configContentObj["signupToastDelayMedium"] as? Double ?? 5.0
                        SignupToastMsgHeadingSubheadingLabels.shared.toastDelayTime = configContentObj["signupToastDelayTime"] as? Double ?? 4.0
                        SyncToastMessages.shared.syncToastMaxTimeInterval = configContentObj["syncToastMaxTimeInterval"] as? Double ?? 300.0
                        SyncToastMessages.shared.syncToastThirdTimeInterval = configContentObj["syncToastThirdTimeInterval"] as? Double ?? 180.0
                        PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse = configContentObj["toastDelayTimeForPulse"] as? Double ?? 3.0
                        
                        SyncToastMessages.shared.popUpInOutTimePeriod = configContentObj["popUpInOutTimePeriod"] as? Int ?? Int(2.0)
                        
//                        if let addressTypes = configContentObj["addressTypes"] as? [String] {
//                            AppService.shared.addressTypes = []
//                            for addressType in addressTypes {
//                                AppService.shared.addressTypes.append(addressType)
//                            }
//                        }
                        
                        if let languagesArray = configContentObj["languages"] as? [String] {
                            AppService.shared.languages = []
                            for language in languagesArray {
                                AppService.shared.languages.append(language)
                            }
                        }
                    }
                }
            } else {
                
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
        
    }
    
    // Description - Get translated text from backend
    // Author - Vaishali
    func getTranslatedText(updatelanguage: String) {
        //            self.startLoader()
        let getTranslation = GetTranslatedText(language: updatelanguage)
        getTranslation.addDidFinishBlockObserver { [unowned self] (_, _) in
            NotificationCenter.default.post(name: CHANGE_LANGUAGE, object: nil)
        }
        AppDelegate.addProcedure(operation: getTranslation)
    }
    // End
    
    // Description - Get translated text from backend
    // Author - Vaishali
    func deleteUserLocations() {
        
        let getTranslation = DeleteUsersPerviousLocations()
        getTranslation.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getTranslation)
    }
    // End
}
