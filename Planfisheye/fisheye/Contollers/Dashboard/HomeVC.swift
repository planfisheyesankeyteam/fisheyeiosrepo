//
//  HomeVC.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 01/07/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import Contacts
import SDWebImage

@objc protocol HomeVCDelegate {
    func navigateToViewWith(title: String)
    func logbookNavigate(title: String)
    
}

class HomeVC: UIViewController, CardViewDelegate {
    func logbookClick(withTitle: String) {
        homeDelegate?.logbookNavigate(title: withTitle)
        
    }
    let appSharedPrefernce = AppSharedPreference()
    
    var ellipse: PSEllipse?
    
    var timerIntro: Timer = Timer()
    var arrayButtons: [UIButton] = []
    var arrayButtonTitle: [String] = []
    var arrayConstraintX: [NSLayoutConstraint] = []
    var arrayConstraintY: [NSLayoutConstraint] = []
    var lastTranslation: CGPoint = CGPoint(x: 0, y: 0)
    var buttonOffset = 0
    let ellipsePoints = 360
    let profileText = ProfileToastMsgHeadingSubheadingLabels.shared
    let appService = AppService.shared
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userImage: UIImageView! {
        didSet {
            userImage.layer.cornerRadius = userImage.frame.size.width/2
            userImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var welcomeLabel: UILabel!
    
    var cardViews = [UIView]()
    
    var cardViewController = [CardView]()
    
    var numberOfCards: Int = 0
    
    var isFirstDisplayed: Bool = true
    
    weak var homeDelegate: HomeVCDelegate?
    
    var feContacts: [FEContact]!
    
    var noOfPulseNotifications = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        getNotificationCount()
        self.userName.textColor = UIColor(red: (76.0/255.0), green: (67.0/255.0), blue: (112.0/255.0), alpha: 1.0)
        
        if let username = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeName") {
            self.userName.text = username as! String
        } else {
            self.userName.text = "Username"
        }
        
        let userImageUrl = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyePhoto") as? String ?? ""
        let gender = self.appSharedPrefernce.getAppSharedPreferences(key: "gender") as? String ?? ""
        if userImageUrl == "" || userImageUrl == nil {
            if gender == "Female" || gender == "Trans-Female" {
                self.userImage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "female"))
            } else {
                self.userImage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Male"))
            }
        } else {
//            self.userImage?.sd_setImage(with: URL(string: userImageUrl.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
           
            self.userImage?.sd_setImage(with: URL(string: userImageUrl.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            
        }
        self.userImage.layer.borderWidth = 2
        self.userImage.layer.borderColor = UIColor.hexStringToUIColor(hex: self.appService.profileImageBorderColor).cgColor
        self.setUpview(numOfCards: self.appService.noOfCarouselCards)
        self.addObservers()
    }
    
      // Add all observers here
      func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMeetNotificationCount), name: REFRESH_MEET_NOTIFICATION_COUNT, object: nil)
      }
    
    @objc func refreshMeetNotificationCount(){
        for (_, btn) in self.cardViews.enumerated() {
            DispatchQueue.main.async {
                let cardViewController = self.cardViewController[(btn.tag)]
                if cardViewController.titleLbl.text == self.profileText.meetMe || cardViewController.titleLbl.text == ProfileToastMsgHeadingSubheadingLabels().meetMe{
                    cardViewController.inviteCountLbl.text = String(self.appService.noOfUnreadMeetNotifications)
                }
            }
        }
    }
    
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                for (_, btn) in self.cardViews.enumerated() {
                        let cardViewController = self.cardViewController[(btn.tag)]
                        if cardViewController.titleLbl.text == self.profileText.meetMe || cardViewController.titleLbl.text == ProfileToastMsgHeadingSubheadingLabels().meetMe{
                              cardViewController.titleLbl.text = self.profileText.meetMe
                        }else if cardViewController.titleLbl.text == self.profileText.sync || cardViewController.titleLbl.text == ProfileToastMsgHeadingSubheadingLabels().sync{
                              cardViewController.titleLbl.text = self.profileText.sync
                        }else if cardViewController.titleLbl.text == self.profileText.myProfile || cardViewController.titleLbl.text == ProfileToastMsgHeadingSubheadingLabels().myProfile{
                              cardViewController.titleLbl.text = self.profileText.myProfile
                        }else if cardViewController.titleLbl.text == self.profileText.lookout || cardViewController.titleLbl.text == ProfileToastMsgHeadingSubheadingLabels().lookout{
                              cardViewController.titleLbl.text = self.profileText.lookout
                        }else if cardViewController.titleLbl.text == self.profileText.pulse || cardViewController.titleLbl.text == ProfileToastMsgHeadingSubheadingLabels().pulse{
                              cardViewController.titleLbl.text = self.profileText.pulse
                        }
                  }
            }
      }
                  
    func createPanGestureRecognizer(_ targetView: UIView) {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        targetView.addGestureRecognizer(panGesture)
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: self.view)
        
        if panGesture.state == .began {
            
            self.lastTranslation = translation
        } else {
            self.adjustTranslation(translation)
            
            if panGesture.state == .ended {
            }
        }
    }
    
    func adjustTranslation(_ translation: CGPoint) {
        buttonOffset = buttonOffset + Int(translation.x - self.lastTranslation.x)
        if buttonOffset < 0 {
            buttonOffset += ellipsePoints
        }
        buttonOffset = buttonOffset % ellipsePoints
        
        self.rotate()
        self.lastTranslation = translation
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let screenRect = self.view.frame
        if let username = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeName") {
            self.userName.text = username as! String
        } else {
            self.userName.text = "Username"
        }
        
        let userImageUrl = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyePhoto") as? String ?? ""
        let gender = self.appSharedPrefernce.getAppSharedPreferences(key: "gender") as? String ?? ""
        if userImageUrl == "" || userImageUrl == nil {
            if gender == "Female" || gender == "Trans-Female" {
                self.userImage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "female"))
            } else {
                self.userImage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Male"))
            }
        } else {
//            self.userImage?.sd_setImage(with: URL(string: userImageUrl.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            self.userImage?.sd_setImage(with: URL(string: userImageUrl.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        }
        
        let adjusted = decreaseRect(rect: screenRect, byPercentage: 0.5)
        
        let rect =  CGRect(x: adjusted.origin.x, y: adjusted.origin.y/2, width: adjusted.size.width, height: adjusted.size.height/2)
        
        self.ellipse = PSEllipse(rect: rect, ellipsePoints: self.ellipsePoints)
        
        self.rotate()
    }
    
    func decreaseRect(rect: CGRect, byPercentage percentage: CGFloat) -> CGRect {
        let startWidth = rect.width
        let startHeight = rect.height
        let adjustmentWidth = (startWidth * percentage) / 2.0
        let adjustmentHeight = (startHeight * percentage) / 2.0
        return rect.insetBy(dx: adjustmentWidth, dy: adjustmentHeight)
    }
    
    func refreshButtons() {
        let step = ellipsePoints / self.cardViews.count
        
        for i in 0..<self.cardViews.count {
            let button = self.cardViews[i]
            
            let offset = (buttonOffset + (i + 1) * step) % ellipsePoints
            let point = self.ellipse!.array[offset]
            let baseY = self.view.frame.size.height / 2.0 - button.frame.size.height / 2.0
            let x: CGFloat = self.ellipse!.frame.origin.x + point.x //- button.frame.size.width / 2.0
            let y: CGFloat = baseY + point.y
            
            button.center = CGPoint(x: x, y: y)
            
            button.layer.zPosition = y
            
            let scale = ((point.y / self.ellipse!.maxY) / 4.0) + 0.75
            button.layer.transform = CATransform3DMakeScale(scale, scale, 1.0)
            
            button.layer.shadowOpacity = 1.0
            button.layer.shadowOffset = CGSize(width: 10, height: y - baseY)
            button.layer.shadowRadius = 10.0
        }
        self.view.setNeedsLayout()
    }
    
    func didSelectCard(withTitle: String) {
        guard (homeDelegate?.navigateToViewWith(title: withTitle)) != nil else {
            return
        }
    }
    
    func moveCard(position: Int) {
        switch position {
        case 0:
            self.rotateClockwise()
            self.perform(#selector(self.rotateClockwise), with: nil, afterDelay: 0.3)
        case 1:
            rotateClockwise()
        case 4:
            rotateAntiClockwise()
            self.perform(#selector(self.rotateAntiClockwise), with: nil, afterDelay: 0.3)
        default:
            rotateAntiClockwise()
        }
    }
    
    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey, CNContactEmailAddressesKey, CNContactOrganizationNameKey, CNContactJobTitleKey, CNContactPostalAddressesKey, CNContactSocialProfilesKey, CNContactInstantMessageAddressesKey] as [Any]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
            } catch {
                
            }
        }
        
        return results
    }()
    
    func getContactsFromiPhone() {
        
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts, completionHandler: {
            granted, error in
            
            guard granted else {
                DispatchQueue.main.async {
                    
                    kSweetAlert.showAlert(NSLocalizedString("Can't access contact", comment: ""), subTitle: "Please go to Settings -> FishEye to enable contact permission", style: .error, buttonTitle: "Settings", buttonColor: UIColor.frenchBlue(), otherButtonTitle: "Cancel", action: { (result) in
                        if result {
                            
                            let url = NSURL(string: UIApplication.openSettingsURLString)
                            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
                        }
                    })
                }
                
                return
            }
            
            self.feContacts = [FEContact]()
            
            for contact in self.contacts {
                
                let feContact = FEContact()
                
                let fullName = CNContactFormatter.string(from: contact, style: .fullName) ?? "No Name"
                
                feContact?.name = fullName
                
                feContact?.company = contact.organizationName
                
                var feAllAddress = [FEAddress]()
                for address in contact.postalAddresses {
                    
                    let feAddress = FEAddress()
                    feAddress?.country = address.value.country
                    feAddress?.postalCode = address.value.postalCode
                    feAddress?.streetAddress = address.value.street
                    feAddress?.locality = address.value.city
                    feAddress?.region = address.value.state
                    
                    if let street = feAddress?.streetAddress {
                        feAddress?.formatted = street
                    }
                    
                    if let city = feAddress?.locality {
                        
                        feAddress?.formatted?.append(",\(city)")
                    }
                    if let state = feAddress?.region {
                        
                        feAddress?.formatted?.append(",\(state)")
                    }
                    
                    if let postalCode = feAddress?.postalCode {
                        feAddress?.formatted?.append(",\(postalCode)")
                    }
                    if let country = feAddress?.country {
                        feAddress?.formatted?.append(",\(country)")
                    }
                    
                    feAllAddress.append(feAddress!)
                }
                
                if feAllAddress.count > 0 {
                    feContact?.address = feAllAddress[0]
                }
                
                var feAllIMAddress = [CNInstantMessageAddress]()
                
                for imAddress in contact.instantMessageAddresses {
                    
                    let imAddress = CNInstantMessageAddress(username: imAddress.value.username, service: imAddress.value.service)
                    
                    feAllIMAddress.append(imAddress)
                }
                
                if feAllIMAddress.count > 0 {
                }
                
                var feAllSocialAddress = [CNSocialProfile]()
                
                for socialProfile in contact.socialProfiles {
                    
                    let social = CNSocialProfile.init(urlString: socialProfile.value.urlString, username: socialProfile.value.username, userIdentifier: nil, service: socialProfile.value.service)
                    
                    feAllSocialAddress.append(social)
                }
                
                if feAllSocialAddress.count > 0 {
                }
                
                var feEmails = [String]()
                for email in contact.emailAddresses {
                    let emailId = email.value as String
                    feEmails.append(emailId)
                }
                if feEmails.count > 0 {
                    
                    let primaryEmail = feEmails.remove(at: 0)
                    
                    feContact?.email =  "\(primaryEmail)"
                    
                    feContact?.secondaryEmails = feEmails.count>0 ? feEmails : nil
                }
                
                var fePhnNums = [String]()
                for num in contact.phoneNumbers {
                    
                    if let number = num.value.value(forKey: "digits") as? String {
                        fePhnNums.append(number)
                        
                    }
                }
                
                if fePhnNums.count > 0 {
                    
                    let primaryNum = fePhnNums.remove(at: 0)
                    
                    feContact?.phoneNumber =  "\(primaryNum)"
                    
                    feContact?.secondaryPhoneNumbers = fePhnNums.count>0 ? fePhnNums : nil
                    
                }
                self.feContacts.append(feContact!)
            }
            
            self.postContatcs(contacts: self.feContacts)
            
        })
    }
    
    func postContatcs(contacts: [FEContact]) {
        
        let syncReq = FESyncContactsRequest()
        
        syncReq?.contacts = contacts
        
        WebServiceManager.shared().postContacts(syncReq: syncReq!, handler: { (_, _, success) in
            if success {
                userDefaults.set(true, forKey: kSyncedContacts)
            } else {
                
            }
        })
        
    }
    
    func setUpview(numOfCards: Int) {
        
        self.numberOfCards = numOfCards
        var basePoint = CGPoint()
        if numberOfCards == 4{
            basePoint = CGPoint(x: self.view.bounds.width/2, y: self.view.bounds.height/2-25)
        }else{
            basePoint = CGPoint(x: self.view.bounds.width/2, y: self.view.bounds.height/2-50)
        }
        var curAngle = 0.0
        let incAngle1 = (360.0/CGFloat(4))*CGFloat(.pi/180.0)
        let incAngle2 = (360.0/CGFloat(5))*CGFloat(.pi/180.0)
        let circleCenter = basePoint
        let circleRadius = Double(0.1875 * kScreenSize.width)
        
        for i in 0..<numOfCards {
            var buttonCenter = CGPoint()
            
            buttonCenter.x = circleCenter.x + CGFloat(cos(curAngle)*circleRadius)
            
            buttonCenter.y = circleCenter.y + CGFloat(sin(curAngle)*circleRadius)
            
            let btn = UIView()
            
            let width: CGFloat = 0.46875 * kScreenSize.width
            
            btn.frame = CGRect(x: buttonCenter.x, y: buttonCenter.y, width: width, height: width*0.9090909091)
            
            self.view.addSubview(btn)
            
            btn.tag = i
            
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            gesture.direction = UISwipeGestureRecognizer.Direction.up
            btn.addGestureRecognizer(gesture)
            
            let gesture1 = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            gesture1.direction = UISwipeGestureRecognizer.Direction.down
            btn.addGestureRecognizer(gesture1)
            
            let gesture2 = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            gesture2.direction = UISwipeGestureRecognizer.Direction.left
            btn.addGestureRecognizer(gesture2)
            
            let gesture3 = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
            gesture3.direction = UISwipeGestureRecognizer.Direction.right
            btn.addGestureRecognizer(gesture3)
            
            btn.isUserInteractionEnabled = true
            
            btn.center = buttonCenter
            
            let cardVC = CardView.instantiateFromStoryboardWithIdentifier(identifier: "CardView")
            
            cardVC.cardViewDelegate = self
            
            cardVC.view.frame = btn.bounds
            
            cardVC.view.tag = btn.tag
            
            switch i {
                
            case 0:
                
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    btn.center = buttonCenter
                    btn.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                }, completion: {_ in
                    DispatchQueue.main.async {
                        cardVC.position = .right
                        cardVC.titleLbl.text = self.profileText.sync
                        cardVC.inviteCountLbl.isHidden = true
                        cardVC.imageView.image = UIImage(named: "rectangle2Copy")
                        cardVC.imageView.image?.accessibilityLabel = "Sync"
                        self.view.sendSubviewToBack(btn)
                        self.hideViewsin(cardVC, inView: btn)
                    }
                })
                
            case 1:
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    btn.center = buttonCenter
                    btn.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion: {_ in
                    DispatchQueue.main.async {
                        cardVC.position = .down
                        cardVC.titleLbl.text = self.profileText.meetMe
                        let count = self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeNotificationCount")
                        cardVC.inviteCountLbl.text  = count as? String
                        cardVC.inviteCountLbl.isHidden = true
                        cardVC.imageView.image = UIImage(named: "shakingHandsCopy")
                        cardVC.imageView.image?.accessibilityLabel = "Meet"
                        self.unHideViewsin(cardVC, inView: btn)
                        cardVC.view.restorationIdentifier = "MeetMeCardView"
                    }
                })
                
            case 2:
                
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    btn.center = buttonCenter
                    btn.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                }, completion: {_ in
                    DispatchQueue.main.async {
                        cardVC.position = .left
                        cardVC.inviteCountLbl.isHidden = true
                        cardVC.titleLbl.text = self.profileText.myProfile
                        cardVC.imageView.image = UIImage(named: "newCaroselProfile")
                        cardVC.imageView.image?.accessibilityLabel = "Profilw"
                        self.view.sendSubviewToBack(btn)
                        self.hideViewsin(cardVC, inView: btn)
                    }
                })
                
            case 3:
                
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    btn.center = buttonCenter
                    btn.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                }, completion: {_ in
                    DispatchQueue.main.async {
                        cardVC.position = .leftUp
                        cardVC.titleLbl.text = self.profileText.pulse
                        cardVC.inviteCountLbl.isHidden = true
                        cardVC.imageView.image = UIImage(named: "speedLocationIcon-1")
                        self.view.sendSubviewToBack(btn)
                        self.hideViewsin(cardVC, inView: btn)
                    }
                })
                
            default:
                
                UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    btn.center = buttonCenter
                    btn.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
                }, completion: {_ in
                    DispatchQueue.main.async {
                        cardVC.position = .upRight
                        cardVC.titleLbl.text = self.profileText.lookout
                        cardVC.imageView.image = UIImage(named: "LookoutIcon")
                        self.view.sendSubviewToBack(btn)
                        self.hideViewsin(cardVC, inView: btn)
                    }
                })
                
            }
            
            cardViews.append(btn)
            
            if numOfCards == 4{
                curAngle += Double(incAngle1)
            }else{
                if(i < 3) {
                    curAngle += Double(incAngle1)
                } else {
                    curAngle += Double(incAngle2)
                }
            }
            
            
            btn.addSubview(cardVC.view)
            
            self.addChild(cardVC)
            
            cardVC.didMove(toParent: self)
            
            cardViewController.append(cardVC)
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if isFirstDisplayed {
            
            let btn = cardViews.remove(at: 0)
            cardViews.append(btn)
            rotate()
            isFirstDisplayed = false
        }
        
        if let username = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyeName") {
            self.userName.text = username as! String
        } else {
            self.userName.text = "Username"
        }
        
        let userImageUrl = self.appSharedPrefernce.getAppSharedPreferences(key: "fisheyePhoto") as? String ?? ""
        let gender = self.appSharedPrefernce.getAppSharedPreferences(key: "gender") as? String ?? ""
        if userImageUrl == "" || userImageUrl == nil {
            if gender == "Female" || gender == "Trans-Female" {
                self.userImage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "female"))
            } else {
                self.userImage?.sd_setImage(with: URL(string: ""), placeholderImage: #imageLiteral(resourceName: "Male"))
            }
        } else {
//           self.userImage?.sd_setImage(with: URL(string: userImageUrl.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
            //Need to check - Akshata
            
            self.userImage?.sd_setImage(with: URL(string: userImageUrl.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        }
    }
    
    func navigateToModel(_ sender: UITapGestureRecognizer) {
        
        if let btnTag = sender.view?.tag {
            
            let vc = cardViewController[btnTag]
            
            guard let method = homeDelegate?.navigateToViewWith(title: vc.titleLbl.text!) else {
                return
                
            }
            method
            
        }
        
    }
    
    func getNotificationCount( ) {
        let getNotificationCountApi = getProfileOperation()
        getNotificationCountApi.addDidFinishBlockObserver { [unowned self] (operation, _) in
            if let profileReputationpoints = operation.profileObject.userMeetmeNotificationCount {
                self.appService.noOfUnreadMeetNotifications = profileReputationpoints
            }
            if let userPulseNotiCount = operation.profileObject.userPulseNotificationCount {
                self.noOfPulseNotifications = userPulseNotiCount
            }
        }
        AppDelegate.addProcedure(operation: getNotificationCountApi)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            let btn = gesture.view
            let cardViewController = self.cardViewController[(btn?.tag)!]
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                handleRotatefor(contoller: cardViewController, direction: .right)
            case UISwipeGestureRecognizer.Direction.down:
                handleRotatefor(contoller: cardViewController, direction: .down)
            case UISwipeGestureRecognizer.Direction.left:
                handleRotatefor(contoller: cardViewController, direction: .left)
            case UISwipeGestureRecognizer.Direction.up:
                handleRotatefor(contoller: cardViewController, direction: .up)
            default:
                break
            }
        }
    }
    
    func handleRotatefor(contoller: CardView, direction: UISwipeGestureRecognizer.Direction) {
        if let position = contoller.position {
            switch position {
            case .right:
                if direction == .left || direction == .up {
                    self.rotateAntiClockwise()
                } else {
                    self.rotateClockwise()
                }
            case .down:
                if direction == .right || direction == .up {
                    self.rotateAntiClockwise()
                } else {
                    self.rotateClockwise()
                }
            case .left:
                if direction == .right || direction == .down {
                    self.rotateAntiClockwise()
                } else {
                    self.rotateClockwise()
                }
                
            case .upRight:
                if direction == .left || direction == .down {
                    self.rotateClockwise()
                } else {
                    self.rotateAntiClockwise()
                }
            case .leftUp:
                if direction == .right || direction == .down {
                    self.rotateAntiClockwise()
                } else {
                    self.rotateClockwise()
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func rotateClockwise() {
        
        if self.numberOfCards > 0 {
            let btn = cardViews.remove(at: self.cardViews.count - 1)
            cardViews.insert(btn, at: 0)
            rotate()
        }
    }
    
    @objc func rotateAntiClockwise() {
        
        let btn = cardViews.remove(at: 0)
        cardViews.append(btn)
        rotate()
        
    }
    
    func rotate() {
        let basePoint = CGPoint(x: self.view.bounds.width/2, y: self.view.bounds.height/2-25)
        var curAngle = 0.0
        let incAngle = (360.0/CGFloat(cardViews.count))*CGFloat(.pi/180.0)
        let circleCenter = basePoint
        let circleRadius = Double(0.1875 * kScreenSize.width)
        
        
        for (index, btn) in cardViews.enumerated() {
            curAngle += Double(incAngle)
            self.view.sendSubviewToBack(btn)
            var buttonCenter = CGPoint()
            buttonCenter.x = circleCenter.x + CGFloat(cos(curAngle)*circleRadius)
            buttonCenter.y = circleCenter.y + CGFloat(sin(curAngle)*circleRadius)
            let cardViewController = self.cardViewController[(btn.tag)]
            
            switch index {
            case 0:
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    btn.center = CGPoint(x: self.view.center.x, y: buttonCenter.y + CGFloat(50.0))
                    btn.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: {_ in
                    self.unHideViewsin(cardViewController, inView: btn)
                    cardViewController.position = .down
                    self.view.bringSubviewToFront(btn)
                })
            case 1:
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    //                              btn.center = CGPoint(x: buttonCenter.x + CGFloat(-40.0) , y: buttonCenter.y + CGFloat(-10.0))
                    if self.cardViews.count < 4 {
                        btn.center = CGPoint(x: self.view.center.x - self.view.frame.width / 4, y: self.view.center.y - CGFloat(35.0))

                    }else if self.cardViews.count == 4{
                        btn.center = CGPoint(x: self.view.center.x - self.view.frame.width / 4, y: buttonCenter.y + CGFloat(10.0))
                    }else{
                        btn.center = CGPoint(x: self.view.center.x - self.view.frame.width / 4, y: buttonCenter.y + CGFloat(-10.0))
                    }
                    btn.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                }, completion: {_ in
                    self.hideViewsin(cardViewController, inView: btn)
                    cardViewController.position = .left
                })
                
            case 2:
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    if self.cardViews.count < 4{
                        btn.center = CGPoint(x: self.view.center.x + self.view.frame.width / 4, y: buttonCenter.y + CGFloat(-10.0))
                        btn.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                    }else if self.cardViews.count == 4{
                        btn.center = buttonCenter
                        btn.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                    }else{
                        btn.center = CGPoint(x: self.view.center.x - self.view.frame.width / 6, y: buttonCenter.y + CGFloat(-10.0))
                        btn.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                    }
                }, completion: {_ in
                    self.hideViewsin(cardViewController, inView: btn)
                    //                            cardViewController.position = .leftUp
                    if self.cardViews.count < 4 {
                        cardViewController.position = .right
                    }
                    else if self.cardViews.count == 4{
                        cardViewController.position = .upRight
                    }else{
                        cardViewController.position = .leftUp
                    }
                })
                
            case 3:
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    if self.cardViews.count <= 4{
                        btn.center = CGPoint(x: buttonCenter.x + CGFloat(40.0) , y: buttonCenter.y + CGFloat(10.0))
                        btn.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                    }else{
                        btn.center = CGPoint(x: self.view.center.x + self.view.frame.width / 6, y: self.cardViews[2].center.y)
                        btn.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                    }
                }, completion: {_ in
                    self.hideViewsin(cardViewController, inView: btn)
                    if self.cardViews.count <= 4{
                        cardViewController.position = .right
                    }else{
                        cardViewController.position = .upRight
                    }
                })
            case 4:
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    btn.center = CGPoint(x: buttonCenter.x + CGFloat(35.0), y: self.cardViews[1].center.y)
                    btn.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                }, completion: {_ in
                    self.hideViewsin(cardViewController, inView: btn)
                    cardViewController.position = .right
                    self.view.bringSubviewToFront(btn)
                    
                })
                
            default:
                break
            }
        }
        self.view.setNeedsLayout()
    }
    
    func hideViewsin(_ cardViewController: CardView, inView: UIView) {
        cardViewController.inviteCountLbl.isHidden = true
        cardViewController.notificationBtn.isHidden = true
        cardViewController.titleLbl.isHidden = false
        // cardViewController.titleLbl?.font = UIFont(name: "ROBOTO-MEDIUM", size: 20)
    }
    
    func unHideViewsin(_ cardViewController: CardView, inView: UIView) {
        cardViewController.notificationBtn.isHidden = false
        cardViewController.titleLbl.isHidden = false
        // cardViewController.titleLbl?.font = UIFont(name: "ROBOTO-MEDIUM", size: 17)
        if cardViewController.titleLbl.text == self.profileText.meetMe && appService.noOfUnreadMeetNotifications>0 {
            cardViewController.inviteCountLbl.isHidden = false
            cardViewController.inviteCountLbl.text = String(appService.noOfUnreadMeetNotifications)
        } else if cardViewController.titleLbl.text == self.profileText.pulse && self.noOfPulseNotifications>0 {
            cardViewController.inviteCountLbl.isHidden = false
            cardViewController.inviteCountLbl.text = String(self.noOfPulseNotifications)
        } else {
            cardViewController.inviteCountLbl.isHidden = true
        }
        
        var cardViewText = cardViewController.titleLbl.text
        cardViewText = cardViewText ?? ""
        
        switch cardViewText {
        case self.profileText.myProfile :
            cardViewController.notificationBtn.isHidden = true
            cardViewController.notificationBtn.isUserInteractionEnabled = false
            break
        case self.profileText.sync :
            cardViewController.notificationBtn.isHidden = true
            cardViewController.notificationBtn.isUserInteractionEnabled = false
            break
        case self.profileText.pulse :
            cardViewController.notificationBtn.isHidden = false
            cardViewController.notificationBtn.isUserInteractionEnabled = true
            break
        case self.profileText.meetMe :
            cardViewController.notificationBtn.isHidden = false
            cardViewController.notificationBtn.isUserInteractionEnabled = true
            break
        case self.profileText.lookout :
            cardViewController.notificationBtn.isHidden = true
            cardViewController.notificationBtn.isUserInteractionEnabled = false
            break
            
        case .none:
            break
            
        case .some:
            break
        }
    }
    
}

extension UIPanGestureRecognizer {
    
    public struct PanGestureDirection: OptionSet {
        public let rawValue: UInt8
        
        public init(rawValue: UInt8) {
            self.rawValue = rawValue
        }
        
        static let Up = PanGestureDirection(rawValue: 1 << 0)
        static let Down = PanGestureDirection(rawValue: 1 << 1)
        static let Left = PanGestureDirection(rawValue: 1 << 2)
        static let Right = PanGestureDirection(rawValue: 1 << 3)
    }
    
    private func getDirectionBy(velocity: CGFloat, greater: PanGestureDirection, lower: PanGestureDirection) -> PanGestureDirection {
        if velocity == 0 {
            return []
        }
        return velocity > 0 ? greater : lower
    }
    
    public func direction(in view: UIView) -> PanGestureDirection {
        let velocity = self.velocity(in: view)
        let yDirection = getDirectionBy(velocity: velocity.y, greater: PanGestureDirection.Down, lower: PanGestureDirection.Up)
        let xDirection = getDirectionBy(velocity: velocity.x, greater: PanGestureDirection.Right, lower: PanGestureDirection.Left)
        return xDirection.union(yDirection)
    }
    
}

extension UILabel {
    func animateToFont(_ font: UIFont, withDuration duration: TimeInterval) {
        let oldFont = self.font
        self.font = font
        // let oldOrigin = frame.origin
        let labelScale = oldFont!.pointSize / font.pointSize
        let oldTransform = transform
        transform = transform.scaledBy(x: labelScale, y: labelScale)
        setNeedsUpdateConstraints()
        UIView.animate(withDuration: duration) {
            self.transform = oldTransform
            self.layoutIfNeeded()
        }
    }
}

class PSEllipse {
    var array: [PSPoint] = []
    var frame: CGRect
    var minY: CGFloat = CGFloat.greatestFiniteMagnitude
    var maxY: CGFloat = CGFloat.leastNormalMagnitude
    
    init(rect: CGRect, ellipsePoints: Int) {
        self.frame = rect
        
        let cx: CGFloat = frame.width / 2.0
        let cy: CGFloat = frame.height / 2.0
        
        for i in 0..<ellipsePoints {
            let a: CGFloat = (CGFloat(Double.pi * 2) / CGFloat(ellipsePoints)) * CGFloat(i)
            let x = cx + cx * cos(a)
            let y = cy + cy * sin(a)
            array.append(PSPoint(x: x, y: y))
            
            if y < self.minY {
                self.minY = y
            }
            if y > self.maxY {
                self.maxY = y
            }
        }
    }
}

class PSPoint {
    var x: CGFloat
    var y: CGFloat
    
    init(x: CGFloat, y: CGFloat) {
        self.x = x
        self.y = y
    }
}
