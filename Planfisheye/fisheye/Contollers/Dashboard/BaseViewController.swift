//
//  BaseViewController.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 03/07/17.
//  Modified by Hitesh Patil.
//  Updated by Hitesh Patil on 24/01/17.
//  Copyright © 2017 fisheye. All rights reserved.

import UIKit
import AWSAuthCore
import Contacts
import UserNotifications
import CoreLocation

class Source {
    var image: UIImage?
    let title: String?
    
    init(imageStr: String, title: String ) {
        self.image = UIImage.init(named: imageStr)
        self.title = title
    }
}

class BaseViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, FloatyDelegate, Profiledelegate, MenuBarDelegate, HomeVCDelegate, CLLocationManagerDelegate, UICollectionViewDataSource {
    var locationStatus: NSString = "Not Started"
    var seenError: Bool = false
    var locationFixAchieved: Bool = false
    var userInfo: NSDictionary?
    let appSharedPrefernce = AppSharedPreference()
    let appService = AppService.shared
    let signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
    let syncmsg = SyncToastMessages.shared
    let pulsemsg = PulseToastMsgHeadingSubheadingLabels.shared
    var store = CNContactStore()
    var locManager = CLLocationManager()
    var isFirstDisplayed: Bool = true
    var addedSubview: Bool = false
    var userNameStr: String! = ""
    var loginViewController: LoginViewController?
    var syncViewContoller: SyncViewController?
    var syncBaseViewController: SyncBaseViewController?
    var pulseBaseViewcontroller: PulseListingViewController!
    var pulseLogbookViewcontroller: PulseListingViewController!
    var meetMeBaseViewcontroller: MeetMeBaseViewController!
    var meetMeCreateViewController: CreateMeetVC!
    var profileBaseviewController: ProfileBaseViewController!
    var lookoutBaseVCViewController: LookoutBaseVC!
    var validationController = ValidationController()
    var profileText = ProfileToastMsgHeadingSubheadingLabels.shared
    var menuNameSelection: String = ""
    var isToOpenPulse: Bool = false
    var isToOpenAddContact: Bool = false
    var version = ""
    var idtoken = ""
    weak var menubarDelegate: MenuBarDelegate?
    var selectedIndex: String = ""
    var isFromMeetMeNotification: Bool = false
    var notificationMeetMeId = ""
    var isFromPulseNotification: Bool = false
    var isFromCapsuleChatNotification: Bool = false
    var isCapsuleLocked: Bool = false
    var meetTitle: String = ""
    var notificationPulseId = ""
    var isPulsed: Bool = false
    var inputData: [Source] =
        [
            Source(imageStr: "newProfileIcon", title: "My Profile"),
            Source(imageStr: "shakingHands", title: "Meet me"),
            Source(imageStr: "rectangle2", title: "Sync"),
            Source(imageStr: "speedLocationIcon", title: "Location")
    ]
    var cardInputData: [Source] =
        [
            Source(imageStr: "newProfileIcon", title: "My Profile"),
            Source(imageStr: "shakingHands", title: "Meet me"),
            Source(imageStr: "rectangle2", title: "Sync"),
            Source(imageStr: "speedLocationIcon", title: "Location"),
            Source(imageStr: "lookOutHeader", title: "Lookout")
    ]
    var containerViewController: HomeVC?
    let containerSegueName = "testSegue"
    
    
    @IBOutlet var rightArrowBtn: UIButton!
    @IBOutlet var leftArrowBtn: UIButton!
    @IBOutlet var fviewdid: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var termsNConditionsBtn: UIButton!
    @IBOutlet weak var footerTitle: UILabel!
    @IBOutlet weak var footerSubTitle: UILabel!
    @IBOutlet weak var fisheyeLogo: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    //      @IBOutlet weak var menuRadialView: RadialGradientView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    //      @IBOutlet var mainView: RadialGradientViewForLightCenterNDarkCorners!
    @IBOutlet weak var floaty: Floaty!
    
    @IBOutlet var mainView: UIView!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == containerSegueName {
            containerViewController = segue.destination as? HomeVC
            containerViewController?.homeDelegate = self
        }
    }
    
    @IBAction func leftArraowTapped(_ sender: Any) {
        self.showRibbonByLeftSliding()
    }
    
    func showRibbonByLeftSliding() {
        if(self.inputData[0].title == self.profileText.myProfile) {
            self.inputData =
                [
                    Source(imageStr: "lookOutHeader", title: self.profileText.lookout),
                    Source(imageStr: "newProfileIcon", title: self.profileText.myProfile),
                    Source(imageStr: "shakingHands", title: self.profileText.meetMe),
                    Source(imageStr: "rectangle2", title: self.profileText.sync)
            ]
        } else if(self.inputData[0].title == self.profileText.meetMe) {
            self.inputData = [
                Source(imageStr: "newProfileIcon", title: self.profileText.myProfile),
                Source(imageStr: "shakingHands", title: self.profileText.meetMe),
                Source(imageStr: "rectangle2", title: self.profileText.sync),
                Source(imageStr: "speedLocationIcon", title: self.profileText.pulse)
            ]
        }
        self.collectionView.reloadData()
        
    }
    
    func showRibbonByRightSliding() {
        if(self.inputData[3].title == self.profileText.pulse) {
            self.inputData =
                [
                    Source(imageStr: "shakingHands", title: self.profileText.meetMe),
                    Source(imageStr: "rectangle2", title: self.profileText.sync),
                    Source(imageStr: "speedLocationIcon", title: self.profileText.pulse),
                    Source(imageStr: "lookOutHeader", title: self.profileText.lookout)
            ]
            
        } else if(self.inputData[3].title == self.profileText.sync) {
            self.inputData = [
                Source(imageStr: "newProfileIcon", title: self.profileText.myProfile),
                Source(imageStr: "shakingHands", title: self.profileText.meetMe),
                Source(imageStr: "rectangle2", title: self.profileText.sync),
                Source(imageStr: "speedLocationIcon", title: self.profileText.pulse)
            ]
        }
        self.collectionView.reloadData()
    }
    
    @IBAction func rightArraowTapped(_ sender: Any) {
        self.showRibbonByRightSliding()
    }
    /* ---------------------------------- Check contacts that have blocked to FEUser  ------------------------------------- */
    
    func getBlockedByContactsFromBackend() {
        let getBlockedByContacts = getBlockedByContactsAPIOperation()
        getBlockedByContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getBlockedByContacts)
    }
    
    /* ---------------------------------- Check contacts that have blocked to FEUser END ------------------------------------- */
    
    private func locationManager(manager: CLLocationManager!,
                                 didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        
        switch status {
        case CLAuthorizationStatus.restricted:
            locationStatus = "Restricted Access to location"
        case CLAuthorizationStatus.denied:
            locationStatus = "User denied access to location"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Status not determined"
        default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LabelHasbeenUpdated"), object: nil)
        if (shouldIAllow == true) {
            NSLog("Location to Allowed")
            locManager.startUpdatingLocation()
        } else {
            NSLog("Denied access: \(locationStatus)")
        }
    }
    
    private func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        locManager.stopUpdatingLocation()
        if ((error) != nil) {
            if (seenError == false) {
                seenError = true
                // v(error)
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        if (locationFixAchieved == false) {
            locationFixAchieved = true
            let locationArray = locations as NSArray
            let locationObj = locationArray.lastObject as! CLLocation
            _ = locationObj.coordinate
            self.locManager.stopUpdatingLocation()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "backgroundFE.png")
        backgroundImage.contentMode =  UIView.ContentMode.scaleAspectFill
        self.mainView.insertSubview(backgroundImage, at: 0)
        self.setLocalizationText()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.getVersion()
        self.footerTitle.text = "Fisheye Hub"
        let nibCell = UINib(nibName: "MenuBarCollectionViewCell", bundle: nil)
        self.collectionView.register(nibCell, forCellWithReuseIdentifier: "MenuBarCollectionViewCell")
        self.collectionView.backgroundColor = UIColor.clear
        if(NetworkStatus.sharedManager.isNetworkReachable()) {
            self.getUpdatedContactsFromBackend()
            self.getBlockedByContactsFromBackend()
        }
        
        self.navigationController?.navigationBar.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapOnLogo))
        fisheyeLogo.addGestureRecognizer(gesture)
        fisheyeLogo.isUserInteractionEnabled = true
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        
        self.layoutFAB()
        
        // to open pulse logbook from insertCapsule popUp & master key popUp
        if isToOpenPulse {
            self.toRemoveSubView()
            self.menuNameSelection = "pulse"
            self.pulsePage()
        }
        
        // to open add contact from add contact of requester to respond to his pulse
        if isToOpenAddContact {
            self.toRemoveSubView()
            self.menuNameSelection = "sync"
            self.syncPage()
        }
        
        if self.isFromMeetMeNotification {
            self.toRemoveSubView()
            self.menuNameSelection = "meetMe"
            self.meetMePage()
        }
        
        if self.isFromCapsuleChatNotification {
            self.toRemoveSubView()
            self.menuNameSelection = "meetMe"
            self.meetMePage()
        }
        
        if self.isFromPulseNotification {
            self.toRemoveSubView()
            self.menuNameSelection = "pulse"
            self.pulsePage()
            
        }
        
        self.showNotificationsPermissionsPopup()
        self.addObservers()
        if self.appService.noOfCarouselCards > 4{
            self.rightArrowBtn.isUserInteractionEnabled = true
            self.leftArrowBtn.isUserInteractionEnabled = true
        }else{
            self.rightArrowBtn.isUserInteractionEnabled = false
            self.leftArrowBtn.isUserInteractionEnabled = false
        }
    }
    
    
    func showNotificationsPermissionsPopup(){
//        UIView.transition(with: self.view, duration: 20.0, options: .transitionCrossDissolve, animations: { _ in

            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound, .carPlay]){(success,  error) in
                if error == nil{
                    
                    DispatchQueue.main.async {
//                        UIView.animate(withDuration: TimeInterval(20.0), animations: {
//
//                        UIApplication.shared.registerForRemoteNotifications()
//                        })
                            UIApplication.shared.registerForRemoteNotifications()
                        
                    }
                }
            }
//            }, completion: nil)
    }
    
    // Add all observers here
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
        
        NotificationCenter.default.addObserver(forName: LOAD_SYNC_BASE_VIEW, object: nil, queue: nil) { (_) in
            if self.menuNameSelection != "sync" {
                self.toRemoveSubView()
                self.menuNameSelection = "sync"
                self.syncPage()
            }
        }
        
        NotificationCenter.default.addObserver(forName: LOAD_PULSE_BASE_VIEW, object: nil, queue: nil) { (_) in
            self.toRemoveSubView()
            self.menuNameSelection = "pulse"
            self.pulsePage()
        }
        
        NotificationCenter.default.addObserver(forName: LOAD_MEET_BASE_VIEW, object: nil, queue: nil) { (_) in
            if self.menuNameSelection != "meetMe" {
                self.toRemoveSubView()
                self.menuNameSelection = "meetMe"
                self.meetMePage()
            }
        }
    }
    
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.footerSubTitle.text = self.profileText.letsWorkTogether
            self.cardInputData =
                [
                    Source(imageStr: "newProfileIcon", title: self.profileText.myProfile),
                    Source(imageStr: "shakingHands", title: self.profileText.meetMe),
                    Source(imageStr: "rectangle2", title: self.profileText.sync),
                    Source(imageStr: "speedLocationIcon", title: self.profileText.pulse),
                    Source(imageStr: "lookOutHeader", title: self.profileText.lookout)
            ]
            
            if self.inputData[0].title == self.profileText.lookout {
                self.showRibbonByLeftSliding()
            } else if self.inputData[3].title == self.profileText.lookout {
                self.showRibbonByRightSliding()
            } else {
                self.inputData =
                    [
                        Source(imageStr: "newProfileIcon", title: self.profileText.myProfile),
                        Source(imageStr: "shakingHands", title: self.profileText.meetMe),
                        Source(imageStr: "rectangle2", title: self.profileText.sync),
                        Source(imageStr: "speedLocationIcon", title: self.profileText.pulse)
                ]
            }
            self.collectionView.reloadData()
        }
    }
    
    func getVersion() {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            if let bundle = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                self.version = self.appService.version + version + " (" + bundle + ")"
            }
        }
    }
    
    deinit {
    }
    
    func openLogbook() {
        let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
        pulseLogbookViewcontroller = storyboard.instantiateViewController(withIdentifier: "PulseListingViewController") as! PulseListingViewController
        pulseLogbookViewcontroller.view.frame =  self.containerView.bounds
        pulseLogbookViewcontroller.view.isUserInteractionEnabled = true
        self.addChild(pulseLogbookViewcontroller)
        pulseLogbookViewcontroller.didMove(toParent: self)
        self.floaty.isHidden = true
        pulseLogbookViewcontroller.view.frame.origin.y = -self.view.frame.size.height
        self.containerView.addSubview(pulseLogbookViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: {
            self.pulseLogbookViewcontroller.view.frame.origin.y = self.containerView.bounds.origin.y
        }, completion: nil)
    }
    
    func logbookNavigate(title: String) {
        for (_, source) in inputData.enumerated() {
            if source.title == title {
                let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
                pulseLogbookViewcontroller = storyboard.instantiateViewController(withIdentifier: "PulseListingViewController") as! PulseListingViewController
                pulseLogbookViewcontroller.view.frame =  self.containerView.bounds
                pulseLogbookViewcontroller.view.isUserInteractionEnabled = true
                self.addChild(pulseLogbookViewcontroller)
                pulseLogbookViewcontroller.didMove(toParent: self)
                self.floaty.isHidden = true
                pulseLogbookViewcontroller.view.frame.origin.y = -self.view.frame.size.height
                self.containerView.addSubview(pulseLogbookViewcontroller.view)
                
                UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: {
                    self.pulseLogbookViewcontroller.view.frame.origin.y = self.containerView.bounds.origin.y
                }, completion: nil)
            }
        }
    }
    
    
    func navigateToViewWith(title: String) {
        for (index, source) in cardInputData.enumerated() {
            if source.title == title {
                cellTappedAt(index: index)
            }
        }
    }
    
    @objc func didTapOnLogo() {
        if addedSubview {
            updateParent()
            if let syncVc = syncViewContoller {
                syncVc.view.removeFromSuperview()
                syncVc.removeFromParent()
                syncViewContoller = nil
            }
            
            if let profileVc = profileBaseviewController {
                profileVc.view.removeFromSuperview()
                profileVc.removeFromParent()
            }
            
            if let pulseVc = pulseBaseViewcontroller {
                pulseVc.view.removeFromSuperview()
                pulseVc.removeFromParent()
            }
            
            if let meetMeVC = meetMeBaseViewcontroller {
                meetMeVC.view.removeFromSuperview()
                meetMeVC.removeFromParent()
            }
            
            if let syncVC = syncBaseViewController {
                syncVC.view.removeFromSuperview()
                syncVC.removeFromParent()
            }
            
            if let lookoutVC = lookoutBaseVCViewController {
                lookoutVC.view.removeFromSuperview()
                lookoutVC.removeFromParent()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ScreenLoader.shared.stopLoader()
        self.navigationController?.navigationBar.isHidden = true
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.BaseViewController)
          self.bottomConstraint = NSLayoutConstraint(item: self.floaty, attribute: .bottom, relatedBy: .equal, toItem: mainView, attribute: .bottom, multiplier: 1.0, constant: 12)
    }
    
    func createGradientLayer(inView view: UIView, colors: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        let gradientLocations: [NSNumber] = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        gradientLayer.colors = colors
        gradientLayer.locations = gradientLocations
        view.layer.addSublayer(gradientLayer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //      layoutFAB()
    }
    
    /* Created by venkatesh modified by vaishali  */
    func applyBtnShadow() {
        // shadow
        //            self.menuRadialView.layer.shadowColor = UIColor.black.cgColor
        //            self.menuRadialView.layer.shadowOffset = CGSize(width: 3, height: 3)
        //            self.menuRadialView.layer.shadowOpacity = 0.4
        //            self.menuRadialView.layer.shadowRadius = 5.0
    }
    /* END */
    
    func updateParent() {
        self.floaty.isHidden = false
    }
    
    func profilePage() {
        self.hideBottomviews()
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        self.profileBaseviewController = storyboard.instantiateViewController(withIdentifier: "ProfileBaseViewController") as! ProfileBaseViewController
       self.profileBaseviewController.view.frame = self.containerView.bounds
        self.addChild(self.profileBaseviewController)
        self.profileBaseviewController.didMove(toParent: self)
        self.floaty.isHidden = true
        self.profileBaseviewController.delegate = self

        self.containerView.addSubview(self.profileBaseviewController.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.profileBaseviewController.view.frame.origin.y = self.containerView.bounds.origin.y
        }, completion: nil)
        
    }
    
    func meetMePage() {
        self.unHideBottomviews()
        let storyboard = UIStoryboard.init(name: "Meetme", bundle: nil)
        self.meetMeBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "MeetMeBaseViewController") as! MeetMeBaseViewController
        if self.isFromMeetMeNotification {
            self.meetMeBaseViewcontroller.isFromNotification = self.isFromMeetMeNotification
            self.meetMeBaseViewcontroller.notificationMeetMeId = self.notificationMeetMeId
        }else if self.isFromCapsuleChatNotification{
            self.meetMeBaseViewcontroller.isFromCapsuleChatNotification = self.isFromCapsuleChatNotification
            self.meetMeBaseViewcontroller.notificationMeetMeId = self.notificationMeetMeId
            self.meetMeBaseViewcontroller.isCapsuleLocked = self.isCapsuleLocked
            self.meetMeBaseViewcontroller.meetTitle = self.meetTitle
        }
        self.meetMeBaseViewcontroller.view.frame = self.containerView.bounds
        self.addChild(self.meetMeBaseViewcontroller)
        self.meetMeBaseViewcontroller.didMove(toParent: self)
        self.floaty.isHidden = true
        self.containerView.addSubview(self.meetMeBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.meetMeBaseViewcontroller.view.frame.origin.y = self.containerView.bounds.origin.y
        }, completion: nil)
        self.isFromMeetMeNotification = false
        self.notificationMeetMeId = ""
    }
    
    func loadMeetMeIntroScreen1() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MeetMePageViewController") as! MeetMePageViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadSyncIntroScreen1() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SyncPageViewController") as! SyncPageViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func loadPulseIntroScreen1() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PulsePageViewController") as! PulsePageViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func syncPage() {
        self.hideBottomviews()
        let storyboard = UIStoryboard.init(name: "Sync", bundle: nil)
        self.syncBaseViewController = storyboard.instantiateViewController(withIdentifier: "SyncBaseViewController") as! SyncBaseViewController
        if isToOpenAddContact {
            self.syncBaseViewController?.isToOpenAddContact = true
        }
        self.syncBaseViewController?.view.frame =  self.containerView.bounds
        self.syncBaseViewController?.view.isUserInteractionEnabled = true
        
        self.addChild(self.syncBaseViewController!)
        self.syncBaseViewController?.didMove(toParent: self)
        self.floaty.isHidden = true
        self.syncBaseViewController?.view.backgroundColor = UIColor.clear
        self.syncBaseViewController?.view.frame.origin.x = 0
        self.containerView.addSubview(self.syncBaseViewController!.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: { self.syncBaseViewController?.view.frame.origin.y = self.containerView.bounds.origin.y
        }, completion: nil)
    }
    
    func pulsePage() {
        self.unHideBottomviews()
        let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
        pulseBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "PulseListingViewController") as! PulseListingViewController
        if self.isFromPulseNotification {
            self.pulseBaseViewcontroller.isFromNotification = self.isFromPulseNotification
            self.pulseBaseViewcontroller.notificationPulseId = self.notificationPulseId
        }
        pulseBaseViewcontroller.view.frame = self.containerView.bounds
        self.addChild(pulseBaseViewcontroller)
        pulseBaseViewcontroller.didMove(toParent: self)
        self.floaty.isHidden = true
        self.hideBottomviews()
        self.containerView.addSubview(self.pulseBaseViewcontroller.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.pulseBaseViewcontroller.view.frame.origin.y = self.containerView.bounds.origin.y
        }, completion: nil)
    }
    
    func toRemoveSubView() {
        self.containerView.subviews.forEach({ (subview) in
            subview.removeFromSuperview()
        })
    }
    
    func layoutFAB() {
        floaty.hasShadow = true
        floaty.buttonImage = UIImage.init(named: "more")
        floaty.addItem(icon: UIImage(named: "shareFab"), handler: { (_) in
            self.openShareVC()
        })
        floaty.addItem(icon: UIImage(named: "contactUs"), handler: { (_) in
            self.loadContactUsPopUp()
        })
        floaty.addItem(icon: UIImage(named: "appInfo"), handler: { (_) in
            self.navigateToAppInfo()
        })
        floaty.addItem(icon: UIImage(named: "logoutFab"), handler: { (_) in
            DispatchQueue.main.async {
                self.handleLogout()
            }
        })
        floaty.fabDelegate = self
        self.bottomConstraint = NSLayoutConstraint(item: self.floaty, attribute: .bottom, relatedBy: .equal, toItem: mainView, attribute: .bottom, multiplier: 1.0, constant: 12)
        self.view.addSubview(floaty)
    }
    
    func navigateToAppInfo() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "AppInfo") as! AppInfo
        nextVC.version = self.version
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func floatyWillOpen(_ floaty: Floaty) {
    }
    
    func floatyDidOpen(_ floaty: Floaty) {
    }
    
    func floatyWillClose(_ floaty: Floaty) {
    }
    
    func floatyDidClose(_ floaty: Floaty) {
    }
    
    func openShareVC() {
        let activityVC = UIActivityViewController(activityItems: ["https://itunes.apple.com/gb/app/fish-eye/id1286294135?mt=8"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func loadContactUsPopUp() {
        DispatchQueue.main.async{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            self.present(nextVC, animated: true, completion: nil)
        }
    }
    
    func cellTappedAt(index: Int) {
        self.collectionView.reloadData()
        addedSubview = true
        switch index {
        case 0:
            if self.menuNameSelection != "profile" {
                self.selectedIndex = self.profileText.myProfile
                self.toRemoveSubView()
                self.menuNameSelection = "profile"
                self.profilePage()
            }
            
            break
        case 1:
            
            if self.menuNameSelection != "meetMe" {
                self.selectedIndex = self.profileText.meetMe
                self.toRemoveSubView()
                //                let meetMeIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "meetMeIntroductoryVariableIndex") as? String ?? "0"
                //                self.appSharedPrefernce.setAppSharedPreferences(key: "meetMeIntroductoryVariableIndex", value: "1")
                //                  if(meetMeIntroductoryVariableIndex == "0") {
                //                        self.loadMeetMeIntroScreen1()
                //                  } else {
                self.menuNameSelection = "meetMe"
                self.meetMePage()
                //                  }
            }
            
            break
        case 2:
            if self.menuNameSelection != "sync" {
                self.selectedIndex = self.profileText.sync
                self.toRemoveSubView()
                //                 let syncIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "syncIntroductoryVariableIndex") as? String ?? "0"
                //                self.appSharedPrefernce.setAppSharedPreferences(key: "syncIntroductoryVariableIndex", value: "1")
                
                //                if(syncIntroductoryVariableIndex == "0") {
                //                     self.loadSyncIntroScreen1()
                //                } else {
                self.menuNameSelection = "sync"
                self.syncPage()
                //                }
            }
            break
            
        case 3:
            if self.menuNameSelection != "pulse" {
                self.selectedIndex = self.profileText.pulse
                self.toRemoveSubView()
                //                  let pulseIntroductoryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "pulseIntroductoryVariableIndex") as? String ?? "0"
                //                  self.appSharedPrefernce.setAppSharedPreferences(key: "pulseIntroductoryVariableIndex", value: "1")
                //                if(pulseIntroductoryVariableIndex == "0") {
                //                  self.loadPulseIntroScreen1()
                //                } else {
                self.menuNameSelection = "pulse"
                self.pulsePage()
                //                }
            }
            
            break
            
        case 4:
            if self.menuNameSelection != self.profileText.lookout {
                self.selectedIndex = self.profileText.lookout
                self.toRemoveSubView()
                self.menuNameSelection = self.profileText.lookout
                self.lookoutPage()
            }
            break
        default:
            break
        }
    }
    
    func lookoutPage() {
        self.unHideBottomviews()
        self.fisheyeLogo.isHidden = true
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        self.lookoutBaseVCViewController = storyboard.instantiateViewController(withIdentifier: "LookoutBaseVC") as! LookoutBaseVC
        self.lookoutBaseVCViewController.view.frame = self.containerView.bounds
        self.addChild(self.lookoutBaseVCViewController)
        self.lookoutBaseVCViewController.didMove(toParent: self)
        self.floaty.isHidden = true
        self.containerView.addSubview(self.lookoutBaseVCViewController.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.lookoutBaseVCViewController.view.frame.origin.y = self.containerView.bounds.origin.y
        }, completion: nil)
    }
    
    func handleLogout() {
        self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: self.profileText.logout)
        self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: self.profileText.logoutConfirmation)
        self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: "success")
        self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: "logout")
        self.appSharedPrefernce.setAppSharedPreferences(key: "prevaction", value: "dismissMethod")
        self.popUpVCController()
    }
    
    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "ActionPopUpViewController")
        self.present(nextVC, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func unHideBottomviews() {
        footerTitle.isHidden = false
        //        footerSubTitle.isHidden = false
        fisheyeLogo.isHidden = false
    }
    
    func hideBottomviews() {
        footerTitle.isHidden = true
        //        footerSubTitle.isHidden = true
        fisheyeLogo.isHidden = true
    }
    
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            store.requestAccess(for: .contacts) { granted, _ in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Would you like to open settings and grant permission to contacts?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { _ in
            completionHandler(false)
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
            completionHandler(false)
        })
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func getUpdatedContactsFromBackend() {
        let getUpdatedContacts = getUpdatedContactsAPIOperation()
        getUpdatedContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getUpdatedContacts)
    }
    
    func applyShadow() {
        self.view.layer.shadowColor = UIColor.lightGray.cgColor
        self.view.layer.shadowOpacity = 1
        self.view.layer.shadowOffset = CGSize.zero
        self.view.layer.shadowRadius = 5
        self.view.layer.shouldRasterize = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (collectionView.frame.width/CGFloat(self.appService.noOfTabsToShowInTopRibbon)), height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView.frame.width - CGFloat((collectionView.frame.width/CGFloat(self.appService.noOfTabsToShowInTopRibbon)) * CGFloat(self.appService.noOfTabsToShowInTopRibbon))
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.appService.noOfTabsToShowInTopRibbon
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuBarCollectionViewCell", for: indexPath) as! MenuBarCollectionViewCell
        
        cell.imageView.image = self.inputData[indexPath.row].image
        
        cell.titleLabel.text  =  self.inputData[indexPath.row].title
        
        if cell.titleLabel.text == self.selectedIndex {
            cell.selectedView.isHidden = false
        } else {
            cell.selectedView.isHidden = true
        }
        
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuBarCollectionViewCell", for: indexPath) as! MenuBarCollectionViewCell
        var index: Int = 0
        
        let title = self.inputData[indexPath.row].title
        
        switch title {
        case self.profileText.myProfile?:
            index = 0
            break
        case self.profileText.meetMe?:
            
            index = 1
            break
        case self.profileText.sync?:
            
            index = 2
            break
            
        case self.profileText.pulse?:
            
            index = 3
            break
            
        case self.profileText.lookout?:
            
            index = 4
            cell.selectedView.backgroundColor = UIColor.white
            break
            
        default:
            break
        }
        
        self.cellTappedAt(index: index)
    }
    
}

//Extension to the class to reload the profile data after pivacy update
extension BaseViewController: RedirectToDashboard {
    func redirectDashboard(toRedirect: Bool) {
        if toRedirect == true {
        }
    }
}
//End of Extension to the class to reload the profile data after pivacy update

class RadialGradientViewForLightCenterNDarkCorners: UIView {
    
    private let gradientLayer = gradientLayerForLightCenterNDarkAtCorners()
    
    public var colors: [UIColor] {
        get {
            return gradientLayer.colors
        }
        set {
            gradientLayer.colors = newValue
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if gradientLayer.superlayer == nil {
            layer.insertSublayer(gradientLayer, at: 0)
        }
        gradientLayer.frame = bounds
    }
    
}

class gradientLayerForLightCenterNDarkAtCorners: CALayer {
    
    var center: CGPoint {
        return CGPoint(x: bounds.width/2, y: bounds.height/2)
    }
    
    var radius: CGFloat {
        return (bounds.width + bounds.height)/2
    }
    
    var colors: [UIColor] = [UIColor.contactsTypeBg(), UIColor.white] {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var cgColors: [CGColor] {
        return colors.map({ (color) -> CGColor in
            return color.cgColor
        })
    }
    
    override init() {
        super.init()
        //            needsDisplayOnBoundsChange = true
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //      required init(coder aDecoder: NSCoder) {
    //            super.init()
    //      }
    
    override func draw(in ctx: CGContext) {
        ctx.saveGState()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let locations: [CGFloat] = [1.0, 0.0]
        guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
            return
        }
        ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
    }
}
