//
//  PulseUserSearchViewController.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 04/09/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class PulseUserSearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var mainview: UIView!

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchTextfield: DesignableUITextField!
    var contatcs: [FEContact]?
    var searchContatcs: [FEContact]?

    var searchActive: Bool = false
    var search: String=""
    var isshare: Bool = false

    @IBAction func backclick(_ sender: Any) {

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        applyViewShadow()

        tableview.delegate = self

        getContacts()
        searchTextfield.delegate = self
        // Do any additional setup after loading the view.
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            search = String(search.characters.dropLast())
        } else {
            search=textField.text!+string
        }
        //print(search)

//        let arr=(AllData as NSArray).filtered(using: predicate)
//        
//        if arr.count > 0
//        {
//            SearchData.removeAll(keepingCapacity: true)
//            SearchData=arr as! Array<Dictionary<String,String>>
//        }
//        else
//        {
//            SearchData=AllData
//        }

        if(self.contatcs != nil) {
        let arr = self.contatcs!.filter({( aSpecies: FEContact) in
            // to start, let's just search by name
            return aSpecies.name!.lowercased().contains(search.lowercased())
        })

        searchContatcs?.removeAll()

        if(arr.count > 0) {
            searchActive = false
            searchContatcs = arr
        } else {
            searchContatcs = contatcs
            searchActive = true
        }

        tableview.reloadData()
        }
        return true
    }
    func getContacts() {
        WebServiceManager.shared().getSyncContacts { (result, _, success) in
            if success {
                DispatchQueue.main.async {
                    CommonMethods.hideProgressView()
                    let res = result as! FEDisplayContactsResponse
                    if let contact = self.contatcs {
                        if (res.contacts?.count)! > contact.count {
                            self.contatcs =  res.contacts
                            self.searchContatcs = res.contacts
//                            self.countLbl.text = "\(contact.count)"
                            self.tableview.reloadData()
                        }
                    } else {
                        self.contatcs = res.contacts
                        self.searchContatcs = res.contacts
//                        self.countLbl.text = "\(self.contatcs!.count)"
                        self.tableview.reloadData()

                    }
                }
            }
        }
    }

    func requestPulse(pulserequest: FEPulseRequestNow) {

        WebServiceManager.shared().pulseRequest(fepulserequest: pulserequest) { (result, _, success) in
            if success {
                DispatchQueue.main.async {
                    CommonMethods.hideProgressView()
                    let res = result as! FEResponse

//                    if let contact = self.contatcs {
//                        if (res.contacts?.count)! > contact.count {
//                            self.contatcs =  res.contacts
//                            //                            self.countLbl.text = "\(contact.count)"
//                            self.tableview.reloadData()
//                        }
//                    }
//                    else{
//                        self.contatcs = res.contacts
//                        //                        self.countLbl.text = "\(self.contatcs!.count)"
//                        self.tableview.reloadData()
//                        
//                    }
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func applyViewShadow() {
   //     self.mainview.layer.cornerRadius = 8
//        
//        // border
//        self.mainview.layer.borderWidth = 0
//        self.mainview.layer.borderColor = UIColor.black.cgColor
//        //        self.mainview.clipsToBounds = true
//        
//        // shadow
//        self.mainview.layer.shadowColor = UIColor.black.cgColor
//        self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
//        self.mainview.layer.shadowOpacity = 0.7
//        self.mainview.layer.shadowRadius = 10.0

//        self.mainview.layer.borderWidth = 1
//        self.mainview.layer.borderColor = UIColor.black.cgColor

        self.mainview.layer.cornerRadius = 8

        // border
        self.mainview.layer.borderWidth = 0
        self.mainview.layer.borderColor = UIColor.black.cgColor
        //        self.mainview.clipsToBounds = true

        // shadow
        self.mainview.layer.shadowColor = UIColor.black.cgColor
        self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.mainview.layer.shadowOpacity = 0.7
        self.mainview.layer.shadowRadius = 10.0
        //self.searchTextfield.layer.cornerRadius = 8

        // border
//        self.searchTextfield.layer.borderWidth = 0
//        self.searchTextfield.layer.borderColor = UIColor.black.cgColor
//        //        self.mainview.clipsToBounds = true
//        
//        // shadow
//        self.searchTextfield.layer.shadowColor = UIColor.black.cgColor
//        self.searchTextfield.layer.shadowOffset = CGSize(width: 3, height: 3)
//        self.searchTextfield.layer.shadowOpacity = 0.7
//        self.searchTextfield.layer.shadowRadius = 10.0

        self.searchTextfield.layer.masksToBounds = false
        self.searchTextfield.layer.shadowRadius = 1.0
        self.searchTextfield.layer.shadowColor = UIColor.black.cgColor
        self.searchTextfield.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.searchTextfield.layer.shadowOpacity = 1.0

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchContatcs != nil ? self.searchContatcs!.count : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

           let cell = self.tableview.dequeueReusableCell(withIdentifier: "PulseSearchTableViewCell", for: indexPath)as! PulseSearchTableViewCell
        cell.selectionStyle = .none

        let contact =  self.searchContatcs?[indexPath.row]
        if((contact?.isfisheye) != nil && contact?.isfisheye == true) {
            cell.fisheeyelogoimg.isHidden = false
        } else {
            cell.fisheeyelogoimg.isHidden = true

        }

        cell.nameLabel.text = (contact?.name)!

        return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        view.endEditing(true)

        let contact =  self.searchContatcs?[indexPath.row]
        if((contact?.isfisheye) != nil && contact?.isfisheye == true) {
            let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
            let profileBaseviewController = storyboard.instantiateViewController(withIdentifier: "PulseRequestViewController") as! PulseRequestViewController
            //
            profileBaseviewController.isshare = self.isshare
            profileBaseviewController.selectedName = (self.searchContatcs?[indexPath.row].name)!
            profileBaseviewController.selectedPhoneNumber = (self.searchContatcs?[indexPath.row].phoneNumber)!
            profileBaseviewController.selectedContactID = (self.searchContatcs?[indexPath.row].contactId)!

            profileBaseviewController.view.frame = self.view.bounds
            self.addChildViewController(profileBaseviewController)
            profileBaseviewController.didMove(toParentViewController: self)

            //        //  profileBaseviewController.delegate = self
            //
            profileBaseviewController.view.frame.origin.y = -self.view.frame.size.height
            //
            //

            self.view.addSubview(profileBaseviewController.view)
            //
            //
            UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: { _ in
                profileBaseviewController.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)

        } else {
            let alert = UIAlertController(title: "Not a Fisheye user" ,
                                          message: "Invite \(contact?.name! ?? "")  to fisheye" ,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Invite", style: .default, handler: {(_: UIAlertAction!) in

            }))

            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {(_: UIAlertAction!) in""

            }))

            self.present(alert, animated: true, completion: nil)

        }

        //\(CommonMethods.retriveIdToken())

//        let pulserequest = FEPulse()
//        pulserequest?.email = "venkimurthy07@gmail.com"
//        
//        pulserequest?.isrequest = 1
//        pulserequest?.message = "hi"
//        pulserequest?.phonenumber = "+919972463668"
//        pulserequest?.isencrypted = 1
//        pulserequest?.latitude = ""
//        pulserequest?.longitude = ""
//        
//        requestPulse(pulserequest: pulserequest!)
//        print("\(searchContatcs?[indexPath.row].email)")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
