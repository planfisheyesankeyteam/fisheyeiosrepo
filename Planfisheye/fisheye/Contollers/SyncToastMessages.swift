//
//  SyncToastMessages.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 16/04/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

class SyncToastMessages {
    
    static let shared: SyncToastMessages = SyncToastMessages()
    
    // Toast Timers //
    var syncToastMaxTimeInterval: Double = 300.0
    var syncToastThirdTimeInterval: Double = 180.0
    // -------------------------------------------------------------    Toast Messages   --------------------------------------------------------------- //
    
    // Common Toast Messages //
    
    // to show syncing is in progress in the background.
    var syncInProgress: String = "Please bear with us, while we are syncing your contacts"
    
    // to show fetching of contacts from iphone has completed and contacts will send to the database.
    var frontendSyncCompleted: String = "Please bear with us, while we are syncing your contacts"
    
    // to show contacts syncing process fully completed.
    var syncCompleted: String = "Your Sync is now updated"
    
    // to show sync is already in the progress in the background.
    // scenario :- when sync is in proress and user taps on the sync button
    var waitSyncInProgress: String = "Please bear with us, while we are syncing your contacts"
    
    // Toast messages on Sync Contacts listing page //
    
    // to show no contacts has found from user's iphone. So User can add contact in sync.
    var noContactsFound: String = "No contacts retrieved from your phone. But you may add your contacts manually"
    
    // to show no contacts has found from user's iphone. So User can add contact in sync.
    var areUSureToSync: String = "Do you wish to sync your iphone contacts to Fisheye? This will enable you to use Sync app effectivelly"
    
    // to show no contacts found for the text entered by user for searching.
    // scenario :- when no contacts found by searching.
    var noContactsFoundFromSearch: String = "No matching contact found in your Sync"
    
    // to take confirmation from user whether user really wants to block the contact
    // comment : - contact name will get appended after this messages when contact name will be non empty
    var areUSureToBlock: String = "Are you sure you wish to block it from your Sync"
    
    // to take confirmation from user whether user really wants to block the contact.
    // comment : - when contact name will be empty this generalised text will be shown.
    var areUSureToBlockContact: String = "Are you sure you wish to block it on your Sync? This will prevent all future comms with this Sync contact ?"
    
    // to take confirmation from user whether user really wants to unblock the contact
    // comment : - contact name will get appended after this messages when contact name will be non empty
    var areUSureToUnblock: String = "Please confirm that you wish to unblock this Sync contact"
    
    // to take confirmation from user whether user really wants to unblock the contact.
    // comment : - when contact name will be empty this generalised text will be shown.
    var areUSureToUnblockContact: String = "Please confirm that you wish to unblock this Sync contact?"
    
    // to take confirmation from user whether user really wants to delete the contact.
    // comment : - when contact name will be empty this generalised text will be shown.
    var areUSureToDeleteContact: String = "Please confirm that you wish to delete this Sync contact ?"
    
    // to take confirmation from user whether user really wants to delete the contact
    // comment : - contact name will get appended after this messages when contact name will be non empty
    var areUSureToDelete: String = "Please confirm that you wish to delete this Sync contact"
    
    // to take confirmation from user whether user really wants to merge the contact
    // comment : - contact name will get appended after this messages when contact name will be non empty
    var areUSureToMergeContact: String = "Please confirm that you wish to merge these two contacts"
    
    // scenario :- used between two contact names while merging contacts, when contact name will be non empty.
    var with: String = "with"
    
    // to take confirmation from user whether user really wants to merge the contact.
    // comment : - when contact name will be empty this generalised text will be shown.
    var areUSureToMeregeTheseContacts: String = "Please confirm that you wish to merge these two contacts?"
    
    // to show there is no internet to unblock the contact
    var noInternetToUnblock: String = "Please check your network connection and try again"
    
    // to show there is no internet to block the contact
    var noInternetToBlock: String = "Please check your network connection and try again"
    
    // to show some error has occured while deleting the contact
    var failedToDelete: String = "We are experiencing higher than expected traffic, and unable to delete this contact. Please bear with us, and try again in a few minutes"
    
    // scenario :- when user has blocked particular contact  and trying to call that contact after blocking. user has to unblock the contact for calling.
    // comment :- 'Unblock' text and then contact name will be preceding this text.
    // eg :- Unblock Anuradha to place a call.
    var toPlaceCall: String = "if you wish to make this call"
    
    // scenario :- When user tries to call by native ios calling from Fisheye app. But phone number is absent in the contact details.
    var phoneRequired: String = "No phone number available for this contact!"
    
    // scenario :- When email and phone number both are absent in the contact details and user tries to block that contact.
    var phoneOrEmailRequired: String = "No phone number or email address is available for this contact!"
    
    // scenario :- when user has blocked particular contact  and trying to call that contact after blocking. user has to unblock the contact for calling.
    // comment :- 'Unblock' text and then contact name will be preceding this text.
    var toPlaceFacetimeCall: String = "if you wish to make this Facetime call"
    
    // scenario :- when user has blocked particular contact  and trying to call that contact after blocking. user has to unblock the contact for calling.
    // comment :- 'Unblock' text and then contact name will be preceding this text.
    var toPlaceWhatsAppCall: String = "if you wish to make this Whatsapp call"
    
    // scenario :- When some error occures while blocking the contact.
    var failedToBlockContact: String = "We are experiencing higher than expected traffic, and unable to block this contact. Please bear with us, and try again in a few minutes"
    
    // scenario :- When some error occures while unblocking the contact.
    var  failedToUnblocContact: String = "We are experiencing higher than expected traffic, and unable to unblock this contact. Please bear with us, and try again in a few minutes"
    
    // scenario :- When some error occures while syncing contacts.
    var failedToSyncContacts: String = "We are experiencing higher than expected traffic, and unable to Sync this contact. Please bear with us, and try again in a few minutes"
    
    // scenario :- When some error occures while fetching the contacts from database.
    var errorWhileFetchingContact: String = "We are experiencing higher than expected traffic, and unable to Sync. Please bear with us, and try again in a few minutes"
    
    // Toast messages on add contact's pages //
    var notAllowedToAddSelfPhoneAndEmailInSync: String = "You may not add your own phone number or email address to a Sync contact"
    var contactAlreadyExists: String = "This Contact already exists on Sync by Phone Number or Email id"
    var contactAlreadyExistsByPhoneOrEmail: String = "This Contact already exists on Sync by Phone Number or Email id"
    var failedToAddContact: String = "Unable to add contact"
    
    // Add contact postal info page & Update contact postal info page
    var emptyAddressViewtext: String = "You may add multiple addresses such as home, office or other address"
    
    // Toast message all pages of add contact
    var addContactPersonalInfo: String = "You may want to add personal information for this contact"
    
    // Toast message on all pages of update contact
    var failedToUpdateContact: String = "We are experiencing higher than expected traffic, and unable to Sync. Please bear with us, and try again in a few minutes"
    
    var areUSureToDeleteAddress: String = "Please confirm that you wish to delete this Sync contact's address"
    
    var areUSureToDisablePrivacyForContact: String = "You want this contact to be able to see your personal information on Fisheye Hub?"
    
    var areUSureToEnablePrivacyForContact: String = "You want your personal information on Fisheye Hub kept private for this contact?"
    
    var confirmationOfPrivacyWillDisabledInProfile: String = "Are you sure you want to disable. This is the last contact that was enabled for privacy. This action will result in your profile privacy disabled"
    
    var confirmationOfPrivacyWillEnabledInProfile: String = "Are you sure you want to enable. This is the last contact that was disabled for privacy. This action will result in your profile privacy enabled"
    // ---------------------------------------------------------  End of toast messages   ------------------------------------------------------------ //
    
    // ---------------------------------------------------------------    Labels   -------------------------------------------------------------------- //
    
    // Sync Contacts listing page //
    var syncContacts: String = "Sync Contacts"
    var fishEyeContacts: String = "Contacts on Fisheye"
    var phoneContacts: String = "Contacts from Phone"
    var recentlyAddedContacts: String = "Recently Added"
    var call: String = "Call"
    var whatsAppCall: String = "WhatsApp Call"
    var pulseRequest: String = "Location Request"
    var pulseShare: String = "Location Share"
    var facetimeCall: String = "Facetime Call"
    var blockContact: String = "Block Contact"
    var unblockContact: String = "Unblock Contact"
    var mergeContact: String = "Merge Contact"
    var deleteContact: String = "Delete Contact"
    var ok: String = "OK"
    var cancel: String = "Cancel"
    var unblock: String = "Unblock"
    var sync: String = "Sync"
    var delete: String = "Delete"
    // Add Contact page //
    var addContact: String = "Add Contact"
    
    // Common labels in Add contact & update contact pages //
    var name: String = "Name"
    var phone: String = "Mobile Number"
    var email: String = "Email"
    var addAddress: String = "Add Address"
    var deleteAddress: String = "Delete Address"
    var address: String = "Address"
    var companyName: String = "Company Name"
    var title: String = "Title"
    var website: String = "Website"
    var notes: String = "Notes"
    var submit: String = "Submit"
    var personal: String = "Personal"
    var postal: String = "Postal"
    var professional: String = "Professional"
    var general: String = "General"
    var privacyChange: String = "Privacy Change"
    var showPrivacy: String = "Show"
    var keepPrivate: String = "Keep private"
    var addPhone: String = "Add Phone"
    var addEmail: String = "Add Email"
    
    // Add / Edit address Pop-up
    var pincode: String = "Postcode"
    var state: String = "State"
    var country: String = "Country"
    var city: String = "City"
    var addressLine1: String = "Address Line 1"
    var addressLine2: String = "Address Line 2"
    var addressType: String = "Address Type"
    var home: String = "Home"
    var work: String = "Work"
    var homeImg: String = "Home"
    

    
    // ---------------------------------------------------------------  End of Labels   ---------------------------------------------------------------- //
    
    // ----------------------------------------------------------------    Validations   ---------------------------------------------------------------- //
    
    // Add contact personal info page & Update contact personal info page //
    var enterName: String = "Enter Name"
    var invalidPhone: String = "Invalid Mobile number"
    var invalidEmail: String = "Invalid email address"
    
    // Add contact general info page & Update contact general info page //
    var enterWebsite: String = "Enter Website"
    var enterNotes: String = "Enter Notes"
    
    // Add / Edit address pop-up
    var selectAddressType: String = "Select Address Type"
    var enterAddressType: String = "Enter Address Type"
    var enterAddressLine1: String = "Enter Addressline 1"
    var enterCity: String = "Enter City"
    var enterState: String = "Enter State"
    var enterPincode: String = "Enter Pin Code"
    var enterCountry: String = "Enter Country"
    var invalidPinCode: String = "Invalid Pin Code"
    
    //  Add contact professional info page & Update contact professional info page
    var enterCompany: String = "Enter Company Name"
    var enterTitle: String = "Enter Title"
    var enterEmail: String = "Enter email"
    var invalidWebsite: String = "Invalid Website"
    
    // ---------------------------------------------------------------  End of Validations   ---------------------------------------------------------------- //
    
    // -------------------------------------------------------------- Hard coded URLs ------------------------------------------------------------------- //
    var facetimeItunesUrl: String = "https://itunes.apple.com/in/app/facetime/id414307850?mt=12"
    var whatsappItunesUrl: String = "https://itunes.apple.com/in/app/whatsapp-messenger/id310633997?mt=8"
    
    // sync Pop up x- constraints
    var popUpLeftConstraint: CGFloat = -700
    var popUpCenterConstraint: CGFloat = 0
    var popUpInOutTimePeriod: Int = Int(2.0)
    
}
