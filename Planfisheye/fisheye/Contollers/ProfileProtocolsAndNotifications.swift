//
//  ProfileProtocolsAndNotifications.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 05/04/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import Foundation

// notification to Dismiss change master key page when master key id updated from MasterkeyOTPVerification page
let DISMISS_CHANGE_MASTER_KEY_PAGE = Notification.Name("DismissMasterKeyPage")

let DISMISS_UNNECESSARY_SIGNUP_PAGES = Notification.Name("DismissUnnecessarySignUpPages")

protocol ShowDisabledOTPView {
      func toDisableOTPView()
}

protocol ToEmptyMasterKeyTextFields {
      func toEmptyMasterKeyTexts()
}

protocol ToSmallTextChange {
    func masteraKeyTextChange()
}

protocol StartInteraction {
    func startButtonInteractions()
}
