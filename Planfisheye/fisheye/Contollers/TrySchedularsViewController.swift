//
//  TrySchedularsViewController.swift
//  fisheye
//
//  Created by Sankey Solutions on 17/01/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftCron

class TrySchedularsViewController: UIViewController {

    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var whenUpdatedlabel: UILabel!
    var time: Date?
    let appService = AppService()
    let appSharedPrefernce = AppSharedPreference()
    var idtoken: String?
//    var timer: Timer?
////    let activity = NSBackgroundActivityScheduler(identifier:  "com.example.MyApp.updatecheck")
////    activity.tolerance = 15 * 60
//    //using swuftcron
//    let myCronExpression = CronExpression(minute: "0", hour: "0", day: "8")
//    //using backgroundtask
//    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
//
    func updateUI() {
        time = Date()
        //self.whenUpdatedlabel.text="\(time)"
        //print("hitesh")
       // self.sendOTP()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.registerBackgroundTask()
        //self.doSomeDownload()
        print("in view did l")
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
//        self.timer = Timer.scheduledTimer(
//            timeInterval: 15.0, //in seconds
//            target: self, //where you'll find the selector (next argument)
//            selector: #selector(self.Tick), //MyClass is the current class
//            userInfo: nil, //no idea what this is, Apple, help?
//            repeats: true) //keep going!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func Tick () {
        print("inside tick")
         //self.whenUpdatedlabel.text="\(time)"
      //  self.sendOTP()
    }

    func sendOTP() {
        print("inside send otp")
        let headers: HTTPHeaders =
            [
                "Content-Type": "application/json",
                "Accept": "application/json"
            ]
        let parameters: Parameters =
            [
                "action": "sendotp",
                "idtoken": idtoken,
                "phonenumber": "7506578584",
                "countryCode": "+91",
                "email": "vaishalisagvekar20@gmail.com"
        ]
        let awsURL = AppConfig.sendOTP
        print("send otp url", AppConfig.sendOTP)
        Alamofire.request(awsURL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON {
                response in

                switch response.result {
                case .success:
                    print("inside send otp success")
                    let data = JSON(response.result.value!)
                    let statusCodeRecieved = data["statusCode"].stringValue
                    self.appSharedPrefernce.setAppSharedPreferences(key: "sendOtpStatus", value: statusCodeRecieved)
                    print("status code", statusCodeRecieved)
                case .failure(let error):
                    print("inside send otp failure")
                    self.appSharedPrefernce.setAppSharedPreferences(key: "sendOtpStatus", value: "NetworkError")

                }
        }
    }

    func doSomeDownload() {
        //call endBackgroundTask() on completion..
        switch UIApplication.shared.applicationState {
        case .active:
            print("App is active.")
        case .background:
            print("App is in background.")
            print("Background time remaining = \(UIApplication.shared.backgroundTimeRemaining) seconds")
        case .inactive:
            break
        }
    }

//    func registerBackgroundTask() {
//        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
//            self?.endBackgroundTask()
//        }
//        assert(backgroundTask != UIBackgroundTaskInvalid)
//    }
//    
//    func endBackgroundTask() {
//        print("Background task ended.")
//        UIApplication.shared.endBackgroundTask(backgroundTask)
//        backgroundTask = UIBackgroundTaskInvalid
//    }
}
