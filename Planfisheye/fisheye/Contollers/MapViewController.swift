//
//  MapViewController.swift
//  Planfisheye
//
//  Created by Keerthi on 06/08/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol HandleMapSearch {
    func dropPinZoomIn(placemark: MKPlacemark)
}

class MapViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
      @IBOutlet weak var pressLongToSelectLbl: UILabel!

    @IBOutlet weak var done: UIBarButtonItem!
    var locationManager: CLLocationManager!

    var resultSearchController: UISearchController?

    var selectedPin: MKPlacemark?
    var originalLat: Double?
    var originalLong: Double?
    var appService = AppService.shared

    var addressSearchDelegate: AddressSearchNew?

    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.navigationController?.navigationBar.isHidden = false

        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self

        // user activated automatic authorization info mode
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {
            // present an alert indicating location authorization required
            // and offer to take the user to Settings for the app via
            // UIApplication -openUrl: and UIApplicationOpenSettingsURLString
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.requestLocation()

        //        locationManager.startUpdatingLocation()
        //        locationManager.startUpdatingHeading()

        //mapview setup to show user location

        mapView.delegate = self
        mapView.showsUserLocation = true
        if originalLat != nil {

            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: self.originalLat!, longitude: self.originalLong!), completionHandler: {(placemarks, error) -> Void in
                if error != nil {

                    return
                }

                if (placemarks?.count)! > 0 {

                    if let pm = placemarks?[0] {

                        if let addressDict = pm.addressDictionary, let coordinate = pm.location?.coordinate {
                            let mkPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict as? [String: Any])
                            self.selectedPin = mkPlacemark
                            self.mapView.addAnnotation(mkPlacemark)

                            self.mapView.layoutMargins = UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)
                            self.mapView.showAnnotations(self.mapView.annotations, animated: true)

                            self.zoomAnnotations(on: self.mapView, toFrame: self.mapView.annotationVisibleRect, animated: true)
                            let currentView = self.mapView.visibleMapRect
                            self.mapView.annotations(in: currentView)
//                            //2: Get the current visible map rect
//                            let currentMapRect = self.mapView.visibleMapRect
//
//                            //3: Create the edges inset that you want the map view to show all annotation within
//                            let padding = UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100)
//
//                            //4: Set current map rect but with new padding, also set animation to true to see effect
//                            self.mapView.setVisibleMapRect(currentMapRect, edgePadding: padding, animated: true)
//                            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//                            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//                            self.map.setRegion(region, animated: true)
                        }

                    }

                } else {

                   
                    self.dismiss(animated: true, completion: nil)

                }

            })

        } else {
            mapView.userTrackingMode = MKUserTrackingMode(rawValue: 2)!
        }

        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTableVC") as! LocationSearchTableVC
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)

        resultSearchController?.searchResultsUpdater = locationSearchTable

        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = self.appService.searchForPlaces
        navigationItem.titleView = resultSearchController?.searchBar

        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true

        locationSearchTable.mapView = mapView

        locationSearchTable.handleMapSearchDelegate = self as HandleMapSearch

        //        let gestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(handleTap(gestureReconizer:)))
        //        mapView.addGestureRecognizer(gestureRecognizer)

        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleTap(gestureReconizer:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.mapView.addGestureRecognizer(lpgr)
        self.addObservers()
        self.setLocalizationText()
    }

      // add all observers to this viewController here
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      // End

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.pressLongToSelectLbl.text = self.appService.pressLongToSelectLocation
                  self.done.title = self.appService.done
            }
      }
      /* END */

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func handleTap(gestureReconizer: UILongPressGestureRecognizer) {

        if gestureReconizer.state != UIGestureRecognizer.State.ended {

            let location = gestureReconizer.location(in: mapView)
            let coordinate = mapView.convert(location, toCoordinateFrom: mapView)

            // Add annotation:
            mapView.removeAnnotations(mapView.annotations)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate

            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude), completionHandler: {(placemarks, error) -> Void in
                if error != nil {
                   
                    return
                }

                if (placemarks?.count)! > 0 {
                    if let pm = placemarks?[0] {

                        // not all places have thoroughfare & subThoroughfare so validate those values
                        //                annotation.title = (pm?.thoroughfare)! + ", " + pm?.subThoroughfare!
                        //                annotation.subtitle = pm?.subLocality
                        annotation.title = pm.name

                        if let city = pm.locality,

                            let state = pm.administrativeArea,
                            let subLocality = pm.subLocality {
                            annotation.subtitle = "\(city) \(subLocality) \(state)"

                        }
                       
                        self.mapView.addAnnotation(annotation)

                        if let addressDict = pm.addressDictionary, let coordinate = pm.location?.coordinate {
                            let mkPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict as? [String: Any])
                            self.selectedPin = mkPlacemark
                        }

                    }

                } else {
                    annotation.title = "Unknown Place"
                    self.mapView.addAnnotation(annotation)
                   

                }

                
            })
            return
        }
        if gestureReconizer.state != UIGestureRecognizer.State.began {
            return
        }
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {

        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(popView))
        //
        //        view.addGestureRecognizer(tapGesture)

    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        //        self.popView("")
    }

    @IBAction func popView(_ sender: Any) {
       
        if let pin = self.selectedPin {
            
            addressSearchDelegate?.addrssObjectFrom(place: pin)
            self.dismiss(animated: true, completion: nil)
        } else {
            
            CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: mapView.userLocation.coordinate.latitude, longitude: mapView.userLocation.coordinate.longitude), completionHandler: {(placemarks, error) -> Void in
                if error != nil {
                    
                    return
                }

                if (placemarks?.count)! > 0 {
                    
                    if let pm = placemarks?[0] {
                        

                        if let addressDict = pm.addressDictionary, let coordinate = pm.location?.coordinate {
                            let mkPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict as? [String: Any])
                            self.addressSearchDelegate?.addrssObjectFrom(place: mkPlacemark)
                            self.dismiss(animated: true, completion: nil)
                        }

                    }

                } else {

                   
                    self.dismiss(animated: true, completion: nil)

                }

            })

        }

    }

}

extension MapViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
           
            let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
            let region = MKCoordinateRegion(center: location.coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       
    }

}

extension MapViewController: HandleMapSearch {
    /************************** To fit all annotations functions start here **************************/
    func zoomAnnotations(on mapView: MKMapView, toFrame annotationsFrame: CGRect, animated: Bool) {
        if mapView.annotations.count < 2 {
            return
        }
        // Step 1: make an MKMapRect that contains all the annotations
        let annotations = mapView.annotations
        let firstAnnotation: MKAnnotation = annotations[0]
        var minPoint: MKMapPoint = MKMapPoint(firstAnnotation.coordinate)
        var maxPoint: MKMapPoint = minPoint
        for annotation: MKAnnotation in annotations {
            let point: MKMapPoint = MKMapPoint(annotation.coordinate)
            if point.x < minPoint.x {
                minPoint.x = point.x
            }
            if point.y < minPoint.y {
                minPoint.y = point.y
            }
            if point.x > maxPoint.x {
                maxPoint.x = point.x
            }
            if point.y > maxPoint.y {
                maxPoint.y = point.y
            }
        }
        let mapRect: MKMapRect = MKMapRect(x: minPoint.x, y: minPoint.y, width: maxPoint.x - minPoint.x, height: maxPoint.y - minPoint.y)
        // Step 2: Calculate the edge padding
        let edgePadding: UIEdgeInsets = UIEdgeInsets(top: annotationsFrame.minY, left: annotationsFrame.minX, bottom: mapView.bounds.maxY - annotationsFrame.maxY, right: mapView.bounds.maxX - annotationsFrame.maxX)
        // Step 3: Set the map rect
        mapView.setVisibleMapRect(mapRect, edgePadding: edgePadding, animated: animated)
    }
    /************************* To fit all annotations functions end here *************************/
    func dropPinZoomIn(placemark: MKPlacemark) {
        // cache the pin
        selectedPin = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,
            let state = placemark.administrativeArea,
            let code = placemark.postalCode {
            annotation.subtitle = "\(city) \(state) \(code)"
        }
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: placemark.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}
