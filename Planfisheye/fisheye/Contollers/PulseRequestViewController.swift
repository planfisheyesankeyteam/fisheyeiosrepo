//
//  PulseRequestViewController.swift
//  Planfisheye
//
//  Created by venkatesh murthy on 04/09/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class PulseRequestViewController: UIViewController {
    @IBOutlet weak var mainview: UIView!
    var isshare: Bool = false
    let runkeeperSwitch2 = DGRunkeeperSwitch()

    @IBOutlet weak var requestorshareLabel: UILabel!
    @IBOutlet weak var sendrequestButton: UIButton!
    @IBOutlet weak var nameBaseView: UIView!
    //@IBOutlet weak var scheduleBaseview: UIView!
   // @IBOutlet weak var nowBaseView: UIView!
   // @IBOutlet weak var scheduleRoundButton: KGRadioButton!
//    @IBAction func scheduleButtonClick(_ sender: KGRadioButton) {
//        
//        sender.isSelected = true
//        nowbutton.isSelected = false
//        
//        if sender.isSelected {
//            // radioBtn.text = "Selected"
//            
//        } else{
//            //radioBtn.text = "Not Selected"
//        }
//    }

//    @IBAction func scheduleTextButtonClick(_ sender: Any) {
//        nowbutton.isSelected = false
//        scheduleRoundButton.sendActions(for: UIControlEvents.touchUpInside)
//        
//    }
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nowbutton: KGRadioButton!
    var radioButtonController: UIButton?

    var selectedPhoneNumber: String = ""
    var selectedEmailID: String = ""
    var selectedName: String = ""
    var selectedContactID: String = ""

    @IBAction func dateorTimeClick(_ sender: Any) {

        DatePickerDialog().show("Date&Time Picker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .dateAndTime) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"

                // self.textField.text = formatter.string(from: dt)
            }
        }
    }

//    @IBAction func nowButtonClick(_ sender: Any) {
//        
//        scheduleRoundButton.isSelected = false
//        nowbutton.sendActions(for: UIControlEvents.touchUpInside)
//    }
    @IBOutlet weak var nowBtn: UIButton!

//    @IBAction func radioClick(_ sender: KGRadioButton) {
//        sender.isSelected = true
//        scheduleRoundButton.isSelected = false
//        
//        if sender.isSelected {
//            // radioBtn.text = "Selected"
//        } else{
//            //radioBtn.text = "Not Selected"
//        }
//    }

    @IBAction func sendRequestClick(_ sender: Any) {

        let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
        let insertCapsuleViewController = storyboard.instantiateViewController(withIdentifier: "InsertCapsuleViewController") as! InsertCapsuleViewController
        insertCapsuleViewController.isshare = self.isshare
        insertCapsuleViewController.selectedContactID = self.selectedContactID
        insertCapsuleViewController.view.frame = self.view.bounds
        self.addChildViewController(insertCapsuleViewController)
        insertCapsuleViewController.didMove(toParentViewController: self)

        //  profileBaseviewController.delegate = self

        insertCapsuleViewController.view.frame.origin.y = -self.view.frame.size.height

        self.view.addSubview(insertCapsuleViewController.view)

        UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn, animations: { _ in
            insertCapsuleViewController.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }

    @IBAction func backclick(_ sender: Any) {

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()

    }
    @IBAction func nowbuttonClick(_ sender: Any) {

    }
//    @IBAction func byLocationClick(_ sender: Any) {
//        
//        
//        self.bytimeButton.backgroundColor = UIColor.clear
//        //self.byloactionButton.layer.shadowColor = UIColor.white.cgColor
//        
//        self.bytimeButton.layer.shadowOpacity = 0.0;
//        
//        self.bytimeButton.titleLabel?.textColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
//        
//        
//        
//        self.byloactionButton.layer.cornerRadius = 15.0
//        self.byloactionButton.layer.masksToBounds = false
//        self.byloactionButton.layer.shadowRadius = 1.0
//        self.byloactionButton.layer.shadowColor = UIColor.black.cgColor
//        self.byloactionButton.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        self.byloactionButton.layer.shadowOpacity = 1.0
//        
//        DispatchQueue.main.async {
//            self.byloactionButton.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")
//            self.byloactionButton.titleLabel?.textColor = UIColor.white
//            
//        }
//        
//    }
//    @IBAction func bytimeClick(_ sender: Any) {
//        
//        
//        self.byloactionButton.backgroundColor = UIColor.clear
//        //self.byloactionButton.layer.shadowColor = UIColor.white.cgColor
//        self.byloactionButton.layer.shadowOpacity = 0.0;
//        
//        
//        
//        
//        self.bytimeButton.layer.cornerRadius = 15.0
//        self.bytimeButton.layer.masksToBounds = false
//        self.bytimeButton.layer.shadowRadius = 1.0
//        self.bytimeButton.layer.shadowColor = UIColor.black.cgColor
//        self.bytimeButton.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        self.bytimeButton.layer.shadowOpacity = 1.0
//        DispatchQueue.main.async {
//            
//            self.byloactionButton.titleLabel?.textColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")
//
//            self.bytimeButton.backgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")
//            
//            self.bytimeButton.titleLabel?.textColor = UIColor.white
//        }
//        
//    }
  //  @IBOutlet weak var byloactionButton: UIButton!
    @IBOutlet weak var pulserequestview: UIView!
 //   @IBOutlet weak var bytimeButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        applyshadow()

//        phoneNumberLabel.text = selectedPhoneNumber
       nameLabel.text = selectedName
//        nowbutton.isSelected = true
        if(self.isshare) {
            requestorshareLabel.text = "Share location to"
        } else {
            requestorshareLabel.text = "Requesting location to"

        }
        // Do any additional setup after loading the view.
    }

    func applyshadow() {

        // Bounce back to the main thread to update the UI
        DispatchQueue.main.async {
            self.mainview.layer.cornerRadius = 8

            // border
            self.mainview.layer.borderWidth = 0
            self.mainview.layer.borderColor = UIColor.black.cgColor
            //        self.mainview.clipsToBounds = true

            // shadow
            self.mainview.layer.shadowColor = UIColor.black.cgColor
            self.mainview.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.mainview.layer.shadowOpacity = 0.7
            self.mainview.layer.shadowRadius = 10.0

            self.sendrequestButton.layer.cornerRadius = 18.0
            self.pulserequestview.layer.cornerRadius = 15.0

          //  self.bytimeButton.sendActions(for: UIControlEvents.touchUpInside)

          //  self.nowBaseView.layer.cornerRadius = 10.0
            //self.scheduleBaseview.layer.cornerRadius = 10.0
            self.nameBaseView.layer.cornerRadius = 10.0

//            self.bytimeButton.layer.cornerRadius = 15.0
//            self.bytimeButton.layer.masksToBounds = false
//            self.bytimeButton.layer.shadowRadius = 1.0
//            self.bytimeButton.layer.shadowColor = UIColor.black.cgColor
//            self.bytimeButton.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//            self.bytimeButton.layer.shadowOpacity = 1.0
//            self.byloactionButton.titleLabel?.textColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")

        }
        runkeeperSwitch2.titles = ["Now", "Around Location"]
        runkeeperSwitch2.backgroundColor = UIColor.hexStringToUIColor(hex: "#F7F7F7")

        //UIColor(red: 239.0/255.0, green: 95.0/255.0, blue: 49.0/255.0, alpha: 1.0)
        runkeeperSwitch2.selectedBackgroundColor = UIColor.hexStringToUIColor(hex: "#263D93")//.white
        runkeeperSwitch2.titleColor = UIColor.hexStringToUIColor(hex: "#3F5ABD")//.white
        runkeeperSwitch2.selectedTitleColor = .white
        //UIColor(red: 239.0/255.0, green: 95.0/255.0, blue: 49.0/255.0, alpha: 1.0)
        runkeeperSwitch2.addTarget(self, action: #selector(PulseRequestViewController.switchValueDidChange(sender:)), for: .valueChanged)
        runkeeperSwitch2.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 13.0)
        runkeeperSwitch2.frame = CGRect(x: 50.0, y: 20.0, width: pulserequestview.bounds.width - 80.0, height: pulserequestview.bounds.height)
        runkeeperSwitch2.setSelectedIndex(0, animated: true)
        runkeeperSwitch2.autoresizingMask = [.flexibleWidth]
        self.pulserequestview.addSubview(runkeeperSwitch2)

    }

    @IBAction func switchValueDidChange(sender: DGRunkeeperSwitch!) {

        if(sender.selectedIndex == 1) {
            kSweetAlert.showAlert("Message!", subTitle: "Coming soon!", style: AlertStyle.warning)
            runkeeperSwitch2.setSelectedIndex(0, animated: true)

        } else {

        }
        print("valueChanged: \(sender.selectedIndex)")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}
