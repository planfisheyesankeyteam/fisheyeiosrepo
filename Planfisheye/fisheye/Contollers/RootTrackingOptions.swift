//
//  RootTrackingOptions.swift
//  fisheye
//
//  Created by SankeyMacPro on 03/12/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

public struct RootTrackingOptions {
      
      public var trackingOptions : [TrackingOption]!
      
}

