//
//  PulseDetailsViewController.swift
//  fisheye
//
//  Created by Anuradha on 12/13/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit

class PulseDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {

    /*********************************** outlet variables ***********************************/

    //base view contanining FE icon,labels,and duck blue background
    @IBOutlet weak var pulseDeatilsBaseView: UIView!
    @IBOutlet weak var fishEyeLabel: UILabel!
    @IBOutlet weak var itsAboutYouLabel: UILabel!
    @IBOutlet weak var fisheyeIconButton: UIButton!
    @IBOutlet weak var creatApulseButton: UIButton!
    @IBOutlet weak var singleParticipantVIew: UIView!
    @IBOutlet weak var singleParticipantName: UILabel!
    @IBOutlet weak var singleParticipantImg: UIImageView!
    @IBOutlet weak var singleParticipantStackViewDetails: UIView!
    @IBOutlet weak var singleParticipantSingleDetailLbl: UILabel!
    @IBOutlet weak var singleParticipantEmailLbl: UILabel!
    @IBOutlet weak var singleParticipantSeparator: UIView!
    @IBOutlet weak var singleParticipentIsFisheyeIndicator: UIView!
    //main view which contains pulse details data
    @IBOutlet weak var singleParticipantPhone: UILabel!
    @IBOutlet weak var viewWithBase: UIView!
    @IBOutlet weak var emptyPulsedetailsImageView: UIImageView!
    //view equal to main View,shown till details loads
    @IBOutlet weak var viewEqualToMainView: UIView!
    //uppper View of Pulse deatils screen,with time,location name,repeaters
    @IBOutlet weak var upperViewOfPulseDetails: UIView!
    //line 1
    @IBOutlet weak var backButtonImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pulseIsOfHimImageView: UIImageView!
    @IBOutlet weak var pulseTitleOnDetails: UILabel!
    //
    @IBOutlet weak var leftMostButtonNearTitle: UIButton!
    @IBOutlet weak var righrMostButtonNearTitle: UIButton!
    @IBOutlet weak var deleteIconImage: UIImageView!
    @IBOutlet weak var editIconImage: UIImageView!
    @IBOutlet weak var acceptedImageIcon: UIImageView!
    @IBOutlet weak var dismissImageIcon: UIImageView!
    @IBOutlet weak var AcceptedDeclinedLabel: UILabel!
    //line2
    @IBOutlet weak var nowPulseTimeView: UIView!
    @IBOutlet weak var pulseCreatedDate: UILabel!
    @IBOutlet weak var betCreatedTimeAndDate: UILabel!
    @IBOutlet weak var pulseCreatedTime: UILabel!
    //for scheduled pulse line
    @IBOutlet weak var scheduledPulseTimeView: UIView!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var betStartDateAndEndDate: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var betEndDateAndTime: UILabel!
    @IBOutlet weak var pulseTime: UILabel!
    @IBOutlet weak var expiredPulseLabelWhenIHaveCreatedPulse: UILabel!
    //line3
    @IBOutlet weak var repeatersView: UIView!
    @IBOutlet weak var repeatersIcon: UIImageView!
    @IBOutlet weak var repeatersListLabel: UILabel!
    //line4
    @IBOutlet weak var locationLabel: UILabel!
    //line 5(end)
    @IBOutlet weak var underlineToUpperView: UIView!
    //Recipient view
    @IBOutlet weak var RecipientsView: UIView!
    @IBOutlet weak var recipientLabel: UILabel!
    @IBOutlet weak var recipientStackView: UIStackView!
    @IBOutlet weak var showParticipentListButton: UIButton!
    //first participent
    @IBOutlet weak var firstRecipientView: UIView!
    @IBOutlet weak var firstRecipientImage: UIImageView!
    @IBOutlet weak var firstRecipientNameLabel: UILabel!
    @IBOutlet weak var firstIsAcceptImage: UIImageView!
    //second participent
    @IBOutlet weak var secondRecipientName: UILabel!
    @IBOutlet weak var secondRecipientIsAcceptedImage: UIImageView!
    @IBOutlet weak var secondRecipientImage: UIImageView!
    @IBOutlet weak var secondRecipientView: UIView!
    //third participent
    @IBOutlet weak var thirdRecipientName: UILabel!
    @IBOutlet weak var thirdRecipientIsAcceptedImage: UIImageView!
    @IBOutlet weak var thirdRecipientImage: UIImageView!
    @IBOutlet weak var thirdRecipientView: UIView!
    //fourth participent
    @IBOutlet weak var fourthRecipientName: UILabel!
    @IBOutlet weak var fourthRecipientIsAccepted: UIImageView!
    @IBOutlet weak var fourthRecipientView: UIView!
    @IBOutlet weak var fourthRecipientImage: UIImageView!
    //fifth(more particiepnt)
    @IBOutlet weak var moreRecipientImageDotDot: UIImageView!
    @IBOutlet weak var moreRecipientView: UIView!
    //table view to show participents
    @IBOutlet weak var contactTableOnPulseDetails: UITableView!
    @IBOutlet weak var showRecipients: UIImageView!
    @IBOutlet weak var hideRecipients: UIImageView!
    //capsule view
    @IBOutlet weak var capsuleView: UIView!
    @IBOutlet weak var requestCapsuleView: UIView!
    @IBOutlet weak var capsuleMessageLabelInRequestedCapsuleView: UILabel!
    @IBOutlet weak var hisCapsuleImage: UIImageView!
    @IBOutlet weak var capsuleLabel: UILabel!
    @IBOutlet weak var masterKeyIcon: UIImageView!
    @IBOutlet weak var unlockMasterKeyButton: UIButton!
    //map view to show location
    @IBOutlet weak var sharedLocationMapView: MKMapView!
    // zoom in & zoom out Buttons
    @IBOutlet weak var zoomOutBtn: UIButton!
    @IBOutlet weak var zoomInBtn: UIButton!

    @IBOutlet weak var networkIssueLabel: UILabel!

/**************************************** IB actions *************************************/

    @IBAction func unlockMasterkeyButtonClicked(_ sender: Any) {
        self.openMasterKeyPopUp()
    }

    //fisheyeicon button action at bottom eft corner
    @IBAction func fisheyeIconBtn(_ sender: Any) {
        self.goToDashBoard()
    }

    //creat pulse button at bottom right corner
    @IBAction func creatAPulseButtonClicked(_ sender: Any) {
        self.goToCreatPulsePage(reachingShareRequestToEditPulse: false)
    }

    //back button action
    @IBAction func pulseArrowUpper(_ sender: Any) {
        self.goToDashBoard()
//       self.openPulseListingPage()
    }

    //second left button action
    @IBAction func leftNextToRightMostButton(_ sender: Any) {
        if(!self.editIconImage.isHidden) {
            self.goToCreatPulsePage(reachingShareRequestToEditPulse: true)
        }
        if(!self.dismissImageIcon.isHidden) {
            self.confirmPulseDeclination()
        }
    }

    //right most utton action
    @IBAction func rightMostButton(_ sender: Any) {
        if(!self.deleteIconImage.isHidden) {
            self.confirmPulseDeletion()
        }
        if(!self.acceptedImageIcon.isHidden) {
            self.tellWhatAcceptMeans()
        }
    }

    //show participent button action at the right of participent stack
    @IBAction func showParticipentButtonClicked(_ sender: Any) {
        if(self.contactTableOnPulseDetails.isHidden) {
            self.openParticipentListInTable()
        } else {
            self.hideParticipentListTable()
        }
    }

    @IBAction func zoomOutButtonTapped(_ sender: Any) {
        var region: MKCoordinateRegion? = sharedLocationMapView?.region
        let latitude = min((region?.span.latitudeDelta)! * 2.0, 180.0)
        let longitude = min((region?.span.longitudeDelta)! * 2.0, 180.0)
        region?.span.latitudeDelta = latitude
        region?.span.longitudeDelta = longitude
        sharedLocationMapView?.setRegion(region!, animated: true)
    }

    @IBAction func zoomInButtonTapped(_ sender: Any) {
        var region: MKCoordinateRegion? = sharedLocationMapView?.region
        region?.span.latitudeDelta /= (2.0 as  CLLocationDegrees)
        region?.span.longitudeDelta /= (2.0 as  CLLocationDegrees)
        sharedLocationMapView?.setRegion(region!, animated: true)
    }

/******************************************** global variables ******************************/

    var pulseData=PulseDetails()
    var respondersData=RespondersData()
    var respondersDataArray=[RespondersData]()
    let validationController = ValidationController()
    var sections = [String]()
    var sectionData: [Int: [RespondersData]] = [:]
    var acceptedRespondersData=[RespondersData]()
    var declinedRespondersData=[RespondersData]()
    var notYetRespondedRespondersData=[RespondersData]()
    var pulseIdOfWhichDetailsShown: String!
    var idtoken: String!
      var appService = AppService()
    let appSharedPrefernce = AppSharedPreference()
      var pulseMsgs = PulseToastMsgHeadingSubheadingLabels()

    var isPulsed: Bool!
    var hideTableSection0 = false
    var hideTableSection1 = false
    var hideTableSection2 = false
   

/**************************************** predefined functions *****************************/

    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
        self.appService = AppService.shared
        self.pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
        self.setLocalizationText()
        self.addObservers()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

        self.sections = [self.pulseMsgs.acceptedPulseRecipients, self.pulseMsgs.notRespondedRecipients, self.pulseMsgs.declinedPulseRecipients]

        self.getPulseDetailsById()
        self.initiallyApplyThesePropertiesToElemnts()
        self.applyViewShadow()
        self.beforeAssigningDetailsToComponents()
        self.singleParticipantVIew.isHidden = true

        self.hideRecipients.isHidden=true
        sectionData =  [0: acceptedRespondersData, 1: notYetRespondedRespondersData, 2: declinedRespondersData]
        self.hideRecipients.isHidden=true
        self.expiredPulseLabelWhenIHaveCreatedPulse.isHidden=true
        self.networkIssueLabel.text=self.pulseMsgs.checkInternetConnection
    }

    override func viewWillAppear(_ animated: Bool) {
        GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.PulseDetailsViewController)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

/**************************************** Table view Functions *******************************/

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        let contactCell = tableView.dequeueReusableCell(withIdentifier: "ContactTableCellOnPulseDetailsTableViewCell", for: indexPath) as! ContactTableCellOnPulseDetailsTableViewCell
        contactCell.recipientImage.layer.cornerRadius = contactCell.recipientImage.frame.size.width / 2
        contactCell.recipientImage.clipsToBounds = true
        contactCell.recipientImage.layer.borderWidth = 1.5

        let respondersDataObj = sectionData[indexPath.section]![indexPath.row]

        var contactImageUrl=respondersDataObj.responderImage
        
//        contactCell.recipientImage?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        contactCell.recipientImage?.sd_setImage(with: URL(string: contactImageUrl?.addingPercentEncoding(withAllowedCharacters:
            NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
        

        if(respondersDataObj.isFisheye || respondersDataObj.contactId.contains("FE")) {
           contactCell.isFisheyeIndicationView.isHidden=false
        } else {
           contactCell.isFisheyeIndicationView.isHidden=true
        }

        let phone = respondersDataObj.responderPhoneNumber ?? ""
        let email = respondersDataObj.responderEmail ?? ""

      if(phone.isEmpty && !email.isEmpty) {
            contactCell.stackViewDetails.isHidden = true
            contactCell.singleDetailDisplay.isHidden = false
            contactCell.singleDetailDisplay.text = email
      } else if(!phone.isEmpty && email.isEmpty) {
            contactCell.stackViewDetails.isHidden = true
            contactCell.singleDetailDisplay.isHidden = false
            contactCell.singleDetailDisplay.text = phone
      } else if(!phone.isEmpty && !email.isEmpty) {
            contactCell.singleDetailDisplay.isHidden = true
            contactCell.stackViewDetails.isHidden = false
            contactCell.recipientPhoneNumber.text = phone
            contactCell.recipientEmail.text = email
      } else {
            contactCell.recipientPhoneNumber.text = "n/a"
            contactCell.recipientEmail.text = "n/a"
            contactCell.seperationBtwPhoneNumberAndEmail.isHidden = false
      }

      if(respondersDataObj.responderName == " ") {
            contactCell.recipientName.text = "Unknown"
      } else {
            contactCell.recipientName.text = respondersDataObj.responderName
      }

        switch( indexPath.section ) {
        case 0 :
            contactCell.recipientImage.layer.borderColor = UIColor.acceptGreen().cgColor
        case 1:
            contactCell.recipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
        case 2:
            contactCell.recipientImage.layer.borderColor = UIColor.declinedRed().cgColor
        default:
            contactCell.recipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
        }

        cell=contactCell
        return cell!
    }

    func openParticipentListInTable() {
        self.capsuleView.isHidden=true
        self.showRecipients.isHidden=true
        self.hideRecipients.isHidden=false
        self.contactTableOnPulseDetails.isHidden=false
    }

    func hideParticipentListTable() {
        self.capsuleView.isHidden=false
        self.showRecipients.isHidden=false
        self.hideRecipients.isHidden=true
        self.contactTableOnPulseDetails.isHidden=true
    }

    func  openMasterKeyPopUp() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController")  as! MasterKeyPopUpViewController
        nextVC.fromUnlockCapsuleofDetails=true
        nextVC.deletePulseProtocol = self as DeletePulseProtocol
        self.present(nextVC, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0 :
            if hideTableSection0 {
                return 0
            } else {
                return (sectionData[section]?.count)!
            }
        case 1:
            if hideTableSection1 {
                return 0
            } else {
                return (sectionData[section]?.count)!
            }
        case 2 :
            if hideTableSection2 {
                return 0
            } else {
                return (sectionData[section]?.count)!
            }
        default:
            return (sectionData[section]?.count)!
        }
    }

    //to provide name to a section,need to return a uiView always
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch (section) {
        case 0 :
            if hideTableSection0 {
                return UIView(frame: CGRect.zero)
            } else {
                let view = UIView()
                let title = UILabel()
                title.font = UIFont(name: "Helvetica Neue", size: 10)
                title.text = self.sections[section]
                title.textColor = UIColor.acceptGreen()
                title.font = UIFont.italicSystemFont(ofSize: 10)
                title.frame = CGRect(x: 19, y: 5, width: 200, height: 20 )
                view.addSubview(title)
                view.backgroundColor = UIColor.contactsTypeBg()
                return view
            }
        case 1:
            if hideTableSection1 {
                return UIView(frame: CGRect.zero)
            } else {
                let view = UIView()
                let title = UILabel()
                title.font = UIFont(name: "Helvetica Neue", size: 10)
                title.text = self.sections[section]
                title.textColor = UIColor.purpleRing()
                title.font = UIFont.italicSystemFont(ofSize: 10)
                title.frame = CGRect(x: 19, y: 5, width: 200, height: 20 )
                view.addSubview(title)
                view.backgroundColor = UIColor.contactsTypeBg()
                return view
            }
        case 2 :
            if hideTableSection2 {
                return UIView(frame: CGRect.zero)
            } else {
                let view = UIView()
                let title = UILabel()
                title.font = UIFont(name: "Helvetica Neue", size: 10)
                title.text = self.sections[section]
                title.textColor = UIColor.declinedRed()
                title.font = UIFont.italicSystemFont(ofSize: 10)
                title.frame = CGRect(x: 19, y: 5, width: 200, height: 20 )
                view.addSubview(title)
                view.backgroundColor = UIColor.contactsTypeBg()
                return view
            }
        default:
            return nil
        }

    }

    //viewForHeaderInSection methods works only when  heightForHeadersInSection method is implemented
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch (section) {
        case 0 :
            if hideTableSection0 {
                return 1
            } else {
                return 25
            }
        case 1:
            if hideTableSection1 {
                return 1
            } else {
                return 25
            }
        case 2 :
            if hideTableSection2 {
                return 1
            } else {
                return 25
            }
        default:
            return 25
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }

    /*****************************************Helping FUnctions********************************/

    func applyViewShadow() {
        self.viewWithBase.layer.cornerRadius = 8
        self.viewWithBase.layer.borderWidth = 0
        self.viewWithBase.layer.borderColor = UIColor.black.cgColor
        self.viewWithBase.layer.shadowColor = UIColor.black.cgColor
        self.viewWithBase.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.viewWithBase.layer.shadowOpacity = 0.7
        self.viewWithBase.layer.shadowRadius = 10.0
    }

    func initiallyApplyThesePropertiesToElemnts() {

        self.contactTableOnPulseDetails.isHidden=true
        self.AcceptedDeclinedLabel.isHidden=true

        self.requestCapsuleView.layer.cornerRadius = 8
        self.viewEqualToMainView.layer.cornerRadius = 8
        self.sharedLocationMapView.layer.cornerRadius = 8

        self.hisCapsuleImage.layer.cornerRadius = self.hisCapsuleImage.frame.size.width / 2
        self.hisCapsuleImage.clipsToBounds = true

        self.pulseIsOfHimImageView.layer.cornerRadius = self.pulseIsOfHimImageView.frame.size.width / 2
        self.pulseIsOfHimImageView.clipsToBounds = true
        self.firstRecipientImage.layer.cornerRadius = self.firstRecipientImage.frame.size.width / 2
        self.firstRecipientImage.clipsToBounds = true
        self.secondRecipientImage.layer.cornerRadius = self.secondRecipientImage.frame.size.width / 2
        self.secondRecipientImage.clipsToBounds = true
        self.thirdRecipientImage.layer.cornerRadius = self.thirdRecipientImage.frame.size.width / 2
        self.thirdRecipientImage.clipsToBounds = true
        self.fourthRecipientImage.layer.cornerRadius = self.fourthRecipientImage.frame.size.width / 2
        self.fourthRecipientImage.clipsToBounds = true

        self.firstRecipientImage.layer.borderWidth = 1.5
        self.secondRecipientImage.layer.borderWidth = 1.5
        self.thirdRecipientImage.layer.borderWidth = 1.5
        self.fourthRecipientImage.layer.borderWidth = 1.5

        self.firstRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
        self.secondRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
        self.thirdRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
        self.fourthRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
    }

    func afterAssiningDetailsToComponents() {
        self.viewWithBase.isHidden=false
        self.viewEqualToMainView.isHidden=true
    }

    func hideSecondRecipientInStack() {
        self.secondRecipientView.isHidden=false
        self.secondRecipientImage.isHidden=true
        self.secondRecipientIsAcceptedImage.isHidden=true
        self.secondRecipientName.isHidden=true
    }

    func hideThirdRecipientInStack() {
        self.thirdRecipientView.isHidden=false
        self.thirdRecipientImage.isHidden=true
        self.thirdRecipientIsAcceptedImage.isHidden=true
        self.thirdRecipientName.isHidden=true
    }

    func hideFourthRecipientInStack() {
        self.fourthRecipientView.isHidden=false
        self.fourthRecipientImage.isHidden=true
        self.fourthRecipientIsAccepted.isHidden=true
        self.fourthRecipientName.isHidden=true
    }

    func hideFifthMemberOfStack() {
        self.moreRecipientView.isHidden=false
        self.moreRecipientImageDotDot.isHidden=true
    }

    func showUnlockedCapsule() {
        self.masterKeyIcon.image=UIImage(named: "masterKeyUnlocked")!
        if(self.pulseData.message == "" || self.pulseData.message == self.pulseMsgs.capsuleMsgPlaceHolder) {
            if(self.pulseData.pulseType=="0") {
                self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseMsgs.emptyCapWhileRequestingPulse
            } else {
                self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseMsgs.emptyCapWhileSharingPulse
            }
        } else {
            self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseData.message
        }
//        self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseData.message!
        self.unlockMasterKeyButton.isUserInteractionEnabled=false
    }

    func showSingleParticipantsData() {
        let respondersDataObj1=self.pulseData.respondersArray[0]

        self.singleParticipantImg.layer.cornerRadius = self.singleParticipantImg.frame.size.width / 2
        self.singleParticipantImg.clipsToBounds = true
        self.singleParticipantImg.layer.borderWidth = 1.5

        let firstRecipientImageUrl=respondersDataObj1.responderImage
//        self.singleParticipantImg?.sd_setImage(with: URL(string: firstRecipientImageUrl?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
        self.singleParticipantImg?.sd_setImage(with: URL(string: firstRecipientImageUrl?.addingPercentEncoding(withAllowedCharacters:
            NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        self.singleParticipantName.text=respondersDataObj1.responderName

        if(respondersDataObj1.isFisheye) {
          self.singleParticipentIsFisheyeIndicator.backgroundColor=UIColor.purpleRing()
        }

        let phone = respondersDataObj1.responderPhoneNumber ?? ""
        let email = respondersDataObj1.responderEmail ?? ""

        if(phone.isEmpty && !email.isEmpty) {
              self.singleParticipantStackViewDetails.isHidden = true
              self.singleParticipantSingleDetailLbl.isHidden = false
              self.singleParticipantSingleDetailLbl.text = email
        } else if(!phone.isEmpty && email.isEmpty) {
              self.singleParticipantStackViewDetails.isHidden = true
              self.singleParticipantSingleDetailLbl.isHidden = false
              self.singleParticipantSingleDetailLbl.text = phone
        } else if(!phone.isEmpty && !email.isEmpty) {
              self.singleParticipantSingleDetailLbl.isHidden = true
              self.singleParticipantStackViewDetails.isHidden = false
              self.singleParticipantPhone.text = phone
              self.singleParticipantEmailLbl.text = email
              self.singleParticipantSeparator.isHidden = false
        } else {
              self.singleParticipantPhone.text = "n/a"
              self.singleParticipantEmailLbl.text = "n/a"
              self.singleParticipantSeparator.isHidden = false
              self.singleParticipantStackViewDetails.isHidden = false
              self.singleParticipantSingleDetailLbl.isHidden = true
        }

        let isAccepted1=respondersDataObj1.isAccepted!
        let isDeclined1=respondersDataObj1.isDeclined!
        let isResponded1=respondersDataObj1.isResponded!
        if(isAccepted1 && !isDeclined1 && isResponded1) {
              self.singleParticipantImg.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
              self.singleParticipantImg.layer.borderColor = UIColor.declinedRed().cgColor
        } else {
              self.singleParticipantImg.layer.borderColor = UIColor.notRespondedGray().cgColor
        }
    }

    func showFirstRecipientInStack() {
        var respondersDataArr=[RespondersData]()
        let respondersDataObj1=self.pulseData.respondersArray[0]
        self.firstRecipientView.isHidden=false
        var firstRecipientImageUrl=respondersDataObj1.responderImage
        self.firstRecipientImage?.sd_setImage(with: URL(string: firstRecipientImageUrl?.addingPercentEncoding(withAllowedCharacters:
            NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        self.firstIsAcceptImage.isHidden=false
        self.firstRecipientNameLabel.text=respondersDataObj1.responderName
        var isAccepted1=respondersDataObj1.isAccepted!
        var isDeclined1=respondersDataObj1.isDeclined!
        var isResponded1=respondersDataObj1.isResponded!
        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.firstRecipientImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.firstRecipientImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.firstIsAcceptImage.image=UIImage(named: "pluscopy2")!
        } else {
            self.firstRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.firstIsAcceptImage.isHidden=true
        }
    }

    func showSecondRecipientInStack() {
        var respondersDataArr=[RespondersData]()
        let respondersDataObj2=self.pulseData.respondersArray[1]
        self.secondRecipientView.isHidden=false
        var secondRecipientImageUrl=respondersDataObj2.responderImage
//        self.secondRecipientImage?.sd_setImage(with: URL(string: secondRecipientImageUrl?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
         self.secondRecipientImage?.sd_setImage(with: URL(string: secondRecipientImageUrl?.addingPercentEncoding(withAllowedCharacters:
            NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        self.secondRecipientIsAcceptedImage.isHidden=false
        self.secondRecipientName.text=respondersDataObj2.responderName
        var isAccepted1=respondersDataObj2.isAccepted!
        var isDeclined1=respondersDataObj2.isDeclined!
        var isResponded1=respondersDataObj2.isResponded!
        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.secondRecipientImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.secondRecipientImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.secondRecipientIsAcceptedImage.image=UIImage(named: "pluscopy2")!
        } else {
            self.secondRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.secondRecipientIsAcceptedImage.isHidden=true
        }
    }

    func showThirdRecipientInStack() {
        var respondersDataArr=[RespondersData]()
        let respondersDataObj3=self.pulseData.respondersArray[2]
        self.thirdRecipientView.isHidden=false
        var thirdRecipientImageUrl=respondersDataObj3.responderImage
        self.thirdRecipientImage?.sd_setImage(with: URL(string: thirdRecipientImageUrl?.addingPercentEncoding(withAllowedCharacters:
            NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        self.thirdRecipientIsAcceptedImage.isHidden=false
        self.thirdRecipientName.text=respondersDataObj3.responderName
        var isAccepted1=respondersDataObj3.isAccepted!
        var isDeclined1=respondersDataObj3.isDeclined!
        var isResponded1=respondersDataObj3.isResponded!
        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.thirdRecipientImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.thirdRecipientImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.thirdRecipientIsAcceptedImage.image=UIImage(named: "pluscopy2")!
        } else {
            self.thirdRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.thirdRecipientIsAcceptedImage.isHidden=true
        }
    }

    func showFourthRecipientInStack() {
        var respondersDataArr=[RespondersData]()
        let respondersDataObj4=self.pulseData.respondersArray[3]
        self.fourthRecipientView.isHidden=false
        var fourthRecipientImageUrl=respondersDataObj4.responderImage
        self.fourthRecipientImage?.sd_setImage(with: URL(string:fourthRecipientImageUrl?.addingPercentEncoding(withAllowedCharacters:
            NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        self.fourthRecipientIsAccepted.isHidden=false
        self.fourthRecipientName.text=respondersDataObj4.responderName
        var isAccepted1=respondersDataObj4.isAccepted!
        var isDeclined1=respondersDataObj4.isDeclined!
        var isResponded1=respondersDataObj4.isResponded!
        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.fourthRecipientImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.fourthRecipientImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.fourthRecipientIsAccepted.image=UIImage(named: "pluscopy2")!
        } else {
            self.fourthRecipientImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.fourthRecipientIsAccepted.isHidden=true
        }
    }

    func showFifthMemberOfStack() {
        self.moreRecipientView.isHidden=false
        self.moreRecipientImageDotDot.isHidden=false
    }

    func beforeAssigningDetailsToComponents() {
        self.viewWithBase.isHidden=false
        self.viewEqualToMainView.isHidden=false
    }

    func assignPulseDetailsGotFromLogBookToComponentOnPulseDeatils() {
        //set image of requester
        DispatchQueue.main.async {
            var requestersImageUrl=self.pulseData.pulseRequesterImage
            self.pulseIsOfHimImageView?.sd_setImage(with: URL(string: requestersImageUrl?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))

            //set pulse title
            if(self.isPulsed) {
                self.pulseTitleOnDetails.text=self.pulseData.requesterMessage
            } else {
                self.pulseTitleOnDetails.text=self.pulseData.responserMessage
                self.RecipientsView.isHidden=true
            }

            //set start data,end date,time if pulse is scheduled,if not scheduled set
            //date and time when pulse is sent
            self.assignValuesAccIsScheduled()
            //if pulse is of type request location is empty and if pulsed(send by logged in user) then
            //accept and reject are hidden,and edit and delete are shown
            //if pulse is of type share show map,and isPulsed show edit and delete.
            self.assignValuesAccordingToPulseType()

            self.assignAccToCapProtectedOrNot()

            //load stack view according to responders data
            self.loadingRecipientStackViewAccordingRespondersDataArr()

            if self.acceptedRespondersData.count==0 {
                self.hideTableSection0 = true
            }
            if  self.notYetRespondedRespondersData.count==0 {
                self.hideTableSection1 = true
            }
            if self.declinedRespondersData.count == 0 {
                self.hideTableSection2 = true
            }

            self.sectionData =  [0: self.acceptedRespondersData, 1: self.notYetRespondedRespondersData, 2: self.declinedRespondersData]

            self.contactTableOnPulseDetails.reloadData()
            //after assigning value to all the components on pulse details,hide empty pulse deatils icon and
            //show assigned puls deatils instead
            self.afterAssiningDetailsToComponents()

        }

        self.stopLoader()
    }

    func assignAccToCapProtectedOrNot() {
        //capsule message on pulse deatils
        var hisCapsuleImageURL=self.pulseData.pulseRequesterImage
        self.hisCapsuleImage?.sd_setImage(with: URL(string: hisCapsuleImageURL?.addingPercentEncoding(withAllowedCharacters:
            NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        if(self.pulseData.isEncrypted) {
            self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseMsgs.capLockNeedMasterKey
            self.masterKeyIcon.image=UIImage(named: "masterKeyLock")!
            self.unlockMasterKeyButton.isUserInteractionEnabled=true
        } else {
            self.masterKeyIcon.image=UIImage(named: "masterKeyUnlocked")!
            if(self.pulseData.message == "" || self.pulseData.message == self.pulseMsgs.capsuleMsgPlaceHolder) {
                if(self.pulseData.pulseType=="0") {
                   self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseMsgs.emptyCapWhileRequestingPulse
                } else {
                    self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseMsgs.emptyCapWhileSharingPulse
                }
            } else {
                self.capsuleMessageLabelInRequestedCapsuleView.text=self.pulseData.message
            }
            self.unlockMasterKeyButton.isUserInteractionEnabled=false
        }
    }

    func loadingRecipientStackViewAccordingRespondersDataArr() {

        let responderDataArrayCount=self.pulseData.respondersArray.count

        switch responderDataArrayCount {
        case 0:
            self.RecipientsView.isHidden=true
            self.repeatersListLabel.isHidden=true
            self.recipientStackView.isHidden=true
            self.showParticipentListButton.isHidden=true
            break
        case 1:
            self.showSingleParticipantView()
            break
        case 2:
            self.showFirstRecipientInStack()
            self.showSecondRecipientInStack()
            self.hideThirdRecipientInStack()
            self.hideFourthRecipientInStack()
            self.hideFifthMemberOfStack()
            break
        case 3:
            self.hideSingleParticipantView()
            self.showFirstRecipientInStack()
            self.showSecondRecipientInStack()
            self.showThirdRecipientInStack()
            self.hideFourthRecipientInStack()
            self.hideFifthMemberOfStack()
            break
        case 4:
            self.hideSingleParticipantView()
            self.showFirstRecipientInStack()
            self.showSecondRecipientInStack()
            self.showThirdRecipientInStack()
            self.showFourthRecipientInStack()
            self.hideFifthMemberOfStack()
            break
        default:
            self.hideSingleParticipantView()
            self.showFirstRecipientInStack()
            self.showSecondRecipientInStack()
            self.showThirdRecipientInStack()
            self.showFourthRecipientInStack()
            self.showFifthMemberOfStack()
            break
        }
    }

      func showSingleParticipantView() {
            self.singleParticipantVIew.isHidden = false
            self.showParticipentListButton.isHidden = true
            self.recipientStackView.isHidden = true
            self.hideRecipients.isHidden = true
            self.showRecipients.isHidden = true
            self.showSingleParticipantsData()
      }

      func hideSingleParticipantView() {
            self.singleParticipantVIew.isHidden = true
            self.showParticipentListButton.isHidden = false
            self.recipientStackView.isHidden = false
            self.showRecipients.isHidden = false
      }

    func assignValuesAccIsScheduled() {
        if(self.pulseData.isScheduled) {
            var oldDate=validationController.convert_date_from_UTC_time(givenUTCdate:
                self.pulseData.pulseEndDate!)
           
            var isActive=self.pulseData.isActive!
            
            if(oldDate == "01/01/1970") {
                self.scheduledPulseTimeView.isHidden=true
                self.repeatersIcon.isHidden=true
                self.repeatersListLabel.isHidden=true
                self.pulseCreatedDate.text=validationController.convert_date_from_UTC_time(givenUTCdate:
                    self.pulseData.pulseStartDate!)
                self.betCreatedTimeAndDate.text=self.pulseMsgs.labelBetweenEDandPT
                self.pulseCreatedTime.text=validationController.convert_time_from_UTC_time(givenUTCtime:
                    self.pulseData.pulseTime!)
            } else {
                self.nowPulseTimeView.isHidden=true
                self.startDateLabel.text=validationController.convert_date_from_UTC_time(givenUTCdate:
                    self.pulseData.pulseStartDate!)
                self.betStartDateAndEndDate.text=self.pulseMsgs.labelBetweenSDandED
                self.endDateLabel.text=validationController.convert_date_from_UTC_time(givenUTCdate:
                    self.pulseData.pulseEndDate!)
                self.betEndDateAndTime.text=self.pulseMsgs.labelBetweenEDandPT
                self.pulseTime.text=validationController.convert_time_from_UTC_time(givenUTCtime:
                    self.pulseData.pulseTime!)
                self.repeatersIcon.isHidden=false
                self.recipientLabel.isHidden=false
                let daysOfRepeatationArray=self.pulseData.pulseRepeatersForDays
                var dayRepeatersString=""
//                for day in daysOfRepeatationArray {
//                    dayRepeatersString=dayRepeatersString+""+"\(day)"+","
//                }
                for i in 0..<daysOfRepeatationArray.count {
                    
                    if(i<daysOfRepeatationArray.count-1){
                       dayRepeatersString=dayRepeatersString+""+"\(daysOfRepeatationArray[i])"+", "
                    }else{
                        dayRepeatersString=dayRepeatersString+""+"\(daysOfRepeatationArray[i])"
                    }
                }
                self.repeatersListLabel.text=dayRepeatersString
            }

        } else if(!self.pulseData.isScheduled) {
            self.scheduledPulseTimeView.isHidden=true
            self.repeatersIcon.isHidden=true
            self.repeatersListLabel.isHidden=true
            self.pulseCreatedDate.text=validationController.convert_date_from_UTC_time_fromStringIO(givenUTCdate: self.pulseData.createdTime!)
            self.betCreatedTimeAndDate.text=self.pulseMsgs.labelBetweenEDandPT
            self.pulseCreatedTime.text=validationController.convert_time_from_UTC_time_fromStringIO(givenUTCtime: self.pulseData.createdTime!)
        }
    }

    func assignValuesAccordingToPulseType() {
        let isActive=self.pulseData.isActive!

        if(self.pulseData.pulseType=="0") {
            self.sharedLocationMapView.isHidden=true
            self.zoomOutBtn.isHidden = true
            self.zoomInBtn.isHidden = true

            if(self.isPulsed && self.pulseData.isScheduled) {//scheduled request by me
                self.acceptedImageIcon.isHidden=true
                self.dismissImageIcon.isHidden=true
                self.deleteIconImage.isHidden=false
                if(isActive) {//scheduled,pulsed and active
                    self.editIconImage.isHidden=false
                    self.AcceptedDeclinedLabel.isHidden=true
                } else {
                    self.editIconImage.isHidden=true
                    self.AcceptedDeclinedLabel.isHidden=true
                    self.expiredPulseLabelWhenIHaveCreatedPulse.isHidden=false
                    //delete chya sobat
                }
            } else if(self.isPulsed && !self.pulseData.isScheduled) {//now request by me(no funda of active/expired)
                self.editIconImage.isHidden=true
                self.deleteIconImage.isHidden=false
                self.acceptedImageIcon.isHidden=true
                self.dismissImageIcon.isHidden=true
            } else if(!self.isPulsed) {//not by me
                self.editIconImage.isHidden=true
                self.deleteIconImage.isHidden=true
              if(self.pulseData.respondersArray.count>0) {
                let isAccepted=self.pulseData.respondersArray[0].isAccepted!
                let isDeclined=self.pulseData.respondersArray[0].isDeclined!
                let isResponded=self.pulseData.respondersArray[0].isResponded!

                if(self.pulseData.isScheduled) {
                    if(isActive) {
                        //here the scheduled pulse is active
                        if(isAccepted && !isDeclined && isResponded) {
                            self.acceptedImageIcon.isHidden=false
                            self.dismissImageIcon.isHidden=false
                            self.acceptedImageIcon.image=UIImage(named: "AcceptedFilled")
                            self.righrMostButtonNearTitle.isUserInteractionEnabled=false
                            self.leftMostButtonNearTitle.isUserInteractionEnabled=true
                        } else if(!isAccepted && isDeclined && isResponded) {
                            self.acceptedImageIcon.isHidden=false
                            self.dismissImageIcon.isHidden=false
                            self.dismissImageIcon.image=UIImage(named: "DeclinedFilled")
                            self.leftMostButtonNearTitle.isUserInteractionEnabled=false
                            self.righrMostButtonNearTitle.isUserInteractionEnabled=true
                        } else {
                            self.acceptedImageIcon.isHidden=false
                            self.dismissImageIcon.isHidden=false
                            self.AcceptedDeclinedLabel.isHidden=true
                        }
                    } else {
                        //here the scheduled pulse is expired,m not a createor of pulse
                        self.AcceptedDeclinedLabel.isHidden=false
                        self.AcceptedDeclinedLabel.text=self.pulseMsgs.pulseExpired
                        self.acceptedImageIcon.isHidden=true
                        self.dismissImageIcon.isHidden=true
                    }
                } else {
                    if(isAccepted && !isDeclined && isResponded) {
                        self.AcceptedDeclinedLabel.isHidden=false
                        self.acceptedImageIcon.isHidden=true
                        self.dismissImageIcon.isHidden=true
                        self.AcceptedDeclinedLabel.text=self.pulseMsgs.pulseAccepted
                    } else if(!isAccepted && isDeclined && isResponded) {
                        self.AcceptedDeclinedLabel.isHidden=false
                        self.acceptedImageIcon.isHidden=true
                        self.dismissImageIcon.isHidden=true
                        self.AcceptedDeclinedLabel.text=self.pulseMsgs.pulseDeclined
                    } else {
                        self.acceptedImageIcon.isHidden=false
                        self.dismissImageIcon.isHidden=false
                        self.AcceptedDeclinedLabel.isHidden=true
                    }
                }

            }
          }
            self.locationLabel.text=""
        }

        if(self.pulseData.pulseType=="1") {
            self.sharedLocationMapView.isHidden=false
            self.zoomOutBtn.isHidden = false
            self.zoomInBtn.isHidden = false
            //For shared location on map
            self.sharedLocationMapView.delegate = self
            let annotation = MKPointAnnotation()
            let lat = (self.pulseData.latitude! as NSString).doubleValue
            let long = (self.pulseData.longitude! as NSString).doubleValue
            
            let destinationAnnotation = CustomAnnotation()
            
            var loc = CLLocationCoordinate2D(latitude:lat,longitude:long)
            
            var region = MKCoordinateRegion()
            region = MKCoordinateRegion(center: loc, latitudinalMeters: 100, longitudinalMeters: 100)
            
            sharedLocationMapView?.setRegion(region, animated: true)
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.title = self.pulseMsgs.annotationTitle
            self.sharedLocationMapView.addAnnotation(annotation)
//            self.sharedLocationMapView.showAnnotations([annotation], animated: true)
            self.sharedLocationMapView.showsScale = true
            self.locationLabel.text=self.pulseData.locationName

            if(self.isPulsed && self.pulseData.isScheduled) {//scheduled share by me
                self.acceptedImageIcon.isHidden=true
                self.dismissImageIcon.isHidden=true
                self.editIconImage.isHidden=false
                self.deleteIconImage.isHidden=false
                self.sharedLocationMapView.isHidden=true
                self.zoomInBtn.isHidden=true
                self.zoomOutBtn.isHidden=true
            } else if(self.isPulsed && !self.pulseData.isScheduled) {//now share by me
                self.editIconImage.isHidden=true
                self.deleteIconImage.isHidden=false
                self.acceptedImageIcon.isHidden=true
                self.dismissImageIcon.isHidden=true
            } else if(!self.isPulsed) {//not by me
                self.editIconImage.isHidden=true
                self.deleteIconImage.isHidden=true
                self.acceptedImageIcon.isHidden=true
                self.dismissImageIcon.isHidden=true
            }
        }
    }

    func comingBackToPulseDetailsAfterAcceptPulseSuccess() {
        if(self.pulseData.isScheduled) {
            self.acceptedImageIcon.isHidden = false
            self.dismissImageIcon.isHidden = false
            self.acceptedImageIcon.image=UIImage(named: "AcceptedFilled")
            self.righrMostButtonNearTitle.isUserInteractionEnabled=false
            self.dismissImageIcon.image=UIImage(named: "DeclinedHollow")
            self.leftMostButtonNearTitle.isUserInteractionEnabled=true
            self.AcceptedDeclinedLabel.isHidden=true
        } else {
            self.AcceptedDeclinedLabel.isHidden=false
            self.AcceptedDeclinedLabel.text=self.pulseMsgs.pulseAccepted
            self.acceptedImageIcon.isHidden=true
            self.dismissImageIcon.isHidden=true
        }
    }

    func comingbackToPulseDetailsAfterDeclinePulseSuccess() {
        DispatchQueue.main.async {
            if(self.pulseData.isScheduled) {
                self.acceptedImageIcon.isHidden = false
                self.dismissImageIcon.isHidden = false
                self.dismissImageIcon.image=UIImage(named: "DeclinedFilled")
                self.leftMostButtonNearTitle.isUserInteractionEnabled=false
                self.acceptedImageIcon.image=UIImage(named: "AcceptedHollow")
                self.righrMostButtonNearTitle.isUserInteractionEnabled=true
                self.AcceptedDeclinedLabel.isHidden=true
            } else {
                self.AcceptedDeclinedLabel.isHidden=false
                self.AcceptedDeclinedLabel.text=self.pulseMsgs.pulseDeclined
                self.acceptedImageIcon.isHidden=true
                self.dismissImageIcon.isHidden=true
            }
        }
    }

    func startLoader() {
        self.view.isUserInteractionEnabled=false
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
    }

    func stopLoader() {
        DispatchQueue.main.async {
            self.view.isUserInteractionEnabled=true
            ScreenLoader.shared.stopLoader()
        }
    }

    /********************** Map view annotation functions *******************/

    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        // this is where visible maprect should be set
        mapView.showAnnotations(mapView.annotations, animated: false)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is MKPointAnnotation) {
            return nil
        }

        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }

        let pinImage = UIImage(named: "speedLocationIcon-1")
        annotationView!.image = pinImage
        annotationView?.frame.size = CGSize(width: 33.0, height: 33.0)

        return annotationView
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.acceptGreen()
            renderer.lineDashPattern = [5, 5]
            renderer.lineWidth = 2
            return renderer
        }

        return MKOverlayRenderer()
    }
    /********************** Map view annotation functions end *******************/

    /******************************************** Navigation related functions********************/

    //Common aleart popup for user
    func popUpVCController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController") as! BasicPopUpViewController
        self.present(nextVC, animated: true, completion: nil)
    }

    func addContactToYourContactListToRespond() {
        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Add Contact", action: "Add requester to your contact list from pulse details", label: "Add pulse requester in your contact list to respond to his pulse request.", value: 0)
        self.dualActionPopUp(heading: self.pulseMsgs.addContactHeading, subheading: self.pulseMsgs.addContactSubHeading, action: "warning", method: "addReqContToRespToPulseReq", module: "NA" )
        self.dualButtonpopUpVCController(addRequester: true)
    }

    func  openInsertCapsulePopUp() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "capsuleInsertPopUpViewController")  as! capsuleInsertPopUpViewController
        nextVC.deletePulseProtocol = self as DeletePulseProtocol
        nextVC.fromAcceptPulse=true
        nextVC.acceptThisPulse=self.pulseIdOfWhichDetailsShown
        nextVC.respondToId=self.pulseData.fromId
        nextVC.respondToPulseIdIsScheduled=self.pulseData.isScheduled
        nextVC.acceptThisPulseObj=self.pulseData
        nextVC.fromDetailsWhileAccepting=true
        self.present(nextVC, animated: true, completion: nil)
    }

    func openPulseListingPage() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "PulseListingViewController")  as! PulseListingViewController
        nextVC.view.frame = CGRect.init(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        nextVC.view.clipsToBounds = true
        nextVC.view.isUserInteractionEnabled = true
        self.addChild(nextVC)
        nextVC.didMove(toParent: self)
        self.view.addSubview(nextVC.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            nextVC.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }

    func goToDashBoard() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
        self.present(nextVC, animated: true, completion: nil)
    }

    func goToCreatPulsePage(reachingShareRequestToEditPulse: Bool) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "ShareAndRequestPulseVCViewController")  as! ShareAndRequestPulseVCViewController
        nextVC.reachedToEditPulse=reachingShareRequestToEditPulse
        if(reachingShareRequestToEditPulse) {
            self.pulseData.isPulsed=self.isPulsed
           nextVC.pulseDetailsNeedToBeEdited=self.pulseData
        }
        nextVC.view.frame = CGRect.init(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height)
        nextVC.view.clipsToBounds = true
        nextVC.view.isUserInteractionEnabled = true
        self.addChild(nextVC)
        nextVC.didMove(toParent: self)
        self.view.addSubview(nextVC.view)
        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                nextVC.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
    }

    //a common alert pop-up
      func dualButtonpopUpVCController(addRequester: Bool) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "AllPopupDualActionVC") as! AllPopupDualActionVC
        nextVC.deletePulseProtocol = self as DeletePulseProtocol
        self.present(nextVC, animated: true, completion: nil)
    }

    func dualActionPopUp(heading: String, subheading: String, action: String, method: String, module: String) {
        self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
        self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
        self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
        self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
    }

    func confirmPulseDeletion() {
        self.dualActionPopUp(heading: self.pulseMsgs.pulseDeletionHeading, subheading: self.pulseMsgs.pulseDeletionSubHeading, action: "deletePulse", method: "deletePulse", module: "NA")
        self.dualButtonpopUpVCController(addRequester: false)
    }

    func confirmPulseDeclination() {
        self.dualActionPopUp(heading: self.pulseMsgs.tellDeclineMeansHeading, subheading: self.pulseMsgs.tellDeclineMeansSubHeading, action: "declinePulse", method: "declinePulse", module: "NA" )
        self.dualButtonpopUpVCController(addRequester: false)
    }

    func tellWhatAcceptMeans() {
        self.dualActionPopUp(heading: self.pulseMsgs.tellAcceptMeansHeading, subheading: self.pulseMsgs.tellAcceptMeansSubHeading, action: "acceptPulseAtDetails", method: "acceptPulseAtDetails", module: "NA" )
        self.dualButtonpopUpVCController(addRequester: false)
    }

    func showToastMsg(toastMsg: String, action: Int) {
        DispatchQueue.main.async {
            self.view.makeToast(toastMsg)
            if(action==1) {
                self.beforeAssigningDetailsToComponents()
//                self.comingbackToPulseDetailsAfterDeclinePulseSuccess()
            }
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse, execute: {
            self.view.hideToast()
        })
    }

    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
      DispatchQueue.main.async {
            self.AcceptedDeclinedLabel.text = self.pulseMsgs.pastButtonTitle
            self.recipientLabel.text = self.pulseMsgs.recipient
            self.capsuleLabel.text = self.pulseMsgs.capsule
      }
    }
    /* END */

    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */

/************************************* API Calls ***************************************/

    //---------------------------- Get Pulse details API Call --------------------------//

    func getPulseDetailsById() {
      guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                  self.stopLoader()
                  //self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, action: 0)
                  return
      }
        self.networkIssueLabel.isHidden=true
        self.startLoader()

        let parameters =
            [
                "idtoken": self.idtoken,
                "action": "getpulsedetails",
                "pulseId": self.pulseIdOfWhichDetailsShown
            ] as [String: Any]

        let awsURL = AppConfig.getPulseDetailsAPI

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"

        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {

                if let resultObject = json as? [String: Any] {
                    let statusCodeRecieved = resultObject["statusCode"] as? String ?? ""

                    if(statusCodeRecieved == "0" || statusCodeRecieved == "200") {
                        if let pulseDetailsObj = resultObject["pulse"] as? [String: Any] {
                            var dayReapterArray=[String]()
                            self.pulseData.pulseId = pulseDetailsObj ["pulseId"] as? String ?? ""
                            self.pulseData.pulseType =  pulseDetailsObj ["pulseType"] as? String ?? ""
                            self.pulseData.message = pulseDetailsObj ["message"] as? String ?? ""
                            self.pulseData.fromId =  pulseDetailsObj ["requesterId"] as? String ?? ""
                            self.pulseData.pulseRequesterName = pulseDetailsObj ["requesterName"] as? String ?? ""
                            self.pulseData.pulseRequesterImage = pulseDetailsObj ["requesterImage"] as? String ?? ""
                            self.pulseData.latitude = pulseDetailsObj ["latitude"] as? String ?? ""
                            self.pulseData.longitude = pulseDetailsObj ["longitude"] as? String ?? ""
                            self.pulseData.locationName = pulseDetailsObj ["locationName"] as? String ?? ""
                            self.pulseData.createdTime =  pulseDetailsObj ["createdTimeStamp"] as? String ?? ""
                            self.pulseData.alertMessage = pulseDetailsObj ["alertMessage"] as? String ?? ""
                            self.pulseData.requesterMessage = pulseDetailsObj ["requesterMessage"] as? String ?? ""
                            self.pulseData.responserMessage = pulseDetailsObj ["responserMessage"] as? String ?? ""
                            self.pulseData.isEncrypted = pulseDetailsObj ["isEncrypted"] as? Bool ?? false
                            self.pulseData.isActive = pulseDetailsObj ["isActive"] as? Bool ?? false
                            self.pulseData.isScheduled = pulseDetailsObj ["isScheduled"] as? Bool ?? false
                            self.pulseData.pulseTime = pulseDetailsObj ["pulseTime"] as? Double ?? 0.0
                            self.pulseData.pulseStartDate = pulseDetailsObj ["pulseStartDate"] as? Double ?? 0.0
                            self.pulseData.pulseEndDate = pulseDetailsObj ["pulseEndDate"] as? Double ?? 0.0
                            if let dayRepeatersArray=pulseDetailsObj ["pulseRepeatersForDays"] as? [String] {
                                for day in dayRepeatersArray {
                                    self.pulseData.pulseRepeatersForDays.append(day)
                                }
                            }
                            if let respondersArray = pulseDetailsObj["pulseResponderDataList"] as? [[String: Any]] {
                                for responderObject in respondersArray {
                                    if let responderObj = responderObject as? [String: Any] {
                                        var respondersData=RespondersData()
                                        respondersData.entryId = responderObj["entryId"] as? String ?? ""
                                        respondersData.contactId = responderObj["contactId"] as? String ?? ""
                                        respondersData.isAccepted = responderObj["isAccepted"] as? Bool ?? false
                                        respondersData.isDeclined = responderObj["isDeclined"] as? Bool ?? false
                                        respondersData.isResponded = responderObj["isResponded"] as? Bool ?? false
                                        respondersData.isRead = responderObj["isRead"] as? Bool ?? false
                                        respondersData.isFisheye = responderObj["isFisheye"] as? Bool ?? false
                                        respondersData.isDeleted = responderObj["isDeleted"] as? Bool ?? false
                                        respondersData.pulseId = responderObj["pulseId"] as? String ?? ""
                                        let name =  responderObj["responderName"] as? String ?? ""
                                        if(name == " ") {
                                            respondersData.responderName = "Unknown"
                                        } else {
                                            respondersData.responderName = name
                                        }
                                        respondersData.responderImage = responderObj["responderImage"] as? String ?? ""
                                        respondersData.responderEmail = responderObj["responderEmail"] as? String ?? ""
                                        respondersData.responderPhoneNumber = responderObj["responderPhoneNumber"]as? String ?? ""
                                        self.pulseData.respondersArray.append(respondersData)

                                        //responded and isAccepted istrue
                                        if ((respondersData.isAccepted) && (respondersData.isResponded)) {
                                            self.acceptedRespondersData.append(respondersData)
                                        }
                                        //responded and is Accepted is false
                                        if ((respondersData.isDeclined) && (respondersData.isResponded)) {
                                            self.declinedRespondersData.append(respondersData)
                                        }
                                        //not responeded yet
                                        if ((!respondersData.isResponded) && (!respondersData.isAccepted) && (!respondersData.isDeclined)) {
                                            self.notYetRespondedRespondersData.append(respondersData)
                                        }
                                    }
                                }
                            }
                            self.assignPulseDetailsGotFromLogBookToComponentOnPulseDeatils()

                        }
                         self.stopLoader()
                    } else {
                        self.stopLoader()
//                        self.showToastMsg(toastMsg:self.pulseMsgs.serverErrorOfPulse,action:0)
                         self.networkIssueLabel.isHidden = false
                        self.networkIssueLabel.text = self.pulseMsgs.serverErrorOfPulse
                    }
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Details", action: "get pulse details by pulse id", label: "Pulse details loaded successfully", value: 0)
                } else {
                    self.stopLoader()
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Details", action: "get pulse details by pulse id", label: "Failure while loading pulse details", value: 0)
                    self.assignPulseDetailsGotFromLogBookToComponentOnPulseDeatils()
                    self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
                }

            } else {
                self.stopLoader()
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Details", action: "get pulse details by pulse id", label: "Failure while loading pulse details", value: 0)
                self.assignPulseDetailsGotFromLogBookToComponentOnPulseDeatils()
                self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }

//------------------------------------- Delete Pulse API call ----------------------------------------//

    func deletePulseById() {
      guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                  self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, action: 0)
                  return
      }
        self.startLoader()

        let parameters =
            [
                "idtoken": self.idtoken,
                "action": "deletepulse",
                "pulseId": self.pulseIdOfWhichDetailsShown
            ] as [String: Any]

        let awsURL = AppConfig.deletePulseAPI

        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody

        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }

        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                self.stopLoader()
                if let resultObject = json as? [String: Any] {
                    let statusCodeRecieved = resultObject["statusCode"] as? String ?? ""
                    if(statusCodeRecieved == "0" || statusCodeRecieved == "200") {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Delete Pulse", action: "Delete pulse at pulse details", label: "Success while deleting pulse from pulse details screen with pulse id : "+self.pulseIdOfWhichDetailsShown, value: 0)
                        DispatchQueue.main.async {
                            self.openPulseListingPage()
                        }
                    } else {
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Delete Pulse", action: "Delete pulse at pulse details", label: "Error while deleting pulse from pulse details screen with pulse id : "+self.pulseIdOfWhichDetailsShown, value: 0)
                        self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)                    }
                } else {
                    self.stopLoader()
                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Delete Pulse", action: "Delete pulse at pulse details", label: "Error while deleting pulse from pulse details screen with pulse id : "+self.pulseIdOfWhichDetailsShown, value: 0)
                   self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
                }
            } else {
                self.stopLoader()
                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Delete Pulse", action: "Delete pulse at pulse details", label: "Error while deleting pulse from pulse details screen with pulse id : "+self.pulseIdOfWhichDetailsShown, value: 0)
                self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }

//---------------------------------- decline pulse API call --------------------------------------//

    func updateAcceptRejectStatus(pulseId: String, isAccepted: Bool) {
        self.startLoader()
        let feedbackOperation = DeclineAPulseRequest(isAccepted: false, pulseToBeDeclined: pulseId)
        feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
            self.stopLoader()
            if(operation.statusCode == "200") {
                self.showToastMsg(toastMsg: self.pulseMsgs.pulseReqDeclined, action: 0)
                self.comingbackToPulseDetailsAfterDeclinePulseSuccess()
            } else {
                self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
            }
        }
        AppDelegate.addProcedure(operation: feedbackOperation)
    }

}

/************************************** protocol *****************************************/

extension PulseDetailsViewController: DeletePulseProtocol {

    func toDeletePulse(toReload: Bool) {
        self.deletePulseById()
    }

    func toDeclinePulse(toReload: Bool) {
        self.updateAcceptRejectStatus(pulseId: self.pulseIdOfWhichDetailsShown, isAccepted: false)
    }

    func afetrAcceptingPulse(toReload: Bool) {
        self.comingBackToPulseDetailsAfterAcceptPulseSuccess()
    }

    func toEnterCapsuleWhileAcceptingPulse(toReload: Bool) {
        self.openInsertCapsulePopUp()
    }

    func toAlertToAddRequestInContactList(toReload: Bool) {
        self.addContactToYourContactListToRespond()
    }

    func unlockedCapsuleDisplay(toReload: Bool) {
        self.showUnlockedCapsule()
    }

}
