//
//  ScreenLoader.swift
//  fisheye
//
//  Created by venkatesh murthy on 12/10/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import Foundation

public class ScreenLoader{
    static let shared: ScreenLoader = ScreenLoader()
    let imageName = "loader"
    var imageView = UIImageView()
    var isLoaderRotating = false
    
    func rotateView(targetView: UIView, duration: Double = 1.0) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
            targetView.transform = targetView.transform.rotated(by: CGFloat(Double.pi))
        }) { finished in
            self.rotateView(targetView: targetView, duration: duration)
        }
    }

    func startLoader(view: UIView){
        if !isLoaderRotating {
            self.isLoaderRotating = true
            let image = UIImage(named: imageName)
            self.imageView = UIImageView(image: image!)
            self.imageView.frame = CGRect(x: 0, y: 0, width: 75, height: 75)
            self.imageView.center = CGPoint(x: view.bounds.width / 2, y: view.bounds.height / 2)
            DispatchQueue.main.async {
                view.addSubview(self.imageView)
                self.rotateView(targetView: self.imageView)
            }
        }
    }
    
    func stopLoader(){
        if isLoaderRotating{
            self.isLoaderRotating = false
            DispatchQueue.main.async {
                self.imageView.layer.removeAllAnimations()
                self.imageView.removeFromSuperview()
            }
        }
    }
}

