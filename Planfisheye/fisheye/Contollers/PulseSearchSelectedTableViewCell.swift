//
//  PulseSearchSelectedTableViewCell.swift
//  fisheye
//
//  Created by venkatesh murthy on 18/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class PulseSearchSelectedTableViewCell: UITableViewCell {

    @IBOutlet weak var selectedRadioBtn: KGRadioButton!
    @IBOutlet weak var phoneoremailLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
