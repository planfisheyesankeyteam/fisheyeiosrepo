//
//  PulseShowLogsViewController.swift
//  fisheye
//
//  Created by Sylveon on 29/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

struct cellData {
    var cell: Int!
    var cellTitle: String!
    var cellMapView: String!
    var cellPulseTime: String!

}

class PulseShowLogsViewController: UITableViewController {
    var arrayOfCellData=[cellData]()

    @IBOutlet weak var tableViewOfPulseLogBook: UITableView!
    @IBOutlet weak var viewEqualToVC: UIView!

    @IBOutlet weak var searchViewAtTop: UIView!

    @IBOutlet weak var pulseLogbookLabel: UILabel!

    @IBAction func backBtnAtTop(_ sender: Any) {
    }

    //@IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var viewEqualToTableView: UIView!

    @IBAction func searchButtonAtTop(_ sender: Any) {
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if arrayOfCellData[indexPath.row].cell==1 {
            return 286
        } else if arrayOfCellData[indexPath.row].cell==2 {
            return 78
        } else {
            return 286
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if arrayOfCellData[indexPath.row].cell==1 {
            let cell=Bundle.main.loadNibNamed("PulseLogbookCustomViewCell", owner: self, options: nil)?.first as! PulseLogbookCustomViewCell
            cell.pulseTitleLabel.text=arrayOfCellData[indexPath.row].cellTitle

            return cell
        } else if arrayOfCellData[indexPath.row].cell==2 {
            let cell=Bundle.main.loadNibNamed("PulseLogbookNoteViewCell", owner: self, options: nil)?.first as! PulseLogbookNoteViewCell
            cell.pulseTitleLabelOnNote.text=arrayOfCellData[indexPath.row].cellTitle

            return cell
        } else {
            let cell=Bundle.main.loadNibNamed("PulseLogbookCustomViewCell", owner: self, options: nil)?.first as! PulseLogbookCustomViewCell
            cell.pulseTitleLabel.text=arrayOfCellData[indexPath.row].cellTitle

            return cell
        }

    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfCellData.count
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.arrayOfCellData=[cellData(cell: 1, cellTitle: "anuradha want to know your location", cellMapView: "fdffdfd", cellPulseTime: "ggggg"),
                               cellData(cell: 2, cellTitle: "anuradha want to know your location", cellMapView: "fdffdfd", cellPulseTime: "ggggg"),
                               cellData(cell: 3, cellTitle: "anuradha want to know your location", cellMapView: "fdffdfd", cellPulseTime: "ggggg")]

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
