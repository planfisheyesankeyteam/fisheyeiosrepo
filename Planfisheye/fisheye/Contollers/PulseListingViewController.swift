//
//  PulseListingViewController.swift
//  fisheye
//
//  Created by Anuradha urgunde  on 12/15/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit
import Crashlytics

struct PulseDetails {
      var pulseRequesterName: String!
      var pulseRequesterImage: String!
      var pulseId: String!
      var pulseType: String!
      var locationName: String!
      var isScheduled: Bool!
      var pulseStartDate: Double!
      var pulseEndDate: Double!
      var pulseTime: Double!
      var message: String!
      var latitude: String!
      var longitude: String!
      var createdTime: String!
      var requesterMessage: String!
      var responserMessage: String!
      var alertMessage: String!
      var fromId: String!
      var respondersArray = [RespondersData]()
      var isEncrypted: Bool!
      var isPulsed: Bool!
      var isActive: Bool!
      var requestedPulseId: String!
      var pulseRepeatersForDays=[String]()
}

struct RespondersData {
      var entryId: String!
      var contactId: String!
      var isAccepted: Bool!
      var isDeleted: Bool!
      var isFisheye: Bool!
      var isRead: Bool!
      var pulseId: String!
      var responderName: String!
      var responderImage: String!
      var responderEmail: String!
      var responderPhoneNumber: String!
      var isResponded: Bool!
      var isDeclined: Bool!
}

class PulseListingViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate {

      /******************************************* outlet variables **************************************/

      @IBOutlet weak var tableViewPulse: UITableView!
      @IBOutlet weak var searchPulseButton: UIButton!
      @IBOutlet weak var seachBarForPulse: UISearchBar!
      @IBOutlet weak var pulseLabelAtTop: UILabel!
      @IBOutlet weak var seachViewAtPulseTop: UIView!
      @IBOutlet weak var searchViewOfPulseList: UIView!
      @IBOutlet weak var baseViewOfPulseListing: UIView!
      @IBOutlet weak var viewEqualToTableView: UIView!
      @IBOutlet weak var underlineOfUpperView: UIView!
      @IBOutlet weak var stackOfShareAndRequest: UIStackView!
      @IBOutlet weak var requestBtnView: UIView!
      @IBOutlet weak var shareBtnView: UIView!
      @IBOutlet weak var selectedRequestImageView: UIImageView!
      @IBOutlet weak var unselectedRequestImageView: UIImageView!
      @IBOutlet weak var requestBtnLabel: UILabel!
      @IBOutlet weak var unselectedShareImageView: UIImageView!
      @IBOutlet weak var selectedShareImageView: UIImageView!
      @IBOutlet weak var shareBtnLabel: UILabel!
      @IBOutlet weak var noPulseFoundCorrespondingToSearch: UILabel!
      @IBOutlet weak var viewContainEmptyLogBook: UIView!
      @IBOutlet weak var creatPulseBtn: UIButton!
      //@IBOutlet weak var youCanRequestLabel: UILabel!
      @IBOutlet weak var youCanShareLabel: UILabel!
      @IBOutlet weak var emptyPulseLogbookImageView: UIImageView!
      @IBOutlet weak var fishEyeIcon: UIButton!

      @IBOutlet weak var pastButtonLabel: UILabel!
      @IBOutlet weak var PastButtonImage: UIImageView!
      @IBOutlet weak var pastPulseLogbookButton: UIButton!
      @IBOutlet weak var fishEyeBottomLabel: UILabel!
      @IBOutlet weak var letsWorkTogetherLabel: UILabel!
    
      @IBOutlet weak var popUpCentreConstraint: NSLayoutConstraint!
    
      @IBOutlet weak var popUpSuccessImg: UIImageView!
      @IBOutlet weak var popUpDismissImg: UIImageView!
      @IBOutlet weak var popUpWarningImg: UIImageView!
      @IBOutlet weak var pupUpView: UIView!
    
      @IBOutlet weak var popUpMessage: UILabel!
      @IBOutlet weak var popUpHeading: UILabel!
      @IBOutlet weak var messageLable: UILabel!
      @IBOutlet weak var syncContactsHeader: UILabel!
      @IBOutlet weak var noContactsLbl: UILabel!
      @IBOutlet weak var popUpCancelLbl: UILabel!
      @IBOutlet weak var popUpOkLbl: UILabel!

      /************************************ globel variables **********************************************/

      //this array stores all the logs of logged in user
      var logsArray = [PulseDetails]()
      var requestedLogsArray = [PulseDetails]()
      var sharedLogsArray = [PulseDetails]()
      var isLogListedAreOfTypeRequest: Bool?
      var respondersArray = [RespondersData]()
      let validationController = ValidationController()
      var locationNameFromLatLong: String?
      var expandedRows = Set<Int>()
      var idtoken: String?
      var appService = AppService.shared
      let appSharedPrefernce = AppSharedPreference()
      var pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
      var floatingBtn = UIButton(frame: CGRect(origin: CGPoint(x: 100, y: 320), size: CGSize(width: 140, height: 30)))
      var shareAndRequestLoaded: Bool?
      var deletePulseProtocol: DeletePulseProtocol?
      var acceptThisPulse: String!
      var acceptThisPulseObj = PulseDetails()
      var respondToId: String!
      var acceptPulseAtIndex: Int = 0
      var declineThisPulse: String!
      var declinePulseAtIndex: Int = 0
      var searchKey: String = ""
      var unlockPulseAtIndex: Int = 0
      var respondToPulseIdIsScheduled: Bool = false
      var isPastLoglistEnabled: Bool = false
      var isFromNotification: Bool = false
      var notificationPulseId = ""
      var pulseObjToBeDeleted = PulseDetails()
    
      /******************************************* Action variables **************************************/

      //this button is shown when logbook is empty at the center of the empty logbook screen
      //which will open create pulse screen for us.
      @IBAction func createPulseBtnClicked(_ sender: Any) {
            self.openShareAndRequestPage()
      }

      //pulse logbook search button click,coveres upper view of pulse logook screen with search bar
      @IBAction func searchBtnAtTop(_ sender: Any) {
            self.seachBarForPulse.isHidden=false
            self.seachBarForPulse.setShowsCancelButton(true, animated: true)
            self.seachBarForPulse.becomeFirstResponder()
            self.seachBarForPulse.delegate = self
      }

      @IBAction func pastLogbookButtonClicked(_ sender: Any) {
            isPastLoglistEnabled = !isPastLoglistEnabled
            self.getLogs()
            if(isPastLoglistEnabled) {
                  self.PastButtonImage.image=UIImage(named: "ActiveLogbookIcon")!
                  self.pastButtonLabel.textColor=UIColor.purpleRing()
            } else {
                  self.PastButtonImage.image=UIImage(named: "LogbookIcon-1")!
                  self.pastButtonLabel.textColor=UIColor.lightGray
            }
      }

      //will take you to dashboard!!
      @IBAction func backBtnAtTop(_ sender: Any) {
            appSharedPrefernce.setAppSharedPreferences(key: "isPulseLogbook", value: false)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
            self.present(nextVC, animated: true, completion: nil)
      }

      //will take us to share and request screen to create pulse
      @IBAction func addPulseRequestOrShareBtn(_ sender: Any) {
            appSharedPrefernce.setAppSharedPreferences(key: "isPulseLogbook", value: false)
            self.openShareAndRequestPage()
      }

      //will takes us to dashboard
      @IBAction func fishEyeIconClicked(_ sender: Any) {
            appSharedPrefernce.setAppSharedPreferences(key: "isPulseLogbook", value: false)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
            self.present(nextVC, animated: true, completion: nil)
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.pastButtonLabel.text = self.pulseMsgs.pastButtonTitle
                  self.fishEyeBottomLabel.text = self.pulseMsgs.fisheyeLabel
                  self.letsWorkTogetherLabel.text = self.pulseMsgs.letsWorkTogetherLabel
                  self.pulseLabelAtTop.text = self.pulseMsgs.pulseLogbookTitle
                  self.requestBtnLabel.text = self.pulseMsgs.myReuestsTapTitle
                  self.shareBtnLabel.text = self.pulseMsgs.sharesTapTitle
                  self.creatPulseBtn.setTitle(self.pulseMsgs.createPulse, for: .normal)
                  self.youCanShareLabel.text = self.pulseMsgs.creatPulsetext
            }
      }
      /* END */

      /*  Add observers  */
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      /* END */

      /************************************* function relatd to search ************************************/

      //cancel button og search bar is clicked
      func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            self.searchKey=""
            self.logsArray=[]
            self.tableViewPulse.reloadData()
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Search pulse", label: "Pulse searcj is cancelled by clicking ob cancel button of search bar", value: 0)
            self.noPulseFoundCorrespondingToSearch.isHidden=true
            self.seachBarForPulse.endEditing(true)
            self.seachBarForPulse.resignFirstResponder()
            self.seachBarForPulse.isHidden = true
            self.tableViewPulse.isHidden=false
            self.getLogs()
      }

      //search button of search bar is clicked
      func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        var _: String = searchBar.text!
            self.seachBarForPulse.endEditing(true)
            self.logsArray=[]
            self.tableViewPulse.reloadData()
            self.getLogs()
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Search pulse", label: "search button of pulse logbook with search key : "+searchBar.text!, value: 0)
            self.seachBarForPulse.resignFirstResponder()
      }

      //search bar text is captured in globel variable
      func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            self.searchKey=self.seachBarForPulse.text!
      }

      /************************************ predefined functions ******************************************/

      override func viewDidLoad() {

            super.viewDidLoad()
            self.stopLoader()
            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.appService = AppService.shared
            self.pulseMsgs = PulseToastMsgHeadingSubheadingLabels.shared
            self.setLocalizationText()
            self.addObservers()
            GetLocationGlobally.shared.initialize()
        
            self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            appSharedPrefernce.setAppSharedPreferences(key: "isPulseLogbook", value: true)

            //initially we set these as
            self.logsArray=[]
            self.isLogListedAreOfTypeRequest=true
            self.isPastLoglistEnabled=false
            self.seachBarForPulse.isHidden = true
            self.noPulseFoundCorrespondingToSearch.isHidden=true
            self.viewContainEmptyLogBook.isHidden=true
            self.viewContainEmptyLogBook.layer.cornerRadius=8
            self.creatPulseBtn.layer.cornerRadius=6

            self.creatPulseBtn.setTitle(self.pulseMsgs.createPulse, for: .normal)

            //share and request tabs initially
            self.selectedRequestImageView.isHidden=true
            self.unselectedRequestImageView.isHidden=false
            self.selectedShareImageView.isHidden=false
            self.unselectedShareImageView.isHidden=true

            let requestBtn = UITapGestureRecognizer(target: self, action: #selector(loadRequestedLogbook(sender:)))
            requestBtn.numberOfTapsRequired = 1
            self.requestBtnView.addGestureRecognizer(requestBtn)

            let shareBtn = UITapGestureRecognizer(target: self, action: #selector(loadSharedLogbook(sender:)))
            shareBtn.numberOfTapsRequired = 1
            self.shareBtnView.addGestureRecognizer(shareBtn)

            self.requestBtnLabel.textColor=UIColor.purpleRing()
            self.shareBtnLabel.textColor=UIColor.white

            self.applyViewShadow()
            self.getLogs()
            self.PastButtonImage.image=UIImage(named: "LogbookIcon-1")!

            self.tableViewPulse.delegate = self
            self.tableViewPulse.dataSource = self
            self.tableViewPulse.isHidden=false
        self.tableViewPulse.rowHeight = UITableView.automaticDimension

            if self.isFromNotification && self.notificationPulseId != ""{
                  let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
                  let pulseDetailsViewController = storyboard.instantiateViewController(withIdentifier: "PulseDetailsViewController") as! PulseDetailsViewController

                  pulseDetailsViewController.pulseIdOfWhichDetailsShown = self.notificationPulseId
                  pulseDetailsViewController.isPulsed=false
                  pulseDetailsViewController.view.frame = self.view.bounds
                self.addChild(pulseDetailsViewController)
                pulseDetailsViewController.didMove(toParent: self)
                  self.view.addSubview(pulseDetailsViewController.view)

                  UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        pulseDetailsViewController.view.frame.origin.y = self.view.bounds.origin.y
                  }, completion: nil)
            }
            self.isFromNotification = false
            self.notificationPulseId = ""
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.PulseListingViewController)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      /********************************************* table view functions ***********************************/

      //to have number cells equal to size of logsArray
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return logsArray.count
      }

      //To assign data to xib file outlets from logsArray
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let pulseObj = self.logsArray[indexPath.row]

            let cell=Bundle.main.loadNibNamed("PulseLogbookCustomViewCell", owner: self, options: nil)?.first as! PulseLogbookCustomViewCell
            cell.requestCapLAbel.text=self.pulseMsgs.capLabel

            cell.shareCapLabel.text=self.pulseMsgs.capLabel

            //formatting cell before loading
            cell.hisPulseImage.layer.cornerRadius = cell.hisPulseImage.frame.size.width / 2
            cell.hisPulseImage.clipsToBounds = true
            cell.firstParticipentImage.layer.cornerRadius=cell.firstParticipentImage.frame.size.width / 2
            cell.firstParticipentImage.clipsToBounds = true
            cell.firstParticipentImage.layer.borderWidth = 1.5
            cell.secondparticipentImage.layer.cornerRadius=cell.secondparticipentImage.frame.size.width / 2
            cell.secondparticipentImage.clipsToBounds = true
            cell.secondparticipentImage.layer.borderWidth = 1.5
            cell.thirdParticipantImage.layer.cornerRadius=cell.thirdParticipantImage.frame.size.width / 2
            cell.thirdParticipantImage.clipsToBounds = true
            cell.thirdParticipantImage.layer.borderWidth = 1.5
            cell.fourthParticipentImage.layer.cornerRadius=cell.fourthParticipentImage.frame.size.width / 2
            cell.fourthParticipentImage.clipsToBounds = true
            cell.fourthParticipentImage.layer.borderWidth = 1.5

            //on single tap expand log
            let tapGestureCloseButton = UITapGestureRecognizer(target: self, action: #selector(openClosePulseDetailsViewOnLogbook(sender:)))
            tapGestureCloseButton.numberOfTapsRequired = 1
            cell.pulseTitleView.addGestureRecognizer(tapGestureCloseButton)

            //on double tap open pulse deatils for tapped log
            let tapGestureRespond = UITapGestureRecognizer(target: self, action: #selector(self.openPulseDetailsViewController(sender:)))
            tapGestureRespond.numberOfTapsRequired = 2
            cell.pulseTitleView.addGestureRecognizer(tapGestureRespond)
            tapGestureCloseButton.require(toFail: tapGestureRespond)

            var pulseRequesterImageUrl=pulseObj.pulseRequesterImage

            cell.pulseTitleView.tag = indexPath.row
//            cell.hisPulseImage?.sd_setImage(with: URL(string: pulseRequesterImageUrl?.addingPercentEncoding(withAllowedCharacters: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            cell.hisPulseImage?.sd_setImage(with: URL(string: pulseRequesterImageUrl?.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
            cell.isPulsed=pulseObj.isPulsed
            cell.pulseTitleLabel.text = pulseObj.alertMessage
            cell.pulseType=pulseObj.pulseType
            cell.isScheduled=pulseObj.isScheduled

            //time assignments by checking pulse is scheduled or not
            if(pulseObj.isScheduled) {
                var oldDate=validationController.convert_date_from_UTC_time(givenUTCdate:
                    pulseObj.pulseEndDate!)
               
                if(oldDate=="01/01/1970") {
                    cell.pulseStartDate.text=validationController.convert_date_from_UTC_time(givenUTCdate:
                        pulseObj.pulseStartDate!)
                    cell.betweenSDandEDLabel.text=self.pulseMsgs.labelBetweenEDandPT
                    cell.pulseEndDate.text=validationController.convert_time_from_UTC_time(givenUTCtime:
                        pulseObj.pulseTime!)
                    cell.betweenEDandTime.text=""
                    cell.pulseShareOrRequestTime.text=""
                } else {
                    cell.pulseStartDate.text=validationController.convert_date_from_UTC_time(givenUTCdate:
                        pulseObj.pulseStartDate!)
                    cell.betweenSDandEDLabel.text=self.pulseMsgs.labelBetweenSDandED
                    cell.pulseEndDate.text=validationController.convert_date_from_UTC_time(givenUTCdate:
                        pulseObj.pulseEndDate!)
                    cell.betweenEDandTime.text=self.pulseMsgs.labelBetweenEDandPT
                    cell.pulseShareOrRequestTime.text=validationController.convert_time_from_UTC_time(givenUTCtime:
                        pulseObj.pulseTime!)
                }
            } else if(!pulseObj.isScheduled) {
                 DispatchQueue.main.async {
                    cell.pulseStartDate.text=self.validationController.convert_date_from_UTC_time_fromStringIO(givenUTCdate: pulseObj.createdTime!)
                  cell.betweenSDandEDLabel.text=self.pulseMsgs.labelBetweenEDandPT
                    cell.pulseEndDate.text=self.validationController.convert_time_from_UTC_time_fromStringIO(givenUTCtime: pulseObj.createdTime!)
                  cell.betweenEDandTime.text=""
                  cell.pulseShareOrRequestTime.text=""
                }
            }

            //-----------------------------------------------pulse request-----------------------------------------//
            //pulse for this cell is of type request
            if(pulseObj.pulseType=="0") {
                  //pulse type is request and it is by logged in user(i.e pulsed)
                  if(pulseObj.isPulsed) {
                        cell.acceptPulseButton.isHidden=true
                        cell.declinePulseBtn.isHidden=true
                  } else {
                        //Pulse type is request and it is not by the logged in user(i.e pulsed)
                        //therefore user will be accept or decline
                        if(pulseObj.respondersArray.count>0) {
                              let isAccepted=pulseObj.respondersArray[0].isAccepted!
                              let isDeclined=pulseObj.respondersArray[0].isDeclined!
                              let isResponded=pulseObj.respondersArray[0].isResponded!
                              //Pulse request not created by logged in user is scheduled,user will be able to accept or decline
                              //till pulse gets expired or can change accept decline prefrence till pulse get expired
                              if(pulseObj.isScheduled) {
                                    let isActive=pulseObj.isActive!
                                    //if scheduled pulse is active then only allow user will be to accept or decline the pulse
                                    if(isActive) {
                                          //here the scheduled pulse is active
                                          if(isAccepted && !isDeclined && isResponded) {
                                                cell.acceptPulseButton.isHidden=false
                                                cell.declinePulseBtn.isHidden=false
                                                cell.acceptPulseButton.setImage(UIImage(named: "AcceptedFilled"), for: .normal)
                                                cell.acceptPulseButton.isUserInteractionEnabled=false
                                                cell.acceptPulseButton.tag=indexPath.row
                                                cell.declinePulseBtn.tag=indexPath.row
                                                let tapAlreadyAcceptedDecline = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseDeclineBtnClicked(sender:)))
                                                cell.declinePulseBtn.isUserInteractionEnabled=true
                                                tapAlreadyAcceptedDecline.numberOfTapsRequired = 1
                                                cell.declinePulseBtn.addGestureRecognizer(tapAlreadyAcceptedDecline)
                                          } else if(!isAccepted && isDeclined && isResponded) {
                                                cell.acceptPulseButton.isHidden=false
                                                cell.declinePulseBtn.isHidden=false
                                                cell.acceptPulseButton.tag=indexPath.row
                                                cell.declinePulseBtn.tag=indexPath.row
                                                cell.declinePulseBtn.setImage(UIImage(named: "DeclinedFilled"), for: .normal)
                                                cell.declinePulseBtn.isUserInteractionEnabled=false

                                                cell.acceptPulseButton.isUserInteractionEnabled=true
                                                let tapAlreadyDeclinededAccept = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseAcceptBtnClicked(sender:)))
                                                tapAlreadyDeclinededAccept.numberOfTapsRequired = 1
                                                cell.acceptPulseButton.addGestureRecognizer(tapAlreadyDeclinededAccept)
                                          } else {
                                                cell.acceptPulseButton.isHidden=false
                                                cell.declinePulseBtn.isHidden=false
                                                cell.acceptPulseButton.tag=indexPath.row
                                                cell.declinePulseBtn.tag=indexPath.row
                                                let tapAcceptBtn = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseAcceptBtnClicked(sender:)))
                                                tapAcceptBtn.numberOfTapsRequired = 1
                                                cell.acceptPulseButton.addGestureRecognizer(tapAcceptBtn)

                                                let tapDeclineBtn = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseDeclineBtnClicked(sender:)))
                                                tapDeclineBtn.numberOfTapsRequired = 1
                                                cell.declinePulseBtn.addGestureRecognizer(tapDeclineBtn)

                                                cell.acceptedRejectedStatusLabel.isHidden=false
                                          }
                                    } else {//here the scheduled pulse is expired
                                          cell.acceptedRejectedStatusLabel.isHidden=false
                                          cell.acceptedRejectedStatusLabel.text=self.pulseMsgs.pulseExpired
                                          cell.acceptPulseButton.isHidden=true
                                          cell.declinePulseBtn.isHidden=true
                                    }
                              } else {//pulse is of type request,not scheudeled,not created by user
                                    if(isAccepted && !isDeclined && isResponded) {
                                          cell.acceptPulseButton.isHidden=true
                                          cell.declinePulseBtn.isHidden=true
                                          cell.acceptedRejectedStatusLabel.text=self.pulseMsgs.pulseAccepted
                                    } else if(!isAccepted && isDeclined && isResponded) {
                                          cell.acceptPulseButton.isHidden=true
                                          cell.declinePulseBtn.isHidden=true
                                          cell.acceptedRejectedStatusLabel.text=self.pulseMsgs.pulseDeclined
                                    } else {
                                          cell.acceptPulseButton.isHidden=false
                                          cell.declinePulseBtn.isHidden=false
                                          let tapAcceptBtn = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseAcceptBtnClicked(sender:)))
                                          tapAcceptBtn.numberOfTapsRequired = 1
                                          cell.acceptPulseButton.addGestureRecognizer(tapAcceptBtn)
                                          cell.acceptPulseButton.tag=indexPath.row
                                          let tapDeclineBtn = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseDeclineBtnClicked(sender:)))
                                          tapDeclineBtn.numberOfTapsRequired = 1
                                          cell.declinePulseBtn.addGestureRecognizer(tapDeclineBtn)
                                          cell.declinePulseBtn.tag=indexPath.row
                                          cell.acceptedRejectedStatusLabel.isHidden=false
                                    }
                              }//for now ends
                        }
                  }//not by user ends

                  if(pulseObj.isEncrypted) {//if requested pulse capsule is encrpted then
                        let reqCapMasterKeyBtn = UITapGestureRecognizer(target: self, action: #selector(self.actionOnReqCapMasterKeyBtnClicked(sender:)))
                        reqCapMasterKeyBtn.numberOfTapsRequired = 1
                        cell.requestedCapsuleMasterKeyButton.addGestureRecognizer(reqCapMasterKeyBtn)
                        cell.requestedCapsuleMasterKeyButton.isUserInteractionEnabled=true
                        cell.requestedCapsuleLabel.text=self.pulseMsgs.capLockNeedMasterKey
                        cell.requestedCapsuleMasterKeyButton.tag = indexPath.row
                        cell.requestedCapMasterKeyImageView.image=UIImage(named: "masterKeyLock")!
                  } else {//requested pulse capsule is not encrypted
                        cell.requestedCapsuleMasterKeyButton.isUserInteractionEnabled=false
                        if(pulseObj.message=="") {
                              cell.requestedCapsuleLabel.text=self.pulseMsgs.emptyCapWhileRequestingPulse
                        } else {
                              cell.requestedCapsuleLabel.text=pulseObj.message
                        }
                  }
            }//pulse type 0 ends

            //---------------------------------------------- pulse share -----------------------------------------//
            if(pulseObj.pulseType=="1") {
                  //For destination on map
                  let annotation = MKPointAnnotation()
                  let lat = (pulseObj.latitude! as NSString).doubleValue
                  let long = (pulseObj.longitude! as NSString).doubleValue
                  annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                  annotation.title = "Location"
                  cell.mapViewInPulseLog.delegate = self
                  cell.mapViewInPulseLog.addAnnotation(annotation)
                  //cell.mapViewInPulseLog.showAnnotations([annotation], animated: true)
                  cell.mapViewInPulseLog.showsScale = true
                  cell.acceptPulseButton.isHidden=true
                  cell.declinePulseBtn.isHidden=true
                  cell.locationNameLabel.text=pulseObj.locationName

                  if(pulseObj.isEncrypted) {//if shared pulse capsule message is encrypted
                        let shareCapMasterKeyBtn = UITapGestureRecognizer(target: self, action: #selector(self.actionOnReqCapMasterKeyBtnClicked(sender:)))
                        shareCapMasterKeyBtn.numberOfTapsRequired = 1
                        cell.sharedCapsuleMasterKeyButton.addGestureRecognizer(shareCapMasterKeyBtn)
                        cell.sharedCapsuleLabel.text=self.pulseMsgs.capLockNeedMasterKey
                        cell.sharedCapsuleMasterKeyButton.tag = indexPath.row
                        cell.sharedCapMasterKeyImageView.image=UIImage(named: "masterKeyLock")!
                  } else {
                        //if shared pulse capsule message in not encrypted
                        if(pulseObj.message=="") {
                              cell.sharedCapsuleLabel.text=self.pulseMsgs.emptyCapWhileSharingPulse
                        } else {
                              cell.sharedCapsuleLabel.text=pulseObj.message
                        }
                  }
            }//pulse type 1 ends here

            cell.participentNumber=pulseObj.respondersArray.count
            cell.respondersArray=pulseObj.respondersArray
            cell.isExpanded = self.expandedRows.contains(indexPath.row)
            return cell
      }

      func numberOfSections(in tableView: UITableView) -> Int {
            return 1
      }

      /*********************************************** helping functions *************************************/

      func applyViewShadow() {
            self.viewEqualToTableView.layer.cornerRadius = 8
            self.viewEqualToTableView.layer.borderWidth = 0
            self.viewEqualToTableView.layer.borderColor = UIColor.black.cgColor
            self.viewEqualToTableView.layer.shadowColor = UIColor.black.cgColor
            self.viewEqualToTableView.layer.shadowOffset = CGSize(width: 3, height: 3)
            self.viewEqualToTableView.layer.shadowOpacity = 0.7
            self.viewEqualToTableView.layer.shadowRadius = 10.0
      }

      //to expand or shrink particular cell of pulse logbook table
    @objc func openClosePulseDetailsViewOnLogbook(sender: Any) {

            let index = (sender as AnyObject).view!.tag
            let indexPath = IndexPath(row: index, section: 0)
            guard let cell = tableViewPulse.cellForRow(at: indexPath ) as? PulseLogbookCustomViewCell
                  else { return }

            switch cell.isExpanded {
            case true:
                  self.expandedRows.remove(((indexPath as AnyObject).row)!)
                  break
            case false:
                  self.expandedRows.insert(((indexPath as AnyObject).row)!)
                  break
            }

            cell.isExpanded = !cell.isExpanded
            self.tableViewPulse.beginUpdates()
            self.tableViewPulse.endUpdates()

            let cellRect = self.tableViewPulse.rectForRow(at: indexPath)
            let completelyVisible = self.tableViewPulse.bounds.contains(cellRect)

            if(!completelyVisible) {
                  DispatchQueue.main.async {
                        self.tableViewPulse.scrollToRow(at: indexPath, at: .bottom, animated: true)
                  }
            }
      }

    @objc func loadSharedLogbook(sender: Any) {
            self.expandedRows.removeAll()
            self.logsArray=[]
            self.tableViewPulse.reloadData()
            self.viewContainEmptyLogBook.isHidden=true
            self.isLogListedAreOfTypeRequest=false
            self.selectedRequestImageView.isHidden=false
            self.unselectedRequestImageView.isHidden=true
            self.selectedShareImageView.isHidden=true
            self.unselectedShareImageView.isHidden=false
            self.requestBtnLabel.textColor=UIColor.white
            self.shareBtnLabel.textColor=UIColor.purpleRing()
            self.getLogs()
      }

    @objc func loadRequestedLogbook(sender: Any) {
            self.expandedRows.removeAll()
            self.logsArray=[]
            self.tableViewPulse.reloadData()
            self.viewContainEmptyLogBook.isHidden=true
            self.isLogListedAreOfTypeRequest=true
            self.selectedRequestImageView.isHidden=true
            self.unselectedRequestImageView.isHidden=false
            self.selectedShareImageView.isHidden=false
            self.unselectedShareImageView.isHidden=true
            self.requestBtnLabel.textColor=UIColor.purpleRing()
            self.shareBtnLabel.textColor=UIColor.white
            self.getLogs()
      }

    @objc func actionOnPulseAcceptBtnClicked(sender: Any) {
            let index = (sender as AnyObject).view!.tag
        
            let indexPath = IndexPath(row: index, section: 0)
            var pulseObjToBeDeclinedOrAccepted=PulseDetails()
            pulseObjToBeDeclinedOrAccepted=self.logsArray[indexPath.row]
            self.acceptThisPulse=pulseObjToBeDeclinedOrAccepted.pulseId
            self.acceptThisPulseObj=pulseObjToBeDeclinedOrAccepted
            self.respondToId=pulseObjToBeDeclinedOrAccepted.fromId
            self.respondToPulseIdIsScheduled = pulseObjToBeDeclinedOrAccepted.isScheduled!
            self.acceptPulseAtIndex=indexPath.row
        
            self.tellWhatAcceptMeans()
      }

    @objc func actionOnPulseDeclineBtnClicked(sender: Any) {
            let index = (sender as AnyObject).view!.tag
            let indexPath = IndexPath(row: index, section: 0)
            var pulseObjToBeDeclinedOrAccepted=PulseDetails()
            pulseObjToBeDeclinedOrAccepted=self.logsArray[indexPath.row]
            self.declineThisPulse=pulseObjToBeDeclinedOrAccepted.pulseId
            self.declinePulseAtIndex=indexPath.row
            self.respondToPulseIdIsScheduled = pulseObjToBeDeclinedOrAccepted.isScheduled!
            self.confirmPulseDeclination()
      }

      //when requested capsule master key button is clicked
    @objc func actionOnReqCapMasterKeyBtnClicked(sender: Any) {
            let index = (sender as AnyObject).view!.tag
            let indexPath = IndexPath(row: index, section: 0)
            self.unlockPulseAtIndex=indexPath.row
            self.openMasterKeyPopUp()
      }

      func showUnlockedCapsule() {
            let index = IndexPath(row: self.unlockPulseAtIndex, section: 0)
            let cell = (self.tableViewPulse.cellForRow(at: index) as? PulseLogbookCustomViewCell)!
            var pulseObjToBeUnlocked=PulseDetails()
            pulseObjToBeUnlocked=self.logsArray[index.row]
            self.logsArray[index.row].isEncrypted=false
            cell.sharedCapMasterKeyImageView.image=UIImage(named: "masterKeyUnlocked")!
            cell.requestedCapMasterKeyImageView.image=UIImage(named: "masterKeyUnlocked")!
            if(pulseObjToBeUnlocked.pulseType=="0") {
                  cell.requestedCapsuleLabel.text=pulseObjToBeUnlocked.message
            } else {
                  cell.sharedCapsuleLabel.text=pulseObjToBeUnlocked.message
            }
      }

      func comingBackToListingAfetrDecliningPulse() {
            let index = IndexPath(row: self.declinePulseAtIndex, section: 0)
            self.logsArray[index.row].respondersArray [0].isDeclined=true
            self.logsArray[index.row].respondersArray [0].isAccepted=false
            self.logsArray[index.row].respondersArray [0].isResponded=true
            DispatchQueue.main.async {
                  let cell = (self.tableViewPulse.cellForRow(at: index) as? PulseLogbookCustomViewCell)!
                  if(self.logsArray[index.row].isScheduled) {
                        cell.acceptPulseButton.isHidden = false
                        cell.declinePulseBtn.isHidden = false
                        cell.declinePulseBtn.setImage(UIImage(named: "DeclinedFilled"), for: .normal)
                        cell.declinePulseBtn.isUserInteractionEnabled=false
                        cell.acceptPulseButton.setImage(UIImage(named: "AcceptedHollow"), for: .normal)
                        cell.acceptPulseButton.isUserInteractionEnabled=true
                        let tapAlreadyDeclinededAccept = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseAcceptBtnClicked(sender:)))
                        tapAlreadyDeclinededAccept.numberOfTapsRequired = 1
                        cell.acceptPulseButton.addGestureRecognizer(tapAlreadyDeclinededAccept)
                        cell.acceptedRejectedStatusLabel.isHidden=true
                  } else {
                        cell.acceptPulseButton.isHidden = true
                        cell.declinePulseBtn.isHidden = true
                        cell.acceptedRejectedStatusLabel.isHidden=false
                        cell.acceptedRejectedStatusLabel.text=self.pulseMsgs.pulseDeclined
                  }
            }
      }

      func comingBackToListingAfetrAcceptingPulse() {
      //  DispatchQueue.main.async {
            self.stopLoader()
       // }
        
            let index = IndexPath(row: self.acceptPulseAtIndex, section: 0)
            self.logsArray[index.row].respondersArray [0].isDeclined=false
            self.logsArray[index.row].respondersArray [0].isAccepted=true
            self.logsArray[index.row].respondersArray [0].isResponded=true
            let cell = (self.tableViewPulse.cellForRow(at: index) as? PulseLogbookCustomViewCell)!
            if(self.logsArray[index.row].isScheduled) {
                  cell.acceptPulseButton.isHidden = false
                  cell.declinePulseBtn.isHidden = false
                  cell.acceptPulseButton.setImage(UIImage(named: "AcceptedFilled"), for: .normal)
                  cell.acceptPulseButton.isUserInteractionEnabled=false
                  cell.declinePulseBtn.setImage(UIImage(named: "DeclinedHollow"), for: .normal)
                  cell.declinePulseBtn.isUserInteractionEnabled=true
                  let tapAlreadyAcceptedDecline = UITapGestureRecognizer(target: self, action: #selector(self.actionOnPulseDeclineBtnClicked(sender:)))
                  tapAlreadyAcceptedDecline.numberOfTapsRequired = 1
                  cell.declinePulseBtn.addGestureRecognizer(tapAlreadyAcceptedDecline)
                  cell.acceptedRejectedStatusLabel.isHidden=true
            } else {
                  cell.acceptPulseButton.isHidden = true
                  cell.declinePulseBtn.isHidden = true
                  cell.acceptedRejectedStatusLabel.isHidden=false
                  cell.acceptedRejectedStatusLabel.text=self.pulseMsgs.pulseAccepted
            }
      }

      func showThisWhenNothingForLogbook() {
            self.tableViewPulse.isHidden=true
            self.viewContainEmptyLogBook.isHidden=false
            self.creatPulseBtn.isHidden=false
            //self.youCanRequestLabel.isHidden=false
            self.youCanShareLabel.isHidden=false
            self.emptyPulseLogbookImageView.isHidden=false
      }

      func startLoader() {
            self.requestBtnView.isUserInteractionEnabled=false
            self.shareBtnView.isUserInteractionEnabled=false
            self.searchPulseButton.isUserInteractionEnabled=false
            self.tableViewPulse.isScrollEnabled=false
        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
      }

      func stopLoader() {
       
            DispatchQueue.main.async {
                  self.viewEqualToTableView.isUserInteractionEnabled=true
                  self.tableViewPulse.isScrollEnabled=true
                  self.requestBtnView.isUserInteractionEnabled=true
                  self.shareBtnView.isUserInteractionEnabled=true
                  self.searchPulseButton.isUserInteractionEnabled=true
                  ScreenLoader.shared.stopLoader()
            }
      }

      func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
            // this is where visible maprect should be set
            mapView.showAnnotations(mapView.annotations, animated: false)
      }

      func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            if !(annotation is MKPointAnnotation) {
                  return nil
            }

            let annotationIdentifier = "AnnotationIdentifier"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)

            if annotationView == nil {
                  annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
                  annotationView!.canShowCallout = true
            } else {
                  annotationView!.annotation = annotation
            }

            let pinImage = UIImage(named: "speedLocationIcon-1")
            annotationView!.image = pinImage
            annotationView?.frame.size = CGSize(width: 33.0, height: 33.0)

            return annotationView
      }

      func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

            if overlay is MKPolyline {
                  let renderer = MKPolylineRenderer(overlay: overlay)
                  renderer.strokeColor = UIColor.acceptGreen()
                  renderer.lineDashPattern = [5, 5]
                  renderer.lineWidth = 2
                  return renderer
            }

            return MKOverlayRenderer()
      }

      /***************************************** Pop-up related functions ***********************************/

      //a common alert pop-up
      func popUpVCController(addresponderFlag: Bool) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "AllPopupDualActionVC") as! AllPopupDualActionVC
            nextVC.pulseListingProtocol = self as PulseListingProtocol
            self.present(nextVC, animated: true, completion: nil)
      }

      func dualActionPopUp(heading: String, subheading: String, action: String, method: String, module: String, addresponder: Bool) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.popUpVCController(addresponderFlag: addresponder)
      }

      func tellWhatAcceptMeans() {
            self.dualActionPopUp(heading: self.pulseMsgs.tellAcceptMeansHeading, subheading: self.pulseMsgs.tellAcceptMeansSubHeading, action: "acceptPulse", method: "acceptPulse", module: "NA", addresponder: false)
      }

      func confirmPulseDeclination() {
            self.dualActionPopUp(heading: self.pulseMsgs.tellDeclineMeansHeading, subheading: self.pulseMsgs.tellDeclineMeansSubHeading, action: "declinePulseAtList", method: "declinePulseAtList", module: "NA", addresponder: false)
      }

      func showToastMsg(toastMsg: String, action: Int) {
            DispatchQueue.main.async {
                  self.view.makeToast(toastMsg)
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+PulseToastMsgHeadingSubheadingLabels.shared.toastDelayTimeForPulse, execute: {
                  self.view.hideToast()
            })
      }

      //user tries to respond to such a requester which is not in his contact list,then need to alert the user
      func addContactToYourContactListToRespond() {
            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Accept Pulse", action: "Add requester to your contact list from pulse list", label: "Add pulse requester in your contact list to respond to his pulse request.", value: 0)
            self.dualActionPopUp(heading: self.pulseMsgs.addContactHeading, subheading: self.pulseMsgs.addContactSubHeading, action: "warning", method: "addReqContToRespToPulseReq", module: "NA", addresponder: true )
      }

      /************************************* rediecting to another page functions *******************************/

      func  openInsertCapsulePopUp() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "capsuleInsertPopUpViewController")  as! capsuleInsertPopUpViewController
            nextVC.pulseListingProtocol = self as PulseListingProtocol
            nextVC.fromAcceptPulse=true
            nextVC.acceptThisPulse=self.acceptThisPulse
            nextVC.acceptThisPulseObj=self.acceptThisPulseObj
            nextVC.respondToId=self.respondToId
            nextVC.fromListingWhileAccepting=true
            nextVC.respondToPulseIdIsScheduled=self.respondToPulseIdIsScheduled
            self.present(nextVC, animated: true, completion: nil)
      }

      func  openShareAndRequestPage() {
            if(self.shareAndRequestLoaded==nil) {
                  self.shareAndRequestLoaded=true
                  let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
                  let nextVC = storyBoard.instantiateViewController(withIdentifier: "ShareAndRequestPulseVCViewController")  as! ShareAndRequestPulseVCViewController
                nextVC.reloadPulseLogbook = self as ReloadPulseLogbook
                  nextVC.view.frame = CGRect.init(x: self.view.bounds.origin.x, y: self.view.bounds.origin.y, width: self.view.bounds.size.width, height: self.view.bounds.size.height) //self.view.bounds
                  nextVC.view.clipsToBounds = true
                  nextVC.view.isUserInteractionEnabled = true
                self.addChild(nextVC)
                  self.shareAndRequestLoaded=true
                nextVC.didMove(toParent: self)
                  self.view.addSubview(nextVC.view)
                  UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        nextVC.view.frame.origin.y = self.view.bounds.origin.y
                  }, completion: nil)
            } else {

            }
      }

      func  openMasterKeyPopUp() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController")  as! MasterKeyPopUpViewController
            nextVC.fromUnlockCapsule=true
            nextVC.pulseListingProtocol = self as PulseListingProtocol
            self.present(nextVC, animated: true, completion: nil)
      }

    
    @objc func openPulseDetailsViewController(sender: Any) {
            let storyboard = UIStoryboard.init(name: "Pulse", bundle: nil)
            let pulseDetailsViewController = storyboard.instantiateViewController(withIdentifier: "PulseDetailsViewController") as! PulseDetailsViewController
            let index = (sender as AnyObject).view!.tag
            let indexPath = IndexPath(row: index, section: 0)
            pulseDetailsViewController.pulseIdOfWhichDetailsShown=self.logsArray[indexPath.row].pulseId
            pulseDetailsViewController.isPulsed=self.logsArray[indexPath.row].isPulsed
            pulseDetailsViewController.view.frame = self.view.bounds
        self.addChild(pulseDetailsViewController)
        pulseDetailsViewController.didMove(toParent: self)
            self.view.addSubview(pulseDetailsViewController.view)

            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                  pulseDetailsViewController.view.frame.origin.y = self.view.bounds.origin.y
            }, completion: nil)
      }

    
      /********************************************* API calls *******************************************/

      func updateAcceptRejectStatus(pulseId: String, isAccepted: Bool) {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, action: 0)
                        return
            }
            self.startLoader()
            let feedbackOperation = DeclineAPulseRequest(isAccepted: false, pulseToBeDeclined: pulseId)
            feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
                  self.stopLoader()
                  if(operation.statusCode == "200") {
                        self.comingBackToListingAfetrDecliningPulse()
                        self.showToastMsg(toastMsg: self.pulseMsgs.pulseReqDeclined, action: 0)
                  } else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
                  }
            }
            AppDelegate.addProcedure(operation: feedbackOperation)
      }
    

      func getLogs() {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        DispatchQueue.main.async {
                            self.logsArray=[]
                            self.tableViewPulse.isHidden=true
                            self.noPulseFoundCorrespondingToSearch.isHidden=false
                            self.noPulseFoundCorrespondingToSearch.text=self.pulseMsgs.checkInternetConnection
                            self.tableViewPulse.reloadData()
                        }
                        return
            }

            self.noPulseFoundCorrespondingToSearch.isHidden=true
            self.startLoader()

            var headers: [String: Any] = [:]

            var awsURL: String = " "

            if(self.isLogListedAreOfTypeRequest! && !isPastLoglistEnabled) {
                  //only my requested logs
                  awsURL=AppConfig.getRequestTypePulsesAPI
            } else if(!(self.isLogListedAreOfTypeRequest!) && !isPastLoglistEnabled) {
                  //not my requested (i.e shared)
                  awsURL=AppConfig.getShareTypeOfPulsesAPI
            } else if(self.isLogListedAreOfTypeRequest! && isPastLoglistEnabled) {
                  //my requested logs which a requested in past
                  awsURL=AppConfig.getPastRequestedPulseAPI
            } else if(!(self.isLogListedAreOfTypeRequest!) && isPastLoglistEnabled) {
                  //not my requested (i.e shared) which shared in past
                  awsURL=AppConfig.getPastSharedPulseAPI
            }

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.addValue(self.idtoken!, forHTTPHeaderField: "idtoken")
            request.addValue(self.searchKey, forHTTPHeaderField: "searchKey")
            request.httpBody = "".data(using: String.Encoding.utf8)
            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }
        
            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                    
                        self.stopLoader()
                        var logCount: Int = 0
                        if let data = json as? [String: Any] {
                              self.logsArray=[]
                              let statusCodeRecieved = data["statusCode"] as? String ?? ""
                              if(statusCodeRecieved == "0" || statusCodeRecieved == "200") {
                                    if let pulseList = data["logs"] as? [[String: Any]] {
                                          logCount=pulseList.count

                                          for pulseDataObj in pulseList {
                                                if let pulseObj = pulseDataObj as? [String: Any] {
                                                      var pulseData = PulseDetails()
                                                      var respondersObj=RespondersData()
                                                      var dayReapterArray=[String]()
                                                      //String values
                                                      pulseData.pulseId = pulseObj["pulseId"] as? String ?? ""
                                                      pulseData.fromId =  pulseObj["requesterId"] as? String ?? ""
                                                      pulseData.pulseType =  pulseObj["pulseType"] as? String ?? ""
                                                      pulseData.requesterMessage = pulseObj["requesterMessage"] as? String ?? ""
                                                      pulseData.responserMessage = pulseObj["responserMessage"] as? String ?? ""
                                                      pulseData.isPulsed = pulseObj["isPulsed"] as? Bool ?? false
                                                      pulseData.message = pulseObj["message"] as? String ?? ""
                                                      pulseData.pulseRequesterName=pulseObj["requesterName"] as? String ?? ""
                                                      pulseData.pulseRequesterImage=pulseObj["requesterImage"] as? String ?? ""
                                                      pulseData.latitude = pulseObj["latitude"]  as? String ?? ""
                                                      pulseData.longitude = pulseObj["longitude"]  as? String ?? ""
                                                      pulseData.locationName = pulseObj["locationName"] as? String ?? ""
                                                      pulseData.createdTime =  pulseObj["createdTimeStamp"] as? String ?? ""
                                                      //Double values
                                                      pulseData.pulseTime = pulseObj["pulseTime"] as? Double ?? 0.0
                                                      pulseData.pulseStartDate = pulseObj["pulseStartDate"] as? Double ?? 0.0
                                                      pulseData.pulseEndDate = pulseObj["pulseEndDate"] as? Double ?? 0.0
                                                      //Boolean values
                                                      pulseData.isEncrypted = pulseObj["isEncrypted"] as? Bool ?? false
                                                      pulseData.isScheduled = pulseObj["isScheduled"] as? Bool ?? false
                                                    
                                                      pulseData.isActive = pulseObj["isActive"] as? Bool ?? false
                                                    
                                                    if(pulseData.isPulsed){
                                                        pulseData.alertMessage = pulseObj["requesterMessage"] as? String ?? ""
                                                    }else{
                                                        pulseData.alertMessage = pulseObj["responserMessage"] as? String ?? ""
                                                    }

                                                      if let dayRepeatersArray=pulseObj["pulseRepeatersForDays"] as? [[String]] {
                                                            for dayForPulse in dayReapterArray {
                                                                  pulseData.pulseRepeatersForDays.append(dayForPulse)
                                                            }
                                                      }
                                                      if let respondersArray = pulseObj["pulseResponderDataList"] as? [[String: Any]] {
                                                            for responderObject in respondersArray {
                                                                  if let responderData = responderObject as? [String: Any] {
                                                                        respondersObj.entryId = responderData["entryId"] as? String  ?? ""
                                                                        respondersObj.contactId = responderData["contactId"] as? String ?? ""
                                                                        respondersObj.isAccepted = responderData["isAccepted"] as? Bool ?? false
                                                                        respondersObj.isDeclined = responderData["isDeclined"] as? Bool ?? false
                                                                        respondersObj.isResponded = responderData["isResponded"] as? Bool ?? false
                                                                        respondersObj.isRead = responderData["isRead"] as? Bool ?? false
                                                                        respondersObj.isFisheye = responderData as? Bool ?? false
                                                                        respondersObj.isDeleted = responderData["isDeleted"] as? Bool ?? false
                                                                        respondersObj.pulseId = responderData["pulseId"] as? String ?? ""
                                                                        respondersObj.responderImage = responderData["responderImage"] as? String ?? ""
                                                                        respondersObj.responderEmail = responderData["responderEmail"] as? String ?? ""
                                                                        respondersObj.responderPhoneNumber = responderData["responderPhoneNumber"] as? String ?? ""
                                                                        let responderName = responderData["responderName"] as? String ?? ""
                                                                        if(responderName.isEmpty || responderName=="" || responderName==" ") {
                                                                              respondersObj.responderName="Unknown"
                                                                        } else {
                                                                              respondersObj.responderName=responderName
                                                                        }
                                                                        pulseData.respondersArray.append(respondersObj)
                                                                  }
                                                            }
                                                      }
                                                      self.logsArray.append(pulseData)
                                                }
                                          }
                                    }

                                    DispatchQueue.main.async {

                                          self.viewContainEmptyLogBook.isHidden=true
                                          self.tableViewPulse.isHidden=false
                                          self.tableViewPulse.reloadData()

                                          if(logCount==0 && self.searchKey=="") {
                                                self.stopLoader()
                                                self.showThisWhenNothingForLogbook()
                                                if(self.isLogListedAreOfTypeRequest)! {
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get request type of pulse logbook", label: "Empty Request type pulse logbook found without a search key", value: 0)
                                                } else {
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get share type of pulse logbook", label: "Empty share type pulse logbook found without a search key", value: 0)
                                                }

                                          } else if(logCount==0 && !(self.searchKey=="")) {
                                                self.noPulseFoundCorrespondingToSearch.isHidden=false
                                                self.noPulseFoundCorrespondingToSearch.text=self.pulseMsgs.noPulseCorrSearchKey+self.searchKey
                                                self.tableViewPulse.isHidden=true

                                                if(self.isLogListedAreOfTypeRequest)! {
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get request type of pulse logbook", label: "Empty Request type pulse logbook found without a search key", value: 0)
                                                } else {
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get share type of pulse logbook", label: "Empty share type pulse logbook found without a search key", value: 0)
                                                }
                                          } else if(logCount>0) {
                                                if(self.isLogListedAreOfTypeRequest)! {
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get request type of pulse logbook", label: "Request type pulse found successfullly", value: 0)
                                                } else {
                                                      GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get share type of pulse logbook", label: "Share type pulse found successfullly", value: 0)
                                                }
                                          }
                                    }
                              } else {
                                    self.stopLoader()
                                    DispatchQueue.main.async {
                                          self.tableViewPulse.isHidden=true
                                          self.noPulseFoundCorrespondingToSearch.isHidden=false
                                          self.noPulseFoundCorrespondingToSearch.text=self.pulseMsgs.serverErrorOfPulse
                                    }
                              }
                        } else {
                              self.stopLoader()
                              DispatchQueue.main.async {
                                    self.tableViewPulse.isHidden=true
                                    self.noPulseFoundCorrespondingToSearch.isHidden=false
                                    self.noPulseFoundCorrespondingToSearch.text=self.pulseMsgs.serverErrorOfPulse
                              }
                        }
                  } else {
                        self.stopLoader()
                        DispatchQueue.main.async {
                              self.tableViewPulse.isHidden=true
                              self.noPulseFoundCorrespondingToSearch.isHidden=false
                              self.noPulseFoundCorrespondingToSearch.text=self.pulseMsgs.serverErrorOfPulse
                        }
                        if(self.isLogListedAreOfTypeRequest)! {
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get request type of pulse logbook", label: "Error while getting request type of logbook", value: 0)
                        } else {
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Pulse Listing", action: "Get share type of pulse logbook", label: "Error while getting share type of logbook", value: 0)
                        }
                  }

            })
            task.resume()
            session.finishTasksAndInvalidate()
      }//end of get logs Api call
    
    func confirmPulseDeletion() {
        self.dualActionPopUp(heading: self.pulseMsgs.pulseDeletionHeading, subheading: self.pulseMsgs.pulseDeletionSubHeading, action: "deletePulseFromList", method: "deletePulseFromList", module: "NA",addresponder : false)
       // self.popUpVCController(addresponderFlag : false)
    }

    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            self.confirmPulseDeletion()
//            self.pulseObjToBeDeleted = self.logsArray[indexPath.row]
//            self.logsArray=self.logsArray.filter {$0.pulseId !=  self.pulseObjToBeDeleted.pulseId}
//            self.tableViewPulse.reloadData()
//            if(self.logsArray.count == 0){
//                self.showThisWhenNothingForLogbook()
//            }
//            UIView.animate(withDuration: TimeInterval(2), animations: {
//                self.view.layoutIfNeeded()
//            })
//        }
//    }
    
    func deletePulseById() {
        guard NetworkStatus.sharedManager.isNetworkReachable()
            else {
                self.showToastMsg(toastMsg: self.pulseMsgs.networkFailureMsg, action: 0)
                return
        }
//        self.startLoader()
        
        let parameters =
            [
                "idtoken": self.idtoken,
                "action": "deletepulse",
                "pulseId": self.pulseObjToBeDeleted.pulseId
                ] as [String: Any]
        
        let awsURL = AppConfig.deletePulseAPI
        
        guard let URL = URL(string: awsURL) else { return }
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        var request = URLRequest(url: URL)
        request.httpBody = "".data(using: String.Encoding.utf8)
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue(idtoken!, forHTTPHeaderField: "idtoken")
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
        }
        
        let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
//                self.stopLoader()
                if let resultObject = json as? [String: Any] {
                    let statusCodeRecieved = resultObject["statusCode"] as? String ?? ""
                    if(statusCodeRecieved == "0" || statusCodeRecieved == "200") {
                        DispatchQueue.main.async {
                            self.tableViewPulse.reloadData()
                        }
                    } else {
                        self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
                    }
                } else {
//                    self.stopLoader()
                    self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
                }
            } else {
                self.stopLoader()
                self.showToastMsg(toastMsg: self.pulseMsgs.serverErrorOfPulse, action: 0)
            }
        })
        task.resume()
        session.finishTasksAndInvalidate()
    }

}//class ends here

/****************************************** Protocols *******************************************/

extension PulseListingViewController: ReloadPulseLogbook {
      func reloadPulseListing(toReload: Bool) {
            if toReload {
                  self.dismiss(animated: true, completion: nil)
                  self.getLogs()
            }
      }
}

extension PulseListingViewController: PulseListingProtocol {
    
      func toDeletePulse(toReload: Bool) {
            self.deletePulseById()
      }

      func toDeclinePulse(toReload: Bool) {
            self.updateAcceptRejectStatus(pulseId: self.declineThisPulse, isAccepted: false)
      }

      func afetrAcceptingPulse(toReload: Bool) {
            self.comingBackToListingAfetrAcceptingPulse()
      }

      func toEnterCapsuleWhileAcceptingPulse(toReload: Bool) {
            self.openInsertCapsulePopUp()
      }

      func unlockedCapsuleDisplay(toReload: Bool) {
            self.showUnlockedCapsule()
      }

      func toAlertToAddRequestInContactList(toReload: Bool) {
            self.addContactToYourContactListToRespond()
      }
    
}
