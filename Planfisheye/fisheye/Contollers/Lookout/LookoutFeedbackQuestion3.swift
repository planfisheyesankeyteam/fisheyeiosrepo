//
//  LookoutFeedbackQuestion3.swift
//  fisheye
//
//  Created by SankeyMacPro on 01/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class LookoutFeedbackQuestion3: UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var purpleYesBtn: UIButton!
    @IBOutlet weak var whiteYesBtn: UIButton!
    @IBOutlet weak var purpleNoBtn: UIButton!
    @IBOutlet weak var whiteNoBtn: UIButton!
    @IBOutlet weak var innerPopUpView: UIView!
    @IBOutlet weak var feedbackQuestion: UILabel!
    
    var lookoutUserFeedbackObj = LookoutUserFeedbackObj()
    let lookoutText = LookoutToastMessages.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.whiteYesBtn.layer.cornerRadius=20
        self.whiteNoBtn.layer.cornerRadius=20
        self.purpleYesBtn.isHidden = true
        self.purpleNoBtn.isHidden = true
        self.whiteYesBtn.isHidden = false
        self.whiteNoBtn.isHidden = false
        self.addObservers()
        self.setLocalizationText()
    }
    
    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */
    
    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.feedbackQuestion.text = self.lookoutText.feedbackQuestionThree
            self.whiteYesBtn.setTitle(self.lookoutText.yes, for: .normal)
            self.whiteNoBtn.setTitle(self.lookoutText.no, for: .normal)
        }
    }
    /* END */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func purpleYesBtnTapped(_ sender: Any) {
        self.purpleNoBtn.isHidden = true
        self.whiteNoBtn.isHidden = false
        self.purpleYesBtn.isHidden = false
        self.whiteYesBtn.isHidden = true
        self.nextVCCall()
    }
    @IBAction func whiteNoBtnTapped(_ sender: Any) {
        self.purpleNoBtn.isHidden = true
        self.whiteNoBtn.isHidden = false
        self.purpleYesBtn.isHidden = false
        self.whiteYesBtn.isHidden = true
        self.lookoutUserFeedbackObj.isHappyToMakeFutureCommitementForPremiumVersionOfFisheye = "Yes"
        self.nextVCCall()
    }
    @IBAction func purpleNoBtnTapped(_ sender: Any) {
        self.purpleNoBtn.isHidden = false
        self.whiteNoBtn.isHidden = true
        self.purpleYesBtn.isHidden = true
        self.whiteYesBtn.isHidden = false
        self.nextVCCall()
    }
    @IBAction func whiteYesBtnTapped(_ sender: Any) {
        self.purpleNoBtn.isHidden = false
        self.whiteNoBtn.isHidden = true
        self.purpleYesBtn.isHidden = true
        self.whiteYesBtn.isHidden = false
        self.lookoutUserFeedbackObj.isHappyToMakeFutureCommitementForPremiumVersionOfFisheye = "No"
        self.nextVCCall()
    }
    
    @IBAction func closeFunctionTapped(_ sender: Any) {
        self.closeVCCall()
    }
    
    func closeVCCall() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutBaseVCViewController") as! LookoutBaseVCViewController
        lookoutBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(lookoutBaseViewcontroller)
        lookoutBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(lookoutBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }
    
    func nextVCCall() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutFeedbackQuestion4") as! LookoutFeedbackQuestion4
        lookoutBaseViewcontroller.lookoutUserFeedbackObj = self.lookoutUserFeedbackObj
        lookoutBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(lookoutBaseViewcontroller)
        lookoutBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(lookoutBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }
    
    @IBAction func prevPageCallTapped(_ sender: Any) {
        self.prevPageFuncCall()
    }
    
    func prevPageFuncCall() {
        self.view.removeFromSuperview()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
