//
//  LookoutFeedbackQuestion6.swift
//  fisheye
//
//  Created by SankeyMacPro on 01/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class LookoutFeedbackQuestion6: UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var feedBackSubmit: UIButton!
    @IBOutlet weak var hiUsernameLbl: UILabel!
    @IBOutlet weak var thankYouLbl: UILabel!
    
    var appSharedPreference = AppSharedPreference()
    var userName: String = ""
    var lookoutUserFeedbackObj = LookoutUserFeedbackObj()
    var lookoutText = LookoutToastMessages.shared
    var appService = AppService.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stopLoader()
        self.appService = AppService.shared
        self.lookoutText = LookoutToastMessages.shared
        self.feedBackSubmit.layer.cornerRadius=20
        userName = appSharedPreference.getAppSharedPreferences(key: "fisheyeName") as! String
        self.hiUsernameLbl.text = self.lookoutText.hi + " " + userName +  ","
        self.addObservers()
        self.setLocalizationText()
    }
    
    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */
    
    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.thankYouLbl.text = self.lookoutText.thanksBeforeSubmitting
            self.feedBackSubmit.setTitle(self.lookoutText.submit, for: .normal)
        }
    }
    /* END */
    
    @IBAction func feedBackSubmitTapped(_ sender: Any) {
        self.feedBackSubmit.isUserInteractionEnabled = false
        self.nextVCCall()
    }
    
    //Function to start and stop the loader
    func startLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
    }
    
    func stopLoader() {
        DispatchQueue.main.async {
            ScreenLoader.shared.stopLoader()
            
        }
    }
    //End of Function to start and stop the loader
    
    func nextVCCall() {
        if(NetworkStatus.sharedManager.isNetworkReachable()) {
            self.startLoader()
            self.submitLookoutUserFeedback()
        } else {
            self.view.makeToast(self.lookoutText.noInternet)
            self.feedBackSubmit.isUserInteractionEnabled = true
        }
        
    }
    
    func submitLookoutUserFeedback() {
        let feedbackOperation = LookoutUserFeedback()
        feedbackOperation.feedbackObj = self.lookoutUserFeedbackObj
        feedbackOperation.addDidFinishBlockObserver { [unowned self] (operation, _) in
            DispatchQueue.main.async {
                self.stopLoader()
                self.feedBackSubmit.isUserInteractionEnabled = true
            }
            if(operation.statusCode == "0") {
                LookoutSharedInstances.shared.isLookoutInterestRegistered = true
                self.makeToast(msg: self.lookoutText.successInAddingFeedback)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval, execute: {
                    let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
                    let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutBaseVCViewController") as! LookoutBaseVCViewController
                    lookoutBaseViewcontroller.view.frame = self.view.bounds
                    self.addChild(lookoutBaseViewcontroller)
                    lookoutBaseViewcontroller.didMove(toParent: self)
                    self.view.addSubview(lookoutBaseViewcontroller.view)
                    
                    UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
                        lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
                    }, completion: nil)
                })
            } else {
                self.makeToast(msg: self.lookoutText.failedToAddFeedback)
            }
        }
        AppDelegate.addProcedure(operation: feedbackOperation)
    }
    
    func makeToast(msg: String) {
        DispatchQueue.main.async {
            self.view.makeToast(msg)
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + AppService.shared.firstTimeInterval, execute: {
            self.view.hideToast()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
