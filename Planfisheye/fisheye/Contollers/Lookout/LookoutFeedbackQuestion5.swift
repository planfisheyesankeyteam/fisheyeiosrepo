//
//  LookoutFeedbackQuestion5.swift
//  fisheye
//
//  Created by SankeyMacPro on 01/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class LookoutFeedbackQuestion5: UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var feedBackTextView: UITextView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var innerPopUpView: UIView!
    @IBOutlet weak var feedbackQuestion: UILabel!
    
    var lookoutUserFeedbackObj = LookoutUserFeedbackObj()
    let lookoutText = LookoutToastMessages.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.addObservers()
        self.setLocalizationText()
        // Do any additional setup after loading the view.
    }
    
    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */
    
    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.feedbackQuestion.text = self.lookoutText.feedbackQuestionFive
        }
    }
    /* END */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextBtnTapped(_ sender: Any) {
        let feedbackText = feedBackTextView.text
        self.lookoutUserFeedbackObj.suggestionForFisheye = feedbackText
        self.nextVCCall()
    }
    
    @IBAction func closeFunctionTapped(_ sender: Any) {
        self.closeVCCall()
    }
    
    func closeVCCall() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutBaseVCViewController") as! LookoutBaseVCViewController
        lookoutBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(lookoutBaseViewcontroller)
        lookoutBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(lookoutBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }
    
    func nextVCCall() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutFeedbackQuestion6") as! LookoutFeedbackQuestion6
        lookoutBaseViewcontroller.lookoutUserFeedbackObj = self.lookoutUserFeedbackObj
        lookoutBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(lookoutBaseViewcontroller)
        lookoutBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(lookoutBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }
    
    @IBAction func prevPageCallTapped(_ sender: Any) {
        self.prevPageFuncCall()
    }
    
    func prevPageFuncCall() {
        self.view.removeFromSuperview()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
