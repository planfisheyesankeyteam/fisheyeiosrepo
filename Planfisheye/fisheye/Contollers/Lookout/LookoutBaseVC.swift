//
//  LookoutBaseVC.swift
//  fisheye
//
//  Created by SankeyMacPro on 01/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class LookoutBaseVC: UIViewController {

    @IBOutlet weak var lookoutBaseControllerView: UIView!
    @IBOutlet weak var mainContainerView: UIView!
    @IBOutlet weak var baseContainerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        roundCorners(myview: mainContainerView)
        loadLookOut()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

      func roundCorners(myview: UIView) {
            myview.layer.shadowColor = UIColor.black.cgColor
            myview.layer.shadowOffset = CGSize(width: 3, height: 3)
            myview.layer.shadowOpacity = 0.7
            myview.layer.shadowRadius = 10.0
            self.baseContainerView.layer.masksToBounds = true
            self.baseContainerView.layer.cornerRadius = 8
      }

    func loadLookOut() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LookoutBaseVCViewController") as! LookoutBaseVCViewController
        vc.view.frame = self.baseContainerView.bounds
        self.addChild(vc)
        vc.didMove(toParent: self)
        vc.view.frame.origin.y = -self.view.frame.size.height
        self.baseContainerView.addSubview(vc.view)
        vc.view.frame.origin.y = self.baseContainerView.bounds.origin.y
    }

      @IBAction func feLogoTapped(_ sender: Any) {
            self.navigateToDashboard()
      }

      func navigateToDashboard() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
            self.present(nextVC, animated: true, completion: nil)
      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
