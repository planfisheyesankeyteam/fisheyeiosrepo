//
//  LookoutFeedbackQuestion2.swift
//  fisheye
//
//  Created by SankeyMacPro on 01/03/18.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class LookoutFeedbackQuestion2: UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var purpleYearBtn: UIButton!
    @IBOutlet weak var whiteYearBtn: UIButton!
    @IBOutlet weak var purpleQuaterBtn: UIButton!
    @IBOutlet weak var whiteQuaterBtn: UIButton!
    @IBOutlet weak var innerPopUpView: UIView!
    @IBOutlet weak var feedbackQuestion: UILabel!
    
    var lookoutUserFeedbackObj = LookoutUserFeedbackObj()
    let lookoutText = LookoutToastMessages.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        self.purpleYearBtn.isHidden = true
        self.purpleQuaterBtn.isHidden = true
        self.whiteYearBtn.isHidden = false
        self.whiteQuaterBtn.isHidden = false
        self.whiteYearBtn.layer.cornerRadius=20
        self.whiteQuaterBtn.layer.cornerRadius=20
        self.addObservers()
        self.setLocalizationText()
    }
    
    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */
    
    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.feedbackQuestion.text = self.lookoutText.feedbackQuestionTwo
            self.whiteYearBtn.setTitle(self.lookoutText.payForFEOptionOne, for: .normal)
            self.whiteQuaterBtn.setTitle(self.lookoutText.payForFEOptionTwo, for: .normal)
        }
    }
    /* END */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func purpleYearBtnTapped(_ sender: Any) {
        self.purpleYearBtn.isHidden = false
        self.purpleQuaterBtn.isHidden = true
        self.whiteYearBtn.isHidden = true
        self.whiteQuaterBtn.isHidden = false
        self.nextVCCall()
    }
    
    @IBAction func whiteYearBtnTapped(_ sender: Any) {
        self.purpleYearBtn.isHidden = false
        self.purpleQuaterBtn.isHidden = true
        self.whiteYearBtn.isHidden = true
        self.whiteQuaterBtn.isHidden = false
        self.lookoutUserFeedbackObj.amountWillingToPayForFisheye = "$10 / $12 / $15 year"
        self.nextVCCall()
    }
    
    @IBAction func purpleQtrBtnTapped(_ sender: Any) {
        self.purpleYearBtn.isHidden = true
        self.purpleQuaterBtn.isHidden = false
        self.whiteYearBtn.isHidden = false
        self.whiteQuaterBtn.isHidden = true
        self.nextVCCall()
    }
    
    @IBAction func whiteQtrBtnTapped(_ sender: Any) {
        self.purpleYearBtn.isHidden = true
        self.purpleQuaterBtn.isHidden = false
        self.whiteYearBtn.isHidden = false
        self.whiteQuaterBtn.isHidden = true
        self.lookoutUserFeedbackObj.amountWillingToPayForFisheye = "$3 / $4 / $5 quarter"
        self.nextVCCall()
    }
    
    @IBAction func closeFunctionTapped(_ sender: Any) {
        self.closeVCCall()
    }
    
    func closeVCCall() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutBaseVCViewController") as! LookoutBaseVCViewController
        lookoutBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(lookoutBaseViewcontroller)
        lookoutBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(lookoutBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }
    
    func nextVCCall() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutFeedbackQuestion3") as! LookoutFeedbackQuestion3
        lookoutBaseViewcontroller.lookoutUserFeedbackObj = self.lookoutUserFeedbackObj
        lookoutBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(lookoutBaseViewcontroller)
        lookoutBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(lookoutBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }
    
    @IBAction func prevPageCallTapped(_ sender: Any) {
        self.prevPageFuncCall()
    }
    
    func prevPageFuncCall() {
        self.view.removeFromSuperview()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
