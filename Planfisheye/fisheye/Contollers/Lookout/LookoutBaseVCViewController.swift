//
//  LookoutBaseVCViewController.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 27/02/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit

class LookoutBaseVCViewController: UIViewController {
    
    //@IBOutlet weak var registerInterestButtonView: UIView!
    @IBOutlet weak var feedbackRecordedLbl: UILabel!
    
    @IBOutlet weak var feedbackRecordedView: UIView!
    @IBOutlet weak var registerLookoutBtn: UIButton!
    var isLookoutInterestRegistered: Bool = false
    let lookoutText = LookoutToastMessages.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ScreenLoader.shared.stopLoader()
        // shadow
        self.isLookoutInterestRegistered = LookoutSharedInstances.shared.isLookoutInterestRegistered ?? false
        self.feedbackRecordedView.backgroundColor = UIColor.purpleRing()
        self.feedbackRecordedView.layer.cornerRadius = 6
        
        if isLookoutInterestRegistered {
            self.registerLookoutBtn.isHidden = true
            self.feedbackRecordedView.isHidden = false
        } else {
            self.feedbackRecordedView.isHidden = true
        }
        self.feedbackRecordedView.isUserInteractionEnabled = false
        //      self.registerLookoutBtn.layer.shadowColor = UIColor.black.cgColor
        self.registerLookoutBtn.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.registerLookoutBtn.layer.shadowOpacity = 0.4
        self.registerLookoutBtn.layer.shadowRadius = 5.0
        self.registerLookoutBtn.layer.cornerRadius = 21
        self.addObservers()
        self.setLocalizationText()
    }
    
    /*  Add observers  */
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
    }
    /* END */
    
    /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
        DispatchQueue.main.async {
            self.registerLookoutBtn.setTitle(self.lookoutText.registeredInterest, for: .normal)
            self.feedbackRecordedLbl.text = self.lookoutText.thankYouLbl
        }
    }
    /* END */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerLookoutBtnTapped(_ sender: Any) {
        self.feedbackQuestion1()
    }
    
    func feedbackQuestion1() {
        let storyboard = UIStoryboard.init(name: "Lookout", bundle: nil)
        let lookoutBaseViewcontroller = storyboard.instantiateViewController(withIdentifier: "LookoutFeedbackQuestion1VC") as! LookoutFeedbackQuestion1VC
        lookoutBaseViewcontroller.view.frame = self.view.bounds
        self.addChild(lookoutBaseViewcontroller)
        lookoutBaseViewcontroller.didMove(toParent: self)
        self.view.addSubview(lookoutBaseViewcontroller.view)
        
        UIView.transition(with: self.view, duration: 0.1, options: .transitionCrossDissolve, animations: {
            lookoutBaseViewcontroller.view.frame.origin.y = self.view.bounds.origin.y
        }, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
