//
//  SMSFisheyeOperations.swift
//  fisheye
//
//  Created by sankeyMacOs on 18/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import Foundation
import ProcedureKit

// Creating structure to send OTP
struct SendOTPObject {
    init() {

    }
    var phonenumber: String?
    var countryCode: String?
    var email: String?
}

//Operation to change Mobile and Email verification send OTP

class sendOtpUpdateOperation: Procedure {
    let appSharedPrefernce = AppSharedPreference()
    var sendSMSObject = SendOTPObject()
    var idtoken = ""
    init(sendOtp: SendOTPObject) {
        super.init()
        self.idtoken = appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
        sendSMSObject = sendOtp
    }

    override func execute() {
        let parameters =
            [
                "action": "newNumberSendOtp",
                "idtoken": idtoken,
                "phonenumber": sendSMSObject.phonenumber,
                "countryCode": sendSMSObject.countryCode,
                "email": sendSMSObject.email

        ] as [String: Any]

      let awsURL = AppConfig.updateBasicDetails
      guard let URL = URL(string: awsURL) else { return }
      let sessionConfig = URLSessionConfiguration.default
      let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)

      var request = URLRequest(url: URL)
      request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
      request.httpMethod = "POST"
      guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
      }
      request.httpBody = httpBody

      guard let signedRequest = URLRequestSigner().sign(request: request) else {
            return
      }

      let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
            if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                  let data = json
                 
                  let statusCodeRecieved = data["statusCode"] as? String ?? ""
                  self.appSharedPrefernce.setAppSharedPreferences(key: "sendOtpStatus", value: statusCodeRecieved)
                  self.finish()

            } else {
                  self.appSharedPrefernce.setAppSharedPreferences(key: "sendOtpStatus", value: "NetworkError")
                  self.finish()
            }
      })
      task.resume()
      session.finishTasksAndInvalidate()
    }
}
//End of Operation to change Mobile and Email verification send OTP
