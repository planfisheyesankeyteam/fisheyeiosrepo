//
//  ContactUsVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 08/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var phonContactButtonView: UIView!
    @IBOutlet weak var emailContactButtonView: UIView!
    @IBOutlet weak var phoneButton: UIButton!
    @IBAction func dismissPopUpTapped(_ sender: Any) {

       self.dismiss(animated: true, completion: nil)

    }
    @IBAction func phoneButtonTapped(_ sender: Any) {

        if let url = URL(string: "tel://+919920477137"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        } else {
           print("Contact Not Supported")
            //self.present("Contact Not Supported", animated: true, completion: nil)
        }

    }
    @IBOutlet weak var emailButton: UIButton!
    @IBAction func emailButtonTapped(_ sender: Any) {

        let mailComposeViewController = self.configuredMailComposeViewController()

        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            //self.present(self.appService.showSendMailErrorAlert(), animated: true, completion: nil)
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.popUpView.layer.cornerRadius = 6
        self.popUpView.layer.shadowColor = UIColor.black.cgColor
        self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
        self.popUpView.layer.shadowOpacity = 0.4
        self.popUpView.layer.shadowRadius = 6.0
        self.popUpView.layer.masksToBounds = false

        self.phonContactButtonView.layer.cornerRadius = 6
        self.phonContactButtonView.layer.shadowColor = UIColor.black.cgColor
        self.phonContactButtonView.layer.shadowOffset = CGSize(width: 0.2, height: 0.1)
        self.phonContactButtonView.layer.shadowOpacity = 0.4
        self.phonContactButtonView.layer.shadowRadius = 6.0
        self.phonContactButtonView.layer.masksToBounds = false
        self.phonContactButtonView.backgroundColor = UIColor.duckEggBlue()
         self.phoneButton.backgroundColor = UIColor.duckEggBlue()

        self.emailContactButtonView.layer.cornerRadius = 6
        self.emailContactButtonView.layer.shadowColor = UIColor.black.cgColor
        self.emailContactButtonView.layer.shadowOffset = CGSize(width: 0.2, height: 0.1)
        self.emailContactButtonView.layer.shadowOpacity = 0.4
        self.emailContactButtonView.layer.shadowRadius = 6.0
        self.emailContactButtonView.layer.masksToBounds = false
        self.emailContactButtonView.backgroundColor = UIColor.duckEggBlue()
         self.emailButton.backgroundColor = UIColor.duckEggBlue()

    }

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
     //   mailComposerVC.mailComposeDelegate = self as! MFMailComposeViewControllerDelegate // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property

        mailComposerVC.setToRecipients(["hiteshame13@gmail.com"])
        mailComposerVC.setSubject("Contact Us")
        mailComposerVC.setMessageBody("My Message Body", isHTML: false)

        return mailComposerVC
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
