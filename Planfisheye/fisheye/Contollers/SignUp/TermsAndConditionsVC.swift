//
//  TermsAndConditionsVC.swift
//  fisheye
//
//  Created by Sankey Solutions on 13/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class TermsAndConditionsVC: UIViewController, UIWebViewDelegate {

      let appSharedPrefernce = AppSharedPreference()
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      var isFromSignUp: Bool = true

      @IBOutlet weak var signInLabel: UILabel!
      @IBOutlet weak var myWebView: UIWebView!
      @IBOutlet weak var mainVIew: UIView!
      @IBOutlet weak var fisheyeStatement: UILabel!
      @IBOutlet weak var fisheyeMotto: UILabel!
      @IBAction func backButtonTapped(_ sender: Any) {
            self.stopLoader()
            self.backFunctionCalled()
      }
      @IBAction func agreeButtonTapped(_ sender: Any) {
            self.stopLoader()
            self.popUpVCController()
      }

    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
            super.viewDidLoad()
//            self.stopLoader()
//        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
//        backgroundImage.image = UIImage(named: "backgroundFE.png")
//        backgroundImage.contentMode =  UIViewContentMode.scaleAspectFill
//        self.view.insertSubview(backgroundImage, at: 0)
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
            self.applyShadowNCornerRadius()
           
            self.startLoader()

            let URLget = URL(string: AppConfig.termsAndConditions)
            let urlRequest1 = URLRequest(url: URLget!)
            myWebView.loadRequest(urlRequest1)
            self.myWebView.delegate = self
            self.subscribeToObservers()
            self.setLocalizationText()
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.signInLabel.text = self.signupmsg.signIn
                  self.fisheyeStatement.text = self.signupmsg.fisheyeStatement
                  self.fisheyeMotto.text = self.signupmsg.letsWorkTogether
            }
      }
      /* END */

      func webViewDidFinishLoad(_ webView: UIWebView) {
            if myWebView.isLoading {
                  return
            }
            self.stopLoader()
      }

      func applyShadowNCornerRadius() {
            self.mainVIew.layer.cornerRadius = 6
            self.mainVIew.layer.shadowColor = UIColor.black.cgColor
            self.mainVIew.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.mainVIew.layer.shadowOpacity = 0.4
            self.mainVIew.layer.shadowRadius = 6.0
            self.mainVIew.layer.masksToBounds = false
      }

      func subscribeToObservers() {
            NotificationCenter.default.addObserver(forName: DISMISS_UNNECESSARY_SIGNUP_PAGES, object: nil, queue: nil) { (_) in
                  self.dismiss(animated: true, completion: nil)
            }

            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.TermsAndConditionsVC)
      }

      //Function for Redirection to Basic Information Page
      func backFunctionCalled() {
            self.dismiss(animated: true, completion: nil)
      }
      //End of function for Redirection to Basic Information Page

      func startLoader() {
        DispatchQueue.main.async{
           self.submitButton.isUserInteractionEnabled=false
          ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
        }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                self.submitButton.isUserInteractionEnabled=true
                  ScreenLoader.shared.stopLoader()
            }
      }

      //Function to Redirect to Pop-Up
      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsNConditionsConfirmationPopUp") as! TermsNConditionsConfirmationPopUp
            self.present(nextVC, animated: true, completion: nil)
      }
      //End of Redirection to Pop-Up

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }
}
