//
//  SignUPOTPVC.swift
//  fisheye
//
//  Created by Sankey Solution on 11/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class SignUPOTPVC: UIViewController {

      var phonenumber = ""
      var idtoken = ""
      var countrycode = ""
      var emailIdd = ""
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      /*Defined Variables*/
      var remSeconds: Double = 900
      var timer = Timer()
      let appSharedPrefernce = AppSharedPreference()
      var validation: Bool = false
      var startInteraction: StartInteraction?

    
      @IBOutlet weak var timerImageView: UIImageView!
      @IBOutlet weak var timeRemainingTextField: UILabel!
      @IBOutlet weak var resendButton: UIButton!
      @IBOutlet weak var securityCodeSecondaryHeader: UILabel!
      @IBOutlet weak var emailOTPBlueUnderline: UIImageView!
      @IBOutlet weak var emailOTPBlackUnderline: UIImageView!
      @IBOutlet weak var smsOTPBlueUnderline: UIImageView!
      @IBOutlet weak var smsOTPBlackUnderline: UIImageView!
      @IBOutlet weak var enterSixDigitKeyText: UILabel!

    
      @IBAction func resendButtonTapped(_ sender: Any) {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        return
            }
            resendOtpFunctionCalled()
      }

      @IBOutlet weak var otpTextField: UICustomTextField!
      @IBOutlet weak var emailOTPTextField: UICustomTextField!
      @IBOutlet weak var doneButtton: UIButton!

      @IBAction func doneButtonTapped(_ sender: Any) {
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        return
            }
            otpVerificationButtonTapped(smsOtp: otpTextField.text!, emailOtp: emailOTPTextField.text! )
      }

      @IBOutlet weak var otpScreenSubView: UIView!
      @IBAction func otpBackButtonTapped(_ sender: Any) {
            otpBackFunctionCalled()
      }

      @IBOutlet weak var otpBackButton: UIButton!

      func otpBackFunctionCalled() {
        self.dismiss(animated: true, completion: {
            self.startInteraction?.startButtonInteractions()
        })
      }

      @IBOutlet weak var sigInLabel: UILabel!
      func resendOtpFunctionCalled() {
            self.startLoader()
            let parameters =
                  [
                        "action": "resendotp",
                        "idtoken": idtoken,
                        "phonenumber": phonenumber,
                        "countryCode": countrycode,
                        "email": emailIdd
                        ] as [String: Any]

            let awsURL = AppConfig.resendOTPAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        self.stopLoader()
                        let statusCode = data["statusCode"] as? String ?? ""
                        if(statusCode == "200") {
                              DispatchQueue.main.async {
                                    self.timerImageView.isHidden = false
                                    self.scheduleTimer()
                                    self.timeRemainingTextField.isHidden = false
                                    self.view.makeToast(self.profilemsg.otpsent, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Resend OTP", label: "OTP sent successful", value: 0)
                        } else {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.signupmsg.resndotp, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Resend OTP", label: "Failed to send OTP", value: 0)
                        }

                  } else {
                        self.stopLoader()
                        DispatchQueue.main.async {
                              self.view.makeToast(self.signupmsg.resndotp, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Resend OTP", label: "Failed to send OTP", value: 0)
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }

//    signUpMethod: signUpMethod,fisheyeName: fisheyeName,fisheyeId: fisheyeId,fisheyeId: fisheyeId,genderSelection: genderSelection, email : email
    func goToTermsNConditions() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
        self.present(nextVC, animated: true, completion: nil)
    }
   
    
    func nextPageFunction(signUpMethod: String, fisheyeName: String, fisheyeId: String, genderSelection: String ,email: String) {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUpSecurityQuestionVC") as! SignUpSecurityQuestionVC
            nextVC.signUpMethod = signUpMethod
            nextVC.fisheyeName = fisheyeName
            nextVC.fisheyeId = fisheyeId
            nextVC.genderSelection = genderSelection
            nextVC.email = email
            self.present(nextVC, animated: true, completion: nil)

      }

      //Code for checking validation while the user edits TextFields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {
            switch textField {

            case emailOTPTextField:
                  self.emailOTPBlueUnderline.isHidden = false
                  self.emailOTPBlackUnderline.isHidden = true
                  if (self.emailOTPTextField.text?.characters.count)! <= 0 {
                        self.emailOTPTextField.configOnError(withPlaceHolder: self.profilemsg.enteremailOTP)
                  } else {
                        self.emailOTPTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                        if((emailOTPTextField.text?.characters.count)! > 6) {
                              emailOTPTextField.text = String((emailOTPTextField.text?.dropLast())!)
                              self.emailOTPTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                        }
                  }
                  break

            case otpTextField:
                  self.smsOTPBlueUnderline.isHidden = false
                  self.smsOTPBlackUnderline.isHidden = true
                  if (self.otpTextField.text?.characters.count)! <= 0 {
                        self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.entermobotp)
                  } else {
                        self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                        if((otpTextField.text?.characters.count)! > 6) {
                              otpTextField.text = String((otpTextField.text?.dropLast())!)
                              self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                        }
                  }

                  break

            default:
                  break
            }
      }

      func onViewLoadToBeShowAs() {
            self.otpTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.otpTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.emailOTPTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.emailOTPTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
      }

      //Code for checking validation when the user is done editing TextFields
    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case emailOTPTextField:
                  self.emailOTPBlueUnderline.isHidden = true
                  self.emailOTPBlackUnderline.isHidden = false
                  if (self.emailOTPTextField.text?.characters.count)! <= 0 {
                        self.emailOTPTextField.configOnError(withPlaceHolder: self.profilemsg.enteremailOTP)
                  } else if (self.emailOTPTextField.text?.characters.count)! != 6 {
                        self.emailOTPTextField.configOnError(withPlaceHolder: self.profilemsg.invalidemailotp)
                  } else {
                        self.emailOTPTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                  }
                  break

            case otpTextField:
                  self.smsOTPBlueUnderline.isHidden = true
                  self.smsOTPBlackUnderline.isHidden = false
                  if (self.otpTextField.text?.characters.count)! <= 0 {
                        self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.entermobotp)
                  } else if (self.otpTextField.text?.characters.count)! != 6 {
                        self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidmobotp)
                  } else {
                        self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                  }

                  break

            default:
                  break
            }
      }

      func otpVerificationButtonTapped(smsOtp: String, emailOtp: String) {
            var isvalidationSuccessful: Bool = true

            if (emailOtp.count) <= 0 {
                  self.emailOTPTextField.configOnError(withPlaceHolder: self.profilemsg.enteremailOTP)
                  isvalidationSuccessful = false
            } else {
                  if emailOtp.count < 6 {
                        isvalidationSuccessful = false
                        self.emailOTPTextField.configOnError(withPlaceHolder: self.profilemsg.invalidotp)
                  } else {
                        self.emailOTPTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                  }
            }

            if (smsOtp.count) <= 0 {
                  self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.entermobotp)
                  isvalidationSuccessful = false
            } else {
                  if smsOtp.count < 6 {
                        isvalidationSuccessful = false
                        self.otpTextField.configOnError(withPlaceHolder: self.profilemsg.invalidotp)
                  } else {
                        self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.mobileotp)
                  }
            }

            validation = isvalidationSuccessful

            if isvalidationSuccessful {
                  submitOTP()
            }
      }

      func submitOTP() {
            self.doneButtton.isUserInteractionEnabled=false
            self.startLoader()
            let otpEntered = otpTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let emailOTP = emailOTPTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            let parameters =
                  [

                        "action": "verifyotp",
                        "idtoken": idtoken,
                        "phonenumber": phonenumber,
                        "countryCode": countrycode,
                        "otpValue": otpEntered,
                        "mailOTPValue": emailOTP

                        ] as [String: Any]

            let awsURL = AppConfig.verifyOTPAPI
        
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        let statusCode = data["statusCode"] as? String ?? ""
                        if( statusCode == "200") {
                              if let user = data["user"] as? [String: Any] {
                                    let signUpMethod = user["signupMethod"] as? String ?? ""
                                    let fisheyeName = user["name"] as? String ?? ""
                                    let fisheyeId = user["id"] as? String ?? ""
//                                    self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: user["signupMethod"] as? String ?? "")
//                                    self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeId", value: fisheyeId)
//                                    self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeName", value: fisheyeName) as? String ?? ""
                                    let genderSelection = user["gender"] as? String ?? ""
//                                    self.appSharedPrefernce.setAppSharedPreferences(key: "gender", value: genderSelection)
//                                    self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeUserObject", value: "")
                                    let email = user["email"] as? String ?? ""
//                                    self.appSharedPrefernce.setAppSharedPreferences(key: "loginId", value: email)
                                DispatchQueue.main.async {
                                    self.nextPageFunction(signUpMethod: signUpMethod,fisheyeName: fisheyeName,fisheyeId: fisheyeId,genderSelection: genderSelection, email : email)
                                }
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP submitted", label: "OTP verification successful - Redirectionion to Register master key screen", value: 0)
                              }
                              
                        }else if(statusCode == "1")
                        {
                              DispatchQueue.main.async {
                                    
                                    self.view.makeToast(self.profilemsg.otpverificationfailed)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP submitted", label: "Phone OTP verification failed", value: 0)
                        }
                        else if(statusCode == "2") {
                              DispatchQueue.main.async {
                                    
                                    self.view.makeToast(self.profilemsg.otpverificationfailed)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP submitted", label: "Email Id  verification failed", value: 0)
                        } else if(statusCode == "3") {
                              DispatchQueue.main.async {
                                    
                                    self.view.makeToast(self.profilemsg.otpverificationfailed)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP submitted", label: "Mobile Verification Failed", value: 0)
                        } else if(statusCode == "4") {
                              DispatchQueue.main.async {
                                    self.makeResendButtonEnable()
                                    self.view.makeToast(self.profilemsg.otpExpired)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP expired", label: "Mobile Verification Failed", value: 0)
                        } else {
                              DispatchQueue.main.async {
                                    self.makeResendButtonEnable()
                                    self.view.makeToast(self.profilemsg.otpverificationfailed)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP submitted", label: "Mobile and Email Verification both Failed", value: 0)

                        }
                    DispatchQueue.main.async {
                     self.doneButtton.isUserInteractionEnabled=true
                        self.stopLoader()
                    }
                  } else {
                    
                        DispatchQueue.main.async {
                              self.makeResendButtonEnable()
                            self.stopLoader()
                            self.doneButtton.isUserInteractionEnabled=true
                        }
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "OTP submitted", label: "OTP verification failed", value: 0)
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }

      func makeResendButtonEnable() {
            self.timer.invalidate()
            self.timeRemainingTextField.isHidden = true
            self.timerImageView.isHidden = true
            self.resendButton.isUserInteractionEnabled = true
      }
      
      

      func scheduleTimer() {
            DispatchQueue.main.async {
                  self.remSeconds = 900
                  self.timer.invalidate()
                  self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
                  self.resendButton.isUserInteractionEnabled = false
            }
      }

    @objc func update() {
            self.remSeconds -= 1
            if self.remSeconds == 0 {
                  timer.invalidate()
                  timeRemainingTextField.text = ""
                  resendButton.alpha = 1.0
                  self.timerImageView.isHidden = true
                  resendButton.isUserInteractionEnabled = true
                  return
            }
            self.animate()
            let remSecondsStr = String(self.remSeconds.minuteSecond)
        timeRemainingTextField.text = self.signupmsg.otpValidFor + " " +  remSecondsStr + self.signupmsg.minutes
      }

      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
            self.present(nextVC, animated: true, completion: nil)
      }

      func startLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)            }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                  ScreenLoader.shared.stopLoader()
            }
      }
      func animate() {
            DispatchQueue.main.async {
                  self.rotateAnimation(button: self.timerImageView)
            }
      }

      @IBOutlet weak var fishEyeLabel: UILabel!
      @IBOutlet weak var itsAllAboutYou: UILabel!
      override func viewDidLoad() {
            super.viewDidLoad()
            self.stopLoader()
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
            self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
            self.scheduleTimer()
            self.fishEyeLabel.text = "Fisheye Hub"
            self.otpScreenSubView.layer.cornerRadius = 6
            self.otpScreenSubView.layer.shadowColor = UIColor.black.cgColor
            self.otpScreenSubView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.otpScreenSubView.layer.shadowOpacity = 0.4
            self.otpScreenSubView.layer.shadowRadius = 6.0
            self.otpScreenSubView.layer.masksToBounds = false
            self.phonenumber = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.countrycode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
            self.emailIdd = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
            self.onViewLoadToBeShowAs()
            self.subscribeToObservers()
            self.setLocalizationText()

      }

      func subscribeToObservers() {

            NotificationCenter.default.addObserver(forName: DISMISS_UNNECESSARY_SIGNUP_PAGES, object: nil, queue: nil) { (_) in
                  self.dismiss(animated: true, completion: nil)
            }

            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.sigInLabel.text = self.signupmsg.signIn
                  self.securityCodeSecondaryHeader.text = self.signupmsg.securityKey
                  self.itsAllAboutYou.text = self.signupmsg.letsWorkTogether
                  self.enterSixDigitKeyText.text = self.signupmsg.enterSixDigitKey
                  self.emailOTPTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.emailotp)
                  self.otpTextField.configOnErrorCleared(withPlaceHolder: self.profilemsg.entermobotp)
                  self.resendButton.setTitle(self.signupmsg.resend, for: .normal)
            }
      }
      /* END */

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SignUPOTPVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      /* Created by  by vaishali */
      func rotateAnimation(button: UIImageView, duration: CFTimeInterval = 2.0) {
            DispatchQueue.main.async {
                  let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
                  rotateAnimation.fromValue = 0.0
                  rotateAnimation.toValue = CGFloat(.pi * 2.0)
                  rotateAnimation.duration = duration
                  rotateAnimation.repeatCount = Float.greatestFiniteMagnitude
                  button.layer.add(rotateAnimation, forKey: "syncRotation")
            }
      }
      /* END */

}
