//
//  TermsNConditionsConfirmationPopUp.swift
//  fisheye
//
//  Created by SankeySolutionsMac4 on 06/06/2018.
//  Copyright © 2018 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class TermsNConditionsConfirmationPopUp: UIViewController {

      @IBOutlet weak var popUpView: UIView!
      @IBOutlet weak var linksLbl: UILabel!
      @IBOutlet weak var agreeBtn: UIButton!
      @IBOutlet weak var acceptanceOfTNC: UILabel!
      
      @IBOutlet weak var acceptingThisTNC: UILabel!
      @IBOutlet weak var disagreeLabel: UILabel!
      @IBOutlet weak var agreeLabel: UILabel!
      @IBOutlet weak var privacyPolicyBtn: UIButton!
      @IBOutlet weak var termsOfUsePolicyBtn: UIButton!
      @IBOutlet weak var cookiePolicyBtn: UIButton!
      @IBOutlet weak var disclaimerPolicyBtn: UIButton!

      var idToken: String?
      let appSharedPrefernce = AppSharedPreference()
      var mobile: String?
      var countrycode: String?
      var email: String?
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      var appService = AppService()

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.stopLoader()
        self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
        self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
        self.appService = AppService.shared
        self.applyShadowNCornerRadius()
        self.initializeData()
        self.subscribeToObservers()
        self.setLocalizationText()
        self.addObservers()
        // Do any additional setup after loading the view.
    }

      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

      func applyShadowNCornerRadius() {
            self.popUpView.layer.cornerRadius = 6
            self.popUpView.layer.shadowColor = UIColor.black.cgColor
            self.popUpView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.popUpView.layer.shadowOpacity = 0.4
            self.popUpView.layer.shadowRadius = 6.0

      }

      @IBAction func privacyPolicyTapped(_ sender: Any) {
            if let url = URL(string: self.appService.privacyPolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      @IBAction func termsOfUseTapped(_ sender: Any) {
            if let url = URL(string: self.appService.marketplacePolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      @IBAction func cookiePolicyTapped(_ sender: Any) {
            if let url = URL(string: self.appService.cookiePolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      @IBAction func disclaimerPolicyTapped(_ sender: Any) {
            if let url = URL(string: self.appService.disclaimerPolicyURL) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
      }

      func initializeData() {
            self.idToken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""
            self.mobile = self.appSharedPrefernce.getAppSharedPreferences(key: "mobile") as? String ?? ""
            self.countrycode = self.appSharedPrefernce.getAppSharedPreferences(key: "countrycode") as? String ?? ""
            self.email = self.appSharedPrefernce.getAppSharedPreferences(key: "email") as? String ?? ""
      }

      func subscribeToObservers() {
            NotificationCenter.default.addObserver(forName: DISMISS_UNNECESSARY_SIGNUP_PAGES, object: nil, queue: nil) { (_) in
                  self.dismiss(animated: true, completion: nil)
            }
      }

      @IBAction func disagreeTapped(_ sender: Any) {
            self.dismiss(animated: true, completion: nil)
      }

      @IBAction func agreeTapped(_ sender: Any) {
            self.agreeBtn.isUserInteractionEnabled = false
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              self.agreeBtn.isUserInteractionEnabled = true
                        }
                        return
            }
            self.updateTermsAndConditionflag()
      }

      func startLoader() {
        ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
      }

      func stopLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.stopLoader()
            }
      }

      // add all observers to this viewController here
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      // End

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.acceptanceOfTNC.text = self.signupmsg.acceptanceOFTNCHeader
                  self.acceptingThisTNC.text = self.signupmsg.acceptanceOfTNCText
                  self.disagreeLabel.text = self.signupmsg.disagree
                  self.agreeLabel.text = self.signupmsg.agree
                  self.privacyPolicyBtn.setTitle(self.signupmsg.privacyPolicy, for: .normal)
                  self.termsOfUsePolicyBtn.setTitle(self.signupmsg.termsOfUse, for: .normal)
                  self.cookiePolicyBtn.setTitle(self.signupmsg.cookiePolicyFor, for: .normal)
                  self.disclaimerPolicyBtn.setTitle(self.signupmsg.disclaimerPolicy, for: .normal)
            }
      }
      /* END */

      //Function to Update Terms and condition flag
      func updateTermsAndConditionflag() {

            self.startLoader()
            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        DispatchQueue.main.async {
                            self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        return
            }

            let parameters =
                  [
                        "action": "acceptTermsNConditions",
                        "idtoken": idToken!,
                        "isTermsNConditionsAccepted": true
                        ] as [String: Any]

            let awsURL = AppConfig.updateTNC
            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idToken!, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        self.stopLoader()
                        if(data["statusCode"] as? String ?? "" == "200") {
                            DispatchQueue.main.async {
                                self.view.makeToast(self.signupmsg.registrationsuccess, duration: self.signupmsg.toastDelayTime, position: .bottom)
                            }
                            if let user = data["user"] as? [String: Any] {
                                let fisheyeName = user["name"] as? String ?? ""
                                let fisheyeId = user["id"] as? String ?? ""
                                self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: user["signupMethod"] as? String ?? "")
                                self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeId", value: fisheyeId)
                                self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeName", value: fisheyeName) as? String ?? ""
                                let genderSelection = user["gender"] as? String ?? ""
                                self.appSharedPrefernce.setAppSharedPreferences(key: "gender", value: genderSelection)
                                self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeUserObject", value: "")
                                let email = user["email"] as? String ?? ""
                                self.appSharedPrefernce.setAppSharedPreferences(key: "loginId", value: email)
                            }
                              DispatchQueue.main.async {
                                
                                DatabaseManagement.shared.deleteAllContactsFromSQLite()
                                DatabaseManagement.shared.deleteAllRecentlyAddedContactsFromSQLite()
                                self.startContactFrontEndSync()
                                    self.redirectToDashboard()
                              }

                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Agreed Terms & Conditions", label: "Terms & Conditions Accepted - Redirection OTP verification screen", value: 0)
                        } else {
                              //self.stopLoader()
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Agreed Terms & Conditions", label: "Failed to accept Terms & Conditions", value: 0)
                        }

                  } else {
                        self.stopLoader()
                        DispatchQueue.main.async {
                            self.agreeBtn.isUserInteractionEnabled = true
                              self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                              self.view.hideToast()
                        })
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Agreed Terms & Conditions", label: "Failed to accept Terms & Conditions", value: 0)
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()
      }
      //End of Function to Update Terms and condition flag
    
    
    func redirectToDashboard(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "BaseViewController") as! BaseViewController
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func startContactFrontEndSync() {
        let frontendSyncGlobalOperation = DispatchWorkItem {
            DatabaseManagement.shared.createRecentlyAddedTable()
            
            /* Check SQLite table is created or not? */
            let isSQliteTableExists: Bool = DatabaseManagement.shared.isSQLiteTableAreadyExists()
            if(!isSQliteTableExists) {                                                                                                                                                                               // SQLite table does't exists
                DatabaseManagement.shared.createOwenerContactsTable()                                                                                      // create table
                self.getFEUserContactsFromBackend()
            } else {                                                                                                                                                                                                                // SQLite table  exists
                let totalRecordsInTable: Int64 = DatabaseManagement.shared.checkIsOwnerContactsTableEmpty()
                if(totalRecordsInTable == 0) {                                                                                                                                                               // SQLite table is empty
                    self.getFEUserContactsFromBackend()
                } else {                                                                                                                                                                                                        // SQLite table is not empty
                    
                    self.getBlockedByContactsFromBackend()
                    let isFetchFromBackendCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isFetchFromBackendCompleted") as? String ?? "yes"
                    let isfronendSyncFullyCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isfronendSyncFullyCompleted") as? String ?? "yes"
                    if(isFetchFromBackendCompleted == "no") {
                        self.getFEUserContactsFromBackend()
                    } else if(isfronendSyncFullyCompleted == "no") {
                        ContactsSync.shared.frontEndSync()
                    } else {
                        if(NetworkStatus.sharedManager.isNetworkReachable()) {
                            ContactsSync.shared.getNonSyncedContacts()
                            if(!ContactsSync.shared.globalNonSyncedContcts.isEmpty ) {
                                ContactsSync.shared.backendSync()
                            }
                            
                            AppSharedPreference().setAppSharedPreferences(key: "toUpdateFEIdIsNecessary", value: "yes")
                        }
                    }
                }
            }
        }
        DispatchQueue.global().async(execute: frontendSyncGlobalOperation)
    }

    func getFEUserContactsFromBackend() {
        self.appSharedPrefernce.setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "yes")
        let getContactsFromBackend = getLoggedInUserContactsOperation()
        getContactsFromBackend.addDidFinishBlockObserver { [unowned self] (operation, _) in
            DispatchQueue.main.async {
                let contactsCount = operation.contactsCount
                ContactsSync.shared.isSyncInProgress = false
                if(contactsCount > 0) {
                    self.getBlockedByContactsFromBackend()
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: false)
                    AppSharedPreference().setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "no")
                } else {
                    ContactsSync.shared.isToCheckBlockedByContacts = true
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: true)
                }
            }
        }
        AppDelegate.addProcedure(operation: getContactsFromBackend)
    }
    
    func getBlockedByContactsFromBackend() {
        let getBlockedByContacts = getBlockedByContactsAPIOperation()
        getBlockedByContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getBlockedByContacts)
    }


      func sendOtpVc() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
            self.present(nextVC, animated: true, completion: nil)
      }

}

extension UITapGestureRecognizer {

      func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
            // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
            let layoutManager = NSLayoutManager()
            let textContainer = NSTextContainer(size: CGSize.zero)
            let textStorage = NSTextStorage(attributedString: label.attributedText!)

            // Configure layoutManager and textStorage
            layoutManager.addTextContainer(textContainer)
            textStorage.addLayoutManager(layoutManager)

            // Configure textContainer
            textContainer.lineFragmentPadding = 0.0
            textContainer.lineBreakMode = label.lineBreakMode
            textContainer.maximumNumberOfLines = label.numberOfLines
            let labelSize = label.bounds.size
            textContainer.size = labelSize

            // Find the tapped character location and compare it to the specified range
            let locationOfTouchInLabel = self.location(in: label)
            let textBoundingBox = layoutManager.usedRect(for: textContainer)
            let textContainerOffset = CGPoint(x: ( labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
            let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                         y: locationOfTouchInLabel.y - textContainerOffset.y)
            let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
            return NSLocationInRange(indexOfCharacter, targetRange)
      }

}
