//
//  SignUpMyProfileVC.swift
//  fisheye
//
//  Created by Sankey Solution on 11/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
// 

import UIKit
import MRCountryPicker
import DropDown
import Toast_Swift

class SignUpMyProfileVC: UIViewController, UITextFieldDelegate, MRCountryPickerDelegate, UITableViewDelegate, UITableViewDataSource {
      //********* Declaration of all variables, outlet variables and button actions ***********//
      var appService = AppService()
      let validationController = ValidationController()
      var iconClick: Bool!
      var pwdVieww: Bool! = false
      var code: String!
      let appSharedPrefernce = AppSharedPreference()
      var idtoken = ""
      var mobileNumber1 = ""
      var countryCod = ""
      var signUpMethod = ""
      var emailIdd = ""
      var placeHolderOne = ""
      var placeHolderTwo = ""
      var placeHolderThree = ""
      var placeHolderFour = ""
      var placeHolderFive = ""
      var placeHolderSix = ""
      var placeholderSeven = ""
      var placeholderEight = ""
      var placeholderNine = ""
      var placeholderTen = ""
      var placeholderEleven = ""
      var placeholderTwelve = ""
      var placeholderThirteen = ""
      var placeholderFourteen = ""
      var minMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBelesserThan
      var maxMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBeGreaterThan
      var obj: [String: Any]?
      var modificationDataCalled = false
      var phoneNumberVerified1: Bool? = false
      var emailVerifyFlag: Bool? = false
      var isTermsNConditionsAccepted: Bool? = false
      var isImportantDetailsChanged: Bool = false
      var isToDisplayData: Bool = false
      var isSecurityAnsesExists: Bool? = false
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      let genderDropdown: DropDown = DropDown()
      var genderArray = ["Male", "Female", "Trans-Female", "Bi-Gender", "Non-Binary", "Gender nonconforming", "Undisclosed", "Rather not say" ]
      var englishGenderArray: [String] = []
      var selectedGenderIndex: Int = 7
      var selectedText: String!
      @IBOutlet weak var signInLabel: UILabel!
      @IBOutlet weak var myProfileView: UIView!
      @IBOutlet weak var userProfileLabel: UILabel!
      @IBOutlet weak var backButton: UIButton!
      @IBOutlet weak var backButtonTapped: UIButton!
      @IBOutlet weak var passwordEyebutton: UIButton!
      @IBOutlet weak var submitButton: UIButton!
      @IBOutlet weak var dropDownButton: UIButton!
      @IBOutlet weak var fisheyeIcon: UIImageView!
      @IBOutlet weak var profileIcon: UIImageView!
      @IBOutlet weak var countryImageDisplay: UIImageView!
      @IBOutlet weak var reEnterPwdBlueUnderline: UIImageView!
      @IBOutlet weak var reEnterPwdBlackUnderline: UIImageView!
      @IBOutlet weak var passwordBlackunderline: UIImageView!
      @IBOutlet weak var passwordBlueUnderline: UIImageView!
      @IBOutlet weak var mobileNumberBlackUnderline: UIImageView!
      @IBOutlet weak var mobileNumberBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlackUnderline: UIImageView!
      @IBOutlet weak var nameBlueUnderline: UIImageView!
      @IBOutlet weak var nameBlackUnderline: UIImageView!
      @IBOutlet weak var RadialGradientView: RadialGradientView!
      @IBOutlet weak var emailUnderLine: RadialGradientView!
      @IBOutlet weak var reenterPasswordUnderLine: RadialGradientView!
      @IBOutlet weak var passwordUnderLine: RadialGradientView!
      @IBOutlet weak var phoneUnderLine: RadialGradientView!
      @IBOutlet weak var reenterPasswordTextField: UICustomTextField!
      @IBOutlet weak var passwordTextField: UICustomTextField!
      @IBOutlet weak var fisheyeMotto: UILabel!

      @IBOutlet weak var genderTextField: UICustomTextField!
      @IBOutlet weak var genderBtn: UIButton!
      @IBOutlet weak var genderTableView: UITableView!
      @IBOutlet weak var mobileNumber: UICustomTextField!
      @IBOutlet weak var countryCode: UICustomTextField!
      @IBOutlet weak var emailTextField: UICustomTextField!
      @IBOutlet weak var nameTextField: UICustomTextField!
      @IBOutlet weak var countryPicker: MRCountryPicker!
      @IBOutlet weak var passwordDetailView: UITextView!
      @IBAction func submitButtonTapped(_ sender: Any) {
        self.submitButton.isUserInteractionEnabled = false
            submitFunction(name: nameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, mobNumber: mobileNumber.text!, reEnterPassword: reenterPasswordTextField.text!)
      }
      @IBAction func dropDownButtonTapped(_ sender: Any) {
            myProfileView.isHidden = true
            countryPicker.isHidden = false
      }

      @IBAction func passwordEyeButtonTapped(_ sender: Any) {
            if(iconClick == true) {
                  self.passwordTextField.isSecureTextEntry = false
                  iconClick = false
            } else {
                  self.passwordTextField.isSecureTextEntry = true
                  iconClick = true
            }
      }
      @IBAction func hideShowPwdDetailsButton(_ sender: Any) {
            if(pwdVieww == true) {
                  self.passwordDetailView.isHidden = false
                  pwdVieww = false
            } else {
                  self.passwordDetailView.isHidden = true
                  pwdVieww = true
            }
      }
      @IBAction func backBtnTapped(_ sender: Any) {
            backFunction()
      }
    
    @IBOutlet weak var scrollView: UIScrollView!
    //********* End of Declaration of all variables, outlet variables and button actions ***********//

      override func viewDidLoad() {
            super.viewDidLoad()
        
            self.stopLoader()
            self.scrollView.layer.cornerRadius = 6.0
            self.countryPicker.layer.cornerRadius=6.0
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
            self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
            self.appService = AppService.shared
            self.englishGenderArray = self.genderArray
            self.passwordDetailView.isHidden = true
            self.myProfileView.layer.cornerRadius = 6
            self.signUpMethod = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpMethod") as? String ?? ""
            profileCardShadowView()
            countryPicker.countryPickerDelegate = self
            countryPicker.showPhoneNumbers = true

            if isToDisplayData {
                  self.showPreFilledData()
            }

            if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
                  if !isToDisplayData {
                        // set country by its code
                        countryPicker.setCountry(countryCode)
                        self.countryCode.text = CommonMethods.localeCountryCode()
                  }
            }
            // optionally set custom locale; defaults to system's locale
            countryPicker.setLocale("gb_GB")
            // set country by its name
            countryPicker.setCountryByName("London")
            if(self.signUpMethod != "fisheye") {
                  self.emailTextField.text = self.appSharedPrefernce.getAppSharedPreferences(key: "socialemail") as? String ?? ""
                  self.nameTextField.text = self.appSharedPrefernce.getAppSharedPreferences(key: "socialname") as? String ?? ""
            }

            // self.signInLabel.textColor = UIColor.frenchBlue70()
            //self.signInLabel.font = UIFont(name: "Myriad Pro Regular", size:CGFloat(20))

            self.passwordTextField.isSecureTextEntry = true
            self.reenterPasswordTextField.isSecureTextEntry = true
            iconClick = true
            pwdVieww = true

            self.pwdVieww = true
            countryCode.delegate = self
            self.nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.mobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.nameTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.emailTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.passwordTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.mobileNumber.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.reenterPasswordTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.reenterPasswordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

            self.nameTextField.addTarget(self, action: #selector(textFieldShouldReturn(_:)), for: .editingDidEndOnExit)
            self.emailTextField.addTarget(self, action: #selector(textFieldShouldReturn(_:)), for: .editingDidEndOnExit)
            self.code = (Locale.current as NSLocale).object(forKey: .countryCode) as? String

            self.nameTextField.delegate=self
            self.emailTextField.delegate=self
            self.reenterPasswordTextField.delegate = self
            self.mobileNumber.delegate = self
            self.passwordTextField.delegate = self

            self.nameBlackUnderline.isHidden = false
            self.nameBlueUnderline.isHidden = true

            self.emailBlueUnderline.isHidden = true
            self.emailBlackUnderline.isHidden = false

            self.mobileNumberBlueUnderline.isHidden = true
            self.mobileNumberBlackUnderline.isHidden = false

            self.passwordBlueUnderline.isHidden = true
            self.passwordBlackunderline.isHidden = false

            self.reEnterPwdBlueUnderline.isHidden = true
            self.reEnterPwdBlackUnderline.isHidden = false

            self.passwordDetailView.isUserInteractionEnabled = false
            self.countryCode.isUserInteractionEnabled = false
            self.initializeGender()
            self.setLocalizationText()
            self.addObservers()
      }

      func initializeGender() {
            self.genderArray = signupmsg.genderArrayList
            self.genderTableView.isHidden = true
            self.genderTextField.isUserInteractionEnabled = false
            self.genderDropdown.anchorView = self.genderTableView
            self.genderDropdown.dataSource = self.genderArray
            self.genderDropdown.width = 200
            self.genderDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                  self.genderTextField.text = item
                  self.selectedGenderIndex = index
                
                
                let objArray = self.signupmsg.localGenderArrayList.filter( { return ($0.genderTranslatedText == item ) } )
                
                self.genderTextField.text = objArray[0].genderTranslatedText
                
                self.selectedText = objArray[0].genderActualText
            
            }
      }

      // add all observers to this viewController here
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      // End

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.signInLabel.text = self.signupmsg.signIn
                  self.userProfileLabel.text = self.signupmsg.userProfile
                  self.passwordDetailView.text = self.signupmsg.passwordPolicy
                  self.fisheyeMotto.text = self.signupmsg.letsWorkTogether
                  self.nameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)

                  self.nameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)
                  self.emailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                  self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                  self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
                  self.reenterPasswordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.reenterpassword)
                  self.genderTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.selectGender)
                  self.countryCode.configOnErrorCleared(withPlaceHolder: self.signupmsg.code)
            }
      }
      /* END */

      public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.genderArray.count
      }

      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.genderTextField.text = self.genderArray[indexPath.row]
            self.genderTableView.isHidden = true
      }

      public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = self.genderArray[indexPath.row]
            return cell
      }

      @IBAction func genderBtnTapped(_ sender: Any) {
            if  genderDropdown.isHidden {
                  self.genderDropdown.show()
            } else {
                  self.genderDropdown.hide()
            }
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SignUpMyProfileVC)
      }

      // ********** Function to first carryout the validation check and then request to store the data **********//
      func submitFunction(name: String, email: String, password: String, mobNumber: String, reEnterPassword: String) {
            self.submitButton.isUserInteractionEnabled = false
            var isValidationSuccessful: Bool = true

            if (name.characters.count) <= 0 {
                  self.nameTextField.configOnError(withPlaceHolder: self.signupmsg.username)
                  placeHolderOne = nameTextField.placeholder!
                  isValidationSuccessful = false
            } else {
                  self.nameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)
                  placeHolderTwo = nameTextField.placeholder!
            }

        let _: String = emailTextField.text!
            if (email.characters.count) <= 0 {
                  self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  placeHolderThree = emailTextField.placeholder!
                  isValidationSuccessful = false
            } else {
                  if validationController.isValidEmail(testStr: email) {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                        placeHolderFour = emailTextField.placeholder!
                  } else {
                        self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.invalidemail)
                        isValidationSuccessful = false
                        placeHolderFive = emailTextField.placeholder!
                  }
            }

            if (password.characters.count) <= 0 {
                  self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.enterpassword)
                  placeHolderSix = passwordTextField.placeholder!
                  isValidationSuccessful = false
            } else {
                  if validationController.isValidPassword(testStr: password) {
                        self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
                        placeholderSeven = passwordTextField.placeholder!
                  } else {
                        self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.invalidpassword)
                    self.view.makeToast(self.signupmsg.passwordPolicy, duration: self.signupmsg.toastDelayMedium, position: .bottom)
                        placeholderEight = passwordTextField.placeholder!
                        isValidationSuccessful = false
                  }
            }

            if (mobNumber.characters.count) <= 0 {
                  self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  placeholderNine = mobileNumber.placeholder!
                  isValidationSuccessful = false
            } else {
                if (mobNumber.characters.count) > self.minMobNumDigits && (mobNumber.characters.count) < self.maxMobNumDigits {
                        self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                        placeholderTen = mobileNumber.placeholder!
                  } else {
                        self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.invalidphone)
                        placeholderEleven = mobileNumber.placeholder!
                        isValidationSuccessful = false
                  }
            }

            if (reEnterPassword.characters.count) <= 0 {
                  self.reenterPasswordTextField.configOnError(withPlaceHolder: self.signupmsg.enterReEnterPassword)
                  placeholderTwelve = reenterPasswordTextField.placeholder!
                  isValidationSuccessful = false
            } else {
                  let password = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  let reenterPassword = reEnterPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                  if (password != reenterPassword) {
                        self.reenterPasswordTextField.configOnError(withPlaceHolder: self.signupmsg.paswordmismatch)
                        placeholderThirteen = reenterPasswordTextField.placeholder!
                        isValidationSuccessful = false
                  } else {
                        self.reenterPasswordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.reenterpassword)
                        placeholderFourteen = reenterPasswordTextField.placeholder!
                  }
            }

            if isValidationSuccessful {
                  guard NetworkStatus.sharedManager.isNetworkReachable()
                        else {
                              self.showToast(message: self.profilemsg.networkfailureMsg)
                            self.submitButton.isUserInteractionEnabled = true
//                              self.startInteration()
                              return
                  }
                  self.requestObject()
            }else{
//                self.startInteration()
                self.submitButton.isUserInteractionEnabled = true
            }
      }

      // **** Show prefilled data on back button of further pages of sign-Up flow
      func showPreFilledData() {
            self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpBasicInfo") as? [String: Any]
            if let userObj = self.obj {
                  self.nameTextField.text = userObj["name"] as? String ?? ""
                  self.emailTextField.text = userObj["email"] as? String ?? ""
                  self.countryCode.text = userObj["countryCode"] as? String ?? ""
                  self.mobileNumber.text = userObj["phonenumber"] as? String ?? ""
                  countryPicker.setCountryByPhoneCode(self.countryCode.text!)
                  let gender = userObj["gender"] as? String ?? ""
                  self.genderTextField.text = gender
                
            }

      }

      // ********** End of Function to first carryout the validation check and then request to store the data **********//
      func requestObject() {
            self.submitButton.isUserInteractionEnabled = false
            self.startLoader()
            let userName = nameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let email = emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let countryCode = self.countryCode.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            var mobile = mobileNumber.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if(mobile.first == "0") {
                  mobile = String(mobile.dropFirst(1))
            }
            let password = passwordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let signupMethod = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpMethod") as? String ?? ""
            let feId = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpFisheyeId") as? String ?? ""
            var genderSelection = self.genderTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
            var parameters: [String: Any]
            var awsURL = ""
            if genderSelection == ""{
                  genderSelection = "Rather not say"
            } else {
                  genderSelection = selectedText
            }
            if modificationDataCalled == false {
                  parameters =
                        [
                              "action": "adduserdetails",
                              "user":
                                    [
                                          "email": email,
                                          "name": userName,
                                          "countryCode": countryCode,
                                          "phonenumber": mobile,
                                          "password": password,
                                          "signupMethod": signupMethod,
                                          "privacyEnabled": true,
                                          "gender": genderSelection
                              ]
                  ]
                  awsURL = AppConfig.addBasicInfo
            }
                  // ********** Function called to modify the user details that already exist but the mobile and email is not verified **********//
                  // Description: Details of users whose mobile number and email are not verifed can only be allowed to modify their details so we need to call different api to update the details. If their mobile number and email gets verified then their details can't be updated
            else {

                if let userObj = self.obj {
                        if((email != userObj["email"] as? String ?? "") || (mobile != userObj["phonenumber"] as? String ?? "") || (countryCode != userObj["countryCode"] as? String ?? "")) {
                              self.isImportantDetailsChanged = true
                        }
                  }
                  parameters =
                        [
                              "action": "adduserdetails",
                              "idtoken": idtoken,
                              "user":
                                    [
                                          "email": email,
                                          "name": userName,
                                          "countryCode": countryCode,
                                          "phonenumber": mobile,
                                          "password": password,
                                          "signupMethod": signupMethod,
                                          "privacyEnabled": true,
                                          "id": feId,
                                          "gender": genderSelection
                              ],
                              "isImportantDetailsChanged": self.isImportantDetailsChanged
                  ]
                  awsURL = AppConfig.modifyUserDetails
            }

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, error: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        self.stopLoader()
                        let user1 = data["user"] as? [String: Any]
                        if(user1 != nil) {
                              self.appSharedPrefernce.setAppSharedPreferences(key: "signUpBasicInfo", value: data["user"] as? [String: Any])
                        }
                        self.idtoken = data["idtoken"]as? String ?? ""
                        let statusCode = data["statusCode"] as? String ?? ""
                        if statusCode != "4"{
                              if let user = data["user"] as? [String: Any] {
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "idtoken", value: self.idtoken)
                                    self.mobileNumber1 = user["phonenumber"] as? String ?? ""
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.mobileNumber1)
                                    self.countryCod = user["countryCode"] as? String ?? ""
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "countrycode", value: self.countryCod)
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "idtoken", value: data["idtoken"] as? String ?? "")
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "signUpFisheyeId", value: user["id"] as? String ?? "")
                                    self.emailIdd = user["email"] as? String ?? ""
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "email", value: self.emailIdd)
                                    self.phoneNumberVerified1 = user["phonenumber_verified"] as? Bool
                                    self.emailVerifyFlag = user["email_verified"] as? Bool
                                    self.isTermsNConditionsAccepted = user["isTermsNConditionsAccepted"] as? Bool
                                    self.isSecurityAnsesExists = user["securityQuesAnses_exist"] as? Bool

                                    if(statusCode == "0") {
                                        self.appSharedPrefernce.setAppSharedPreferences(key: "loginPwd", value: password)
                                          self.sendOtpToRegisteredNumber()
                                          GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "SignUp incomplete - Redirection to Terms & Conditions screen", value: 0)
                                    } else if(statusCode == "1") {
                                        if(self.isSecurityAnsesExists == true || self.isTermsNConditionsAccepted == true) {
                                            self.showToast(message: self.signupmsg.userexistlogin)
                                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                                self.backFunction()
                                            })
                                          } else {
                                            self.appSharedPrefernce.setAppSharedPreferences(key: "loginPwd", value: password)
                                            self.showToast(message: self.signupmsg.userexist)
                                            self.sendOtpToRegisteredNumber()
                                            GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "SignUp incomplete - Terms & Conditions accepted - Send OTP to registered number", value: 0)
                                          }
                                    } else if(statusCode == "2" || statusCode == "3") {
                                        if(self.isSecurityAnsesExists == true || self.isTermsNConditionsAccepted == true) {
                                                self.showToast(message: self.signupmsg.userexistlogin)
                                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                                    self.backFunction()
                                                })
                                          } else {
                                            self.appSharedPrefernce.setAppSharedPreferences(key: "loginPwd", value: password)
                                                self.showToast(message: self.signupmsg.userexist)
                                                self.sendOtpToRegisteredNumber()
                                                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Show popup - User Already exists by this email. Do you wish to Modify the Email?", value: 0)
                                          }
                                    } else if(statusCode == "5") {
                                          self.showToast(message: self.signupmsg.twoUsersFoundWhileSignUp)
                                          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                                self.backFunction()
                                          })
                                          GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Two different users found while sign-Up.", value: 0)
                                          return
                                    }
                              } else if(statusCode == "5") {
                                    self.showToast(message: self.signupmsg.twoUsersFoundWhileSignUp)
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                          self.backFunction()
                                    })
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Two different users found while sign-Up.", value: 0)
                                    return
                              }
                        } else if(statusCode=="4") {
                              self.showToast(message: self.profilemsg.servererror)
//                              self.startInteration()
                            self.submitButton.isUserInteractionEnabled = true
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Failed to submit details", value: 0)
                              return
                        }

                  } else {
                    self.submitButton.isUserInteractionEnabled = true
                    
                }
            })
            task.resume()
            session.finishTasksAndInvalidate()
            // ******** End of Function called to modify the user details that already exist but the mobile and email is not verified ********//

      }

      func sendOtpToRegisteredNumber() {

            guard NetworkStatus.sharedManager.isNetworkReachable()
                  else {
                        self.showToast(message: self.profilemsg.networkfailureMsg)
                        return
            }
            DispatchQueue.main.async{
                self.startLoader()
            }
            let parameters =
                  [

                        "action": "sendotp",
                        "idtoken": self.idtoken,
                        "phonenumber": self.mobileNumber1,
                        "countryCode": self.countryCod,
                        "email": self.emailIdd
                        ]  as [String: Any]

            let awsURL = AppConfig.sendOTPAPI

            guard let URL = URL(string: awsURL) else { return }

            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        DispatchQueue.main.async{
                            self.stopLoader()
                        }
                        if(data["statusCode"] as? String ?? "" == "200") {
                              self.showToast(message: self.profilemsg.otpsent)
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.redirectToOTPVC()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Phone number not verified - OTP sent successfully", value: 0)
                        } else {
                              self.showToast(message: self.signupmsg.smssendingfailed)
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Phone number not verified - Failed to sent OTP", value: 0)
                        }

                  } else {
                        self.showToast(message: self.signupmsg.smssendingfailed)
                        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Login failed", value: 0)
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }

      //Adding Information to shared preferences for Pop-ups
      func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, prevaction: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.appSharedPrefernce.setAppSharedPreferences(key: "prevaction", value: prevaction)
      }
      //End of Adding Information to shared preferences for Pop-ups

      func redirectToOTPVC() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
            nextVC.startInteraction = self as StartInteraction
            self.present(nextVC, animated: true, completion: nil)
      }

      func backFunction() {
            DispatchQueue.main.async {
                  self.dismiss(animated: true, completion: nil)
            }
      }

      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ActionPopUpViewController") as! ActionPopUpViewController
            nextVC.modifySignUpDataProtocol = self as ModifySignUpDataProtocol
            self.present(nextVC, animated: true, completion: nil)
      }

      func startLoader() {
            DispatchQueue.main.async {
                ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                  ScreenLoader.shared.stopLoader()
            }
      }

//      func stopInteraction(){
//        DispatchQueue.main.async {
//            self.submitButton.isUserInteractionEnabled = false
//        }
//      }
    
//    func startInteration(){
//        DispatchQueue.main.async {
//            self.submitButton.isUserInteractionEnabled = true
//        }
//    }
//
      // a picker item was selected
      func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
            self.countryCode.text = phoneCode
            self.countryImageDisplay.image = flag
            myProfileView.isHidden = false
            countryPicker.isHidden = true
      }

      func profileCardShadowView() {
            self.myProfileView.layer.shadowColor = UIColor.black.cgColor
            self.myProfileView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.myProfileView.layer.shadowOpacity = 0.4
            self.myProfileView.layer.shadowRadius = 6.0
            self.myProfileView.layer.masksToBounds = false
      }

      //Code to put validation on all textfields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  nameTextField:
                  self.nameBlackUnderline.isHidden = true
                  self.nameBlueUnderline.isHidden = false
                  if (nameTextField.text?.characters.count)! <= 0 {
                        self.nameTextField.configOnError(withPlaceHolder: self.signupmsg.username)
                  } else {
                        self.nameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)
                  }

            case emailTextField:

                  self.emailBlueUnderline.isHidden = false
                  self.emailBlackUnderline.isHidden = true

                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  } else {
                        self.emailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                  }

            case passwordTextField:
                  self.passwordBlueUnderline.isHidden = false
                  self.passwordBlackunderline.isHidden = true
                  if (passwordTextField.text?.characters.count)! <= 0 {
                        self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.enterpassword)
                  } else {
                        self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
                  }

            case mobileNumber:
                  self.mobileNumberBlueUnderline.isHidden = false
                  self.mobileNumberBlackUnderline.isHidden = true
                  if (mobileNumber.text?.characters.count)! <= 0 {
                        self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  } else {
                        self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                  }

            case reenterPasswordTextField:
                  self.reEnterPwdBlueUnderline.isHidden = false
                  self.reEnterPwdBlackUnderline.isHidden = true
                  if (reenterPasswordTextField.text?.characters.count)! <= 0 {
                        self.reenterPasswordTextField.configOnError(withPlaceHolder: self.signupmsg.enterReEnterPassword)
                  } else {
                        self.reenterPasswordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.reenterpassword)
                  }

            default:
                  break
            }
      }

      //Function to Handle Hide show of the underline
    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case  nameTextField:
                  self.nameBlackUnderline.isHidden = false
                  self.nameBlueUnderline.isHidden = true
                  if (nameTextField.text?.characters.count)! <= 0 {
                        self.nameTextField.configOnError(withPlaceHolder: self.signupmsg.username)
                  } else {
                        self.nameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)
                  }

            case emailTextField:
                  self.emailBlueUnderline.isHidden = true
                  self.emailBlackUnderline.isHidden = false
                  let emailText: String = textField.text!
                  if (emailTextField.text?.characters.count)! <= 0 {
                        self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  } else {
                        if validationController.isValidEmail(testStr: emailText) {
                              self.emailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                        } else {
                              self.emailTextField.configOnError(withPlaceHolder: self.signupmsg.invalidemail)
                        }
                  }

            case passwordTextField:

                  self.passwordBlueUnderline.isHidden = true
                  self.passwordBlackunderline.isHidden = false
                  let passwordText: String = textField.text!
                  if (passwordTextField.text?.characters.count)! <= 0 {
                        self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.enterpassword)
                  } else {
                        if validationController.isValidPassword(testStr: passwordText) {
                              self.passwordTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.password)
                        } else {
                              self.passwordTextField.configOnError(withPlaceHolder: self.signupmsg.invalidpassword)
                            self.view.makeToast(self.signupmsg.passwordPolicy, duration: self.signupmsg.toastDelayMedium, position: .bottom)
                        }
                  }

            case mobileNumber:
                  self.mobileNumberBlueUnderline.isHidden = true
                  self.mobileNumberBlackUnderline.isHidden = false
                  if (mobileNumber.text?.characters.count)! <= 0 {
                        self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  } else {
                        self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                    if (mobileNumber.text?.characters.count)! > self.minMobNumDigits && (mobileNumber.text?.characters.count)! < self.maxMobNumDigits {
                              self.mobileNumber.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                        } else {
                              self.mobileNumber.configOnError(withPlaceHolder: self.signupmsg.invalidphone)
                        }
                    self.showToast(message: self.signupmsg.phoneNumberShouldBeValid)
                  }

            case reenterPasswordTextField:
                  self.reEnterPwdBlueUnderline.isHidden = true
                  self.reEnterPwdBlackUnderline.isHidden = false
                  if (reenterPasswordTextField.text?.characters.count)! <= 0 {
                        self.reenterPasswordTextField.configOnError(withPlaceHolder: self.signupmsg.enterReEnterPassword)
                  } else {
                        let password = passwordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        let reenterPassword = reenterPasswordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                        if (password != reenterPassword) {
                              self.reenterPasswordTextField.configOnError(withPlaceHolder: self.signupmsg.paswordmismatch)
                        }
                  }

            default:
                  break
            }
      }
      //@objc private func textFieldShouldReturn(_ textField: UICustomTextField) -> Bool {
        @objc func textFieldShouldReturn(_ textField: UICustomTextField) -> Bool {
            switch(textField) {
            case  nameTextField:
                  emailTextField.becomeFirstResponder()
                  break

            case emailTextField:
                  mobileNumber.becomeFirstResponder()
                  break

            case passwordTextField:
                  reenterPasswordTextField.becomeFirstResponder()
                  break

            case mobileNumber:
                  passwordTextField.becomeFirstResponder()
                  break

            case reenterPasswordTextField:
                  textField.resignFirstResponder()
                  break
                
            default:
                  break
            }

            return false
      }

      //End of Function to Handle Hide show of the underline

      // Show toast on view
      //Author: Vaishali
      func showToast(message: String) {
            DispatchQueue.main.async {
                self.view.makeToast(message, duration: self.signupmsg.toastDelayTime, position: .bottom)
            }
      }
      // end of function showing toast on screen

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
      }

      class RadialGradientLayer: CALayer {

            var center: CGPoint {
                  return CGPoint(x: bounds.width/2, y: bounds.height/2)

            }

            var radius: CGFloat {
                  return (bounds.width + bounds.height)/2
            }

            var colors: [UIColor] = [] {
                  didSet {
                        setNeedsDisplay()
                  }
            }

            var cgColors: [CGColor] {
                  return colors.map({ (color) -> CGColor in
                        return color.cgColor
                  })
            }

            override init() {
                  super.init()
                  needsDisplayOnBoundsChange = true
            }

            required init(coder aDecoder: NSCoder) {
                  super.init()
            }

            override func draw(in ctx: CGContext) {
                  ctx.saveGState()
                  let colorSpace = CGColorSpaceCreateDeviceRGB()
                  let locations: [CGFloat] = [0.0, 1.0]
                  guard let gradient = CGGradient(colorsSpace: colorSpace, colors: cgColors as CFArray, locations: locations) else {
                        return
                  }
                  ctx.drawRadialGradient(gradient, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: radius, options: CGGradientDrawingOptions(rawValue: 0))
            }

      }

      class RadialGradientView: UIView {

            private let gradientLayer = RadialGradientLayer()

            public var colors: [UIColor] {
                  get {
                        return gradientLayer.colors
                  }
                  set {
                        gradientLayer.colors = newValue
                  }
            }

            override func layoutSubviews() {
                  super.layoutSubviews()
                  if gradientLayer.superlayer == nil {
                        layer.insertSublayer(gradientLayer, at: 0)
                  }
                  gradientLayer.frame = bounds
            }
        
      }
}

extension SignUpMyProfileVC: ModifySignUpDataProtocol {
      func modifyData(toReload: Bool) {
            if toReload {
                  self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpBasicInfo")  as? [String: Any]
                if let userObj = self.obj {
                        self.nameTextField.text = userObj["name"] as? String ?? ""
                  }
                  self.passwordTextField.text = ""
                  self.reenterPasswordTextField.text = ""
                  self.modificationDataCalled = true
            }
      }
}

extension SignUpMyProfileVC: StartInteraction {
    func startButtonInteractions() {
        self.submitButton.isUserInteractionEnabled = true
    }
}
