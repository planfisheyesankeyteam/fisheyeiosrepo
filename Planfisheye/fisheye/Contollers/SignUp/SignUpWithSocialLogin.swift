//
//  SignUpWithSocialLogin.swift
//  fisheye
//
//  Created by Sankey Solutions on 28/11/17.
//  Copyright © 2017 Sankey Solutions. All rights reserved.
//

import Foundation
import UIKit
import MRCountryPicker
import DropDown
import Toast_Swift

class SignUpWithSocialLogin: UIViewController, MRCountryPickerDelegate, UITableViewDelegate, UITableViewDataSource {

      //Outlets
      @IBOutlet weak var pickerButton: UIButton!
      @IBAction func pickerButtonTapped(_ sender: Any) {
            mainView.isHidden = true
            countryPicker.isHidden = false
      }
      @IBOutlet weak var backButton: UIButton!
      @IBOutlet weak var socialSignUpSaveButton: UIButton!
      @IBOutlet weak var socialPhoneCodeTextField: UICustomTextField!
      @IBOutlet weak var socialPhoneNoTextField: UICustomTextField!
      @IBOutlet weak var socialEmailTextField: UICustomTextField!
      @IBOutlet weak var socialNameTextField: UICustomTextField!

      @IBOutlet weak var genderTextField: UICustomTextField!
      @IBOutlet weak var genderBtn: UIButton!
      @IBOutlet weak var nameBlackUndrline: UIImageView!
      @IBOutlet weak var nameBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlueUnderline: UIImageView!
      @IBOutlet weak var emailBlackUnderline: UIImageView!
      @IBOutlet weak var mobilenumberBlackUnderline: UIImageView!
      @IBOutlet weak var flagView: UIImageView!
      @IBOutlet weak var mainView: UIView!
      @IBOutlet weak var mobileNumberBlueUndeline: UIImageView!
      @IBOutlet weak var signInLabel: UILabel!
      @IBOutlet weak var userProfileLabel: UILabel!
      @IBOutlet weak var fisheyeMotto: UILabel!

    @IBOutlet weak var scrollView: UIScrollView!
    //Variables
      var appService = AppService()
      let validationController = ValidationController()
      let appSharedPrefernce = AppSharedPreference()
      var idtoken = ""
      var mobileNumber = ""
      var countryCode = ""
      var signUpMethod = ""
      var emailId = ""
      var placeholderone = ""
      var placeholdertwo = ""
      var placeholderthree = ""
      var placeholderfour = ""
      var placeholderfive = ""
      var placeholdersix  = ""
      var placeholderseven = ""
      var placeholdereight = ""

      var selectedText: String!
      var genderMaleSelectedFlag = false
      var genderFemaleSelectedFlag = false
      var modificationDataCalled = false
      var isImportantDetailsChanged: Bool = false
      var obj: [String: Any]?
      var isToDisplayData: Bool = false
      var isTermsNConditionsAccepted: Bool? = false
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      let genderDropdown: DropDown = DropDown()
      var genderArray = ["Male", "Female", "Trans-Female", "Bi-Gender", "Non-Binary", "Gender nonconforming", "Undisclosed", "Rather not say" ]
      var englishGenderArray: [String] = []
      var selectedGenderIndex: Int = 7
    var minMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBelesserThan
    var maxMobNumDigits = ProfileToastMsgHeadingSubheadingLabels.shared.mobNumLengthCantBeGreaterThan

      @IBOutlet weak var genderTableView: UITableView!
      @IBOutlet weak var maleGenderBtn: UIButton!
      @IBOutlet weak var femaleGenderBtn: UIButton!

      @IBAction func backButtonTapped(_ sender: Any) {
            self.appSharedPrefernce.removeAppSharedPreferences(key: "loginId")
            self.appSharedPrefernce.removeAppSharedPreferences(key: "loginPwd")
            self.appSharedPrefernce.removeAppSharedPreferences(key: "loginMethod")

            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(nextVC, animated: true, completion: nil)
      }

      // a picker item was selected
      @IBOutlet weak var countryPicker: MRCountryPicker!
      func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
            self.socialPhoneCodeTextField.text = phoneCode
            self.flagView.image = flag
            mainView.isHidden = false
            countryPicker.isHidden = true
      }

      override func viewDidLoad() {
            super.viewDidLoad()
        self.stopLoader()
        self.scrollView.layer.cornerRadius = 6.0
        self.countryPicker.layer.cornerRadius = 6.0 
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
            self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
            self.appService = AppService.shared
            self.englishGenderArray = self.genderArray
            self.socialEmailTextField.isUserInteractionEnabled =  false
            countryPicker.countryPickerDelegate = self
            if !isToDisplayData {
                  // set country by its code
                  countryPicker.setCountry("GB")
            }

            self.loadSocialSignUpData()

            // optionally set custom locale; defaults to system's locale
            countryPicker.setLocale("gb_GB")

            // set country by its name
            countryPicker.setCountryByName("London")
            self.profileCardShadowView()
            countryPicker.showPhoneNumbers = true

            self.socialNameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.socialNameTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.socialEmailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.socialEmailTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.socialPhoneNoTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.socialPhoneNoTextField.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)
            self.socialPhoneCodeTextField.isUserInteractionEnabled = false
            self.initializeGender()
            self.setLocalizationText()
            self.addObservers()
      }

      func initializeGender() {
            self.genderArray = signupmsg.genderArrayList
            self.genderTextField.isUserInteractionEnabled = false
            self.genderDropdown.anchorView = self.genderTableView
            self.genderDropdown.dataSource = self.genderArray
            self.genderDropdown.width = 200
            self.genderDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
                  self.genderTextField.text = item
                  self.selectedGenderIndex = index
                
                _ = self.signupmsg.localGenderArrayList.filter( { return ($0.genderTranslatedText == item ) } )
                
                //self.genderTextField.text = objArray[0].genderTranslatedText
                self.genderTextField.text = "Male"
                
                //self.selectedText = objArray[0].genderActualText
                self.selectedText = "Male"
            }
      }

      // add all observers to this viewController here
      func addObservers() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }
      // End

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.signInLabel.text = self.signupmsg.signIn
                  self.userProfileLabel.text = self.signupmsg.userProfile
                  self.fisheyeMotto.text = self.signupmsg.letsWorkTogether
                  self.socialNameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)

                  self.socialEmailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                  self.socialPhoneNoTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                  self.genderTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.selectGender)
                  self.socialPhoneCodeTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.code)
            }
      }
      /* END */

      @IBAction func genderbtnTapped(_ sender: Any) {
            if  genderDropdown.isHidden {
                  self.genderDropdown.show()
            } else {
                  self.genderDropdown.hide()
            }
      }

      public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.genderArray.count
      }

      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            self.genderTextField.text = self.genderArray[indexPath.row]
            self.genderTableView.isHidden = true
      }

      public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = self.genderArray[indexPath.row]
            return cell
      }

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SignUpWithSocialLogin)
      }

      func profileCardShadowView() {
            self.mainView.layer.shadowColor = UIColor.black.cgColor
            self.mainView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.mainView.layer.shadowOpacity = 0.4
            self.mainView.layer.shadowRadius = 6.0
            self.mainView.layer.masksToBounds = false
            self.mainView.layer.cornerRadius = 6
      }

      // Redirection to Terms and Condition Page
      func redirectToTermsAndCondition() {
            DispatchQueue.main.async {
                  let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                  let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
                  self.present(nextVC, animated: true, completion: nil)
            }
      }
      // End ofRedirection to Terms and Condition Page

    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case  socialNameTextField:
                  self.nameBlackUndrline.isHidden = true
                  self.nameBlueUnderline.isHidden = false
                  if (socialNameTextField.text?.characters.count)! <= 0 {
                        self.socialNameTextField.configOnError(withPlaceHolder: self.signupmsg.username)
                  } else {
                        self.socialNameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)
                  }

            case socialEmailTextField:
                  self.emailBlueUnderline.isHidden = false
                  self.emailBlackUnderline.isHidden = true
                  if (socialEmailTextField.text?.characters.count)! <= 0 {
                        self.socialEmailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  } else {
                        self.socialEmailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                  }

            case socialPhoneNoTextField:
                  self.mobileNumberBlueUndeline.isHidden = false
                  self.mobilenumberBlackUnderline.isHidden = true
                  if (socialPhoneNoTextField.text?.characters.count)! <= 0 {
                        self.socialPhoneNoTextField.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  } else {
                        self.socialPhoneNoTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                  }

            default:
                  break
            }
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {

            case  socialNameTextField:
                  self.nameBlackUndrline.isHidden = false
                  self.nameBlueUnderline.isHidden = true
                  if (socialNameTextField.text?.characters.count)! <= 0 {
                        self.socialNameTextField.configOnError(withPlaceHolder: self.signupmsg.username)
                  } else {
                        self.socialNameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)
                  }

            case socialEmailTextField:
                  self.emailBlueUnderline.isHidden = true
                  self.emailBlackUnderline.isHidden = false
                  let emailText: String = textField.text!
                  if (socialEmailTextField.text?.characters.count)! <= 0 {
                        self.socialEmailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  } else {
                        if validationController.isValidEmail(testStr: emailText) {
                              self.socialEmailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                        } else {
                              self.socialEmailTextField.configOnError(withPlaceHolder: self.signupmsg.invalidemail)
                        }
                  }

            case socialPhoneNoTextField:
                  self.mobileNumberBlueUndeline.isHidden = true
                  self.mobilenumberBlackUnderline.isHidden = false
                  if (socialPhoneNoTextField.text?.characters.count)! <= 0 {
                        self.socialPhoneNoTextField.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  } else {
                        self.socialPhoneNoTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                    if (socialPhoneNoTextField.text?.characters.count)! > self.minMobNumDigits && (socialPhoneNoTextField.text?.characters.count)! < self.maxMobNumDigits {
                              self.socialPhoneNoTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                        } else {
                              self.socialPhoneNoTextField.configOnError(withPlaceHolder: self.signupmsg.invalidphone)
                        }
                    self.showToast(message: self.signupmsg.phoneNumberShouldBeValid)
                  }

            default:
                  break
            }
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      @IBAction func onBackButtonPressed(_ sender: Any) {

      }

    
//    func stopInteraction(){
//        DispatchQueue.main.async {
//            self.socialSignUpSaveButton.isUserInteractionEnabled = false
//        }
//    }
//
//    func startInteration(){
//        DispatchQueue.main.async {
//            self.socialSignUpSaveButton.isUserInteractionEnabled = true
//        }
//    }
    
      @IBAction func onSaveButtonTapped(_ sender: Any) {
//            self.stopInteraction()
        self.socialSignUpSaveButton.isUserInteractionEnabled = false
            self.saveButtonTapped(socialName: socialNameTextField.text!, socialEmail: socialEmailTextField.text!, socialPhone: socialPhoneNoTextField.text!)
      }

      func saveButtonTapped(socialName: String, socialEmail: String, socialPhone: String) {
            var isValidationSuccessful: Bool = true

            if (socialName.characters.count) <= 0 {
                  self.socialNameTextField.configOnError(withPlaceHolder: self.signupmsg.username)
                  placeholderone = socialNameTextField.placeholder!
                  isValidationSuccessful = false
            } else {
                  self.socialNameTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.name)
                  placeholdertwo = socialNameTextField.placeholder!
            }

            //let emailText: String = socialEmailTextField.text!
            if (socialEmail.characters.count) <= 0 {
                  self.socialEmailTextField.configOnError(withPlaceHolder: self.signupmsg.enteremail)
                  placeholderthree = socialEmailTextField.placeholder!
                  isValidationSuccessful = false
            } else {
                  if validationController.isValidEmail(testStr: socialEmail) {
                        self.socialEmailTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.email)
                        placeholderfour = socialEmailTextField.placeholder!
                  } else {
                        self.socialEmailTextField.configOnError(withPlaceHolder: self.signupmsg.invalidemail)
                        placeholderfive = socialEmailTextField.placeholder!
                        isValidationSuccessful = false
                  }
            }

            if (socialPhone.characters.count) <= 0 {
                  self.socialPhoneNoTextField.configOnError(withPlaceHolder: self.signupmsg.entermobilenumber)
                  placeholdereight = socialPhoneNoTextField.text!
                  isValidationSuccessful = false
            } else {
                if (socialPhone.characters.count) > self.minMobNumDigits && (socialPhone.characters.count) < self.maxMobNumDigits {
                        self.socialPhoneNoTextField.configOnErrorCleared(withPlaceHolder: self.signupmsg.Phoneno)
                        placeholdersix = socialPhoneNoTextField.placeholder!
                  } else {
                        self.socialPhoneNoTextField.configOnError(withPlaceHolder: self.signupmsg.invalidphone)
                        placeholderseven = socialPhoneNoTextField.placeholder!
                        isValidationSuccessful = false
                  }
            }

            if isValidationSuccessful {
                  guard NetworkStatus.sharedManager.isNetworkReachable()
                        else {
                              DispatchQueue.main.async {
                                self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                                    self.socialSignUpSaveButton.isUserInteractionEnabled = true
                              }
                              return
                  }
                  self.requestObject()
            }else{
                self.socialSignUpSaveButton.isUserInteractionEnabled = true
            }
      }

      func loadSocialSignUpData() {
            self.socialEmailTextField.text = self.appSharedPrefernce.getAppSharedPreferences(key: "socialemail") as? String ?? ""
            self.socialNameTextField.text = self.appSharedPrefernce.getAppSharedPreferences(key: "socialname") as? String ?? ""
            if isToDisplayData {
                  self.obj = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpBasicInfo")  as? [String: Any]
                if let userObj = self.obj {
                        self.socialPhoneNoTextField.text = userObj["phonenumber"] as? String ?? ""
                        self.socialPhoneCodeTextField.text = userObj["countryCode"] as? String ?? ""
                        countryPicker.setCountryByPhoneCode(self.socialPhoneCodeTextField.text!)
                        let gender = userObj["gender"] as? String ?? ""
                        self.genderTextField.text = gender
                  }

            }
      }

      func requestObject() {

            self.startLoader()
            let userName = self.socialNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let email = self.socialEmailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let countryCode = self.socialPhoneCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let mobile = self.socialPhoneNoTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let signupMethod = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpMethod") as? String ?? ""
            let feId = self.appSharedPrefernce.getAppSharedPreferences(key: "signUpFisheyeId") as? String ?? ""
            var genderSelection = self.genderTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)

            if genderSelection == ""{
                  genderSelection = "Rather not say"
            } else {
                  genderSelection = selectedText
            }

            var awsURL = ""

            var parameters: [String: Any] = [:]

            if modificationDataCalled == false {
                  parameters =
                        [
                              "action": "adduserdetails",
                              "user":
                                    [
                                          "email": email,
                                          "name": userName,
                                          "countryCode": countryCode,
                                          "phonenumber": mobile,
                                          "password": "password",
                                          "signupMethod": signupMethod,
                                          "privacyEnabled": false,
                                          "gender": genderSelection
                              ]
                  ]
                  awsURL = AppConfig.addBasicInfo
            }
                  // ********** Function called to modify the user details that already exist but the mobile and email is not verified **********//
                  // Description: Details of users whose mobile number and email are not verifed can only be allowed to modify their details so we need to call different api to update the details. If their mobile number and email gets verified then their details can't be updated
            else {
                if let userObj = self.obj {
                        if((email != userObj["email"] as? String ?? "") || (mobile != userObj["phonenumber"] as? String ?? "") || (countryCode != userObj["countryCode"] as? String ?? "")) {
                              self.isImportantDetailsChanged = true
                        }
                  }
                  parameters =
                        [
                              "action": "adduserdetails",
                              "idtoken": idtoken,
                              "user":
                                    [
                                          "email": email,
                                          "name": userName,
                                          "countryCode": countryCode,
                                          "phonenumber": mobile,
                                          "password": "password",
                                          "signupMethod": signupMethod,
                                          "privacyEnabled": true,
                                          "id": feId,
                                          "gender": genderSelection
                              ],
                              "isImportantDetailsChanged": self.isImportantDetailsChanged
                  ]
                  awsURL = AppConfig.modifyUserDetails
            }

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        self.stopLoader()
                        let statusCode = data["statusCode"] as? String ?? ""
                        if statusCode != "4"{
                              if let user = data["user"] as? [String: Any] {
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "signUpBasicInfo", value: data["user"] as? [String: Any])
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "signUpFisheyeId", value: user["id"] as? String ?? "")
                                    let phoneNumberVerified1 = user["phonenumber_verified"] as? Bool
                                    let emailVerifyFlag = user["email_verified"] as? Bool
                                    let isTermsNConditionsAccepted = user["isTermsNConditionsAccepted"] as? Bool
                                    let isSecurityAnsesExists = user["securityQuesAnses_exist"] as? Bool

                                    if(statusCode == "0") {
                                          self.idtoken = data["idtoken"] as? String ?? ""
                                          self.mobileNumber = user["phonenumber"] as? String ?? ""
                                          self.countryCode = user["countryCode"] as? String ?? ""
                                          self.emailId = user["email"] as? String ?? ""
                                          self.appSharedPrefernce.setAppSharedPreferences(key: "idtoken", value: self.idtoken)
                                          self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.mobileNumber)
                                          self.appSharedPrefernce.setAppSharedPreferences(key: "countrycode", value: self.countryCode)
                                          self.appSharedPrefernce.setAppSharedPreferences(key: "email", value: self.emailId)

                                          DispatchQueue.main.async {
                                                self.sendOtpToRegisteredNumber()
                                          }

                                          GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "SignUp incomplete - Redirection to Terms & Conditions screen", value: 0)

                                    } else if(statusCode=="1") {

                                          self.idtoken = data["idtoken"] as? String ?? ""
                                          self.appSharedPrefernce.setAppSharedPreferences(key: "idtoken", value: self.idtoken)
                                          self.emailId = user["email"] as? String ?? ""
                                          self.mobileNumber = user["phonenumber"] as? String ?? ""
                                          self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.mobileNumber)
                                          self.countryCode = user["countryCode"] as? String ?? ""
                                          self.appSharedPrefernce.setAppSharedPreferences(key: "countrycode", value: self.countryCode)
                                        self.isTermsNConditionsAccepted = user["isTermsNConditionsAccepted"] as? Bool

                                          if(isSecurityAnsesExists == true || self.isTermsNConditionsAccepted == true) {
                                                self.showToast(message: self.signupmsg.userexistlogin)
                                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                                      self.backFunction()
                                                })
                                          } else {
                                                self.showToast(message: self.signupmsg.userexist)
                                                self.sendOtpToRegisteredNumber()
                                                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "SignUp incomplete - Terms & Conditions accepted - Send OTP to registered number", value: 0)
                                          }
                                    } else if(statusCode=="2" || statusCode=="3") {
                                          self.idtoken = data["idtoken"] as? String ?? ""
                                          self.emailId = user["email"] as? String ?? ""
                                          self.mobileNumber = user["phonenumber"] as? String ?? ""
                                          self.countryCode = user["countryCode"] as? String ?? ""
                                        self.isTermsNConditionsAccepted = user["isTermsNConditionsAccepted"] as? Bool
                                        self.appSharedPrefernce.setAppSharedPreferences(key: "mobile", value: self.mobileNumber)

                                          if(isSecurityAnsesExists == true || self.isTermsNConditionsAccepted == true) {
                                                self.showToast(message: self.signupmsg.userexistlogin)
                                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                                      self.backFunction()
                                                })
                                          } else  {
                                                self.showToast(message: self.signupmsg.userexist)
                                                self.sendOtpToRegisteredNumber()
                                                GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Show popup - User Already exists by this email. Do you wish to Modify the Email?", value: 0)
                                          }
                                    } else if(statusCode=="5") {
                                          self.showToast(message: self.signupmsg.twoUsersFoundWhileSignUp)
                                          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                                self.backFunction()
                                          })
                                          GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Two different users found while sign-Up.", value: 0)
                                    }
                              } else if(statusCode=="5") {
                                    self.showToast(message: self.signupmsg.twoUsersFoundWhileSignUp)
                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime, execute: {
                                          self.backFunction()
                                    })
                                    GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Two different users found while sign-Up.", value: 0)
                              }
                        } else if(statusCode=="4") {
                              DispatchQueue.main.async {
                                    self.view.makeToast(self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit basic details", label: "Failed to submit basic user details", value: 0)
                              return
                        }
                  } else {
                        self.socialSignUpSaveButton.isUserInteractionEnabled = true
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }

      // Show toast on view
      //Author: Vaishali
      func showToast(message: String) {
            DispatchQueue.main.async {
                self.view.makeToast(message, duration: self.signupmsg.toastDelayTime, position: .bottom)
            }
      }
      // end of function showing toast on screen

      //Adding Information to shared preferences for Pop-ups
      func popUpDetails(heading: String, subheading: String, action: String, method: String, module: String, prevaction: String) {
            self.appSharedPrefernce.setAppSharedPreferences(key: "heading", value: heading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "subheading", value: subheading)
            self.appSharedPrefernce.setAppSharedPreferences(key: "action", value: action)
            self.appSharedPrefernce.setAppSharedPreferences(key: "method", value: method)
            self.appSharedPrefernce.setAppSharedPreferences(key: "module", value: module)
            self.appSharedPrefernce.setAppSharedPreferences(key: "prevaction", value: prevaction)
      }

      //End of Adding Information to shared preferences for Pop-ups

      func actionPopUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "ActionPopUpViewController") as! ActionPopUpViewController
        nextVC.modifySignUpDataProtocol = self as ModifySignUpDataProtocol
            self.present(nextVC, animated: true, completion: nil)
      }

      func redirectToOTPVC() {
            DispatchQueue.main.async {
                  let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                  let nextVC = storyBoard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
                  nextVC.startInteraction = self as StartInteraction
                  self.present(nextVC, animated: true, completion: nil)
            }
      }

      func sendOtpToRegisteredNumber() {
            self.startLoader()
            let parameters =
                  [

                        "action": "sendotp",
                        "idtoken": idtoken,
                        "phonenumber": mobileNumber,
                        "countryCode": countryCode,
                        "email": emailId
                        ] as [String: Any]

            let awsURL = AppConfig.sendOTPAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
                        self.stopLoader()
                        let statusCode = data["statusCode"] as? String ?? ""
                        if(statusCode == "200") {
                              self.showToast(message: self.profilemsg.otpsent)
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.redirectToOTPVC()
                              })
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Phone number not verified - OTP sent successfully", value: 0)
                        } else {
                              self.showToast(message: self.signupmsg.smssendingfailed)
                              GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Login", action: "Login to fisheye", label: "Phone number not verified - Failed to sent OTP", value: 0)
                        }
                  } else {
                        self.stopLoader()
                        DispatchQueue.main.async {
                              self.view.makeToast(self.signupmsg.smssendingfailed, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }

      func backFunction() {
            self.dismiss(animated: true, completion: nil)
      }

      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
            self.present(nextVC, animated: true, completion: nil)
      }

      func startLoader() {
            DispatchQueue.main.async {
                  ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                  ScreenLoader.shared.stopLoader()
            }

      }
}

extension SignUpWithSocialLogin: ModifySignUpDataProtocol {
      func modifyData(toReload: Bool) {
            if toReload {
                  self.obj =  self.appSharedPrefernce.getAppSharedPreferences(key: "signUpBasicInfo")  as? [String: Any]
                if let userObj = self.obj {
                        self.socialNameTextField.text = userObj["name"] as? String ?? ""
                  }

                  self.modificationDataCalled = true
            }
      }
}

extension SignUpWithSocialLogin: StartInteraction {
    func startButtonInteractions() {
        self.socialSignUpSaveButton.isUserInteractionEnabled = true
    }
}
