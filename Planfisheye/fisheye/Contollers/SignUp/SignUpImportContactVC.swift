//
//  SignUpImportContactVC.swift
//  fisheye
//
//  Created by Sankey Solution on 11/12/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit

class SignUpImportContactVC: UIViewController {

    @IBOutlet weak var allowBtn: UIButton!
    @IBAction func allowBtnTapped(_ sender: Any) {
    }
    @IBOutlet weak var cancelBtn: UIButton!
    @IBAction func cancelBtnTapped(_ sender: Any) {
    }
    @IBOutlet weak var popUpView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        popUpView.layer.cornerRadius = 6
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
