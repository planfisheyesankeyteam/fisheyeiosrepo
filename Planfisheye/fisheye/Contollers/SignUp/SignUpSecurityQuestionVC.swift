//
//  SignUpSecurityQuestionVC.swift
//  fisheye
//
//  Created by Sankey Solution on 11/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import Toast_Swift

class SignUpSecurityQuestionVC: UIViewController {

      var showView = 0
      var idtoken = ""
      let appSharedPrefernce = AppSharedPreference()
      var masterKey = ""
      var reEnterMasterKey = ""
      var signUpMethod = ""
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
      var signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
      var emptyMasterKey: ToEmptyMasterKeyTextFields?
      var placeholder = ""
      var placeholdertwo = ""
      var placeholderthree = ""
      var placeholderfour = ""
      var placeholderfive = ""
      var placeholdersix = ""
      var fisheyeName = ""
      var fisheyeId = ""
      var email = ""
      var genderSelection = ""
      var isCameAfterRegistration: Bool = false
      var isFromLogin: Bool = true

      @IBOutlet weak var securityQuestion1: UITextField!
      @IBOutlet var skipBtn: UIButton!
      @IBOutlet weak var securityQuestionAnswer1: UICustomTextField!
      @IBOutlet weak var editQuestion1: UIButton!
      @IBOutlet weak var securityQuestion2: UITextField!
      @IBOutlet weak var securityQuestionAnswer2: UICustomTextField!
      @IBOutlet weak var editQuestion2: UIButton!
      @IBOutlet weak var securityQuestion3: UITextField!
      @IBOutlet weak var fisheyeMotto: UILabel!
      @IBOutlet weak var securityQuestionAnswer3: UICustomTextField!
      @IBOutlet weak var editQuestion3: UIButton!
      @IBOutlet weak var submitButton: UIButton!
      @IBOutlet weak var signUpLabel: UILabel!
      @IBOutlet weak var securityQuestion1BlackUnderline: UIImageView!
      @IBOutlet weak var securityQuestion2Blueunderline: UIImageView!
      @IBOutlet weak var securityQuestion2BlackUnderline: UIImageView!
      @IBOutlet weak var securityQuestion1BlueUnderline: UIImageView!
      @IBOutlet weak var securityQuestion3Blueunderline: UIImageView!
      @IBOutlet weak var securityQuestion3BlackUnderline: UIImageView!
      @IBOutlet weak var securityquestionSubView: UIView!
      @IBOutlet weak var securityQuestions: UILabel!
      @IBOutlet var signUpPagerImage: UIImageView!
    
      @IBAction func submitButtonTapped(_ sender: Any) {
            checkThreeQuestionEnteredFunction(answerone: securityQuestionAnswer1.text!, answertwo: securityQuestionAnswer2.text!, answerthree: securityQuestionAnswer3.text!)
      }

      @IBAction func editQuestion1Tapped(_ sender: Any) {
            self.securityQuestion1.isUserInteractionEnabled = true
            self.securityQuestion1.isEnabled = true
            self.securityQuestion1.textColor = UIColor.black
            self.securityQuestion2.textColor = UIColor.questionDisableColor()
            self.securityQuestion3.textColor = UIColor.questionDisableColor()
            self.securityQuestion2.isUserInteractionEnabled = false
            self.securityQuestion2.isEnabled = false
            self.securityQuestion3.isUserInteractionEnabled = false
            self.securityQuestion3.isEnabled = false
      }

      @IBAction func editQuestion2Tapped(_ sender: Any) {
            self.securityQuestion1.isUserInteractionEnabled = false
            self.securityQuestion1.isEnabled = false
            self.securityQuestion2.isUserInteractionEnabled = true
            self.securityQuestion2.isEnabled = true
            self.securityQuestion3.isUserInteractionEnabled = false
            self.securityQuestion3.isEnabled = false
            self.securityQuestion1.textColor = UIColor.questionDisableColor()
            self.securityQuestion2.textColor = UIColor.black
            self.securityQuestion3.textColor = UIColor.questionDisableColor()
      }

      @IBAction func editQuestion3Tapped(_ sender: Any) {
            self.securityQuestion1.isUserInteractionEnabled = false
            self.securityQuestion1.isEnabled = false
            self.securityQuestion2.isUserInteractionEnabled = false
            self.securityQuestion2.isEnabled = false
            self.securityQuestion3.isUserInteractionEnabled = true
            self.securityQuestion3.isEnabled = true
            self.securityQuestion1.textColor = UIColor.questionDisableColor()
            self.securityQuestion2.textColor = UIColor.questionDisableColor()
            self.securityQuestion3.textColor = UIColor.black
      }

      func checkThreeQuestionEnteredFunction(answerone: String, answertwo: String, answerthree: String) {
            let question1 = self.securityQuestion1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let question2 = self.securityQuestion2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let question3 = self.securityQuestion3.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let answer1 = answerone.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
            let answer2 = answertwo.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
            let answer3 = answerthree.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()

            if (question1.characters.count) <= 0 {
            } else if(question2.characters.count) <= 0 {
            } else if(question3.characters.count) <= 0 {
            } else if(answer1.characters.count) <= 0 {
                  self.validationFailurePopUp(answer: 1)
            } else if(answer2.characters.count) <= 0 {
                  self.validationFailurePopUp(answer: 2)
            } else if(answer3.characters.count) <= 0 {
                  self.validationFailurePopUp(answer: 3)
            } else {
                  addsecurityQuestionfunc()
            }
      }

      func validationFailurePopUp(answer: Int) {
            switch answer {
            case 1:
                  self.securityQuestionAnswer1.configOnError(withPlaceHolder: self.signupmsg.questionone)
                  placeholder = securityQuestionAnswer1.placeholder!
            case 2:
                  self.securityQuestionAnswer2.configOnError(withPlaceHolder: self.signupmsg.questionone)
                  placeholdertwo = securityQuestionAnswer1.placeholder!
            case 3:
                  self.securityQuestionAnswer3.configOnError(withPlaceHolder: self.signupmsg.questionone)
                  placeholderthree = securityQuestionAnswer1.placeholder!
            default:
                  break
            }
      }

      func addsecurityQuestionfunc(  ) {
            DispatchQueue.main.async {
                self.startLoader()
                self.submitButton.isUserInteractionEnabled=false
            }
        
            let question1 = self.securityQuestion1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let question2 = self.securityQuestion2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let question3 = self.securityQuestion3.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let answer1 = self.securityQuestionAnswer1.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
            let answer2 = self.securityQuestionAnswer2.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()
            let answer3 = self.securityQuestionAnswer3.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lowercased()

            let parameters =
                  [
                        "action": "addsecurityquesanses",
                        "idtoken": idtoken,
                        "securityQuesAnses":
                              [
                                    [
                                          "securityQuesAnsId": "1",
                                          "securityQues": self.signupmsg.securityQuestionOne,
                                          "securityAns": answer1
                                    ],
                                    [
                                          "securityQuesAnsId": "2",
                                          "securityQues": self.signupmsg.securityQuestionTwo,
                                          "securityAns": answer2
                                    ],
                                    [
                                          "securityQuesAnsId": "3",
                                          "securityQues": self.signupmsg.securityQuestionThree,
                                          "securityAns": answer3
                                    ]
                        ]
                        ] as [String: Any]

            let awsURL = AppConfig.addSecurityQuestionAnswerAPI

            guard let URL = URL(string: awsURL) else { return }
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: URL)
            request.addValue("application/json; charset=utf-8", forHTTPHeaderField: AppConfig.contentType)
            request.addValue(self.idtoken, forHTTPHeaderField: "idtoken")
            request.httpMethod = AppConfig.httpMethodPost
            guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
                  return
            }
            request.httpBody = httpBody

            guard let signedRequest = URLRequestSigner().sign(request: request) else {
                  return
            }

            let task: URLSessionDataTask  = session.dataTask(with: signedRequest, completionHandler: { (data: Data?, _: URLResponse?, _: Error?) -> Void in
                  if let d = data, let jsonOpt = try? JSONSerialization.jsonObject(with: d, options: []) as? [String: AnyObject], let json = jsonOpt {
                        let data = json
//                        self.stopLoader()
                        if(data["statusCode"] as? String ?? "" == "200") {
                            
                            if self.isCameAfterRegistration{
                            
                                self.view.makeToast(self.signupmsg.loginsuccess)
                                self.loadBaseViewController(data: data)
                                if let user = data["user"] as? [String: Any] {
                                    let loginId = user["email"] as? String ?? ""
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "loginId", value: loginId)
                                    LookoutSharedInstances.shared.isLookoutInterestRegistered = user["isLookoutFeedbackRegistered"] as? Bool
                                    
                                }
                                self.startContactFrontEndSync()
                                if(NetworkStatus.sharedManager.isNetworkReachable()) {
                                    self.appSharedPrefernce.setAppSharedPreferences(key: "isBackendSyncInProgress", value: "yes")
                                }
                            }else{
                               
                                DispatchQueue.main.async {
                                    self.goToTermsNConditions()
                                    self.view.hideToast()
                                }
                                 GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit security question answers", label: "Security question answers added successfully - Redirection to Dashboard page", value: 0)
                            }
                        } else {
                              DispatchQueue.main.async {
                                    self.view.makeToast( self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                              }
                              DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                                    self.view.hideToast()
                              })
                        }
                        DispatchQueue.main.async {
                            self.stopLoader()
                           self.submitButton.isUserInteractionEnabled=true
                        }
                  } else {
                        DispatchQueue.main.async {
                            self.stopLoader()
                            self.submitButton.isUserInteractionEnabled=true
                              self.view.makeToast( self.profilemsg.networkfailureMsg, duration: self.signupmsg.toastDelayTime, position: .bottom)
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+self.profilemsg.toastDelayTime, execute: {
                              self.view.hideToast()
                        })
                  }
            })
            task.resume()
            session.finishTasksAndInvalidate()

      }
    
    func loadBaseViewController(data: [String: Any]) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + self.profilemsg.toastDelayTime) {
            //            let introductaryVariableIndex = self.appSharedPrefernce.getAppSharedPreferences(key: "dashboardIntroducataryPages") as? String ?? "0"
            
            saveFisheyeUserObject(data: data)
            //            if (introductaryVariableIndex == "0") {
            //                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //                let nextVC = storyBoard.instantiateViewController(withIdentifier: "DashBoardPageViewController") as! DashBoardPageViewController
            //                self.present(nextVC, animated: true, completion: nil)
            //            } else {
            let baseVC  = BaseViewController.instantiateFromStoryboardWithIdentifier(identifier: "BaseViewController")
            kApplicationDelegate.nVC = UINavigationController(rootViewController: baseVC)
            kApplicationDelegate.nVC.navigationBar.isHidden  = true
            kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC
            //            }
            
        }
    }
    
    func startContactFrontEndSync() {
        let frontendSyncGlobalOperation = DispatchWorkItem {
            DatabaseManagement.shared.createRecentlyAddedTable()
            
            /* Check SQLite table is created or not? */
            let isSQliteTableExists: Bool = DatabaseManagement.shared.isSQLiteTableAreadyExists()
            if(!isSQliteTableExists) {                                                                                                                                                                               // SQLite table does't exists
                DatabaseManagement.shared.createOwenerContactsTable()                                                                                      // create table
                self.getFEUserContactsFromBackend()
            } else {                                                                                                                                                                                                                // SQLite table  exists
                let totalRecordsInTable: Int64 = DatabaseManagement.shared.checkIsOwnerContactsTableEmpty()
                if(totalRecordsInTable == 0) {                                                                                                                                                               // SQLite table is empty
                    self.getFEUserContactsFromBackend()
                } else {                                                                                                                                                                                                        // SQLite table is not empty
                    self.getBlockedByContactsFromBackend()
                    let isFetchFromBackendCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isFetchFromBackendCompleted") as? String ?? "yes"
                    let isfronendSyncFullyCompleted = self.appSharedPrefernce.getAppSharedPreferences(key: "isfronendSyncFullyCompleted") as? String ?? "yes"
                    if(isFetchFromBackendCompleted == "no") {
                        self.getFEUserContactsFromBackend()
                    } else if(isfronendSyncFullyCompleted == "no") {
                        ContactsSync.shared.frontEndSync()
                    } else {
                        if(NetworkStatus.sharedManager.isNetworkReachable()) {
                            ContactsSync.shared.getNonSyncedContacts()
                            if(!ContactsSync.shared.globalNonSyncedContcts.isEmpty ) {
                                ContactsSync.shared.backendSync()
                            }
                        }
                    }
                }
            }
        }
        DispatchQueue.global().async(execute: frontendSyncGlobalOperation)
    }

    
    func getFEUserContactsFromBackend() {
        self.appSharedPrefernce.setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "yes")
        let getContactsFromBackend = getLoggedInUserContactsOperation()
        getContactsFromBackend.addDidFinishBlockObserver { [unowned self] (operation, _) in
            DispatchQueue.main.async {
                ContactsSync.shared.isSyncInProgress = false
                let contactsCount = operation.contactsCount
                
                if(contactsCount > 0) {
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: false)
                    AppSharedPreference().setAppSharedPreferences(key: "isFrontendSyncInProgress", value: "no")
                } else {
                    takePermissionToSyncNFrontendSync(isToFrontEndSync: true)
                }
            }
        }
        AppDelegate.addProcedure(operation: getContactsFromBackend)
    }
    
    func getBlockedByContactsFromBackend() {
        let getBlockedByContacts = getBlockedByContactsAPIOperation()
        getBlockedByContacts.addDidFinishBlockObserver { [unowned self] (_, _) in
        }
        AppDelegate.addProcedure(operation: getBlockedByContacts)
    }

      func startLoader() {
            DispatchQueue.main.async {
                  ScreenLoader.shared.startLoader(view: UIApplication.shared.keyWindow!)
            }
      }

      func stopLoader() {
            DispatchQueue.main.async {
                  ScreenLoader.shared.stopLoader()
            }
      }

      @IBAction func backButtonTapped(_ sender: Any) {
        if self.isFromLogin{
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: DISMISS_UNNECESSARY_SIGNUP_PAGES, object: self)
            }
        }else{
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        }
      }

    @IBAction func skipBtnTapped(_ sender: Any) {
//        self.appSharedPrefernce.setAppSharedPreferences(key: "loginPreferenceUsed", value: self.signUpMethod)
//        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeId", value: self.fisheyeId)
//        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeName", value: self.fisheyeName)
//        self.appSharedPrefernce.setAppSharedPreferences(key: "gender", value: self.genderSelection)
//        self.appSharedPrefernce.setAppSharedPreferences(key: "fisheyeUserObject", value: "")
//        self.appSharedPrefernce.setAppSharedPreferences(key: "loginId", value: self.email)
        self.goToTermsNConditions()
        GoogleAnalytics.shared.trackEventOnGoogleAnalytics(category: "Sign-Up", action: "Submit security question answers", label: "Skipped to submit security question answers", value: 0)
    }
    
    override func viewDidLoad() {

            super.viewDidLoad()
            self.stopLoader()
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "backgroundFE.png")!)
            self.profilemsg = ProfileToastMsgHeadingSubheadingLabels.shared
            self.signupmsg = SignupToastMsgHeadingSubheadingLabels.shared
            self.securityquestionSubView.layer.cornerRadius = 6
            self.securityquestionSubView.layer.shadowColor = UIColor.black.cgColor
            self.securityquestionSubView.layer.shadowOffset = CGSize(width: 0.5, height: 0.4)
            self.securityquestionSubView.layer.shadowOpacity = 0.4
            self.securityquestionSubView.layer.shadowRadius = 6.0
            self.securityquestionSubView.layer.masksToBounds = false
            self.idtoken = self.appSharedPrefernce.getAppSharedPreferences(key: "idtoken") as? String ?? ""

            self.securityQuestion1.isUserInteractionEnabled = false
            self.securityQuestion1.isEnabled = false
            self.securityQuestion2.isUserInteractionEnabled = false
            self.securityQuestion2.isEnabled = false
            self.securityQuestion3.isUserInteractionEnabled = false
            self.securityQuestion3.isEnabled = false

            self.securityQuestion1.textColor = UIColor.questionDisableColor()
            self.securityQuestion2.textColor = UIColor.questionDisableColor()
            self.securityQuestion3.textColor = UIColor.questionDisableColor()

            self.securityQuestionAnswer1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.securityQuestionAnswer1.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.securityQuestionAnswer2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.securityQuestionAnswer2.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.securityQuestionAnswer3.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.securityQuestionAnswer3.addTarget(self, action: #selector(textFieldDidEnd(_:)), for: .editingDidEnd)

            self.securityQuestion1BlueUnderline.isHidden = true
            self.securityQuestion1BlackUnderline.isHidden = false

            self.securityQuestion2Blueunderline.isHidden = true
            self.securityQuestion2BlackUnderline.isHidden = false

            self.securityQuestion3Blueunderline.isHidden = true
            self.securityQuestion3BlackUnderline.isHidden = false
        
            if isCameAfterRegistration{
                self.skipBtn.isUserInteractionEnabled = false
                self.skipBtn.isHidden = true
                self.signUpLabel.isHidden = true
                self.signUpPagerImage.isHidden = true
            }
            self.setLocalizationText()
            self.addObserver()

      }

      func addObserver() {
            NotificationCenter.default.addObserver(self, selector: #selector(setLocalizationText), name: CHANGE_LANGUAGE, object: nil)
      }

      /*  Set all text labels according to language selected by FEUser */
    @objc func setLocalizationText() {
            DispatchQueue.main.async {
                  self.signUpLabel.text = self.signupmsg.signIn
                  self.securityQuestions.text = self.signupmsg.securityQuestions
                  self.securityQuestion1.text = self.signupmsg.securityQuestionOne
                  self.securityQuestion2.text = self.signupmsg.securityQuestionTwo
                  self.securityQuestion3.text = self.signupmsg.securityQuestionThree
                  self.fisheyeMotto.text = self.signupmsg.letsWorkTogether
                  self.securityQuestionAnswer1.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                  self.securityQuestionAnswer2.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                  self.securityQuestionAnswer3.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                      self.skipBtn.setTitle(self.signupmsg.skip, for: .normal)
            }
      }
      /* END */

      override func viewWillAppear(_ animated: Bool) {
            GoogleAnalytics.shared.trackScreenOnGoogleAnalytics(screenName: AppConfig.SignUpSecurityQuestionVC)
      }

      override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
      }

      //Code for checking validation while the user edits TextFields
    @objc func textFieldDidChange(_ textField: UICustomTextField) {

            switch textField {

            case securityQuestionAnswer1:
                  if (self.securityQuestionAnswer1.text?.characters.count) != 0 {
                        self.securityQuestionAnswer1.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                  } else {
                        self.securityQuestionAnswer1.configOnError(withPlaceHolder: self.signupmsg.questionone)
                  }
                  self.securityQuestion1BlueUnderline.isHidden = false
                  self.securityQuestion1BlackUnderline.isHidden = true

            case securityQuestionAnswer2:
                  if (self.securityQuestionAnswer2.text?.characters.count) != 0 {
                        self.securityQuestionAnswer2.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                  } else {
                        self.securityQuestionAnswer2.configOnError(withPlaceHolder: self.signupmsg.questionone)
                  }
                  self.securityQuestion2Blueunderline.isHidden = false
                  self.securityQuestion2BlackUnderline.isHidden = true

            case securityQuestionAnswer3:
                  if (self.securityQuestionAnswer3.text?.characters.count) != 0 {
                        self.securityQuestionAnswer3.configOnErrorCleared(withPlaceHolder: self.signupmsg.answerone)
                  } else {
                        self.securityQuestionAnswer3.configOnError(withPlaceHolder: self.signupmsg.questionone)
                  }
                  self.securityQuestion3Blueunderline.isHidden = false
                  self.securityQuestion3BlackUnderline.isHidden = true

            default:
                  break
            }
      }

      func popUpVCController() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "BasicPopUpViewController")
            self.present(nextVC, animated: true, completion: nil)
      }

      func goToTermsNConditions() {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsVC") as! TermsAndConditionsVC
            self.present(nextVC, animated: true, completion: nil)
      }

    @objc func textFieldDidEnd(_ textField: UICustomTextField) {
            switch textField {
            case securityQuestionAnswer1:
                  self.securityQuestion1BlueUnderline.isHidden = true
                  self.securityQuestion1BlackUnderline.isHidden = false

            case securityQuestionAnswer2:
                  self.securityQuestion2Blueunderline.isHidden = true
                  self.securityQuestion2BlackUnderline.isHidden = false

            case securityQuestionAnswer3:
                  self.securityQuestion3Blueunderline.isHidden = true
                  self.securityQuestion3BlackUnderline.isHidden = false

            default:
                  break
            }
      }
}

