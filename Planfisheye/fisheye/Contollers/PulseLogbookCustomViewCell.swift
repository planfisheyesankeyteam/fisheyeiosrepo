//
//  PulseLogbookCustomViewCell.swift
//  fisheye
//
//  Created by Anuradha on 28/11/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import MapKit

class PulseLogbookCustomViewCell: UITableViewCell {

    @IBOutlet weak var pulseDetailViewOnLogbook: UIView!
    @IBOutlet weak var pulseDetailsViewOnLogbookHeight: NSLayoutConstraint!

    @IBOutlet weak var pulseTitleView: UIView!
    @IBOutlet weak var pulseTitleLabel: UILabel!
    @IBOutlet weak var hisPulseImage: UIImageView!
    @IBOutlet weak var betweenEDandTime: UILabel!
    @IBOutlet weak var betweenSDandEDLabel: UILabel!
    @IBOutlet weak var pulseStartDate: UILabel!
    @IBOutlet weak var pulseShareOrRequestTime: UILabel!
    @IBOutlet weak var pulseEndDate: UILabel!

    @IBOutlet weak var acceptPulseButton: UIButton!
    @IBOutlet weak var declinePulseBtn: UIButton!

    @IBOutlet weak var locationNameView: UIView!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var locationNameViewHeight: NSLayoutConstraint!

    @IBOutlet weak var requestedCapsuleView: UIView!
    @IBOutlet weak var requestedCapsuleLabel: UILabel!
    @IBOutlet weak var requestedCapMasterKeyImageView: UIImageView!
    @IBOutlet weak var requestedCapsuleMasterKeyButton: UIButton!
    @IBOutlet weak var requestedCapsuleViewHeight: NSLayoutConstraint!

    @IBOutlet weak var mapViewInPulseLog: MKMapView!
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!

    @IBOutlet weak var sharedCapsuleView: UIView!
    @IBOutlet weak var sharedCapsuleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var sharedCapsuleLabel: UILabel!
    @IBOutlet weak var sharedCapMasterKeyImageView: UIImageView!
    @IBOutlet weak var sharedCapsuleMasterKeyButton: UIButton!

    @IBOutlet weak var SentToThemView: UIView!
    @IBOutlet weak var sentToThemViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewOfSentTo: UIStackView!

    @IBOutlet weak var firstParticipentView: UIView!
    @IBOutlet weak var firstParticipentImage: UIImageView!
    @IBOutlet weak var firstParticipentAcceptRejectStatus: UIImageView!
    @IBOutlet weak var firstParticipentName: UILabel!

    @IBOutlet weak var secondParticipentView: UIView!
    @IBOutlet weak var secondParticipantAcceptRejectStatus: UIImageView!
    @IBOutlet weak var secondparticipentImage: UIImageView!
    @IBOutlet weak var secondParticipentName: UILabel!

    @IBOutlet weak var thirdParticipentView: UIView!
    @IBOutlet weak var thirdParticipantImage: UIImageView!
    @IBOutlet weak var thirdParticientAcceptRejectStatus: UIImageView!
    @IBOutlet weak var thirdParticipentName: UILabel!

    @IBOutlet weak var fourthParticipentView: UIView!
    @IBOutlet weak var fourthParticipentImage: UIImageView!
    @IBOutlet weak var fourthParticipentAcceptRejectStatus: UIImageView!
    @IBOutlet weak var fourthParticipentName: UILabel!

    @IBOutlet weak var moreParticipentPresentView: UIView!
    @IBOutlet weak var moreParticientPresentImageView: UIImageView!

    @IBOutlet weak var acceptedRejectedStatusLabel: UILabel!
    @IBOutlet weak var zoomOutBtn: UIButton!
    @IBOutlet weak var zoomInBtn: UIButton!

    @IBOutlet weak var shareCapLabel: UILabel!
    @IBOutlet weak var requestCapLAbel: UILabel!
    var pulseType: String?
    var participentNumber: Int!
    var respondersArray = [RespondersData]()
    var isPulsed: Bool!
    var isScheduled: Bool!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)
    }

    var isExpanded: Bool = false {
        didSet {
            if !isExpanded {
                self.pulseDetailsViewOnLogbookHeight.constant=62
                self.mapViewHeight.constant = 0.0
                self.zoomInBtn.isHidden = true
                self.zoomOutBtn.isHidden = true
                self.sharedCapsuleViewHeight.constant=0.0
                self.locationNameViewHeight.constant=0.0
                self.requestedCapsuleViewHeight.constant=0.0
                self.sentToThemViewHeight.constant=0.0
                self.locationNameView.isHidden = true
                self.mapViewInPulseLog.isHidden=true
                self.requestedCapsuleView.isHidden = true
                self.sharedCapsuleView.isHidden=true
                self.SentToThemView.isHidden=true
            } else if(isExpanded==true && self.pulseType=="0") {//request
                self.pulseDetailsViewOnLogbookHeight.constant=142.5
                self.mapViewHeight.constant = 0
                self.zoomInBtn.isHidden = true
                self.zoomOutBtn.isHidden = true
                self.sharedCapsuleViewHeight.constant=0
                self.locationNameViewHeight.constant=0
                self.requestedCapsuleViewHeight.constant=35
                self.sentToThemViewHeight.constant=45
                self.locationNameView.isHidden = true
                self.mapViewInPulseLog.isHidden=true
                self.requestedCapsuleView.isHidden = false
                self.sharedCapsuleView.isHidden=true
                self.SentToThemView.isHidden=false
                self.participentAccordingly()
            } else if(isExpanded==true && self.pulseType=="1") {//share
                if(self.isScheduled) {
                    self.pulseDetailsViewOnLogbookHeight.constant=143.5
                    self.mapViewHeight.constant = 0
                    self.sharedCapsuleViewHeight.constant=36
                    self.locationNameViewHeight.constant=0
                    self.sentToThemViewHeight.constant=45
                    self.locationNameView.isHidden = true
                    self.mapViewInPulseLog.isHidden=true
                    self.zoomInBtn.isHidden = true
                    self.zoomOutBtn.isHidden = true
                    self.requestedCapsuleView.isHidden = true
                    self.sharedCapsuleView.isHidden=false
                    self.SentToThemView.isHidden=false
                    self.participentAccordingly()
                } else {
                    self.pulseDetailsViewOnLogbookHeight.constant=329.5
                    self.mapViewHeight.constant = 160
                    self.sharedCapsuleViewHeight.constant=36
                    self.locationNameViewHeight.constant=26
                    self.requestedCapsuleViewHeight.constant=0
                    self.sentToThemViewHeight.constant=45
                    self.locationNameView.isHidden = false
                    self.mapViewInPulseLog.isHidden=false
                    self.zoomInBtn.isHidden = false
                    self.zoomOutBtn.isHidden = false
                    self.requestedCapsuleView.isHidden = true
                    self.sharedCapsuleView.isHidden=false
                    self.SentToThemView.isHidden=false
                    self.participentAccordingly()
                }
            } else {
                self.pulseDetailsViewOnLogbookHeight.constant=364.5
                self.mapViewHeight.constant = 160
                self.sharedCapsuleViewHeight.constant=36
                self.locationNameViewHeight.constant=26
                self.requestedCapsuleViewHeight.constant=35
                self.sentToThemViewHeight.constant=45
                self.locationNameView.isHidden = false
                self.mapViewInPulseLog.isHidden=false
                self.zoomInBtn.isHidden = false
                self.zoomOutBtn.isHidden = false
                self.requestedCapsuleView.isHidden = false
                self.sharedCapsuleView.isHidden=false
                self.SentToThemView.isHidden=false
            }
        }
    }

    func noParticipentForPulse() {
        self.SentToThemView.isHidden=true
        self.sentToThemViewHeight.constant=0.0
        self.stackViewOfSentTo.isHidden=true
        self.firstParticipentView.isHidden=true
        self.secondParticipentView.isHidden=true
        self.thirdParticipentView.isHidden=true
        self.fourthParticipentView.isHidden=true
        self.moreParticipentPresentView.isHidden=true
        if(self.pulseType=="0") {
            self.pulseDetailsViewOnLogbookHeight.constant=107.5
        } else if(self.pulseType=="1") {
            self.pulseDetailsViewOnLogbookHeight.constant=284.5
        }
    }

    func showfirstParticipentInStack() {
        self.SentToThemView.isHidden=false
        self.stackViewOfSentTo.isHidden=false
        self.firstParticipentView.isHidden=false

        var contactImageUrl1=self.respondersArray[0].responderImage
        var contactName1=self.respondersArray[0].responderName
        var isAccepted1=self.respondersArray[0].isAccepted!
        var isDeclined1=self.respondersArray[0].isDeclined!
        var isResponded1=self.respondersArray[0].isResponded!

//        self.firstParticipentImage?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEscapes(using: String.Encoding.utf8) ?? ""), placeholderImage: #imageLiteral(resourceName: "inactivePersonalInfo"))
        
        self.firstParticipentImage?.sd_setImage(with: URL(string: contactImageUrl1?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage:#imageLiteral(resourceName: "inactivePersonalInfo"))
        
        
        self.firstParticipentName.text=contactName1

        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.firstParticipentImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.firstParticipentImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.firstParticipentAcceptRejectStatus.image=UIImage(named: "pluscopy2")!
        } else {
            self.firstParticipentImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.firstParticipentAcceptRejectStatus.isHidden=true
        }
    }

    func hideSecondparticiepntInStack() {
        self.secondParticipantAcceptRejectStatus.isHidden=true
        self.secondparticipentImage.isHidden=true
        self.secondParticipentName.isHidden=true
    }

    func showSecondparticiepntInStack() {
        self.secondParticipentView.isHidden=false

        var contactImageUrl2=self.respondersArray[1].responderImage
        var contactName2=self.respondersArray[1].responderName
        var isAccepted1=self.respondersArray[1].isAccepted!
        var isDeclined1=self.respondersArray[1].isDeclined!
        var isResponded1=self.respondersArray[1].isResponded!

        self.secondparticipentImage?.sd_setImage(with: URL(string: contactImageUrl2?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage:#imageLiteral(resourceName: "inactivePersonalInfo"))
        
        self.secondParticipentName.text=contactName2

        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.secondparticipentImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.secondparticipentImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.secondParticipantAcceptRejectStatus.image=UIImage(named: "pluscopy2")!
        } else {
            self.secondparticipentImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.secondParticipantAcceptRejectStatus.isHidden=true
        }
    }

    func hideThirdparticiepntInStack() {
        self.thirdParticientAcceptRejectStatus.isHidden=true
        self.thirdParticipantImage.isHidden=true
        self.thirdParticipentName.isHidden=true
    }

    func showThirdparticiepntInStack() {
        self.thirdParticipentView.isHidden=false

        var contactImageUrl3=self.respondersArray[2].responderImage
        var contactName3=self.respondersArray[2].responderName
        var isAccepted1=self.respondersArray[2].isAccepted!
        var isDeclined1=self.respondersArray[2].isDeclined!
        var isResponded1=self.respondersArray[2].isResponded!

        self.thirdParticipantImage?.sd_setImage(with: URL(string: contactImageUrl3?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage:#imageLiteral(resourceName: "inactivePersonalInfo"))
        
        self.thirdParticipentName.text=contactName3
        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.thirdParticipantImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.thirdParticipantImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.thirdParticientAcceptRejectStatus.image=UIImage(named: "pluscopy2")!
        } else {
            self.thirdParticipantImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.thirdParticientAcceptRejectStatus.isHidden=true
        }
    }

    func hideFourthparticiepntInStack() {
        self.fourthParticipentAcceptRejectStatus.isHidden=true
        self.fourthParticipentName.isHidden=true
        self.fourthParticipentImage.isHidden=true
    }

    func showFourthparticiepntInStack() {
        self.fourthParticipentView.isHidden=false

        var contactImageUrl4=self.respondersArray[3].responderImage
        var contactName4=self.respondersArray[3].responderName
        var isAccepted1=self.respondersArray[3].isAccepted!
        var isDeclined1=self.respondersArray[3].isDeclined!
        var isResponded1=self.respondersArray[3].isResponded!

        self.fourthParticipentImage?.sd_setImage(with: URL(string: contactImageUrl4?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""), placeholderImage:#imageLiteral(resourceName: "inactivePersonalInfo"))
        
        self.fourthParticipentName.text=contactName4

        if(isAccepted1 && !isDeclined1 && isResponded1) {
            self.fourthParticipentImage.layer.borderColor = UIColor.acceptGreen().cgColor
        } else if(!isAccepted1 && isDeclined1 && isResponded1) {
            self.fourthParticipentImage.layer.borderColor = UIColor.declinedRed().cgColor
            self.fourthParticipentAcceptRejectStatus.image=UIImage(named: "pluscopy2")!
        } else {
            self.fourthParticipentImage.layer.borderColor = UIColor.notRespondedGray().cgColor
            self.fourthParticipentAcceptRejectStatus.isHidden=true
        }
    }

    func hideFifthInStack() {
        self.moreParticientPresentImageView.isHidden=true
    }

    func showFifthInStack() {
        self.moreParticipentPresentView.isHidden=false
    }

    func participentAccordingly() {

        if(self.isPulsed) {
            switch (self.respondersArray.count) {
            case 0:
                self.noParticipentForPulse()
                break

            case 1:
                self.showfirstParticipentInStack()
                self.hideSecondparticiepntInStack()
                self.hideThirdparticiepntInStack()
                self.hideFourthparticiepntInStack()
                self.hideFifthInStack()
                break

            case 2:
                self.showfirstParticipentInStack()
                self.showSecondparticiepntInStack()
                self.hideThirdparticiepntInStack()
                self.hideFourthparticiepntInStack()
                self.hideFifthInStack()
                break

            case 3:
                self.showfirstParticipentInStack()
                self.showSecondparticiepntInStack()
                self.showThirdparticiepntInStack()
                self.hideFourthparticiepntInStack()
                self.hideFifthInStack()
                break

            case 4:
                self.showfirstParticipentInStack()
                self.showSecondparticiepntInStack()
                self.showThirdparticiepntInStack()
                self.showFourthparticiepntInStack()
                self.hideFifthInStack()
                break

            default:
                self.showfirstParticipentInStack()
                self.showSecondparticiepntInStack()
                self.showThirdparticiepntInStack()
                self.showFourthparticiepntInStack()
                self.showFifthInStack()
                break
            }
        } else {
            self.SentToThemView.isHidden=true
            self.sentToThemViewHeight.constant=0.0
            if(self.pulseType=="0") {
                self.pulseDetailsViewOnLogbookHeight.constant=97.0
            } else if(self.pulseType=="1") {
                if(self.isScheduled) {
                    self.pulseDetailsViewOnLogbookHeight.constant=107.5
                } else {
                    self.pulseDetailsViewOnLogbookHeight.constant=284.5
                }
            }
        }
    }

/* ----------------------------------- Zoom In & Zoom Out Map View ------------------------- */
      @IBAction func zoomOutButtonTapped(_ sender: Any) {
            var region: MKCoordinateRegion? = mapViewInPulseLog?.region
            let latitude = min((region?.span.latitudeDelta)! * 2.0, 180.0)
            let longitude = min((region?.span.longitudeDelta)! * 2.0, 180.0)
            region?.span.latitudeDelta = latitude
            region?.span.longitudeDelta = longitude
            mapViewInPulseLog?.setRegion(region!, animated: true)
      }

      @IBAction func zoomInButtonTapped(_ sender: Any) {
            var region: MKCoordinateRegion? = mapViewInPulseLog?.region
            region?.span.latitudeDelta /= (2.0 as  CLLocationDegrees)
            region?.span.longitudeDelta /= (2.0 as  CLLocationDegrees)
            mapViewInPulseLog?.setRegion(region!, animated: true)
      }
}
