//
//  AppDelegate.swift
//  fisheye
//
//  Created by Keerthi on 12/09/17.
//  Copyright © 2017 Keerthi. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import AWSAuthCore
import AWSUserPoolsSignIn
import AWSGoogleSignIn
import AWSPinpoint
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import CoreLocation
import FacebookCore
import Fabric
import Crashlytics
import LoginWithAmazon
import ProcedureKit
import Contacts
import GoogleSignIn

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {

    // Receive displayed notifications for iOS 10 devices.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([UNNotificationPresentationOptions.alert, UNNotificationPresentationOptions.sound, UNNotificationPresentationOptions.badge])

    }


    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response:   UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        userInfo = response.notification.request.content.userInfo as NSDictionary
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyNotificationReceived"), object: nil, userInfo: userInfo as? [AnyHashable: Any])

        if let notificationInfo = userInfo as? [String: Any] {
            let id = notificationInfo["id"] as? String ?? ""
            let type = notificationInfo["type"] as? String ?? ""
            let isPulsed = notificationInfo["isPulsed"] as? Bool ?? false
            let isCapsuleLockedstatus = notificationInfo["isCapsuleLocked"] as? String ?? ""

            let baseVC  = BaseViewController.instantiateFromStoryboardWithIdentifier(identifier: "BaseViewController")
            if type == "MEETME"{
                baseVC.isFromMeetMeNotification = true
                baseVC.notificationMeetMeId = id
            }
            
            if type == "PULSE"{
                baseVC.isFromPulseNotification = true
                baseVC.notificationPulseId = id
                baseVC.isPulsed = isPulsed
            }
            
            if type == "CAPSULE"{
                baseVC.isFromCapsuleChatNotification = true

                baseVC.notificationMeetMeId = id
                if isCapsuleLockedstatus == "true"{
                    baseVC.isCapsuleLocked = true
                }else{
                    baseVC.isCapsuleLocked = false
                }

                if let aps = notificationInfo["aps"] as? [String: Any]{
                    if let alert = aps["alert"] as? [String: Any]{
                        let title = alert["title"] as? String ?? ""
                        baseVC.meetTitle = title
                    }
                }
            }

            kApplicationDelegate.nVC = UINavigationController(rootViewController: baseVC)
            kApplicationDelegate.nVC.navigationBar.isHidden  = true
            kApplicationDelegate.window?.rootViewController = kApplicationDelegate.nVC
        }

        completionHandler()
    }
}




@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, CLLocationManagerDelegate {

    let gcmMessageIDKey = "gcm.message_id"
    var window: UIWindow?
    var pinpoint: AWSPinpoint?
    var nVC: UINavigationController!
    var locationManager: CLLocationManager?
//    var significatLocationManager: CLLocationManager?
   // var currentLocationTest: CLLocation!
    var userInfo: NSDictionary?
    var operationQueue: ProcedureQueue
    var sharedPreference = AppSharedPreference()
    let validationController = ValidationController()
//    var locationName: String?
//    var latitude: Double?
//    var longitude: Double?
    var contactStore = CNContactStore()
    private var reachability: Reachability!

    override init() {
        operationQueue = ProcedureQueue()
        super.init()
    }

    // Operation Queue function
    public class func addProcedure(operation: Operation) {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.operationQueue.addOperation(operation)
        }
    }

    var seenError: Bool = false
    var locationFixAchieved: Bool = false
//    var locationStatus: NSString = "Not Started"
    //Used for checking whether Push Notification is enabled in Amazon Pinpoint
    static let remoteNotificationKey = "RemoteNotification"
    var isInitialized: Bool = false
    let kUserDefault = UserDefaults.standard

    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {

        CommonMethods.saveFCMID(fcmToken)
    }

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        print("Hello1")
        //        let localize = Localize.shared
        //        localize.update(provider: .json)
        //        localize.update(fileName: "lang")
        //        localize.update(language: "en")
        //        localize.resetLanguage()
        
        GIDSignIn.sharedInstance().clientID = "361866842537-2e6pjfmvlqe9973419c02mdqsm1rc910.apps.googleusercontent.com"
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return true
        }
        gai.tracker(withTrackingId: "UA-115365244-1")
        gai.trackUncaughtExceptions = true
        
        Fabric.with([Crashlytics.self])
        self.logUser()
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(20)
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        
        let aPushNotification = launchOptions?[.remoteNotification] as? [AnyHashable: Any]
        if (aPushNotification != nil && !(aPushNotification?.isEmpty)!){
            CommonMethods.saveUserInfo(aPushNotification! as NSDictionary)
            
        }
        self.configureApp()
        
        FirebaseApp.configure()
        var configureError: NSError?
        
        
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            NotificationCenter.default.addObserver(self, selector: #selector(self.refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        }
        
        application.registerForRemoteNotifications()
        
        //************code for s3**************//
        let credentialsProviderS3 = AWSStaticCredentialsProvider(accessKey: "AKIAIM73RR6SXIK6YS6A", secretKey: "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")
        let configurationS3 = AWSServiceConfiguration(region: .EUWest2, credentialsProvider: credentialsProviderS3)
        AWSServiceManager.default().defaultServiceConfiguration = configurationS3
        
        //----***********Facebook code by rushikesh**********---//
        
        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        IQKeyboardManager.sharedManager().enable = true
        
        ConnectionManager.sharedInstance.observeReachability()
        
        return true
        
    }

//    private func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//print("Hello2")
////        let localize = Localize.shared
////        localize.update(provider: .json)
////        localize.update(fileName: "lang")
////        localize.update(language: "en")
////        localize.resetLanguage()
//
//        GIDSignIn.sharedInstance().clientID = "361866842537-2e6pjfmvlqe9973419c02mdqsm1rc910.apps.googleusercontent.com"
//        guard let gai = GAI.sharedInstance() else {
//            assert(false, "Google Analytics not configured correctly")
//            return true
//        }
//        gai.tracker(withTrackingId: "UA-115365244-1")
//        gai.trackUncaughtExceptions = true
//
//        Fabric.with([Crashlytics.self])
//        self.logUser()
//
//        UIApplication.shared.setMinimumBackgroundFetchInterval(20)
//        UIApplication.shared.applicationIconBadgeNumber = 0
//
//        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
//
//        let aPushNotification = launchOptions?[.remoteNotification] as? [AnyHashable: Any]
//        if (aPushNotification != nil && !(aPushNotification?.isEmpty)!){
//            CommonMethods.saveUserInfo(aPushNotification! as NSDictionary)
//
//        }
//        self.configureApp()
//
//        FirebaseApp.configure()
//        var configureError: NSError?
//
//
//
//        Messaging.messaging().delegate = self
//
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            NotificationCenter.default.addObserver(self, selector: #selector(self.refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
//        }
//
//        application.registerForRemoteNotifications()
//
//        //************code for s3**************//
//        let credentialsProviderS3 = AWSStaticCredentialsProvider(accessKey: "AKIAIM73RR6SXIK6YS6A", secretKey: "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")
//        let configurationS3 = AWSServiceConfiguration(region: .EUWest2, credentialsProvider: credentialsProviderS3)
//        AWSServiceManager.default().defaultServiceConfiguration = configurationS3
//
//        //----***********Facebook code by rushikesh**********---//
//
//        SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
//
//        IQKeyboardManager.sharedManager().enable = true
//
//        ConnectionManager.sharedInstance.observeReachability()
//
//        return true
//    }

    func logUser() {
        Crashlytics.sharedInstance().setUserEmail("ext1dev@planfisheye.com")
        Crashlytics.sharedInstance().setUserName("Crash Tester")
        Crashlytics.setUserName("CfabricTester")
    }
    

//    private func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        let amazonId = AIMobileLib.handleOpen(url, sourceApplication: UIApplication.OpenURLOptionsKey.sourceApplication.rawValue)
        let facebookApp = SDKApplicationDelegate.shared.application(app, open: url, options: options)
        let awsGoogleApp = GIDSignIn.sharedInstance().handle(url as URL?,
                                                             sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                             annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        return facebookApp || awsGoogleApp  || amazonId
    }
    

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        isInitialized = true
        return true
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    

    func applicationDidEnterBackground(_ application: UIApplication) {
            Messaging.messaging().shouldEstablishDirectChannel = false
            self.locationManager?.startMonitoringSignificantLocationChanges()
    }
    


      internal func application(_ application: UIApplication,
                                willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
            return true
      }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        // Clear the badge icon when you open the app.
        //UIApplication.shared.applicationIconBadgeNumber = 0
    }

    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: AppDelegate.remoteNotificationKey), object: deviceToken)
//        if let refreshedToken = InstanceID.instanceID().token(withAuthorizedEntity: <#String#>, scope: <#String#>, handler: <#InstanceIDTokenHandler#>) {
//            self.sharedPreference.setAppSharedPreferences(key: "fcmToken", value: refreshedToken)
//            saveFcmToken(fcmToken: refreshedToken)
//        }
        
        let refreshedToken = InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                self.sharedPreference.setAppSharedPreferences(key: "fcmToken", value: result.token)
                self.saveFcmToken(fcmToken: result.token)
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        let token1 = Messaging.messaging().fcmToken
        var token = ""
        Messaging.messaging().apnsToken = deviceToken
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        if(token1 != nil) {
            CommonMethods.saveFCMID(token1!)
        }
    }
    

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if (application.applicationState == .active) {
            let alert = UIAlertController(title: "Notification Received",
                                          message: userInfo.description,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }

   
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "fisheye")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

  
    func configureApp() {
        if userDefaults.value(forKey: kSyncedContacts) == nil {
            userDefaults.set(false, forKey: kSyncedContacts)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(addressBookDidChange), name: Notification.Name.CNContactStoreDidChange, object: nil)
        self.setStatusBarBackgroundColor(color: UIColor.white)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().isTranslucent = false
        UIApplication.shared.statusBarStyle = .default
    }
    

    @objc func refreshToken(notification: NSNotification) {
        //let refreshToken = InstanceID.instanceID().token(withAuthorizedEntity: <#String#>, scope: <#String#>, handler: <#InstanceIDTokenHandler#>)!
        
        let refreshToken = InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        fireBaseHandler()
    }
    
    
    func  fireBaseHandler() {
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    

    @objc func addressBookDidChange(notification: NSNotification) {
        //Handle event here...
        notification.userInfo
    }
    

    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = UIColor.duckEggBlue()
    }
    


    func saveFcmToken( fcmToken: String) {
        let saveFcmTokenApiCall = SaveFCMToken(fcmTokenRecieved: fcmToken)
        saveFcmTokenApiCall.addDidFinishBlockObserver { [unowned self] (operation, _) in
            let responseStatus = operation.responseStatus
            if responseStatus == "200"{
            }
        }
        AppDelegate.addProcedure(operation: saveFcmTokenApiCall)
    }


}


