//
//  Constants.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

let kScreenSize: CGRect = UIScreen.main.bounds

let userDefaults = UserDefaults.standard

let kApplicationDelegate = UIApplication.shared.delegate! as! AppDelegate

//let kApplicationDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

let kSweetAlert = SweetAlert.sharedSweetAlert

let kNetworkNotReachableCode = 1000

let kCollectionViewCell = "collectionViewCell"

let kSyncedContacts = "syncedContacts"

/*
 
 TA Color Palette:
 
 <color name="primary">#009688</color>
 <color name="primary_dark">#00796B</color>
 <color name="primary_light">#B2DFDB</color>
 <color name="accent">#00BCD4</color>
 <color name="primary_text">#212121</color>
 <color name="secondary_text">#727272</color>
 <color name="icons">#FFFFFF</color>
 <color name="divider">#B6B6B6</color>
 <color name="tealAndWhite">#93E2D5</color>
 <color name="error_view_text">#656565</color>
 
 TAG Color Palatte:
 
 <color name="primary">#0388D1</color>
 <color name="primary_dark">#0064AD</color>
 <color name="primary_light">#23A8F1</color>
 <color name="accent">#00BCD4</color>
 <color name="primary_text">#212121</color>
 <color name="secondary_text">#727272</color>
 <color name="icons">#FFFFFF</color>
 <color name="divider">#B6B6B6</color>
 <color name="tealAndWhite">#93E2D5</color>
 <color name="error_view_text">#656565</color>
 

 
 
 <!--ITI test URL's-->
 <string name="ITItest_application_url">https://iti1.transactionanalysts.com:444/ebatwatest/Process.aspx</string>
 <string name="ITItest_disaster_recovery_application_url">https://iti1.transactionanalysts.com:444/ebatwatest/Process.aspx</string>
 <string name="ITItest_application_terminal_Id">302</string>
 <string name="ITItest_access_code">AVJZ00DG45AH42ZJHA</string>
 <string name="ITItest_disaster_recovery_ccavenue_url">https://iti1.transactionanalysts.com:444/ebatwatest/CCAvenue_Response.aspx</string>
 <string name="ITItest_application_prod_ccavenue_url">https://iti1.transactionanalysts.com:444/ebatwatest/CCAvenue_Response.aspx</string>
 <string name="ITItest_application_terminal_name">DOP</string>
 <string name="ITItest_CCAvenueRequestURL">https://iti1.transactionanalysts.com:444/ebatwatest/Process.aspx</string>
 <string name="ITItest_ccavenue_trans_url">https://test.ccavenue.com/transaction/initTrans</string>
 
 <!--ITI production URL's-->
 <string name="ITIprod_application_url">https://iti.transactionanalysts.com:444/ebatwa/Process.aspx</string>
 <string name="ITIprod_disaster_recovery_application_url">https://iti.transactionanalysts.com:444/ebatwa/Process.aspx</string>
 <string name="ITIprod_application_terminal_Id">302</string>
 <string name="ITIprod_access_code"><!--AVEM65DF92CL26MELC-->AVKF63DA58BD54FKDB</string>
 <string name="ITIprod_disaster_recovery_ccavenue_url">https://iti1.transactionanalysts.com:444/ebatwa/CCAvenue_Response.aspx</string>
 <string name="ITIprod_application_prod_ccavenue_url">https://iti1.transactionanalysts.com:444/ebatwa/CCAvenue_Response.aspx</string>
 <string name="ITIprod_application_terminal_name">DP</string>
 <string name="ITIprod_CCAvenueRequestURL">https://iti1.transactionanalysts.com:444/ebatwa/Process.aspx</string>
 <string name="ITIprod_ccavenue_trans_url">https://secure.ccavenue.com/transaction/initTrans</string>
 
 
 
 <!--TWallet Test URL's-->
 <string name="Ttest_application_url">https://pptawallet.transactionanalysts.com/Process.aspx</string>
 <string name="Ttest_disaster_recovery_application_url">https://pptawallet.transactionanalysts.com/Process.aspx</string>
 <string name="Ttest_application_terminal_Id">81</string>
 <string name="Ttest_access_code">AVKF63DA58BD54FKDB</string>
 <string name="Ttest_disaster_recovery_ccavenue_url">https://pptawallet.transactionanalysts.com/CCAvenue_Response.aspx</string>
 <string name="Ttest_application_prod_ccavenue_url">https://pptawallet.transactionanalysts.com/CCAvenue_Response.aspx</string>
 <string name="Ttest_application_terminal_name">TW</string>
 <string name="Ttest_ccavenue_trans_url">https://test.ccavenue.com/transaction/initTrans</string>
 
 <!--TWallet Production URL's-->
 <string name="Tprod_application_url">https://service.transactionanalysts.com:459/Process.aspx</string>
 <string name="Tprod_disaster_recovery_application_url">https://service.transactionanalysts.com:459/Process.aspx</string>
 <string name="Tprod_application_terminal_Id">77</string>
 <string name="Tprod_access_code">AVKF63DA58BD54FKDB</string>
 <string name="Tprod_disaster_recovery_ccavenue_url">https://service.transactionanalysts.com:459/CCAvenue_Response.aspx</string>
 <string name="Tprod_application_prod_ccavenue_url">https://service.transactionanalysts.com:459/CCAvenue_Response.aspx</string>
 <string name="Tprod_application_terminal_name">TA</string>
 <string name="Tprod_ccavenue_trans_url">https://secure.ccavenue.com/transaction/initTrans</string>

 
 !--TA Test URL's-->
 <string name="TAtest_application_url">https://pptawallet.transactionanalysts.com/Process.aspx</string>
 <string name="TAtest_disaster_recovery_application_url">https://pptawallet.transactionanalysts.com/Process.aspx</string>
 <string name="TAtest_application_terminal_Id">65</string>
 <string name="TAtest_access_code">AVKF63DA58BD54FKDB</string>
 <string name="TAtest_disaster_recovery_ccavenue_url">https://pptawallet.transactionanalysts.com/CCAvenue_Response.aspx</string>
 <string name="TAtest_application_prod_ccavenue_url">https://pptawallet.transactionanalysts.com/CCAvenue_Response.aspx</string>
 <string name="TAtest_application_terminal_name">TA</string>
 <string name="TAtest_ccavenue_trans_url">https://test.ccavenue.com/transaction/initTrans</string>
 
 <!--TA Production URL's-->
 <string name="TAprod_application_url">https://service.transactionanalysts.com:459/Process.aspx</string>
 <string name="TAprod_disaster_recovery_application_url">https://service.transactionanalysts.com:459/Process.aspx</string>
 <string name="TAprod_application_terminal_Id">77</string>
 <string name="TAprod_access_code">AVKF63DA58BD54FKDB</string>
 <string name="TAprod_disaster_recovery_ccavenue_url">https://service.transactionanalysts.com:459/CCAvenue_Response.aspx</string>
 <string name="TAprod_application_prod_ccavenue_url">https://service.transactionanalysts.com:459/CCAvenue_Response.aspx</string>
 <string name="TAprod_application_terminal_name">TA</string>
 <string name="TAprod_ccavenue_trans_url">https://secure.ccavenue.com/transaction/initTrans</string>
 
 
 
 */

enum PointingTo: Int {
    case QA = 0, Production
}

/*class AppConfiguration:NSObject {
    
    var baseUrl : String!
    var terminalID: String!
    var accessCode: String!
    var ccavenue_cancel_url : String!
    var ccavenue_recovery_url : String!
    var ccavenue_trans_url : String!
    var terminal_name : String!
    var merchantId : String!
    var aadhaarRegUrl : String!
    var mobRegTermsCondition : String!
    
    var primary: String?
    var primary_dark: String?
    var primary_light : String?
    var accent: String?
    var primary_text: String?
    var secondary_text :  String?
    var icons: String?
    var divider: String?
    var tealAndWhite: String?
    var error_view_text: String?
    
    
    init(pointingTo: PointingTo,forApp:AppIdentifier) {
        switch forApp {
        case .TA:
            switch pointingTo {
            case .QA:
                terminalID = "65"
                accessCode = "AVZY00EB87AR75YZRA"
                merchantId = "87969"
                baseUrl = "https://pptawallet.transactionanalysts.com/Process.aspx"
                ccavenue_cancel_url = "https://pptawallet.transactionanalysts.com/CCAvenue_Response.aspx"
                ccavenue_recovery_url = "https://pptawallet.transactionanalysts.com/CCAvenue_Response.aspx"
                ccavenue_trans_url = "https://test.ccavenue.com/transaction/initTrans"
                terminal_name = "TA"
                aadhaarRegUrl = "https://pptarecon.azurewebsites.net/aadhaar/AadhaarAuthentication.aspx"
                mobRegTermsCondition = "http://transactionanalysts.com/customer-grievance-policy/"
                
            case .Production:
                terminalID = "77"
                accessCode = "AVKF63DA58BD54FKDB"
                merchantId = "87969"
                baseUrl = "https://service.transactionanalysts.com:459/Process.aspx"
                ccavenue_cancel_url = "https://service.transactionanalysts.com:459/CCAvenue_Response.aspx"
                ccavenue_recovery_url = "https://service.transactionanalysts.com:459/CCAvenue_Response.aspx"
                ccavenue_trans_url = "https://secure.ccavenue.com/transaction/initTrans"
                terminal_name = "TA"
                
            }
            primary = "#009688"
            primary_dark = "#00796B"
            primary_light = "#B2DFDB"
            accent = "#00BCD4"
            primary_text = "#212121"
            secondary_text = "#727272"
            icons = "#FFFFFF"
            divider = "#B6B6B6"
            tealAndWhite = "#93E2D5"
            error_view_text = "#656565"
            
            
        case .TAG:
            switch pointingTo {
            case .QA:
                terminalID = "#009688"
                accessCode = "#00796B"
                baseUrl = "#B2DFDB"
                
            case .Production:
                terminalID = "#0388D1"
                accessCode = "#0064AD"
                baseUrl = "#23A8F1"
                
            }
            primary = "#0388D1"
            primary_dark = "#0064AD"
            primary_light = "#23A8F1"
            accent = "#00BCD4"
            primary_text = "#212121"
            secondary_text = "#727272"
            icons = "#FFFFFF"
            divider = "#B6B6B6"
            tealAndWhite = "#93E2D5"
            error_view_text = "#656565"
        default:
            break
        }
        
    }
    
}*/
