/*
 Copyright © 2014, Audvisor Inc.
 Written under contract by Robosoft Technologies Pvt. Ltd.
 */

#import <UIKit/UIKit.h>

/*!
 @class
 @abstract      ProgressView class is show the activity indicator over semi transparent view
 @discussion    This class has two method two methods 'showProgressView' which is used to show the activity indicator 
                and 'hideProgressView' which is used to remove the activity indicator. on calling 'showProgressView' 
                ProgressView is added to the window so as to cover the completer UI.
 */
@interface ProgressView : UIView

/*!
 @method
 @abstract      This method is used to return the shared instance of progress view.
 @return        Returns the shared progressview.
 */
+ (ProgressView *)sharedProgressView;

/*!
 @method
 @abstract      This method is used to add the ProgressView to the window.
 */
- (void)showProgressView;

/*!
 @method
 @abstract      This method is used to remove the ProgressView from the window.
 */
- (void)hideProgressView;

@end
