//
//  NSTimeIntervalExtension.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

extension TimeInterval {
    var minuteSecond: String {
        if hour > 0 {
            return String(format: "%d:%d:%02d", hour, minute, second)
        } else {
            return String(format: "%d:%02d", minute, second)
        }
    }

    var minuteSecondWithText: String {
        return String(format: "%d m %02d sec", minute, second)
    }

    var hour: Int {
        return Int((self/3600.0).truncatingRemainder(dividingBy: 60))
    }
    var minute: Int {
        return Int((self/60.0).truncatingRemainder(dividingBy: 60))
    }
    var second: Int {
        return Int(self.truncatingRemainder(dividingBy: 60))
    }
    var millisecond: Int {
        return Int(self*1000.truncatingRemainder(dividingBy: 60) )
    }
}
