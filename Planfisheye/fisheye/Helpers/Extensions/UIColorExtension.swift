//
//  UIColorExtension.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

extension UIColor {
    class func hexStringToUIColor(hex: String?) -> UIColor {
        if let hexStr = hex {

            var cString: String = hexStr.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

            if (cString.hasPrefix("#")) {
                cString.remove(at: cString.startIndex)
            }

            if ((cString.characters.count) != 6) {
                return UIColor.gray
            }

            var rgbValue: UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)

            return UIColor(
                red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                alpha: CGFloat(1.0)
            )
        }
        return UIColor.black
    }
//    func hexStringToUIColor (hex:String) -> UIColor {
//        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
//
//        if (cString.hasPrefix("#")) {
//            cString.remove(at: cString.startIndex)
//        }
//
//        if ((cString.characters.count) != 6) {
//            return UIColor.gray
//        }
//
//        var rgbValue:UInt32 = 0
//        Scanner(string: cString).scanHexInt32(&rgbValue)
//
//        return UIColor(
//            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
//            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
//            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
//            alpha: CGFloat(1.0)
//        )
//    }
    class func rgbComponent(red redColor: CGFloat, green greenColor: CGFloat, blue blueColor: CGFloat, alpha: CGFloat) -> UIColor {

        return UIColor(red: redColor/255.0, green: greenColor/255.0, blue: blueColor/255.0, alpha: alpha)

    }

    class func duckEggBlue() -> UIColor {

        return rgbComponent(red: 221, green: 211, blue: 230, alpha: 1.0)

    }

    class func acceptedGreen() -> UIColor {

        return rgbComponent(red: 153, green: 224, blue: 156, alpha: 1.0)

    }

    class func declinedRed() -> UIColor {

        return rgbComponent(red: 255, green: 135, blue: 135, alpha: 1.0)

    }

    class func notRespondedGray() -> UIColor {

        return rgbComponent(red: 199, green: 204, blue: 225, alpha: 1.0)

    }

    class func purpleRing() -> UIColor {

        return rgbComponent(red: 86, green: 37, blue: 131, alpha: 0.9)

    }

    class func capsuleTextViewPlaceHolderColor() -> UIColor {

        return rgbComponent(red: 178, green: 178, blue: 178, alpha: 1.0)

    }
    class func selectedTextColor() -> UIColor {

        return rgbComponent(red: 86.0, green: 37.0, blue: 131.0, alpha: 1.0)

    }

    class func unSelectedTextColor() -> UIColor {

        return rgbComponent(red: 11.0, green: 11.0, blue: 11.0, alpha: 0.48)

    }

    class func contactsTypeBg() -> UIColor {
        return rgbComponent(red: 221, green: 211, blue: 230, alpha: 1)
    }

    class func capsuleBlue() -> UIColor {
        return rgbComponent(red: 232, green: 232, blue: 244, alpha: 1)
    }

    class func frenchBlue() -> UIColor {

        return rgbComponent(red: 63, green: 90, blue: 189, alpha: 1.0)
    }

    class func frenchBlue54() -> UIColor {

        return rgbComponent(red: 63, green: 90, blue: 189, alpha: 0.54)
    }

    class func frenchBlue70() -> UIColor {

        return rgbComponent(red: 86, green: 37, blue: 131, alpha: 0.5)
    }
    class func frenchBlue80() -> UIColor {

        return rgbComponent(red: 63, green: 90, blue: 189, alpha: 0.8)
    }

    class func purplyBlue10() -> UIColor {

        return rgbComponent(red: 108, green: 37, blue: 241, alpha: 0.1)
    }
    class func purplyBlue87() -> UIColor {

        return rgbComponent(red: 108, green: 37, blue: 241, alpha: 0.87)
    }

    class func lavender() -> UIColor {

        return rgbComponent(red: 171, green: 175, blue: 241, alpha: 1.0)
    }

    class func lavender54() -> UIColor {

        return rgbComponent(red: 171, green: 175, blue: 241, alpha: 0.54)
    }

    class func black60() -> UIColor {

        return rgbComponent(red: 10, green: 10, blue: 10, alpha: 0.6)
    }

    class func black27() -> UIColor {

        return rgbComponent(red: 0, green: 0, blue: 0, alpha: 0.27)
    }

    class func red50() -> UIColor {

        return rgbComponent(red: 255, green: 0, blue: 0, alpha: 0.5)
    }

    class func black60Two() -> UIColor {

        return rgbComponent(red: 11, green: 11, blue: 11, alpha: 0.6)
    }

    class func black60Two87() -> UIColor {

        return rgbComponent(red: 11, green: 11, blue: 11, alpha: 0.87)
    }

    class func black60Two54() -> UIColor {

        return rgbComponent(red: 11, green: 11, blue: 11, alpha: 0.54)
    }

    class func frenchBlue87() -> UIColor {

         return rgbComponent(red: 60, green: 90, blue: 189, alpha: 0.87)
    }

    class func darkIndigo() -> UIColor {

        return rgbComponent(red: 13, green: 31, blue: 100, alpha: 0.87)
    }

    class func drkGrey() -> UIColor {

        return rgbComponent(red: 169, green: 169, blue: 169, alpha: 1)
    }

    class func purpleDots() -> UIColor {

        return rgbComponent(red: 130, green: 122, blue: 239, alpha: 1)
    }

    class func whiteDots() -> UIColor {

        return rgbComponent(red: 255, green: 255, blue: 255, alpha: 0.17)
    }

    class func lightishPurple() -> UIColor {

        return rgbComponent(red: 168, green: 50, blue: 227, alpha: 1)
    }

    class func burple() -> UIColor {

        return rgbComponent(red: 108, green: 50, blue: 227, alpha: 1)
    }

    class func purpleyBlue() -> UIColor {

        return rgbComponent(red: 80, green: 50, blue: 227, alpha: 1)
    }

    class func purplyBlue() -> UIColor {

        return rgbComponent(red: 108, green: 37, blue: 241, alpha: 1)
    }

    class func blueBlue40() -> UIColor {

        return rgbComponent(red: 51, green: 60, blue: 212, alpha: 0.4)
    }

    class func blueBlue54() -> UIColor {

        return rgbComponent(red: 51, green: 60, blue: 212, alpha: 0.54)
    }

    class func lightPurple() -> UIColor {
        return rgbComponent(red: 199, green: 204, blue: 225, alpha: 1)
    }
    class func acceptGreen() -> UIColor {
        return rgbComponent(red: 153, green: 224, blue: 156, alpha: 1)
    }

    class func rejectRed() -> UIColor {
        return rgbComponent(red: 255, green: 135, blue: 135, alpha: 1)
    }

    class func lightGrey() -> UIColor {
        return rgbComponent(red: 247, green: 247, blue: 247, alpha: 1)
    }
    class func lightUrple() -> UIColor {

        return rgbComponent(red: 163, green: 121, blue: 246, alpha: 1)
    }

    class func lightURple() -> UIColor {

        return rgbComponent(red: 162, green: 121, blue: 246, alpha: 1)
    }

    class func questionDisableColor() -> UIColor {

        return rgbComponent(red: 11, green: 11, blue: 11, alpha: 0.7)
    }

    class func questionEnableColor() -> UIColor {

        return rgbComponent(red: 1, green: 1, blue: 1, alpha: 0.7)
    }

    class func profileBottomStack() -> UIColor {

        return rgbComponent(red: 77, green: 68, blue: 113, alpha: 1)
    }

    class func rebrandedPurple() -> UIColor {

        return hexStringToUIColor(hex: "#562583")

    }
    class func rebrandedLightPurple() -> UIColor {

        return rgbComponent(red: 86, green: 37, blue: 131, alpha: 0.5)

    }
    class func rebrandedTextLightPurple() -> UIColor {

        return hexStringToUIColor(hex: "#DDD3E6")

    }

    class func newBlue() -> UIColor {

        return hexStringToUIColor(hex: "#F5E6FF")

    }

    /*
    class func primary_dark() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.primary_dark)
    }
    
    class func primary_light() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.primary_light)
    }
    
    class func accent() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.accent)
    }
    
    class func primary_text() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.primary_text)
    }
    
    class func secondary_text() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.secondary_text)
    }
    class func icons() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.icons)
    }
    
    class func divider() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.divider)
    }
    
    class func tealAndWhite() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.tealAndWhite)
    }
    
    class func error_view_text() -> UIColor {
        return hexStringToUIColor(hex: kApplicationDelegate.appConfig.error_view_text)
    }
    
    class func otherBtnColor() -> UIColor{
        return UIColor.accent()
    }
    */

}


