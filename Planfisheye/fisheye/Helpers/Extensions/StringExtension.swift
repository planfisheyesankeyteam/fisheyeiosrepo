//
//  StringExtension.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import Foundation

extension String {
    // MARK: - Method: Validate Email Address

    func isValidEmail() -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

    func isValidPhoneNumber() -> Bool {
        let phoneRegex = "^\\d{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let isValid = phoneTest.evaluate(with: self)

        return isValid
    }

    func isValidZipCode() -> Bool {
        let zipRegex = "^\\d{5}$"
        let zipTest = NSPredicate(format: "SELF MATCHES %@", zipRegex)
        let isValid = zipTest.evaluate(with: self)

        return isValid
    }
    func isValidPassword() -> Bool {
        return !isEmpty && range(of: ".*[^A-Za-z0-9].*", options: .regularExpression) == nil
    }
    func isValidIfsc() -> Bool {
         return !isEmpty && range(of: "[a-z|A-Z]{4}[0][a-zA-Z0-9]{6}$", options: .regularExpression) == nil
    }

    /*func encryptString(text:String) -> String {
        
        let key = kPrivateKey
        
        let kkey =  key.data(using: .utf8, allowLossyConversion: false)
        
        let a = try! AES.init(key: kkey?.bytes, iv: nil, blockMode: .ECB, padding: PKCS7)
        
        let base64String = try! text.encrypt(a).toBase64()!
        
        return base64String
    }*/

    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""

        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString.toBase64()
    }

    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    func isAlphaNumeric() -> Bool {
        let inverseSet = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").inverted

        let components = self.components(separatedBy: inverseSet)

        let filtered = components.joined(separator: "")

        return self == filtered
    }

}

extension Collection where Indices.Iterator.Element == Index {
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
