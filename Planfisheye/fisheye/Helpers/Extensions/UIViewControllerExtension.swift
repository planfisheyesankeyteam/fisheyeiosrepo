//
//  UIViewControllerExtension.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

extension UIViewController {

    class func instantiateFromStoryboardWithIdentifier(identifier: String) -> Self {

        return instantiateFromStoryboardHelper(type: self, storyboardName: "Main", identifier: identifier)

    }

    private class func instantiateFromStoryboardHelper<T>(type: T.Type, storyboardName: String, identifier: String) -> T {

        let storyboad = UIStoryboard(name: storyboardName, bundle: nil)

        let controller = storyboad.instantiateViewController(withIdentifier: identifier) as! T

        return controller
    }

    func pushViewController(inNavVC: UINavigationController?) {

        if let navigationController = inNavVC {
            navigationController.pushViewController(self, animated: true)
        }
    }

    func popViewController() {

        let navigationController = self.navigationController

        navigationController?.popViewController(animated: true)

    }

    func popToRootViewController() {

        let navigationController = kApplicationDelegate.nVC

        navigationController?.popToRootViewController(animated: true)
    }

}
