/*
 Copyright © 2014, Audvisor Inc.
 Written under contract by Robosoft Technologies Pvt. Ltd.
 */

#import "ProgressView.h"


@implementation ProgressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (instancetype)sharedProgressView
{
    static dispatch_once_t once;
    static ProgressView *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[ProgressView alloc] init];
        
        sharedInstance.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        
        CALayer *layer = [CALayer layer];
        layer.frame = sharedInstance.bounds;
        layer.opacity = 0.5;
        layer.backgroundColor  = [UIColor blackColor].CGColor;
        [sharedInstance.layer addSublayer: layer];
    });
    return sharedInstance;
}

- (void)showProgressView
{
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithFrame:self.bounds];
    activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    [self addSubview:activity];
    [activity startAnimating];
    
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
}

- (void)hideProgressView
{
    [self removeFromSuperview];
}

@end
