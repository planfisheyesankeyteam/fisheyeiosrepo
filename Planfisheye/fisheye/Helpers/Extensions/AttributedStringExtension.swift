//
//  AttributedStringExtension.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key : Any]? = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont(name: "AvenirNext-Medium", size: 12)!]
        let boldString = NSMutableAttributedString(string: "\(text)", attributes: attrs)
        self.append(boldString)
        return self
    }

    func normal(_ text: String) -> NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }

    func colorString(_ text: String, withColor color: UIColor) -> NSMutableAttributedString {

        let attrs: [NSAttributedString.Key : Any]? = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): color]
        let cString = NSMutableAttributedString(string: "\(text)", attributes: attrs)
        self.append(cString)
        return self
    }

    func underLine(_ text: String, withColor color: UIColor) -> NSMutableAttributedString {

        let attrs: [NSAttributedString.Key : Any]? = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): color, NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSUnderlineStyle.single.rawValue as AnyObject]
        let boldString = NSMutableAttributedString(string: "\(text)", attributes: attrs)
        self.append(boldString)
        return self
    }

    public func setAsLink(_ textToFind: String, linkURL: String) -> NSMutableAttributedString {

        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedString.Key.link, value: linkURL, range: foundRange)
            return self
        }
        return self
    }
}
