//
//  UICustomUnderlinedText.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

class UICustomUnderlinedText: UITextView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    var border: UIView!

    let transition = CATransition()

    var label = UILabel()

    override func draw(_ rect: CGRect) {
        // Drawing code

        border = UIView()
        border.frame = CGRect.init(x: self.frame.origin.x, y: self.frame.origin.y+self.frame.height-1, width: self.frame.width, height: 1)
        self.superview!.insertSubview(border, aboveSubview: self)

        label.frame = CGRect.init(x: self.frame.origin.x, y: self.frame.origin.y - 18, width: self.frame.width, height: 22)
        label.font = UIFont(descriptor: self.font.fontDescriptor, size: 12)
        label.textColor = UIColor.black60Two54()

        self.superview!.insertSubview(label, aboveSubview: self)

        self.createGradientLayer(inView: self.border, colors: [UIColor.black.cgColor, UIColor.white.cgColor])

    }

    func createGradientLayer(inView view: UIView, colors: [CGColor]) {

        let gradientLayer = CAGradientLayer()

        // Hoizontal - commenting these two lines will make the gradient veritcal
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)

        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)

        let gradientLocations: [NSNumber] = [0.0, 1.0]

        gradientLayer.frame = view.bounds

        gradientLayer.colors = colors

        gradientLayer.locations = gradientLocations

        view.layer.addSublayer(gradientLayer)

    }

    private struct Constants {
        static let defaultiOSPlaceholderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0980392, alpha: 0.22)
    }
    private let placeholderLabel: UILabel = UILabel()

    private var placeholderLabelConstraints = [NSLayoutConstraint]()

    @IBInspectable public var placeholder: String = "" {
        didSet {
            placeholderLabel.text = placeholder
        }
    }

    @IBInspectable public var placeholderColor: UIColor = UICustomUnderlinedText.Constants.defaultiOSPlaceholderColor {
        didSet {
            placeholderLabel.textColor = placeholderColor
        }
    }

    override public var font: UIFont! {
        didSet {
            placeholderLabel.font = font
        }
    }

    override public var textAlignment: NSTextAlignment {
        didSet {
            placeholderLabel.textAlignment = textAlignment
        }
    }

    override public var text: String! {
        didSet {
            textDidChange()
        }
    }

    override public var attributedText: NSAttributedString! {
        didSet {
            textDidChange()
        }
    }

    override public var textContainerInset: UIEdgeInsets {
        didSet {
            updateConstraintsForPlaceholderLabel()
        }
    }

    override public init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.commonInit()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    private func commonInit() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidChange),
                                               name: UITextView.textDidChangeNotification,
                                               object: nil)

        placeholderLabel.font = font
        placeholderLabel.textColor = UIColor.lavender54()
        placeholderLabel.textAlignment = textAlignment
        placeholderLabel.text = placeholder
        placeholderLabel.numberOfLines = 0
        placeholderLabel.backgroundColor = UIColor.clear
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(placeholderLabel)
        updateConstraintsForPlaceholderLabel()
        //self.textColor = UIColor.primary_text()
    }

    private func updateConstraintsForPlaceholderLabel() {
        var newConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(textContainerInset.left + textContainer.lineFragmentPadding))-[placeholder]",
            options: [],
            metrics: nil,
            views: ["placeholder": placeholderLabel])
        newConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(textContainerInset.top))-[placeholder]-|",
            options: [],
            metrics: nil,
            views: ["placeholder": placeholderLabel])
        newConstraints.append(NSLayoutConstraint(
            item: placeholderLabel,
            attribute: .width,
            relatedBy: .equal,
            toItem: self,
            attribute: .width,
            multiplier: 1.0,
            constant: -(textContainerInset.left + textContainerInset.right + textContainer.lineFragmentPadding * 2.0)
        ))
        removeConstraints(placeholderLabelConstraints)
        addConstraints(newConstraints)
        placeholderLabelConstraints = newConstraints
    }

    @objc private func textDidChange() {

        placeholderLabel.isHidden = !text.isEmpty

        label.isHidden = text.isEmpty

        label.text = placeholderLabel.text

//        self.textColor = UIColor.black
//        if !text.isEmpty{
//            if text.characters.count == 1{
//                placeholderLabel.frame.origin.y = self.bounds.origin.y - 30
//                placeholderLabel.font = UIFont(descriptor: self.font.fontDescriptor, size: 8)
//                placeholderLabel.textColor = UIColor.frenchBlue87()
//                annimateLayer(inView: border)
//            }
//        
//        }
//        else{
//            commonInit()
//        }
    }

    func configOnError(withPlaceHolder text: String) {

        let attrbStr = NSMutableAttributedString()

        let placeholderStr = attrbStr.colorString(text, withColor: UIColor.red50())

        self.createGradientLayer(inView: self.border, colors: [UIColor.red.cgColor, UIColor.white.cgColor])

        self.attributedText = placeholderStr

        self.label.textColor = UIColor.red

        self.border.backgroundColor = UIColor.red
    }

    func configOnFirstResponder() {

        if self.isFirstResponder == true {
            self.createGradientLayer(inView: self.border, colors: [UIColor.frenchBlue().cgColor, UIColor.white.cgColor])
            self.label.textColor = UIColor.frenchBlue87()

        }
    }

    func configOnResignResponder() {
        self.createGradientLayer(inView: self.border, colors: [UIColor.black.cgColor, UIColor.white.cgColor])
        self.label.textColor = UIColor.black60Two54()
    }

    func annimateLayer(inView view: UIView) {
        transition.duration = 0.2
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        view.layer.add(transition, forKey: kCATransition)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        placeholderLabel.preferredMaxLayoutWidth = textContainer.size.width - textContainer.lineFragmentPadding * 2.0
    }

    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name:UITextView.textDidChangeNotification,
                                                  object: nil)
    }
}
