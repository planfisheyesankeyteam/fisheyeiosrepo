//
//  CardView.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 06/07/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit

@objc protocol CardViewDelegate {

    func didSelectCard(withTitle: String)
    func moveCard(position: Int)
    func logbookClick(withTitle: String)
}

class CardView: UIViewController {

    var loadingComplete: ((_ success: Bool) -> Void)?
    let appService = AppService.shared
    enum Position: Int {
        case upRight = 0
        case right = 1
        case down = 2
        case left = 3
        case leftUp = 4
    }

   // @IBOutlet weak var bkgImgView: UIImageView!

    @IBOutlet weak var titleLblConstraints: NSLayoutConstraint!

    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!

    var position: Position!

    weak var cardViewDelegate: CardViewDelegate?

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!

    @IBOutlet weak var btn: UIButton!
      @IBOutlet weak var cardBtn: UIButton!
      @IBOutlet weak var inviteCountLbl: UILabel!

    @IBOutlet weak var notificationBtn: UIButton!

    @IBOutlet var main: UIView!
    func hideViewAfterloadingComplete(_ success: Bool) {
        // handle successful sign in
        if (success) {
        //    self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
            self.cardNameView.layer.cornerRadius = 6
            self.cardNameView.layer.shadowColor = UIColor.black.cgColor
            self.cardNameView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
            self.cardNameView.layer.shadowOpacity = 0.2
            self.cardNameView.layer.shadowRadius = 3.0
            self.cardNameView.layer.masksToBounds = false
          //  self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
            self.view.updateConstraints()

            self.view.layoutSubviews()

        } else {
            // handle cancel operation from user
         //  self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
            self.cardNameView.layer.cornerRadius = 6
            self.cardNameView.layer.shadowColor = UIColor.black.cgColor
            self.cardNameView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
            self.cardNameView.layer.shadowOpacity = 0.2
            self.cardNameView.layer.shadowRadius = 3.0
            self.cardNameView.layer.masksToBounds = false
            self.view.updateConstraints()

            self.view.layoutSubviews()
        }
    }

    @IBOutlet weak var cardNameView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.titleLbl.textColor = UIColor.frenchBlue()
       // self.bkgImgView.backgroundColor = UIColor.duckEggBlue()
        self.cardNameView.layer.cornerRadius = 6
        self.cardNameView.layer.shadowColor = UIColor.black.cgColor
        self.cardNameView.layer.shadowOffset = CGSize(width: 0.2, height: 0.2)
        self.cardNameView.layer.shadowOpacity = 0.2
        self.cardNameView.layer.shadowRadius = 3.0
        self.cardNameView.layer.masksToBounds = false
        self.cardNameView.backgroundColor = UIColor.hexStringToUIColor(hex: self.appService.carouselCardColor)
        self.titleLbl.adjustsFontSizeToFitWidth = true
        self.inviteCountLbl.adjustsFontSizeToFitWidth = true

//        self.inviteCountLbl.backgroundColor = UIColor.profileBottomStack()
        self.inviteCountLbl.textColor = UIColor.white

        self.inviteCountLbl.layer.cornerRadius = self.inviteCountLbl.frame.size.width/2

        self.inviteCountLbl.layer.masksToBounds = true

        self.btn.isUserInteractionEnabled = true
//        btn.layer.borderWidth = 1
//        btn.layer.borderColor = UIColor.green.cgColor
        self.btn.addTarget(self, action: #selector(navigateTo), for: .touchUpInside)
       self.cardBtn.addTarget(self, action: #selector(navigateTo), for: .touchUpInside)
        self.notificationBtn.addTarget(self, action: #selector(notificationBtnClick), for: .touchUpInside)

    }

    @objc func notificationBtnClick() {
   cardViewDelegate?.logbookClick(withTitle: self.titleLbl.text!)

    }

    @objc func navigateTo() {
        if self.position == .down {

            guard let method = cardViewDelegate?.didSelectCard(withTitle: self.titleLbl.text!) else {
                return
            }
            method
        } else {
                  guard let method = cardViewDelegate?.moveCard(position: self.position.rawValue) else {
                        return
            }
                  method

        }
    }

}

extension UIView {

    @discardableResult   // 1
    func fromNib<T: UIView>() -> T? {   // 2
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as? T else {    // 3
            // xib not loaded, or it's top view is of the wrong type
            return nil
        }
        self.addSubview(view)     // 4
        view.translatesAutoresizingMaskIntoConstraints = false   // 5
      //  view.layouta(to: self)   // 6
        return view   // 7
    }
}
