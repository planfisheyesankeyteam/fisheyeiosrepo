//
//  MenuBarCollectionViewCell.swift
//  TA Wallet
//
//  Created by Keerthi Chinivar on 13/01/17.
//  Copyright © 2017 TA. All rights reserved.
//

import UIKit

class MenuBarCollectionViewCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectedView: UIView!

    @IBOutlet weak var imageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)

    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

//    override var isHighlighted: Bool {
//        didSet {
//            titleLabel.textColor = isHighlighted ? UIColor.white : UIColor.purplyBlue10()
//        }
//    }
//    
//    override var isSelected: Bool {
//        didSet {
//            titleLabel.textColor = isSelected ? UIColor.white : UIColor.purplyBlue10()
//        }
//    }

}
