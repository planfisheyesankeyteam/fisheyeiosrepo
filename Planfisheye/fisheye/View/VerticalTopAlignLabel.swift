//
//  VerticalTopAlignLabel.swift
//  IshaYoga
//
//  Created by Sakshi Jain on 25/07/16.
//  Copyright © 2016 Compassites. All rights reserved.
//

import UIKit

class VerticalTopAlignLabel: UILabel {

    override func drawText(in rect: CGRect) {
        guard self.text != nil else {
            return super.drawText(in: rect)
        }

        let attributedText = NSAttributedString.init(string: self.text!, attributes: [NSAttributedString.Key.font: self.font])
        var newRect = rect
        newRect.size.height = attributedText.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size.height

        if self.numberOfLines != 0 {
            newRect.size.height = min(newRect.size.height, CGFloat(self.numberOfLines) * self.font.lineHeight)
        }
        super.drawText(in: newRect)
    }
}
