//
//  UICustomTextField.swift
//  Planfisheye
//
//  Created by Keerthi Chinivar on 20/06/17.
//  Copyright © 2017 fisheye. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class UICustomTextField: JVFloatLabeledTextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    required public init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

     //   self.floatingLabelYPadding = -5.0

        self.textColor = UIColor.black60Two87()

        self.floatingLabelActiveTextColor = UIColor.rebrandedLightPurple()

        self.floatingLabelTextColor = UIColor.rebrandedLightPurple()

        self.animateEvenIfNotFirstResponder = true

        self.font = UIFont(name: "Montserrat-Regular", size: 16)

        let attrs: [NSAttributedString.Key : Any]? = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): self.font!, NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.rebrandedTextLightPurple()]

        let cString = NSAttributedString(string: self.placeholder!, attributes: attrs)

        self.setAttributedPlaceholder(cString, floatingTitle: self.placeholder!)

    }

    func configOnFirstResponder(withPlaceHolder text: String) {

        if self.isFirstResponder == true {

            self.floatingLabelActiveTextColor = UIColor.rebrandedLightPurple()

            self.floatingLabelTextColor = UIColor.black60Two54()

            self.placeholder = text
        }
    }

    func configOnError(withPlaceHolder text: String) {

        let attrbStr = NSMutableAttributedString()

        let placeholderStr = attrbStr.colorString(text, withColor: UIColor.red50())

        self.attributedPlaceholder = placeholderStr

        self.floatingLabelTextColor = UIColor.red

        self.floatingLabelActiveTextColor = UIColor.red

    }
    func configOnErrorCleared(withPlaceHolder text: String) {

        let attrbStr = NSMutableAttributedString()

        let placeholderStr = attrbStr.colorString(text, withColor: UIColor.rebrandedLightPurple())

        self.attributedPlaceholder = placeholderStr

        self.floatingLabelTextColor = UIColor.rebrandedLightPurple()

        self.floatingLabelActiveTextColor = UIColor.rebrandedLightPurple()

    }

}
