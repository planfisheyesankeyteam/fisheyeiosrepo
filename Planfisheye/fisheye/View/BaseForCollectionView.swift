//
//  BaseForCollectionView.swift
//  TA Wallet
//
//  Created by Keerthi Chinivar on 09/01/17.
//  Copyright © 2017 TA. All rights reserved.
//

import UIKit

@objc protocol MenuBarDelegate {
    func cellTappedAt(index: Int)
}

class BaseForCollectionView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    lazy var baseCollectionView: UICollectionView  = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: self.bounds, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.isScrollEnabled = false
        return cv
    }()

    weak var menubarDelegate: MenuBarDelegate?

    var selectedIndex: Int = -1

    var inputData: [Source]!

    let kMenuCell = "menuBarCollectionViewCell"

    init(frame: CGRect, dataSource: [Source] ) {
        super.init(frame: frame)
        self.inputData = dataSource
        self.baseCollectionView.register(UINib.init(nibName: "MenuBarCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: kMenuCell)
        self.baseCollectionView.backgroundColor = UIColor.clear
        self.addSubview(baseCollectionView)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width/4, height: frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inputData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kMenuCell, for: indexPath) as! MenuBarCollectionViewCell

        cell.imageView.image = self.inputData[indexPath.row].image

        cell.titleLabel.text  =  self.inputData[indexPath.row].title

                if indexPath.row == self.selectedIndex {
                    cell.selectedView.isHidden = false
                } else {
                   cell.selectedView.isHidden = true
                  }

        cell.backgroundColor = UIColor.clear
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.baseCollectionView.reloadData()
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kMenuCell, for: indexPath) as! MenuBarCollectionViewCell
        var index: Int = 0

        let title = self.inputData[indexPath.row].title

        switch title {
        case "My Profile"?:
            index = 0
            break
        case "Meet Me"?:

            index = 1
            break
        case "Sync"?:

            index = 2
            break

        case "Pulse"?:

            index = 3
            break

        case "Lookout"?:

            index = 4
            cell.selectedView.backgroundColor = UIColor.white
            break

        default:
            break
        }
        guard let method = menubarDelegate?.cellTappedAt(index: index) else {
            return
        }
    }

    // MARK: UICollectionViewDelegate

    /*
     // Uncomment this method to specify if the specified item should be highlighted during tracking
     override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
     return true
     }
     */

    /*
     // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
     return false
     }
     
     override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
     
     }
     */

}
