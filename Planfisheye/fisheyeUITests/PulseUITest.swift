//
//  PulseUITest.swift
//  fisheyeUITests
//
//  Created by venkatesh murthy on 09/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest

class PulseUITest: XCTestCase {

    override func setUp() {
        super.setUp()

        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    //share pulse
    func testExample() {

//        toolbarDoneButtonButton.tap()
//        app.buttons["FinalSubmit"].tap()

        let app = XCUIApplication()
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        let button = element.children(matching: .other).element(boundBy: 2).children(matching: .other).element.children(matching: .button).element
        button/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        button/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element.children(matching: .other).element(boundBy: 4).children(matching: .other).element.children(matching: .button).element.tap()
        element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()

        let element4 = element.children(matching: .other).element(boundBy: 1)
        let element3 = element4.children(matching: .other).element.children(matching: .other).element
        let element2 = element3.children(matching: .other).element(boundBy: 2)
        element2.tap()
        element2.children(matching: .button).element(boundBy: 1).tap()
        app/*@START_MENU_TOKEN@*/.keys["A"]/*[[".keyboards.keys[\"A\"]",".keys[\"A\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.keys["n"]/*[[".keyboards.keys[\"n\"]",".keys[\"n\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.keys["u"]/*[[".keyboards.keys[\"u\"]",".keys[\"u\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Anuradha"]/*[[".cells.staticTexts[\"Anuradha\"]",".staticTexts[\"Anuradha\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        element3.children(matching: .button).matching(identifier: "Button").element(boundBy: 1).tap()
        element3.children(matching: .button)["Button"].tap()
        element4.children(matching: .other).element(boundBy: 1).tap()
    //    element4.staticTexts["A location request has been sent"].tap()
         XCTAssert(app.staticTexts["Pulse logbook"].exists)

    }
     func testPulseRequest() {

        let app = XCUIApplication()
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        let element = element2.children(matching: .other).element(boundBy: 2).children(matching: .other).element
        element.children(matching: .other).element.children(matching: .button).element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element.children(matching: .button).element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element2.children(matching: .other).element(boundBy: 4).children(matching: .other).element.children(matching: .button).element.tap()
        element2.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
        app.buttons["Request"].tap()

        let element3 = element2.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
        element3.children(matching: .other).element(boundBy: 2).children(matching: .button).element(boundBy: 1).tap()
        app/*@START_MENU_TOKEN@*/.keys["P"]/*[[".keyboards.keys[\"P\"]",".keys[\"P\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.keys["r"]/*[[".keyboards.keys[\"r\"]",".keys[\"r\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

        let iKey = app/*@START_MENU_TOKEN@*/.keys["i"]/*[[".keyboards.keys[\"i\"]",".keys[\"i\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        iKey.tap()

        app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Pritesh "]/*[[".cells.staticTexts[\"Pritesh \"]",".staticTexts[\"Pritesh \"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        element3.children(matching: .button).matching(identifier: "Button").element(boundBy: 1).tap()
        element3.children(matching: .button)["Button"].tap()
        XCTAssert(app.staticTexts["Pulse logbook"].exists)

    }

    //To check No Contact Found
    func testContactPresent() {

        let app = XCUIApplication()
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        let element4 = element2.children(matching: .other).element(boundBy: 4).children(matching: .other).element
        let element = element4.children(matching: .other).element
        element.children(matching: .button).element(boundBy: 0)/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element.children(matching: .button).element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element4.children(matching: .button).element.tap()
        element2.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
        element2.tap()
        app.buttons["Request"].tap()

        let element3 = element2.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
        element3.children(matching: .other).element(boundBy: 2).children(matching: .button).element(boundBy: 1).tap()

        let app2 = app
        app2/*@START_MENU_TOKEN@*/.keys["A"]/*[[".keyboards.keys[\"A\"]",".keys[\"A\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

        let nKey = app2/*@START_MENU_TOKEN@*/.keys["n"]/*[[".keyboards.keys[\"n\"]",".keys[\"n\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        nKey.tap()
        nKey.tap()

        let rKey = app2/*@START_MENU_TOKEN@*/.keys["r"]/*[[".keyboards.keys[\"r\"]",".keys[\"r\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        rKey.tap()
        rKey.tap()
        rKey.tap()
        app2/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
      //  app.staticTexts["No contact found."].tap()
        XCTAssert(app.staticTexts["No contact found."].exists)

//        element3.children(matching: .button).matching(identifier: "Button").element(boundBy: 1).tap()
//        app.staticTexts["You have not choosen any contact to share or request a pulse."].tap()

    }

    //test Schedule Request
    func testScheduleRequest() {

        let app2 = XCUIApplication()
        let element4 = app2.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        let element = element4.children(matching: .other).element(boundBy: 4).children(matching: .other).element
        element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).swipeRight()

        let element2 = element4
        element2/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element.children(matching: .button).element.tap()
        element2.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()

        let app = app2
        app.buttons["Request"].tap()

        let element3 = element2.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
        element3.children(matching: .other).element(boundBy: 2).children(matching: .button).element(boundBy: 1).tap()

        let pKey = app2/*@START_MENU_TOKEN@*/.keys["P"]/*[[".keyboards.keys[\"P\"]",".keys[\"P\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        pKey.tap()
        pKey.tap()

        let rKey = app2/*@START_MENU_TOKEN@*/.keys["r"]/*[[".keyboards.keys[\"r\"]",".keys[\"r\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        rKey.tap()
        rKey.tap()

        let iKey = app2/*@START_MENU_TOKEN@*/.keys["i"]/*[[".keyboards.keys[\"i\"]",".keys[\"i\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        iKey.tap()
        iKey.tap()
        app2/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards.buttons[\"Search\"]",".buttons[\"Search\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app2.tables/*@START_MENU_TOKEN@*/.staticTexts["Pritesh "]/*[[".cells.staticTexts[\"Pritesh \"]",".staticTexts[\"Pritesh \"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        element3.children(matching: .button).matching(identifier: "Button").element(boundBy: 1).tap()
        element3.children(matching: .other).element(boundBy: 5).children(matching: .button).element(boundBy: 0).tap()
        app.staticTexts["hh:mm"].tap()

        let datePickersQuery = app.datePickers
        datePickersQuery.pickerWheels["16 o’clock"].swipeUp()
        datePickersQuery.pickerWheels["17 o’clock"]/*@START_MENU_TOKEN@*/.press(forDuration: 0.6);/*[[".tap()",".press(forDuration: 0.6);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/

        let doneButton = app.buttons["Done"]
        doneButton.tap()
        app.buttons["Repeat"].tap()
        app.staticTexts["Start Date"].tap()

        let pickerWheel = datePickersQuery.pickerWheels["9"]
        pickerWheel.tap()

        let augustPickerWheel = datePickersQuery.pickerWheels["August"]
        augustPickerWheel.tap()

        let pickerWheel2 = datePickersQuery.pickerWheels["2018"]
        pickerWheel2.tap()
        pickerWheel.tap()
        pickerWheel2.swipeUp()

        let pickerWheel3 = datePickersQuery.pickerWheels["2020"]
        pickerWheel3.tap()
        datePickersQuery.pickerWheels["11"]/*@START_MENU_TOKEN@*/.press(forDuration: 3.0);/*[[".tap()",".press(forDuration: 3.0);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/

        let pickerWheel4 = datePickersQuery.pickerWheels["10"]
        pickerWheel4.tap()
        pickerWheel4.tap()
        doneButton.tap()

        let button = element3.children(matching: .other).element(boundBy: 6).children(matching: .button).element(boundBy: 3)
        button.tap()
        pickerWheel.swipeUp()
        pickerWheel4.tap()
        augustPickerWheel.tap()
        pickerWheel2.tap()
        pickerWheel3.tap()
        pickerWheel3.tap()
        pickerWheel3.tap()
        doneButton.tap()
        app.buttons["Sun"].tap()
        app.buttons["Tue"].tap()

        let button2 = element3.children(matching: .button)["Button"]
        button2.tap()
        button.tap()
        pickerWheel2/*@START_MENU_TOKEN@*/.press(forDuration: 0.7);/*[[".tap()",".press(forDuration: 0.7);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        datePickersQuery.pickerWheels["2019"].tap()
        doneButton.tap()
        button2.tap()
        app.buttons["Share"].tap()
        button.swipeUp()
        element3.children(matching: .other).element(boundBy: 0).children(matching: .button).element.tap()

    }

    //valid date
    func testValidDate() {

        let app = XCUIApplication()
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        let element = element2.children(matching: .other).element(boundBy: 4).children(matching: .other).element
        element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
        element2.tap()
        element.children(matching: .button).element.tap()
        element2.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()

        let element3 = element2.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
        element3.children(matching: .other).element(boundBy: 5).children(matching: .button).element(boundBy: 0).tap()
        app.staticTexts["hh:mm"].tap()

        let datePickersQuery = app.datePickers
        datePickersQuery.pickerWheels["17 o’clock"].tap()

        let pickerWheel = datePickersQuery.pickerWheels["36 minutes"]
        pickerWheel/*@START_MENU_TOKEN@*/.press(forDuration: 1.2);/*[[".tap()",".press(forDuration: 1.2);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        pickerWheel.tap()
        datePickersQuery.pickerWheels["35 minutes"].tap()
        app.buttons["Done"].tap()
        app.staticTexts["Start Date"].tap()

        let pickerWheel2 = datePickersQuery.pickerWheels["9"]
        pickerWheel2.tap()
        pickerWheel2/*@START_MENU_TOKEN@*/.press(forDuration: 0.8);/*[[".tap()",".press(forDuration: 0.8);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        pickerWheel2.tap()
        pickerWheel2.swipeDown()
        pickerWheel2.tap()
        app.buttons["Cancel"].tap()
        element3.children(matching: .other).element(boundBy: 6)/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element3.children(matching: .button)["Button"].tap()
        app.staticTexts["You have not choosen any contact to share or request a pulse."].tap()

    }
}
