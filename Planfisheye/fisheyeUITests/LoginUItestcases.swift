import XCTest

class LoginUItestcases: XCTestCase {

    override func setUp() {
        super.setUp()

        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()

    }

    func testExample() {

        let app = XCUIApplication()
        let textField = app.textFields["Username"]
        textField.tap()
        textField.typeText("pritamcc.chavan@gmail.com")
        sleep(3)
        let password = app.secureTextFields["Password"]
        password.tap()
        password.typeText("Pritam@1996")
        sleep(3)
        app.buttons["FinalSubmit"].tap()
        sleep(3)
       //  app.staticTexts["Login Successful"].tap()
        XCTAssert(app.staticTexts["Login Successful"].exists)

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

    }
    func testchangeMasterkey() {

        let app = XCUIApplication()
        let textField = app.textFields["Username"]
        textField.tap()
        textField.typeText("pritamchavan@gmail.com")
        sleep(3)
        let password = app.secureTextFields["Password"]
        password.tap()
        password.typeText("Pritam@195")
        sleep(3)
        app.buttons["FinalSubmit"].tap()
        sleep(3)
       XCTAssert( app.staticTexts["Your login details are incorrect. Request you to please check the details provided"].exists)
       // app.staticTexts["Your login details are incorrect. Request you to please check the details provided"].tap()

        app.staticTexts["Your login details are incorrect"].tap()

    }

    func testSocialUser() {

        let app = XCUIApplication()
        let textField = app.textFields["Username"]
        textField.tap()
        textField.typeText("priteshchavan1998@gmail.com")
        sleep(3)
        let password = app.secureTextFields["Password"]
        password.tap()
        password.typeText("Pritam@195")
        sleep(3)
        app.buttons["FinalSubmit"].tap()
        sleep(3)
        XCTAssert( app.staticTexts["Your login details are incorrect"].exists)
    }

    func testSocilSignUpUser() {

//        let app = XCUIApplication()
//        app.textFields["Username"].tap()
//
//        let passwordSecureTextField = app.secureTextFields["Password"]
//
//        passwordSecureTextField.tap()
//        app.toolbars["Toolbar"].buttons["Toolbar Done Button"].tap()
//        app.buttons["FinalSubmit"].tap()
//        app.staticTexts["Login Successful"].tap()
//
//        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
//        let element = element2.children(matching: .other).element(boundBy: 4).children(matching: .other).element
//        element.tap()
//        element2.children(matching: .other).element(boundBy: 2).children(matching: .other).element.children(matching: .button).element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//
//        let element3 = element2.children(matching: .other).element(boundBy: 0).children(matching: .other).element
//        element3.children(matching: .other).element.children(matching: .button).element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        element.swipeDown()
//        element3.children(matching: .button).element.swipeDown()
//
//        let element4 = element2.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
//        let button = element4.children(matching: .button).element
//        button/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        button/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeDown()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
//        button.tap()
//        element.children(matching: .button).element.tap()
//        element2.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
//        element4.children(matching: .other).element(boundBy: 2).children(matching: .button).element(boundBy: 1).tap()
//
//        _ = XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 2)
//        element.tap()
//        element.tap()
//

    }

}
