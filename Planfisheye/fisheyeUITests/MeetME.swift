//
//  MeetME.swift
//  fisheyeUITests
//
//  Created by venkatesh murthy on 08/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest

class MeetME: XCTestCase {

    override func setUp() {
        super.setUp()

        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let app = XCUIApplication()
        let textField = app.textFields["Username"]
        textField.tap()
        textField.typeText("pritamcc.chavan@gmail.com")
        sleep(3)
        let password = app.secureTextFields["Password"]
        password.tap()
        password.typeText("Pritam@1996")
        sleep(3)
        app.buttons["FinalSubmit"].tap()
        sleep(3)

        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        element.children(matching: .other).element(boundBy: 4).children(matching: .other).element.children(matching: .button).element.tap()

        let element2 = element.children(matching: .other).element
        element2.children(matching: .button).element(boundBy: 0).tap()

        let scrollViewsQuery = app.scrollViews
        scrollViewsQuery.otherElements.containing(.staticText, identifier: "Participants can invite others").element.swipeUp()
        scrollViewsQuery.otherElements.containing(.image, identifier: "route").children(matching: .other).element(boundBy: 2).children(matching: .textField).element.swipeUp()
        app/*@START_MENU_TOKEN@*/.scrollViews.containing(.staticText, identifier: "Tap to locate on map").element/*[[".scrollViews.containing(.button, identifier:\"FinalSubmit\").element",".scrollViews.containing(.staticText, identifier:\"Tap to locate on map\").element"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        scrollViewsQuery.buttons["FinalSubmit"].tap()
    element2.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element(boundBy: 1).tap()

         XCTAssert( app.staticTexts["Details Incomplete"].exists)

    }

    func testcreateMeetme() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let app = XCUIApplication()
        let textField = app.textFields["Username"]
        textField.tap()
        textField.typeText("pritamcc.chavan@gmail.com")
        sleep(3)
        let password = app.secureTextFields["Password"]
        password.tap()
        password.typeText("Pritam@1996")
        sleep(3)
        app.buttons["FinalSubmit"].tap()

        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        element.children(matching: .other).element(boundBy: 4).children(matching: .other).element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
        element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()

        let scrollViewsQuery = app.scrollViews
        scrollViewsQuery.otherElements.textFields["Title"].tap()

        let nKey = app/*@START_MENU_TOKEN@*/.keys["n"]/*[[".keyboards.keys[\"n\"]",".keys[\"n\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        nKey.tap()
        nKey.tap()

        let iKey = app/*@START_MENU_TOKEN@*/.keys["i"]/*[[".keyboards.keys[\"i\"]",".keys[\"i\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        iKey.tap()
        iKey.tap()
        app/*@START_MENU_TOKEN@*/.keys["t"]/*[[".keyboards.keys[\"t\"]",".keys[\"t\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        iKey.tap()
        nKey.tap()
        nKey.tap()

        let spaceKey = app/*@START_MENU_TOKEN@*/.keys["space"]/*[[".keyboards.keys[\"space\"]",".keys[\"space\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        spaceKey.tap()
        spaceKey.tap()

        let cKey = app/*@START_MENU_TOKEN@*/.keys["c"]/*[[".keyboards.keys[\"c\"]",".keys[\"c\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        cKey.tap()
        cKey.tap()
        app/*@START_MENU_TOKEN@*/.keys["o"]/*[[".keyboards.keys[\"o\"]",".keys[\"o\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.keys["m"]/*[[".keyboards.keys[\"m\"]",".keys[\"m\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.keys["p"]/*[[".keyboards.keys[\"p\"]",".keys[\"p\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.keys["a"]/*[[".keyboards.keys[\"a\"]",".keys[\"a\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        nKey.tap()

        let yKey = app/*@START_MENU_TOKEN@*/.keys["y"]/*[[".keyboards.keys[\"y\"]",".keys[\"y\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        yKey.tap()
        yKey.tap()

        let toolbar = app.toolbars["Toolbar"]
        toolbar.buttons["Toolbar Done Button"].tap()
        scrollViewsQuery.otherElements.containing(.textField, identifier: "Location").children(matching: .button).element.tap()
        app.buttons["Cancel"].tap()

        let dableNagarMap = app/*@START_MENU_TOKEN@*/.maps.containing(.other, identifier: "Dable Nagar").element/*[[".maps.containing(.other, identifier:\"Satkar Grande\").element",".maps.containing(.other, identifier:\"Padwal\").element",".maps.containing(.other, identifier:\"Bhat Wadi\").element",".maps.containing(.other, identifier:\"Kisan Nagar No 4\").element",".maps.containing(.other, identifier:\"Kailash Nagar\").element",".maps.containing(.other, identifier:\"Shri Nagar\").element",".maps.containing(.other, identifier:\"Kisan Nagar No 3\").element",".maps.containing(.other, identifier:\"Kisan Nagar No 1\").element",".maps.containing(.other, identifier:\"Fusion Dhaba\").element",".maps.containing(.other, identifier:\"Nehru Nagar\").element",".maps.containing(.other, identifier:\"Wagle Estate Police Station\").element",".maps.containing(.other, identifier:\"Hanuman Nagar\").element",".maps.containing(.other, identifier:\"Ambe Wadi\").element",".maps.containing(.other, identifier:\"Karvalo Nagar\").element",".maps.containing(.other, identifier:\"Yashoda Nagar\").element",".maps.containing(.other, identifier:\"Patil Nagar\").element",".maps.containing(.other, identifier:\"Dable Nagar\").element"],[[[-1,16],[-1,15],[-1,14],[-1,13],[-1,12],[-1,11],[-1,10],[-1,9],[-1,8],[-1,7],[-1,6],[-1,5],[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        dableNagarMap/*@START_MENU_TOKEN@*/.press(forDuration: 1.0);/*[[".tap()",".press(forDuration: 1.0);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        dableNagarMap/*@START_MENU_TOKEN@*/.press(forDuration: 2.1);/*[[".tap()",".press(forDuration: 2.1);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        app/*@START_MENU_TOKEN@*/.maps.containing(.other, identifier: "Vartak Nagar Police station").element/*[[".maps.containing(.other, identifier:\"Mulund Railway Station\").element",".maps.containing(.other, identifier:\"Johnson & Johnson Garden\").element",".maps.containing(.other, identifier:\"R Mall\").element",".maps.containing(.other, identifier:\"Yama\").element",".maps.containing(.other, identifier:\"Eternity Mall\").element",".maps.containing(.other, identifier:\"Thane\").element",".maps.containing(.other, identifier:\"Korum Mall\").element",".maps.containing(.other, identifier:\"Jupiter Hospital\").element",".maps.containing(.other, identifier:\"Vartak Nagar Police station\").element"],[[[-1,8],[-1,7],[-1,6],[-1,5],[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        toolbar.buttons["Done"].tap()

        let element2 = scrollViewsQuery.children(matching: .other).element(boundBy: 2)
        let button = element2.children(matching: .other).element(boundBy: 0).children(matching: .button).element
        button.tap()

        let datePickersQuery = app.datePickers
        let pickerWheel = datePickersQuery.pickerWheels["2018"]
        pickerWheel.swipeUp()

        let doneButton = app.buttons["Done"]
        doneButton.tap()

        let element3 = scrollViewsQuery.children(matching: .other).element(boundBy: 3)
        let button2 = element3.children(matching: .other).element(boundBy: 0).children(matching: .button).element
        button2.tap()
        pickerWheel.tap()
        datePickersQuery.pickerWheels["2025"].tap()
        button.tap()
        pickerWheel.tap()
        datePickersQuery.pickerWheels["2019"].tap()
        doneButton.tap()
        button2.tap()
        pickerWheel.tap()
        doneButton.tap()
        element2.children(matching: .other).element(boundBy: 2).children(matching: .button).element.tap()

        let pickerWheel2 = datePickersQuery.pickerWheels["16 o’clock"]
        pickerWheel2.tap()
        doneButton.tap()
        element3.children(matching: .other).element(boundBy: 2).children(matching: .button).element.tap()
        pickerWheel2.tap()
        doneButton.tap()
        scrollViewsQuery.otherElements.containing(.image, identifier: "profile").children(matching: .button).element.tap()

        _ = XCUIApplication()
        _ = app.scrollViews
        scrollViewsQuery.otherElements.collectionViews.cells.otherElements.containing(.staticText, identifier: "Anuradha").element.tap()
        scrollViewsQuery.otherElements.containing(.image, identifier: "profile").children(matching: .collectionView).element/*@START_MENU_TOKEN@*/.swipeRight()/*[[".swipeUp()",".swipeRight()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        app/*@START_MENU_TOKEN@*/.buttons["FinalSubmit"]/*[[".scrollViews.buttons[\"FinalSubmit\"]",".buttons[\"FinalSubmit\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

//        let element = XCUIApplication().children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
//        element.children(matching: .other).element(boundBy: 4).children(matching: .other).element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
//        element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
//
//        let app2 = XCUIApplication()
//        let scrollViewsQuery = app.scrollViews
//        scrollViewsQuery.otherElements.textFields["Title"].tap()
//        scrollViewsQuery.otherElements.containing(.textField, identifier:"Location").children(matching: .button).element.tap()
//        app2.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.tap()
//
//        _ = scrollViewsQuery.children(matching: .other).element(boundBy: 2)
//        element.children(matching: .other).element(boundBy: 0).children(matching: .button).element.tap()
//
//        let datePickersQuery = app.datePickers
//        let pickerWheel = datePickersQuery.pickerWheels["2018"]
//        pickerWheel.tap()
//
//        let doneButton = app.buttons["Done"]
//        doneButton.tap()
//
//        let element2 = scrollViewsQuery.children(matching: .other).element(boundBy: 3)
//        element2.children(matching: .other).element(boundBy: 0).children(matching: .button).element.tap()
//        pickerWheel.tap()
//        doneButton.tap()
//        element.children(matching: .other).element(boundBy: 2).children(matching: .button).element.tap()
//        doneButton.tap()
//        element2.children(matching: .other).element(boundBy: 2).children(matching: .button).element.tap()
//        datePickersQuery.pickerWheels["3 o’clock"].tap()
//        doneButton.tap()
//        scrollViewsQuery.otherElements.containing(.image, identifier:"profile").children(matching: .button).element.tap()

    }

}
