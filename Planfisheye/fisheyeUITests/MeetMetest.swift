//
//  MeetMetest.swift
//  fisheyeUITests
//
//  Created by Sankey Solutions on 08/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest

class MeetMetest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let app = XCUIApplication()
        let textField = app.textFields["Username"]
        textField.tap()
        textField.typeText("pritamcc.chavan@gmail.com")
        sleep(3)
        let password = app.secureTextFields["Password"]
        password.tap()
        password.typeText("Pritam@1996")
        sleep(3)
        app.buttons["FinalSubmit"].tap()
        sleep(3)
        app.buttons["FinalSubmit"].tap()
        app.staticTexts["Login Successful"].tap()

        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element
        element.children(matching: .other).element(boundBy: 4).children(matching: .other).element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
        element.children(matching: .other).element.children(matching: .button).element(boundBy: 0).tap()
        app.scrollViews.otherElements.containing(.staticText, identifier: "Participants can invite others").element.swipeUp()

    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
