//
//  Address+CoreDataProperties.swift
//  
//
//  Created by Keerthi on 13/10/17.
//
//

import Foundation
import CoreData

extension Address {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Address> {
        return NSFetchRequest<Address>(entityName: "Address")
    }

    @NSManaged public var addressId: String?
    @NSManaged public var formatted: String?
    @NSManaged public var streetAddress: String?
    @NSManaged public var locality: String?
    @NSManaged public var region: String?
    @NSManaged public var postalCode: String?
    @NSManaged public var country: String?
    @NSManaged public var type: String?

}
