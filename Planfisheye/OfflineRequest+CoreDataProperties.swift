//
//  OfflineRequest+CoreDataProperties.swift
//  
//
//  Created by Keerthi on 19/10/17.
//
//

import Foundation
import CoreData

extension OfflineRequest {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OfflineRequest> {
        return NSFetchRequest<OfflineRequest>(entityName: "OfflineRequest")
    }

    @NSManaged public var requestTask: NSObject?
    @NSManaged public var requestTaskId: Int64

}
