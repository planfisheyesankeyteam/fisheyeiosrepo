//
//  Contact+CoreDataProperties.swift
//  
//
//  Created by Keerthi on 13/10/17.
//
//

import Foundation
import CoreData

extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var company: String?
    @NSManaged public var contactId: String?
    @NSManaged public var email: String?
    @NSManaged public var isfisheye: Int64
    @NSManaged public var name: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var secondaryPhoneNumbers: NSObject?
    @NSManaged public var secondaryEmails: NSObject?
    @NSManaged public var address: NSObject?

}
