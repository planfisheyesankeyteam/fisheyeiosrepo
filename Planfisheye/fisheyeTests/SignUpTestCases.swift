//
//  SignUpTestCases.swift
//  fisheyeTests
//
//  Created by Sankey Solutions on 25/07/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class SignUpTestCases: XCTestCase {

    var signUpotpvc: SignUPOTPVC!
    var signUpMasterKeyVC: SignUpMasterKeyVC!
    var signUpSecurityQuestionVC: SignUpSecurityQuestionVC!

    var signupmsg = SignupToastMsgHeadingSubheadingLabels()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        signUpotpvc = storyboard.instantiateViewController(withIdentifier: "SignUPOTPVC") as! SignUPOTPVC
        _ = signUpotpvc.view

        signUpMasterKeyVC = storyboard.instantiateViewController(withIdentifier: "SignUpMasterKeyVC") as! SignUpMasterKeyVC
        _ = signUpMasterKeyVC.view

        signUpSecurityQuestionVC = storyboard.instantiateViewController(withIdentifier: "SignUpSecurityQuestionVC") as! SignUpSecurityQuestionVC
        _ = signUpSecurityQuestionVC.view

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testEmailSmsOtp() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let emailotp = "123456"
        signUpotpvc.emailOTPTextField.text = emailotp

        let smsotp = "123456"
        signUpotpvc.otpTextField.text = smsotp

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "123456", emailOtp: "123456")
        let isValidationSuccess1 = self.signUpotpvc.validation as? Bool
        XCTAssertTrue(isValidationSuccess1!, "valid")

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "1236", emailOtp: "123456")
        let isValidationSuccess2 = self.signUpotpvc.validation as? Bool
        XCTAssertFalse(isValidationSuccess2!, "Invalid")

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "123456", emailOtp: "1234")
        let isValidationSuccess3 = self.signUpotpvc.validation as? Bool
        XCTAssertFalse(isValidationSuccess3!, "Invalid")

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "", emailOtp: "123456")
        let isValidationSuccess4 = self.signUpotpvc.validation as? Bool
        XCTAssertFalse(isValidationSuccess4!, "Invalid")

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "123456", emailOtp: "")
        let isValidationSuccess5 = self.signUpotpvc.validation as? Bool
        XCTAssertFalse(isValidationSuccess5!, "Invalid")

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "", emailOtp: "")
        let isValidationSuccess6 = self.signUpotpvc.validation as? Bool
        XCTAssertFalse(isValidationSuccess6!, "Invalid")

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "123456", emailOtp: "")
        let isValidationSuccess7 = self.signUpotpvc.validation as? Bool
        XCTAssertFalse(isValidationSuccess7!, "Invalid")

        signUpotpvc.otpVerificationButtonTapped(smsOtp: "", emailOtp: "123456")
        let isValidationSuccess8 = self.signUpotpvc.validation as? Bool
        XCTAssertFalse(isValidationSuccess8!, "Invalid")

    }

    func testMasterkey() {

        let masterkeysample = "123456"
        let reentermasterKeysample = ""

        let masterkeysample2 = "123456"
        let reentermasterKeysample2 = ""

        signUpMasterKeyVC.validationCheck(masterkeyText: "1234", reentermasterKeyText: "1234")
        let isValidationSuccess1 = self.signUpMasterKeyVC.validation as? Bool
        XCTAssertTrue(isValidationSuccess1!, "valid")

        signUpMasterKeyVC.validationCheck(masterkeyText: "1234", reentermasterKeyText: reentermasterKeysample)
        var titleLength = reentermasterKeysample.characters.count
        let isValidationSuccess2 = self.signUpMasterKeyVC.validation as? Bool
        XCTAssertTrue(titleLength==0, "Invalid")

        signUpMasterKeyVC.validationCheck(masterkeyText: "", reentermasterKeyText: "1234")
        let isValidationSuccess3 = self.signUpMasterKeyVC.validation as? Bool
        XCTAssertFalse(isValidationSuccess3!, "Invalid")

        signUpMasterKeyVC.validationCheck(masterkeyText: "1234", reentermasterKeyText: "1278")

        let isValidationSuccess4 = self.signUpMasterKeyVC.validation as? Bool
        XCTAssertTrue(isValidationSuccess4!, "Invalid")

        signUpMasterKeyVC.validationCheck(masterkeyText: "", reentermasterKeyText: "")
        let isValidationSuccess5 = self.signUpMasterKeyVC.validation as? Bool
        XCTAssertFalse(isValidationSuccess5!, "Invalid")

    }

    func testSecurityanswer() {
        signUpSecurityQuestionVC.checkThreeQuestionEnteredFunction(answerone: "", answertwo: "abc", answerthree: "abc")
        XCTAssertEqual(signupmsg.questionone, signUpSecurityQuestionVC.placeholder)

        signUpSecurityQuestionVC.checkThreeQuestionEnteredFunction(answerone: "abc", answertwo: "", answerthree: "abc")
        XCTAssertEqual(signupmsg.questionone, signUpSecurityQuestionVC.placeholdertwo)

        signUpSecurityQuestionVC.checkThreeQuestionEnteredFunction(answerone: "abc", answertwo: "abc", answerthree: "")
        XCTAssertEqual(signupmsg.questionone, signUpSecurityQuestionVC.placeholderthree)

        signUpSecurityQuestionVC.checkThreeQuestionEnteredFunction(answerone: "", answertwo: "", answerthree: "")
        XCTAssertEqual(signupmsg.questionone, signUpSecurityQuestionVC.placeholder)

        //        signUpSecurityQuestionVC.checkThreeQuestionEnteredFunction(answerone: "abc", answertwo: "abc", answerthree: "abc")
        
        //        XCTAssertEqual(signupmsg.questionone,signUpSecurityQuestionVC.placeholder)

        //        signUpSecurityQuestionVC.checkThreeQuestionEnteredFunction(answerone: "abc123!", answertwo: "abc112", answerthree: "abc!8*")
        //         XCTAssertEqual(signupmsg.questionone,signUpSecurityQuestionVC.placeholder)
        
    }

}
