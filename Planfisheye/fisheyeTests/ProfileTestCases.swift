//
//  ProfileTestCases.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 31/07/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class ProfileTestCases: XCTestCase {

    var profileChangeMasterKeyVC: ProfileChangeMasterKeyVC!
    var profileChangePasswordVC: ProfileChangePasswordVC!
    var profileOTPVerificationVC: ProfileOTPVerificationVC!
    var profileBasicViewController: ProfileBasicViewController!

    var profilemsg = ProfileToastMsgHeadingSubheadingLabels()
    var signupmsg = SignupToastMsgHeadingSubheadingLabels()

    override func setUp() {
        super.setUp()

        let storyboard: UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
        profileChangeMasterKeyVC = storyboard.instantiateViewController(withIdentifier: "ProfileChangeMasterKeyVC") as! ProfileChangeMasterKeyVC
        _ = profileChangeMasterKeyVC.view

        profileChangePasswordVC = storyboard.instantiateViewController(withIdentifier: "ProfileChangePasswordVC") as! ProfileChangePasswordVC
        _ = profileChangeMasterKeyVC.view

        profileOTPVerificationVC = storyboard.instantiateViewController(withIdentifier: "ProfileOTPVerificationVC") as! ProfileOTPVerificationVC
        _ = profileOTPVerificationVC.view

        profileBasicViewController = storyboard.instantiateViewController(withIdentifier: "ProfileBasicViewController") as! ProfileBasicViewController
        _ = profileBasicViewController.view

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testChangeMasterKey() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        profileChangeMasterKeyVC.submitButtontapped(oldKey: "", newKey: "")
        XCTAssertEqual(profilemsg.entercurrentmasterkey, profileChangeMasterKeyVC.placeholderone)

        profileChangeMasterKeyVC.submitButtontapped(oldKey: "1234", newKey: "")
        XCTAssertEqual(profilemsg.enternewmk, profileChangeMasterKeyVC.placeholdertwo)

        //    profileChangeMasterKeyVC.submitButtontapped(oldKey: "123499", newKey: "1234999")
        //    XCTAssertEqual(profilemsg.enternewmk,profileChangeMasterKeyVC.placeholdertwo)
    }

    func testProfileOtpVerification() {
//        profileOTPVerificationVC.doneButton(emailOtp: "123", smsOtp: "123456")
//        XCTAssertEqual(profilemsg.invalidemailotp,profileOTPVerificationVC.placeholderone)
//        
//        profileOTPVerificationVC.doneButton(emailOtp: "123456", smsOtp: "123456")
//        XCTAssertEqual(profilemsg.emailotp,profileOTPVerificationVC.placeholdertwo)
//        
//        profileOTPVerificationVC.doneButton(emailOtp: "", smsOtp: "123456")
//        XCTAssertEqual(profilemsg.enteremailOTP,profileOTPVerificationVC.placeholderthree)
//        
//        
//        profileOTPVerificationVC.doneButton(emailOtp: "123456", smsOtp: "1234")
//        XCTAssertEqual(profilemsg.invalidmobotp, profileOTPVerificationVC.placeholderfour)
//        
//        profileOTPVerificationVC.doneButton(emailOtp: "123456", smsOtp: "1234")
//        XCTAssertEqual(profilemsg.mobileotp, profileOTPVerificationVC.placeholderfive)
//        
//        profileOTPVerificationVC.doneButton(emailOtp: "123456", smsOtp:"" )
//        XCTAssertEqual(profilemsg.entermobotp, profileOTPVerificationVC.placeholdersix)
//        
//        

    }

    func testPassword() {
        //when password length is greater than 0
        profileChangePasswordVC.submitbuttonTapped(oldPassword: "Pritam@1995", newPassword: "", retypePassword: "")
        XCTAssertEqual(profilemsg.currentpassword, profileChangePasswordVC.placeHolderOne)

        profileChangePasswordVC.submitbuttonTapped(oldPassword: "Pritam1995", newPassword: "", retypePassword: "")
        XCTAssertEqual(profilemsg.currentpassword, profileChangePasswordVC.placeHolderOne)

    }

    func testProfileUpdate() {
        profileBasicViewController.savebuttonTapped(newProfileName: "", newPhoneNumber: "")
        XCTAssertEqual(profilemsg.entername, profileBasicViewController.placeHolderOne)

        profileBasicViewController.savebuttonTapped(newProfileName: "", newPhoneNumber: "")
        XCTAssertEqual(profilemsg.invalidphone, profileBasicViewController.placeHolderTwo)

        profileBasicViewController.verifybuttonTapped(newPhonenumber: "3456789", newEmail: "")
        XCTAssertEqual(profilemsg.invalidphone, profileBasicViewController.placeHolderFive)

        profileBasicViewController.verifybuttonTapped(newPhonenumber: "345678912", newEmail: "")
        XCTAssertEqual(profilemsg.invalidphone, profileBasicViewController.placeHolderFive)

        profileBasicViewController.verifybuttonTapped(newPhonenumber: "12345678", newEmail: "")
        XCTAssertEqual(profilemsg.phone, profileBasicViewController.placeHolderSix)

        profileBasicViewController.verifybuttonTapped(newPhonenumber: "12345678", newEmail: "pcplanfisheye.com")
    XCTAssertEqual(profilemsg.invalidemail, profileBasicViewController.placeHolderSeven)

//        profileBasicViewController.verifybuttonTapped(newPhonenumber: "12345678",newEmail: "pc@planfisheye.com")
//    XCTAssertEqual(profilemsg.emailid,profileBasicViewController.placeHolderEight)
    }

}
