//
//  MeetMeTestCases.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 11/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class MeetMeTestCases: XCTestCase {
    var createMeetVC: CreateMeetVC!
    var editMeetVC: EditMeetVC!

    override func setUp() {
        super.setUp()
        let storyboard: UIStoryboard = UIStoryboard(name: "Meetme", bundle: nil)

        createMeetVC = storyboard.instantiateViewController(withIdentifier: "CreateMeetVC") as! CreateMeetVC

        _ = createMeetVC.view

        let _: UIStoryboard = UIStoryboard(name: "Meetme", bundle: nil)

        editMeetVC = storyboard.instantiateViewController(withIdentifier: "EditMeetVC") as! EditMeetVC

        _ = editMeetVC.view
    }

    func testInputs() {
          createMeetVC.onMeetMeButtonTapped(meetingTitle: "", startdate: "", Enddate: "", startTime: "", EndTime: "")
          XCTAssertEqual("Invalid title", createMeetVC.placeHolderOne)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "MeetingTitle", startdate: "", Enddate: "", startTime: "", EndTime: "")
        XCTAssertEqual("Title", createMeetVC.placeHolderTwo)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "MeetmeTitle", startdate: "", Enddate: "", startTime: "HH:mm", EndTime: "")
        XCTAssertEqual("Invalid startTime", createMeetVC.InvalidStartTime)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "MeetMEtI", startdate: "16/04/2018", Enddate: "", startTime: "04:30", EndTime: "")
         XCTAssertEqual("valid startTime", createMeetVC.validStartTime)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "MeetMe", startdate: "16/04/2018", Enddate: "", startTime: "", EndTime: "HH:mm")
        XCTAssertEqual("Invalid endTime", createMeetVC.InvalidEndTime)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "MeetMe", startdate: "16/04/2018", Enddate: "", startTime: "", EndTime: "gfgghgf")
        XCTAssertEqual("Invalid endTime", createMeetVC.InvalidEndTime)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "MeetMe", startdate: "16/04/2018", Enddate: "", startTime: "", EndTime: "05:30")
        XCTAssertEqual("valid endTime", createMeetVC.validEndTime)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "", startdate: "dd/MM/yyyy", Enddate: "", startTime: "", EndTime: "")
        XCTAssertEqual("Invalid start date", createMeetVC.InvalidStartDate)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "", startdate: "12/05/2018", Enddate: "", startTime: "", EndTime: "")
        XCTAssertEqual("valid start date", createMeetVC.validStartDate)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "", startdate: "12/05/2018", Enddate: "dd/MM/yyyy", startTime: "", EndTime: "")
        XCTAssertEqual("Invalid EndDate", createMeetVC.InvalidEndDate)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "", startdate: "dfsghasfd", Enddate: "dd/MM/yyyy", startTime: "", EndTime: "")
        XCTAssertEqual("Invalid EndDate", createMeetVC.InvalidEndDate)

        createMeetVC.onMeetMeButtonTapped(meetingTitle: "", startdate: "12/05/2018", Enddate: "15/09/2018", startTime: "", EndTime: "")
        XCTAssertEqual("valid EndDate", createMeetVC.validEndDate)
      }

        func editMeetme() {
            editMeetVC.onSaveButtonTapped(meetingTitle: "", startdate: "", Enddate: "", startTime: "", EndTime: "")
            XCTAssertEqual("Invalid title", editMeetVC.placeHolderOne)

            editMeetVC.onSaveButtonTapped(meetingTitle: "MeetingTitle", startdate: "", Enddate: "", startTime: "", EndTime: "")
            XCTAssertEqual("Invalid title", editMeetVC.placeHolderTwo)

            editMeetVC.onSaveButtonTapped(meetingTitle: "", startdate: "", Enddate: "", startTime: "HH:mm", EndTime: "")
            XCTAssertEqual("invalid start time", editMeetVC.invalidStartTime)

            editMeetVC.onSaveButtonTapped(meetingTitle: "", startdate: "", Enddate: "", startTime: "12:30", EndTime: "")
            XCTAssertEqual("valid start time", editMeetVC.validStartTime)

            editMeetVC.onSaveButtonTapped(meetingTitle: "", startdate: "", Enddate: "", startTime: "", EndTime: "HH:mm")
            XCTAssertEqual("invalid end time", editMeetVC.inValidEndTime)

            editMeetVC.onSaveButtonTapped(meetingTitle: "", startdate: "", Enddate: "", startTime: "", EndTime: "4:40")
            XCTAssertEqual("valid end time", editMeetVC.validEndTime)

            editMeetVC.onSaveButtonTapped(meetingTitle: "", startdate: "dd/MM/yyyy", Enddate: "", startTime: "", EndTime: "")
            XCTAssertEqual("valid start date", editMeetVC.inValidStartDate)

            editMeetVC.onSaveButtonTapped(meetingTitle: "", startdate: "13/05/2018", Enddate: "", startTime: "", EndTime: "")
            XCTAssertEqual("valid start date", editMeetVC.validStartDate)

        }

    }
