//
//  SocialSignUpTestCases.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 01/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class SocialSignUpTestCases: XCTestCase {
      var signUpWithSocialLogin: SignUpWithSocialLogin!
      var signupmsg = SignupToastMsgHeadingSubheadingLabels()
      var profilemsg = ProfileToastMsgHeadingSubheadingLabels()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        signUpWithSocialLogin = storyboard.instantiateViewController(withIdentifier: "SignUpWithSocialLogin") as! SignUpWithSocialLogin
        _ = signUpWithSocialLogin.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSocailField() {

        //username
        signUpWithSocialLogin.saveButtonTapped(socialName: "", socialEmail: "pc@planfisheye.com", socialPhone: "8237359937")
        XCTAssertEqual(signupmsg.username, signUpWithSocialLogin.placeholderone)

        signUpWithSocialLogin.saveButtonTapped(socialName: "Pritam", socialEmail: "pc@planfisheye.com", socialPhone: "8237359937")
        XCTAssertEqual(signupmsg.name, signUpWithSocialLogin.placeholdertwo)

        //email
        signUpWithSocialLogin.saveButtonTapped(socialName: "Pritam", socialEmail: "", socialPhone: "8237359937")
        XCTAssertEqual(signupmsg.enteremail, signUpWithSocialLogin.placeholderthree)

        signUpWithSocialLogin.saveButtonTapped(socialName: "Pritam", socialEmail: "pc@planfisheye.com", socialPhone: "8237359937")
        XCTAssertEqual(signupmsg.email, signUpWithSocialLogin.placeholderfour)

        signUpWithSocialLogin.saveButtonTapped(socialName: "Pritam", socialEmail: "pcplanfisheye.com", socialPhone: "8237359937")
        XCTAssertEqual(signupmsg.invalidemail, signUpWithSocialLogin.placeholderfive)

        //phonenumber
        signUpWithSocialLogin.saveButtonTapped(socialName: "Pritam", socialEmail: "pc@planfisheye.com", socialPhone: "8237359937")
        XCTAssertEqual(signupmsg.Phoneno, signUpWithSocialLogin.placeholdersix)

//        signUpWithSocialLogin.saveButtonTapped(socialName: "Pritam",socialEmail : "pc@planfisheye.com" ,socialPhone : "82373599")
//        XCTAssertEqual(signupmsg.Phoneno,signUpWithSocialLogin.placeholdersix)

    }

}
