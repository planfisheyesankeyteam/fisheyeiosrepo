//
//  LoginTestCases.swift
//  fisheyeTests
//
//  Created by Sankey Solutions on 14/07/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class LoginTestCases: XCTestCase {

      //declaring the ViewController under test as an implicitly unwrapped optional
      var loginViewController: LoginViewController!
      var validationController = ValidationController()

      override func setUp() {
            super.setUp()
            // Put setup code here. This method is called before the invocation of each test method in the class.
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

            //get the ViewController we want to test from the storyboard (note the identifier is the id explicitly set in the identity inspector)
            loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            _ = loginViewController.view

      }

     override func tearDown() {
           // Put teardown code here. This method is called after the invocation of each test method in the class.
           super.tearDown()

     }

     func testemailInputValid() {
        let sampleString = "p@gmail.com"
          XCTAssertTrue(validationController.isValidEmail(testStr: sampleString), "Valid email")
        
        let sampleStringtwo = "pritamcgmail.com"
          XCTAssertFalse(validationController.isValidEmail(testStr: sampleStringtwo ), "Invalid email")

        let sampleStringthree = " "
          XCTAssertFalse(validationController.isValidEmail(testStr: sampleStringthree), "dont know")

     }

      func testpasswordInputValid() {
        let samplepassword = "Pritam@1995"
            XCTAssertTrue(validationController.isValidPassword(testStr: samplepassword), "valid password ")

        let samplepasswordtwo = "pritam!1995"
            XCTAssertFalse(validationController.isValidPassword(testStr: samplepasswordtwo), "invalid password")

        let samplepasswordthree = "Pritam1995"
            XCTAssertFalse(validationController.isValidPassword(testStr: samplepasswordthree), "invalid password")
       }

    func testLogin() {
                    let sampleString = "vaishalisagvekar20@gmail.com"
                    let samplepassword = "Vaishu@123"
                    loginViewController.userNameTextField.text = sampleString
                    loginViewController.passwordTextField.text = samplepassword
                    loginViewController.loginBtn.sendActions(for: .touchUpInside)

//
////                     sleep(20)
//        let statuscode = self.loginViewController.testStatuscode as? String
//
//                    if statuscode == "0"{
//
//                        XCTAssertTrue(true,"Login Success")
//                    } else{
//                       XCTAssertTrue(false,"Login Success")
//
//                    }
//        
    }

 }
