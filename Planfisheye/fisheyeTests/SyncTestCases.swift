//
//  SyncTestCases.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 08/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class SyncTestCases: XCTestCase {
    var addContactPersonalInfoVC: addContactPersonalInfoVC!
    var addContactProfesionalInfoVC: addContactProfesionalInfoVC!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        let storyboard: UIStoryboard = UIStoryboard(name: "Sync", bundle: nil)
        addContactPersonalInfoVC = storyboard.instantiateViewController(withIdentifier: "addContactPersonalInfoVC") as! addContactPersonalInfoVC
        _ = addContactPersonalInfoVC.view

        let _: UIStoryboard = UIStoryboard(name: "Sync", bundle: nil)
        addContactProfesionalInfoVC = storyboard.instantiateViewController(withIdentifier: "addContactProfesionalInfoVC") as! addContactProfesionalInfoVC
        _ = addContactProfesionalInfoVC.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAddContact() {
        addContactPersonalInfoVC.addPersonalInfo(contactName: "Abc", contactPhoneone: "123456890", conatctPhonetwo: "", contactPhonethree: "", contactEmail: "", contactEmailtwo: "", contactEmailthree: "")
        let isAddOperationSuccessful = addContactPersonalInfoVC.isAddOperationSuccessful as? Bool ?? false
        XCTAssertTrue(isAddOperationSuccessful, "validation successfull")

        addContactPersonalInfoVC.addPersonalInfo(contactName: "Abc", contactPhoneone: "12345", conatctPhonetwo: "", contactPhonethree: "", contactEmail: "", contactEmailtwo: "", contactEmailthree: "")
        let PhoneNumberisValidOne = addContactPersonalInfoVC.isAddOperationSuccessful as? Bool ?? false
        XCTAssertTrue(PhoneNumberisValidOne, "Invalid")

        addContactPersonalInfoVC.addPersonalInfo(contactName: "Abc", contactPhoneone: "123456789011112345678901", conatctPhonetwo: "", contactPhonethree: "", contactEmail: "", contactEmailtwo: "", contactEmailthree: "")
        let PhoneNumberisValidtwo = addContactPersonalInfoVC.isAddOperationSuccessful as? Bool ?? false
        XCTAssertTrue(PhoneNumberisValidOne, "valid")

    }

    func testCompanyaAdd() {
//                addContactPersonalInfoVC.checkValidation(profEmail: "")
//                XCTAssertEqual(syncText.email,addContactPersonalInfoVC.placeHolderOne)
//
//                addContactPersonalInfoVC.checkValidation(profEmail: "pcplanfisheye.com")
//                XCTAssertEqual(syncText.invalidEmail,addContactPersonalInfoVC.placeHolderTwo)
    }

}
