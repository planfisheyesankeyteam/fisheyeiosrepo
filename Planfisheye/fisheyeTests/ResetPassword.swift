//
//  ResetPassword.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 30/07/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class ResetPassword: XCTestCase {

    var resetPasswordSetNewPwdVC: ResetPasswordSetNewPwdVC!
    var validationController = ValidationController()
    var signupmsg = SignupToastMsgHeadingSubheadingLabels()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        resetPasswordSetNewPwdVC = storyboard.instantiateViewController(withIdentifier: "ResetPasswordSetNewPwdVC") as! ResetPasswordSetNewPwdVC
        _ = resetPasswordSetNewPwdVC.view

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testResetPassword() {
        // passwordTextField
        let samplepassword = "Pritam@1995"
        XCTAssertTrue(validationController.isValidPassword(testStr: samplepassword), "valid password ")

        let samplepasswordtwo = "pritam!1995"
        XCTAssertFalse(validationController.isValidPassword(testStr: samplepasswordtwo), "invalid password")

        let samplepasswordthree = "Pritam1995"
        XCTAssertFalse(validationController.isValidPassword(testStr: samplepasswordthree), "invalid password")

    }
    //check place holder when null value is passed only for first paramete
    func testPlaceholder() {

       resetPasswordSetNewPwdVC.resetPasswordFunctionCalled(newPassword: "", reEnterpassword: "")
        XCTAssertEqual(signupmsg.enterpassword, resetPasswordSetNewPwdVC.placeholder)

        resetPasswordSetNewPwdVC.resetPasswordFunctionCalled(newPassword: "", reEnterpassword: "")
        XCTAssertEqual(signupmsg.enterReEnterPassword, resetPasswordSetNewPwdVC.placeholder2)

    }

    func testInvalidPassword() {
     resetPasswordSetNewPwdVC.resetPasswordFunctionCalled(newPassword: "Pritam@1996", reEnterpassword: "")
     XCTAssertEqual(signupmsg.enterReEnterPassword, resetPasswordSetNewPwdVC.placeholder2)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
