//
//  SyncAddPersonalinfo.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 06/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class SyncAddPersonalinfo: XCTestCase {

    var addContactPersonalInfoVC: addContactPersonalInfoVC!
    var addContactProfesionalInfoVC: addContactProfesionalInfoVC!

    var syncText = SyncToastMessages()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
       let storyboard: UIStoryboard = UIStoryboard(name: "Sync", bundle: nil)
        addContactPersonalInfoVC = storyboard.instantiateViewController(withIdentifier: "addContactPersonalInfoVC") as! addContactPersonalInfoVC
        _ = addContactPersonalInfoVC.view

        let _: UIStoryboard = UIStoryboard(name: "Sync", bundle: nil)
        addContactProfesionalInfoVC = storyboard.instantiateViewController(withIdentifier: "addContactProfesionalInfoVC") as! addContactProfesionalInfoVC
        _ = addContactProfesionalInfoVC.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAddContact() {
        addContactPersonalInfoVC.addPersonalInfo(contactName: "Abc", contactPhoneone: "123456890", conatctPhonetwo: "", contactPhonethree: "", contactEmail: "", contactEmailtwo: "", contactEmailthree: "")
        let isAddOperationSuccessful = addContactPersonalInfoVC.isAddOperationSuccessful as? Bool ?? false
        XCTAssertTrue(isAddOperationSuccessful, "validation successfull")
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testCompanyaAdd() {
//        addContactPersonalInfoVC.checkValidation(profEmail: "")
//        XCTAssertEqual(syncText.email,addContactPersonalInfoVC.placeHolderOne)
//
//        addContactPersonalInfoVC.checkValidation(profEmail: "pcplanfisheye.com")
//        XCTAssertEqual(syncText.invalidEmail,addContactPersonalInfoVC.placeHolderTwo)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
