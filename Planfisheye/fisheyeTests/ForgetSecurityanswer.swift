//
//  ForgetSecurityanswer.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 30/07/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class ForgetSecurityanswer: XCTestCase {

    var forgetPWDSecurityQuestionVC: ForgetPWDSecurityQuestionVC!
    var signupmsg = SignupToastMsgHeadingSubheadingLabels()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        forgetPWDSecurityQuestionVC = storyboard.instantiateViewController(withIdentifier: "ForgetPWDSecurityQuestionVC") as! ForgetPWDSecurityQuestionVC

        _ = forgetPWDSecurityQuestionVC.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSecurityanswerValidation() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        forgetPWDSecurityQuestionVC.verifyAnswer(answerone: "abc", answertwo: "abc")

        let isValidationSuccess1 = self.forgetPWDSecurityQuestionVC.validation as? Bool

        XCTAssertFalse(isValidationSuccess1!, "valid")

        forgetPWDSecurityQuestionVC.verifyAnswer(answerone: " ", answertwo: " ")

         // let isValidationSuccess1 = self.forgetPWDSecurityQuestionVC.validation as? Bool


    }

    func testPlaceholder() {
     forgetPWDSecurityQuestionVC.verifyAnswer(answerone: "", answertwo: "")
      XCTAssertEqual(self.signupmsg.questionone, forgetPWDSecurityQuestionVC.placeholder)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
