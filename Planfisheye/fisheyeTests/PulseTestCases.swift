//
//  PulseTestCases.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 10/08/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class PulseTestCases: XCTestCase {

     var masterKeyPopUpViewController: MasterKeyPopUpViewController!
     var shareAndRequestPulseVCViewController: ShareAndRequestPulseVCViewController!
     var pulseMsgs = PulseToastMsgHeadingSubheadingLabels()
     var validationController = ValidationController()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard: UIStoryboard = UIStoryboard(name: "Pulse", bundle: nil)

        masterKeyPopUpViewController = storyboard.instantiateViewController(withIdentifier: "MasterKeyPopUpViewController") as! MasterKeyPopUpViewController

        _ = masterKeyPopUpViewController.view

        shareAndRequestPulseVCViewController = storyboard.instantiateViewController(withIdentifier: "ShareAndRequestPulseVCViewController") as! ShareAndRequestPulseVCViewController
        _ =  shareAndRequestPulseVCViewController.view

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()

    }
    //test masterkey validation
    func testMasterKey() {

        masterKeyPopUpViewController.validationCheck(PulsemasterKey: "")
        
    XCTAssertEqual(pulseMsgs.configOnErrorEnterMasterKey, masterKeyPopUpViewController.placeHolderOne)

        masterKeyPopUpViewController.actionAccToKeyVerifiedOfNot(verifyMasterKey: " ")
        XCTAssertEqual(pulseMsgs.enterMasterKey, masterKeyPopUpViewController.placeHolderTwo)

        masterKeyPopUpViewController.actionAccToKeyVerifiedOfNot(verifyMasterKey: "1234")
        XCTAssertEqual(pulseMsgs.enterMasterKey, masterKeyPopUpViewController.placeHolderTwo)

    }
    //only date is validated
    func testSchedulePulse() {
//        shareAndRequestPulseVCViewController.schedulingTimeLabel.text! = "16:24"
//        shareAndRequestPulseVCViewController.checkforProperScheduling(fromNextOrInsert: "fromInsert")
//
//        shareAndRequestPulseVCViewController.schedulingTimeLabel.text! = "HH:mm"
//        shareAndRequestPulseVCViewController.checkforProperScheduling(fromNextOrInsert: "fromInsert")
//
        shareAndRequestPulseVCViewController.scheduledOrNow="Scheduled"
          shareAndRequestPulseVCViewController.schedulingTimeLabel.text! = "16:24"
          shareAndRequestPulseVCViewController.schedulingDateLabel.text! = "12/03/1995"
          shareAndRequestPulseVCViewController.checkforProperScheduling(fromNextOrInsert: "fromInsert")
            sleep(2)
          XCTAssertEqual(shareAndRequestPulseVCViewController.startDatetest, "valid startdate")
      }

        func testEndDate() {
            shareAndRequestPulseVCViewController.scheduledOrNow="Scheduled"
            //shareAndRequestPulseVCViewController.schedulingTimeLabel.text! = "16:24"
            shareAndRequestPulseVCViewController.schedulingEndDateLabel.text! = "12/03/1995"
            shareAndRequestPulseVCViewController.checkforProperScheduling(fromNextOrInsert: "fromInsert")
            sleep(2)
             XCTAssertEqual(shareAndRequestPulseVCViewController.endDatetest, "valid date")
        }

       //test when start date is not entered
        func teststartdatisEmpty() {
            shareAndRequestPulseVCViewController.scheduledOrNow="Scheduled"
            shareAndRequestPulseVCViewController.schedulingTimeLabel.text! = "16:24"
            shareAndRequestPulseVCViewController.schedulingDateLabel.text! = "dd/MM/yyy"
            shareAndRequestPulseVCViewController.checkforProperScheduling(fromNextOrInsert: "fromInsert")
            sleep(2)
            XCTAssertEqual(shareAndRequestPulseVCViewController.startDateinvalidtest, "invalid date")
        }

         //test when end date is not entered
         func testenddateisEmpty() {
            shareAndRequestPulseVCViewController.scheduledOrNow="Scheduled"
            shareAndRequestPulseVCViewController.schedulingTimeLabel.text! = "16:24"
            shareAndRequestPulseVCViewController.schedulingDateLabel.text! = "12/03/1995"
            shareAndRequestPulseVCViewController.schedulingEndDateLabel.text! = "dd/MM/yyyy"
            shareAndRequestPulseVCViewController.checkforProperScheduling(fromNextOrInsert: "fromInsert")
            sleep(2)
            XCTAssertEqual("Invalid enddate", shareAndRequestPulseVCViewController.EndDateinvalidtest)
         }
    
    }
