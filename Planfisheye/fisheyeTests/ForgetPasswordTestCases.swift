//
//  ForgetPasswordTestCases.swift
//  fisheyeTests
//
//  Created by venkatesh murthy on 30/07/18.
//  Copyright © 2018 Sankey Solutions. All rights reserved.
//

import XCTest
@testable import fisheye

class ForgetPasswordTestCases: XCTestCase {

    var forgetPasswordScreen1VC: ForgetPasswordScreen1VC!
    var profilemsg = ProfileToastMsgHeadingSubheadingLabels()
    var signupmsg = SignupToastMsgHeadingSubheadingLabels()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        forgetPasswordScreen1VC = storyboard.instantiateViewController(withIdentifier: "ForgetPasswordScreen1VC") as! ForgetPasswordScreen1VC

        _ = forgetPasswordScreen1VC.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testcheckemailandpassword() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        //when email is not entered
        forgetPasswordScreen1VC.verifyUser(emailId: "", mobNumber: "8237359937")
        XCTAssertEqual(signupmsg.enteremailid, forgetPasswordScreen1VC.placeholderone)

        //when phone number is not entered
        forgetPasswordScreen1VC.verifyUser(emailId: "pc@planfisheye.com", mobNumber: "")
        XCTAssertEqual(signupmsg.entermobilenumber, forgetPasswordScreen1VC.placeholdertwo)

        //when phone number is less than 8
        forgetPasswordScreen1VC.verifyUser(emailId: "pc@planfisheye.com", mobNumber: "8237359")
        XCTAssertEqual(signupmsg.invalidphone, forgetPasswordScreen1VC.placeholderthree)

         //when phone number is greater  than 10
        forgetPasswordScreen1VC.verifyUser(emailId: "pc@planfisheye.com", mobNumber: "82373599675")
        XCTAssertEqual(signupmsg.invalidphone, forgetPasswordScreen1VC.placeholderthree)

        //when email is wrong
        forgetPasswordScreen1VC.verifyUser(emailId: "pcplanfisheye.com", mobNumber: "8237359937")
        XCTAssertEqual(profilemsg.invalidemail, forgetPasswordScreen1VC.placeholderfour)

    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
